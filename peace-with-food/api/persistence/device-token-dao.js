let util = require('util'),
    BaseDao = require('./base-dao');

function Dao() {
    BaseDao.call(this);
}

util.inherits(Dao, BaseDao);

Dao.prototype.addDeviceToken = async function(userId, os, token) {
    return this.executeQuery(
        'INSERT INTO `device-tokens` SET ? ON DUPLICATE KEY UPDATE `token` = VALUES(`token`)',
        {
            user_id: userId,
            os: os,
            token: token
        }
    )
};

Dao.prototype.getDeviceTokenById = async function(id) {
    const data = await this.executeQuery(
        'SELECT * FROM `device-tokens` WHERE `id` = ?',
        [
            id
        ]
    );
    return data[0];
};

Dao.prototype.getDeviceTokensByUserId = async function(userId) {
    return this.executeQuery(
        'SELECT * FROM `device-tokens` WHERE `user_id` = ?',
        [
            userId
        ]
    );
};

module.exports = new Dao();