let util = require('util'),
    BaseDao = require('./base-dao');

function Dao() {
    BaseDao.call(this);
}

util.inherits(Dao, BaseDao);

Dao.prototype.addSharedEntry = async function(entryId, coachId) {
    return this.executeQuery(
        'INSERT INTO `rhythm-entries-shared` SET ?',
        {
            entry_id: entryId,
            coach_id: coachId
        }
    );
};

Dao.prototype.addSharedEntriesByDate = async function(userId, coachId, title, beginEpoch, endEpoch)
{
    const data = await
      this.executeQuery(
        'INSERT INTO `rhythm-entries-shared-instances` SET ?',
        {
           title:        title,
           client_notes: '',
           client_id:    userId,
           coach_id:     coachId,
           from_time:    new Date(beginEpoch),
           to_time:      new Date(endEpoch),
        }
      );

    var instance_id = data.insertId;

    return this.executeQuery(
        'INSERT INTO `rhythm-entries-shared` ' +
        '(`instance_id`, `entry_id`) ' +
        'SELECT '+instance_id+', id from `rhythm-entries` ' +
        'WHERE `client_id` = ? ' +
        '  AND `created_date` >= ? ' +
        '  AND `created_date` <= ? ',
        [
            userId,
            new Date(beginEpoch),
            new Date(endEpoch)
        ]
    );
};

Dao.prototype.deleteSharedEntry = async function(entryId, coachId) {
    return this.executeQuery(
        'DELETE FROM `rhythm-entries-shared` WHERE `entry_id` = ? AND `coach_id` = ?',
        [
            entryId,
            coachId
        ]
    );
};

Dao.prototype.getSharedEntryById = async function(id) {
    const data = await this.executeQuery(
        'SELECT * FROM `rhythm-entries-shared` WHERE `id` = ?',
        [
            id
        ]
    );
    return data[0];
};

/**
 * page starts at 0
 */
Dao.prototype.getSharedInstancesAdjusted = async function(coachId, limit, page) {
    const data = await this.executeQuery(
        'SELECT ' +
        ' instance.*, ' +
        ' UNIX_TIMESTAMP( COALESCE( MIN(`rhythm-entries`.created_date), instance.`from_time` )) AS `from_epoch`, ' +
        ' UNIX_TIMESTAMP( COALESCE( MAX(`rhythm-entries`.created_date), instance.`to_time`   )) AS `to_epoch`, ' +
        ' MIN(`rhythm-entries`.created_date) AS `from__debug`, ' +
        ' MAX(`rhythm-entries`.created_date) AS `to__debug`, ' +
        ' UNIX_TIMESTAMP(instance.`created_date`) AS `created_epoch` ' +
        'FROM `rhythm-entries-shared-instances` instance ' +
        'LEFT JOIN `rhythm-entries-shared` ON instance_id =  instance.id ' +
        'LEFT JOIN `rhythm-entries`        ON entry_id    = `rhythm-entries`.id ' +
        'WHERE `coach_id` = ? ' +
        'GROUP BY instance.id ' +
        'ORDER BY instance.`created_date` DESC ' +
        'LIMIT ?,?',
        [
            coachId,
            page,
            limit
        ]
    );
    return data;
};

/**
 * page starts at 0
 */
Dao.prototype.getMySharedInstancesAdjusted = async function(clientId, limit, page) {
    const data = await this.executeQuery(
        'SELECT ' +
        ' instance.*, ' +
        ' UNIX_TIMESTAMP( COALESCE( MIN(`rhythm-entries`.created_date), instance.`from_time` )) AS `from_epoch`, ' +
        ' UNIX_TIMESTAMP( COALESCE( MAX(`rhythm-entries`.created_date), instance.`to_time`   )) AS `to_epoch`, ' +
        ' MIN(`rhythm-entries`.created_date) AS `from__debug`, ' +
        ' MAX(`rhythm-entries`.created_date) AS `to__debug`, ' +
        ' UNIX_TIMESTAMP(instance.`created_date`) AS `created_epoch` ' +
        'FROM `rhythm-entries-shared-instances` instance ' +
        'LEFT JOIN `rhythm-entries-shared` ON instance_id =  instance.id ' +
        'LEFT JOIN `rhythm-entries`        ON entry_id    = `rhythm-entries`.id ' +
        'WHERE `instance`.`client_id` = ? ' +
        'GROUP BY instance.id ' +
        'ORDER BY instance.`created_date` DESC ' +
        'LIMIT ?,?',
        [
            clientId,
            page,
            limit
        ]
    );
    return data;
};

Dao.prototype.getSharedInstanceAdjusted = async function(id) {
    const data = await this.executeQuery(
        'SELECT ' +
        ' instance.*, ' +
        ' UNIX_TIMESTAMP( COALESCE( MIN(`rhythm-entries`.created_date), instance.`from_time` )) AS `from_epoch`, ' +
        ' UNIX_TIMESTAMP( COALESCE( MAX(`rhythm-entries`.created_date), instance.`to_time`   )) AS `to_epoch`, ' +
        ' MIN(`rhythm-entries`.created_date) AS `from__debug`, ' +
        ' MAX(`rhythm-entries`.created_date) AS `to__debug`, ' +
        ' UNIX_TIMESTAMP(instance.`created_date`) AS `created_epoch` ' +
        'FROM `rhythm-entries-shared-instances` instance ' +
        'LEFT JOIN `rhythm-entries-shared` ON instance_id =  instance.id ' +
        'LEFT JOIN `rhythm-entries`        ON entry_id    = `rhythm-entries`.id ' +
        'WHERE `instance`.`id` = ? ',
        [
            id
        ]
    );
    return data[0];
};

module.exports = new Dao();
