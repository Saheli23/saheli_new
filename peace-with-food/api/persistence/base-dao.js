let mysql = require('mysql'),
    configs = require('../utils/configs'),
    connectionPool = mysql.createPool({
        host: configs.get('mysql').host,
        user: configs.get('mysql').username,
        password: configs.get('mysql').password,
        database: configs.get('mysql').database
    });

function BaseDao() {

}

function _executeQuery(pool, query, values) {
    return new Promise((resolve, reject) => {
        pool.getConnection((error, connection) => {
            if (error) {
                return reject(
                    '\nError connecting to pool while running query...\n' + query + '\nError: ' + JSON.stringify(error)
                );
            }
            connection.query(query, values || [], (error, rows) => {
                connection.release();
                if (error) {
                    return reject(error);
                }
                resolve(rows);
            });
        });
    });
}

BaseDao.prototype.executeQuery = function(query, values) {
    if (!connectionPool) {
        throw new Error('No connection pool defined for query %s!', JSON.stringify(query));
    }
    return _executeQuery(connectionPool, query, values);
};

module.exports = BaseDao;