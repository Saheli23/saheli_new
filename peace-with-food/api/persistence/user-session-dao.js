let util = require('util'),
    BaseDao = require('./base-dao');

function Dao() {
    BaseDao.call(this);
}

util.inherits(Dao, BaseDao);

Dao.prototype.createSession = async function(user, token, expires) {
    return this.executeQuery(
        'INSERT INTO `users-sessions` SET ?',
        {
            user_id: user,
            token: token,
            expire_date: expires
        }
    );
};

Dao.prototype.getBySessionId = async function(id) {
    const data = await this.executeQuery(
        'SELECT * FROM `users-sessions` WHERE id = ?',
        [
            id
        ]
    );
    return data[0];
};

Dao.prototype.getBySessionToken = async function(token) {
    const data = await this.executeQuery(
        'SELECT * FROM `users-sessions` WHERE token = ?',
        [
            token
        ]
    );
    return data[0];
};

module.exports = new Dao();