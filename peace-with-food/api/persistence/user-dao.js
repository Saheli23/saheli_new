let util = require('util'),
    BaseDao = require('./base-dao');

function Dao() {
    BaseDao.call(this);
}

util.inherits(Dao, BaseDao);

Dao.prototype.createUser = async function(email, firstName, lastName, role) {
    return this.executeQuery(
        'INSERT INTO `users` SET ?',
        {
            email: email,
            first_name: firstName,
            last_name: lastName,
            role: role
        }
    );
};

Dao.prototype.updateUserById = async function(id, model) {
    let params = {};
    if (model.first_name) {
        params.first_name = model.first_name;
    }
    if (model.last_name) {
        params.last_name = model.last_name;
    }
    if (model.email) {
        params.email = model.email;
    }

    return await this.executeQuery(
        'UPDATE users SET ? WHERE id = ?',
        [
            params,
            id
        ]
    )

};

Dao.prototype.getUsers = async function(offset, limit) {
    return this.executeQuery(
        'SELECT * FROM `users` WHERE enabled = 1 ORDER BY created_date DESC LIMIT ?, ?',
        [
            offset,
            limit
        ]
    );
};

Dao.prototype.getTotalUsers = async function() {
    const data = await this.executeQuery(
        'SELECT COUNT(id) AS total FROM `users` WHERE enabled = 1'
    );
    return data[0].total;
};

Dao.prototype.getUserById = async function(id) {
    const data = await this.executeQuery(
        'SELECT * FROM `users` WHERE id = ? AND enabled = 1',
        [
            id
        ]
    );
    return data[0];
};

Dao.prototype.getUsersByIds = async function(ids) {
    return this.executeQuery(
        'SELECT * FROM `users` WHERE id IN (?) AND enabled = 1',
        [
            ids
        ]
    );
};

Dao.prototype.getUserByEmail = async function(email) {
    const data = await this.executeQuery(
        'SELECT `users`.* ' +
        'FROM `users` '+
        'LEFT JOIN `users-emails` ON `users`.id = `users-emails`.`user_id` ' +
        'WHERE `users`.`email` = ? ' +
        '   OR `users-emails`.`email` = ? ' +
        'AND enabled = 1',
        [
            email,
            email
        ]
    );
    return data[0];
};

Dao.prototype.addEmail = async function(id, email) {
    return this.executeQuery(
        'INSERT IGNORE INTO `users-emails` SET ?',
        {
            email:   email,
            user_id: id,
        }
    );
};


module.exports = new Dao();
