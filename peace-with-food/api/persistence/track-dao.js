let util = require('util'),
    BaseDao = require('./base-dao');

function Dao() {
    BaseDao.call(this);
}

util.inherits(Dao, BaseDao);

Dao.prototype.createEntry = async function(userId, value) {
    return this.executeQuery(
        'INSERT INTO `rhythm-entries` SET ?',
        {
            client_id: userId,
            value: value
        }
    );
};

Dao.prototype.createEntryDetails = async function(userId, rhythmEntryId, thoughts, messageId, options) {
    if ( Object.prototype.toString.call(options) !== "[object String]" ){
           options = JSON.stringify(options);
    }
    return this.executeQuery(
        'INSERT INTO `rhythm-entries-detail` SET ?',
        {
            client_id:  userId,
            rhythm_id:  rhythmEntryId,
            thoughts:   thoughts,
	    message_id: messageId,
            options_json:  options
        }
    );
};

Dao.prototype.updateEntryById = async function(id, model) {

};

Dao.prototype.getEntryById = async function(id) {
    const data = await this.executeQuery(
        'SELECT * FROM `rhythm-entries` WHERE `id` = ?',
        [
            id
        ]
    );
    return data[0];
};

Dao.prototype.getEntryDetailsById = async function(id) {
    const rows = await this.executeQuery(
        'SELECT * FROM `rhythm-entries-detail` ' +
        'WHERE `rhythm_id` = ? ' +
        'ORDER BY created_date DESC ' +
        'LIMIT 1',
        [
            id
        ]
    );
    var data = rows[0];
    try {
    data['options'] = JSON.parse(data.options_json);
    } catch (e){ data['debug_error'] = e; }
    return data;
};

Dao.prototype.getEntriesByUserId = async function(userId, offset, limit) {
    var promise = this.executeQuery(
        'SELECT `rhythm-entries`.* ' +
             ', UNIX_TIMESTAMP(`rhythm-entries`.`created_date`) AS `time` ' +
             ', `rhythm-entries-detail`.`thoughts` ' +
             ', `rhythm-entries-detail`.`message_id` ' +
             ', `rhythm-entries-detail`.`options_json` ' +
        'FROM `rhythm-entries` ' +
        'LEFT JOIN `rhythm-entries-detail` ' +
                'ON `rhythm-entries`.id = `rhythm-entries-detail`.`rhythm_id` ' +
                '   AND `rhythm-entries`.client_id = `rhythm-entries-detail`.client_id ' +
        'WHERE `rhythm-entries`.`client_id` = ? ' +
        'GROUP BY `rhythm-entries`.id ' +
        'ORDER BY `rhythm-entries`.`created_date` DESC LIMIT ?, ?',
        [
            userId,
            offset,
            limit
        ]
    );
    
    
    return new Promise(async (resolve,reject) => {

            var data = await promise;
            
            for ( var i in data ){
                try {
                data[i]['options'] = JSON.parse(data[i]['options_json']);
                } catch (e){ data[i]['debug_error'] = e; }
            }
            resolve( data );
        

    });
};

Dao.prototype.getTotalEntriesByUserId = async function(userId) {
    let data = await this.executeQuery(
        'SELECT COUNT(`id`) AS `total` FROM `rhythm-entries` WHERE `client_id` = ?',
        [
            userId
        ]
    );
    return data[0].total;
};

Dao.prototype.getEntriesByUserIdBetweenDates = async function(userId, date1, date2) {
    var promise = this.executeQuery(
        'SELECT `rhythm-entries`.* ' +
             ', UNIX_TIMESTAMP(`rhythm-entries`.`created_date`) AS `time` ' +
             ', `rhythm-entries-detail`.`thoughts` ' +
             ', `rhythm-entries-detail`.`message_id` ' +
             ', `rhythm-entries-detail`.`options_json` ' +
        'FROM `rhythm-entries` ' +
        'LEFT JOIN `rhythm-entries-detail` ' +
                'ON `rhythm-entries`.id = `rhythm-entries-detail`.`rhythm_id` ' +
                '   AND `rhythm-entries`.client_id = `rhythm-entries-detail`.client_id ' +
        'WHERE `rhythm-entries`.`client_id` = ? ' +
        '  AND `rhythm-entries`.`created_date` BETWEEN ? AND ?' +
        'GROUP BY `rhythm-entries`.id ' +
        'ORDER BY `rhythm-entries`.`created_date` DESC',
        [
            userId,
            date1,
            date2
        ]
    );
    return new Promise(async (resolve,reject) => {

            var data = await promise;
            
            for ( var i in data ){
                try {
                data[i]['options'] = JSON.parse(data[i]['options_json']);
                } catch (e){ data[i]['debug_error'] = e; }
            }
            resolve( data );
        

    });
};

Dao.prototype.getMonthAverageEntriesByUserId = async function(userId, date1, date2) {
    return this.executeQuery(
        'SELECT COUNT(`id`) AS `total_entries`, ROUND(AVG(`value`), 2) AS `average_value`, DATE(`created_date`) AS `date` FROM `rhythm-entries` WHERE `client_id` = ? AND `created_date` BETWEEN ? AND ? GROUP BY YEAR(`created_date`), MONTH(`created_date`) ORDER BY `created_date` DESC',
        [
            userId,
            date1,
            date2
        ]
    );
};

module.exports = new Dao();
