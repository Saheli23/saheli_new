

ALTER TABLE `rhythm-entries-shared` ADD `instance_id` INT(11)  UNSIGNED  NOT NULL  AFTER `id`;
ALTER TABLE `rhythm-entries-shared` ADD UNIQUE INDEX `instance_id` (`instance_id`, `coach_id`, `entry_id`);
ALTER TABLE `rhythm-entries-shared` ADD INDEX (`created_date`);
ALTER TABLE `rhythm-entries-shared` DROP `coach_id`;




-- Create syntax for TABLE 'rhythm-entries-shared-instances'
CREATE TABLE `rhythm-entries-shared-instances` (
  `id`                int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type`              varchar(255)     NOT NULL,
  `title`             varchar(255)     NOT NULL,
  `client_notes`      text             NOT NULL,
  `client_id`         int(11) unsigned NOT NULL,
  `coach_id`          int(11) unsigned NOT NULL,
  `from_time`         datetime         NOT NULL,
  `to_time`           datetime         NOT NULL,
  `created_date`      datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date`     datetime         NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

PRIMARY KEY (`id`),
UNIQUE KEY `from_time` (`client_id`,`coach_id`,`from_time`,`to_time`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

