let util = require('util'),
    BaseDao = require('./base-dao');

function Dao() {
    BaseDao.call(this);
}

util.inherits(Dao, BaseDao);

Dao.prototype.addAuthenticationHash = async function(userId, hash) {
    return this.executeQuery(
        'INSERT INTO `users-authentications` SET ?',
        {
            user_id: userId,
            hash: hash
        }
    );
};

Dao.prototype.setAuthenticationHash = async function(userId, hash) {
    return this.executeQuery(
        'UPDATE `users-authentications` SET ? WHERE user_id = ?',
        [
            {
                hash: hash
            },
            userId
        ]
    );
};

Dao.prototype.addRecoveryAuthenticationHash = async function(userId, hash) {
    var p0, p1;
    p0 = this.executeQuery(
        'DELETE FROM `users-authentications-recover` WHERE `user_id` = ? AND `hash` != ?',
        [
            userId,
            hash
        ]
    );
    p1 = this.executeQuery(
        'INSERT INTO `users-authentications-recover` SET ?',
        {
            user_id: userId,
            hash: hash
        }
    );
    return Promise.all([ p0, p1 ]);
};

Dao.prototype.getByUserId = async function(id) {
    const data = await this.executeQuery(
        'SELECT * FROM `users-authentications` WHERE user_id = ?',
        [
            id
        ]
    );
    return data[0];
};

Dao.prototype.getRecoveryByUserId = async function(id) {
    const data = await this.executeQuery(
        'SELECT * FROM `users-authentications-recover` WHERE user_id = ?',
        [
            id
        ]
    );
    return data[0];
};

module.exports = new Dao();