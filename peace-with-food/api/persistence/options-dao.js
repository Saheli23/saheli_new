let util = require('util'),
    BaseDao = require('./base-dao');

function Dao() {
    BaseDao.call(this);
}

util.inherits(Dao, BaseDao);

Dao.prototype.getRhythms = async function() {
    return this.executeQuery(
        'SELECT * FROM `options-rhythms` ORDER BY `value` ASC'
    );
};

module.exports = new Dao();