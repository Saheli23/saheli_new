let util = require('util'),
    BaseDao = require('./base-dao');

function Dao() {
    BaseDao.call(this);
}

util.inherits(Dao, BaseDao);

Dao.prototype.getCoachesByClientId = async function(id, offset, limit) {
    return this.executeQuery(
        'SELECT * FROM `client-coach-relationships` WHERE `client_id` = ? AND `enabled` = 1 ORDER BY `modified_date` DESC LIMIT ?, ?',
        [
            id,
            offset,
            limit
        ]
    );
};

Dao.prototype.getTotalCoachesByClientId = async function(id) {
    const data = await this.executeQuery(
        'SELECT COUNT(id) AS total FROM `client-coach-relationships` WHERE `client_id` = ? AND `enabled` = 1',
        [
            id
        ]
    );
    return data[0].total;
};

Dao.prototype.getClientsByCoachId = async function(id, offset, limit) {
    return this.executeQuery(
        'SELECT * FROM `client-coach-relationships` WHERE `coach_id` = ? AND `enabled` = 1 ORDER BY `modified_date` DESC LIMIT ?, ?',
        [
            id,
            offset,
            limit
        ]
    );
};

Dao.prototype.getTotalClientsByCoachId = async function(id) {
    const data = await this.executeQuery(
        'SELECT COUNT(id) AS total FROM `client-coach-relationships` WHERE `coach_id` = ? AND `enabled` = 1',
        [
            id
        ]
    );
    return data[0].total;
};

Dao.prototype.getClientCoachRelationship = async function(clientId, coachId) {
    const data = await this.executeQuery(
        'SELECT * FROM `client-coach-relationships` WHERE `client_id` = ? AND `coach_id` = ? AND `enabled` = 1',
        [
            clientId,
            coachId
        ]
    );
    return data[0];
};

Dao.prototype.addClientCoachRelationship = async function(clientId, coachId) {
    return this.executeQuery(
        'INSERT INTO `client-coach-relationships` SET ? ON DUPLICATE KEY UPDATE `enabled` = VALUES(`enabled`)',
        {
            client_id: clientId,
            coach_id: coachId,
            enabled: true
        }
    )
};

Dao.prototype.deleteRelationshipById = async function(id) {
    return await this.executeQuery(
        'UPDATE `client-coach-relationships` SET ? WHERE `id` = ?',
        [
            {
                enabled: false
            },
            id
        ]
    )
};

Dao.prototype.getConditionalByCode = async function(code, coachid) {
    const data = await this.executeQuery(
        'SELECT * FROM `client-coach-conditional` WHERE `code` = ?',
        [
            code,
            coachid
        ]
    );
    return data[0];
};

Dao.prototype.getConditional = async function( clientid, email ) {
    const data = await this.executeQuery(
        'SELECT * FROM `client-coach-conditional` ' +
        'WHERE `client_id` = ? ' +
        '  AND `email`     = ? ',
        [
            clientid,
            email
        ]
    );
    return data[0];
};

Dao.prototype.addConditional = async function( clientid, email, code, token_hashed ) {
    return this.executeQuery(
        'INSERT INTO `client-coach-conditional` SET ? ',
        {
            client_id: clientid,
            email,
            code,
            hash_token: token_hashed
            }
    )
};


module.exports = new Dao();