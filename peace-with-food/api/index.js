let express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    configs = require('./utils/configs'),
    app = express();

//
// verify config
//
 configs.get("api_host"); // should be the ip or hostname of the api

//

let userController = require('./controllers/user-controller')(app),
    linkController = require('./controllers/link-controller')(app),
    testController = require('./controllers/test-controller')(app),
    rhythmController = require('./controllers/rhythm-controller')(app),
    notificationController = require('./controllers/notification-controller')(app);

let userSessionMiddleware = require('./middleware/user-session-middleware');

app.use(cors());
app.use(bodyParser.urlencoded({limit: '500mb', extended: true}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(userSessionMiddleware);

userController.init();
linkController.init();
testController.init();
rhythmController.init();
notificationController.init();

app.use('/docs', express.static(__dirname + '/docs'));

app.listen(configs.get('port'));