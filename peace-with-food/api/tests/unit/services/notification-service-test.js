let fs = require('fs'),
    path = require('path'),
    expect = require('chai').expect,
    rewire = require('rewire'),
    InvalidRequestErrorModel = require('../../../models/error/invalid-request-error-model'),
    NotFoundErrorModel = require('../../../models/error/not-found-error-model'),
    ForbiddenErrorModel = require('../../../models/error/forbidden-error-model'),
    DeviceTokenResponseModel = require('../../../models/response/device-token-response-model'),
    DeviceTokenRequestModel = require('../../../models/request/device-token-request-model'),
    notificationService;

const MOCK_DEVICE_TOKEN = {
    id: 1,
    user_id: 1,
    os: 1,
    token: 'abc123'
};

const MOCK_USER = {
    id: 1,
    role: 1,
    email: 'example@peacewithfood.com'
};

describe('Notification Service Test', () => {
    before(() => {
        notificationService = rewire('../../../services/notification-service');
        notificationService.__set__('userDao', {
            getUserById: (id) => {
                if (id === MOCK_USER.id) {
                    return new Promise((resolve) => {
                        resolve(MOCK_USER);
                    });
                }
                return new Promise((resolve) => {
                    resolve(undefined);
                });
            }
        });
        notificationService.__set__('deviceTokenDao', {
            addDeviceToken: () => {
                return new Promise((resolve) => {
                    resolve({ insertId: MOCK_DEVICE_TOKEN.id });
                });
            },
            getDeviceTokenById: (id) => {
                if (id === 1) {
                    return new Promise((resolve) => {
                        resolve(MOCK_DEVICE_TOKEN);
                    });
                }
                return new Promise((resolve) => {
                    resolve(undefined);
                });
            },
            getDeviceTokensByUserId: (userId) => {
                if (userId === MOCK_DEVICE_TOKEN.user_id) {
                    return new Promise((resolve) => {
                        resolve([MOCK_DEVICE_TOKEN]);
                    });
                }
                return new Promise((resolve) => {
                    resolve([]);
                });
            }
        });
    });

    describe('#addDeviceToken(...)', () => {
        it('should add a new device token', async () => {
            const entry = await notificationService.addDeviceToken(new DeviceTokenRequestModel({
                user_id: MOCK_DEVICE_TOKEN.user_id,
                os: 'ios',
                token: MOCK_DEVICE_TOKEN.token
            }));
            expect(entry).to.be.an.instanceof(DeviceTokenResponseModel);
        });
        it('should not add a device token with an invalid OS', async () => {
            let entry = undefined;
            try {
                entry = await notificationService.addDeviceToken(new DeviceTokenRequestModel({
                    user_id: MOCK_DEVICE_TOKEN.user_id,
                    os: 'boop',
                    token: MOCK_DEVICE_TOKEN.token
                }));
            } catch (e) {
                expect(e).to.be.an.instanceof(InvalidRequestErrorModel);
            }
            expect(entry).to.be.undefined;
        })
        it('should not add a device token with an invalid user ID', async () => {
            let entry = undefined;
            try {
                entry = await notificationService.addDeviceToken(new DeviceTokenRequestModel({
                    user_id: 99,
                    os: 'ios',
                    token: MOCK_DEVICE_TOKEN.token
                }));
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(entry).to.be.undefined;
        });
    });

    describe('#getDeviceTokenById(...)', () => {
        it('should return a device token', async () => {
            let entry = await notificationService.getDeviceTokenById(MOCK_DEVICE_TOKEN.id);
            expect(entry).to.be.an.instanceof(DeviceTokenResponseModel);
        });
        it('should not return a device token if it doesn\'t exist', async () => {
            let entry = undefined;
            try {
                entry = await notificationService.getDeviceTokenById(99);
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(entry).to.be.undefined;
        });
    });

    describe('#getDeviceTokenByUserIdAndOs(...)', () => {
        it('should return a device token', async () => {
            let entry = await notificationService.getDeviceTokenByUserIdAndOs(MOCK_DEVICE_TOKEN.user_id, MOCK_DEVICE_TOKEN.os);
            expect(entry).to.be.an.instanceof(DeviceTokenResponseModel);
        });
        it('should not return a device token if it doesn\'t exist', async () => {
            let entry = undefined;
            try {
                entry = await notificationService.getDeviceTokenByUserIdAndOs(99, MOCK_DEVICE_TOKEN.os);
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(entry).to.be.undefined;
        });
    });
});