let fs = require('fs'),
    path = require('path'),
    expect = require('chai').expect,
    rewire = require('rewire'),
    TrackEntryRequestModel = require('../../../models/request/track-entry-request-model'),
    TrackEntryResponseModel = require('../../../models/response/track-entry-response-model'),
    TrackEntryListResponseModel = require('../../../models/response/track-entry-list-response-model'),
    InvalidRequestErrorModel = require('../../../models/error/invalid-request-error-model'),
    NotFoundErrorModel = require('../../../models/error/not-found-error-model'),
    ForbiddenErrorModel = require('../../../models/error/forbidden-error-model'),
    trackService;

const MOCK_CLIENT = {
    id: 1,
    role: 1,
    email: 'example@peacewithfood.com'
};

const MOCK_COACH = {
    id: 2,
    role: 2,
    email: 'example2@peacewithfood.com'
};

const MOCK_TRACK_ENTRY = {
    id: 1,
    client_id: 1
};

describe('Track Service Test', () => {
    before(() => {
        trackService = rewire('../../../services/track-service');
        trackService.__set__('userDao', {
            getUserById: (id) => {
                if (id === MOCK_CLIENT.id) {
                    return new Promise((resolve) => {
                        resolve(MOCK_CLIENT);
                    });
                }
                if (id === MOCK_COACH.id) {
                    return new Promise((resolve) => {
                        resolve(MOCK_COACH);
                    });
                }
                return new Promise((resolve) => {
                    resolve(undefined);
                });
            }
        });
        trackService.__set__('trackDao', {
            createEntry: () => {
                return new Promise((resolve) => {
                    resolve({ insertId: MOCK_TRACK_ENTRY.id });
                });
            },
            updateEntryById: () => {
                return new Promise((resolve) => {
                    resolve({ insertId: MOCK_TRACK_ENTRY.id });
                });
            },
            getEntryById: (id) => {
                if (id === MOCK_TRACK_ENTRY.id) {
                    return new Promise((resolve) => {
                        resolve(MOCK_TRACK_ENTRY);
                    });
                }
                return new Promise((resolve) => {
                    resolve(undefined);
                });
            },
            getEntriesByUserId: () => {
                return new Promise((resolve) => {
                    resolve([MOCK_TRACK_ENTRY]);
                });
            },
            getEntriesByUserIdBetweenDates: () => {
                return new Promise((resolve) => {
                    resolve([MOCK_TRACK_ENTRY]);
                });
            },
            getTotalEntriesByUserId: () => {
                return new Promise((resolve) => {
                    resolve(1);
                });
            }
        });

        trackService.__set__('sharedRhythmsDao', {
            addSharedEntry: () => {
                return new Promise((resolve) => {
                    resolve({ insertId: MOCK_TRACK_ENTRY.id });
                });
            },
            deleteSharedEntry: () => {
                return new Promise((resolve) => {
                    resolve({ affectedRows: 1 });
                });
            }
        });
    });

    describe('#createEntry(...)', () => {
        it('should create a new trackable entry', async () => {
            let entry = await trackService.createEntry(new TrackEntryRequestModel({
                user_id: MOCK_TRACK_ENTRY.client_id,
                value: 1
            }));
            expect(entry).to.be.an.instanceof(TrackEntryResponseModel);
        });
        it('should not create a trackable entry with an invalid user ID', async () => {
            let entry = undefined;
            try {
                entry = await trackService.createEntry(new TrackEntryRequestModel({
                    user_id: 99,
                    value: 1
                }));
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(entry).to.be.undefined;
        });
        it('should not create a trackable entry with a missing user ID', async () => {
            let entry = undefined;
            try {
                entry = await trackService.createEntry(new TrackEntryRequestModel({value: 1}));
            } catch (e) {
                expect(e).to.be.an.instanceof(InvalidRequestErrorModel);
            }
            expect(entry).to.be.undefined;
        });
        it('should not create a trackable entry with a missing value', async () => {
            let entry = undefined;
            try {
                entry = await trackService.createEntry(new TrackEntryRequestModel({user_id: MOCK_TRACK_ENTRY.client_id}));
            } catch (e) {
                expect(e).to.be.an.instanceof(InvalidRequestErrorModel);
            }
            expect(entry).to.be.undefined;
        });
    });

    describe('#updateEntryById(...)', () => {
        it('should update an entry', async () => {
            let entry = await trackService.updateEntryById(MOCK_TRACK_ENTRY.id, new TrackEntryRequestModel({
                user_id: MOCK_CLIENT.id
            }));
            expect(entry).to.be.an.instanceof(TrackEntryResponseModel);
        });
        it('should not update an entry with an invalid ID', async () => {
            let entry = undefined;
            try {
                entry = await trackService.updateEntryById(99, new TrackEntryRequestModel({
                    user_id: MOCK_CLIENT.id
                }));
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(entry).to.be.undefined;
        });
    });

    describe('#getEntryById(...)', () => {
        it('should return an entry', async () => {
            let entry = await trackService.getEntryById(MOCK_TRACK_ENTRY.id);
            expect(entry).to.be.an.instanceof(TrackEntryResponseModel);
        });
        it('should not return an entry if it doesn\'t exist', async () => {
            let entry = undefined;
            try {
                entry = await trackService.getEntryById(99);
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(entry).to.be.undefined;
        });
    });

    describe('#getEntriesByUserId(...)', () => {
        it('should return entries for a user ID', async () => {
            let entries = await trackService.getEntriesByUserId(MOCK_TRACK_ENTRY.client_id, 0, 20);
            expect(entries).to.be.an.instanceof(TrackEntryListResponseModel);
        });
        it('should return entries for a user ID without offset and limit', async () => {
            let entries = await trackService.getEntriesByUserId(MOCK_TRACK_ENTRY.client_id);
            expect(entries).to.be.an.instanceof(TrackEntryListResponseModel);
        });
    });

    describe('#getEntriesByUserIdBetweenDates(...)', () => {
        it('should return entries for a user ID between dates', async () => {
            let entries = await trackService.getEntriesByUserIdBetweenDates(MOCK_TRACK_ENTRY.client_id, '2017-01-01', '2017-01-31');
            expect(entries).to.be.an.instanceof(TrackEntryListResponseModel);
        });
        it('should throw an error if invalid start date provided', async () => {
            let entries = undefined;
            try {
                entries = await trackService.getEntriesByUserIdBetweenDates(MOCK_TRACK_ENTRY.client_id, '2017', '2017-01-31');
            } catch (e) {
                expect(e).to.be.an.instanceof(InvalidRequestErrorModel);
            }
            expect(entries).to.be.undefined;
        });
        it('should throw an error if invalid end date provided', async () => {
            let entries = undefined;
            try {
                entries = await trackService.getEntriesByUserIdBetweenDates(MOCK_TRACK_ENTRY.client_id, '2017-01-01', '2017');
            } catch (e) {
                expect(e).to.be.an.instanceof(InvalidRequestErrorModel);
            }
            expect(entries).to.be.undefined;
        });
    });

    describe('#addSharedEntry(...)', () => {
        it('should share a trackable entry', async() => {
            let entry = await trackService.addSharedEntry(MOCK_TRACK_ENTRY.client_id, MOCK_TRACK_ENTRY.id, MOCK_COACH.id);;
            expect(entry).to.be.an.instanceof(TrackEntryResponseModel);
        });
        it('should not share a trackable entry if the requesting user is not the owner', async() => {
            let entry = undefined;
            try {
                entry = await trackService.addSharedEntry(99, MOCK_TRACK_ENTRY.id, MOCK_COACH.id);
            } catch (e) {
                expect(e).to.be.an.instanceof(ForbiddenErrorModel);
            }
            expect(entry).to.be.undefined;
        });
        it('should not share a trackable entry if the coach does not exist', async() => {
            let entry = undefined;
            try {
                entry = await trackService.addSharedEntry(MOCK_TRACK_ENTRY.client_id, MOCK_TRACK_ENTRY.id, 99);
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(entry).to.be.undefined;
        });
    });

    describe('#deleteSharedEntry(...)', () => {
        it('should remove a shared trackable entry', async() => {
            try {
                await trackService.deleteSharedEntry(MOCK_TRACK_ENTRY.client_id, MOCK_TRACK_ENTRY.id, MOCK_COACH.id);;
            } catch (e) {
                expect(e).to.be.undefined;
            }
        });
        it('should not remove a shared trackable entry if the requesting user is not the owner', async() => {
            let entry = undefined;
            try {
                entry = await trackService.deleteSharedEntry(99, MOCK_TRACK_ENTRY.id, MOCK_COACH.id);
                expect(entry).to.not.be.undefined;
            } catch (e) {
                expect(e).to.be.an.instanceof(ForbiddenErrorModel);
            }
        });
    });
});