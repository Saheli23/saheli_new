let fs = require('fs'),
    path = require('path'),
    expect = require('chai').expect,
    rewire = require('rewire'),
    UserRequestModel = require('../../../models/request/user-request-model'),
    ClientUserRequestModel = require('../../../models/request/client-user-request-model'),
    CoachUserRequestModel = require('../../../models/request/coach-user-request-model'),
    UserResponseModel = require('../../../models/response/user-response-model'),
    ClientUserResponseModel = require('../../../models/response/client-user-response-model'),
    CoachUserResponseModel = require('../../../models/response/coach-user-response-model'),
    UserListResponseModel = require('../../../models/response/user-list-response-model'),
    ClientCoachListResponseModel = require('../../../models/response/client-coach-list-response-model'),
    CoachClientListResponseModel = require('../../../models/response/coach-client-list-response-model'),
    ClientCoachResponseModel = require('../../../models/response/client-coach-response-model'),
    DuplicateEntityResponseModel = require('../../../models/error/duplicate-entity-error-model'),
    NotFoundErrorModel = require('../../../models/error/not-found-error-model'),
    userService;

const MOCK_USER = {
    id: 1,
    email: 'example@peacewithfood.com',
    role: 1
};

const MOCK_USER_2 = {
    id: 2,
    email: 'example2@peacewithfood.com',
    role: 2
};

const MOCK_USER_3 = {
    id: 3,
    email: 'example3@peacewithfood.com',
    role: 2
};

const MOCK_CLIENT_COACH_RELATIONSHIP = {
    id: 1,
    client_id: MOCK_USER.id,
    coach_id: MOCK_USER_2.id
};

describe('User Service Test', () => {
    before(() => {
        userService = rewire('../../../services/user-service');
        userService.__set__('userDao', {
            createUser: () => {
                return new Promise((resolve) => {
                    resolve({ insertId: MOCK_USER.id });
                });
            },
            updateUserById: () => {
                return new Promise((resolve) => {
                    resolve({ insertId: MOCK_USER.id });
                });
            },
            getUserById: (id) => {
                if (id === MOCK_USER.id) {
                    return new Promise((resolve) => {
                        resolve(MOCK_USER);
                    });
                }
                if (id === MOCK_USER_2.id) {
                    return new Promise((resolve) => {
                        resolve(MOCK_USER_2);
                    });
                }
                if (id === MOCK_USER_3.id) {
                    return new Promise((resolve) => {
                        resolve(MOCK_USER_3);
                    });
                }
                return new Promise((resolve) => {
                    resolve(undefined);
                });
            },
            getUsersByIds: (ids) => {
                let users = [];
                if (ids.indexOf(MOCK_USER.id) !== -1) {
                    users.push(MOCK_USER);
                }
                if (ids.indexOf(MOCK_USER_2.id) !== -1) {
                    users.push(MOCK_USER_2);
                }
                if (ids.indexOf(MOCK_USER_3.id) !== -1) {
                    users.push(MOCK_USER_3);
                }
                return new Promise((resolve) => {
                    resolve(users);
                });
            },
            getUserByEmail: (email) => {
                if (email === MOCK_USER.email) {
                    return new Promise((resolve) => {
                        resolve(MOCK_USER);
                    });
                }
                if (email === MOCK_USER_2.email) {
                    return new Promise((resolve) => {
                        resolve(MOCK_USER_2);
                    });
                }
                if (email === MOCK_USER_3.email) {
                    return new Promise((resolve) => {
                        resolve(MOCK_USER_3);
                    });
                }
                return new Promise((resolve) => {
                    resolve(undefined);
                });
            },
            getTotalUsers: () => {
                return new Promise((resolve) => {
                    resolve(3);
                });
            },
            getUsers: () => {
                return new Promise((resolve) => {
                    resolve([
                        MOCK_USER,
                        MOCK_USER_2,
                        MOCK_USER_3
                    ]);
                });
            }
        });
        userService.__set__('clientCoachDao', {
            getTotalCoachesByClientId: (id) => {
                if (id === MOCK_USER.id) {
                    return new Promise((resolve) => {
                        resolve(1);
                    });
                }
                return new Promise((resolve) => {
                    resolve(0);
                });
            },
            getCoachesByClientId: (id) => {
                if (id === MOCK_USER.id) {
                    return new Promise((resolve) => {
                        resolve([MOCK_USER_2]);
                    });
                }
                return new Promise((resolve) => {
                    resolve([]);
                });
            },
            getTotalClientsByCoachId: (id) => {
                if (id === MOCK_USER_2.id) {
                    return new Promise((resolve) => {
                        resolve(1);
                    });
                }
                return new Promise((resolve) => {
                    resolve(0);
                });
            },
            getClientsByCoachId: (id) => {
                if (id === MOCK_USER_2.id) {
                    return new Promise((resolve) => {
                        resolve([MOCK_USER]);
                    });
                }
                return new Promise((resolve) => {
                    resolve([]);
                });
            },
            getClientCoachRelationship: (clientId, coachId) => {
                if (clientId === MOCK_USER.id && coachId === MOCK_USER_2.id) {
                    return new Promise((resolve) => {
                        resolve(MOCK_CLIENT_COACH_RELATIONSHIP);
                    });
                }
                return new Promise((resolve) => {
                    resolve(undefined);
                });
            },
            addClientCoachRelationship: () => {
                return new Promise((resolve) => {
                    resolve({ insertId: MOCK_CLIENT_COACH_RELATIONSHIP.id });
                });
            }
        });
    });

    describe('#createUser(...)', () => {
        it('should create a new user', async () => {
            let user = await userService.createUser(new UserRequestModel({
                email: 'example-new-user@peacewithfood.com'
            }));
            expect(user).to.be.an.instanceof(UserResponseModel);
        });
        it('should not create a user with a duplicate email address', async () => {
            let user = undefined;
            try {
                user = await userService.createUser(new UserRequestModel({
                    email: MOCK_USER.email
                }));
            } catch (e) {
                expect(e).to.be.an.instanceof(DuplicateEntityResponseModel);
            }
            expect(user).to.be.undefined;
        });
        it('should create a new client user', async () => {
            let user = await userService.createUser(new ClientUserRequestModel({
                email: 'example-new-user@peacewithfood.com'
            }));
            expect(user).to.be.an.instanceof(UserResponseModel);
        });
        it('should create a new coach user', async () => {
            let user = await userService.createUser(new CoachUserRequestModel({
                email: 'example-new-user@peacewithfood.com'
            }));
            expect(user).to.be.an.instanceof(UserResponseModel);
        });
    });

    describe('#updateUserById(...)', () => {
        it('should update a user', async () => {
            let user = await userService.updateUserById(MOCK_USER.id, new UserRequestModel({
                email: 'example-new-user@peacewithfood.com'
            }));
            expect(user).to.be.an.instanceof(UserResponseModel);
        });
        it('should not update a user with an invalid ID', async () => {
            let user = undefined;
            try {
                user = await userService.updateUserById(99, new UserRequestModel({
                    email: MOCK_USER.email
                }));
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(user).to.be.undefined;
        });
        it('should not update a user with a duplicate email', async () => {
            let user = undefined;
            try {
                user = await userService.updateUserById(1, new UserRequestModel({
                    email: MOCK_USER_2.email
                }));
            } catch (e) {
                expect(e).to.be.an.instanceof(DuplicateEntityResponseModel);
            }
            expect(user).to.be.undefined;
        });
    });

    describe('#getUserById(...)', () => {
        it('should return a user', async () => {
            let user = await userService.getUserById(MOCK_USER.id);
            expect(user).to.be.an.instanceof(UserResponseModel);
        });
        it('should not return a user if it doesn\'t exist', async () => {
            let user = undefined;
            try {
                user = await userService.getUserById(99);
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(user).to.be.undefined;
        });
    });

    describe('#getUserByEmail(...)', () => {
        it('should return a user', async () => {
            let user = await userService.getUserByEmail(MOCK_USER.email);
            expect(user).to.be.an.instanceof(UserResponseModel);
        });
        it('should not return a user if it doesn\'t exist', async () => {
            let user = undefined;
            try {
                user = await userService.getUserByEmail('example-new-user@peacewithfood.com');
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(user).to.be.undefined;
        });
    });

    describe('#getUsers(...)', () => {
        it('should return a list of users', async () => {
            let users = await userService.getUsers(0, 20);
            expect(users).to.be.an.instanceof(UserListResponseModel);
            expect(users.total).to.equal(3);
            expect(users.users).to.have.lengthOf(3);
        });
    });

    describe('#getCoachUserByEmail(...)', () => {
        it('should return a coach', async () => {
            let user = await userService.getCoachUserByEmail(MOCK_USER_2.email);
            expect(user).to.be.an.instanceof(CoachUserResponseModel);
        });
        it('should throw an error if coach isn\'t found', async () => {
            let user = undefined;
            try {
                user = await userService.getCoachUserByEmail(MOCK_USER.email);
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(user).to.be.undefined;
        });
    });

    describe('#getClientUserByEmail(...)', () => {
        it('should return a client', async () => {
            let user = await userService.getClientUserByEmail(MOCK_USER.email);
            expect(user).to.be.an.instanceof(ClientUserResponseModel);
        });
        it('should throw an error if client isn\'t found', async () => {
            let user = undefined;
            try {
                user = await userService.getClientUserByEmail(MOCK_USER_2.email);
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(user).to.be.undefined;
        });
    });

    describe('#getCoachesByClientId(...)', () => {
        it('should return a list of coaches', async () => {
            let coaches = await userService.getCoachesByClientId(MOCK_USER.id, 0, 20);
            expect(coaches).to.be.an.instanceof(ClientCoachListResponseModel);
            expect(coaches.total).to.equal(1);
            expect(coaches.coaches).to.have.lengthOf(1);
        });
        it('should return an empty list of coaches if none found', async () => {
            let coaches = await userService.getCoachesByClientId(99, 0, 20);
            expect(coaches).to.be.an.instanceof(ClientCoachListResponseModel);
            expect(coaches.total).to.equal(0);
            expect(coaches.coaches).to.have.lengthOf(0);
        });
    });

    describe('#getClientsByCoachId(...)', () => {
        it('should return a list of clients', async () => {
            let clients = await userService.getClientsByCoachId(MOCK_USER_2.id, 0, 20);
            expect(clients).to.be.an.instanceof(CoachClientListResponseModel);
            expect(clients.total).to.equal(1);
            expect(clients.clients).to.have.lengthOf(1);
        });
        it('should return an empty list of clients if none found', async () => {
            let clients = await userService.getClientsByCoachId(99, 0, 20);
            expect(clients).to.be.an.instanceof(CoachClientListResponseModel);
            expect(clients.total).to.equal(0);
            expect(clients.clients).to.have.lengthOf(0);
        });
    });

    describe('#getCoachesByIds(...)', () => {
        it('should return a list of coaches', async () => {
            let coaches = await userService.getCoachesByIds([MOCK_USER_2.id]);
            expect(coaches).to.have.lengthOf(1);
            coaches.forEach((coach) => {
                expect(coach).to.be.an.instanceof(CoachUserResponseModel);
            })
        });
        it('should return an empty list if no coaches found', async () => {
            let coaches = await userService.getCoachesByIds([MOCK_USER.id, 99]);
            expect(coaches).to.have.lengthOf(0);
        });
    });

    describe('#getClientsByIds(...)', () => {
        it('should return a list of clients', async () => {
            let clients = await userService.getClientsByIds([MOCK_USER.id]);
            expect(clients).to.have.lengthOf(1);
            clients.forEach((client) => {
                expect(client).to.be.an.instanceof(ClientUserResponseModel);
            })
        });
        it('should return an empty list if no clients found', async () => {
            let clients = await userService.getClientsByIds([MOCK_USER_2.id, 99]);
            expect(clients).to.have.lengthOf(0);
        });
    });

    describe('#getClientCoachRelationship(...)', () => {
        it('should return a client coach relationship', async () => {
            let relationship = await userService.getClientCoachRelationship(MOCK_USER.id, MOCK_USER_2.id);
            expect(relationship).to.be.an.instanceof(ClientCoachResponseModel);
        });
        it('should throw error if no client found', async () => {
            let relationship = undefined;
            try {
                relationship = await userService.getClientCoachRelationship(0, MOCK_USER_2.id);
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(relationship).to.be.undefined;
        });
        it('should throw error if no coach found', async () => {
            let relationship = undefined;
            try {
                relationship = await userService.getClientCoachRelationship(MOCK_USER.id, 99);
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(relationship).to.be.undefined;
        });
        it('should throw error if no relationship found', async () => {
            let relationship = undefined;
            try {
                relationship = await userService.getClientCoachRelationship(MOCK_USER.id, MOCK_USER_3.id);
            } catch (e) {
                expect(e).to.be.an.instanceof(NotFoundErrorModel);
            }
            expect(relationship).to.be.undefined;
        });
    });
});