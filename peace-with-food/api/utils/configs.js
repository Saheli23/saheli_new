let config = require('config');

module.exports = {
    has: (key) => {
        return config.has(key);
    },
    get: (key) => {
        return config.get(key);
    }
}