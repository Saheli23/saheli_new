let secureRandom = require('secure-random')
  , bcrypt       = require('bcryptjs')
  , crypto       = require('crypto')
  , _            = require('underscore')
  ;


module.exports = {



  generateToken(){

        let    bytes = secureRandom.randomArray(127)
        let    seed  = _.reduce(bytes, function(sum, value){ 
                                         return sum + String.fromCharCode(value+1); 
                                       }, "");
        let    token       = crypto.createHash('sha256')
                                   .update( seed.toString() )
                                   .digest('base64');
               token       = token.replace( /\//g, '_' );
               token       = token.replace( /\+/g, '-' );
               token       = token.replace( /=/g,   '' );
        return token;
  },



  numericCode( code_length ){
  
        let sum = 0
        let Code = "";
        let bytes = secureRandom.randomArray(127)
        let i = 0;
        while ( Code.length < code_length ){
                let x = bytes[i++]
                Code = Code + (x % 10);
        }
        return Code;
  },

  hash( token ){
        return bcrypt.hashSync( token, 10);
  }

}
