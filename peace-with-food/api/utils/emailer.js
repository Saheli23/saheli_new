const fs            = require('fs')
    , Nodemailer    = require('nodemailer')
    , configEmail   = require('../config/email.json')
    , _             = require('underscore')
    ;

// var configEmail;
// (function(){
//     try {
//         var config = require('../config/email.json');
//         if ( !config )
//               console.error("Please create config/email.json");
//         //
//         // Populate email config
//         configEmail = config;
//         //
//     } catch (e) {
//         console.error(e);
//     }
// })();


let send = async function( template_name, to, data ){
    const to_email      = to;
          template_name = template_name.toLowerCase().replace('_', '');
    
    //
    //  read in template
    let bodyText  =  fs.readFileSync(`email/${template_name}.text`,    'utf8'),
        bodyHtml  =  fs.readFileSync(`email/${template_name}.html`,    'utf8'),
        subject   =  fs.readFileSync(`email/${template_name}.subject`, 'utf8');
        bodyText  =  bodyText.trim();
        bodyHtml  =  bodyHtml.trim();
        subject   =  subject .trim();

    //
    //  replace any {{variables}}
    _.each( data, (value, key) => {

        let regex = new RegExp('{{ *'+key+' *}}', 'g');
            value = (""+value).trim();
        bodyText = bodyText.replace( regex, value );
        bodyHtml = bodyHtml.replace( regex, value );
        subject  = subject .replace( regex, value );

    });
    
    
    //
    //  create reusable transporter object using the default SMTP transport
    let transporter = Nodemailer.createTransport({
                        service:            "Gmail",
                        connectionTimeout:  "7000", // https://stackoverflow.com/questions/33102313
                        greetingTimeout:    "7000",
                        host:               configEmail.host,
                        port:               configEmail.port,
                        secure:             true,
                        requireTLS:         true,
                        auth: {
                            user: configEmail.user, // generated ethereal user
                            pass: configEmail.pass // generated ethereal password
                        }
                      });
    //
    //  setup email data with unicode symbols
    let mailOptions = {
                        from:       '"' + configEmail.from + '" ' +
                                    '<' + configEmail.email + '>',
                        to:         to_email, // can be a list
                        subject:    subject,
                        text:       bodyText,
                        html:       bodyHtml
                      };
    
    
    var doSuccess;
    var doError;
    // connect the transporter to a promise
    var promise_mailer = new Promise(( resolve, reject ) => {
                            doSuccess = function(){
                                resolve.apply(this,arguments);
                            }
                            doError = function(){
                                reject.apply(this,arguments);
                            }
                         });
    
    try {
      // send mail with defined transport object
      transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                doError();
                console.error(error);
                return;
            }
            //console.log('Message sent: %s', info.messageId);
            //
            doSuccess();
            //
      });
    } catch(e) {
        doError(e);
    }
    
    // Display entire message in console
    // console.log("===================");
    // console.log("TO: " + to_email);
    // console.log("SUBJECT: ", mailOptions.subject);
    // console.log(mailOptions.text);
    // console.log("===================");
    
    
    await promise_mailer;
    return true;
}



module.exports.send = send;

