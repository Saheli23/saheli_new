let crypto = require('crypto'),
    bcrypt = require('bcryptjs'),
    moment = require('moment'),
    fs     = require('fs'),
    configs = require('../utils/configs'),
    Nodemailer = require('nodemailer')
    SessionResponseModel = require('../models/response/session-response-model'),
    NotFoundErrorModel = require('../models/error/not-found-error-model'),
    InvalidRequestErrorModel = require('../models/error/invalid-request-error-model'),
    userDao = require('../persistence/user-dao'),
    userAuthenticationDao = require('../persistence/user-authentication-dao'),
    userSessionDao = require('../persistence/user-session-dao');

var configEmail;


(function(){
    try {
        var config = require('../config/email.json');
        if ( !config )
              console.error("Please create config/email.json");
        //
        // Populate email config
        configEmail = config;
        //
    } catch (e) {
        console.error(e);
    }
})();


const SESSION_EXPIRE_DAYS = 30;

function Service() {}

Service.prototype.createAuthentication = async function(email, password) {
    const user = await userDao.getUserByEmail(email);
    if (!user) {
        throw new NotFoundErrorModel('No user found matching email ' + email);
    }
    if (!password) {
        throw new InvalidRequestErrorModel('Password is required');
    }
    const passwordHash = bcrypt.hashSync(password, 10);
    await userAuthenticationDao.addAuthenticationHash(user.id, passwordHash);

    return this.createSessionByEmailPassword(email, password);
};

Service.prototype.updateAuthentication = async function(userId, password) {
    const user = await userDao.getUserById(userId);
    if (!user) {
        throw new NotFoundErrorModel('Cannot change password: could not look up user account');
    }
    if (!password) {
        throw new InvalidRequestErrorModel('Password is required');
    }
    const passwordHash = bcrypt.hashSync(password, 10);
    return userAuthenticationDao.setAuthenticationHash(user.id, passwordHash);
};

/**
 * Creates a new session by email and password
 *
 * @param {String} email Email address
 * @param {String} password Password
 * @returns {Promise<SessionResponseModel>} Newly created session
 *
 * @throws NotFoundErrorModel if no user is found matching the provided email address
 * @throws NotFoundErrorModel if no authentication information is found for the user
 * @throws NotFoundErrorModel if no matching email and password is found
 * @throws Error if an unhandled error occurs
 */
Service.prototype.createSessionByEmailPassword = async function(email, password) {
    function doInvalidCredentials(){
        throw new NotFoundErrorModel('Email and password do not match');
    }
    

    let user = await userDao.getUserByEmail(email);
    if (!user) {
        //throw new NotFoundErrorModel('No user found matching email ' + email);
        doInvalidCredentials();
    }

    let authentication = await userAuthenticationDao.getByUserId(user.id);

    if (!authentication) {
        throw new NotFoundErrorModel('Your account has not been registered. Please reset your password.');
    }

    if (!bcrypt.compareSync(password, authentication.hash)) {
        // here you can log in with forgot password key
        let  authenticationRecovery = await userAuthenticationDao.getRecoveryByUserId(user.id);
        if (!authenticationRecovery || !bcrypt.compareSync(password, authenticationRecovery.hash))
             doInvalidCredentials();//throw new NotFoundErrorModel('Invalid authentication credentials');
    }

    try {
        let token = crypto.createHash('sha256').update(Math.random().toString()).digest('hex'),
            data = await userSessionDao.createSession(user.id, token, moment().add(SESSION_EXPIRE_DAYS, 'days').toDate());
        return this.getSessionById(data.insertId);
    } catch (e) {
        console.log(e);
        throw new Error('Error creating session');
    }
};



Service.prototype.sendForgotPasswordEmail = async function(user_email, app_url) {
    console.log("Reset password attempt: " + user_email);
    let user = await userDao.getUserByEmail(user_email);
    if (!user) {
        throw new NotFoundErrorModel("Email not found: " + user_email + " \n" +
                                     "Have you created an account yet?");
    }
    
    var   token       = crypto.createHash('sha256').update( 
                          Math.random().toString()).digest('base64');
          token       = token.replace( /\//g, '_' );
          token       = token.replace( /\+/g, '-' );
          token       = token.replace( /=/g,   '' );
    const hash        = bcrypt.hashSync( token, 10);
    const promise_addhash =
        // record the key
        userAuthenticationDao.addRecoveryAuthenticationHash(user.id, hash);
    
    //TODO store the hash in the database with an expiration date

    // create reusable transporter object using the default SMTP transport
    let transporter = Nodemailer.createTransport({
            service: "Gmail",
            connectionTimeout : "7000", // https://stackoverflow.com/questions/33102313
            greetingTimeout : "7000",
            host: configEmail.host,
            port: configEmail.port,
            secure: true,
            requireTLS: true,
            auth: {
                user: configEmail.user, // generated ethereal user
                pass: configEmail.pass // generated ethereal password
            }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from:       '"' + configEmail.from + '" <' + configEmail.email + '>', // sender address
        to:         user_email, // list of receivers
        subject:    'Password Reset - Peace with Food App', // Subject line
        text:       fs.readFileSync('email/forgotpassword.text','utf8'),
        html:       fs.readFileSync('email/forgotpassword.html','utf8'), // html body
    };
    
    
    const name = user.full_name || (user.first_name + " " + (user.last_name||'')).trim();
    var   sendData   = "?resetpassword=1&key="+token+"&email="+user_email+"&";
    var   action_url = app_url;
    if ( !action_url.match(/\+/) )
          action_url = action_url + sendData;
    else  action_url = action_url.replace( /\+/, sendData );
    action_url = "http://"+configs.get("api_host")+"/r/" + encodeURIComponent(action_url);
    mailOptions.text = mailOptions.text.replace(/{{ *name *}}/g, name );
    mailOptions.html = mailOptions.html.replace(/{{ *name *}}/g, name );
    mailOptions.text = mailOptions.text.replace(/{{ *action_url *}}/g, action_url );
    mailOptions.html = mailOptions.html.replace(/{{ *action_url *}}/g, action_url );

    // send mail with defined transport object
    var doSuccess;
    var doError;
    var promise_mailer = new Promise(( resolve, reject ) => {
        doSuccess = function(){
            resolve.apply(this,arguments);
        }
        doError = function(){
            reject.apply(this,arguments);
        }
    });
    
    try {
      transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                doError();
                console.error(error);
                return;
            }
            //console.log('Message sent: %s', info.messageId);
            //
            doSuccess();
            //
      });
    } catch(e) {
        doError(e);
    }
    
    // Display entire message in console
    //console.log(mailOptions.text);
    
    
    await promise_mailer;
    await promise_addhash;
};





/**
 * Returns a session by the provided ID
 *
 * @param {Number} id Session ID
 * @returns {Promise.<SessionResponseModel>}
 *
 * @throws NotFoundErrorModel if no session was found with the provided ID
 */
Service.prototype.getSessionById = async function(id) {
    let session = await userSessionDao.getBySessionId(id);
    if (!session) {
        throw new NotFoundErrorModel('No session found matching ID ' + id);
    }
    return new SessionResponseModel(session);
};

/**
 * Returns a session by the provided token
 *
 * @param {String} token Session token
 * @returns {Promise.<SessionResponseModel>}
 *
 * @throws NotFoundErrorModel if no session was found with the provided token
 */
Service.prototype.getSessionByToken = async function(token) {
    let session = await userSessionDao.getBySessionToken(token);
    if (!session) {
        throw new NotFoundErrorModel('No session found matching token ' + token);
    }
    return new SessionResponseModel(session);
};

module.exports = new Service();