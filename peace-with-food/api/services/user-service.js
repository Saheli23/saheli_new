let UserRequestModel = require('../models/request/user-request-model'),
    ClientUserResponseModel = require('../models/response/client-user-response-model'),
    CoachUserResponseModel = require('../models/response/coach-user-response-model'),
    ClientCoachListResponseModel = require('../models/response/client-coach-list-response-model'),
    CoachClientListResponseModel = require('../models/response/coach-client-list-response-model'),
    UserListResponseModel = require('../models/response/user-list-response-model'),
    ClientCoachResponseModel = require('../models/response/client-coach-response-model'),
    DuplicateEntityErrorModel = require('../models/error/duplicate-entity-error-model'),
    NotFoundErrorModel = require('../models/error/not-found-error-model'),
    userTypeEnum = require('../models/enum/user-type-enum'),
    userDao = require('../persistence/user-dao'),
    clientCoachDao = require('../persistence/client-coach-dao'),
    _        = require('underscore'),
    configs  = require('../utils/configs'),
    security = require('../utils/security'),
    emailer  = require('../utils/emailer');


function Service() {}

/**
 * Creates a new user
 *
 * @param {UserRequestModel} model User request model
 * @returns {Promise<UserResponseModel>} Newly created user
 *
 * @throws TypeError if model is not an instance of UserRequestModel
 * @throws DuplicateEntityErrorModel if a user already exists with that email
 * @throws Error if an unhandled error occurs
 */
Service.prototype.createUser = async function(model) {
    if (!(model instanceof UserRequestModel)) {
        throw new TypeError('Model must be instance of UserRequestModel');
    }

    let existingUser = undefined;
    try {
        existingUser = await this.getUserByEmail(model.email);
    } catch (e) {}
    if (existingUser) {
        throw new DuplicateEntityErrorModel('User already exists with the email address ' + model.email);
    }

    try {
        const data = await userDao.createUser(model.email, model.first_name, model.last_name, model.role);

        //
        //  send welcome email
        //
        let k = null;
        try {
            const data  =  {...model};
            k = emailer.send( 'welcome', model.email, data );
        } catch(e) {
            console.error("Could not send email", e );
        }
        //await k <-- no need to wait for email to send


        return this.getUserById(data.insertId);
    } catch (e) {
        console.log(e);
        throw new Error('Error creating user');
    }
};

/**
 * Updates a user
 *
 * @param {Number} id User ID
 * @param {UserRequestModel} model User request model
 * @returns {Promise<CoachUserResponseModel|ClientUserResponseModel>} Newly updated user
 *
 * @throws TypeError if model is not an instance of UserRequestModel
 * @throws NotFoundErrorModel if no user was found with the provided ID
 * @throws DuplicateEntityErrorModel if a user already exists with that email
 * @throws Error if an unhandled error occurs
 */
Service.prototype.updateUserById = async function(id, model) {
    if (!(model instanceof UserRequestModel)) {
        throw new TypeError('Model must be instance of UserRequestModel');
    }

    const user = await this.getUserById(id);
    if (!user) {
        if ( id === void 0 || id === 'undefined' )
             throw new NotFoundErrorModel('Could not lookup your user account');
        else throw new NotFoundErrorModel('No users found that match the ID ' + id);
    }

    if (model.email && model.email !== user.email) {
        let existingUser = undefined;
        try {
            existingUser = await this.getUserByEmail(model.email);
        } catch (e) {}
        if (existingUser) {
            throw new DuplicateEntityErrorModel('User already exists with the email address ' + model.email);
        }
    }

    try {
        await userDao.updateUserById(id, model);
        return this.getUserById(id);
    } catch (e) {
        console.log(e);
        throw new Error('Error updating user');
    }
};

/**
 * Adds an extra email address to a user account. Can be used to log in, etc.
 *
 * @param {Number} id User ID
 * @returns {Promise<CoachUserResponseModel|ClientUserResponseModel>} Newly updated user
 *
 * @throws NotFoundErrorModel if no user was found with the provided ID
 * @throws Error if an unhandled error occurs
 */
Service.prototype.addEmail = async function(id, email) {

    const user = await this.getUserById(id);
    if (!user) {
        if ( id === void 0 || id === 'undefined' )
             throw new NotFoundErrorModel('Could not lookup your user account');
        else throw new NotFoundErrorModel('No users found that match the ID ' + id);
    }
    if (!email) {
        throw new NotFoundErrorModel('Please specify an email address');
    }

    let existingUser = undefined;
    try {
        existingUser = await this.getUserByEmail(email);
    } catch (e) {}


    if (existingUser && existingUser.id != id ) {
        throw new DuplicateEntityErrorModel('A different user already exists with the same email address ' + email);
    }


    try {
        if ( !existingUser )
              await userDao.addEmail(id, email);
        return this.getUserById(id);
    } catch (e) {
        console.log(e);
        throw new Error('Error updating user');
    }
};


/**
 * Returns a list of users
 *
 * @param {Number} [offset = 0] Number of offset results by
 * @param {Number} [limit = 20] Number to limit results by
 * @returns {Promise.<UserListResponseModel>}
 */
Service.prototype.getUsers = async function(offset = 0, limit = 20) {
    const data = await Promise.all(
        [
            userDao.getTotalUsers(),
            userDao.getUsers(offset, limit)
        ]
    );
    return new UserListResponseModel(data[0], data[1]);
};

/**
 * Returns a user by the provided email
 *
 * @param {String} email User email
 * @returns {Promise.<CoachUserResponseModel|ClientUserResponseModel>}
 *
 * @throws NotFoundErrorModel if no user was found with the provided email
 */
Service.prototype.getUserByEmail = async function(email, throwError) {
    const user = await userDao.getUserByEmail(email);
    if (!user) {
        if (throwError===false)
            return null;
        if ( id === void 0 || id === 'undefined' )
             throw new NotFoundErrorModel('Could not lookup the user account');
        else throw new NotFoundErrorModel('No users found that match the email ' + email);
    }
    return this.getUserById(user.id);
};

/**
 * Returns a coach by the provided email
 *
 * @param {String} email User email
 * @returns {Promise.<CoachUserResponseModel>}
 *
 * @throws NotFoundErrorModel if no coach was found with the provided email
 */
Service.prototype.getCoachUserByEmail = async function(email) {
    const user = await userDao.getUserByEmail(email);
    // no need to distinguish between user types
    //if (!user || user.role !== userTypeEnum.types.COACH.role) {
    //    throw new NotFoundErrorModel('No coaches found that match the email ' + email);
    //}
    if (!user) {
         throw new NotFoundErrorModel('No accounts found that match the email ' + email);
    }
    return this.getUserById(user.id);
};

/**
 * Returns a client by the provided email
 *
 * @param {String} email User email
 * @returns {Promise.<ClientUserResponseModel>}
 *
 * @throws NotFoundErrorModel if no client was found with the provided email
 */
Service.prototype.getClientUserByEmail = async function(email) {
    const user = await userDao.getUserByEmail(email);
    if (!user) {
         throw new NotFoundErrorModel('No accounts found that match the email ' + email);
    }
    // no need to distinguish between user types 
    //if (!user || user.role !== userTypeEnum.types.role.CLIENT.role) {
    //    throw new NotFoundErrorModel('No clients found that match the email ' + email);
    //}
    return this.getUserById(user.id);
};

/**
 * Returns a user by the provided ID
 *
 * @param {Number} id User ID
 * @returns {Promise.<CoachUserResponseModel|ClientUserResponseModel>}
 *
 * @throws NotFoundErrorModel if no user was found with the provided ID
 */
Service.prototype.getUserById = async function(id) {
    if ( !id || id < 1 ) {
        if ( id === void 0 || id === 'undefined' )
             throw new NotFoundErrorModel('Could not lookup the user account');
        else throw new NotFoundErrorModel('No users found that match the ID ' + id);
    }
    const user = await userDao.getUserById(id);
    if (!user) {
        throw new NotFoundErrorModel('No users found that match the ID ' + id);
    }
    return (user.role === userTypeEnum.types.COACH.role) ? new CoachUserResponseModel(user) : new ClientUserResponseModel(user);
};

/**
 * Returns the list of coaches for the provided client ID
 *
 * @param {Number} id Client ID
 * @param {Number} [offset = 0] Number of offset results by
 * @param {Number} [limit = 20] Number to limit results by
 * @returns {Promise.<ClientCoachListResponseModel>}
 */
Service.prototype.getCoachesByClientId = async function(id, offset = 0, limit = 20) {
    const data = await Promise.all(
        [
            clientCoachDao.getTotalCoachesByClientId(id),
            clientCoachDao.getCoachesByClientId(id, offset, limit)
        ]
    );

    const total = data[0],
        relationships = data[1];

    if (relationships.length === 0) {
        return new ClientCoachListResponseModel(0, []);
    }

    const coaches = await this.getCoachesByIds(relationships.map(relationship => relationship.coach_id));

    return new ClientCoachListResponseModel(total, relationships.map((relationship) => {
        return coaches.filter(coach => relationship.coach_id === coach.id)[0];
    }));
};

/**
 * Returns the list of clients for the provided coach ID
 *
 * @param {Number} id Coach ID
 * @param {Number} [offset = 0] Number of offset results by
 * @param {Number} [limit = 20] Number to limit results by
 * @returns {Promise.<CoachClientListResponseModel>}
 */
Service.prototype.getClientsByCoachId = async function(id, offset = 0, limit = 20) {
    const data = await Promise.all(
        [
            clientCoachDao.getTotalClientsByCoachId(id),
            clientCoachDao.getClientsByCoachId(id, offset, limit)
        ]
    );

    const total = data[0],
        relationships = data[1];

    if (relationships.length === 0) {
        return new CoachClientListResponseModel(0, []);
    }

    const clients = await this.getClientsByIds(relationships.map(relationship => relationship.client_id));

    return new CoachClientListResponseModel(total, relationships.map((relationship) => {
        return clients.filter(client => relationship.client_id)[0];
    }));
};

/**
 * Returns the list of coaches that match the provided IDs
 *
 * @param {Number[]} ids Coach IDs
 * @returns {Promise.<CoachUserResponseModel[]>}
 */
Service.prototype.getCoachesByIds = async function(ids = []) {
    if (ids.length === 0) {
        return [];
    }
    const coaches = await userDao.getUsersByIds(ids);
    return coaches                               // no need to distinguish between user types
        .filter(coach => coach.role === coach.role/*userTypeEnum.types.COACH.role*/)
        .map(coach => new CoachUserResponseModel(coach));
};

/**
 * Returns the list of clients matching the provided IDs
 *
 * @param {Number[]} ids Client IDs
 * @returns {Promise.<ClientUserResponseModel[]>}
 */
Service.prototype.getClientsByIds = async function(ids = []) {
    if (ids.length === 0) {
        return [];
    }
    const clients = await userDao.getUsersByIds(ids);
    return clients                        // no need to distinguish between user types
        .filter(client => client.role === client.role/*userTypeEnum.types.CLIENT.role*/)
        .map(client => new ClientUserResponseModel(client));
};

/**
 * Returns a client coach relationship
 * Creates a new client coach relationship
 *
 * @param {Number} clientId Client ID
 * @param {Number} coachId Coach ID (one of coach id or email is required)
 * @param {String} email Coach email (one of coach id or email is required)
 * @returns {Promise.<ClientCoachResponseModel>}
 *
 * @throws NotFoundErrorModel if client was found with the provided ID
 * @throws NotFoundErrorModel if coach was not found with the provided ID or email
 * @throws DuplicateEntityErrorModel if client coach relationship already exists
 *
 * @todo Unit test
 */
Service.prototype.addClientCoachRelationship = async function(clientId, coachId, coachEmail, is_coach, errorMessage) {

    let client  =  this.getUserById(clientId);
    let coach;
    let notFoundErrorMessage;
          
    if ( coachId && coachId > 0 )
    {
        try {
        coach = await this.getUserById(coachId);
        } catch (e){}
        notFoundErrorMessage = 'No accounts found that match the ID ' + coachId;

    } else {

        try {
        coach = await this.getCoachUserByEmail(coachEmail); // error is checked 
        } catch (e){}
        notFoundErrorMessage = 'No accounts found that match the email ' + coachEmail;

    }


    client = await client;
    if ( !coach ) {
          throw new NotFoundErrorModel(notFoundErrorMessage);
    }
   /* 
    if (!(coach instanceof CoachUserResponseModel)) {
            throw new NotFoundErrorModel(notFoundErrorMessage);
            }
    if (!(client instanceof ClientUserResponseModel)) {
            throw new NotFoundErrorModel(
            'An unknown error was encountered (ID226) ' + (clientId?'('+clientId+')':''));
            }
    */
    let existingRelationship = await clientCoachDao.getClientCoachRelationship(client.id, coach.id);
    if (existingRelationship) {
            throw new DuplicateEntityErrorModel(
            "You are already connected to this " + (is_coach? "client":"coach") +
            ". " + (errorMessage||"")
            );
            }
    
    
    try {
        // create relationship
        await clientCoachDao.addClientCoachRelationship(client.id, coach.id);
        //
        // retrieve same relationship
        var relationship =  await clientCoachDao.getClientCoachRelationship(client.id, coach.id);
        relationship.coach = coach;
        relationship.client = client;
        relationship.debug = '007';
        return relationship;
        
    } catch (e) {
        console.log(e);
        throw new Error('An unexpected error was encountered. Please try again later');
    }
};

Service.prototype.getName = function( user, error_ ){

        if ( !user )
               throw new NotFoundErrorModel(error_ || 'An unknown error was encountered');
        else if ( user.full_name )
               name = user.full_name;
        else
               name = (user.first_name||'') + " " + (user.last_name||'');

        return name.trim();
};


/**
 *
 * @email  the email address
 *
 */
Service.prototype.inviteCoach = async function(email,{clientid}){

        //
        //  set up context data
        //
        let code     =  security.numericCode(5);
        let token    =  security.generateToken();
        let token_hashed = security.hash(token);
        let api_host =  configs.get('api_host');
        let url      = `${api_host}/a/connect?token=${token}`;
        let user     =  await this.getUserById(clientid);
        let name     =  email.replace(/@.*/, '').trim();
        let friend_name = this.getName(user, 'Please log out and try again');
        //
        //  add client coach conditional
        //
        let previousConditional = await clientCoachDao.getConditional( clientid, email );
        let j;
        if ( previousConditional ){
             code = previousConditional.code;
        } else {
             j = clientCoachDao.addConditional( clientid, email, code, '' );
        }
        //
        //  send email to coach
        //
        const data = {clientid, email, code, token, url, name, friend_name};
        let k = emailer.send( 'invitecoach', email, data );
        //
        //  finish up
        //
        await j;
        return await k;

};



Service.prototype.getConditionalFromCode = async function(code, clientid){
    if ( typeof code !== 'string' )
         throw new Error('Error looking up code');
    let result  =  clientCoachDao.getConditionalByCode(code);
    return await result;
};
Service.prototype.getCoachFromCode = async function(code, clientid){
    if ( typeof code !== 'string' )
         throw new Error('Error looking up code');
    let conditional  =  await clientCoachDao.getConditionalByCode(code);
    let coach = false;
    if (conditional && conditional.email ){
        coach = this.getUserByEmail(conditional.email);
    }
    return coach;
};

/**
 * Removes a client coach relationship
 *
 * @param {Number} clientId Client ID
 * @param {Number} coachId Coach ID
 * @returns {Promise<ClientCoachResponseModel>}
 *
 * @throws NotFoundErrorModel if client was found with the provided ID
 * @throws NotFoundErrorModel if coach was found with the provided ID
 * @throws NotFoundErrorModel if no relationship was found between client and coach
 */
Service.prototype.getClientCoachRelationship = async function(clientId, coachId) {
    const users = await Promise.all(
        [
            this.getUserById(clientId),
            this.getUserById(coachId)
        ]
    );

    const client = users[0], coach = users[1];

    //if (!(client instanceof ClientUserResponseModel)) {
    //    throw new NotFoundErrorModel('No clients found that match the ID ' + clientId);
    //}
    //
    //if (!(coach instanceof CoachUserResponseModel)) {
    //    throw new NotFoundErrorModel('No coaches found that match the ID ' + coachId);
    //}

    const data = await clientCoachDao.getClientCoachRelationship(clientId, coachId);
    if (!data) {
        throw new NotFoundErrorModel('Coach ' + coachId + ' not found in client ' + clientId + ' list of coaches');
    }
    return new ClientCoachResponseModel(data);
};

/**
 * Removes a client coach relationship
 *
 * @param {Number} clientId Client ID
 * @param {Number} coachId Coach ID
 * @returns {Promise.<void>}
 *
 * @throws NotFoundErrorModel if client was found with the provided ID
 * @throws NotFoundErrorModel if coach was found with the provided ID
 * @throws NotFoundErrorModel if no relationship was found between client and coach
 *
 * @todo Unit test
 */
Service.prototype.deleteClientCoachRelationship = async function(clientId, coachId) {
    const relationship = await this.getClientCoachRelationship(clientId, coachId);
    await clientCoachDao.deleteRelationshipById(relationship.id);
};

module.exports = new Service();
