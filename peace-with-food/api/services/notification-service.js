let DeviceTokenResponseModel = require('../models/response/device-token-response-model'),
    DeviceTokenRequestModel = require('../models/request/device-token-request-model'),
    NotFoundErrorModel = require('../models/error/not-found-error-model'),
    InvalidRequestErrorModel = require('../models/error/invalid-request-error-model'),
    userDao = require('../persistence/user-dao'),
    osEnum = require('../models/enum/os-enum'),
    deviceTokenDao = require('../persistence/device-token-dao');

function Service() {}

Service.prototype.addDeviceToken = async function(model) {
    if (!(model instanceof DeviceTokenRequestModel)) {
        throw new TypeError('Model must be instance of DeviceTokenRequestModel');
    }
    if (!model.os) {
        throw new InvalidRequestErrorModel('OS is required to add a device token');
    }
    if (!model.user_id) {
        throw new InvalidRequestErrorModel('user ID is required to add a device token');
    }
    if (!model.token) {
        throw new InvalidRequestErrorModel('Token is required to add a device token');
    }
    if (!(osEnum.getValueByKey(model.os))) {
        throw new InvalidRequestErrorModel('Invalid OS type specified');
    }
    const user = await userDao.getUserById(model.user_id);
    if (!user) {
        throw new NotFoundErrorModel('No user found matching that ID');
    }

    try {
        const os = osEnum.getValueByKey(model.os).value;
        await deviceTokenDao.addDeviceToken(model.user_id, os, model.token);
        return this.getDeviceTokenByUserIdAndOs(model.user_id, os);
    } catch (e) {
        console.log(e);
        throw new Error('Error adding device token');
    }
};

Service.prototype.getDeviceTokenById = async function(id) {
    const data = await deviceTokenDao.getDeviceTokenById(id);
    if (!data) {
        throw new NotFoundErrorModel('No device token found matching that ID');
    }
    return new DeviceTokenResponseModel(data);
};

Service.prototype.getDeviceTokenByUserIdAndOs = async function(userId, os) {
    const data = await deviceTokenDao.getDeviceTokensByUserId(userId);
    const token = data.filter(token => token.os === os)[0];
    if (!token) {
        throw new NotFoundErrorModel('No device token found matching that user ID and OS');
    }
    return new DeviceTokenResponseModel(token);
};

module.exports = new Service();