let moment = require('moment'),
    _      = require('underscore'),
    TrackEntryRequestModel = require('../models/request/track-entry-request-model'),
    TrackEntryResponseModel = require('../models/response/track-entry-response-model'),
    TrackEntryListResponseModel = require('../models/response/track-entry-list-response-model'),
    TrackEntryAverageListResponseModel = require('../models/response/track-entry-average-list-response-model'),
    OptionRhythmListResponseModel = require('../models/response/option-rhythm-list-response-model'),
    NotFoundErrorModel = require('../models/error/not-found-error-model'),
    InvalidRequestErrorModel = require('../models/error/invalid-request-error-model'),
    ForbiddenErrorModel = require('../models/error/forbidden-error-model'),
    userTypeEnum = require('../models/enum/user-type-enum'),
    userDao = require('../persistence/user-dao'),
    trackDao = require('../persistence/track-dao'),
    optionsDao = require('../persistence/options-dao'),
    sharedRhythmsDao = require('../persistence/shared-rhythms-dao');

function Service() {}

/**
 * Creates a new trackable entry
 *
 * @param {TrackEntryRequestModel} model Track request model
 * @returns {Promise<TrackEntryResponseModel>} Newly created trackable entry
 *
 * @throws TypeError if model is not an instance of TrackRequestModel
 * @throws InvalidRequestErrorModel if information was missing from the model
 * @throws NotFoundErrorModel if no user was found matching the provided user ID
 * @throws Error if an unhandled error occurs
 */
Service.prototype.createEntry = async function(model) {
    if (!(model instanceof TrackEntryRequestModel)) {
        throw new TypeError('Model must be instance of TrackEntryRequestModel');
    }
    if (!model.user_id) {
        throw new InvalidRequestErrorModel('User ID is required to track an entry');
    }
    if (!model.value) {
        throw new InvalidRequestErrorModel('Value is required to track an entry');
    }

    let user = await userDao.getUserById(model.user_id);
    // no need to distinguish between user types
    //if (!user || user.role !== userTypeEnum.types.CLIENT.role) {
    //    throw new NotFoundErrorModel('No client found matching that ID');
    //}

    try {
        const data = await trackDao.createEntry(model.user_id, model.value);
        return this.getEntryById(data.insertId);
    } catch (e) {
        console.log(e);
        throw new Error('Error creating entry');
    }
};

/**
 * Creates a new trackable entry
 *
 * @param {TrackEntryDetailRequestModel} model Track request model
 * @returns {Promise<TrackEntryDetailRequestModel>} Newly created trackable entry
 *
 * @throws TypeError if model is not an instance of TrackRequestModel
 * @throws InvalidRequestErrorModel if information was missing from the model
 * @throws NotFoundErrorModel if no user was found matching the provided user ID
 * @throws Error if an unhandled error occurs
 */
Service.prototype.createDetailEntry = async function(model) {
    // if (!(model instanceof TrackEntryDetailRequestModel)) {
    //     throw new TypeError('Model must be instance of TrackEntryDetailRequestModel');
    // }
    if (!model.user_id) {
        throw new InvalidRequestErrorModel('User ID is required to update rhythm details');
    }
    if (!model.rhythm_id && !model.rhythm_entry_id) {
        throw new InvalidRequestErrorModel('Rhythm Entry ID is required to update rhythm details: ' + JSON.stringify(model));
    }
    if (!model.thoughts && model.thoughts !== '') {
        throw new InvalidRequestErrorModel("'Thoughts' value is required to update rhythm details or use an empty string");
    }
    if (!model.options) {
        throw new InvalidRequestErrorModel("'Options' data are required to update rhythm details or use an empty object.");
    }

    let user = await userDao.getUserById(model.user_id);
    // fyi - no need to distinguish between user types

    try {
        const r = await trackDao.createEntryDetails(
                             model.user_id,
                             model.rhythm_id || model.rhythm_entry_id,
                             model.thoughts,
			     model.message_id || null,
                             model.options
                        );
        return this.getEntryById(  model.rhythm_id || model.rhythm_entry_id
                                ,  true );
    } catch (e) {
        console.log(e);
        throw new Error('Error creating entry');
    }
};

/**
 * Updates a trackable entry
 *
 * @param {Number} id Entry ID
 * @param {TrackEntryRequestModel} model Track request model
 * @returns {Promise<TrackEntryResponseModel>} Newly updated entry
 *
 * @throws TypeError if model is not an instance of TrackRequestModel
 * @throws NotFoundErrorModel if no entry was found with the provided ID
 * @throws Error if an unhandled error occurs
 */
Service.prototype.updateEntryById = async function(id, model) {
    if (!(model instanceof TrackEntryRequestModel)) {
        throw new TypeError('Model must be instance of TrackEntryRequestModel');
    }

    let entry = await this.getEntryById(id);
    if (!entry) {
        throw new NotFoundErrorModel('No entry found matching that ID (' + id + ')');
    }

    try {
        await trackDao.updateEntryById(id, model);
        return this.getEntryById(id, true);
    } catch (e) {
        console.log(e);
        throw new Error('Error updating entry');
    }
};

/**
 * Returns the list of possible rhythms
 *
 * @returns {Promise.<OptionRhythmListResponseModel>}
 */
Service.prototype.getRhythms = async function() {
    let rhythms = await optionsDao.getRhythms();
    return new OptionRhythmListResponseModel(rhythms.length, rhythms);
};

/**
 * Returns a trackable entry by the provided ID
 *
 * @param {Number} id Entry ID
 * @param {boolean} get_details
 * @returns {Promise.<TrackEntryResponseModel>}
 *
 * @throws NotFoundErrorModel if no entry was found with the provided ID
 */
Service.prototype.getEntryById = async function(id, get_details) {
    let entry = await trackDao.getEntryById(id);
    var details = null;

    if (!entry) {
        throw new NotFoundErrorModel('No entry found matching that ID (' + id + ')');
    }
    if (get_details) {
        details = await trackDao.getEntryDetailsById(id);

        entry = _.extend( details, entry ); // details on bottom:
                                            // :: id needs to be entry.id
    }
    return new TrackEntryResponseModel(entry);
};


function mergeRhythm( to, from ){
    if ( !to.options  ||  to.options.length === 0 )
          to.options  = from.options;
    if ( !to.thoughts ||  to.thoughts.length === 0 )
          to.thoughts = from.thoughts;
}

function cleanRhythms( data )
{
    var last_timestamp = 0;
    var cleanedData = [];
    //
    //  Merge rhythms that are close in time.
    //  Here we assume that the user accidentally did it twice.
    //
    _.each( data, function(entry){
        entry._debug = Math.abs(last_timestamp - entry.time);
        if ( Math.abs(last_timestamp - entry.time) <= 15 )
        {
            mergeRhythm(last_entry, entry);
        } else
            cleanedData.push(entry);
        last_entry     = entry;
        last_timestamp = entry.time;
    });
    //
    //  Add unix timestamps in order to fix the timezone
    //
    data = cleanedData;
    cleanedData = [];
    _.each( data, function(entry){
        entry.created_unix  = moment(entry.created_date ) .unix();
        entry.modified_unix = moment(entry.modified_date) .unix();
        cleanedData.push(entry);
    });
    //
    //
    return cleanedData;
}


/**
 * Returns a list of trackable entries for the provided user ID
 *
 * @param {Number} userId User ID
 * @param {Number} [offset=0] Number to offset results
 * @param {Number} [limit=20] Number to limit results
 * @returns {Promise.<TrackEntryListResponseModel>}
 */
Service.prototype.getEntriesByUserId = async function(userId, offset = 0, limit = 20, timezoneOffset) {
    let data = await Promise.all(
        [
            trackDao.getTotalEntriesByUserId(userId),
            trackDao.getEntriesByUserId(userId, offset, limit)
        ]
    );
    var count = data[1].length;
    data[1] = cleanRhythms( data[1] );
    data[0] -= (count - data[1].length);
    return new TrackEntryListResponseModel(data[0], data[1]);
};

/**
 * Returns a list of trackable entries for the provided user ID between 2 dates
 *
 * @param {Number} userId User ID
 * @param {String} startDate Start date in YYYY-MM-DD format
 * @param {String} endDate End date in YYYY-MM-DD format
 * @returns {Promise.<TrackEntryListResponseModel>}
 *
 * @throws InvalidRequestErrorModel if start or end date are invalid
 */
Service.prototype.getEntriesByUserIdBetweenDates = async function(userId, startDate, endDate, timezoneOffset) {
    var start =  moment(startDate + ' ' + timezoneOffset, 'YYYY-MM-DD ZZ', true),
        end   =  moment(endDate   + ' ' + timezoneOffset, 'YYYY-MM-DD ZZ', true);
    end = end.add( 1, 'days' );

    if (!start.isValid()) {
        throw new InvalidRequestErrorModel('Invalid start date provided.');
    }
    if (!end.isValid()) {
        throw new InvalidRequestErrorModel('Invalid end date provided.');
    }

    var data = await trackDao.getEntriesByUserIdBetweenDates(userId, start.toDate(), end.toDate());
    data = cleanRhythms( data );
    return new TrackEntryListResponseModel(data.length, data);
};

Service.prototype.getAverageEntriesByUserIdBetweenDates = async function(userId, startDate, endDate) {
    const start = moment(startDate, 'YYYY-MM', true),
        end = moment(endDate, 'YYYY-MM', true);

    if (!start.isValid()) {
        throw new InvalidRequestErrorModel('Invalid start date provided.');
    }
    if (!end.isValid()) {
        throw new InvalidRequestErrorModel('Invalid end date provided.');
    }

    const data = await trackDao.getEntriesByUserIdBetweenDates(userId, start.startOf('month').toDate(), end.endOf('month').toDate());
    rhythmData = cleanRhythms( data );
    let monthEntries = {};
    rhythmData.forEach((entry) => {
        const key = moment(entry.created_date).format('YYYY-MM');
        if (!monthEntries[key]) {
            monthEntries[key] = [];
        }
        monthEntries[key].push(entry);
    });
    return {
        entries: Object.values(monthEntries).map((value) => {
            let averages = {},
                totalEntries = 0;
            value.forEach((entry) => {
                let group = Math.floor((entry.value - 1) / 3); // Groups of 3 ([1 - 3, 4 - 6, 7 - 9])
                if (!averages[group]) {
                    averages[group] = [];
                }
                averages[group].push(entry);
                totalEntries++;
            });
            return {
                client_id: userId,
                total_entries: value.length,
                values: Object.values(averages).map((average) => {
                    return Math.round((average.length / totalEntries) * 100);
                }),
                start_date: moment(value[0].created_date).startOf('month').startOf('day').toDate(),
                end_date: moment(value[0].created_date).endOf('month').endOf('day').subtract(1, 'day').toDate()
            }
        })
    };
};

/**
 * Shares a new track entry with the provided coach
 *
 * @param {Number} clientId Owner ID of the entry
 * @param {Number} entryId Entry ID to share
 * @param {Number} coachId Coach ID to share with
 * @returns {Promise.<TrackEntryResponseModel>}
 *
 * @throws ForbiddenErrorModel if the client does not own the entry
 * @throws NotFoundErrorModel if the coach was not found
 * @throws Error if an unhandled error occurs
 */
Service.prototype.addSharedEntry = async function(clientId, entryId, coachId) {
    const entry = await this.getEntryById(entryId);

    if (entry.client_id !== clientId) {
        throw new ForbiddenErrorModel('Invalid permissions to share trackable entry');
    }
    const coach = await userDao.getUserById(coachId);
    // no need to distinguish between user types
    //if (!coach || coach.role !== userTypeEnum.types.COACH.role) {
    //    throw new NotFoundErrorModel('No coach found matching that ID');
    //}
    try {
        await sharedRhythmsDao.addSharedEntry(entryId, coachId);
        return this.getEntryById(entryId, true);
    } catch (e) {
        console.log(e);
        throw new Error('Error sharing entry');
    }
};

Service.prototype.addSharedEntriesByDate = async function(userId, coachId, title, beginEpoch, endEpoch) {
    const coach = await userDao.getUserById(coachId);
    try {
        let data = await sharedRhythmsDao.addSharedEntriesByDate(userId, coachId, title, beginEpoch, endEpoch);
        return sharedRhythmsDao.getSharedInstanceAdjusted(data.id);
    } catch (e) {

        if ( e.message && e.message.indexOf('Duplicate entry') >= 0 ){
             //
             //  Prevent duplicate entries from causing errors
             //
             return false;
        }

        console.log(e);
        throw new Error('Error sharing entry');
    }
};

Service.prototype.deleteSharedEntry = async function(clientId, entryId, coachId) {
    const entry = await this.getEntryById(entryId);
    if (entry.client_id !== clientId) {
        throw new ForbiddenErrorModel('Invalid permissions to remove shared trackable entry');
    }
    await sharedRhythmsDao.deleteSharedEntry(entryId, coachId);
};

Service.prototype.getSharedEntryInstances = async function(coachId, limit/*optional*/) {

    limit = limit || 50;
    if ( !coachId || !(+coachId > 0)) {
        throw new NotFoundErrorModel('Invalid coachId: ' + coachId);
    }
    //
    //  Retrieve rows
    const instances = await sharedRhythmsDao.getSharedInstancesAdjusted(coachId, limit, 0);
    //
    //  Add coach data to each row
    var clients = {};
    for ( var i in instances ){
          const client_id = instances[i].client_id;
          var   client    = clients[client_id];
          if ( !client ){
                clients[client_id] = client = await userDao.getUserById( client_id );
          }
          instances[i].client = client;
    }

    return instances;

};

Service.prototype.getMySharedEntryInstances = async function(clientId, limit/*optional*/) {

    limit = limit || 50;
    if ( !clientId || !(+clientId > 0)) {
        throw new NotFoundErrorModel('Invalid clientId: ' + coachId);
    }
    //
    //  Retrieve rows
    const instances = await sharedRhythmsDao.getMySharedInstancesAdjusted(clientId, limit, 0);
    //
    //  Add coach data to each row
    var coaches = {};
    for ( var i in instances ){
          const coach_id = instances[i].coach_id;
          var   coach    = coaches[coach_id];
          if ( !coach ){
                coaches[coach_id] = coach = await userDao.getUserById( coach_id );
          }
          instances[i].coach = coach;
    }

    return instances;

};

module.exports = new Service();
