module.exports = function(grunt) {
    grunt.initConfig({
        env: {
            development: {
                NODE_ENV: 'dev',
                NODE_CONFIG_DIR : '/etc/peacewithfood/api/',
                PORT: 3000
            },
            test: {
                NODE_ENV: 'dev',
                NODE_CONFIG_DIR : '/etc/peacewithfood/api/',
                PORT: 3000
            }
        },
        mochaTest: {
            unit: {
                options: {
                    reporter: 'spec'
                },
                src: [
                    'tests/unit/**/*.js'
                ]
            }
        },
        express: {
            api: {
                options: {
                    script: 'index.js'
                }
            }
        },
        apidoc: {
            api: {
                src: [
                    'controllers/',
                    'models/'
                ],
                dest: 'docs/'
            }
        }
    });

    grunt.loadNpmTasks('grunt-env');
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-apidoc');

    grunt.registerTask('doc', ['apidoc']);
    grunt.registerTask('test', ['env:test', 'mochaTest:unit']);
    grunt.registerTask('default', ['env:development', 'express']);
};