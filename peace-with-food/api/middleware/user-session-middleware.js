let userService = require('../services/user-service'),
    authenticationService = require('../services/authentication-service');

module.exports = async function(request, response, next) {
    let token = request.headers['authorization'];
    if (token) {
        let tokenParts = token.split(' ');
        token = tokenParts[tokenParts.length - 1];
    }
    if (token) {
        try {
            let session = await authenticationService.getSessionByToken(token);
            if (session) {
                let user = await userService.getUserById(session.user_id);
                if (user) {
                    request.sessionUser = user;
                }
            }
        } catch (e) {
            console.log('Error setting user session', e);
        }
    }
    next();
};