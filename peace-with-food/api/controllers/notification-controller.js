const util = require('util'),
    BaseController = require('./base-controller'),
    DuplicateEntityErrorModel = require('../models/error/duplicate-entity-error-model'),
    NotFoundErrorModel = require('../models/error/not-found-error-model'),
    InvalidRequestErrorModel = require('../models/error/invalid-request-error-model'),
    ForbiddenErrorModel = require('../models/error/forbidden-error-model'),
    DeviceTokenRequestModel = require('../models/request/device-token-request-model'),
    notificationService = require('../services/notification-service');

function Controller(app) {
    if (!(this instanceof Controller)) {
        return new Controller(app);
    }
    BaseController.call(this, app, {
        path: '/notifications'
    });
}

util.inherits(Controller, BaseController);

Controller.prototype.registerAllMethods = function() {

    /**
     * @api {post} /notifications/device/token Create Device Token
     * @apiName PostNotificationsDeviceToken
     * @apiGroup Notifications
     * @apiVersion 1.0.0
     *
     * @apiDescription Adds a new device token for the provided user ID and OS
     *
     * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with session token
     *
     * @apiParam {Number} user_id User ID
     * @apiParam {String="ios","android"} os Device OS
     * @apiParam {String} token Device token
     *
     * @apiExample {js} Example usage:
     *                  {
     *                      user_id: 1,
     *                      os: "ios",
     *                      token: "abcd1234"
     *                  }
     *
     * @apiError {String} error Error message if the device token wasn't added
     */
    this.registerPostMethod('/device/token', this.addDeviceToken);
};

Controller.prototype._handleThrownError = function(e, response) {
    if ((e instanceof NotFoundErrorModel)) {
        this.sendNotFoundError(response, {
            error: e.message
        });
        return true;
    }
    if ((e instanceof DuplicateEntityErrorModel)) {
        this.sendDuplicateError(response, {
            error: e.message
        });
        return true;
    }
    if ((e instanceof InvalidRequestErrorModel)) {
        this.sendDuplicateError(response, {
            error: e.message
        });
        return true;
    }
    if ((e instanceof ForbiddenErrorModel)) {
        this.sendForbiddenError(response, {
            error: e.message
        });
        return true;
    }
    return false;
};

Controller.prototype.addDeviceToken = async function(request, response) {
    const model = new DeviceTokenRequestModel(request.body);

    if (!this.isAuthenticatedUser(request, model.user_id, true)) {
        this.sendForbiddenError(response, {
            error: 'Invalid permissions to add device token'
        });
        return new Promise(() => {});
    }

    try {
        await notificationService.addDeviceToken(model);
        this.sendSuccessNoContent(response);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error adding device token'
            });
        }
    }
};


module.exports = Controller;