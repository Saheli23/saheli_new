const util = require('util'),
    fs     = require('fs'),
    BaseController = require('./base-controller'),
    userService = require('../services/user-service'),
    authenticationService = require('../services/authentication-service'),
    DuplicateEntityErrorModel = require('../models/error/duplicate-entity-error-model'),
    NotFoundErrorModel = require('../models/error/not-found-error-model'),
    InvalidRequestErrorModel = require('../models/error/invalid-request-error-model'),
    CoachRequestModel = require('../models/request/coach-user-request-model'),
    ClientRequestModel = require('../models/request/client-user-request-model'),
    AuthenticationRequestModel = require('../models/request/authentication-request-model'),
    UserRequestModel = require('../models/request/user-request-model'),
    ClientCoachRequestModel = require('../models/request/client-coach-request-model'),
    UserSessionResponseModel = require('../models/response/user-session-response-model');





function Controller(app) {
    if (!(this instanceof Controller)) {
        return new Controller(app);
    }
    BaseController.call(this, app, {
        path: '/'
    });
}

util.inherits(Controller, BaseController);

Controller.prototype.registerAllMethods = function() {

    /**
     * @api {get} / 
     * @apiName Print html
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Prints root.html
     *
     */
    this.registerGetMethod('/', this.printRoot);

    this.registerGetMethod('r/:url', this.printRedirect);

};


Controller.prototype.printRoot = async function(request, response) {

    let html = fs.readFileSync('html/root.html', 'utf8');
    this.sendHtml(response, html);

};



Controller.prototype.printRedirect = async function(request, response) {
    const model = request.params;

    let html = '';
    html += "<body>\n";
    html += "<div style='display:none;'>\n";
    html += "    Please open this link from your mobile device.</div>\n";
    html += "<script>\n";
    html += "  document.location.href = decodeURIComponent('"+model.url+"');\n";
    html += "  setTimeout(function(){\n";
    html += "    document.body.children[0].style.display = 'block';\n";
    html += "  }, 1500 );\n";
    html += "</script>\n";
    html += "</body>\n";
    this.sendHtml(response, html);

}


module.exports = Controller;
