let configs = require('../utils/configs');

function BaseController(app, options) {
    this.app = app;
    this.options = (options instanceof Object) ? options : {};
}

function _sendResponse(code, data, response, type) {
    if (type == 'html'){
        response.setHeader('Content-Type', 'text/html; charset=utf-8');
        response.status(code).send(data);
        return;
    }
    response.setHeader('Content-Type', 'application/json');
    if (data) {
        data = JSON.stringify(data);
    }
    response.status(code).send(data);
}

BaseController.prototype._registerMethod = function(method, endpoint, callback) {
    let _this = this;
    this.app[method](this.options.path + endpoint, function(request, response) {
        callback.call(_this, request, response);
    });
};

BaseController.prototype.registerGetMethod = function(endpoint, callback, options) {
    this._registerMethod('get', endpoint, callback, options);
};

BaseController.prototype.registerPostMethod = function(endpoint, callback, options) {
    this._registerMethod('post', endpoint, callback, options);
};

BaseController.prototype.registerPutMethod = function(endpoint, callback, options) {
    this._registerMethod('put', endpoint, callback, options);
};

BaseController.prototype.registerDeleteMethod = function(endpoint, callback, options) {
    this._registerMethod('delete', endpoint, callback, options);
};

BaseController.prototype.sendSuccess = function(response, data) {
    _sendResponse(200, data || {}, response);
};

BaseController.prototype.sendHtml = function(response, html) {
    _sendResponse(200, html || '', response, 'html');
};

BaseController.prototype.sendCreatedSuccess = function(response, data) {
    _sendResponse(201, data || { created: true }, response);
};

BaseController.prototype.sendSuccessNoContent = function(response) {
    _sendResponse(204, undefined, response);
};

BaseController.prototype.sendServerError = function(response, data) {
    _sendResponse(500, data || {}, response);
};

BaseController.prototype.sendNotFoundError = function(response, data) {
    _sendResponse(404, data || {}, response);
};

BaseController.prototype.sendBadRequestError = function(response, data) {
    _sendResponse(400, data || {}, response);
};

BaseController.prototype.sendForbiddenError = function(response, data) {
    _sendResponse(403, data || {}, response);
};

BaseController.prototype.sendDuplicateError = function(response, data) {
    _sendResponse(409, data || {}, response);
};

BaseController.prototype.registerAllMethods = function() {};

BaseController.prototype.init = function() {
    if (typeof this.options.path !== 'string') {
        throw new Error('No path defined to start service!');
    }
    this.registerAllMethods();
};

BaseController.prototype.isAuthenticatedUser = function(request, userId, throwError) {

    if (throwError){
        throwError = () => {
            this.sendForbiddenError(response, {
                error: 'You are not logged in. Please try logging in again.'
            });
        }
    } else {
        throwError = function(){};
    }

    // Development environment is always authenticated
    if (configs.has('environment') && configs.get('environment') === 'dev') {
        return true;
    }

    if (!request || !request.sessionUser || !userId) {
        throwError();
        return false;
    }
    return (request.sessionUser.id + '') === (userId + '');
};

BaseController.prototype.getSessionUser = function(request) {
    return request.sessionUser;
};

module.exports = BaseController;