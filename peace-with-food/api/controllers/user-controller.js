const util = require('util'),
    BaseController = require('./base-controller'),
    userService = require('../services/user-service'),
    authenticationService = require('../services/authentication-service'),
    DuplicateEntityErrorModel = require('../models/error/duplicate-entity-error-model'),
    NotFoundErrorModel = require('../models/error/not-found-error-model'),
    InvalidRequestErrorModel = require('../models/error/invalid-request-error-model'),
    CoachRequestModel = require('../models/request/coach-user-request-model'),
    ClientRequestModel = require('../models/request/client-user-request-model'),
    AuthenticationRequestModel = require('../models/request/authentication-request-model'),
    UserRequestModel = require('../models/request/user-request-model'),
    ClientCoachRequestModel = require('../models/request/client-coach-request-model'),
    UserSessionResponseModel = require('../models/response/user-session-response-model');

function Controller(app) {
    if (!(this instanceof Controller)) {
        return new Controller(app);
    }
    BaseController.call(this, app, {
        path: '/users'
    });
}

util.inherits(Controller, BaseController);

Controller.prototype.registerAllMethods = function() {

    /**
     * @api {post} /users/coach Create Coach
     * @apiName PostUsersCoach
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Creates a new coach user
     *
     * @apiParam {String} email Email address for the user
     * @apiParam {String} password Plain text password for the user
     *
     * @apiExample {js} Example usage:
     *                  {
     *                      email: "john.smith@example.com",
     *                      password: "peacewithfood001"
     *                  }
     *
     * @apiUse CoachUserResponseModel
     *
     * @apiError {String} error Error message if the user wasn't added
     */
    this.registerPostMethod('/coach', this.createCoach);

    /**
     * @api {post} /users/client Create Client
     * @apiName PostUsersClient
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Creates a new client user
     *
     * @apiParam {String} email Email address for the user
     * @apiParam {String} password Plain text password for the user
     *
     * @apiExample {js} Example usage:
     *                  {
     *                      email: "john.smith@example.com",
     *                      password: "peacewithfood001"
     *                  }
     *
     * @apiUse ClientUserResponseModel
     *
     * @apiError {String} error Error message if the user wasn't added
     */
    this.registerPostMethod('/client', this.createClient);

    /**
     * @api {post} /users/client/coach Create Client Coach Relationship
     * @apiName PostUsersClientCoach
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Creates a new client coach relationship
     *
     * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with session token
     *
     * @apiParam {Number} client_id Client ID
     * @apiParam {Number} coach_id Coach ID
     * @apiParam {String} coachemail Coach email
     *
     * @apiExample {js} Example usage:
     *                  {
     *                      client_id: 1,
     *                      coach_id: 2
     *                  }
     *
     * @apiUse ClientCoachResponseModel
     *
     * @apiError {String} error Error message if the relationship wasn't created
     */
    this.registerPostMethod('/client/coach', this.createClientCoachRelationship);

    /**
     * @api {post} /users/session Create Session
     * @apiName PostUsersSession
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Create a new session for the user
     *
     * @apiParam {String} email Email address for the user
     * @apiParam {String} password Plain text password for the user
     *
     * @apiExample {js} Example usage:
     *                  {
     *                      email: "john.smith@example.com",
     *                      password: "peacewithfood001"
     *                  }
     *
     * @apiUse UserSessionResponseModel
     *
     * @apiError {String} error Error message if the session wasn't created
     */
    this.registerPostMethod('/session', this.createSession);

    /**
     * @api {post} /users/forgotpassword Request Forgot Password Email
     * @apiName PostUsersSession
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Requests the system to send a Reset Password email
     *
     * @apiParam {String} email Email address for the user
     *
     * @apiExample {js} Example usage:
     *                  {
     *                      email: "john.smith@example.com",
     *                  }
     *
     * @apiUse 
     *
     * @apiError {String} error Error message if the email wasn't found
     */
    this.registerPostMethod('/forgotpassword', this.setupForgotPassword );

    /**
     * @api {post} /users/changepassword Change user password
     * @apiName PostUsersSession
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Changes password for user, used for 'forgot password'
     *
     * @apiParam {String} email Email address for the user
     * @apiParam {String} oldpassword Old password or security key works too
     * @apiParam {String} password New password
     *
     * @apiExample {js} Example usage:
     *                  {
     *                      oldpassword: // optional if not logged in
     *                      password:
     *                      email: "john.smith@example.com",
     *                  }
     *
     * @apiUse 
     *
     * @apiError {String} error Error message if security issue
     */
    this.registerPostMethod('/changepassword', this.changePassword );

    /**
     * @api {get} /users Get Users
     * @apiName GetUsers
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Returns all users
     *
     * @apiParam {Number} [offset = 0] Number to offset results by
     * @apiParam {Number} [limit = 20] Number to limit results by
     *
     * @apiExample {curl} Example Usage
     *                  curl -i https://{BASE_URL}/users?offset=0&limit=20
     *
     * @apiUse UserListResponseModel
     */
    this.registerGetMethod('/', this.getUsers);

    /**
     * @api {get} /users/coach/email/:email Get Coach By Email
     * @apiName GetUsersCoachByEmail
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Returns coach matching the specified email
     *
     * @apiParam {String} email Coach email
     *
     * @apiExample {curl} Example Usage
     *                  curl -i https://{BASE_URL}/users/coach/email/coach@peacewithfood.com
     *
     * @apiUse CoachUserResponseModel
     */
    this.registerGetMethod('/coach/email/:email', this.getCoachByEmail);

    /**
     * @api {get} /users/client/id/:id/coaches Get Client Coaches
     * @apiName GetUsersClientCoaches
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Returns all coaches that have a relationship with the client
     *
     * @apiParam {Number} id Client ID
     * @apiParam {Number} [offset = 0] Number to offset results by
     * @apiParam {Number} [limit = 20] Number to limit results by
     *
     * @apiExample {curl} Example Usage
     *                  curl -i https://{BASE_URL}/users/client/id/1/coaches?offset=0&limit=20
     *
     * @apiUse ClientCoachListResponseModel
     */
    this.registerGetMethod('/client/id/:id/coaches', this.getCoachesByClientId);

    /**
     * @api {get} /users/coach/id/:id/clients Get Coach Clients
     * @apiName GetUsersCoachClients
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Returns all clients that have a relationship with the coach
     *
     * @apiParam {Number} id Coach ID
     * @apiParam {Number} [offset = 0] Number to offset results by
     * @apiParam {Number} [limit = 20] Number to limit results by
     *
     * @apiExample {curl} Example Usage
     *                  curl -i https://{BASE_URL}/users/coach/id/2/clients?offset=0&limit=20
     *
     * @apiUse CoachClientListResponseModel
     */
    this.registerGetMethod('/coach/id/:id/clients', this.getClientsByCoachId);

    /**
     * @api {delete} /users/client/id/:clientid/coach/id/:coachid Remove Client Coach
     * @apiName DeleteUsersClientCoach
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Removes a client coach relationship
     *
     * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with session token
     *
     * @apiParam {Number} clientid Client ID
     * @apiParam {Number} coachid Coach ID
     *
     * @apiExample {curl} Example Usage
     *                  curl -i -X DELETE https://{BASE_URL}/users/client/id/1/coach/id/2
     *
     * @apiError {String} error Error message if the relationship wasn't removed
     */
    this.registerDeleteMethod('/client/id/:clientid/coach/id/:coachid', this.deleteClientCoachRelationship);
   /**
     * @api {put} /users/id/:userid Update User
     * @apiName PutUsers
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Updates a user
     *
     * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with session token
     *
     * @apiParam {Number} userid User ID
     * @apiParam {String} [first_name] Updated first name
     * @apiParam {String} [last_name] Updated last name
     * @apiParam {String} [email] Updated email address
     *
     * @apiExample {curl} Example Usage
     *                  curl -X PUT -H "Content-Type: application/json" -d '{"first_name": "John", "last_name": "Doe"}' https://{BASE_URL}/users/id/1
     *
     * @apiError {String} error Error message if the user wasn't updated
     */
    this.registerPostMethod('/id/:userid', this.updateUserById);

};
Controller.prototype._handleThrownError = function(e, response) {
    if ((e instanceof NotFoundErrorModel)) {
        this.sendNotFoundError(response, {
            error: e.message
        });
        return true;
    }
    if ((e instanceof DuplicateEntityErrorModel)) {
        this.sendDuplicateError(response, {
            error: e.message
        });
        return true;
    }
    if ((e instanceof InvalidRequestErrorModel)) {
        this.sendBadRequestError(response, {
            error: e.message
        });
        return true;
    }
    return false;
};

Controller.prototype.createCoach = async function(request, response) {
    let model = new CoachRequestModel(request.body);

    if (!model.email || !model.password) {
        return this._handleThrownError(new InvalidRequestErrorModel('Email and password are required'), response);
    }

    try {
        const user = await userService.createUser(model);
        const session = await authenticationService.createAuthentication(model.email, model.password);
        this.sendSuccess(response, new UserSessionResponseModel({
            session: session,
            user: user
        }));
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error adding coach'
            });
        }
    }
};

Controller.prototype.createClient = async function(request, response) {
    let model = new ClientRequestModel(request.body);

    if (!model.email || !model.password) {
        return this._handleThrownError(new InvalidRequestErrorModel('Email and password are required'), response);
    }

    try {
        const user = await userService.createUser(model);
        const session = await authenticationService.createAuthentication(model.email, model.password);
        this.sendSuccess(response, new UserSessionResponseModel({
            session: session,
            user: user
        }));
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error adding client'
            });
        }
    }
};

Controller.prototype.createClientCoachRelationship = async function(request, response) {
    //const model = new ClientCoachRequestModel(request.body);
    const model    = request.body;

    if (!this.isAuthenticatedUser(request, model.clientid, true)) {
        this.sendForbiddenError(response, {
            error: 'Invalid permissions to add client coach relationship'
        });
        return new Promise(() => {});
    }

    let   errorMessage = '';
    let   successMessage = '';
    let   is_coach = false;
    let   clientid = model.clientid;
    let   coachid  = model.coachid;
    let   email    = model.coachemail;
    const code     = model.code;
    const email_is_given = (!!email);
    const code_is_given  = (code && code !== 'NO_CODE_GIVEN');

    if (!clientid || !(coachid || email || code_is_given)) {
       let errorMessage = 'Please specify an email or a code';
       if ( code === 'NO_CODE_GIVEN' )
            errorMessage = 'Please specify an email address';
       return this._handleThrownError(new InvalidRequestErrorModel(errorMessage), response);
    }

    var  coach;
    if ( coachid)
         coach = await userService.getUserByEmail(coachid, false);
    if (!coach)
         coach = await userService.getUserByEmail(email, false);

    let skip = false;
    if ( email_is_given && code && userService.getCoachFromCode(code) ){
         skip = true;
    }

    if ( code && !skip ) // for client. It's backwards.
    {
        //
        //  Look up code and get id's in order to connect coach & client
        //
        var conditional_was_found = true;
        try {

            is_coach = true;
            coachid  = clientid;
            let conditional = await userService.getConditionalFromCode(code);
            conditional_was_found = !!conditional;

            clientid = conditional.client_id;
            email    = conditional.email;

        } catch (e) {
            if ( conditional_was_found )
                 console.error(e);
            if (!this._handleThrownError(e, response)) {
                 this.sendServerError(response, {
                     error: 'The code was not found. Please double-check, ' +
                            'or have your client add you again.'
                 });
                 return;
            }
        }
        try {
            await userService.addEmail(coachid, email);
        } catch (e) {
            console.error("Could not add email to user: "+coachid, e);
        }
        errorMessage  = 'You entered code \'' + code + '\'. ';
        errorMessage += 'The next step is to ask your cient to share his/her rhythms.';

    }


    if ( email_is_given && !coach )
    {
        //
        //  Invite the coach and connect them
        //
        try {

            const success = await userService.inviteCoach(model.coachemail,{clientid:clientid});
            let data = {};
            successMessage = 'Your friend has been invited!';
            if ( success )
                 data.message_success = successMessage;
            this.sendCreatedSuccess(response, data);
            return;

        } catch (e) {
            console.error(e);
            if (!this._handleThrownError(e, response)) {
                 this.sendServerError(response, {
                     error: 'An unknown error was encountered ' +
                            'while looking up user: ' + model.coachemail
                 });
                 return;
            }
        }
        if ( !code )
              return;
    }
    if ( email_is_given && !errorMessage && coach ){
        errorMessage  = 'You entered this email: \n';
        errorMessage += '"' + userService.getName(coach) + '" ';
        errorMessage += '<' + model.coachemail + '>';
    }


    try {

        const data = await userService.addClientCoachRelationship(clientid, coachid, email, is_coach, errorMessage);
              data.is_coach = is_coach;
        if ( is_coach  && data.client_id && data.coach_id )
              data.success = true; // a hack because success was false for unknown reason
        if ( successMessage )
              data.message_success = successMessage;

        this.sendCreatedSuccess(response, data );

    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
             this.sendServerError(response, {
                 error: e && (e.error || e.message)
                        || 'Error encountered while connecting you to the coach'
                        /* + JSON.stringify(e && e.message )*/
             });
             return;
        }
    }
};


Controller.prototype.createSession = async function(request, response) {
    let model = new AuthenticationRequestModel(request.body);

    try {
        let session = await authenticationService.createSessionByEmailPassword(model.email, model.password),
            user = await userService.getUserById(session.user_id);
        this.sendSuccess(response, new UserSessionResponseModel({
            session: session,
            user: user
        }));
    } catch (e) {
        console.error(e);
        if ( model.isSilentRequest )
        {
            this.sendSuccess(response, {
                success:false,
                error:true,
            });
        } else {
            if (!this._handleThrownError(e, response)) {
                this.sendServerError(response, {
                    error: 'Error creating session'
                });
            }
        }
    }
};

Controller.prototype.setupForgotPassword = async function(request, response) {
    let model = request.body;

    try {
        await authenticationService.sendForgotPasswordEmail(model.email, model.returnurl);
        this.sendSuccess(response, {
        });
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: "Could not send password recovery email for: \n" + model.email
            });
        }
    }
};





Controller.prototype.changePassword = async function(request, response) {
    let model      = request.body;
    var user       = await userService.getUserByEmail(model.email);
    var secuity_ok;
    const self = this;
    

    secuity_ok = await (async function(){
        if (user && self.isAuthenticatedUser(request, user.id, false)) {
            return true;
        } else {
            let session = await authenticationService.createSessionByEmailPassword(model.email, model.oldpassword),
            user = await userService.getUserById(session.user_id);    
            return ( user.id == session.user_id );
        }
    })();
    
    if ( user.email != model.email ){
         secuity_ok = false;
    }


    if (!secuity_ok) {
        this.sendForbiddenError(response, {
            error: 'Cannot change password: Application safeguards are preventing this action'
        });
        return new Promise(() => {});
    }


    try {
        await authenticationService.updateAuthentication(user.id, model.password);
        this.sendSuccess(response, {});
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: "Could not send reset password: an unknown error was encountered"
            });
        }
    }
};





Controller.prototype.getUsers = async function(request, response) {
    let offset = isNaN(request.query.offset) ? 0 : parseInt(request.query.offset),
        limit = isNaN(request.query.limit) ? 20 : parseInt(request.query.limit);

    try {
        let users = await userService.getUsers(offset, limit);
        this.sendSuccess(response, users);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'An error was encountered while looking up users'
            });
        }
    }
};

Controller.prototype.getCoachByEmail = async function(request, response) {
    let email = request.params.email;

    try {
        let user = await userService.getCoachUserByEmail(email);
        this.sendSuccess(response, user);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error getting coach'
            });
        }
    }
};

Controller.prototype.getCoachesByClientId = async function(request, response) {
    let clientId = request.params.id,
        offset = isNaN(request.query.offset) ? 0 : parseInt(request.query.offset),
        limit = isNaN(request.query.limit) ? 20 : parseInt(request.query.limit);

    try {
        let data = await userService.getCoachesByClientId(clientId, offset, limit);
        this.sendSuccess(response, data);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error getting client coach list'
            });
        }
    }
};

Controller.prototype.getClientsByCoachId = async function(request, response) {
    const coachId = request.params.id,
        offset = isNaN(request.query.offset) ? 0 : parseInt(request.query.offset),
        limit = isNaN(request.query.limit) ? 20 : parseInt(request.query.limit);

    try {
        let data = await userService.getClientsByCoachId(coachId, offset, limit);
        this.sendSuccess(response, data);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error getting coach client list'
            });
        }
    }
};

Controller.prototype.deleteClientCoachRelationship = async function(request, response) {
    let clientId = request.params.clientid,
        coachId = request.params.coachid;

    if (!this.isAuthenticatedUser(request, clientId, true)) {
        this.sendForbiddenError(response, {
            error: 'Invalid permissions to add client coach relationship'
        });
        return new Promise(() => {});
    }

    try {
        await userService.deleteClientCoachRelationship(clientId, coachId);
        this.sendSuccessNoContent(response);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error removing coach relationship from client'
            });
        }
    }
};

Controller.prototype.updateUserById = async function(request, response) {
    let userId = request.params.userid;

    if (!this.isAuthenticatedUser(request, userId, true)) {
        this.sendForbiddenError(response, {
            error: 'Invalid permissions to update user'
        });
        return new Promise(() => {});
    }

    try {
	let newInfo = new UserRequestModel(request.body);
        const promise_updateUser = userService.updateUserById(userId, newInfo);
        var   promise_updatePass;
        if ( newInfo.password )
              promise_updatePass = authenticationService.updateAuthentication(userId, newInfo.password);
        await promise_updatePass;
        await promise_updateUser;
        this.sendSuccess(response, newInfo);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error updating user'
            });
        }
    }
};

module.exports = Controller;
