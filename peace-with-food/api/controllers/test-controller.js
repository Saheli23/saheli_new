const   util = require('util'),
        BaseController = require('./base-controller'),
        userService = require('../services/user-service'),
        authenticationService = require('../services/authentication-service'),
        DuplicateEntityErrorModel = require('../models/error/duplicate-entity-error-model'),
        NotFoundErrorModel = require('../models/error/not-found-error-model'),
        InvalidRequestErrorModel = require('../models/error/invalid-request-error-model'),
        CoachRequestModel = require('../models/request/coach-user-request-model'),
        ClientRequestModel = require('../models/request/client-user-request-model'),
        AuthenticationRequestModel = require('../models/request/authentication-request-model'),
        UserRequestModel = require('../models/request/user-request-model'),
        ClientCoachRequestModel = require('../models/request/client-coach-request-model'),
        UserSessionResponseModel = require('../models/response/user-session-response-model')
        ;

function Controller(app) {
    if (!(this instanceof Controller)) {
        return new Controller(app);
    }
    BaseController.call(this, app, {
        path: '/test'
    });
}

util.inherits(Controller, BaseController);

Controller.prototype.registerAllMethods = function() {

    /**
     * @api {get} /users Get Users
     * @apiName GetUsers
     * @apiGroup Users
     * @apiVersion 1.0.0
     *
     * @apiDescription Returns all users
     *
     * @apiParam {Number} [offset = 0] Number to offset results by
     * @apiParam {Number} [limit = 20] Number to limit results by
     *
     * @apiExample {curl} Example Usage
     *                  curl -i https://{BASE_URL}/users?offset=0&limit=20
     *
     * @apiUse UserListResponseModel
     */
    this.registerGetMethod('/', this.getUsers);
};


Controller.prototype._handleThrownError = function(e, response) {
    if ((e instanceof NotFoundErrorModel)) {
        this.sendNotFoundError(response, {
            error: e.message
        });
        return true;
    }
    if ((e instanceof DuplicateEntityErrorModel)) {
        this.sendDuplicateError(response, {
            error: e.message
        });
        return true;
    }
    if ((e instanceof InvalidRequestErrorModel)) {
        this.sendBadRequestError(response, {
            error: e.message
        });
        return true;
    }
    return false;
};





Controller.prototype.getUsers = async function(request, response) {
    let offset = 0,
         limit = 1,
         html;
    try {
    
    
        let data = await userService.getUsers(offset, limit);
        //this.sendSuccess(response, users);
        
        if ( data && data.users && data.users.length > 0 )
        {
            html = 'CONNECTED';
            this.sendHtml(response, html);
            
        } else {
            
            this.sendServerError(response, {
                error: 'Could not connect to the database'
            });
        }
        
        
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'An error was encountered while accessing the database'
            });
        }
    }
};





module.exports = Controller;
