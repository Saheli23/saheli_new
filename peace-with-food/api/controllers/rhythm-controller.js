const util = require('util'),
    BaseController = require('./base-controller'),
    DuplicateEntityErrorModel = require('../models/error/duplicate-entity-error-model'),
    NotFoundErrorModel = require('../models/error/not-found-error-model'),
    InvalidRequestErrorModel = require('../models/error/invalid-request-error-model'),
    ForbiddenErrorModel = require('../models/error/forbidden-error-model'),
    TrackEntryRequestModel = require('../models/request/track-entry-request-model'),
    trackService = require('../services/track-service');

function Controller(app) {
    if (!(this instanceof Controller)) {
        return new Controller(app);
    }
    BaseController.call(this, app, {
        path: '/rhythms'
    });
}

util.inherits(Controller, BaseController);

Controller.prototype.registerAllMethods = function() {

    /**
     * @api {get} /rhythms Get Rhythm Options
     * @apiName GetRhythms
     * @apiGroup Rhythms
     * @apiVersion 1.0.0
     *
     * @apiDescription Returns all possible rhythms and values
     *
     * @apiExample {curl} Example Usage
     *                  curl -i https://{BASE_URL}/rhythms
     *
     * @apiUse OptionRhythmListResponseModel
     */
    this.registerGetMethod('/', this.getRhythms);

    /**
     * @api {get} /rhythms/client/id/:id Get Rhythms By Client ID
     * @apiName GetRhythmsByClientId
     * @apiGroup Rhythms
     * @apiVersion 1.0.0
     *
     * @apiDescription Returns rhythm entries for the provided client ID
     *
     * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with session token
     *
     * @apiParam {Number} id Client ID
     * @apiParam {Number} [start_date] Start date (inclusive) of results in YYYY-MM-DD format
     * @apiParam {Number} [end_date] End date (inclusive) of results in YYYY-MM-DD format
     * @apiParam {Number} [offset = 0] Number to offset results by (ignored if date range specified)
     * @apiParam {Number} [limit = 20] Number to limit results by (ignored if date range specified)
     *
     * @apiExample {curl} Example Date Range Usage
     *                  curl -i https://{BASE_URL}/rhythms/client/id/1?start_date=2017-12-01&end_date=2017-12-31
     *
     * @apiExample {curl} Example Offset Limit Usage
     *                  curl -i https://{BASE_URL}/rhythms/client/id/1?offset=0&limit=20
     *
     * @apiUse TrackEntryListResponseModel
     */
    this.registerGetMethod('/client/id/:id', this.getRhythmsByClientId);

    /**
     * @api {get} /rhythms/client/id/:id/average Get Average Rhythms By Client ID
     * @apiName GetAverageRhythmsByClientId
     * @apiGroup Rhythms
     * @apiVersion 1.0.0
     *
     * @apiDescription Returns average rhythm entries for the provided client ID
     *
     * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with session token
     *
     * @apiParam {Number} id Client ID
     * @apiParam {Number} [start_date] Start date (inclusive) of results in YYYY-MM format
     * @apiParam {Number} [end_date] End date (inclusive) of results in YYYY-MM format
     *
     * @apiExample {curl} Example Date Range Usage
     *                  curl -i https://{BASE_URL}/rhythms/client/id/1/average?start_date=2017-12&end_date=2017-12
     *
     * @apiUse TrackEntryAverageListResponseModel
     */
    this.registerGetMethod('/client/id/:id/average', this.getAverageRhythmsByClientId);

    /**
     * @api {post} /rhythms Create Rhythm Entry
     * @apiName PostRhythms
     * @apiGroup Rhythms
     * @apiVersion 1.0.0
     *
     * @apiDescription Creates a new rhythm entry for the provided client ID
     *
     * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with session token
     *
     * @apiParam {Number} user_id User ID
     * @apiParam {Number} value Entry value
     *
     * @apiExample {js} Example usage:
     *                  {
     *                      user_id: 1,
     *                      value: 8
     *                  }
     *
     * @apiUse TrackEntryResponseModel
     *
     * @apiError {String} error Error message if the entry wasn't created
     */
    this.registerPostMethod('/', this.addRhythmEntry);

    /**
     * @api {post} /rhythms/details Adds more details to a Rhythm Entry
     * @apiName PostRhythms
     * @apiGroup Rhythms
     * @apiVersion 1.0.0
     *
     * @apiDescription Creates a new rhythm detail entry for the provided entry ID
     *
     * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with session token
     *
     * @apiParam {Number} user_id          User ID
     * @apiParam {Number} rhythm_entry_id  Entry ID
     * @apiParam {Text}   thoughts         Notes from the user, long string
     * @apiParam {Object} options          key: boolean values
     *
     * @apiExample {js} Example usage:
     *                  {
     *                      user_id: 1,
     *                      rhythm_entry_id: 3,
     *                      thoughts: "felt feelings of feeling",
     *                      options: {"Mood":{"Stressed":true,"Lonely":false,"Angry":false}"Hunger":true},
     *                  }
     *
     * @apiUse TrackEntryResponseModel
     *
     * @apiError {String} error Error message if the entry wasn't created
     */
    this.registerPostMethod('/details', this.addRhythmDetailEntry);

    /**
     * @api {post} /rhythms/share Create Shared Rhythm
     * @apiName PostRhythmsShare
     * @apiGroup Rhythms
     * @apiVersion 1.0.0
     *
     * @apiDescription Shares a rhythm with the provided coach ID
     *
     * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with session token
     *
     * @apiParam {Number} entry_id Entry ID to share
     * @apiParam {Number} coach_id Coach ID to share with
     *
     * @apiExample {js} Example usage:
     *                  {
     *                      entry_id: 1,
     *                      coach_id: 2
     *                  }
     *
     * @apiError {String} error Error message if the entry wasn't shared
     */
    this.registerPostMethod('/share', this.shareRhythmEntry);

    /**
     * @api {post} /rhythms/dates Create Shared Rhythm Instance with shared rhythms
     * @apiName PostRhythmsShare
     * @apiGroup Rhythms
     * @apiVersion 1.0.0
     *
     * @apiDescription Shares rhythms based on dates (unix timestamps)
     *
     * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with session token
     *
     * @apiParam {Number} x Entry ID to share
     * @apiParam {Number} x Coach ID to share with
     *
     * @apiExample {js} Example usage:
     *                  {
     *                      TODO
     *                  }
     *
     * @apiError {String} error Error message if the entry wasn't shared
     */
    this.registerPostMethod('/share/dates', this.shareRhythmEntries);


    /**
     * @api {get} /share/coach/:coachid Get Shared Rhythm Instances by coach
     * @apiName GetRhythmsShareInstances
     * @apiGroup Rhythms
     * @apiVersion 1.0.0
     *
     * @apiDescription When a user shares entires, an instance is created
     *
     */
    this.registerGetMethod('/share/instances/coach/:coachid', this.getSharedRhythmInstances);

    /**
     * @api {get} /share/coach/:coachid Get My Shared Rhythm Instances by client
     * @apiName GetMyRhythmsShareInstances
     * @apiGroup Rhythms
     * @apiVersion 1.0.0
     *
     * @apiDescription When a user shares entires, an instance is created
     *
     */
    this.registerGetMethod('/share/instances/client/:clientid', this.getMySharedRhythmInstances);

    /**
     * @api {get} /share/coach Get Shared Rhythm Instances
     * @apiName GetRhythmsShareInstances
     * @apiGroup Rhythms
     * @apiVersion 1.0.0
     *
     * @apiDescription When a user shares entires, an instance is created
     *
     */
    this.registerGetMethod('/share/instances', this.getSharedRhythmInstances);


    /**
     * @api {delete} /rhythms/share/entry/id/:entryid/coach/id/:coachid Remove Shared Rhythm
     * @apiName DeleteRhythmsShare
     * @apiGroup Rhythms
     * @apiVersion 1.0.0
     *
     * @apiDescription Removes a shared trackable entry with a coach
     *
     * @apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with session token
     *
     * @apiParam {Number} entryid Entry ID
     * @apiParam {Number} coachid Coach ID
     *
     * @apiExample {curl} Example Usage
     *                  curl -i -X DELETE https://{BASE_URL}/rhythms/share/entry/id/1/coach/id/2
     *
     * @apiError {String} error Error message if the shared entry wasn't removed
     */
    this.registerDeleteMethod('/share/entry/id/:entryid/coach/id/:coachid', this.deleteSharedRhythmEntry);
};

Controller.prototype._handleThrownError = function(e, response) {
    if ((e instanceof NotFoundErrorModel)) {
        this.sendNotFoundError(response, {
            error: e.message
        });
        return true;
    }
    if ((e instanceof DuplicateEntityErrorModel)) {
        this.sendDuplicateError(response, {
            error: e.message
        });
        return true;
    }
    if ((e instanceof InvalidRequestErrorModel)) {
        this.sendDuplicateError(response, {
            error: e.message
        });
        return true;
    }
    if ((e instanceof ForbiddenErrorModel)) {
        this.sendForbiddenError(response, {
            error: e.message
        });
        return true;
    }
    return false;
};

Controller.prototype.getRhythms = async function(request, response) {
    // get rhythm options
    try {
        let data = await trackService.getRhythms();
        this.sendSuccess(response, data);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error getting rhythms'
            });
        }
    }
};

Controller.prototype.getRhythmsByClientId = async function(request, response) {
    let clientId = request.params.id,
        offset = isNaN(request.query.offset) ? 0 : parseInt(request.query.offset),
        limit = isNaN(request.query.limit) ? 20 : parseInt(request.query.limit),
        startDate = request.query.start_date,
        endDate = request.query.end_date;
	if( typeof( request.query.timezone_offset ) != 'undefined' ){
		timezoneOffset = request.query.timezone_offset;
	}else{
		timezoneOffset = '-06:00';
	}
//
//     TODO Fixme
//
//     if (!this.isAuthenticatedUser(request, clientId, true)) {
//         this.sendForbiddenError(response, {
//             error: 'Invalid permissions to get client rhythms'
//         });
//         return new Promise(() => {});
//     }

    try {
        let data;
        if (startDate && endDate) {
            data = await trackService.getEntriesByUserIdBetweenDates(clientId, startDate, endDate, timezoneOffset);
        } else {
            data = await trackService.getEntriesByUserId(clientId, offset, limit, timezoneOffset);
        }

        this.sendSuccess(response, data);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error getting rhythm entries'
            });
        }
    }
};

Controller.prototype.getAverageRhythmsByClientId = async function(request, response) {
    let clientId = request.params.id,
        startDate = request.query.start_date,
        endDate = request.query.end_date;

    if (!this.isAuthenticatedUser(request, clientId, true)) {
        this.sendForbiddenError(response, {
            error: 'Invalid permissions to get client rhythms'
        });
        return new Promise(() => {});
    }

    try {
        let data = await trackService.getAverageEntriesByUserIdBetweenDates(clientId, startDate, endDate);
        this.sendSuccess(response, data);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error getting rhythm entries'
            });
        }
    }
};

Controller.prototype.addRhythmEntry = async function(request, response) {
    const model = new TrackEntryRequestModel(request.body);

    if (!this.isAuthenticatedUser(request, model.user_id, true)) {
        this.sendForbiddenError(response, {
            error: 'Invalid permissions to add a rhythm entry'
        });
        return new Promise(() => {});
    }

    try {
        let data = await trackService.createEntry(model);
        this.sendCreatedSuccess(response, data);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error encountered while adding the rhythm entry'
            });
        }
    }
};

Controller.prototype.addRhythmDetailEntry = async function(request, response) {
    const model = request.body;

    // if (!this.isAuthenticatedUser(request, model.user_id, true)) {
    //     this.sendForbiddenError(response, {
    //         error: 'Invalid permissions to add rhythm entry details'
    //     });
    //     return new Promise(() => {});
    // }

    try {
        let data = await trackService.createDetailEntry(model);
        this.sendCreatedSuccess(response, data);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error encountered while adding rhythm entry details'
            });
        }
    }
};

Controller.prototype.getSharedRhythmInstances = async function(request, response) {
    var   coachId     = request.params.coachid;
    const sessionUser = this.getSessionUser(request);
    if ( !coachId ){
          coachId =  (sessionUser || {}).id;
    }
    if ( !coachId && !sessionUser ){
                this.sendForbiddenError(response, {
                    error: 'Please log in'
                });
    }

    try {
        const instances = await trackService.getSharedEntryInstances( coachId );
        this.sendSuccess(response,instances);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error encountered while retrieving shared data'
            });
        }
    }
};

Controller.prototype.getMySharedRhythmInstances = async function(request, response) {
    var   clientId     = request.params.clientid;
    const sessionUser = this.getSessionUser(request);
    if ( !clientId ){
          clientId =  (sessionUser || {}).id;
    }
    if ( !clientId && !sessionUser ){
                this.sendForbiddenError(response, {
                    error: 'Please log in'
                });
    }

    try {
        const instances = await trackService.getMySharedEntryInstances( clientId );
        this.sendSuccess(response,instances);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error encountered while retrieving shared data'
            });
        }
    }
};

Controller.prototype.shareRhythmEntry = async function(request, response) {
    const sessionUser = this.getSessionUser(request);

    try {
        await trackService.addSharedEntry((sessionUser || {}).id, request.body.entry_id, request.body.coach_id);
        this.sendSuccessNoContent(response);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error sharing rhythm entry'
            });
        }
    }
};

Controller.prototype.shareRhythmEntries = async function(request, response) {
    const sessionUser = this.getSessionUser(request);
    const model       = request.body;

    if ( Math.abs( model.begin_epoch - model.end_epoch ) < 10*1000 ){
         //
         // hotfix: assume sharing of "today" when dates are equal
         //
         model.end_epoch = new Date().getTime();
    }


    try {
        let instance = await trackService.addSharedEntriesByDate(
                                                    (sessionUser || {}).id,
                                                    model.coach_id,
                                                    model.title,
                                                    model.begin_epoch,
                                                    model.end_epoch);
        this.sendSuccess(response,instance);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error sharing rhythm entry'
            });
        }
    }
};

Controller.prototype.deleteSharedRhythmEntry = async function(request, response) {
    const sessionUser = this.getSessionUser(request);
    const entryId = request.params.entryid;
    const coachId = request.params.coachid;

    try {
        await trackService.deleteSharedEntry((sessionUser || {}).id, entryId, coachId);
        this.sendSuccessNoContent(response);
    } catch (e) {
        console.error(e);
        if (!this._handleThrownError(e, response)) {
            this.sendServerError(response, {
                error: 'Error removing shared rhythm entry'
            });
        }
    }
};

module.exports = Controller;
