function DeviceTokenRequestModel(data = {}) {
    this.user_id = data.user_id;
    this.os = data.os;
    this.token = data.token;
}

module.exports = DeviceTokenRequestModel;