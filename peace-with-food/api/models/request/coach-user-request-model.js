let util = require('util'),
    UserRequestModel = require('./user-request-model'),
    userTypeEnum = require('../enum/user-type-enum');

function CoachUserRequestModel(data) {
    data = data || {};
    data.role = userTypeEnum.types.COACH.role;
    UserRequestModel.call(this, data);
}

util.inherits(CoachUserRequestModel, UserRequestModel);

module.exports = CoachUserRequestModel;