function AuthenticationRequestModel(data) {
    this.email = data.email;
    this.password = data.password;
    this.isSilentRequest = data.silent || false;
}

module.exports = AuthenticationRequestModel;