let util = require('util'),
    UserRequestModel = require('./user-request-model'),
    userTypeEnum = require('../enum/user-type-enum');

function ClientUserRequestModel(data) {
    data = data || {};
    data.role = userTypeEnum.types.CLIENT.role;
    UserRequestModel.call(this, data);
}

util.inherits(ClientUserRequestModel, UserRequestModel);

module.exports = ClientUserRequestModel;