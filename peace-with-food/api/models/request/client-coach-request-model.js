function ClientCoachRequestModel(data = {}) {
    this.client_id = data.client_id;
    this.coach_id = data.coach_id;
}

module.exports = ClientCoachRequestModel;