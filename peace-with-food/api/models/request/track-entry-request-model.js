function TrackEntryRequestModel(data = {}) {
    this.user_id = data.user_id;
    this.value = data.value;
    this.public_description = data.public_description;
    this.private_description = data.private_description;
}

module.exports = TrackEntryRequestModel;