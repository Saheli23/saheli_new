function UserRequestModel(data) {
    this.email = data.email;
    this.password = data.password;
    this.first_name = data.firstname || data.first_name || data.fullname;
    this.last_name  = data.lastname  || data.last_name;
    this.role = data.role;
}

module.exports = UserRequestModel;