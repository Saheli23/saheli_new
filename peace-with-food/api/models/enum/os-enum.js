const types = Object.freeze({
    IOS: {
        key: 'ios',
        value: 1
    },
    ANDROID: {
        key: 'android',
        value: 2
    }
});

module.exports = {
    types,
    getValueByKey: (key) => {
        return Object.values(types).filter(type => type.key === key)[0];
    }
};