const types = Object.freeze({
    CLIENT: {
        role: 1
    },
    COACH: {
        role: 2
    }
});

module.exports = {
    types: types
};