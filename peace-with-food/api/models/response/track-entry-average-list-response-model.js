let TrackEntryAverageResponseModel = require('./track-entry-average-response-model');

/**
 * @apiDefine TrackEntryAverageListResponseModel
 *
 * @apiSuccess {TrackEntryAverageResponseModel[]} entries List of entries
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "averages": [
 *                      {
 *                          "client_id": 1,
 *                          "total_entries": 10,
 *                          "values": [35, 53, 12],
 *                          "start_date": "2017-11-02T22:22:34.000Z",
 *                          "end_date": "2017-11-31T22:22:34.000Z"
 *                      },
 *                      ...
 *                      {
 *                          "client_id": 1,
 *                          "total_entries": 10,
 *                          "values": [35, 53, 12],
 *                          "start_date": "2017-11-02T22:22:34.000Z",
 *                          "end_date": "2017-11-31T22:22:34.000Z"
 *                      }
 *                  ]
 *              }
 */
function TrackEntryAverageListResponseModel(entries) {
    this.entries = (entries || []).map((entry) => new TrackEntryAverageResponseModel(entry));
}

module.exports = TrackEntryAverageListResponseModel;