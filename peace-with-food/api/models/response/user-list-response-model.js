let userTypeEnum = require('../enum/user-type-enum'),
    CoachUserResponseModel = require('./coach-user-response-model'),
    ClientUserResponseModel = require('./client-user-response-model');

/**
 * @apiDefine UserListResponseModel
 *
 * @apiSuccess {Number} total Total number of users
 * @apiSuccess {UserResponseModel[]} users List of users
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "total": 2,
 *                  "users": [
 *                      {
 *                          "id": 1,
 *                          "email": "client@peacewithfood.com",
 *                          "first_name": "John",
 *                          "last_name": "Doe",
 *                          "role": 1
 *                      },
 *                      {
 *                          "id": 2,
 *                          "email": "coach@peacewithfood.com",
 *                          "first_name": "Jane",
 *                          "last_name": "Doe",
 *                          "role": 2
 *                      }
 *                  ]
 *              }
 */
function UserListResponseModel(total, users) {
    this.total = total;
    this.users = users.map((user) => {
        if (user.role === userTypeEnum.types.COACH.role) {
            return new CoachUserResponseModel(user);
        }
        return new ClientUserResponseModel(user);
    });
}

module.exports = UserListResponseModel;