
/**
 * @apiDefine SessionResponseModel
 *
 * @apiSuccess {Number} id Session ID
 * @apiSuccess {Number} user_id User ID
 * @apiSuccess {String} token Session token
 * @apiSuccess {Date} expires Session expire date
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "id": 1,
 *                  "user_id": 1,
 *                  "token": "abcdefg12345",
 *                  "expire_date": "2017-11-02T22:47:56.000Z"
 *              }
 */
function SessionResponseModel(data) {
    this.id = data.id;
    this.user_id = data.user_id;
    this.token = data.token;
    this.expire_date = data.expire_date;
}

module.exports = SessionResponseModel;