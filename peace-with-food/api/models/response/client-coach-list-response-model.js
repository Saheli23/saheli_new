let CoachUserResponseModel = require('./coach-user-response-model');

/**
 * @apiDefine ClientCoachListResponseModel
 *
 * @apiSuccess {Number} total Total number of coaches
 * @apiSuccess {CoachUserResponseModel[]} coaches List of coaches
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "total": 1,
 *                  "coaches": [
 *                      {
 *                          "id": 2,
 *                          "email": "coach@peacewithfood.com",
 *                          "first_name": "Jane",
 *                          "last_name": "Doe",
 *                          "role": 2
 *                      }
 *                  ]
 *              }
 */
function ClientCoachListResponseModel(total, coaches) {
    this.total = total;
    this.coaches = coaches.map((coach) => new CoachUserResponseModel(coach));
}

module.exports = ClientCoachListResponseModel;