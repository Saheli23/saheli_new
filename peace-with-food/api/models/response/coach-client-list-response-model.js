let ClientUserResponseModel = require('./client-user-response-model');

/**
 * @apiDefine CoachClientListResponseModel
 *
 * @apiSuccess {Number} total Total number of clients
 * @apiSuccess {ClientUserResponseModel[]} clients List of clients
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "total": 1,
 *                  "clients": [
 *                      {
 *                          "id": 1,
 *                          "email": "client@peacewithfood.com",
 *                          "first_name": "John",
 *                          "last_name": "Doe",
 *                          "role": 1
 *                      }
 *                  ]
 *              }
 */
function CoachClientListResponseModel(total, clients) {
    this.total = total;
    this.clients = clients.map((client) => new ClientUserResponseModel(client));
}

module.exports = CoachClientListResponseModel;