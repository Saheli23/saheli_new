let util = require('util'),
    UserResponseModel = require('./user-response-model');

/**
 * @apiDefine ClientUserResponseModel
 *
 * @apiSuccess {Number} id Client ID
 * @apiSuccess {String} email Email address
 * @apiSuccess {String} first_name First name
 * @apiSuccess {String} last_name Last name
 * @apiSuccess {Number} role Client role ID
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "id": 1,
 *                  "email": "client@peacewithfood.com",
 *                  "first_name": "John",
 *                  "last_name": "Doe",
 *                  "role": 1
 *              }
 */
function ClientUserResponseModel(data) {
    data = data || {};
    UserResponseModel.call(this, data);
}

util.inherits(ClientUserResponseModel, UserResponseModel);

module.exports = ClientUserResponseModel;