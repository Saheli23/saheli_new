/**
 * @apiDefine OptionRhythmResponseModel
 *
 * @apiSuccess {String} label Label constant for rhythm
 * @apiSuccess {Number} value Rhythm numerical value
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "label": "FAMISHED",
 *                  "value": 1
 *              }
 */
function OptionRhythmResponseModel(data) {
    this.label = data.label;
    this.value = data.value;
}

module.exports = OptionRhythmResponseModel;