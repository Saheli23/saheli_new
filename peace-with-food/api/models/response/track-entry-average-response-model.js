/**
 * @apiDefine TrackEntryAverageResponseModel
 *
 * @apiSuccess {Number} id Entry ID
 * @apiSuccess {Number} client_id Client ID
 * @apiSuccess {Number[]} values Rhythm entry average values
 * @apiSuccess {Date} created_date Date entry was created
 * @apiSuccess {Date} modified_date Date entry was last modified
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "client_id": 1,
 *                  "total_entries": 10,
 *                  "values": [35, 53, 12],
 *                  "start_date": "2017-11-02T22:22:34.000Z",
 *                  "end_date": "2017-11-31T22:22:34.000Z"
 *              }
 */
function TrackEntryAverageResponseModel(data) {
    this.client_id = data.client_id;
    this.total_entries = data.total_entries;
    this.values = data.values;
    this.start_date = data.start_date;
    this.end_date = data.end_date;
}

module.exports = TrackEntryAverageResponseModel;