let TrackEntryResponseModel = require('./track-entry-response-model');

/**
 * @apiDefine TrackEntryListResponseModel
 *
 * @apiSuccess {Number} total Total number of entries
 * @apiSuccess {TrackEntryResponseModel[]} entries List of entries
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "total": 3,
 *                  "rhythms": [
 *                      {
 *                          "id": 3,
 *                          "client_id": 1,
 *                          "value": 5,
 *                          "created_date": "2017-11-02T22:22:34.000Z",
 *                          "modified_date": "2017-11-02T22:22:34.000Z"
 *                      },
 *                      ...
 *                      {
 *                          "id": 1,
 *                          "client_id": 1,
 *                          "value": 5,
 *                          "created_date": "2017-11-01T22:22:21.000Z",
 *                          "modified_date": "2017-11-02T22:47:56.000Z"
 *                      }
 *                  ]
 *              }
 */
function TrackEntryListResponseModel(total, entries) {
    this.total = (isNaN(total)) ? 0 : total;
    this.entries = (entries || []).map((entry) => new TrackEntryResponseModel(entry));
}

module.exports = TrackEntryListResponseModel;