/**
 * @apiDefine ClientCoachResponseModel
 *
 * @apiSuccess (Success 2xx) {Number} id Relationship ID
 * @apiSuccess (Success 2xx) {Number} client_id Client ID
 * @apiSuccess (Success 2xx) {Number} coach_id Coach ID
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "id": 1,
 *                  "client_id": 1,
 *                  "coach_id": 2
 *              }
 */
function ClientCoachResponseModel(data = {}) {
    this.id = data.id;
    this.client_id = data.client_id;
    this.coach_id = data.coach_id;
}

module.exports = ClientCoachResponseModel;