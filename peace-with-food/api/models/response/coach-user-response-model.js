let util = require('util'),
    UserResponseModel = require('./user-response-model');

/**
 * @apiDefine CoachUserResponseModel
 *
 * @apiSuccess {Number} id Coach ID
 * @apiSuccess {String} email Email address
 * @apiSuccess {String} first_name First name
 * @apiSuccess {String} last_name Last name
 * @apiSuccess {Number} role Coach role ID
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "id": 2,
 *                  "email": "coach@peacewithfood.com",
 *                  "first_name": "John",
 *                  "last_name": "Doe",
 *                  "role": 2
 *              }
 */
function CoachUserResponseModel(data) {
    data = data || {};
    UserResponseModel.call(this, data);
}

util.inherits(CoachUserResponseModel, UserResponseModel);

module.exports = CoachUserResponseModel;