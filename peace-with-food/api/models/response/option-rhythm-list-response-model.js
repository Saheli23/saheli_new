let OptionRhythmResponseModel = require('./option-rhythm-response-model');

/**
 * @apiDefine OptionRhythmListResponseModel
 *
 * @apiSuccess {Number} total Total number of rhythms
 * @apiSuccess {OptionRhythmResponseModel[]} rhythms List of rhythms
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "total": 9,
 *                  "rhythms": [
 *                      {
 *                          "label": "FAMISHED",
 *                          "value": 1
 *                      },
 *                      ...
 *                      {
 *                          "label": "SICK",
 *                          "value": 9
 *                      }
 *                  ]
 *              }
 */
function OptionRhythmListResponseModel(total, rhythms) {
    this.total = total;
    this.rhythms = rhythms.map((rhythm) => new OptionRhythmResponseModel(rhythm));
}

module.exports = OptionRhythmListResponseModel;