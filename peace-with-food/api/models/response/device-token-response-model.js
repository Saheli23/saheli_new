
/**
 * @apiDefine DeviceTokenResponseModel
 *
 * @apiSuccess {Number} id Device token ID
 * @apiSuccess {Number} user_id User ID
 * @apiSuccess {String} os Operating system
 * @apiSuccess {String} token Device token
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "id": 1,
 *                  "user_id": 1,
 *                  "os": "ios",
 *                  "token": "abcdefg12345"
 *              }
 */
function DeviceTokenResponseModel(data) {
    this.id = data.id;
    this.user_id = data.user_id;
    this.os = data.os;
    this.token = data.token;
}

module.exports = DeviceTokenResponseModel;