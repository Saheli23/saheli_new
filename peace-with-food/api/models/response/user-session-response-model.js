let UserResponseModel = require('./user-response-model'),
    SessionResponseModel = require('./session-response-model');

/**
 * @apiDefine UserSessionResponseModel
 *
 * @apiSuccess {UserResponseModel} user User model
 * @apiSuccess {SessionResponseModel} session Session model
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "user": {
 *                      "id": 1,
 *                      "email": "client@peacewithfood.com",
 *                      "first_name": "John",
 *                      "last_name": "Doe",
 *                      "role": 1
 *                  },
 *                  "session": {
 *                      "id": 1,
 *                      "user_id": 1,
 *                      "token": "abcdefg12345",
 *                      "expires": "2017-11-02 15:48:01"
 *                  }
 *              }
 */
function UserSessionResponseModel(data) {
    if (data.user && !(data.user instanceof UserResponseModel)) {
        throw new TypeError('User must be type UserResponseModel');
    }
    if (data.session && !(data.session instanceof SessionResponseModel)) {
        throw new TypeError('Session must be type SessionResponseModel');
    }

    this.user = data.user;
    this.session = data.session;
}

module.exports = UserSessionResponseModel;