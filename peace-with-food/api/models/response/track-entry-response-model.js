/**
 * @apiDefine TrackEntryResponseModel
 *
 * @apiSuccess {Number} id Entry ID
 * @apiSuccess {Number} client_id Client ID
 * @apiSuccess {Number} value Rhythm entry value
 * @apiSuccess {Date} created_date Date entry was created
 * @apiSuccess {Date} modified_date Date entry was last modified
 *
 * @apiSuccessExample {json} Example success
 *              {
 *                  "id": 3,
 *                  "client_id": 1,
 *                  "value": 5,
 *                  "created_date": "2017-11-02T22:22:34.000Z",
 *                  "modified_date": "2017-11-02T22:22:34.000Z"
 *              }
 */
function TrackEntryResponseModel(data) {
    this.id         = data.id;
    this.client_id  = data.client_id;
    this.value      = data.value;
    this.time       = data.time;
    this._debug     = data._debug;
    
    if (data.thoughts) {
        this.thoughts = data.thoughts;
    }
    if(data.message_id) {
	this.message_id = data.message_id;
    }
    if (data.options){
        this.options = data.options;
    }
    // This contains the wrong timezone data
    if (data.created_date) {
        this.created_date = data.created_date;
    }
    if (data.modified_date) {
        this.modified_date = data.modified_date;
    }
    //
    if (data.created_unix) {
       this.created_unix = data.created_unix;
    }
    if (data.modified_unix) {
       this.modified_unix = data.modified_unix;
    }
}

module.exports = TrackEntryResponseModel;
