import {createSelector} from 'reselect';
import {AsyncStorage} from 'react-native';
import Config from 'PeaceWithFood/app.json';
import Expo from 'expo';
import * as api from 'PeaceWithFood/src/lib/api';

import user from './mock/user';
import sessionToken from './mock/session-token';

const KEY_USER = "@pwf/user";
const KEY_TOKEN = "@pwf/token";

export const stateKey = 'user';

const TYPES = {
    START_LOGIN: `${stateKey}/START_LOGIN`,
    FINISH_LOGIN: `${stateKey}/FINISH_LOGIN`,
    UPDATE_USER: `${stateKey}/UPDATE_USER`,
    UPDATE_TOKEN: `${stateKey}/UPDATE_TOKEN`,
    CLEAR_ERRORS: `${stateKey}/CLEAR_ERRORS`,
};

const ERRORS = {
    "GenericError": "Error. Please try again later."
};

const startLogin = () => ({type: TYPES.START_LOGIN});

const finishLogin = (error) => ({type: TYPES.FINISH_LOGIN, error});

const updateUser = (user) => ({type: TYPES.UPDATE_USER, user});

const updateToken = (token) => ({type: TYPES.UPDATE_TOKEN, token});

const clearErrors = () => ({type: TYPES.CLEAR_ERRORS});

function wait333ms() {
        return new Promise( resolve => setTimeout(resolve, 333) );
}

const login = (email, password, options) => async (dispatch, getState) => {
    options = options || {};
    dispatch(startLogin());
    try {
        const response = await api.login(email, password, options);
        const data = response && response.data || {};
        if ( options.silent && (data.error || data.success===false )){
             dispatch(finishLogin());
             return { success: false };
        }
        if ( options.verifyOnly ){
             dispatch(finishLogin());
             return response? response.data : null;
        }
        const {user, session} = data || {};
        if ( !user || !session )
              throw new Exception("Could not log in");
        dispatch(updateUser(user));
        dispatch(updateToken(session));
        saveSession(user, session);
    } catch (e) {
        dispatch(finishLogin(e));
    }
};

const requestForgotPassword = (email, callback) =>  async (dispatch, getState) => {
    if ( !email || !email.match(/@/) ){
          callback(false, "Please enter the email address you used to sign up " +
                          "with peace with food");
          return;
    }
    const wait = wait333ms();
    try {
        var response = await api.forgotpassword(email);
        await wait;
        if ( response.error )
              callback(false, response.error);
        else  callback(true);
    } catch (response)
    {
        await wait;
        callback(false, response.error);
    }
};

const changePassword = (securityKey, newPassword, email, callback) =>  async (dispatch, getState) => {
    try {
        var response = await api.changepassword(securityKey, newPassword, email);
        if ( response.error )
              callback(false, response.error);
        else  callback(true);
    } catch (response)
    {
        callback(false, response.error);
    }
};

const signup = (fullname, email, password, isCoach = false) => async (dispatch, getState) => {
    dispatch(startLogin());
    try {
        const {data} = (isCoach) ? await api.signupCoach(fullname, email, password) : await api.signupClient(fullname, email, password);
        const {user, session} = data || {};
        dispatch(updateUser(user));
        dispatch(updateToken(session));
        saveSession(user, session);
    } catch (e) {
        dispatch(finishLogin(e));
    }
};

const logout = () => async (dispatch, getState) => {
    await AsyncStorage.removeItem(KEY_USER);
    await AsyncStorage.removeItem(KEY_TOKEN);
    dispatch(updateUser(null));
    dispatch(updateToken(null));
};

const loadSession = () => async (dispatch) => {
    let user = await AsyncStorage.getItem(KEY_USER);
    let token = await AsyncStorage.getItem(KEY_TOKEN);
    if(user && token) {
        user = JSON.parse(user);
        token = JSON.parse(token);
        dispatch(updateUser(user));
        dispatch(updateToken(token));
        return true;
    }
    return false;
};

const saveSession = async (user, token) => {
    await AsyncStorage.setItem(KEY_USER, JSON.stringify(user));
    await AsyncStorage.setItem(KEY_TOKEN, JSON.stringify(token));
};

const createError = (data) => {
    return ERRORS["GenericError"];
};

const INITIAL_STATE = {
    isLoading: false,
    token: null,
    error: null,
    user: null,
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case TYPES.START_LOGIN:
            return {...state, isLoading: true};
        case TYPES.FINISH_LOGIN:
            const {error} = action;
            return {...state, isLoading: false, error};
        case TYPES.UPDATE_USER:
            const {user} = action;
            return {...state, user,isLoading: false,};
        case TYPES.UPDATE_TOKEN:
            const {token} = action;
            return {...state, token};
        case TYPES.CLEAR_ERRORS:
            return {...state, error: null};
        default:
            return state;
    }
};

const selector = state => state[stateKey];

export const getIsLoggingIn = createSelector(
    selector,
    state => state.isLoading
);

export const getIsLoggedIn = createSelector(
    selector,
    state => !!state.token
);

export const getToken = createSelector(
    selector,
    state => state.token
);

export const getError = createSelector(
    selector,
    state => state.error
);

export const selectors = {
    getIsLoggingIn,
    getIsLoggedIn,
    getToken,
    getError,
};

export const actions = {
    login,
    signup,
    requestForgotPassword,
    changePassword,
    logout,
    loadSession,
    clearErrors
  };
