import { createSelector } from 'reselect';
import * as api from 'PeaceWithFood/src/lib/api';
import Config from 'PeaceWithFood/app.json';
import Expo from 'expo';
import _ from 'underscore';

export const stateKey = 'connect';

function wait333ms(x) {
        return new Promise( resolve =>{   setTimeout(() => {
                                            resolve(x);
                                          }, 333 );
                                      }
        );
}
function wait500ms(x) {
        return new Promise( resolve =>{   setTimeout(() => {
                                            resolve(x);
                                          }, 500 );
                                      }
        );
}
const SHOW_DEBUG_MESSAGES = false;



const addCoach = (email, code, user_id, onFinish) => async (dispatch, getState) => {

        var response;
        var waitMinimumTime = wait500ms();
        try {
            //
            // See Controller.prototype.createClientCoachRelationship
            // Make the request:
            response = await api.createClientCoachRelationship(email, code, user_id);

        } catch (e) {
            response = e;
        }
        await waitMinimumTime; // for UI effect



        var result = {};
        response = response || {};
        // copy the response data
        _.extend( result, response.data );
        // add meta data
        _.extend( result,
                  {  success:  false,
                     email:    email,
                     code:     code,
                     user_id:  user_id,
                     message:  response.error || response.message || '',
                     originalResponse: response
                  });

        //
        //  Clean the data for quality UI responses
        //
        if ( !response.data ){
              if ( !result.message )
                // We chose this message because, most likely
                // this is a connection issue.
                result.message = "Please try again";
        } else {
              const relationship = result;
              const coach        = relationship.coach || {};
              if (  relationship.id
                 && relationship.client_id
                 && relationship.coach_id
                 && coach.id
                 && (result.email||result.code)
                 )
                 result.success = true;
        }



        if ( SHOW_DEBUG_MESSAGES )
        {
              console.log("RESPONSE", response);
              try {
                result.message = JSON.stringify(result);
              } catch (e) {
                try {
                  result.originalResponse = null;
                  result.message = JSON.stringify(result);
                } catch (e) {
                    console.log(response);
                    result.message = "Error lmzh7KD4";
                }
                if ( !result.message ){
                    result.message = "An unknown error was encountered (ID070)";
                }
              }
        }

        onFinish( result );
};



const getMyCoaches = ( user_id ) => async (dispatch, getState) => {
        var response = await api.getMyCoaches( user_id );
        return response && response.data && response.data.coaches;
};

const getCoachByEmail = ( email ) => async (dispatch, getState) => {
        var response = await api.getCoachByEmail( email );
        console.log(response && response.data);
        return response && response.data;
};

const removeCoachById = (user_id , coach_id) => async (dispatch, getState) => {
        const waitMinimumTime = wait500ms();
        var response = api.removeCoachById( user_id , coach_id );
        //
          response = await response;
          await waitMinimumTime;
        //
        console.log(response && response.status);
        return response && response.status;
};

export const actions = {
    addCoach,
    getMyCoaches,
    getCoachByEmail,
    removeCoachById
};
