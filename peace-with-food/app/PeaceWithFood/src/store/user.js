import {createSelector} from 'reselect';
import * as api from 'PeaceWithFood/src/lib/api';

export const stateKey = 'account';

const TYPES = {
    START_SETUP:        `${stateKey}/START_SETUP`,
    FINISH_SETUP:       `${stateKey}/FINISH_SETUP`,
    RESET_ACCOUNT:      `${stateKey}/RESET_ACCOUNT`,
    UPDATE_ACCOUNT:     `${stateKey}/UPDATE_ACCOUNT`,
};

const startSetup = () => ({
    type: TYPES.START_SETUP
});

const finishSetup = () => ({
    type: TYPES.FINISH_SETUP
});

const resetAccount = () => ({
    type: TYPES.RESET_ACCOUNT
});

const updateAccount = (data) => ({
    type: TYPES.UPDATE_ACCOUNT,data
});

const selector = state => state[stateKey];

const getUsers = () => async (dispatch, getState) => {
    await api.getUsers()
};

const updateClientById = (userid,fullname,email) => async (dispatch, getState) => {
    let jasonData={'first_name':fullname,};
    const {data} = await api.updateClient(userid,jasonData);
    //console.log('data',data)
    dispatch(updateAccount(data));

};

const getIsSettingUp = createSelector(
    selector,
    state => state.isSettingUp
);

const getIsSetupComplete = createSelector(
    selector,
    state => !!state.fullName
);

const updateClient = createSelector(
    selector,
    state => state.data
);

export const selectors = {
    getIsSettingUp,
    getIsSetupComplete,
    updateClient
};

export const INITIAL_STATE = {
    isSettingUp: false,
    isLoading: false,
};

export default (state = INITIAL_STATE, action) => {
   //console.log('action',action.type)
    switch(action.type) {
        case TYPES.START_SETUP:
            return {
                ...state,
                isSettingUp: true
            };
        case TYPES.FINISH_SETUP:
            return {
                ...state,
                isSettingUp: false
            };
        case TYPES.RESET_ACCOUNT:
            return INITIAL_STATE;
        case TYPES.UPDATE_ACCOUNT:
            return {...state, data:action.data};
        default:
            return state;
    }
};

export const actions = {
    startSetup,
    finishSetup,
    resetAccount,
    getUsers,
    updateClientById
};
