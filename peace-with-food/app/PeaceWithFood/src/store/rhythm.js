import { createSelector } from 'reselect';
import * as api from 'PeaceWithFood/src/lib/api';
import Config from 'PeaceWithFood/app.json';
import Expo from 'expo';
import moment from 'moment';

export const stateKey = 'rhythm';

const TYPES = {
    SET_SELECTED: `${stateKey}/SET_SELECTED`,
    GET_RHYTHMS : `${stateKey}/GET_RHYTHMS`,
    ADD_RHYTHMS : `${stateKey}/ADD_RHYTHMS`,
    UPDATE_RHYTHMS : `${stateKey}/UPDATE_RHYTHMS`,
    GET_CLIENT_RHYTHMS :`${stateKey}/GET_CLIENT_RHYTHMS`,
    GET_CLIENT_AVG_RHYTHMS :`${stateKey}/GET_CLIENT_AVG_RHYTHMS`,
    GET_SHARED_RHYTHM_INSTANCE :`${stateKey}/GET_SHARED_RHYTHM_INSTANCE`,
    GET_SHARED_RHYTHM_INSTANCES :`${stateKey}/GET_SHARED_RHYTHM_INSTANCES`,
    DELETE_SHARED_RHYTHM_INSTANCE :`${stateKey}/DELETE_SHARED_RHYTHM_INSTANCE`,
    DELETE_OTHERS_SHARED_RHYTHM_INSTANCE :`${stateKey}/DELETE_OTHERS_SHARED_RHYTHM_INSTANCE`,
    GET_OTHERS_SHARED_RHYTHM_INSTANCE:`${stateKey}/GET_OTHERS_SHARED_RHYTHM_INSTANCE`,
    GET_VIDEOS:          `${stateKey}/GET_VIDEOS`,
};

const allRhythms = (rhythm) => ({type: TYPES.GET_RHYTHMS,rhythm});
const createRhythmValue = (data) => ({type: TYPES.ADD_RHYTHMS,data});
const updateRhythmValue = (rhythmDetails) => ({type: TYPES.UPDATE_RHYTHMS,rhythmDetails});
const getClientRhythmValue = (rhythmValue) => ({type: TYPES.GET_CLIENT_RHYTHMS,rhythmValue});
const getClientMonthAvgRhythmValue = (avgRhythmValue) => ({type: TYPES.GET_CLIENT_AVG_RHYTHMS,avgRhythmValue});
const getSharedRhythmInstanceValue = (instanceValue) => ({type: TYPES.GET_SHARED_RHYTHM_INSTANCE,instanceValue});
const getSharedRhythmInstancesValue = (instanceValue) => ({type: TYPES.GET_SHARED_RHYTHM_INSTANCES,instanceValue});
const deleteSharedRhythmInstanceValue = (instanceValue) => ({type: TYPES.DELETE_SHARED_RHYTHM_INSTANCE,instanceValue});
const deleteOthersSharedRhythmInstanceValue = (instanceValue) => ({type: TYPES.DELETE_OTHERS_SHARED_RHYTHM_INSTANCE,instanceValue});
const getOthersSharedRhythmInstancesValue = (instanceValue) => ({type: TYPES.GET_OTHERS_SHARED_RHYTHM_INSTANCE,instanceValue});
const getVideosData = (videoDetails) => ({
    type: TYPES.GET_VIDEOS,videoDetails
});
const setSelected = (rhythm) => ({
    type: TYPES.SET_SELECTED,
    rhythm
});

const getRhythms = () => async (dispatch, getState) => {
    //console.log('rhythm');

    const {data} = await api.rhythmOptions();
    //console.log('rhythmvalue',data)
    dispatch(allRhythms(data.rhythms));

};

const createRhythm = (user_id,value) => async (dispatch, getState) => {
    const {data} = await api.createRhythms(user_id,value);
    //console.log('data',data)
    dispatch(createRhythmValue(data));

};
const updateRhythmDetails = (user_id,rhythm_id,thoughts,options,messageId) => async (dispatch, getState) => {

    const {data} = await api.updateRhythmsDetails(user_id,rhythm_id,thoughts,options,messageId);
    console.log('data',data);
    dispatch(updateRhythmValue(data));

};


const getClientRhythm = (user_id, start_date , end_date) => async (dispatch, getState) => {

    const rhythmValue = await api.getClientRhythm(user_id , start_date , end_date);
   //console.log('rhythmValue',rhythmValue)
    dispatch(getClientRhythmValue(rhythmValue.data.entries));

};

const getClientMonthAvgRhythm = (user_id, start_date , end_date) => async (dispatch, getState) => {
    const avgRhythmValue = await api.getClientMonthAvgRhythm(user_id , start_date , end_date);
    console.log('rhythmValue',avgRhythmValue)
    dispatch(getClientMonthAvgRhythmValue(avgRhythmValue.data.entries));

};

const createSharedRhythms = (user_id, time_key, number_of_days, coach_id) => async (dispatch, getState) => {
    var begin_epoch = null, end_epoch = null;
    var b                 , e;
    const m = moment().hours(0);
    const n = number_of_days;
    const now      = moment();
    const sunday   = moment().day(0).hours(0);
    const midnight = m.add( 1, 'days').clone();
    switch (time_key){
      case 'TODAY':         b = m ;                      e = midnight;      break;
      case 'YESTERDAY':     b = m.subtract(1, 'days');   e = m;             break;
      case 'DAYS':          b = m.subtract(n-1, 'days'); e = now;           break;
      // case 'LAST_WEEK':  b = m.day(0).subtract(14,'days'); e = sunday;   break;
      case 'LAST_WEEK':     b = m.subtract(7,'days');    e = now;           break;
      case 'MONTH_AVERAGE': b = m.date(1); e = now;                         break;
      default:
        // ignore it
        return;
    }
    var title;
    var n_text;
    switch (parseInt(n)){
      case 1:  n_text = "one";   break;
      case 2:  n_text = "two";   break;
      case 3:  n_text = "three"; break;
      case 4:  n_text = "four";  break;
      case 5:  n_text = "five";  break;
      case 6:  n_text = "six";   break;
      case 7:  n_text = "seven"; break;
      default: n_text = n; break
    }
    switch (time_key){
      case 'TODAY':         title = "one day"; break;
      case 'YESTERDAY':     title = "one day"; break;
      case 'DAYS':          title = n_text+" days"; break;
      case 'LAST_WEEK':     title = "one week"; break;
      case 'MONTH_AVERAGE': title = "month avg."; break;
    }

    begin_epoch =  +( b.hours(0).minute(0).second(0).millisecond(0) );
    if ( !e.isSame(now) )
    end_epoch   =  +( e.hours(0).minute(0).second(0).millisecond(0) );
    else
    end_epoch   =  +now;

    //
    // make the request
    const response  = await api.shareRhythms( coach_id, title, begin_epoch, end_epoch );
    const instance = response.data;
    dispatch(getSharedRhythmInstanceValue(instance));
    return instance;


};
const getSharedRhythmInstances = (coach_id) => async (dispatch, getState) => {
    const response  = await api.getSharedRhythmInstances(coach_id);
    const instances = response.data;
    dispatch(getSharedRhythmInstancesValue(instances));
    return instances;

};

const deleteSharedRhythmInstance = (entry_id, coach_id) => async (dispatch, getState) => {
    const response = await api.deleteSharedRhythmInstance(entry_id, coach_id);
    const instance = response.data;
    dispatch(deleteSharedRhythmInstanceValue(instance));
    return instance;
}

const deleteOthersSharedRhythmInstance = (entry_id, coach_id) => async (dispatch, getState) => {
    const response = await api.deleteOthersSharedRhythmInstance(entry_id, coach_id);
    const instance = response.data;
    dispatch(deleteOthersSharedRhythmInstanceValue(instance));
    return instance;
}

const getOthersSharedRhythmInstances = (coach_id) => async (dispatch, getState) => {
    const othersResponse  = await api.getOthersSharedRhythmInstances(coach_id);
    const othersInstances = othersResponse.data;
    dispatch(getOthersSharedRhythmInstancesValue(othersInstances));
    return othersInstances;

};
const getVideos = () => async (dispatch, getState) => {
    const {data} = await api.getVideos();

    dispatch(getVideosData(data));

};


export const actions = {
    setSelected,
    getRhythms,
    createRhythm,
    getClientRhythm,
    getClientMonthAvgRhythm,
    getSharedRhythmInstances,
    deleteSharedRhythmInstance,
    deleteOthersSharedRhythmInstance,
    getOthersSharedRhythmInstances,
    createSharedRhythms,
    updateRhythmDetails,
    getVideos
};

const selector = state => state[stateKey];

const INITIAL_STATE = {
    selected: null,
    rhythm: []
};

const getSelectedRhythm = createSelector(
    selector,
    state => state.selected
);

export const getRhythmOptions = createSelector(
    selector,
    state => state.rhythm
);

export const addRhythmOptions = createSelector(
    selector,
    state => state.data
);
export const setRhythmDetails = createSelector(
    selector,
    state => state.rhythmDetails
);


export const getRhythmByClient = createSelector(
    selector,
    state => state.rhythmValue
);

export const getClientAvgRhythm = createSelector(
    selector,
    state => state.avgRhythmValue
);

export const getSharedInstances = createSelector(
    selector,
    state => state.instances
);
export const getOthersSharedInstances = createSelector(
    selector,
    state => state.othersInstances
);

export const deleteSharedRhythmInstances = createSelector(
    selector,
    state => state.deleteRhythmInstance
)

export const deleteOthersSharedRhythmInstances = createSelector(
    selector,
    state => state.deleteOthersRhythmInstance
)

const getVideosInfo = createSelector(
    selector,
    state => state.videoDetails
);

export const selectors = {
    getSelectedRhythm,
    getRhythmOptions,
    addRhythmOptions,
    getRhythmByClient,
    getClientAvgRhythm,
    getSharedInstances,
    getOthersSharedInstances,
    deleteSharedRhythmInstances,
    deleteOthersSharedRhythmInstances,
    setRhythmDetails,
    getVideosInfo

};

export default (state = INITIAL_STATE, action) => {
  //console.log('action-fgf',action)
    switch(action.type) {
        case TYPES.SET_SELECTED:
            return {
                ...state,
                selected: action.rhythm
            };
            case TYPES.GET_RHYTHMS:
                return {...state, rhythm:action.rhythm};
            case TYPES.ADD_RHYTHMS:
                const {data} = action;
                return {...state, data};
            case TYPES.UPDATE_RHYTHMS:
                const {rhythmDetails} = action;
                return {...state, rhythmDetails};
            case TYPES.GET_CLIENT_RHYTHMS:
                const {rhythmValue} = action;
                return {...state, rhythmValue};
            case TYPES.GET_CLIENT_AVG_RHYTHMS:
                const {avgRhythmValue} = action;
                return {...state, avgRhythmValue};
            case TYPES.GET_SHARED_RHYTHM_INSTANCE:
                const {instance} = action;
                return {...state, instance};
            case TYPES.GET_OTHERS_SHARED_RHYTHM_INSTANCE:
                const {othersInstances} = action;
                return {...state, othersInstances};
            case TYPES.GET_SHARED_RHYTHM_INSTANCES:
                const {instances} = action;
                return {...state, instances};
            case TYPES.DELETE_SHARED_RHYTHM_INSTANCE:
                const {deleteRhythmInstance} = action;
                return{...state, deleteRhythmInstance};
            case TYPES.DELETE_OTHERS_SHARED_RHYTHM_INSTANCE:
                const {deleteOthersRhythmInstance} = action;
                return{...state, deleteRhythmInstance};
            case TYPES.GET_VIDEOS:
                const {videoDetails} = action;
                return {...state, videoDetails};

        default:
            return state;
    }
};
