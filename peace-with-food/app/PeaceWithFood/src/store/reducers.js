import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form';
import authReducer, {stateKey as authKey} from 'PeaceWithFood/src/store/auth';
import userReducer, {stateKey as userKey} from 'PeaceWithFood/src/store/user';
import rhytmReducer, {stateKey as rhythmKey} from 'PeaceWithFood/src/store/rhythm';

export const makeRootReducer = (asyncReducers) => {
    const appReducer = combineReducers({
        form,
        [authKey]: authReducer,
        [userKey]: userReducer,
        [rhythmKey]: rhytmReducer,
        ...asyncReducers
    });

    // Clear state on sign out
    return (state, action) => {
        return appReducer(state, action);
    };
};

export const injectReducer = (store, {key, reducer}) => {
    store.asyncReducers[key] = reducer;
    store.replaceReducer(makeRootReducer(store.asyncReducers));
};

export default makeRootReducer;
