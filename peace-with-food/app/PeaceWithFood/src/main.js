import React from 'react';
import {
    ActivityIndicator,
    Platform,
    StatusBar,
    StyleSheet,
    View,
    AsyncStorage,
    Linking,
} from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';
import {loadFromLocalStorage, saveToLocalStorage} from './lib/persistence';
import {Provider} from 'react-redux';
import {Font} from 'expo';
import createRoutes from './routes';
import createStore from './store/createStore';
import styles from './styles';
import { Notifications,Permissions,Constants } from 'expo';
import * as api from './lib/api';
import * as UrlHandler from './lib/urls';


class App extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            store: null,
            AppRoutes: null,
            isInitialized: false,
        };
    }

    async componentWillMount() {
        const initialState = await loadFromLocalStorage();
        const store = createStore(initialState);
        const AppRoutes = createRoutes(store);
        store.subscribe(saveToLocalStorage(store));
        api.setInterceptors(store);
        this.setState({
            store,
            AppRoutes,
            isInitialized: true
        });
      //  console.log('initialState',initialState);
        AsyncStorage.setItem('CheckedIn','no');
        AsyncStorage.setItem('CheckedInScreen','no');
        const deviceDate =new Date();
          AsyncStorage.setItem('DeviceTime','sun');
          //AsyncStorage.setItem('CheckInTime','00:30'); commented as not want to set default

       /*notification */
       let t = new Date();
       t.setSeconds(t.getSeconds() + 10);
       console.log('time',t.getSeconds());
       // const schedulingOptions = {
       //     time: (new Date()).getTime() + 1000, // (date or number) — A Date object representing when to fire the notification or a number in Unix epoch time. Example: (new Date()).getTime() + 1000 is one second from now.
       //     repeat: 'hour',
       //
       //   };
       // const localNotification = {
       //       title: 'PWF',
       //       body: 'Please Check in', // (string) — body text of the notification.
       //       ios: { // (optional) (object) — notification configuration specific to iOS.
       //         sound: true // (optional) (boolean) — if true, play a sound. Default: false.
       //       },
       //   android: // (optional) (object) — notification configuration specific to Android.
       //       {
       //         sound: true, // (optional) (boolean) — if true, play a sound. Default: false.
       //         //icon (optional) (string) — URL of icon to display in notification drawer.
       //         icon:'../../images/blue_logo_icon.png',
       //         //color (optional) (string) — color of the notification icon in notification drawer.
       //         priority: 'high', // (optional) (min | low | high | max) — android may present notifications according to the priority, for example a high priority notification will likely to be shown as a heads-up notification.
       //         sticky: false, // (optional) (boolean) — if true, the notification will be sticky and not dismissable by user. The notification must be programmatically dismissed. Default: false.
       //         vibrate: true // (optional) (boolean or array) — if true, vibrate the device. An array can be supplied to specify the vibration pattern, e.g. - [ 0, 500 ].
       //         // link (optional) (string) — external link to open when notification is selected.
       //       }
       //     };

           AsyncStorage.getItem('DayEndTime').then((value) =>{
               if(value){
                  //console.log('DayEndTime',value);
                  //deviceDate.toLocaleTimeString().slice(0,5);
                  //console.log(deviceDate.toLocaleTimeString().slice(0,5));
                  if(deviceDate.toLocaleTimeString().slice(0,8) == value){
                    AsyncStorage.setItem('DeviceTime','sun');
                    Notifications.cancelAllScheduledNotificationsAsync();
                  }

               }

             });
           // AsyncStorage.getItem('DeviceTime').then((value) =>{
           //   if(value){
           //    if(value =='moon'){
           //      Notifications.scheduleLocalNotificationAsync(localNotification, schedulingOptions);
           //    }
           //   }
           // });
           //     setInterval(function(){
           //       AsyncStorage.getItem('DeviceTime').then((value) =>{
           //         if(value){
           //          if(value =='moon'){
           //            AsyncStorage.getItem('CheckedIn').then((val) =>{
           //              if(val=='no'){
           //            Notifications.scheduleLocalNotificationAsync(localNotification, schedulingOptions);
           //          }
           //          });
           //          }
           //         }
           //       });
           //   //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
           //   Alert.alert("Alert Shows After 5 Seconds of Delay.")
           //   console.log("Alert Shows After 5 Seconds of Delay.")
           //
           // }, 12000);


    }

    async componentDidMount() {
        let result = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        if (Constants.lisDevice && resut.status === 'granted') {
        // alert('Notification permissions granted.')
        }
        Font.loadAsync({
          'Sarabun-Regular': require('./components/fonts/Sarabun-Regular.ttf'),
          'Sarabun-Bold': require('./components/fonts/Sarabun-Bold.ttf'),
        });

    }

    render() {
        var promise = Linking.getInitialURL();
        promise.then(function(url){
            UrlHandler.handle(url);
        });
        Linking.addEventListener('url', (url) => {
            UrlHandler.handle(url);
        });

        const {
            store,
            AppRoutes,
            isInitialized,
        } = this.state;

        if(isInitialized) {
            return (
                <Provider store={store}>
                    <View style={styles.container}>
                        <View style={styles.statusBar}>
                            <StatusBar
                                backgroundColor='transparent'
                                barStyle="dark-content"
                                translucent
                            />
                        </View>
                        <AppRoutes {...this.props} />
                    </View>
                </Provider>
            );
        }else{
            return (
                <View style={[styles.container, styles.center]}>
                    <ActivityIndicator/>
                </View>
            );
        }
    }
}

export default App;
