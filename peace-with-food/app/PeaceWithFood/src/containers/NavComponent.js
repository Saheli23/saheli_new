import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';

class NavComponent extends Component {
    render() {
        return null;
    }

    resetRoute = (routeName) => {
        const {navigation} = this.props;
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName })
            ]
        });
        navigation.dispatch(resetAction);
    };
}

export default NavComponent;