import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';
import {Images} from 'PeaceWithFood/src/components/icons';

const localStyle = StyleSheet.create({
    logoText: {
        textAlign: 'center',
        fontSize: 30
    }
});

const LogoIconBlue = () => (
    <View>
        <Image
            source={Images.blueLogoIcon}
        />
    </View>
);

export default LogoIconBlue;
