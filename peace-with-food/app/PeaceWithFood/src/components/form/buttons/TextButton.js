import React from 'react';
import {ActivityIndicator, StyleSheet, Text, TouchableOpacity} from 'react-native';

const localStyle = StyleSheet.create({
    common: {
        height: 50,
        paddingLeft: 25,
        paddingRight: 25,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#44A7E4',
        flex: 1
    },
    primary: {
        backgroundColor: '#44A7E4',
    },
    normal: {

    },
    label: {
        fontSize: 18,
    },
    labelPrimary: {
        color: '#FFF',
    },
    labelNormal: {
        color: '#44A7E4',
    }
});

const TextButton = ({
    label,
    primary,
    style,
    labelStyle,
    onPress,
    isLoading,
    loadingColor,
}) => (
    <TouchableOpacity
        onPress={onPress}
        disabled={isLoading}
        style={[
            localStyle.common,
            primary ? localStyle.primary : localStyle.normal,
            style
        ]}
    >
        <ActivityIndicator
            style={{marginLeft: -24, marginRight: 4}}
            color={isLoading ? '#FFF' : loadingColor ? loadingColor : primary ? '#44A7E4' : '#2F3033'}
        />
        <Text style={[
            localStyle.label,
            primary ? localStyle.labelPrimary : localStyle.labelNormal,
            labelStyle,
        ]}>{label}</Text>
    </TouchableOpacity>
);

export default TextButton;