import React from 'react';
import {StyleSheet, TouchableOpacity, Text,Image} from 'react-native';
import {Images} from 'PeaceWithFood/src/components/icons';

const styles = StyleSheet.create({
    button: {
        flex: 1,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

const BackButton = ({
    color,
    onPress
}) => (
    <TouchableOpacity
        style={styles.button}
        onPress={onPress}
    >
        <Image
            source={Images.backIcon}
        />
    </TouchableOpacity>
);

export default BackButton;
