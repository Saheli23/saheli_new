import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {Field} from 'redux-form';

const localStyle = StyleSheet.create({
    row: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'transparent',
    },
    column: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'transparent',
    },
    textInput: {
        color: '#CCCCD4',
        fontSize: 17,
        borderColor: '#D0DCD9',
        borderWidth: 1,
        paddingVertical: 10,
        paddingHorizontal: 6
    },
    errorText: {
        color: '#EF4D40',
        fontSize: 13,
        justifyContent: 'center',
        textAlign: 'center',
        marginTop: 4
    },
    iconContainer: {
        position: "absolute",
        right: 0,
        top: 2,
    }
});

const CustomTextInput = ({
    input: {value, onChange},
    meta: {touched, error: validationError},
    autoFocus,
    editable = true,
    error = "",
    numberOfLines = 1,
    placeholder,
    placeholderTextColor,
    selectionColor,
    secureTextEntry,
    style,
    keyboardType,
    returnKeyType
}) => (
    <View style={localStyle.row}>
        <View style={localStyle.column}>
            <TextInput
                onChangeText={(text) => onChange(text)}
                autoCapitalize="none"
                autoFocus={autoFocus}
                editable={editable}
                numberOfLines={numberOfLines}
                style={[localStyle.textInput, style]}
                placeholder={placeholder}
                placeholderTextColor={placeholderTextColor}
                secureTextEntry={secureTextEntry}
                selectionColor={selectionColor}
                multiline={numberOfLines > 1}
                underlineColorAndroid="transparent"
                value={value}
                keyboardType={keyboardType}
                returnKeyType={returnKeyType}
            />
            <Text style={localStyle.errorText}>{error || (touched && validationError)}</Text>
        </View>
    </View>
);

const TextInputField = ({
    fieldName,
    editable,
    autoFocus,
    error,
    placeholder,
    secureTextEntry,
    style,
    placeholderTextColor = "#999",
    selectionColor = "#999",
    numberOfLines = 1,
    keyboardType,
    returnKeyType
}) => (
    <Field
        name={fieldName}
        component={CustomTextInput}
        autoFocus={autoFocus}
        editable={editable}
        error={error}
        numberOfLines={numberOfLines}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        secureTextEntry={secureTextEntry}
        selectionColor={selectionColor}
        style={style}
        keyboardType={keyboardType}
        returnKeyType={returnKeyType}
    />
);

export default TextInputField;
