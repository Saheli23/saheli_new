import axios from 'axios';
import Config from 'PeaceWithFood/app.json';
import { selectors as auth } from 'PeaceWithFood/src/store/auth';

const client = axios.create({
    baseURL: Config.app.apiUrl
});

export const setInterceptors = (store) => {
    client.interceptors.request.use(config => {
        try {
            const token = auth.getToken(store.getState());
            if (token && token.token) {
                config.headers['Authorization'] = `Bearer ${token.token}`;
            }
        } catch (e) {
            console.log(e);
        }
        return config;
    });

    client.interceptors.response.use(response => response, error => {
        let message = error.message;
        const {response} = error;
        if(response && response.data) {
            const {error: responseError} = response.data;
            if (responseError && responseError.body) {
                message = responseError.body.Message;
            }
        }
        console.log( 'Failed request:'
                   ,  error.request &&error.request.responseURL
                   ,  error.response&&error.response.data );
        if(error.response && error.response.data !== undefined){
        return Promise.reject(error.response.data);
      }
    });
};

export const login = (email, password, options) => {
    options = options || {};
    return client.post('users/session', {email, password, silent:options.silent })
    }

export const forgotpassword = (email) =>
    client.post('users/forgotpassword', {email, returnurl:Expo.Constants.linkingUri})

export const changepassword = (oldpassword, password, email) =>
    client.post('users/changepassword', {oldpassword, password, email})

export const getCoachByEmail = (email) =>
    client.get('users/coach/email/' + email );

export const removeCoachById = (user_id , coach_id ) =>
    client.delete('/users/client/id/'+user_id+'/coach/id/'+coach_id );

export const getMyCoaches = (user_id) =>
    client.get('users/client/id/' + user_id + '/coaches' );

export const createClientCoachRelationship = (coachemail, clientcode, user_id, coach_id/*optional*/) =>
    client.post('users/client/coach', { "coachemail":coachemail,
                                        "code":      clientcode,
                                        "clientid":  user_id,
                                        "coachid":   coach_id
                                      });

export const signupClient = (first_name,email, password) =>
    client.post('users/client', {first_name,email, password});

export const signupCoach = (fullname,email, password) =>
    client.post('users/coach', {fullname,email, password});

export const updateClient = (userid, data) =>
    client.post('/users/id/'+userid, data);

export const rhythmOptions = () =>
    client.get('rhythms');
  //   client.get('rhythms').then(function(response) {
  //   // console.log(response.data);
  //   // console.log(response.status);
  //   // console.log(response.statusText);
  //   // console.log(response.headers);
  //   // console.log(response.config);
  // });
export const createRhythms = (user_id,value) =>
    client.post('rhythms',{user_id,value});



export const getClientRhythm = (user_id , start_date , end_date) =>{

    var d = new Date();
    var n = d.getTimezoneOffset();
    var sn = -n*60; //convert to seconds
    var sec_num = parseInt(sn, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);
    if (hours   < 10){  if(hours > 0 ) {hours   = "+0"+hours;}else{ hours = "-0"+ (Math.sqrt( hours * hours ) ); } }else{ hours = "+" + hours; }
    if (minutes < 10) {minutes = "0"+minutes;}
    var timezone_offset =  hours+':'+minutes;

    if ( start_date && end_date )
    {
        r  =   client.get('/rhythms/client/id/' +  user_id
                                                + '?start_date='+ start_date
                                                + '&end_date='+ end_date
                                                + '&timezone_offset=' + encodeURIComponent(timezone_offset)
                                                + '&limit=1000');
    }
    else if ( start_date && ! end_date )
    {
        r  =   client.get('/rhythms/client/id/' +  user_id
                                                + '?start_date='+ start_date
                                                + '&timezone_offset=' + encodeURIComponent(timezone_offset)
                                                + '&limit=1000');
    }
    else
    {
        r  =   client.get('/rhythms/client/id/' +  user_id
                                                + '&timezone_offset=' + encodeURIComponent(timezone_offset)
                                                + '&limit=1000');
    }
    //console.log('getClientRhythm', r );
    return r;
}



export const updateRhythmsDetails = (user_id,rhythm_id,thoughts,options,message_id) =>
    client.post('rhythms/details',{user_id,rhythm_id,thoughts,options,message_id});

export const shareRhythms =           ( coach_id, title, begin_epoch, end_epoch ) =>
    client.post('rhythms/share/dates',{ coach_id, title, begin_epoch, end_epoch });

export const getSharedRhythmInstances = (coach_id) =>
    client.get('/rhythms/share/instances/coach/' + coach_id );

export const deleteSharedRhythmInstance = (entry_id,coach_id) =>
    client.delete('/rhythms/share/entry/id/' + entry_id + '/coach/id/' + coach_id);

export const deleteOthersSharedRhythmInstance = (entry_id,coach_id) =>
    client.delete('/rhythms/share/entry/id/' + entry_id + '/coach/id/' + coach_id);

// export const getOthersSharedRhythmInstances = (coach_id) =>
//     client.get('http://34.224.170.71:3001/rhythms/share/instances/client/' + coach_id );

export const getOthersSharedRhythmInstances = (coach_id) =>
    client.get('/rhythms/share/instances/client/' + coach_id );

export const getClientMonthAvgRhythm = (user_id , start_date , end_date) =>{
   if(start_date !='' && (end_date !='') ){
    return client.get('/rhythms/client/id/'+ user_id+ '/average?start_date='+ start_date +'&end_date='+ end_date);
   }
   else if(start_date !='' && end_date ==''){
     return client.get('/rhythms/client/id/'+ user_id+ '/average?start_date='+ start_date);
   }
   else{
     return client.get('/rhythms/client/id/'+ user_id +'/average');
   }
}

export const getVideos = () =>
    client.get('https://cms.hellopeacewithfood.com/json/videos?_format=json');
