import debounce from 'debounce';
import {AsyncStorage} from 'react-native';
import {stateKey, INITIAL_STATE} from 'PeaceWithFood/src/store/user';

const KEY_ACCOUNT = `@pwf/state/${stateKey}`;

export const saveToLocalStorage = (store) => {
    return debounce(async () => {
        const state = store.getState();
        const account = JSON.stringify(state[stateKey]);
        console.log("Saving account", state[stateKey]);
        await AsyncStorage.setItem(KEY_ACCOUNT, account);
    }, 1000);
};

export const loadFromLocalStorage = async () => {
    let initialState = {};
    try {
        const account = await AsyncStorage.getItem(KEY_ACCOUNT);
        console.log("Loading account", JSON.parse(account));
        initialState[stateKey] = JSON.parse(account) || INITIAL_STATE;
    }catch(e) {
        console.log("No state found on AsyncStorage");
        initialState[stateKey] = INITIAL_STATE;
    }
    return initialState;
};