import React from 'react';
import {
    Alert

} from 'react-native';
import _ from 'underscore';
import qs from 'qs';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';



const resetHandlers = [];
var incomingData_passwordReset = null;
var ignoreResetRequests = false;


export const cancelAllResetRequests = () => {
        ignoreResetRequests = true;
}





export const handleResetPasswordUrl = (email, key) => {

        if ( ignoreResetRequests )
             return;

        incomingData_passwordReset = {email, key};

        _.each(resetHandlers, (handler) => handler( incomingData_passwordReset.email
                                                  , incomingData_passwordReset.key ));

}





export const onResetPassword = (handler, clear_buffer) => {
        if ( clear_buffer )
             resetHandlers = [];
        if ( ignoreResetRequests )
             return;

        if ( incomingData_passwordReset ){
             handler( incomingData_passwordReset.email, incomingData_passwordReset.key );
        } else {
             resetHandlers.push( handler );
        }
};





export const handle = _.debounce( (url) => {
         let queryString = (url.url) ? '' : url.replace(Expo.Constants.linkingUri, '') ;
         if ( !queryString )
               return;

         let data = qs.parse(queryString.replace(/\?/, "&"));

         if ( data.resetpassword ){
            // reset/forgot password
            handleResetPasswordUrl(data.email, data.key);
         } else {
            console.log("No handler for url: " + url); // do not delete this code
         }


}, 1500, true );
