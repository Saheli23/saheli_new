import {
    Alert as ReactAlert
} from 'react-native';
import _ from 'underscore';


function Util() {}


var alertWait    = false;
var alertTitle   = null;
var alertMessage = null;
var alertOther   = null;

const triggerAlert = _.debounce(() => {
        
        if (alertOther)        ReactAlert.alert(alertTitle, alertMessage, alertOther);
        else if (alertMessage) ReactAlert.alert(alertTitle, alertMessage);
        else                   ReactAlert.alert(alertTitle);
        alertWait    = false;
        alertTitle   = null;
        alertMessage = null;
        alertOther   = null;
        
}, 333, false);



const generic = (string) => {

        if (!string )
             return true;
        if ( string === 'undefined' )
             return true;
        if ( string.match('undefined') )
             return true;
        if ( string.match('Please try again') )
             return true;
        if ( string.match('An unknown error was encountered') )
             return true;
        if ( string.match('Something went wrong') )
             return true;
        
        return false;
}


const alert = (title, message, other) => {
        if ( !alertOther  &&  !alertWait
           || (generic(alertTitle) && generic(alertMessage))
           )
        {
            if ( !message &&  title && title.length > 62 ){
                  message =  title;
                  title   = "Alert";
            }
            if (  message && !title && message.length > 62 ){
                  message =  message;
                  title   = "Alert";
            }
            alertWait    = true;
            alertTitle   = title;
            alertMessage = message;
            alertOther   = other;
        }
        triggerAlert();
};

Util.prototype.Alert = alert;
Util.prototype.Alert.alert = alert;







const instance = new Util();

export const Util  = instance;
export const Alert = instance.Alert;