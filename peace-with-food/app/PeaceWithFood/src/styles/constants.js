'use strict';

export const Colors = {
    TRANSPARENT: 'rgba(0,0,0,0)',
    WHITE: '#FFF',
    //RYTHM: '#EFF3F4',
    //RYTHM: '#e0e2e2',
    RYTHM:   '#EFF3F4',
    // TRACK: '#38ABA3',
    TRACK: '#00bcec',
    // EDUCATE: '#A05C97',
    EDUCATE:'#eac435',
    // SHARE: '#E5A04D',
    SHARE: '#f17d42',
    // HISTORY: '#80D0C7',
    HISTORY:'#2799bb',
    // SETTINGS: '#6F86D6',
    SETTINGS:'#add1e0',
    // EVENING: '#384A54'
    EVENING:'#384a54',
    //SETTINGSBACKGROUND:'#00bcec',
    SETTINGSBACKGROUND:'#2799BB',
    MORNING:'#eac435'
};

export const Sizes = {
    SCREEN_PADDING: 10
};
