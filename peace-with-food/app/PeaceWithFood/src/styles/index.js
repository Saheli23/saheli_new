import {
    Platform,
    StatusBar,
    StyleSheet
} from 'react-native';

import { Colors } from './constants';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    statusBar: {
        height: (Platform.OS === 'ios' ? 0 :  StatusBar.currentHeight),
        backgroundColor: '#2F3033',
    },
    headerTitleStyle: {
        fontSize: 17,
        color: Colors.WHITE
    }
});

