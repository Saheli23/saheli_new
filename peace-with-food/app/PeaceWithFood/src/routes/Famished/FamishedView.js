import React ,{Component} from 'react';
import Expo from 'expo';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    TextInput,
    AsyncStorage,
    Alert

} from 'react-native';


import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

//import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';

const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#00bded'
    },
    section: {
        flex: 1
    },
    bottomSection: {
        backgroundColor: '#00bded',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? height/10 : 25),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#009fc6',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,
        marginTop:(Platform.OS === 'ios' ? height/8 : height/6 ),
        alignItems: 'center',
        justifyContent:'center',
        flexDirection:'column'
      },
      answer:{
        flex:0.9,
        color:'white',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#a2a4a5',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: '#ebf6f9',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
      checkPanel:{
        // alignItems: 'center',
        // justifyContent:'center',
        paddingTop:20,
        paddingLeft:20,
        // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
        flexDirection:'row'
      },
      sharedBlock:{
        //padding:(Platform.OS === 'ios' ? 40 : 10),
        alignItems: 'center',
        justifyContent:'center',
        marginTop:(Platform.OS === 'ios' ? height/45 : 10),
        height:(Platform.OS === 'ios' ? ((height < 700) ? height/7 :height/5) : height/6),
        width:width/1.1,
        flexDirection:'row',
        flexWrap: "wrap"
      },
      timeText:{
        color:'white',
        fontSize:(Platform.OS === 'ios' ? 30 : 30),
        //fontWeight:'500',
        fontFamily:'Sarabun-Bold'
      },
      shareWithCoach:{
        flexDirection:'row',
        backgroundColor:'#0ecbf3',
        width:300,
        height:40,
        padding:10,
        //marginTop:10,
        justifyContent:'space-between'
      },

});



class FamishedView extends Component {

  constructor(props){
    super(props);
    this.state={
      checkin_recorded:false,
      playedSound:false

    }

  }

  _renderModalContent = (onModalClose) => (
      <View style={localStyle.modalContent}>
        <View>
         <View style={{flexDirection:'row',justifyContent:'flex-end'}}>

          <TouchableOpacity onPress={onModalClose}>
            <Image source={Images.closeIcon} />
          </TouchableOpacity>
        </View>

                  <View style={{marginTop:15}}>
                      <Text style={[localStyle.buttonText,{fontWeight:'500',marginBottom:5,marginLeft:10}]}>
                         Thoughts for PwF Connect:
                       </Text>
                       <TextInput
                           multiline={true}
                           numberOfLines={5}
                           style={{borderWidth:2,borderColor:'#2799bb',width:width/1.3,height:height/4}}
                           placeholder="Type thoughts/questions here (share in PwF Connect if you choose.) Find all of your notes in the Summary by tapping the yellow dots."
                           placeholderTextColor="#446a7a"

                       />

                   </View>
                   <View style={{flexDirection:'row',marginBottom:5,marginTop:10,justifyContent:'space-between'}}>
                     <Text style={[localStyle.buttonText,{fontWeight:'500',flex:1}]}><Text style={{color:'#ea783f'}}>Sick?</Text> tell me more</Text>
                     <Image style={{marginRight:5,marginTop:2}} source={Images.checkMarkIcon} />


                   </View>
              </View>
        </View>
    );



  playSound(healthStateId){
      if ( ! healthStateId )
             return;
      const self = this;
      (async function(healthStateId){

        if ( self.state.playedSound )
             return;
        self.state.playedSound = true;
        const animation_length = 783;
        setTimeout(async() => {

          const soundObject = new Expo.Audio.Sound();
          if      (healthStateId < 4)
                   await soundObject.loadAsync(require('../../sound/glock-note-F1.wav'));
          else if (healthStateId > 6)
                   await soundObject.loadAsync(require('../../sound/glock-note-F1.wav'));
                   // when in the 'zone'
          else     await soundObject.loadAsync(require('../../sound/glock-note-F1.wav'));
          // play sound
          await soundObject.playAsync();

        }, animation_length );

      })(healthStateId);
  }



  render (){
    let {

        onHomePress,
        onAddCoachesPress,
        onReadyToBeginPress,
        visibleModal,
        onModalClose,
        healthState,
        healthStateId
    } = this.props;

    let healthStateArray=[['Famished', 'Starving', 'Very Hungry'], ['Ready', 'Neutral', 'Full & Satisfied'], ['Overly Full', 'Stuffed', 'Sick']];
    let color=['#eac435', '#1e7891', '#e87f47'];
    let images=[Images.yellowCheckIcon,Images.greenCheckIcon,Images.whiteCheckIcon];

    let getChangeVal = healthStateArray.map(function(e){
	                 return e.indexOf(healthState) > -1;
                }).indexOf(true);


    this.playSound(healthStateId);

    return (<View style={styles.container}>
        <StatusBar/>
        <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>
            <Modal isVisible={visibleModal}>
             {this._renderModalContent(onModalClose)}
           </Modal>
            <View style={[localStyle.section, localStyle.bottomSection, {backgroundColor:"#04B2E8"}]}>
              <View style={localStyle.sharePanel}>
                    <Image
                      source={images[getChangeVal]}
                      style={{width:235, height:235}}
                    />
                    <View style={localStyle.sharedBlock}>
                       <Text style={localStyle.timeText}>got it! you are <Text style={{color:color[getChangeVal],}}>{healthState.toLowerCase()}</Text></Text>

                    </View>
                    {/*<TouchableOpacity onPress={onReadyToBeginPress} style={localStyle.shareWithCoach} >
                        <Text style={localStyle.answer}>ready to begin?</Text>
                        <Image
                          style={{marginTop:2}}
                          source={Images.whiteArrowIcon}
                        />
                    </TouchableOpacity> */}

                </View>
                <View style={localStyle.homeButtonPanel}>
                    {/*<TextButton
                        label={"Done"}
                        onPress={onHomePress}
                        style={localStyle.buttonStyle}
                        loadingColor='#07c4f0'
                        primary
                        labelStyle={{fontSize:25}}
                    /> */}
                    <TextButton
                        label={"ready to begin?"}
                        onPress={onReadyToBeginPress}
                        style={localStyle.buttonStyle}
                        loadingColor='#07c4f0'
                        primary
                        labelStyle={{fontSize:25}}
                    />
                </View>

            </View>
        </KeyboardAwareScrollView>
    </View>
    );
  }

}

export default FamishedView;
