import React from 'react';
import {Text,AsyncStorage,StyleSheet,Image,TouchableOpacity} from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import FamishedView from './FamishedView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import {actions as rhythmActions,selectors as rhythmSelectors} from 'PeaceWithFood/src/store/rhythm';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
import {Images} from 'PeaceWithFood/src/components/icons';
import {NavigationActions} from 'react-navigation';
import { Notifications, Permissions, Constants } from 'expo';
let routeName='Dashboard';
const localStyles = StyleSheet.create({
    button: {
        flex: 1,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

class FamishedContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: '',
        headerLeft: <TouchableOpacity
            style={localStyles.button}
            onPress={() => navigation.dispatch({type: "Navigation/BACK"})}
        >
            <Image
                source={Images.whiteBackIcon}
            />
        </TouchableOpacity>,
        headerRight:<TouchableOpacity style={{marginRight:10,height:40,width:40,justifyContent:'center',alignItems:'center'}} onPress={() =>
            navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName })
            ]
          }))}><Image source={Images.whiteCloseIcon}/></TouchableOpacity>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTitleStyle: styles.headerTitleStyle,
        cardStack: {
        transition: (previousRoute: Route) => {
         config: { duration: 580 }
        }
      },
    });

    state = {
          visibleModal: false,
          healthState:'Famished',
          user_id:null,
          value:null,
          healthStateId:null,
          checkin_recorded:false,
    };

    componentWillMount(){
      AsyncStorage.getItem('@pwf/user').then((value) =>{
        if(value){
          user =JSON.parse(value)
          this.setState({user_id:user.id});
           console.log('user',user.id);
        }
      });
    }

    componentDidMount() {
      // console.log('navigation',this.props.navigation.state.params.healthState);
      this.setState({healthState:this.props.navigation.state.params.healthState,
                     healthStateId:this.props.navigation.state.params.healthStateId
      });
      // const { params } = this.props.navigation.state;
    }

    componentWillReceiveProps(nextProps) {
           console.log('Rhythm Created',nextProps.addRhythmOptions);
    }
    onHomePress = () =>{
      if(!this.state.checkin_recorded){
        this.props.createRhythm(this.state.user_id,this.state.healthStateId);
        this.resetRoute("Dashboard");
      }
      else{
        // Alert.alert("", "Check in details already added" );
        this.resetRoute("Dashboard");
      }
    }

    onReadyToBeginPress = () =>{
      //closed for only track most recent checkIn
      Notifications.cancelAllScheduledNotificationsAsync();
      this.props.createRhythm(this.state.user_id,this.state.healthStateId);
      this.setState({checkin_recorded:true});
      this.props.navigation.navigate('Meal',{healthState:this.state.healthState});

      //  this.setState({ visibleModal: true})
    }

    onModalClose = () =>{
      this.setState({ visibleModal: false})
    }

    onReminderPress = () =>{
      this.props.navigation.navigate('Reminder');
      this.setState({ visibleModal: false})
    }

    render() {

        return <FamishedView
            {...this.state}
            onHomePress={this.onHomePress}
            onAddCoachesPress={this.onAddCoachesPress}
            onReadyToBeginPress={this.onReadyToBeginPress}
            onModalClose={this.onModalClose}
            onReminderPress={this.onReminderPress}
            {...this.props}


        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
    addRhythmOptions: rhythmSelectors.addRhythmOptions(state)
});

export default connect(mapStateToProps, {
    ...actions,
    ...rhythmActions
})(FamishedContainer);
