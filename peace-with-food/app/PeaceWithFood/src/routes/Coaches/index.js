import React from 'react';
import { Text,
         TouchableOpacity,
         Image,
         AsyncStorage,
         Platform,
         Dimensions,
         View
       } from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import AddCoachesView from './AddCoachesView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions as authActions, selectors} from 'PeaceWithFood/src/store/auth';
import {actions as connectActions, selectors as connectSelectors} from 'PeaceWithFood/src/store/pwfconnect';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
import {Images} from 'PeaceWithFood/src/components/icons';
import {NavigationActions} from 'react-navigation';
let routeName='Dashboard';
var {height, width} = Dimensions.get('window');

const rules = {
    email: 'required_without:code|email',
    code:  'required_without:email|numeric',
};

const validate = values => {
    const validator = new Validator(values, rules);
    validator.passes();
    return validator.errors.all();
};


const AddCoachesViewWithForm = reduxForm({
    form: 'addCoach',
    fields: ['email','code'],
    validate
})(AddCoachesView);



class CoachesContainer extends NavComponent {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage:'',
      coachDetails:{},
      userid:null
    };
  }
    static navigationOptions = ({navigation}) => ({
        title:       <Text style={{color:'#53acc7'}}>ADD NEW FRIEND</Text>,
        headerLeft:  <BackButton color='#2799bb' onPress={() => navigation.dispatch({type: "Navigation/BACK"})}/>,
        headerRight: <TouchableOpacity style={{marginRight:10,height:40,width:40,justifyContent:'center',alignItems:'center'}} onPress={() =>
                        navigation.dispatch(NavigationActions.reset({
                        index: 0,
                        actions: [
                            NavigationActions.navigate({ routeName })
                        ]
                      }))}><Image source={Images.closeIcon}/></TouchableOpacity>,
        headerStyle: {
                        position: 'absolute',
                        backgroundColor: 'transparent',
                        zIndex: 100,
                        top: 0,
                        left: 0,
                        right: 0,
                        borderBottomWidth: 0
                     },
       headerTitleStyle: {
                         fontSize: 17,
                         color: Colors.WHITE,
                         alignSelf: 'center',
                         textAlign: 'center',
                         ...Platform.select({
                           android: { width: width/1.8, },
                         }),
                     }
    });

    componentDidMount() {

    }
    componentWillMount() {

         AsyncStorage.getItem('@pwf/user').then((value) =>{
               if (value) {
                 user = JSON.parse(value)
                 this.setState({
                  userid:user.id
                 });
               }
         });
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.error) {
            this.setState({errorMessage:nextProps.error.error});
            Alert.alert(
                'Please try again',
                 nextProps.error.error,
              )
       }
    }

    onHomePress = () =>{
      this.resetRoute("Dashboard");
    }



    onAddCoachesPress = (view, data, user_id, onFinish) => {

        let code  = data.code || 'NO_CODE_GIVEN';
        let email = data.email;

        this.props.addCoach(email, code, user_id, function( result ){

                if ( result && result.message_success )
                {
                    // If a coach is not in the system
                    // and an invite is sent
                    Alert.alert( result.message_success );
                }
                else if ( !result || !result.success )
                {
                    const message = result && result.message ||
                                    "An unknown error was encountered";
                    Alert.alert("", message );

                }
                onFinish(result);
        });

    }

   onRemoveCoachPress = (view, {email}, user_id, onFinish) => {
       //console.log(email, this.state.userid);
       const promise = this.props.getCoachByEmail(email);
       const self    = this;

       promise.then(function(data){
         if ( !data || !data.success )
         {
             const message = data && (data.message || data.error) ||
                             "Please try again";
             Alert.alert(message);
             onFinish();
             return;
         }
         //console.log('promise',data);
         self.setState({'coachDetails':data});
          const removeCoach =self.props.removeCoachById(self.state.userid,data.id);
           removeCoach.then(function(data){
             console.log(data);
             if(data && data == 204){
               Alert.alert("", "Friend removed successfully" );
             }
             else{
               Alert.alert('Please try again', "Something went wrong" );
             }
             onFinish();
            })
            .catch((error) => {
              console.log(error);
              Alert.alert('Please try again', "Something went wrong" );
              onFinish();
            });


       }, function(data){
            if ( !data || !data.success )
            {
                const message = data && (data.message || data.error) ||
                                "Please try again";
                Alert.alert( message );
                onFinish();
            }
          });
   }

     onManageFriendPress =() =>{
       this.props.navigation.navigate('ManageSupporters');
     }

    render() {
        const {state} = this.props.navigation;
        return <AddCoachesViewWithForm
            {...this.state}
            onHomePress={this.onHomePress}
            onAddCoachesPress={this.onAddCoachesPress}
            onRemoveCoachPress ={this.onRemoveCoachPress}
            onManageFriendPress ={this.onManageFriendPress}
            {...this.props}
        />;
    }
}

const mapStateToProps = (state, props) => ({
});


export default connect(mapStateToProps, {
    ...authActions,
    ...connectActions
})(CoachesContainer);
