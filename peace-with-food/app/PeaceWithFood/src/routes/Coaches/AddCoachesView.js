import React,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    AsyncStorage,
    KeyboardAvoidingView

} from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';
import CheckBox from 'react-native-check-box';
import Toast, {DURATION} from 'react-native-easy-toast'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
//import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
import {Images} from 'PeaceWithFood/src/components/icons';
import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { LogoIcon } from 'PeaceWithFood/src/components/icons';
import { LinearGradient } from 'expo';
var {height, width} = Dimensions.get('window');



const localStyle = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#dce0e2'
  },
  section: {
      flex: 1
  },
  bottomSection: {
      backgroundColor: '#dce0e2',
  },
  topSection: {
      justifyContent: 'center'
  },
  logoPanel:{
    marginTop:height/7,
    alignItems:'center'
  },
  form: {
      justifyContent: 'center',
    //  flex: 1
      marginTop:(Platform.OS === 'ios' ? 80 : 30)
  },
  row: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      paddingLeft: width/20,
      paddingRight: width/20,
      //marginBottom:(Platform.OS === 'ios' ? height/9 : height/8)
  },
  inputText: {
      backgroundColor: '#e8e9ea',
      borderBottomColor:'#2799bb',
      borderLeftColor:'#d4d5d6',
      borderRightColor:'#d4d5d6',
      borderTopColor:'#d4d5d6',
      borderWidth:2,
      color:'#2799bb',
      fontSize: 17,
      paddingVertical: 10,
      paddingHorizontal: 6,
      width:(Platform.OS === 'ios' ? width/1.1 : width/1.1),
      marginBottom:25,

  },
  button:{
      backgroundColor:'#00bded',
      borderRadius:50,
      shadowColor: '#a3a7a8',
      borderTopWidth:0,
      shadowOffset: { width: 1, height: 3 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 1,
  },
  logoText: {
      textAlign: 'center',
      fontSize: 30,
      marginTop:height/5,
      color:'#2799bb'
  },
  buttonText: {
      color: '#446a7a',
      fontSize: 18
  },
  homeButtonPanel:{
      marginTop:(Platform.OS === 'ios' ? height/12 : 5),
      height:60,
      width:width/1.2,
      alignItems: 'center',
      justifyContent:'center',
      marginLeft:width/10,
      flexDirection:'row',

  },
  buttonStyle:{
      backgroundColor:'#4cc5e7',
      //borderRadius:50,
      borderTopLeftRadius:50,
      borderBottomLeftRadius:50,
      borderTopRightRadius:50,
      borderBottomRightRadius:50,
      shadowColor: '#a3a7a8',
      borderTopWidth:0,
      shadowOffset: { width: 1, height: 3 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 1,
      width:width/4,
    },

});
class AddCoachesView extends Component {


  constructor(props, context) {
      super(props, context);
      this.state = {
         toggle: false,
         buttonPanel:true,

      };
  }


  componentWillMount() {

       AsyncStorage.getItem('@pwf/user').then((value) =>{
             if (value) {
               user = JSON.parse(value)
               this.setState({
                user_id:user.id
               });
             }
       });
  }



render(){
  let {

    onHomePress,
    onAddCoachesPress,
    handleSubmit,
    onRemoveCoachPress,
    onManageFriendPress

} = this.props;


    function handleSubmitWithLoader( onPress, view, destination ){

            var this_ = view;
            function hideSpinner()
            {
                    if ( !view.refs ){ return; }
                    if ( view.refs.loadingImage ){
                         view.refs.loadingImage.setNativeProps({
                                      style:{width:0,height:0}});
                        this_.setState({buttonPanel:true});
                                    }
                    if ( view.refs.buttonAdd )
                         view.refs.buttonAdd.setNativeProps({
                                       style:{width:72,height:72}});
                    if ( view.refs.buttonRemove )
                         view.refs.buttonRemove.setNativeProps({
                                       style:{width:72,height:72}});
            }
            function showSpinner()
            {
                    if ( !view.refs ){ return; }
                    if ( view.refs.loadingImage ){
                         view.refs.loadingImage.setNativeProps({
                                       style:{width:72,height:72}});
                          this_.setState({buttonPanel:false});
                          }
                    if ( view.refs.buttonAdd )
                         view.refs.buttonAdd.setNativeProps({
                                       style:{width:0,height:0}});
                    if ( view.refs.buttonRemove )
                         view.refs.buttonRemove.setNativeProps({
                                       style:{width:0,height:0}});
            }

            function onFinish(result){
              hideSpinner();
              if ( result && result.success && destination )
                   view.props.navigation.navigate(destination, {result:result} );
            }
            function onPressWithSpinner(){
              showSpinner();
              return onPress.call(
                            this
                          , view
                          , arguments[0]
                          , this_.state.user_id
                          , onFinish
                          );

            }
            return handleSubmit(onPressWithSpinner);

    }


    return (
  <LinearGradient
      colors={['#EFF3F4', '#E0E4E7']} style={styles.container}>
      <StatusBar/>
      <KeyboardAvoidingView
        style={localStyle.container}
        behavior="padding"
      >
      <KeyboardAwareScrollView resetScrollToCoords={{x: 0, y: 0}} >

          <View style={[localStyle.section, localStyle.bottomSection]}>
              <View style={localStyle.logoPanel}>
                <Image

                    source={Images.addCoachIcon}
                />
              </View>

              <View style={localStyle.form}>
                  {/*
                  <View style={[localStyle.row]}>
                      <TextInput

                          numberOfLines={1}
                          style={localStyle.inputText}
                          placeholder="username"
                          placeholderTextColor="#446a7a"
                          underlineColorAndroid='transparent'


                      />
                  </View>
                  <View style={[localStyle.row,{marginBottom:10,marginTop:-10}]}>
                     <Text style={{color:'#a3a6a8'}}>OR</Text>
                  </View>
                  */}
                  <View style={[localStyle.row, {paddingBottom:0,marginBottom:0}]}>
                    <TextInput
                        fieldName="email"
                        numberOfLines={1}
                        style={[localStyle.inputText,{marginBottom:0}]}
                        placeholder="email?"
                        placeholderTextColor="#446a7a"
                        underlineColorAndroid='transparent'

                    />
                  </View>
                  <View style={[localStyle.row, {paddingBottom:15,marginTop:-3}]}>
                    <Text style={{fontSize:18,color:"#555"}}>
                    or</Text>
                  </View>
                  <View style={[localStyle.row, {paddingBottom:0,marginBottom:0}]}>
                    <TextInput
                          fieldName="code"
                          numberOfLines={1}
                          style={[localStyle.inputText,{marginBottom:0}]}
                          placeholder="have a code?"
                          placeholderTextColor="#446a7a"
                          underlineColorAndroid='transparent'

                    />
                  </View>
                      <View style={[localStyle.homeButtonPanel, {margin:0}]}>
                          <Image
                                ref="loadingImage"
                                style={[{width:0,height:0}]}
                                source={require('../../images/spinners/loadingio-ring-2s-72px.gif')}
                                />
                      </View>
                      { this.state.buttonPanel && (
                      <View style={[localStyle.homeButtonPanel,{marginTop:(Platform.OS === 'ios' ? -85 : -50)}]}>
                      <TextButton
                          label={"Add"}
                          onPress={handleSubmitWithLoader(onAddCoachesPress, this, "CoachAdded")}
                          style={[localStyle.buttonStyle]}
                          loadingColor='#07c4f0'
                          primary
                          labelStyle={{fontSize:(Platform.OS === 'ios' ? 22 : 20)}}
                      />
                      {/* Commented out as per client feedback
                        <TextButton
                          label={"Remove"}
                          onPress={handleSubmitWithLoader(onRemoveCoachPress, this)}
                          style={[localStyle.buttonStyle,{borderTopLeftRadius:0,
                          borderBottomLeftRadius:0,borderTopRightRadius:50,
                          borderBottomRightRadius:50,backgroundColor:'#add1e0'}]}
                          loadingColor='#add1e0'
                          primary
                          labelStyle={{fontSize:(Platform.OS === 'ios' ? 22 : 20)}}
                      />
                      */}
                      </View>
                    )}

                  { this.state.buttonPanel && (
                  <TouchableOpacity onPress={onManageFriendPress} style={{justifyContent:'center',alignItems:'center',marginTop:10}}>
                    <Text style={{color:'#2fb6d8',fontSize:17}}>Manage friends</Text>
                  </TouchableOpacity>
                  )}
              </View>

          </View>
      </KeyboardAwareScrollView>
      </KeyboardAvoidingView>
  </LinearGradient>
  );
  }

  }

export default AddCoachesView;
