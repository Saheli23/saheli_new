import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    Platform
} from 'react-native';
import _ from 'underscore';
import { LinearGradient } from 'expo';
import styles from 'PeaceWithFood/src/styles';
import { Colors, Sizes } from 'PeaceWithFood/src/styles/constants';
import Svg, { G, Path } from 'react-native-svg';
import CircularSlider from 'react-native-circular-slider';
var {height, width} = Dimensions.get('window');

const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#e1e5e8',
        //marginTop:-120,
        marginTop:(Platform.OS === 'ios' ? ((height < 700) ? -(height/8) :-(height/4)) : -(height/7))
        //height:height
    },
    bedtimeText: {
        color: '#ff9800',
        marginLeft: 3,
        fontSize: 16,
    },
    wakeText: {
        color: '#ffcf00',
        marginLeft: 3,
        fontSize: 16,
    },
    timeContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 20,
    },
    time: {
        alignItems: 'center',
        flex: 1,
    },
    timeHeader: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    timeValue: {
        color: 'white',
        fontSize: 35,
        fontWeight: "300",
    },
    sleepTimeContainer: {
        flex: 1,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    }
});

const ADVANCE_AFTER_DELAY = false; // no touch needed
const START_ANGLE = 2.35619; // ~135 degrees
const END_ANGLE = 3.92699; // ~225 degrees

const FULL_RADIANS = 4.712385307179586;

export default class RhythmView extends Component {

    state = {
        startAngle: 0,
        currentValue: 5,
        centerCircleText:'neutral',
        textCircleStrokeColor:'#00bded',
        sickBar:false,
        famishedBar:false,
        gradientColorFrom:"#00bded",
        gradientColorTo:"#16cefc"
    };

    calculateMinutesFromAngle = (angle) => {
        return Math.round(angle / (2 * Math.PI / (12 * 12))) * 5;
    }

    calculateTimeFromAngle = (angle) => {
        const minutes = this.calculateMinutesFromAngle(angle);
        const h = Math.floor(minutes / 60);
        const m = minutes - h * 60;

        return { h, m };
    }

    roundAngleToFives = (angle) => {
        const fiveMinuteAngle = 2 * Math.PI / 144;

        return Math.round(angle / fiveMinuteAngle) * fiveMinuteAngle;
    }

    touchRelease =(touchRelease)=>{
      var self = this, r;
      if ( self.touchRelease_lock )
           return;
      clearTimeout( self.touchRelease_graceperiod_timer );
      if ( touchRelease && ADVANCE_AFTER_DELAY )
      {
        console.log('reset graceperiod');
        r = setTimeout(() =>
          {
            self.touchRelease_lock = true;
            setTimeout(() => { self.touchRelease_lock = false; }, 1500 );

            if (self.state.currentValue == 1 && touchRelease){
                self.props.navigation.navigate('Sick',{ healthState: 'Famished',healthStateId:self.state.currentValue});
            }
            if (self.state.currentValue == 2 && touchRelease){
                self.props.navigation.navigate('Sick',{ healthState: 'Starving',healthStateId:self.state.currentValue});
            }
            if (self.state.currentValue == 3 && touchRelease){
                self.props.navigation.navigate('Sick',{ healthState: 'Very Hungry',healthStateId:self.state.currentValue});
            }
            if (self.state.currentValue == 4 && touchRelease){
                self.props.navigation.navigate('Sick',{ healthState: 'Ready',healthStateId:self.state.currentValue});
            }
            if (self.state.currentValue == 5 && touchRelease){
                self.props.navigation.navigate('Sick',{ healthState: 'Neutral',healthStateId:self.state.currentValue});
            }
            if (self.state.currentValue == 6 && touchRelease){
                self.props.navigation.navigate('Sick',{ healthState: 'Full & Satisfied',healthStateId:self.state.currentValue});
            }
            if (self.state.currentValue == 7 && touchRelease){
                self.props.navigation.navigate('Sick',{ healthState: 'Overly Full',healthStateId:self.state.currentValue});
            }
            if (self.state.currentValue == 8 && touchRelease){
                self.props.navigation.navigate('Sick',{ healthState: 'Stuffed',healthStateId:self.state.currentValue});
            }
            if (self.state.currentValue == 9 && touchRelease){
                self.props.navigation.navigate('Sick',{ healthState: 'Sick',healthStateId:self.state.currentValue});
            }
          }, 666 );
        self.touchRelease_graceperiod_timer = r;
      }
    };

    touchInCenter =() =>{
      var self = this;
      if (self.state.currentValue == 1 ){
          self.props.navigation.navigate('Sick',{ healthState: 'Famished',healthStateId:self.state.currentValue});
      }
      if (self.state.currentValue == 2 ){
          self.props.navigation.navigate('Sick',{ healthState: 'Starving',healthStateId:self.state.currentValue});
      }
      if (self.state.currentValue == 3 ){
          self.props.navigation.navigate('Sick',{ healthState: 'Very Hungry',healthStateId:self.state.currentValue});
      }
      if (self.state.currentValue == 4 ){
          self.props.navigation.navigate('Sick',{ healthState: 'Ready',healthStateId:self.state.currentValue});
      }
      if (self.state.currentValue == 5 ){
          self.props.navigation.navigate('Sick',{ healthState: 'Neutral',healthStateId:self.state.currentValue});
      }
      if (self.state.currentValue == 6 ){
          self.props.navigation.navigate('Sick',{ healthState: 'Full & Satisfied',healthStateId:self.state.currentValue});
      }
      if (self.state.currentValue == 7 ){
          self.props.navigation.navigate('Sick',{ healthState: 'Overly Full',healthStateId:self.state.currentValue});
      }
      if (self.state.currentValue == 8 ){
          self.props.navigation.navigate('Sick',{ healthState: 'Stuffed',healthStateId:self.state.currentValue});
      }
      if (self.state.currentValue == 9 ){
          self.props.navigation.navigate('Sick',{ healthState: 'Sick',healthStateId:self.state.currentValue});
      }
    }

    onUpdate = ({ startAngle, angleLength }) => {
        if (startAngle >= START_ANGLE && startAngle < END_ANGLE - 1) {
            startAngle = START_ANGLE;
        } else if (startAngle <= END_ANGLE && startAngle > START_ANGLE) {
            startAngle = END_ANGLE;
        }

        let base = startAngle;
        if (base > 0 && base <= START_ANGLE) {
            base += Math.PI * 2;
        }

        base -= END_ANGLE;

        var state = {};
        function setState(data){
                 _.extend( state, data );
        }

        setState({
            startAngle: startAngle,
            angleLength: 0,
            currentValue: (Math.round(8 * (base / FULL_RADIANS))) + 1
        });
        const c = state.currentValue;
        if (  c  == 1  ){
          setState({centerCircleText:'famished',textCircleStrokeColor:'#eac435'})
        }
        else if (  c  == 2 ){
          setState({centerCircleText:'starving',textCircleStrokeColor:'#eac435'})
        }
        else if (  c  == 3  ){
          setState({centerCircleText:'very hungry',textCircleStrokeColor:'#eac435'})
        }
        else if (  c  == 4  ){
          setState({centerCircleText:'ready',textCircleStrokeColor:'#00bded',gradientColorFrom:"#00bded",
          gradientColorTo:"#16cefc"})
        }
        else if (  c  == 5  ){
          setState({centerCircleText:'neutral',textCircleStrokeColor:'#00bded',gradientColorFrom:"#00bded",
          gradientColorTo:"#16cefc",})
        }
        else if (  c  == 6 ){
          setState({centerCircleText:"full & \nsatisfied",gradientColorFrom:"#00bded",
          gradientColorTo:"#16cefc",textCircleStrokeColor:'#00bded'})
        }
        else if (  c  == 7  ){
          setState({centerCircleText:'overly full',textCircleStrokeColor:'#f17d42'})
        }
        else if (  c  == 8  ){
          setState({centerCircleText:'stuffed',textCircleStrokeColor:'#f17d42'})
        }
        else if (  c  == 9  ){
          setState({centerCircleText:'sick',textCircleStrokeColor:'#f17d42'})
        }
        if (  c  > 6  ){
            setState({sickBar:true,gradientColorTo:'#fb874c',gradientColorFrom:'#e5753d'});
        }
        else{
            setState({sickBar:false,});
        }
        if (  c  < 4  ){
           setState({famishedBar:true,gradientColorFrom:"#e5be32",gradientColorTo:"#f4ce3f"});
        }
        else{
          setState({famishedBar:false,});
        }
        if (  c  == -6  ){
          setState({famishedBar:false,gradientColorFrom:"#00bded",
          gradientColorTo:"#16cefc",})
        }


        this.setState( state );
        console.log(this.state.currentValue);
    };

    render() {
        const { startAngle, angleLength,centerCircleText,sickBar,famishedBar,gradientColorFrom,gradientColorTo } = this.state;

        return (
            <LinearGradient
                colors={['#EFF3F4', '#E0E4E7']} style={localStyles.container}>
                <View style={{backgroundColor:'transparent', alignItems:'center',justifyContent:'center',width:(width/1.5),height:(Platform.OS === 'ios' ? 50 : 80),flexDirection:'column', marginBottom:(Platform.OS === 'ios' ? ((height < 700) ? (height/25) :(height/12)) : 0)}}>
                <Text style={{color:'#2b6780',fontSize:(Platform.OS === 'ios' ? 28 : 25 ),fontFamily:'Sarabun-Bold'}}>
                WHERE ARE YOU NOW?</Text>
                                               </View>
                <View>
                    <CircularSlider
                        startAngle={startAngle}
                        angleLength={0}
                        onUpdate={this.onUpdate}
                        segments={5}
                        strokeWidth={15}
                        radius={140}
                        gradientColorFrom={gradientColorFrom}
                        gradientColorTo={gradientColorTo}
                        bgCircleColor="#00bded"
                        textBgCircleColor='white'
                        textCircleStrokeColor={this.state.textCircleStrokeColor}
                        centerCircleText={centerCircleText}
                        sickBar={sickBar}
                        famishedBar={famishedBar}
                        touchRelease={this.touchRelease}
                        touchInCenter={this.touchInCenter}
                    />
                </View>
            </LinearGradient>
        );
    }
}
