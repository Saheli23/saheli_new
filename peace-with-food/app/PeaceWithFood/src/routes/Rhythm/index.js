import React from 'react';

import {
    Text,
    Dimensions,
    TouchableOpacity,
    Image
} from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';

import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import RhythmView from './RhythmView';
import {connect} from 'react-redux';
import {actions as authActions, selectors} from 'PeaceWithFood/src/store/auth';
import {actions as userActions} from 'PeaceWithFood/src/store/user';
import {actions as rhythmActions,selectors as rhythmSelectors} from 'PeaceWithFood/src/store/rhythm';
import styles from 'PeaceWithFood/src/styles';
import { Colors, Sizes } from 'PeaceWithFood/src/styles/constants';
import {NavigationActions} from 'react-navigation';
import {Images} from 'PeaceWithFood/src/components/icons';
let routeName='Dashboard';
var {height, width} = Dimensions.get('window');

class RhythmContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: '',
        headerLeft: null,
        headerRight:<TouchableOpacity style={{marginRight:10,height:40,width:40,justifyContent:'center',alignItems:'center'}} onPress={() =>
            navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName })
            ]
          }))}><Image source={Images.closeIcon}/></TouchableOpacity>,
        headerStyle: {
            backgroundColor: '#EFF3F4',
            borderBottomWidth: 0
        },
        headerTitleStyle: {width:width/1.3,left:3},
        gesturesEnabled: false
    });

    componentWillMount(){
      this.props.getRhythms()
    }

    componentWillReceiveProps(nextProps) {
      console.log('getRhythmOptions', nextProps.getRhythmOptions);
        if(!nextProps.isLoggedIn) {
            this.resetRoute("Splash");
        }
    }

    async componentDidMount() {

    }

    render() {
        return <RhythmView
            {...this.props}
        />
    }
}

const mapStateToProps = (state, props) => ({
    isLoggedIn: selectors.getIsLoggedIn(state),
    getRhythmOptions: rhythmSelectors.getRhythmOptions(state)
});

export default connect(mapStateToProps, {
    ...authActions,
    ...userActions,
    ...rhythmActions
})(RhythmContainer);
