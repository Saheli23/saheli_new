import React from 'react';
import {Text,TouchableOpacity,Image,AsyncStorage,Alert} from 'react-native';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import AskToCheckInView from './AskToCheckInView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
import {Images} from 'PeaceWithFood/src/components/icons';
import {NavigationActions} from 'react-navigation';
let routeName='Dashboard';
class AskToCheckInContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: '',
        headerRight:<TouchableOpacity style={{marginRight:10}} onPress={() =>
            navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName })
            ]
          }))}><Image source={Images.whiteCloseIcon}/></TouchableOpacity>,
        // headerLeft: <BackButton color={Colors.WHITE} onPress={() => navigation.dispatch({type: "Navigation/BACK"})}/>,
        headerLeft:null,
        headerStyle: {
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTitleStyle: styles.headerTitleStyle
    });

    state = {
          visibleModal: false,
    };

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }
    onHomePress = () =>{
      //this.resetRoute("Dashboard");
      this.props.navigation.navigate('Connect');
    }

    onYesPress = () =>{
      //this.resetRoute("Dashboard");
      AsyncStorage.getItem('DeviceTime').then((value) =>{
        if(value){
            if(value == 'sun'){
              Alert.alert(
                    '',
                    "Looks like your PwF app got turned off. No worries! Tap the sun and check-in as usual.",
                    [
                      {text: 'OK', onPress: () => this.resetRoute("Dashboard")},
                    ],
                  )
            }
            else{
            this.props.navigation.navigate('Rhythm');
              }
            }
          });
        }

    onNoPress = () =>{

      AsyncStorage.setItem('CheckedIn','no');
      this.resetRoute("Dashboard");

    }


    render() {

        return <AskToCheckInView
            {...this.state}
            onHomePress={this.onHomePress}
            onAddCoachesPress={this.onAddCoachesPress}
            onYesPress={this.onYesPress}
            onNoPress={this.onNoPress}

        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
});

export default connect(mapStateToProps, actions)(AskToCheckInContainer);
