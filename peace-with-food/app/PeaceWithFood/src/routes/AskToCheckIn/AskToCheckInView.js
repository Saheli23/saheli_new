import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView

} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';

const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dce0e2'
    },
    section: {
        flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? height/12 : 30),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5,
        flexDirection:'row',
    },
    buttonStyle:{
        backgroundColor:'#00bded',
        //borderRadius:50,
        borderTopLeftRadius:50,
        borderBottomLeftRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:1,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,
        marginTop:height/6,
        alignItems: 'center',
        justifyContent:'center',
        flexDirection:'column'
      },
      answer:{
        color:'#627078',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#a2a4a5',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: 'white',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
  checkPanel:{
    // alignItems: 'center',
    // justifyContent:'center',
    paddingTop:20,
    paddingLeft:20,
    // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    flexDirection:'row'
  },
  sharedBlock:{
    padding:40,

    marginTop:(Platform.OS === 'ios' ? 50 : 50),
    height:150,
    width:width/1.1,
    flexDirection:'column',
    //flexWrap: "wrap",
    justifyContent:'center',
    alignItems:'center'
  },
  timeText:{
    color:'#53acc7',
    fontSize:25,
    fontWeight:'500',
  }

});



class AskToCheckInView extends Component {

  constructor(props){
    super(props);

  }



  render (){
    let {

        onHomePress,
        onAddCoachesPress,
        onShareWithCoachPress,
        visibleModal,
        onModalClose,
        onYesPress,
        onNoPress
    } = this.props;
    return (<View style={styles.container}>
        <StatusBar/>
        <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>

            <View style={[localStyle.section, localStyle.bottomSection]}>
              <View style={localStyle.sharePanel}>
                    <Image
                      source={Images.bigCheckinIcon}

                    />
                    <View style={localStyle.sharedBlock}>
                       <Text style={[localStyle.timeText,{color:'#99bdcc'}]}>Would you like to </Text>
                       <Text style={[localStyle.timeText,{color:'#99bdcc'}]}>check in? </Text>

                    </View>

                </View>
                <View style={localStyle.homeButtonPanel}>
                    <TextButton
                        label={"Yes"}
                        onPress={onYesPress}
                        style={[localStyle.buttonStyle,{width:width/4}]}
                        loadingColor='#00bded'
                        primary
                        labelStyle={{fontSize:(Platform.OS === 'ios' ? 22 : 20),fontWeight:'700'}}
                    />
                    <TextButton
                        label={"No"}
                        onPress={onNoPress}
                        style={[localStyle.buttonStyle,{width:width/4,borderTopLeftRadius:0,
                        borderBottomLeftRadius:0,borderTopRightRadius:50,
                        borderBottomRightRadius:50,backgroundColor:'#add1e0'}]}
                        loadingColor='#add1e0'
                        primary
                        labelStyle={{fontSize:(Platform.OS === 'ios' ? 22 : 20),fontWeight:'700'}}
                    />
                </View>

            </View>
        </KeyboardAwareScrollView>
    </View>
    );
  }

}

export default AskToCheckInView;
