import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    ImageBackground


} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';
import Dash from 'react-native-dash';
import { AreaChart,XAxis,YAxis,LineChart } from 'react-native-svg-charts';
import { Circle,Line,G } from 'react-native-svg';
import {actions as rhythmActions,selectors as rhythmSelectors} from 'PeaceWithFood/src/store/rhythm';
import {connect} from 'react-redux';
import * as scale from 'd3-scale';
import * as shape from 'd3-shape';
import dateFns from 'date-fns';
import moment from 'moment';
import _ from 'underscore';
const aspectRatio = height/width;



const data = [ 50, 10, 40, 95, -4, -120, 85, 91, 35, -53, -20, -120, 10, 40, 95, -4, -24, 85, 91, 35, -53, 24, 50, -20, -80, ];

const localStyle = StyleSheet.create({
    container: {
        //flex: 1,
        //backgroundColor: 'yellow',
        height:height
    },
    section: {
      //  flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? 70 : 50),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,

        //alignItems: 'center',
      //  justifyContent:'center',
        marginTop:50,
        flexDirection:'column',

      },
      borderPanel:{
        height:height/1.2,
        marginTop:height/10,
        marginBottom:(Platform.OS === 'ios' ? 10 : 10),
        marginLeft:15,
        marginRight:15,
        // borderBottomWidth:2,
        // borderBottomColor:'#2799bb',
        borderTopWidth:2,
        borderTopColor:'#2799bb'
      },
      answer:{
        color:'#627078',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#a2a4a5',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: 'white',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
      checkPanel:{
        // alignItems: 'center',
        // justifyContent:'center',
        paddingTop:20,
        paddingLeft:20,
        // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
        flexDirection:'row'
      },
      sharedBlock:{
        //padding:40,
        alignItems: 'center',
        justifyContent:'space-between',
        marginTop:5,
        height:35,
        width:width/1.2,
        flexDirection:'row',
        flexWrap: "wrap",
        backgroundColor:'#eaeced',

      },
      timeText:{
        color:'#2799bb',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
        marginRight:5,
      },
      nameText:{
        color:'#447990',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
      },
      areaStyleIos:{
        height: height/2.2,
        width:height/1.2,

      },
      areaStyleAndroid:{
        flex:1,
      },
      axisStyleIos:{
       height: height/2.1,
       width:height/1.2,
       backgroundColor:'transparent',


     },
     axisStyleAndroid:{
       height:48,
       backgroundColor:'transparent'
     }

});


class DayEndGraphChartView extends Component {

  constructor(props){
    super(props);
    this.state = {
      chatPanelOpen:false,
      visibleModal:false,
      page: true,
      selectedDate:{},
      user_id:null,
      rhythmEntries:[],
      dataFetch:false,
      dayEnd:false,
      healthStatusId:null,
      thoughtValue:{}

    }

  }

  componentWillMount() {
       this.setState({selectedDate:this.props.navigation.state.params.dateObj,dayEnd:this.props.navigation.state.params.dayEnd});

       AsyncStorage.getItem('@pwf/user').then((value) =>{

         if(value){
           user =JSON.parse(value)
           this.setState({user_id:user.id});
            console.log('user',this.state.user_id);
            this.props.getClientRhythm(this.state.user_id , this.props.navigation.state.params.selectedStartDate,this.props.navigation.state.params.selectedEndDate );
         }
       });


  }
  componentWillReceiveProps(nextProps) {
    console.log('nextProps',nextProps.getRhythmByClient);
    if(nextProps.getRhythmByClient){
        this.setState({
          rhythmEntries:nextProps.getRhythmByClient,
          dataFetch:true


        });
    }
  }

  onChatPress = (chatPanelOpen) =>{
    this.setState({chatPanelOpen : chatPanelOpen ? true : false});
  }

  changePage(p){
     this.setState({page:p});
  }

  _renderModalContent = (onModalClose,healthState,onCheckMarkPress,onMessagePress,healthStatusId,thoughtValue) => {
    let Reason1,Reason2,Reason3,Reason4;
    let healthStateArray=[['Famished', 'Starving', 'Very Hungry'], ['Ready', 'Neutral', 'Full & Satisfied'], ['Overly Full', 'Stuffed', 'Sick']];
    let getChangeVal = healthStateArray.map(function(e){
                   return e.indexOf(healthState) > -1;
                }).indexOf(true);
    let healthStateArrayForModal=['','Famished', 'Starving', 'Very Hungry','Ready', 'Neutral', 'Full & Satisfied','Overly Full', 'Stuffed', 'Sick'];
    let getChangeValForModal = healthStateArray.map(function(e){
                   return e.indexOf(healthStatusId) > -1;
                }).indexOf(true);

    //if(getChangeVal ==0){
      let reasonArray=[[
                        {text:'Busy/ Forgot',type:'checkbox'},
                        {text:'Food Unavailable',type:'checkbox'},
                        {text:'Consciously Restricting',type:'label'},
                        {text:'Weight worries',type:'radio',name:'consciously_restricting'},
                        {text:'I ate too much last night',type:'radio',name:'consciously_restricting'},
                        {text:'Other',type:'input',name:'consciously_restricting'},
                        {text:'Mood',type:'label'},
                        {text:'Stressed',type:'radio',name:'moodFamished'},
                        {text:'Lonely',type:'radio',name:'moodFamished'},
                        {text:'Angry',type:'radio',name:'moodFamished'},
                        {text:'Anxious',type:'radio',name:'moodFamished'},
                        {text:'Sad',type:'radio',name:'moodFamished'},
                        {text:'Tired',type:'radio',name:'moodFamished'}],
                        [],
                        [{text:'Hunger',type:'checkbox'},
                        {text:'Tempting Food',type:'checkbox'},
                        {text:'Mindless Eating',type:'checkbox'},
                        {text:'Mood',type:'label'},
                        {text:'Stressed',type:'radio',name:'moodSick'},
                        {text:'Lonely',type:'radio',name:'moodSick'},
                        {text:'Angry',type:'radio',name:'moodSick'},
                        {text:'Anxious',type:'radio',name:'moodSick'},
                        {text:'Sad',type:'radio',name:'moodSick'},
                        {text:'Tired',type:'radio',name:'moodSick'},
                        {text:'Other',type:'input',name:'moodSick'},
                        {text:'Social Pressure to Eat',type:'label'},
                        {text:'Friends',type:'radio',name:'social_pressure_to_eat'},
                        {text:'Family',type:'radio',name:'social_pressure_to_eat'}]]
      let userSelection = reasonArray[getChangeVal];
      // console.log('=======================================');
      // console.log(userSelection);
      // console.log('=======================================');
    //}

  return(
      <View style={localStyle.modalContent}>
      { this.state.page ? (
        <View>
               <View style={{flexDirection:'row',justifyContent:'flex-end'}}>

                <TouchableOpacity style={{width:50,height:50,justifyContent:'center',alignItems:'center'}} onPress={this.onModalClose.bind(this)}>
                  <Image source={Images.orangeCloseIcon} />
                </TouchableOpacity>
              </View>
              <View style={{height:height/20}}>
              <Text style={{color:'#e87f47',fontSize:20}}>{healthStateArrayForModal[healthStatusId]}</Text>
              <ScrollView>
              {/*<View  style={localStyle.checkPanel}>
              <CheckBox
                      style={{alignItems:'center',marginRight:10}}
                      onClick={()=>console.log('checked')}
                      isChecked={true}
                      checkBoxColor='#e0e4e7'

                  />
                  <Text style={localStyle.buttonText}>
                      Reason1
                  </Text>
                </View>
                <View  style={localStyle.checkPanel}>
                  <CheckBox
                        style={{alignItems:'center',marginRight:10}}
                        onClick={()=>console.log('checked')}
                        isChecked={true}
                        checkBoxColor='#e0e4e7'

                    />
                    <Text style={localStyle.buttonText}>
                        Reason2
                    </Text>
                  </View> */}
                        {  _.has(thoughtValue.options, 'checks') ? (
                          <View>
                              {(_.has(thoughtValue.options.checks, 'Busy_Forgot') && thoughtValue.options.checks.Busy_Forgot )? (
                                    <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>

                                      <Text style={[localStyle.buttonText]}>
                                          Busy/ Forgot
                                      </Text>

                                    </View>
                                    )
                                    :null
                                  }

                                {(_.has(thoughtValue.options.checks, 'Food_Unavailable') && thoughtValue.options.checks.Food_Unavailable )? (
                                    <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>

                                      <Text style={[localStyle.buttonText]}>
                                          Food Unavailable
                                      </Text>

                                    </View>
                                    )
                              :null
                            }

                                  {(_.has(thoughtValue.options.checks, 'Hunger') && thoughtValue.options.checks.Hunger )? (
                                        <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>

                                          <Text style={[localStyle.buttonText]}>
                                              Hunger
                                          </Text>

                                        </View>
                                        )
                                        :null
                                      }

                                    {(_.has(thoughtValue.options.checks, 'Tempting_Food') && thoughtValue.options.checks.Tempting_Food )? (
                                        <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>

                                          <Text style={[localStyle.buttonText]}>
                                              Tempting_Food
                                          </Text>

                                        </View>
                                        )
                                  :null
                                }
                                {(_.has(thoughtValue.options.checks, 'Mindless_Eating') && thoughtValue.options.checks.Mindless_Eating )? (
                                    <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>

                                      <Text style={[localStyle.buttonText]}>
                                          Mindless Eating
                                      </Text>

                                    </View>
                                    )
                              :null
                            }
                          </View>

                        )
                        :null
                      }
                      {  (_.has(thoughtValue.options, 'Mood') && thoughtValue.options.Mood !='')  ? (
                        <View>
                        <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>
                          <Text style={[localStyle.buttonText,{color:'#2799bb'}]}>
                              Mood
                          </Text>
                        </View>
                          <View  style={localStyle.checkPanel}>
                            <Text style={localStyle.buttonText}>
                                {thoughtValue.options.Mood}
                            </Text>
                          </View>
                        </View>

                      )
                      :null
                    }
                    { ( _.has(thoughtValue.options, 'consciously_restricting') && thoughtValue.options.consciously_restricting !='')  ? (
                      <View>
                      <View  style={localStyle.checkPanel}>
                        <Text style={[localStyle.buttonText,{color:'#2799bb'}]}>
                            Consciously Restricting
                        </Text>
                      </View>
                        <View  style={localStyle.checkPanel}>
                          <Text style={localStyle.buttonText}>
                              {thoughtValue.options.consciously_restricting}
                          </Text>
                        </View>
                      </View>
                    )
                    :null
                    }
                    {  (_.has(thoughtValue.options, 'social_pressure_to_eat') && thoughtValue.options.social_pressure_to_eat !='') ? (
                      <View>
                      <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>
                        <Text style={[localStyle.buttonText,{color:'#2799bb'}]}>
                            Social Pressure To Eat
                        </Text>
                      </View>
                        <View  style={localStyle.checkPanel}>
                          <Text style={localStyle.buttonText}>
                              {thoughtValue.options.social_pressure_to_eat}
                          </Text>
                        </View>
                      </View>

                  )
                  : null
                }
              </ScrollView>
              </View>
              <View style={{marginTop:15}}>
                { _.has(thoughtValue, 'message_id') && (
                    <Text style={[localStyle.buttonText,{fontWeight:'500',marginBottom:5,marginLeft:10}]}>
                        Thoughts for PwF Connect:
                    </Text>
                  )}
                { !_.has(thoughtValue, 'message_id') && (
                    <Text style={[localStyle.buttonText,{fontWeight:'500',marginBottom:5,marginLeft:10}]}>
                      Thoughts:
                    </Text>
                  )}
                  <View style={{borderWidth:2,borderColor:'#2799bb',width:width/1.3,height:height/5,backgroundColor:'#e5e9ea'}}>
                      <Text style={{color:'#4d6e7a',fontSize:18,backgroundColor:'#abe6f4',padding:10}}>
                          {  _.has(thoughtValue, 'thoughts') ? thoughtValue.thoughts :'' }
                      </Text>
                      {/* _.has(thoughtValue, 'message_id') && (
                      <TouchableOpacity onPress={this.changePage.bind(this,false)} style={{ flex: 1,justifyContent:'flex-end',alignItems:'flex-end',padding:5}}>
                        <Text style={{fontSize:18,color:'#6a7980'}}>Reply</Text>
                      </TouchableOpacity>
                    ) */}
                  </View>
              </View>

              </View>
            )  : (
              <View>
                     <View style={{flexDirection:'row',}}>
                       <TouchableOpacity style={{marginTop:height/50}} onPress={this.changePage.bind(this,true)}>
                          <Image source={Images.smallBackIcon} />
                        </TouchableOpacity>
                        <Text style={{color:'#5fb0c6',fontSize:20,marginLeft:10,flex:3,marginTop:height/50}}>thoughts for PwF</Text>
                        {/*<TouchableOpacity style={{width:50,height:50,justifyContent:'space-between',marginLeft:(Platform.OS === 'ios' ? 120 : 70)}} onPress={this.onModalClose.bind(this)}>
                          <Image  source={Images.orangeCloseIcon} />
                        </TouchableOpacity>*/}
                        {/*<TextButton
                            label={"Send"}
                            onPress={this.onCheck.bind(this,onCheckMarkPress)}
                            style={[localStyle.buttonStyle,{borderRadius:5,justifyContent:'center',alignItems:'center'}]}
                            loadingColor='transparent'
                            primary
                        />*/}

                    </View>
                    <View style={{height:height/5}}>
                        <Text style={{color:'#e87f47',fontSize:20,marginTop:height/32,marginBottom:5}}>{healthStateArrayForModal[healthStatusId]}</Text>
                        <View style={{height:100,width:width/1.32,backgroundColor:'#abe6f4',flexDirection:'row',padding:5}}>
                          <Text style={{color:'#4d6e7a',fontSize:18}}>
                              {  _.has(thoughtValue, 'thoughts') ? thoughtValue.thoughts :'' }
                          </Text>
                          <View style={{backgroundColor:'#abe6f4',width:10,height:10,marginTop:85,borderTopRightRadius:30,marginLeft:13}}></View>
                        </View>
                    </View>
                    <View style={{marginTop:35}}>
                        <TextInput
                            multiline={true}
                            numberOfLines={4}
                            style={{padding:5,backgroundColor:'#e8eced',borderWidth:2,borderColor:'#add1e0',width:width/1.3,height:height/5}}
                            placeholder="Enter Message"
                            placeholderTextColor="#446a7a"
                            underlineColorAndroid='transparent'

                        />
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'flex-end',marginTop:10}}>
                       <Text style={{color:'#5fb0c6',fontSize:20,marginLeft:10,flex:3,marginTop:height/50}}></Text>
                        <TextButton
                            label={"Send"}
                            onPress={this.onCheck.bind(this,onCheckMarkPress)}
                            style={[localStyle.buttonStyle,{borderRadius:5,justifyContent:'center',alignItems:'center'}]}
                            loadingColor='transparent'
                            primary
                        />
                      {/*  <Image style={{marginTop:2,marginRight:10}}source={Images.rightArrowIcon} />
                        <TouchableOpacity onPress={this.onMessage.bind(this,onMessagePress)}>
                            <Image style={{marginTop:2}} source={Images.chatIcon} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{width:50,height:50}} onPress={this.onCheck.bind(this,onCheckMarkPress)}>
                          <Image style={{marginRight:5,marginTop:2,marginLeft:20}} source={Images.checkMarkIcon} />
                        </TouchableOpacity>*/}
                    </View>

                    </View>
                    )}
        </View>
    )};

    onCheck = (onCheckMarkPress) =>{
      this.setState({visibleModal:false});
      onCheckMarkPress()
    }

    onMessage = (onMessagePress) =>{
      this.setState({visibleModal:false});
      onMessagePress()
    }

    onModalPress = (visibleModal,healthState,value) =>{
       this.setState({visibleModal:visibleModal, healthStatusId:healthState,thoughtValue:value})
     }

    onModalClose = () =>{
      this.setState({visibleModal:false})
    }


  render (){
    let {

        onHomePress,
        onAddCoachesPress,
        onShareWithCoachPress,
        onModalClose,
        onCalendarPress,
        onCheckMarkPress,
        onMessagePress

    } = this.props;
    // let customDataY=[];
    // let customDataX=[];
    // let date;
    // let customMin=[];
    let newData=[];
    let time = moment.duration("00:02:00"),arr=this.state.rhythmEntries,len=(arr).length;
  newData = this.state.rhythmEntries.map((e,i)=>{
    switch (true) {
          case (e.value == 2):
              return {
                value: 1.5,
                date: moment(e.created_date),
                all_data: e
              };
              break;
          case (e.value == 3):
              return {
                value: 2.25,
                date: moment(e.created_date),
                all_data: e
              };
              break;
          case (e.value == 4):
              return {
                value: 3.75,
                date: moment(e.created_date),
                all_data: e
              };
              break;
          case (e.value == 6):
              return {
                value: 6.25,
                date: moment(e.created_date),
                all_data: e
              };
              break;
          case (e.value == 7):
              return {
                value: 7.75,
                date: moment(e.created_date),
                all_data: e
              };
              break;
          case (e.value == 8):
              return {
                value: 8.5,
                date: moment(e.created_date),
                all_data: e
              };
              break;
          default:
              return {
                value: e.value,
                date: moment(e.created_date),
                all_data: e
              };
            break;
          }
    });

  if(newData[0] != undefined){
      //console.log("=============", newData[0].date);
      newData.unshift({
        value: 0,
        date: moment(newData[0].date).subtract(time),
        all_data: {}
      });

      newData.push({
        value: 10,
        date: moment(newData[newData.length - 1].date).add(time),
        all_data: {}
      });
    }
      console.log('newData',newData);
    //console.log('customDataX',reverseCustomDataY,customDataY.reverse());
    return (<View style={styles.container}>
        <StatusBar/>
        <ImageBackground  source={Images.chartBackground} resizeMode={Image.resizeMode.streach} style={{width:width, height:(Platform.OS === 'ios' ? (height-60) : (height-80))}}>
        <ScrollView  contentContainerStyle={localStyle.container}>
            <Modal isVisible={this.state.visibleModal}>
             {this._renderModalContent(onModalClose,'sick',onCheckMarkPress,onMessagePress,this.state.healthStatusId,this.state.thoughtValue)}
           </Modal>
           {!this.state.dataFetch && (
              <View style={{alignItems:'center',justifyContent:'center',marginTop:height/2}}>
                <ActivityIndicator size="large" color="#00bcec" />
              </View>

           )}
           {this.state.rhythmEntries.length == 0 && this.state.dataFetch && (
              <View style={{alignItems:'center',justifyContent:'center',marginTop:height/2,backgroundColor:'transparent'}}>
                <Text style={{color:'#9ea0a3',fontSize:22}}>No records found for selected day!</Text>
              </View>

           )}
           {this.state.rhythmEntries.length > 0 && (


                         <View style={{height: (Platform.OS === 'ios' ? (aspectRatio >2.1 ? height/1.7 : height/1.90)  : height/1.85),width:(Platform.OS === 'ios' ? width : width),top:(Platform.OS === 'ios' ? 85 : 120),right:(Platform.OS === 'ios' ? 5 : 0),bottom:20,transform: [{ rotate: '90deg'}] }}>
                             <LineChart
                               style={(Platform.OS === 'ios' ? localStyle.areaStyleIos : localStyle.areaStyleAndroid)}
                               data={newData}
                               showGrid={false}
                               yAccessor={({ item }) => item.value}
                               xAccessor={({ item }) => item.date}
                               xScale={scale.scaleTime}
                               contentInset={{ top: 10, bottom: -25, left: (Platform.OS === 'ios' ? 18 : 15), right: 15}}
                               svg={ {
                                   fill: 'transparent',
                                   stroke: '#616162',
                                   strokeWidth:3

                               } }
                               curve={shape.curveCardinalOpen.tension(0.5)}
                               extras={ [(({ y }) => (
                                          <G key={ 'dash' }>
                                           <Line
                                               key={ 'zero-axis' }
                                               x1={ '0%' }
                                               x2={ '100%' }
                                               y1={ y(7) }
                                               y2={ y(7) }
                                               stroke={ '#00bded' }
                                               strokeDasharray={ [ 10, 10 ] }
                                               strokeWidth={ 2 }
                                           />
                                           <Line
                                               key={ 'zero-axis-line' }
                                               x1={ '0%' }
                                               x2={ '100%' }
                                               y1={ y(3) }
                                               y2={ y(3) }
                                               stroke={ '#00bded' }
                                               strokeDasharray={ [ 10, 10 ] }
                                               strokeWidth={ 2 }
                                           />
                                         </G>
                                       ))] }
                                       renderDecorator={ ({ x, y, index, value }) => (value.value == 0 || value.value ==10) ?  null :
                                         (
                                         ((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) ) ? (
                                               [(
                                                 // this is for hitSlop
                                                 <Circle
                                                     onPressIn={((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) )
                                                       ? this.onModalPress.bind(this,true,value.all_data.value,value.all_data)
                                                       : this.onModalPress.bind(this,false,'','') }
                                                     key={ index + '.2' }
                                                     cx={ x(value.date)}
                                                     cy={ y(value.value) }
                                                     r={ 15 }
                                                     stroke="transparent"
                                                     strokeWidth={2}
                                                     fill="transparent"
                                                     />
                                                   ),(
                                                     <Circle
                                                         onPressIn={((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) )
                                                           ? this.onModalPress.bind(this,true,value.all_data.value,value.all_data)
                                                           : this.onModalPress.bind(this,false,'','') }
                                                         key={ index + '.1' }
                                                         cx={ x(value.date)}
                                                         cy={ y(value.value) }
                                                         r={ 6 }
                                                         stroke={ ((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) )  ? '#616162' : '#61bce7' }
                                                         strokeWidth={2}
                                                         fill={ ((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) ) ? '#f8ec07' : 'white' }
                                                     />
                                                   )]) : ([(
                                                         <Circle
                                                             onPress={((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) )
                                                               ? this.onModalPress.bind(this,true,value.all_data.value,value.all_data)
                                                               : this.onModalPress.bind(this,false,'','') }
                                                             key={ index + '.3' }
                                                             cx={ x(value.date)}
                                                             cy={ y(value.value) }
                                                             r={ 6 }
                                                             stroke={ ((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) )  ? '#616162' : '#61bce7' }
                                                             strokeWidth={2}
                                                             fill={ ((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) ) ? '#f8ec07' : 'white' }
                                                         />
                                                       )])
                                         ) }
                             />

                             <XAxis
                               data={newData}
                               svg={{
                                   fill: '#7c7d7f',
                                   fontSize: 10,
                                   fontWeight: 'bold',
                                   rotation: -90,
                                   originY:(Platform.OS === 'ios' ? 24 :20),
                                   y: 1,
                               }}
                               xAccessor={({ item }) => item.date}
                               scale={scale.scaleTime}
                               numberOfTicks={6}
                               style={(Platform.OS === 'ios' ? localStyle.axisStyleIos:localStyle.axisStyleAndroid)}
                               contentInset={{ left: (Platform.OS === 'ios' ? 20 :18),right:20 }}
                               formatLabel={(value) => moment(value).format("hh:mm a").replace(' ','') }
                             />

                         </View>



                  )
                }


                  {/*<View style={[localStyle.homeButtonPanel,{backgroundColor:'red'}]}>
                  <HomeButton
                        onPress={onHomePress}
                        style={localStyle.buttonStyle}
                        loadingColor='#07c4f0'
                        primary
                    />
                </View>*/}

        </ScrollView>
         </ImageBackground>


          <View style={{height: 60, backgroundColor: this.state.dayEnd ? '#384a54' : '#4cc5e7',alignItems:'center',justifyContent:'center',flexDirection:'row',justifyContent:'space-between'}} >

             <Text style={{color:'white',fontSize:20,marginLeft:10}}>{
               this.state.dayEnd ? ('Today' ) :('Month Avg.')
             }</Text>

          </View>

    </View>
    );
  }

}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
    getRhythmByClient: rhythmSelectors.getRhythmByClient(state)
});

export default connect(mapStateToProps, {
    ...rhythmActions
})(DayEndGraphChartView);
