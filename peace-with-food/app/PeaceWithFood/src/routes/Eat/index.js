import React from 'react';

import {
    Text,
    View,
    Dimensions,
    Platform,
    TouchableOpacity,
    Image

} from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';

import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import EatView from './EatView';
import {connect} from 'react-redux';
import {actions as authActions, selectors} from 'PeaceWithFood/src/store/auth';
import {actions as userActions} from 'PeaceWithFood/src/store/user';
import styles from 'PeaceWithFood/src/styles';
import { Colors, Sizes } from 'PeaceWithFood/src/styles/constants';
var {height, width} = Dimensions.get('window');
import {NavigationActions} from 'react-navigation';
import {Images} from 'PeaceWithFood/src/components/icons';
let routeName='Dashboard';

class EatContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        // title: Platform.OS === 'ios' ? <View style={{alignItems:'center',justifyContent:'center',width:(width/1.5),height:50,flexDirection:'column'}}><Text style={{color:'#53acc7',fontSize:18,fontWeight:'600'}}>WHERE ARE YOU NOW</Text>
        //                                <Text style={{color:'#53acc7',fontSize:18,fontWeight:'600'}}>
        //                                AS YOU BEGIN?
        //                                </Text>
        //                                </View> : <Text style={{color:'#53acc7',fontSize:14,}}>WHERE ARE YOU NOW AS YOU BEGIN?</Text> ,
        headerLeft: null,
        headerRight:<TouchableOpacity style={{height:40,width:40,justifyContent:'center',alignItems:'center'}} onPress={() =>
            navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName })
            ]
          }))}><Image source={Images.closeIcon}/></TouchableOpacity>,
        headerStyle: {
            backgroundColor: '#EFF3F4',
            borderBottomWidth: 0
        },
        headerTitleStyle: styles.headerTitleStyle,
        gesturesEnabled: false
    });

    componentWillReceiveProps(nextProps) {
        if(!nextProps.isLoggedIn) {
            this.resetRoute("Splash");
        }
    }

    async componentDidMount() {

    }

    render() {
        return <EatView
            {...this.props}
        />
    }
}

const mapStateToProps = (state) => ({
    isLoggedIn: selectors.getIsLoggedIn(state)
});

export default connect(mapStateToProps, {
    ...authActions,
    ...userActions
})(EatContainer);
