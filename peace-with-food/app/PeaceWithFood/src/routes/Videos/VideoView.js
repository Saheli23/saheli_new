import React,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    WebView,
    ScrollView,
    FlatList,
    ActivityIndicator
} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
import {actions as rhythmActions,selectors as rhythmSelectors} from 'PeaceWithFood/src/store/rhythm';
import {connect} from 'react-redux';
var {height, width} = Dimensions.get('window');

const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dce0e2'
    },
    section: {
        flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#446a7a',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? 20 : 10),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      videoPanel:{
        height: 300,
        width:width/1.1,

        //marginLeft:10,
        //marginTop:10
        //paddingLeft:30,
        //paddingRight:30,
      //  paddingTop:20
      },
      qusAnsBlock:{
        //paddingLeft:40,
        //marginTop:10,
      //  height:60,
      },
      question:{
        color:'#53acc7',
        fontSize:(Platform.OS === 'ios' ? 32 : 25),
        fontWeight:'600',
        fontFamily:'Sarabun-Bold',
        textAlign:'center'
      },
      answer:{
        color:'#95989a',
        fontSize:18,
        marginTop:10
      },
      scrollPanel:{
        height:height/1.2,
        justifyContent:'center',
        alignItems:'center'
      }
});

class VideoView extends Component {
        constructor(props){
          super(props);
          this.state ={
            videoDetails:[],
            dataFetch:false,
          }
         }

         componentWillMount() {

           this.props.getVideos();
         }


        componentWillReceiveProps(nextProps) {
          //console.log('videodata',nextProps);
            if(nextProps.getVideosValue) {
                this.setState({videoDetails:nextProps.getVideosValue,dataFetch:true});
              // console.log('videodata',nextProps.getVideosValue);
              }
          }
        render (){
          let {

              onHomePress
              } = this.props;
              // data={[{id : 1, videolink:'https://www.youtube.com/embed/DW1v0DRQ3IQ?rel=0&autoplay=0&showinfo=0&controls=1',title:'What About My Weight?'},
              //        {id : 2, videolink:'https://www.youtube.com/embed/Jq0oW_UnmDw?rel=0&autoplay=0&showinfo=0&controls=1',title:'What Do I Eat?'},
              //        {id : 3, videolink:'https://www.youtube.com/embed/VYfhGpvvEjg?rel=0&autoplay=0&showinfo=0&controls=1',title:'What Is Intuitive Eating?'},
              //        {id : 4, videolink:'https://www.youtube.com/embed/iZt2LRgd5cI?rel=0&autoplay=0&showinfo=0&controls=1',title:'Do You Have Peace With Food?'},]}
        return  (
          <View style={styles.container}>
              <StatusBar/>
              <View style={localStyle.container} contentContainerStyle={localStyle.container}>

                  <View style={[localStyle.section, localStyle.bottomSection]}>
                    {/*<ScrollView style={localStyle.videoPanel}>
                        <WebView
                            source={{ uri: `https://www.youtube.com/watch?v=DW1v0DRQ3IQ` }}
                            style={{marginTop: 70,}}
                          />
                          <WebView
                              source={{ uri: `https://www.youtube.com/watch?v=DW1v0DRQ3IQ` }}
                              style={{marginTop: 70,}}
                            />
                            <WebView
                                source={{ uri: `https://www.youtube.com/watch?v=DW1v0DRQ3IQ` }}
                                style={{marginTop: 70,}}
                              />
                      </ScrollView>*/}
                      <View style={localStyle.scrollPanel}>
                          {!this.state.dataFetch && (
                             <View style={{alignItems:'center',justifyContent:'center',marginTop:height/2}}>
                               <ActivityIndicator size="large" color="#00bcec" />
                             </View>

                          )}
                          <FlatList
                          style={[{marginTop:70,}]}
                          horizontal={false}
                          data={this.state.videoDetails}
                          keyExtractor={(item, index) => index}
                          renderItem={({item}) => <View style={[localStyle.videoPanel,]} >
                          <View style={[localStyle.qusAnsBlock,{marginTop:20}]}>
                             <Text style={localStyle.question}>{item.title}</Text>
                         </View>
                                                            <WebView javaScriptEnabled={true} source={{uri: item.videolink}}  />

                                                       </View> }
                            />
                      </View>
                    {/*  <View style={localStyle.qusAnsBlock}>
                         <Text style={localStyle.question}>What is intuitive Eating?</Text>
                         <Text style={localStyle.answer}>A short explanation of what all we are about.</Text>
                      </View>*/}
                      <View style={localStyle.homeButtonPanel}>
                          <HomeButton
                              onPress={onHomePress}
                              style={localStyle.buttonStyle}
                              loadingColor='#07c4f0'
                              primary
                          />
                      </View>

                  </View>
              </View>
          </View>
    );
  }
}

const mapStateToProps = (state, props) => ({

    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
    getVideosValue: rhythmSelectors.getVideosInfo(state)
});

export default connect(mapStateToProps, {
    ...rhythmActions
})(VideoView);
