import React from 'react';
import {Text,TouchableOpacity,Image,Platform,
Dimensions,View} from 'react-native';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import VideoView from './VideoView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
import {NavigationActions} from 'react-navigation';
import {Images} from 'PeaceWithFood/src/components/icons';
let routeName='Dashboard';
var {height, width} = Dimensions.get('window');

class VideoContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: <Text style={{color:'#53acc7'}}>VIDEOS</Text>,
        headerLeft: <View/>,
        headerRight:<TouchableOpacity style={{marginRight:10,height:40,width:40,justifyContent:'center',alignItems:'center'}} onPress={() =>
            navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName })
            ]
          }))}><Image source={Images.closeIcon}/></TouchableOpacity>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTitleStyle: {
            fontSize: 17,
            color: Colors.WHITE,
            alignSelf: 'center',
            textAlign: 'center',
            ...Platform.select({
              android: { width: width/1.8, },
            }),
            // width:(Platform.OS === 'ios' ? 0 : width/1.8)
        }
    });

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }
    onHomePress = () =>{
      this.resetRoute("Dashboard");
    }

    render() {

        return <VideoView
            {...this.state}
            onHomePress={this.onHomePress}
        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
});

export default connect(mapStateToProps, actions)(VideoContainer);
