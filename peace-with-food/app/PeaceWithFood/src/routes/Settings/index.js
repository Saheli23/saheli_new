import React from 'react';

import {
    Image,
    TouchableOpacity,
    View,
    AsyncStorage,
    Platform,
    Dimensions
} from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';

import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import SettingsView from './SettingsView';
import {connect} from 'react-redux';
import {actions as authActions, selectors} from 'PeaceWithFood/src/store/auth';
import {actions as userActions} from 'PeaceWithFood/src/store/user';
import styles from 'PeaceWithFood/src/styles';
import { Colors, Sizes } from 'PeaceWithFood/src/styles/constants';
import {Images} from 'PeaceWithFood/src/components/icons';
import {NavigationActions} from 'react-navigation';
let routeName='Dashboard';
var {height, width} = Dimensions.get('window');

class SettingsContainer extends NavComponent {
    constructor(props) {
      super(props);
      this.state = {
        support:true,
      };
    }
    static navigationOptions = ({navigation}) => ({
        title: 'MORE',
        headerRight:<TouchableOpacity style={{marginRight:10,height:40,width:40,justifyContent:'center',alignItems:'center'}} onPress={() =>
            navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName })
            ]
          }))}><Image source={Images.whiteCloseIcon}/></TouchableOpacity>,
        // headerLeft: <BackButton color={Colors.WHITE} onPress={() => navigation.dispatch({type: "Navigation/BACK"})}/>,
        headerLeft:<View/>,
        headerStyle: {
            backgroundColor: Colors.SETTINGSBACKGROUND,
            borderBottomWidth: 0.4,
            //borderBottomColor:'#04b6e2'
            borderBottomColor:'#00a8d2',
            shadowColor: '#0a90b2',
            shadowOffset: { width: 1, height: 1 },
            shadowOpacity: 0.8,
        },
        headerTitleStyle: {
            fontSize: 17,
            color: Colors.WHITE,
            alignSelf: 'center',
            textAlign: 'center',
            ...Platform.select({
              android: { width: width/1.8, },
            }),
        }
    });

    componentWillReceiveProps(nextProps) {
        if(!nextProps.isLoggedIn) {
            this.resetRoute("Splash");
        }
    }

    async componentDidMount() {

    }

    onAccountPress = () => {
        this.props.navigation.navigate("Account");
    };

    onFaqPress = () => {
        this.props.navigation.navigate("Faq",{support:false});
    };

    onSharePress = () => {
        this.props.navigation.navigate("Social");
    };

    onHelpPress = () => {
        this.props.navigation.navigate("Faq",{support:this.state.support});
    };
    onHomePress = () =>{
      this.props.navigation.navigate("Dashboard");
    }
    onFrequencyChangePress = () =>{
      this.props.navigation.navigate("FrequencyChange");
    }

    onLogoutPress = () => {
        Alert.alert(
            'Log Out',
            'Are you sure you want to log out of your Peace with Food account?',
            [
                {text: 'Cancel', style: 'cancel'},
                {text: 'Log Out', onPress: () => {
                    AsyncStorage.setItem('ShareCounter', '');
                    AsyncStorage.setItem('ShareCounterForOthers', '');
                    AsyncStorage.setItem('DeviceTime','sun');
                    this.props.logout();
                    this.props.resetAccount();
                }}
            ]
        );
    };

    render() {
        return <SettingsView
            {...this.props}
            onAccountPress={this.onAccountPress}
            onFaqPress={this.onFaqPress}
            onSharePress={this.onSharePress}
            onHelpPress={this.onHelpPress}
            onLogoutPress={this.onLogoutPress}
            onHomePress={this.onHomePress}
            onFrequencyChangePress={this.onFrequencyChangePress}
            {...this.state}
        />
    }
}

const mapStateToProps = (state) => ({
    isLoggedIn: selectors.getIsLoggedIn(state)
});

export default connect(mapStateToProps, {
    ...authActions,
    ...userActions
})(SettingsContainer);
