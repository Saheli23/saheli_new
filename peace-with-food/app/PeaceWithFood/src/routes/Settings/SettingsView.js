import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    Platform,
    Image,
    WebView
} from 'react-native';

import styles from 'PeaceWithFood/src/styles';
import { Colors, Sizes } from 'PeaceWithFood/src/styles/constants';
var {height, width} = Dimensions.get('window');
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import {Images} from 'PeaceWithFood/src/components/icons';

const SETTING_ROW_ICON_SIZE = 100;

const localStyles = StyleSheet.create({
    container: {
        padding: (Platform.OS === 'ios' ? Sizes.SCREEN_PADDING : 0),
        //backgroundColor:'#00bcec'
        backgroundColor:'#2799BB'
    },
    headerSection: {
        flex:(Platform.OS === 'ios' ? 0 : 0),
        justifyContent: 'center',
        alignItems: 'center',
    },
    footerSection: {
        alignItems: 'flex-end',
        //paddingVertical: 20,
        marginRight:(Platform.OS === 'ios' ? 0 : 15),
        marginBottom:(Platform.OS === 'ios' ? 0 : 0)
    },
    settingRow: {
       marginBottom: (Platform.OS === 'ios' ? 20 : 0),
       height:(Platform.OS === 'ios' ? height/8 : height/6.4),
       width:width,
       flexDirection: 'column',
       alignItems: 'center',
       justifyContent:'center',
       width:width,
       borderBottomWidth:1,
       borderLeftWidth:0,
       borderRightWidth:0,
       borderBottomColor:'#00a8d2',
       shadowColor: '#0a90b2',
       shadowOffset: { width: 1, height: 3 },
       shadowOpacity: 0.8,
       shadowRadius: 2,
       elevation: 1,
    },
    settingRowText: {
        //flex: 1,
        fontSize: (Platform.OS === 'ios' ? 20 : 18),
        //paddingLeft: 20,
        color:'white',
        alignItems: 'center',
        justifyContent:'center',
      //  height: SETTING_ROW_ICON_SIZE,
    },
    settingRowIconContainer: {
        width: SETTING_ROW_ICON_SIZE,
        height: SETTING_ROW_ICON_SIZE,
        backgroundColor: Colors.SETTINGS,
        borderRadius: SETTING_ROW_ICON_SIZE / 2,


    },
    logoutText: {
        color: 'white',
        fontSize: 20,
        //paddingVertical: (Platform.OS === 'ios' ? 20 : 20)
    },
    copyrightText:{
      color:'white',
    //  marginTop:(Platform.OS === 'ios' ? 10 : 0),
    //  marginBottom:(Platform.OS === 'ios' ? 0 : 20)
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#00a5d0',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? 30 : 50),
        height:45,
        width:width/1.3
    }
});

const SettingRowView = ({text, onPress, icon}) => (

    <TouchableOpacity onPress={onPress} style={localStyles.settingRow}>
        {/*<View style={localStyles.settingRowIconContainer}>

        </View>*/}
        {icon != Images.alarmIcon && (
          <Image
            style={{height:(height/12)}}
            source={icon}
          />
          )}
          {icon == Images.alarmIcon && (
            <Image
              source={icon}
            />
          )}
        <Text style={localStyles.settingRowText}>
            {text}
        </Text>

    </TouchableOpacity>
);

const SettingsView = ({
    onAccountPress,
    onFaqPress,
    onSharePress,
    onHelpPress,
    onLogoutPress,
    onHomePress,
    onFrequencyChangePress
}) => (
    <View style={[styles.container, localStyles.container]}>
        <View style={[localStyles.section, localStyles.headerSection]}>
        <SettingRowView text={'FREQUENCY CHANGE'} onPress={onFrequencyChangePress} icon={Images.alarmIcon} />
            <SettingRowView text={'QUESTIONS AND ANSWERS'} onPress={onFaqPress} icon={Images.faqIcon} />
            <SettingRowView text={'GET MORE HELP FROM EXPERTS'} onPress={onHelpPress} icon={Images.helpIcon} />
            <SettingRowView text={'ACCOUNT'} onPress={onAccountPress} icon={Images.accountIcon} />
                <SettingRowView text={'TELL A FRIEND'} onPress={onSharePress} icon={Images.tellFriendIcon} />
          {/*<View style={localStyles.homeButtonPanel}><HomeButton
                style={localStyles.buttonStyle}
                loadingColor='#07c4f0'
                primary
                onPress={onHomePress}
            /></View>*/}
        </View>

        <View style={[localStyles.footerSection]}>
            <TouchableOpacity onPress={onLogoutPress}>
                <Text style={localStyles.logoutText}>
                    Log out
                </Text>
            </TouchableOpacity>
            <Text style={localStyles.copyrightText}>
                Version X.X &copy; Peace with Food {new Date().getFullYear()}
            </Text>
        </View>
    </View>
);

export default SettingsView;
