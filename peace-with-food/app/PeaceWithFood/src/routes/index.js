import React from 'react';
import {StackNavigator, TabNavigator, TabBarBottom} from 'react-navigation';

import Splash from './Splash';
import Login from './Login';
import Signup from './Signup';
import Dashboard from './Dashboard';
import Settings from './Settings';
import Rhythm from './Rhythm';
import Videos  from './Videos';
import Social from './Social';
import Connect from './Connect';
import Coaches from './Coaches';
import SharedToCoaches from './SharedToCoaches';
import SharedGraphChart from './SharedGraphChart';
import Account from './Account';
import Reminder from './Reminder';
import SharedWithMe from './SharedWithMe';
import Sick from './Sick';
import Famished from './Famished';
import Meal from './Meal';
import NotConnected from './NotConnected';
import Charts from './Charts';
import CoachAdded from './CoachAdded';
import Eat from './Eat';
import AfterEatCheckIn from './AfterEatCheckIn';
import DayEnd from './DayEnd';
import AskToCheckIn from './AskToCheckIn';
import Conversation from './Conversation';
import GraphChart from './GraphChart';
import MessageQueue from './MessageQueue';
import MessageHistory from './MessageHistory';
import NotAllowToEat from './NotAllowToEat';
import FrequencyChange from './FrequencyChange';
import Faq from './Faq';
import TermsAndConditions from './TermsAndConditions';
import MonthAvgChart from './MonthAvgChart';
import DayEndGraphChart from './DayEndGraphChart';
import ManageSupporters from './ManageSupporters';
import SharedWithOthers from './SharedWithOthers';
import OthersSharedGraphChart from './OthersSharedGraphChart';
const firebase = require("firebase");

// Initialize Firebase
// var config = {
//   apiKey: "AIzaSyCBlPCxTPCrBQKrcbBC0R6e8VuNmq_Lgjg",
//   authDomain: "peace-with-food-db735.firebaseapp.com",
//   databaseURL: "https://peace-with-food-db735.firebaseio.com",
//   projectId: "peace-with-food-db735",
//   storageBucket: "",
//   messagingSenderId: "39591163312",
// }; commented out as it is firbase config of saheli's Account

var config = {
  apiKey: "AIzaSyAIHEaqDKtfRbCsUwzzxYkUU418hk8Cwus",
  authDomain: "peace-with-food.firebaseapp.com",
  databaseURL: "https://peace-with-food.firebaseio.com",
  projectId: "peace-with-food",
  storageBucket: "peace-with-food.appspot.com",
  messagingSenderId: "812627416494",
};

firebase.initializeApp(config);


export default (store) => {
    const AppNavigator = StackNavigator({
        Splash: {screen: Splash},
        Login: {screen: Login},
        Signup: {screen: Signup},
        Dashboard: {screen: Dashboard},
        Settings: {screen: Settings},
        Rhythm: {screen: Rhythm},
        Videos:{screen: Videos},
        Social:{screen:Social},
        Connect:{screen:Connect},
        Coaches:{screen:Coaches},
        SharedToCoaches:{screen:SharedToCoaches},
        SharedGraphChart:{screen:SharedGraphChart},
        Account:{screen:Account},
        Reminder:{screen:Reminder},
        SharedWithMe:{screen:SharedWithMe},
        Sick:{screen:Sick},
        Famished:{screen:Famished},
        Meal:{screen:Meal},
        NotConnected:{screen:NotConnected},
        Charts:{screen:Charts},
        CoachAdded:{screen:CoachAdded},
        Eat:{screen:Eat},
        AfterEatCheckIn:{screen:AfterEatCheckIn},
        DayEnd:{screen:DayEnd},
        AskToCheckIn:{screen:AskToCheckIn},
        Conversation:{screen:Conversation},
        GraphChart:{screen:GraphChart},
        MessageQueue:{screen:MessageQueue},
        MessageHistory:{screen:MessageHistory},
        NotAllowToEat:{screen:NotAllowToEat},
        FrequencyChange:{screen:FrequencyChange},
        Faq:{screen:Faq},
        TermsAndConditions:{screen:TermsAndConditions},
        MonthAvgChart:{screen:MonthAvgChart},
        DayEndGraphChart:{screen:DayEndGraphChart},
        ManageSupporters:{screen:ManageSupporters},
        SharedWithOthers:{screen:SharedWithOthers},
        OthersSharedGraphChart:{screen:OthersSharedGraphChart}
    }, {
        headerMode: 'float',
        mode: 'card',
        cardStyle: {
            backgroundColor: 'transparent'
        }
    });

    return StackNavigator({
        AppNavigator:   {screen: AppNavigator}
    }, {
        mode: 'modal',
        headerMode: 'none',
        cardStyle: {
            backgroundColor: 'transparent'
        },
    });
};
