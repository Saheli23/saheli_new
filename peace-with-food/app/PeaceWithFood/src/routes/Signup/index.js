import React from 'react';
import {
AsyncStorage
} from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import SignupView from './SignupView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
const firebase = require("firebase");

const rules = {
    fullname:'required',
    email: 'required|email',
    password: 'required',
    confirmpassword: 'required'
};

const validate = values => {
    const validator = new Validator(values, rules);
    validator.passes();
    return validator.errors.all();
};

const SignupViewWithForm = reduxForm({
    form: 'signup',
    fields: ['fullname','email', 'password', 'confirmpassword'],
    validate
})(SignupView);

class SignupContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: '',
        headerLeft: <BackButton color='#2799bb' onPress={() => navigation.dispatch({type: "Navigation/BACK"})}/>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTitleStyle: styles.headerTitleStyle
    });

    constructor(props){
        super(props);
        this.state = {
            isCoach: false,
            isTermsChecked: false,
            errorMessage:'',
            visibleModal: false,
            finishAllStep:false,
            fullname:'',
            email:'',
            password:''

        }
    }

    onSignupPress = ({fullname, email, password, confirmpassword}) => {
        if (password !== confirmpassword) {
            // TODO - Show error, passwords don't match
            Alert.alert(
                'Error Message',
                'Password and confirm password are not matching',
              )
            return;
        }

        if (!this.state.isTermsChecked) {
            Alert.alert(
                'Error Message',
                'You must first accept the terms & conditions',
              )
            return;
        }
        // this.setState({ visibleModal: true})
        // console.log(this.state.visibleModal);

        try {
          //commented out for remove professional popup
          //this.setState({ visibleModal: true,fullname:fullname, email:email, password:password})
          //console.log(this.state.visibleModal);
          
          //added for remove professional popup
           this.setState({ visibleModal: true,fullname:fullname, email:email, password:password,finishAllStep:true})
           this.props.signup(fullname, email, password, this.state.isCoach);
           // this.props.signup(fullname, email, password, this.state.isCoach);

        } catch (e) {
            console.log(e);
            // TODO - Problem signing up, show errors
        }
    };

    onIsCoachToggle = () => {
        this.setState({ isCoach: !this.state.isCoach })
    };

    onIsTermsToggle = () => {
        this.setState({ isTermsChecked: !this.state.isTermsChecked })
    };

    componentDidMount() {
        this.props.clearErrors();
    }

    componentWillReceiveProps(nextProps) {
        let firebasePassword='firebase';
        if(nextProps.isLoggedIn) {
            AsyncStorage.setItem('firstLogin','yes');
            this.resetRoute("Dashboard");
            // firebase.auth()
            //     .createUserWithEmailAndPassword(this.state.email, firebasePassword);
            //     firebase.auth()
            //         .signInWithEmailAndPassword(this.state.email, firebasePassword);
            this.resetRoute("Dashboard");
        }
        if(nextProps.error) {
        console.log('nextProps',nextProps.error.error);
        this.setState({errorMessage:nextProps.error.error});
        Alert.alert(
            'Please try again',
             nextProps.error.error,
          )

       }
    }

    onModalClose = () =>{
      this.setState({ visibleModal: false,finishAllStep:true})
       this.props.signup(this.state.fullname, this.state.email, this.state.password, this.state.isCoach);
    }

    render() {
        const {state} = this.props.navigation;
        const {
            fullname:fullnameError,
            email: emailError,
            password: passwordError,
            passwordConfirm: passwordConfirmError
        } = this.props.error || {};

        return <SignupViewWithForm
            {...this.state}
            onFullnameError={fullnameError}
            onEmailError={emailError}
            onPasswordError={passwordError}
            onPasswordConfirmError={passwordConfirmError}
            onSignupPress={this.onSignupPress}
            onIsCoachToggle={this.onIsCoachToggle}
            onIsTermsToggle={this.onIsTermsToggle}
            isCoach={this.state.isCoach}
            isTermsChecked={this.state.isTermsChecked}
            onModalClose={this.onModalClose}
            {...this.props}
        />;
    }
}

const mapStateToProps = (state, props) => ({
    isLoggedIn: selectors.getIsLoggedIn(state),
    isLoggingIn: selectors.getIsLoggingIn(state),
    error: selectors.getError(state)
});

export default connect(mapStateToProps, actions)(SignupContainer);
