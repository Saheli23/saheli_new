import React,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    Animated,
    Keyboard,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons'
import styles from 'PeaceWithFood/src/styles';
import CheckBox from 'react-native-check-box';
import Modal from 'react-native-modal';
import {Images} from 'PeaceWithFood/src/components/icons';
import {NavigationActions} from 'react-navigation';
var {height, width} = Dimensions.get('window');

const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dce0e2'
    },
    section: {
        flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
        // marginTop:(Platform.OS === 'ios' ? height/5 : height/7),
        alignItems:'center',
        flex: 1,
        minHeight: 150,
        maxHeight: 200,
        justifyContent: 'flex-end'
    },
    form: {

        ...Platform.select({
          android: { alignItems: 'center', },
        }),
        justifyContent:'center',

      //  flex: 1
      //   marginTop:(Platform.OS === 'ios' ? 80 : 30),
        paddingVertical: (Platform.OS === 'ios' ? 50 : 10),
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb',
        height:40
    },
    button:{
        backgroundColor:'#00bded',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#446a7a',
        fontSize: 18
    },
    termsText: {
        color: '#00bded',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      marginTop:(Platform.OS === 'ios' ? 10 : 10),
      marginBottom: (Platform.OS === 'ios' ? 10 : 10),
    },
    getStartedButton: {
        paddingTop: (Platform.OS === 'ios' ? 20 : 15)
    },
    modalContent: {
      backgroundColor: '#ebf6f9',
      padding: 22,
      // justifyContent: 'center',
      // alignItems: 'center',
      borderRadius: 4,
      borderColor: 'rgba(0, 0, 0, 0.1)',

},
});


let tips=false;
class SignupView extends Component {

  _renderModalContent = (onModalClose,isCoach,onIsCoachToggle) => (
      <View style={localStyle.modalContent}>
                  <View>
                   <View style={{flexDirection:'row',justifyContent:'flex-end'}}>

                  {/*  <TouchableOpacity onPress={onModalClose}>
                      <Image source={Images.closeIcon} />
                    </TouchableOpacity>*/}
                  </View>
                  <View  style={[localStyle.tipsPanel,{marginTop:(Platform.OS === 'ios' ? 10 : 0)}]}>
                  <CheckBox
                        style={{alignItems:'center',marginRight:10,flex:1}}
                        onClick={onIsCoachToggle}
                        isChecked={isCoach}
                        checkBoxColor='#13d0f6'
                        rightText='I am using this app as an Intuitive Eating Professional'
                        rightTextStyle={{color: '#446a7a',fontSize: 18,flexDirection:'column',flexWrap:'wrap'}}

                    />
                    {/*<View style={{flexDirection:'column'}}>
                      <Text style={localStyle.buttonText}>
                          I am using this app as an
                      </Text>
                      <Text style={localStyle.buttonText}>
                          Intuitive Eating Professional
                      </Text>
                    </View>*/}
                  </View>
                   <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
                     <TouchableOpacity style={{flexDirection:'row',marginBottom:5,marginTop:10,}} onPress={onModalClose}>
                        {!isCoach && (
                       <Text style={[localStyle.buttonText,{fontWeight:'500',color:'green'}]}>No, Thanks!</Text>
                       )}
                       {isCoach && (
                      <Text style={[localStyle.buttonText,{fontWeight:'500',color:'green'}]}>Got it!</Text>
                      )}
                      </TouchableOpacity>
                    </View>
              </View>
        </View>
    );
    render (){
      let {
        hasAccount = true,
        onEmailError,
        onPasswordError,
        onPasswordConfirmError,
        onFullnameError,
        onSignupPress,
        isSigningUp,
        handleSubmit,
        onIsCoachToggle,
        isCoach,
        onIsTermsToggle,
        isTermsChecked,
        visibleModal,
        onModalClose
    } = this.props;
    if (Platform.OS === 'ios') {
    return (
        <View style={styles.container}>
            <StatusBar/>
            <KeyboardAvoidingView
              style={localStyle.container}
              behavior="padding"
            >
            <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>
            {/*.......AS PER CLIENT'S REQUEST, REMOVING PROFFESIONAL POP-UP.......*/}
              {/* <Modal isVisible={visibleModal}>
               {this._renderModalContent(onModalClose,isCoach,onIsCoachToggle)}
               </Modal> */}
                <View style={[localStyle.section, localStyle.bottomSection]}>
                    <View style={localStyle.logoPanel}>
                        <LogoIconBlue/>
                    </View>
                    <View style={localStyle.form}>
                        <View style={localStyle.row}>
                            <TextInput
                                fieldName="fullname"
                                numberOfLines={1}
                                style={localStyle.inputText}
                                placeholder="Full name"
                                placeholderTextColor="#446a7a"
                                editable={!isSigningUp}
                                error={onFullnameError}
                            />
                        </View>
                        <View style={localStyle.row}>
                            <TextInput
                                fieldName="email"
                                numberOfLines={1}
                                style={localStyle.inputText}
                                placeholder="Email address"
                                placeholderTextColor="#446a7a"
                                editable={!isSigningUp}
                                error={onEmailError}
                            />
                        </View>
                        <View style={localStyle.row}>
                            <TextInput
                                fieldName="password"
                                numberOfLines={1}
                                style={localStyle.inputText}
                                placeholder="Password"
                                placeholderTextColor="#446a7a"
                                editable={!isSigningUp}
                                error={onPasswordError}
                                secureTextEntry
                            />
                        </View>
                        <View style={localStyle.row}>
                            <TextInput
                                fieldName="confirmpassword"
                                numberOfLines={1}
                                style={localStyle.inputText}
                                placeholder="Confirm password"
                                placeholderTextColor="#446a7a"
                                editable={!isSigningUp}
                                error={onPasswordConfirmError}
                                secureTextEntry
                            />
                        </View>
                        <View style={[localStyle.tipsPanel,{width:width/1.5,marginLeft:width/6,}]}>
                        {/*  <View style={{flexDirection:'column'}}>
                            <Text style={localStyle.buttonText}>
                                Get health tips and updates
                            </Text>
                            <Text style={localStyle.buttonText}>
                             from Peace With Food.
                            </Text>
                          </View>*/}
                        </View>
                      {/*  <View  style={[localStyle.tipsPanel,{marginTop:(Platform.OS === 'ios' ? 10 : 0)}]}>
                        <CheckBox
                              style={{alignItems:'center',marginRight:10}}
                              onClick={onIsCoachToggle}
                              isChecked={isCoach}
                              checkBoxColor='#13d0f6'

                          />
                          <View style={{flexDirection:'column'}}>
                            <Text style={localStyle.buttonText}>
                                I am using this app as an
                            </Text>
                            <Text style={localStyle.buttonText}>
                                Intuitive Eating Professional
                            </Text>
                          </View>
                        </View> */}
                        <View  style={[localStyle.tipsPanel,{width:width/1.4,marginLeft:width/6,height:height/15,flexDirection:'column',}]}>
                            <CheckBox
                                style={{alignItems:'center',flex: 1,}}
                                onClick={onIsTermsToggle}
                                isChecked={isTermsChecked}
                                checkBoxColor='#13d0f6'
                                rightText='I have read and agree to the'
                                rightTextStyle={{color: '#446a7a',fontSize: 18,flexDirection:'column',flexWrap:'wrap'}}

                            />
                            <TouchableOpacity style={{flexDirection:'column'}} onPress= {() => this.props.navigation.navigate('TermsAndConditions')}>
                              <Text style={[localStyle.termsText,{alignItems: 'center',justifyContent:'center'}]}>
                                    Terms and Conditions
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[localStyle.row, localStyle.getStartedButton]}>
                            <TextButton
                                label={"Let's Get Started"}
                                style={localStyle.button}
                                onPress={handleSubmit(onSignupPress)}
                                isLoading={isSigningUp}
                                loadingColor='#00bded'
                                primary
                                labelStyle={{fontSize:22}}
                            />
                        </View>
                    </View>
                </View>
            </KeyboardAwareScrollView>
          </KeyboardAvoidingView>
        </View>
    );
    }
return (
  <View style={styles.container}>
      <StatusBar/>
      <KeyboardAvoidingView
        style={localStyle.container}
        behavior="padding"
      >
      <ScrollView>
       {/*.......AS PER CLIENT'S REQUEST, REMOVING PROFFESIONAL POP-UP.......*/}
      {/*  <Modal isVisible={visibleModal}>
         {this._renderModalContent(onModalClose,isCoach,onIsCoachToggle)}
         </Modal>*/}
          <View style={[localStyle.section, localStyle.bottomSection]}>
              <View style={localStyle.logoPanel}>
                  <LogoIconBlue/>
              </View>
              <View style={localStyle.form}>
                  <View style={localStyle.row}>
                      <TextInput
                          fieldName="fullname"
                          numberOfLines={1}
                          style={localStyle.inputText}
                          placeholder="Full name"
                          placeholderTextColor="#446a7a"
                          editable={!isSigningUp}
                          error={onFullnameError}
                      />
                  </View>
                  <View style={localStyle.row}>
                      <TextInput
                          fieldName="email"
                          numberOfLines={1}
                          style={localStyle.inputText}
                          placeholder="Email address"
                          placeholderTextColor="#446a7a"
                          editable={!isSigningUp}
                          error={onEmailError}
                      />
                  </View>
                  <View style={localStyle.row}>
                      <TextInput
                          fieldName="password"
                          numberOfLines={1}
                          style={localStyle.inputText}
                          placeholder="Password"
                          placeholderTextColor="#446a7a"
                          editable={!isSigningUp}
                          error={onPasswordError}
                          secureTextEntry
                      />
                  </View>
                  <View style={localStyle.row}>
                      <TextInput
                          fieldName="confirmpassword"
                          numberOfLines={1}
                          style={localStyle.inputText}
                          placeholder="Confirm password"
                          placeholderTextColor="#446a7a"
                          editable={!isSigningUp}
                          error={onPasswordConfirmError}
                          secureTextEntry
                      />
                  </View>
                  {/* commented out for client feedback mail

                    <View style={[localStyle.tipsPanel,{width:width/1.2,marginLeft:width/9,}]}>
                    <CheckBox
                          style={{alignItems:'center',marginRight:10,flex:1}}
                          onClick={()=>tips ? false : true}
                          isChecked={tips}
                          checkBoxColor='#00bded'
                          rightText='Get health tips and updates from Peace With Food.'
                          rightTextStyle={{color: '#446a7a',fontSize: 18,flexDirection:'column',flexWrap:'wrap'}}

                      />

                  </View>*/}
                {/*  <View  style={[localStyle.tipsPanel,{marginTop:(Platform.OS === 'ios' ? 10 : 0)}]}>
                  <CheckBox
                        style={{alignItems:'center',marginRight:10}}
                        onClick={onIsCoachToggle}
                        isChecked={isCoach}
                        checkBoxColor='#13d0f6'

                    />
                    <View style={{flexDirection:'column'}}>
                      <Text style={localStyle.buttonText}>
                          I am using this app as an
                      </Text>
                      <Text style={localStyle.buttonText}>
                          Intuitive Eating Professional
                      </Text>
                    </View>
                  </View> */}
                  <View  style={[localStyle.tipsPanel,{width:width/1.32,height:height/12,flexDirection:'column',alignItems: 'center',justifyContent:'center',}]}>
                      <CheckBox
                          style={{alignItems:'center',marginRight:10,flex:1}}
                          onClick={onIsTermsToggle}
                          isChecked={isTermsChecked}
                          checkBoxColor='#13d0f6'
                          rightText='I have read and agree to the'
                          rightTextStyle={{color: '#446a7a',fontSize: 18,flexDirection:'column',flexWrap:'wrap'}}

                      />
                      <TouchableOpacity style={{flexDirection:'column'}} onPress= {() => this.props.navigation.navigate('TermsAndConditions')}>
                            <Text style={[localStyle.termsText,{alignItems: 'center',justifyContent:'center',}]}>
                                Terms and Conditions
                            </Text>
                        </TouchableOpacity>
                  </View>
                  <View style={[localStyle.row, localStyle.getStartedButton]}>
                      <TextButton
                          label={"Let's Get Started"}
                          style={localStyle.button}
                          onPress={handleSubmit(onSignupPress)}
                          isLoading={isSigningUp}
                          loadingColor='#00bded'
                          primary
                          labelStyle={{fontSize:22}}
                      />
                  </View>
              </View>
          </View>
      </ScrollView>
    </KeyboardAvoidingView>
  </View>
)
}

}
export default SignupView;
