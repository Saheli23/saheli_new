import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    AsyncStorage,
    ActivityIndicator

} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import _ from 'underscore';
import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';
import {NavigationActions} from 'react-navigation';
const firebase = require("firebase");
import {connect} from 'react-redux';
import {actions as connectActions, selectors as connectSelectors} from 'PeaceWithFood/src/store/pwfconnect';
import moment from 'moment';

let blocks=[];
const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dce0e2'
    },
    section: {
      //  flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? 70 : 50),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,
        alignItems: 'center',
        justifyContent:'center',
        flexDirection:'column',

      },
      borderPanel:{
        height:height/1.2,
        marginTop:(Platform.OS === 'ios' ? height/10 : height/8),
        marginBottom:(Platform.OS === 'ios' ? 10 : 10),
        marginLeft:15,
        marginRight:15,
        // borderBottomWidth:2,
        // borderBottomColor:'#2799bb',
        borderTopWidth:2,
        borderTopColor:'#2799bb'
      },
      answer:{
        color:'#627078',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#a2a4a5',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: 'white',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
      checkPanel:{
        // alignItems: 'center',
        // justifyContent:'center',
        paddingTop:20,
        paddingLeft:20,
        // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
        flexDirection:'row'
      },
      sharedBlock:{
        //padding:40,
        alignItems: 'center',
        justifyContent:'space-between',
        marginTop:5,
        height:35,
        width:width/1.2,
        flexDirection:'row',
        flexWrap: "wrap",
        backgroundColor:'#eaeced',

      },
      timeText:{
        color:'#2799bb',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
        marginRight:5,
      },
      nameText:{
        color:'#447990',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
      }

});


let messageItems =[];
let mail=[];
let temp1 = [];
let count =0;
let content =[];
class ConversationView extends Component {

  constructor(props){
    super(props);
    this.state = {
      chatPanelOpen:false,
      messageItems:[],
      dataFetch:false,
      coaches:[],
      userId:null,
      chatPanelOpen:false,
      chatOpen:-1
    }
    this.chatRef = firebase.database().ref().child('messages/');
    this.chatRefData = this.chatRef.orderByChild('child_added')
  }
  componentWillMount() {
    AsyncStorage.getItem('@pwf/user').then((value) =>{
      if(value){
        user =JSON.parse(value);
        let firebasePassword ='firebase';
        let email=user.email
          this.setState({userEmail:user.email,userId:user.id});
          const promise = this.props.getMyCoaches( user.id );
          const self    = this;
          promise.then(function(coaches){
            self.setState({'coaches':coaches,});
          });
        }
      });

  }

  componentDidMount(){
    var query = this.chatRef;
     let self=this;
     let items=[];

    query.on("child_added", function(snapshot) {

      var messageId = snapshot.key;
      var snapshotval = snapshot.val();
      let loggedUser=_.filter(snapshotval, function(e){return (e.user.email == self.state.userEmail  ); });
      if(loggedUser.length>0){
      content.push({message_id:snapshot.key,message_details:loggedUser});
      }
      let shared=_.filter(snapshotval, function(e){
        let ids=_.pluck(e.sharedCoaches,'userId')

          if(self.state.userId !=null){
            if(_.indexOf(ids,self.state.userId.toString())!=-1)
              return e;
             }
            });

       if(shared.length > 0)
       {
        content.push({message_id:snapshot.key,message_details:shared});
        }

        items.push({message_id:snapshot.key,message_details:[snapshot.val()]})
        self.setState({messageItems:content,dataFetch:true})

    })

  }


  onChatPress = (i) =>{
    this.setState({chatOpen : i,chatPanelOpen: !this.state.chatPanelOpen});
  }
  onMessagePress = (messageId)=>{
    this.props.navigation.navigate('MessageHistory',{messageDetailsId:messageId});
  }
  render (){
    let {

        onHomePress,
        onAddCoachesPress,
        onShareWithCoachPress,
        visibleModal,
        onModalClose,

    } = this.props;
      let self=this;
      var grouped = _.mapObject(_.groupBy(this.state.messageItems, function(e){  return e.message_details[0].user.email;})
                        );
       if(!_.isEmpty(grouped)){
       let keys=Object.keys(grouped);

      if(keys.length>0){
       var blocks =_.map( keys, function( val, k ){
        return (
            <View key={k}>
            <TouchableOpacity  style={localStyle.sharedBlock}>
                     <View style={{backgroundColor:'#9fc1ce',height: 25, width: 25, borderRadius: 25/2, marginLeft:(Platform.OS === 'ios' ? -10 : 0), justifyContent:'center', alignItems:'center'}}>
                        <Text style={[localStyle.timeText,{marginLeft:0,marginRight:0}]}></Text>
                     </View>
                     <Text style={[localStyle.nameText,{marginRight:5}]}>{val.split("@")[0].charAt(0).toUpperCase() + val.split("@")[0].slice(1)}</Text>

                     <TouchableOpacity onPress={self.onChatPress.bind(self,k)}>
                        <Image
                              style={{marginRight:5,}}
                              source={(self.state.chatOpen == k && self.state.chatPanelOpen) ? Images.upArrow : Images.downArrow }
                          />
                      </TouchableOpacity>



            </TouchableOpacity>
            {(self.state.chatOpen == k && self.state.chatPanelOpen ) ?
              (

               _.map( grouped[val].reverse(), function( subgrp, l ){


             return  (
              <View key={l} style={{flexDirection:'column',alignItems:'center',justifyContent:'center',marginTop:10}}>
                 <View style={{flexDirection:'row'}}>
                     <View style={{backgroundColor: '#9fc1ce', height: 15, width: 15, borderRadius: 15/2, marginTop: 10}}>
                     </View>
                     <TouchableOpacity onPress={self.onMessagePress.bind(self,subgrp.message_id)}>
                     <Text
                         style={[localStyle.nameText,{marginLeft:15,marginRight:0,marginTop:5}]}>
                           {moment(subgrp.message_details[0].createdAt).format("MMM Do h:mm a")}
                     </Text>
                     </TouchableOpacity>
                 </View>
              </View>
            );

          })
        ): null
        }
            </View>
            );
       })
     }

   }
    


    return (<View style={styles.container}>
        <StatusBar/>
        <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>

            <View style={[localStyle.section, localStyle.bottomSection]}>
             <View style={localStyle.borderPanel}>
              <ScrollView contentContainerStyle={localStyle.sharePanel}>
              {!this.state.dataFetch && (
                 <View style={{alignItems:'center',justifyContent:'center',marginTop:height/2}}>
                   <ActivityIndicator size="large" color="#00bcec" />
                 </View>

              )}
              {this.state.messageItems  && (
                blocks
              )}

              </ScrollView>
              </View>
                {/*<View style={localStyle.homeButtonPanel}>
                    <HomeButton
                        onPress={onHomePress}
                        style={localStyle.buttonStyle}
                        loadingColor='#07c4f0'
                        primary
                    />
                </View>*/}

            </View>
        </KeyboardAwareScrollView>
    </View>
    );
  }

}

const mapStateToProps = (state, props) => ({
  //getSharedInstances:rhythmSelectors.getSharedInstances
});
export default connect(mapStateToProps, {
    ...connectActions
})(ConversationView);
