import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    AsyncStorage,
    ActivityIndicator

} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import _ from 'underscore';
import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';
import {NavigationActions} from 'react-navigation';
const firebase = require("firebase");
import {connect} from 'react-redux';
import {actions as connectActions, selectors as connectSelectors} from 'PeaceWithFood/src/store/pwfconnect';

let blocks;
const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dce0e2'
    },
    section: {
      //  flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? 70 : 50),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,

        alignItems: 'center',
        justifyContent:'center',
        flexDirection:'column',

      },
      borderPanel:{
        height:height/1.2,
        marginTop:(Platform.OS === 'ios' ? height/10 : height/8),
        marginBottom:(Platform.OS === 'ios' ? 10 : 10),
        marginLeft:15,
        marginRight:15,
        // borderBottomWidth:2,
        // borderBottomColor:'#2799bb',
        borderTopWidth:2,
        borderTopColor:'#2799bb'
      },
      answer:{
        color:'#627078',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#a2a4a5',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: 'white',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
      checkPanel:{
        // alignItems: 'center',
        // justifyContent:'center',
        paddingTop:20,
        paddingLeft:20,
        // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
        flexDirection:'row'
      },
      sharedBlock:{
        //padding:40,
        alignItems: 'center',
        justifyContent:'space-between',
        marginTop:5,
        height:35,
        width:width/1.2,
        flexDirection:'row',
        flexWrap: "wrap",
        backgroundColor:'#eaeced',

      },
      timeText:{
        color:'#2799bb',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
        marginRight:5,
      },
      nameText:{
        color:'#447990',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
      }

});


let messageItems =[];
class ConversationView extends Component {

  constructor(props){
    super(props);
    this.state = {
      chatPanelOpen:false,
      messageItems:[],
      dataFetch:false,
      coaches:[],
      userId:null,
    }
    this.chatRef = firebase.database().ref().child('messages/');
    this.chatRefData = this.chatRef.orderByChild('child_added')
  }
  componentWillMount() {
    AsyncStorage.getItem('@pwf/user').then((value) =>{
      if(value){
        user =JSON.parse(value);
        let firebasePassword ='firebase';
        let email=user.email
          this.setState({userEmail:user.email,userId:user.id});
          const promise = this.props.getMyCoaches( user.id );
          const self    = this;
          promise.then(function(coaches){
            self.setState({'coaches':coaches,});
          });
        }
      });

  }

  componentDidMount(){
    var query = this.chatRef;
     let self=this;
     let items=[];

    query.on("child_added", function(snapshot) {

      var messageId = snapshot.key;
      var snapshotval = snapshot.val();
      //console.log('coaches',self.state.coaches)
      items.push({message_id:snapshot.key,message_details:[snapshot.val()]})
      self.setState({messageItems:items,dataFetch:true})
      // console.log('messageId',messageId,);
      // console.log('snapshotval',snapshotval,);
       //console.log('items',self.state.messageItems);

    })
  }


  onChatPress = (chatPanelOpen) =>{
    this.setState({chatPanelOpen : chatPanelOpen ? true : false});
  }
  onMessagePress = (messageId)=>{
    //console.log('this.messageId',messageId)
    //this.props.navigation.navigate('MessageHistory');
    this.props.navigation.navigate('MessageHistory',{messageDetailsId:messageId});
  }
  render (){
    let {

        onHomePress,
        onAddCoachesPress,
        onShareWithCoachPress,
        visibleModal,
        onModalClose,

    } = this.props;
      let self=this;
    {this.state.messageItems && (
     blocks = _.map( this.state.messageItems, function( item, i ){
       let obj_val= Object.values(item.message_details),
           values=Object.values(obj_val[0]),
           last_val=values[values.length-1];
       let user_email= last_val.email ? last_val.email : last_val.user.email;
      //  var evens = _.filter(self.state.coaches, function(e){ console.log("e",e,user_email);return e.email || self.state.userEmail  == user_email; });
      //console.log('obj_val', values[0]);
      let flag = false;
      if(_.has(values[0], 'sharedCoaches')){

        values[0].sharedCoaches.forEach(function(k, i){
          //console.log(k.userId);
          if(k.userId == self.state.userId){
            flag = true;
          }
        });
      }

      if(values[0].user.email == self.state.userEmail){
          flag = true;
      }
      if(self.state.userEmail == user_email){
        flag = true;
      }

      // self.state.coaches.forEach(function(k, i){
      //   if(k.email == user_email){
      //     flag = true;
      //   }
      // });
      //


      // let emailList=_.pluck(self.state.coaches,'email');
      // let fina=_.find(values, function(e){ let val= _.findIndex(emailList,e.email) || _.findIndex(emailList,e.user.email);console.log(val); return (val != -1)})
      // console.log(fina);
      let templates1 = null;
      if(flag){
        templates1 = (

            <TouchableOpacity onPress={self.onMessagePress.bind(self,item.message_id)} key={i} style={localStyle.sharedBlock}>
                     <View style={{backgroundColor:'#9fc1ce',height: 25, width: 25, borderRadius: 25/2, marginLeft:(Platform.OS === 'ios' ? -10 : 0), justifyContent:'center', alignItems:'center'}}>
                        <Text style={[localStyle.timeText,{marginLeft:0,marginRight:0}]}></Text>
                     </View>
                     <Text style={[localStyle.nameText,{marginRight:5}]}>{user_email}</Text>

            </TouchableOpacity>

        );
      }

          return templates1;
        })
    )}

    return (<View style={styles.container}>
        <StatusBar/>
        <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>

            <View style={[localStyle.section, localStyle.bottomSection]}>
             <View style={localStyle.borderPanel}>
              <ScrollView contentContainerStyle={localStyle.sharePanel}>
              {!this.state.dataFetch && (
                 <View style={{alignItems:'center',justifyContent:'center',marginTop:height/2}}>
                   <ActivityIndicator size="large" color="#00bcec" />
                 </View>

              )}
              {this.state.messageItems && this.state.dataFetch && (
               blocks
              )}

              </ScrollView>
              </View>
                {/*<View style={localStyle.homeButtonPanel}>
                    <HomeButton
                        onPress={onHomePress}
                        style={localStyle.buttonStyle}
                        loadingColor='#07c4f0'
                        primary
                    />
                </View>*/}

            </View>
        </KeyboardAwareScrollView>
    </View>
    );
  }

}

const mapStateToProps = (state, props) => ({
  //getSharedInstances:rhythmSelectors.getSharedInstances
});
export default connect(mapStateToProps, {
    ...connectActions
})(ConversationView);
