import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    ImageBackground


} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';
import Dash from 'react-native-dash';
import { AreaChart,XAxis,YAxis,LineChart } from 'react-native-svg-charts';
import { Circle,Line,G } from 'react-native-svg';
import {actions as rhythmActions,selectors as rhythmSelectors} from 'PeaceWithFood/src/store/rhythm';
import {connect} from 'react-redux';
import * as scale from 'd3-scale';
import * as shape from 'd3-shape';
import dateFns, { differenceInDays } from 'date-fns';
import moment from 'moment';
import _ from 'underscore';
const aspectRatio = height/width;
const firebase = require("firebase");
import { Permissions, Notifications } from 'expo';
var fetch = require('node-fetch');
let diffBetweenDates = -1;
let dayByDayRythmEntry = [];
let newData=[];
let count=0;
let haveData = false;
let testCount = 1;

const data = [ 50, 10, 40, 95, -4, -120, 85, 91, 35, -53, -20, -120, 10, 40, 95, -4, -24, 85, 91, 35, -53, 24, 50, -20, -80, ];
let getFullMonthName;
let fullMonth=[];
const data1 = [
            {
                value: 2,
                date: dateFns.setHours(new Date(2018, 0, 0), 6),
            },
            {
                value: 6,
                date: dateFns.setHours(new Date(2018, 0, 0), 9),
            },
            {
                value: 3,
                date: dateFns.setHours(new Date(2018, 0, 0), 15),
            },
            {
                value: 9,
                date: dateFns.setHours(new Date(2018, 0, 0), 18),
            },
            {
                value: 5,
                date: dateFns.setHours(new Date(2018, 0, 0), 21),
            },
            {
                value: 7,
                date: dateFns.setHours(new Date(2018, 0, 0), 24),
            },

        ]
        const localStyle = StyleSheet.create({
            container: {
              marginTop:(Platform.OS === 'ios' ? height/8.5 : height/7)
                //flex: 1,
                //backgroundColor: 'yellow',
                //height:height
            },
            section: {
              //  flex: 1
            },
            bottomSection: {
                backgroundColor: '#dce0e2',
            },
            topSection: {
                justifyContent: 'center'
            },
            logoPanel:{
              marginTop:height/5,
              alignItems:'center'
            },
            form: {
                justifyContent: 'center',
              //  flex: 1
                marginTop:(Platform.OS === 'ios' ? 80 : 30)
            },
            row: {
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                paddingLeft: 32,
                paddingRight: 32
            },
            inputText: {
                backgroundColor: '#e8e9ea',
                borderBottomColor:'#2799bb',
                borderLeftColor:'#d4d5d6',
                borderRightColor:'#d4d5d6',
                borderTopColor:'#d4d5d6',
                borderWidth:2,
                color:'#2799bb'
            },
            logoText: {
                textAlign: 'center',
                fontSize: 30,
                marginTop:height/5,
                color:'#2799bb'
            },
            buttonText: {
                color: '#7ec2d6',
                fontSize: 18
            },
            tipsPanel:{
              alignItems: 'center',
              justifyContent:'center',
              flexDirection:'row',
              paddingTop:(Platform.OS === 'ios' ? 40 : 20),
              paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
            },
            homeButtonPanel:{
                marginTop:(Platform.OS === 'ios' ? 70 : 50),
                height:45,
                width:width/1.3,
                alignItems: 'center',
                justifyContent:'center',
                paddingLeft:width/5
            },
            buttonStyle:{
                backgroundColor:'#4cc5e7',
                borderRadius:50,
                shadowColor: '#a3a7a8',
                borderTopWidth:0,
                shadowOffset: { width: 1, height: 3 },
                shadowOpacity: 0.8,
                shadowRadius: 2,
                elevation: 1,
                width:width/1.3,
              },
              sharePanel:{
                //height: 300,
                //paddingLeft:30,
                //paddingRight:30,

                //alignItems: 'center',
              //  justifyContent:'center',
                marginTop:50,
                flexDirection:'column',

              },
              borderPanel:{
                height:height/1.2,
                marginTop:height/10,
                marginBottom:(Platform.OS === 'ios' ? 10 : 10),
                marginLeft:15,
                marginRight:15,
                // borderBottomWidth:2,
                // borderBottomColor:'#2799bb',
                borderTopWidth:2,
                borderTopColor:'#2799bb'
              },
              answer:{
                color:'#627078',
                fontSize:16,
                //marginTop:10
              },
              socialIcon:{
                marginLeft:20
              },
              socialPanel:{
                flexDirection:'row',
                marginTop:20,
                marginBottom:20,
              },
              shareBox:{
                flexDirection:'column',
                alignItems:'center'
              },
              iconText:{
                color:'#a2a4a5',
                //marginTop:15,
                fontSize:16,
                fontWeight:'400'
              },
              modalContent: {
                backgroundColor: 'white',
                padding: 22,
                // justifyContent: 'center',
                // alignItems: 'center',
                borderRadius: 4,
                borderColor: 'rgba(0, 0, 0, 0.1)',

          },
              checkPanel:{
                // alignItems: 'center',
                // justifyContent:'center',
                paddingTop:20,
                paddingLeft:20,
                // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
                flexDirection:'row'
              },
              sharedBlock:{
                //padding:40,
                alignItems: 'center',
                justifyContent:'space-between',
                marginTop:5,
                height:35,
                width:width/1.2,
                flexDirection:'row',
                flexWrap: "wrap",
                backgroundColor:'#eaeced',

              },
              timeText:{
                color:'#2799bb',
                fontSize:18,
                fontWeight:'400',
                marginLeft:5,
                marginRight:5,
              },
              nameText:{
                color:'#447990',
                fontSize:18,
                fontWeight:'400',
                marginLeft:5,
              },
              areaStyleIos:{
                height: height/2.2,
                width:height/1.2,

              },
              areaStyleAndroid:{
                flex:1,
              },
              axisStyleIos:{
               height: height/2.1,
               width:height/1.2,
               backgroundColor:'transparent',


             },
             axisStyleAndroid:{
               height:48,
               backgroundColor:'transparent'
             }

        });



class OthersSharedGraphChartView extends Component {

  constructor(props){
    super(props);
    this.state = {
      chatPanelOpen:false,
      visibleModal:false,
      page: true,
      selectedDate:{},
      user_id:null,
      rhythmEntries:[],
      dataFetch:false,
      dayEnd:false,
      healthStatusId:null,
      thoughtValue:{},
      message:'',
      userEmail:'',
      userName:'',
      expoToken:'',
      lastResponse:{},
      messageDataFetch:false,
      sharedUserEmail:'',
      flag:false,

    }

  }

  componentWillMount() {
      //console.log("sharedUserEmail",this.props.navigation.state.params.sharedUserEmail)
      let newMessageEntryID=[];
      AsyncStorage.setItem('newMessageEntryID',JSON.stringify(newMessageEntryID));

       this.setState({ title: this.props.navigation.state.params.labelString, sharedUserEmail:this.props.navigation.state.params.sharedUserEmail });
       //this.setState({selectedDate:this.props.navigation.state.params.dateObj,dayEnd:this.props.navigation.state.params.dayEnd});
       //console.log('selectedDate',this.props.navigation.state.params.dateObj);
       let shortMonth=['00','01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
           fullMonth=["",'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
           getFullMonthName = shortMonth.map((e)=>{
                      // TODO return e.indexOf(this.props.navigation.state.params.dateObj.month) > -1;
                   }).indexOf(true);
                  //console.log( fullMonth[getFullMonthName]);

       AsyncStorage.getItem('@pwf/user').then((value) =>{

         if(value){
           user =JSON.parse(value)
           this.setState({user_id:user.id,userEmail:user.email});
           this.setState({user_id:user.id, userName:user.first_name});
            //console.log('user',this.state.user_id);
            const params = this.props.navigation.state.params;
            // this.props.getClientRhythm(  params.userId,
            //                              params.selectedStartDate,
            //                              params.selectedEndDate );
                                         // SC 21/05/2018 //
                            //console.log(params.selectedStartDate,"............", params.selectedEndDate);
                            if(params.selectedStartDate == params.selectedEndDate){
                            this.props.getClientRhythm( params.userId,
                            params.selectedStartDate,
                            params.selectedEndDate );
                            diffBetweenDates = (moment(params.selectedStartDate)).diff(moment(params.selectedEndDate), 'days');

                            }
                            else{

                            let startDate = params.selectedStartDate;
                            let endDate = params.selectedEndDate;
                            diffBetweenDates = (moment(endDate)).diff(moment(startDate), 'days');


                            for(let i=0; i<diffBetweenDates; i++){

                            this.props.getClientRhythm( params.userId,
                            startDate,
                            startDate );
                            startDate = moment(startDate).add(1,'days').format('YYYY-MM-DD');

                            }
                            //diffBetweenDates = -1;
                            }
                            // SC END //
         }
       });


  }
  componentDidMount() {
    this.registerForPushNotificationsAsync();
  }
  componentWillUnmount(){
    newData=[];
    dayByDayRythmEntry=[];
    count=0;
    haveData = false;
    testCount = 1;
  }

  registerForPushNotificationsAsync = async () => {
    // console.log('noti', await Notifications.getExpoPushTokenAsync())
        const { existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
        let finalStatus = existingStatus;

        // only ask if permissions have not already been determined, because
        // iOS won't necessarily prompt the user a second time.
        if (existingStatus !== 'granted') {
            // Android remote notification permissions are granted during the app
            // install, so this will only ask on iOS
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }

        // Stop here if the user did not grant permissions
        if (finalStatus !== 'granted') {
            return;
        }

        // Get the token that uniquely identifies this device
        let token = await Notifications.getExpoPushTokenAsync();
        //console.log('token',token);
        this.setState({expoToken:token});

        // // POST the token to our backend so we can use it to send pushes from there
        // var updates = {}
        // updates['/expoToken'] = token
        // await firebase.database().ref('/users/' + currentUser.uid).update(updates)
        //call the push notification
    }
  componentWillReceiveProps(nextProps) {
    //console.log('nextProps',nextProps.getRhythmByClient);
    if(nextProps.getRhythmByClient){
        this.setState({
          rhythmEntries:nextProps.getRhythmByClient,
          //dataFetch:true


        });
    }
    testCount++;
    if(nextProps.getRhythmByClient.length >0){
      //console.log('hello push',count);
      dayByDayRythmEntry.push(nextProps.getRhythmByClient);
      count++;
    }
    if((diffBetweenDates==0) || count <= diffBetweenDates){
      //console.log('dataFetch--true');
      this.setState({dataFetch:true})
    }
  }

  onChatPress = (chatPanelOpen) =>{
    this.setState({chatPanelOpen : chatPanelOpen ? true : false});
  }

  changePage(p,message_id){
    let self=this;
    this.setState({page:p,flag:false});
     var lastReply = firebase.database().ref().child("messages/"+message_id);
     var messagePresent =firebase.database().ref().child("messages")
     messagePresent.child(message_id).once('value', function(snapshot) {
        if(snapshot.val() == null)
        {
          self.setState({lastResponse:{text:"No reply found!"},messageDataFetch:true,flag:true});
        }
      });
     lastReply.on("child_added", function(snapshot) {
       var messageId = snapshot.key;
       var messageDetails = snapshot.val();

       if(messageId !=0 && (messageDetails.user.email == self.state.sharedUserEmail)){
       self.setState({lastResponse:messageDetails,messageDataFetch:true,flag:true});
       }
       else{
         if(!self.state.flag){
         self.setState({lastResponse:{text:"No reply found!"},messageDataFetch:true});
         }
       }

     });
  }

  _renderModalContent = (onModalClose,healthState,onCheckMarkPress,onMessagePress,healthStatusId,thoughtValue) => {
    let Reason1,Reason2,Reason3,Reason4;
    let healthStateArray=[['Famished', 'Starving', 'Very Hungry'], ['Ready', 'Neutral', 'Full & Satisfied'], ['Overly Full', 'Stuffed', 'Sick']];
    let getChangeVal = healthStateArray.map(function(e){
                   return e.indexOf(healthState) > -1;
                }).indexOf(true);
    let healthStateArrayForModal=['','Famished', 'Starving', 'Very Hungry','Ready', 'Neutral', 'Full & Satisfied','Overly Full', 'Stuffed', 'Sick'];
    let getChangeValForModal = healthStateArray.map(function(e){
                   return e.indexOf(healthStatusId) > -1;
                }).indexOf(true);
                //console.log('getChangeValForModal',healthStateArrayForModal[healthStatusId],getChangeValForModal,healthStatusId);
       //console.log('thoughtvalue',thoughtValue);
    //if(getChangeVal ==0){
      let reasonArray=[[
                        {text:'Busy/ Forgot',type:'checkbox'},
                        {text:'Food Unavailable',type:'checkbox'},
                        {text:'Consciously Restricting',type:'label'},
                        {text:'Weight worries',type:'radio',name:'consciously_restricting'},
                        {text:'I ate too much last night',type:'radio',name:'consciously_restricting'},
                        {text:'Other',type:'input',name:'consciously_restricting'},
                        {text:'Mood',type:'label'},
                        {text:'Stressed',type:'radio',name:'moodFamished'},
                        {text:'Lonely',type:'radio',name:'moodFamished'},
                        {text:'Angry',type:'radio',name:'moodFamished'},
                        {text:'Anxious',type:'radio',name:'moodFamished'},
                        {text:'Sad',type:'radio',name:'moodFamished'},
                        {text:'Tired',type:'radio',name:'moodFamished'}],
                        [],
                        [{text:'Hunger',type:'checkbox'},
                        {text:'Tempting Food',type:'checkbox'},
                        {text:'Mindless Eating',type:'checkbox'},
                        {text:'Mood',type:'label'},
                        {text:'Stressed',type:'radio',name:'moodSick'},
                        {text:'Lonely',type:'radio',name:'moodSick'},
                        {text:'Angry',type:'radio',name:'moodSick'},
                        {text:'Anxious',type:'radio',name:'moodSick'},
                        {text:'Sad',type:'radio',name:'moodSick'},
                        {text:'Tired',type:'radio',name:'moodSick'},
                        {text:'Other',type:'input',name:'moodSick'},
                        {text:'Social Pressure to Eat',type:'label'},
                        {text:'Friends',type:'radio',name:'social_pressure_to_eat'},
                        {text:'Family',type:'radio',name:'social_pressure_to_eat'}]]
      let userSelection = reasonArray[getChangeVal];
       // console.log('=======================================');
       // console.log(thoughtValue);
      // console.log('=======================================');
    //}

  return(
    <View style={localStyle.modalContent}>
    { this.state.page ? (
      <View>
             <View style={{flexDirection:'row',justifyContent:'flex-end'}}>

              <TouchableOpacity style={{width:50,height:50,justifyContent:'center',alignItems:'center'}} onPress={this.onModalClose.bind(this)}>
                <Image source={Images.orangeCloseIcon} />
              </TouchableOpacity>
            </View>
            <View style={{height:height/20}}>
            <Text style={{color:'#e87f47',fontSize:20}}>{healthStateArrayForModal[healthStatusId]}</Text>
              <ScrollView>
              {/*<View  style={localStyle.checkPanel}>
              <CheckBox
                      style={{alignItems:'center',marginRight:10}}
                      onClick={()=>console.log('checked')}
                      isChecked={true}
                      checkBoxColor='#e0e4e7'

                  />
                  <Text style={localStyle.buttonText}>
                      Reason1
                  </Text>
                </View>
                <View  style={localStyle.checkPanel}>
                  <CheckBox
                        style={{alignItems:'center',marginRight:10}}
                        onClick={()=>console.log('checked')}
                        isChecked={true}
                        checkBoxColor='#e0e4e7'

                    />
                    <Text style={localStyle.buttonText}>
                        Reason2
                    </Text>
                  </View> */}
                        {  _.has(thoughtValue.options, 'checks') ? (
                          <View>
                              {(_.has(thoughtValue.options.checks, 'Busy_Forgot') && thoughtValue.options.checks.Busy_Forgot )? (
                                    <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>

                                      <Text style={[localStyle.buttonText]}>
                                          Busy/ Forgot
                                      </Text>

                                    </View>
                                    )
                                    :null
                                  }

                                {(_.has(thoughtValue.options.checks, 'Food_Unavailable') && thoughtValue.options.checks.Food_Unavailable )? (
                                    <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>

                                      <Text style={[localStyle.buttonText]}>
                                          Food Unavailable
                                      </Text>

                                    </View>
                                    )
                              :null
                            }

                                  {(_.has(thoughtValue.options.checks, 'Hunger') && thoughtValue.options.checks.Hunger )? (
                                        <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>

                                          <Text style={[localStyle.buttonText]}>
                                              Hunger
                                          </Text>

                                        </View>
                                        )
                                        :null
                                      }

                                    {(_.has(thoughtValue.options.checks, 'Tempting_Food') && thoughtValue.options.checks.Tempting_Food )? (
                                        <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>

                                          <Text style={[localStyle.buttonText]}>
                                              Tempting_Food
                                          </Text>

                                        </View>
                                        )
                                  :null
                                }
                                {(_.has(thoughtValue.options.checks, 'Mindless_Eating') && thoughtValue.options.checks.Mindless_Eating )? (
                                    <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>

                                      <Text style={[localStyle.buttonText]}>
                                          Mindless Eating
                                      </Text>

                                    </View>
                                    )
                              :null
                            }
                          </View>

                        )
                        :null
                      }
                      {  (_.has(thoughtValue.options, 'Mood') && thoughtValue.options.Mood !='')  ? (
                        <View>
                        <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>
                          <Text style={[localStyle.buttonText,{color:'#2799bb'}]}>
                              Mood
                          </Text>
                        </View>
                          <View  style={localStyle.checkPanel}>
                            <Text style={localStyle.buttonText}>
                                {thoughtValue.options.Mood}
                            </Text>
                          </View>
                        </View>

                      )
                      :null
                    }
                    { ( _.has(thoughtValue.options, 'consciously_restricting') && thoughtValue.options.consciously_restricting !='')  ? (
                      <View>
                      <View  style={localStyle.checkPanel}>
                        <Text style={[localStyle.buttonText,{color:'#2799bb'}]}>
                            Consciously Restricting
                        </Text>
                      </View>
                        <View  style={localStyle.checkPanel}>
                          <Text style={localStyle.buttonText}>
                              {thoughtValue.options.consciously_restricting}
                          </Text>
                        </View>
                      </View>
                    )
                    :null
                    }
                    {  (_.has(thoughtValue.options, 'social_pressure_to_eat') && thoughtValue.options.social_pressure_to_eat !='') ? (
                      <View>
                      <View  style={[localStyle.checkPanel,{flexDirection:'column'}]}>
                        <Text style={[localStyle.buttonText,{color:'#2799bb'}]}>
                            Social Pressure To Eat
                        </Text>
                      </View>
                        <View  style={localStyle.checkPanel}>
                          <Text style={localStyle.buttonText}>
                              {thoughtValue.options.social_pressure_to_eat}
                          </Text>
                        </View>
                      </View>

                  )
                  : null
                }
              </ScrollView>
            </View>
            <View style={{marginTop:15}}>
              { _.has(thoughtValue, 'message_id') && (
                  <Text style={[localStyle.buttonText,{fontWeight:'500',marginBottom:5,marginLeft:10}]}>
                      Thoughts for PwF Connect:
                  </Text>
                )}
              { !_.has(thoughtValue, 'message_id') && (
                  <Text style={[localStyle.buttonText,{fontWeight:'500',marginBottom:5,marginLeft:10}]}>
                    Thoughts:
                  </Text>
                )}
                <View style={{borderWidth:2,borderColor:'#2799bb',width:width/1.3,height:height/5,backgroundColor:'#e5e9ea'}}>
                    <Text style={{color:'#4d6e7a',fontSize:18,backgroundColor:'#abe6f4',padding:10}}>
                      {  _.has(thoughtValue, 'thoughts') ? thoughtValue.thoughts :'' }
                    </Text>
                    { _.has(thoughtValue, 'message_id') && (
                    <TouchableOpacity onPress={this.changePage.bind(this,false,thoughtValue.message_id)} style={{ flex: 1,justifyContent:'flex-end',alignItems:'flex-end',padding:5}}>
                      <Text style={{fontSize:18,color:'#6a7980'}}>Check reply</Text>
                    </TouchableOpacity>
                    )}
                </View>
            </View>

            </View>
          )  : (
            <View>
                   <View style={{flexDirection:'row',}}>
                     <TouchableOpacity style={{marginTop:height/50}} onPress={this.changePage.bind(this,true,thoughtValue.message_id)}>
                        <Image source={Images.smallBackIcon} />
                      </TouchableOpacity>
                    <Text style={{color:'#5fb0c6',fontSize:20,marginLeft:10,flex:3,marginTop:height/50}}>thoughts for PwF</Text>
                    {/*<TouchableOpacity style={{width:50,height:50,justifyContent:'space-between',marginLeft:(Platform.OS === 'ios' ? 120 : 70)}} onPress={this.onModalClose.bind(this)}>
                      <Image  source={Images.orangeCloseIcon} />
                    </TouchableOpacity>*/}
                  {/*  <TextButton
                        label={"Send"}
                        onPress={this.onCheck.bind(this,onCheckMarkPress,thoughtValue.message_id)}
                        style={[localStyle.buttonStyle,{borderRadius:5,justifyContent:'center',alignItems:'center'}]}
                        loadingColor='transparent'
                        primary
                    />*/}
                  </View>
                  {/*<View style={{height:height/5,}}>*/}
                  <View>
                      <Text style={{color:'#e87f47',fontSize:20,marginTop:height/32,marginBottom:5}}>{healthStateArrayForModal[healthStatusId]}</Text>
                      <Text style={{color:'#e87f47',fontSize:16}}>
                         {  (_.has(thoughtValue, 'message_id') && !_.isEmpty(this.state.lastResponse) && this.state.lastResponse.user ) ? this.state.lastResponse.user.name :'' }
                      </Text>
                      <View style={{width:width/1.32,backgroundColor:'#abe6f4',flexDirection:'row',padding:5}}>
                        {!this.state.messageDataFetch && !this.state.flag && (

                             <ActivityIndicator size="large" color="#00bcec" style={{alignItems:'center',justifyContent:'center',}} />

                        )}
                        <Text style={{color:'#4d6e7a',fontSize:18}}>
                          {  (_.has(thoughtValue, 'message_id') ) ? this.state.lastResponse.text :'' }
                        </Text>
                        {/*<View style={{backgroundColor:'#abe6f4',width:10,height:10,marginTop:85,borderTopRightRadius:30,marginLeft:13}}></View>*/}
                      </View>
                  </View>
                  {/*<View style={{marginTop:35}}>
                      <TextInput
                          multiline={true}
                          numberOfLines={4}
                          style={{padding:5,backgroundColor:'#e8eced',borderWidth:2,borderColor:'#add1e0',width:width/1.3,height:height/5}}
                          placeholder="Enter Message"
                          placeholderTextColor="#446a7a"
                          underlineColorAndroid='transparent'
                          onChangeText={ (text) => this.setState({ message: text }) }

                      />
                  </View>
                  <View style={{flexDirection:'row',justifyContent:'flex-end',marginTop:10}}>
                     <Text style={{color:'#5fb0c6',fontSize:20,marginLeft:10,flex:3,marginTop:height/50}}></Text>
                      <TextButton
                          label={"Send"}
                          onPress={this.onCheck.bind(this,onCheckMarkPress,thoughtValue.message_id)}
                          style={[localStyle.buttonStyle,{borderRadius:5,justifyContent:'center',alignItems:'center'}]}
                          loadingColor='transparent'
                          primary
                      />
                    {/*  <Image style={{marginTop:2,marginRight:10}}source={Images.rightArrowIcon} />
                      <TouchableOpacity onPress={this.onMessage.bind(this,onMessagePress)}>
                          <Image style={{marginTop:2}} source={Images.chatIcon} />
                      </TouchableOpacity>
                      <TouchableOpacity style={{width:50,height:50}} onPress={this.onCheck.bind(this,onCheckMarkPress)}>
                        <Image style={{marginRight:5,marginTop:2,marginLeft:20}} source={Images.checkMarkIcon} />
                      </TouchableOpacity>
                  </View> */}

                  </View>
                  )}
      </View>
    )};

    onCheck = (onCheckMarkPress,message_id) =>{
      let currentUser = firebase.auth().currentUser
      let createdAt = new Date().getTime()
      let chatMessage = {
        _id:createdAt,
        text: this.state.message,
        createdAt: createdAt,
        expoToken:this.state.expoToken,
        user: {
          name: this.state.userName,
          email: this.state.userEmail
        }
      };
      var query = firebase.database().ref().child("messages").limitToLast(1);
      let userTokens=[];
      query.on("child_added", function(snapshot) {
        var messageId = snapshot.key;
        var timestamp = snapshot.val();
        //console.log('messageId,timestamp',messageId,timestamp)

        if(message_id){
            firebase.database().ref().child("messages/" + message_id).push(chatMessage, (error) => {
              if(error){
                console.log('error')
              }else{
                firebase.database().ref().child("messages/" + message_id).on("child_added", function(resp) {
                  //console.log('snapshot', resp.val());
                  userTokens.push(resp.val().expoToken);

                  var messages = [];
                  messages.push({
                      "to": resp.val().expoToken,
                      "sound": "default",
                      "body": chatMessage.text
                  });
                  //console.log('messages',messages);
                  fetch('https://exp.host/--/api/v2/push/send', {
                      method: 'POST',
                      headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'application/json',
                      },
                      body: JSON.stringify(messages)

                  });
                });
              }
            });
           }
          });



      this.setState({visibleModal:false});
      onCheckMarkPress()
    }

    onMessage = (onMessagePress) =>{
      this.setState({visibleModal:false});
      onMessagePress()
    }

    onModalPress = (visibleModal,healthState,value) =>{
       this.setState({visibleModal:visibleModal, healthStatusId:healthState,thoughtValue:value})
     }

     onModalClose = () =>{
       this.setState({visibleModal:false})
     }


  render (){
    let {

        onHomePress,
        onAddCoachesPress,
        onShareWithCoachPress,
        onModalClose,
        onCalendarPress,
        onCheckMarkPress,
        onMessagePress,
        title

    } = this.props;
    // let customDataY=[];
    // let customDataX=[];
    // let date;
    // let customMin=[];
    if(this.state.dataFetch){
    let time = moment.duration("00:02:00"),arr=dayByDayRythmEntry,len=(arr).length;
    let newData1=[];
    //console.log('dayByDayRythmEntry...',dayByDayRythmEntry.length,"DayDiff.......", diffBetweenDates);
    if(testCount > diffBetweenDates){
      haveData = true;
      dayByDayRythmEntry.map((e,i)=>{
        //console.log('dayByDayRythmEntry...elementLength......',e.length);
        newData1=e.map((j,k) =>{
          //console.log("k..",k);
        switch (true) {
            case (j.value == 2):
                return {
                  value: 1.5,
                  date: moment(j.created_date),
                  all_data: j
                };
                break;
            case (j.value == 3):
                return {
                  value: 2.25,
                  date: moment(j.created_date),
                  all_data: j
                };
                break;
            case (j.value == 4):
                return {
                  value: 3.75,
                  date: moment(j.created_date),
                  all_data: j
                };
                break;
            case (j.value == 6):
                return {
                  value: 6.25,
                  date: moment(j.created_date),
                  all_data: j
                };
                break;
            case (j.value == 7):
                return {
                  value: 7.75,
                  date: moment(j.created_date),
                  all_data: j
                };
                break;
            case (j.value == 8):
                return {
                  value: 8.5,
                  date: moment(j.created_date),
                  all_data: j
                };
                break;
            default:
                return {
                  value: j.value,
                  date: moment(j.created_date),
                  all_data: j
                };
              break;
            }

      })
      newData.push(newData1);
        });
    }
      for(i=0;i<newData.length;i++){
      if(newData[i][0] != undefined){
          //console.log("=============", newData[0].date);
          newData[i].unshift({
            value: 0,
            date: moment(newData[i][0].date).subtract(time),
            all_data: {}
          });

          newData[i].push({
            value: 10,
            date: moment(newData[i][newData[i].length - 1].date).add(time),
            all_data: {}
          });
        }
      }
    }


      //console.log("qwerty....",newData.length);
    //console.log('data1',width/414,width);
    return (<View style={styles.container}>
        <StatusBar/>
        <ImageBackground  source={Images.chartBackground} resizeMode={Image.resizeMode.streach} style={{width:width, height:(Platform.OS === 'ios' ? (height-60) : (height-80))}}>
        <ScrollView  contentContainerStyle={localStyle.container}>
            <Modal isVisible={this.state.visibleModal}>
             {this._renderModalContent(onModalClose,'sick',onCheckMarkPress,onMessagePress,this.state.healthStatusId,this.state.thoughtValue)}
           </Modal>
           {!haveData && (
              <View style={{alignItems:'center',justifyContent:'center',marginTop:height/2}}>
                <ActivityIndicator size="large" color="#00bcec" />
              </View>

           )}

           {dayByDayRythmEntry.length == 0 && haveData && (
              <View style={{alignItems:'center',justifyContent:'center',marginTop:height/2,backgroundColor:'transparent'}}>
                <Text style={{color:'#9ea0a3',fontSize:22}}>No rhythms found!</Text>
              </View>

           )}

            {haveData && dayByDayRythmEntry.map((e,i)=>{


              let ids=_.pluck(e,'created_date');

              return (
            <ScrollView contentContainerStyle={{height:(height+50)}} key={i}>
              <View style={{width:width,height:30,backgroundColor:'#4cc5e7'}}>
              <Text style={{color:'white',fontSize:22,fontWeight:'500',textAlign:'center'}}>
              {moment(ids[0]).format('ll').split(",")[0]}</Text>
              </View>
                <View  style={{height: (Platform.OS === 'ios' ? (aspectRatio >2.1 ? height/1.7 : height/1.90)  : height/1.85),width:(Platform.OS === 'ios' ? width : width),top:(Platform.OS === 'ios' ? 5 : 120),right:(Platform.OS === 'ios' ? 5 : 0),bottom:20,transform: [{ rotate: '90deg'}] }}>
                    <LineChart
                      style={(Platform.OS === 'ios' ? localStyle.areaStyleIos : localStyle.areaStyleAndroid)}
                      data={newData[i]}
                      showGrid={false}
                      yAccessor={({ item }) => item.value}
                      xAccessor={({ item }) => item.date}
                      xScale={scale.scaleTime}
                      contentInset={{ top: 10, bottom: -25, left: (Platform.OS === 'ios' ? 18 : 15), right: 15}}
                      svg={ {
                          fill: 'transparent',
                          stroke: '#616162',
                          strokeWidth:3

                      } }
                      curve={shape.curveCardinalOpen.tension(0.5)}
                      extras={ [(({ y }) => (
                                 <G key={ 'dash' }>
                                  <Line
                                      key={ 'zero-axis' }
                                      x1={ '0%' }
                                      x2={ '100%' }
                                      y1={ y(7) }
                                      y2={ y(7) }
                                      stroke={ '#00bded' }
                                      strokeDasharray={ [ 10, 10 ] }
                                      strokeWidth={ 2 }
                                  />
                                  <Line
                                      key={ 'zero-axis-line' }
                                      x1={ '0%' }
                                      x2={ '100%' }
                                      y1={ y(3) }
                                      y2={ y(3) }
                                      stroke={ '#00bded' }
                                      strokeDasharray={ [ 10, 10 ] }
                                      strokeWidth={ 2 }
                                  />
                                </G>
                              ))] }
                              renderDecorator={ ({ x, y, index, value }) => (value.value == 0 || value.value ==10) ?  null :
                                (
                                ((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) ) ? (
                                      [(
                                        // this is for hitSlop
                                        <Circle
                                            onPressIn={((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) )
                                              ? this.onModalPress.bind(this,true,value.all_data.value,value.all_data)
                                              : this.onModalPress.bind(this,false,'','') }
                                            key={ index + '.2' }
                                            cx={ x(value.date)}
                                            cy={ y(value.value) }
                                            r={ 15 }
                                            stroke="transparent"
                                            strokeWidth={2}
                                            fill="transparent"
                                            />
                                          ),(
                                            <Circle
                                                onPressIn={((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) )
                                                  ? this.onModalPress.bind(this,true,value.all_data.value,value.all_data)
                                                  : this.onModalPress.bind(this,false,'','') }
                                                key={ index + '.1' }
                                                cx={ x(value.date)}
                                                cy={ y(value.value) }
                                                r={ 6 }
                                                stroke={ ((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) )  ? '#616162' : '#61bce7' }
                                                strokeWidth={2}
                                                fill={ ((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) ) ? '#f8ec07' : 'white' }
                                            />
                                          )]) : ([(
                                                <Circle
                                                    onPress={((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) )
                                                      ? this.onModalPress.bind(this,true,value.all_data.value,value.all_data)
                                                      : this.onModalPress.bind(this,false,'','') }
                                                    key={ index + '.3' }
                                                    cx={ x(value.date)}
                                                    cy={ y(value.value) }
                                                    r={ 6 }
                                                    stroke={ ((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) )  ? '#616162' : '#61bce7' }
                                                    strokeWidth={2}
                                                    fill={ ((_.has(value.all_data, 'options') && !_.isEmpty(value.all_data.options ) || _.has(value.all_data, 'thoughts')) ) ? '#f8ec07' : 'white' }
                                                />
                                              )])
                                ) }
                    />

                    <XAxis
                      data={newData[i]}
                      svg={{
                          fill: '#7c7d7f',
                          fontSize: 10,
                          fontWeight: 'bold',
                          rotation: -90,
                          originY:(Platform.OS === 'ios' ? 24 :20),
                          y: 1,
                      }}
                      xAccessor={({ item }) => item.date}
                      scale={scale.scaleTime}
                      numberOfTicks={6}
                      style={(Platform.OS === 'ios' ? localStyle.axisStyleIos:localStyle.axisStyleAndroid)}
                      contentInset={{ left: (Platform.OS === 'ios' ? 20 :18),right:20 }}
                      formatLabel={(value) => moment(value).format("hh:mm a").replace(' ','') }
                    />
                </View>
          </ScrollView>


          )})}


                  {/*<View style={[localStyle.homeButtonPanel,{backgroundColor:'red'}]}>
                  <HomeButton
                        onPress={onHomePress}
                        style={localStyle.buttonStyle}
                        loadingColor='#07c4f0'
                        primary
                    />
                </View>*/}

        </ScrollView>
         </ImageBackground>

          <View style={{height: 60, backgroundColor: this.state.dayEnd ? '#384a54' : '#4cc5e7',alignItems:'center',justifyContent:'center',flexDirection:'row',justifyContent:'space-between'}} >

             <Text style={{color:'white',fontSize:20,marginLeft:10}}>{
               this.state.title
             }</Text>
             {/*
             <TouchableOpacity onPress={onCalendarPress}>
               <Image
                   source={Images.calenderIcon}
               />
              </TouchableOpacity>
              */}
          </View>

    </View>
    );
  }

}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
    getRhythmByClient: rhythmSelectors.getRhythmByClient(state)
});

export default connect(mapStateToProps, {
    ...rhythmActions
})(OthersSharedGraphChartView);
