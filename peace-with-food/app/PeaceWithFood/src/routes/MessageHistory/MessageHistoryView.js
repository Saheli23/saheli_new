import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    AsyncStorage

} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';
import { GiftedChat, Bubble } from 'react-native-gifted-chat';
import {NavigationActions} from 'react-navigation';
const firebase = require("firebase");
import _ from 'underscore';
import { Permissions, Notifications } from 'expo';
var fetch = require('node-fetch');


const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dce0e2'
    },
    section: {
      //  flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? 70 : 50),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,

        alignItems: 'center',
        justifyContent:'center',
        flexDirection:'column',

      },
      borderPanel:{
        height:(Platform.OS === 'ios' ? height/1.2 : height/1.3),
        marginTop:height/10,
        marginBottom:(Platform.OS === 'ios' ? 10 : 10),
        marginLeft:15,
        marginRight:15,
        // borderBottomWidth:2,
        // borderBottomColor:'#2799bb',
        borderTopWidth:(Platform.OS === 'ios' ? 2 : 0),
        borderTopColor:'#2799bb'
      },
      answer:{
        color:'#627078',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#a2a4a5',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: 'white',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
      checkPanel:{
        // alignItems: 'center',
        // justifyContent:'center',
        paddingTop:20,
        paddingLeft:20,
        // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
        flexDirection:'row'
      },
      sharedBlock:{
        //padding:40,
        alignItems: 'center',
        justifyContent:'space-between',
        marginTop:5,
        height:35,
        width:width/1.2,
        flexDirection:'row',
        flexWrap: "wrap",
        backgroundColor:'#eaeced',

      },
      timeText:{
        color:'#2799bb',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
        marginRight:5,
      },
      nameText:{
        color:'#447990',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
      },
      replyBer:{
        flexDirection:'row',
        backgroundColor:'#eef0f2',
        width:width/1.2,
        height:40,
        padding:10,
        // marginTop:50,
        justifyContent:'space-between'
      },
      calenderIcon:{
        marginLeft:width/1.4
      }

});



class MessageHistoryView extends Component {

  constructor(props){
    super(props);
    this.state = {
      chatPanelOpen:false,
      messages: [],
      userEmail:'',
      userName:'',
      expoToken:''
    }
    this.currentUser = firebase.auth().currentUser
    this.chatRef = firebase.database().ref().child('messages/'+this.props.navigation.state.params.messageDetailsId);
    this.chatRefData = this.chatRef.orderByChild('child_added')
    this.onSend = this.onSend.bind(this);
  }
  componentWillMount() {
    AsyncStorage.getItem('@pwf/user').then((value) =>{
      if(value){
        user =JSON.parse(value);
        let firebasePassword ='firebase';
        let email=user.email
        let name =  user.first_name
          this.setState({userEmail:user.email});
          this.setState({userName:user.first_name});
        }
      });

  }

  componentDidMount() {
      this.listenForItems(this.chatRefData);
      this.registerForPushNotificationsAsync();
  }

  registerForPushNotificationsAsync = async () => {
    // console.log('noti', await Notifications.getExpoPushTokenAsync())
        const { existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
        let finalStatus = existingStatus;

        // only ask if permissions have not already been determined, because
        // iOS won't necessarily prompt the user a second time.
        if (existingStatus !== 'granted') {
            // Android remote notification permissions are granted during the app
            // install, so this will only ask on iOS
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }

        // Stop here if the user did not grant permissions
        if (finalStatus !== 'granted') {
            return;
        }

        // Get the token that uniquely identifies this device
        let token = await Notifications.getExpoPushTokenAsync();
        //console.log('token',token);
        this.setState({expoToken:token});

        // // POST the token to our backend so we can use it to send pushes from there
        // var updates = {}
        // updates['/expoToken'] = token
        // await firebase.database().ref('/users/' + currentUser.uid).update(updates)
        //call the push notification
    }

  componentWillUnmount() {
      this.chatRefData.off()
  }
  getRef() {
      return firebase.database().ref('messages');
  }

  onSend(messages = []) {
    let self=this;
    // this.setState((previousState) => ({
    //   messages: GiftedChat.append(previousState.messages, messages),
    // }));
    //let currentUser = firebase.auth().currentUser
    let createdAt = new Date().getTime()
    let chatMessage = {

      text: this.state.thoughts,
      createdAt: createdAt,
      expoToken:this.state.expoToken,
      user: {
        name: self.setState.userName,
        email: self.state.userEmail
      }
    };
    messages.forEach(message => {
      var now = new Date().getTime()
      this.chatRef.push({
          _id: now,
          text: message.text,
          createdAt: now,
          expoToken:this.state.expoToken,
          user: {
            name: self.state.userName,
            email: self.state.userEmail
          }

      })
    })
    this.chatRef.on('value', (snap) => {

        snap.forEach((child) => {

         var messages = [];
         messages.push({
             "to": child.val().expoToken,
             "sound": "default",
             "body": "New message"
         });
         //console.log('messages',messages);
         fetch('https://exp.host/--/api/v2/push/send', {
             method: 'POST',
             headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'application/json',
             },
             body: JSON.stringify(messages)

         });

        });
      });

  }

  onChatPress = (chatPanelOpen) =>{
    this.setState({chatPanelOpen : chatPanelOpen ? true : false});
  }

  listenForItems(chatRef) {
    //console.log('child',chatRef);
      chatRef.on('value', (snap) => {

          // get children as an array
          var items = [];
          snap.forEach((child) => {

              // var avatar = 'https://www.gravatar.com/avatar/' + ( child.val().uid == this.user.uid? md5(this.user.email) : md5(this.friend.email))
              // var name = child.val().uid == this.user.uid? this.user.name: this.friend.name
            items.push({
               _id: child.val().createdAt,
               text: child.val().text,
               expoToken:child.val().expoToken,
               createdAt: new Date(child.val().createdAt),
               user: {
                   _id:_.has(child.val(), 'user') ? child.val().user.email :child.val().email,
                   email:_.has(child.val(), 'user') ? child.val().user.email :child.val().email,
                   avatar:_.has(child.val(), 'user') ? child.val().user.email :child.val().email,
               }
           });

          });

          this.setState({
              loading: false,
              messages: items.reverse()
          })


      });
  }

  renderBubble(props) {
    let self=this;
      if (props.isSameUser(props.currentMessage, props.previousMessage) && props.isSameDay(props.currentMessage, props.previousMessage)) {
        return (
          <Bubble
            {...props}
          />
        );
      }
      return (
        <View>
        <Text style={[styles.name,{color:'#827f7f',fontSize:16,marginBottom:5}]}>{props.currentMessage.user.email.split("@")[0].charAt(0).toUpperCase() + props.currentMessage.user.email.split("@")[0].slice(1)  }</Text>
          <Bubble
            {...props}
          />
        </View>
      );
    }

  render (){
    let {

        onHomePress,
        onAddCoachesPress,
        onShareWithCoachPress,
        visibleModal,
        onModalClose,

    } = this.props;
    return (<View style={styles.container}>
        <StatusBar/>
        <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>

            <View style={[localStyle.section, localStyle.bottomSection]}>
              <View style={[localStyle.borderPanel]}>
                  {/*<ScrollView contentContainerStyle={localStyle.sharePanel}>
                      <View style={{height:(Platform.OS === 'ios' ? 100 : 110),width:width/1.32,backgroundColor:'#becfd6',flexDirection:'row',padding:10,marginTop:10,borderTopRightRadius:15,borderTopLeftRadius:15,borderBottomRightRadius:15}}>
                          <View style={{backgroundColor:'#becfd6',width:10,height:10,marginTop:80,borderTopLeftRadius:30,marginLeft:-20}}></View>
                          <Text style={{color:'#4d6e7a',fontSize:18,marginLeft:5}}>
                              Hello, Name! I&apos;m J.Doe and I&apos;ll be helping you with finding peace with food. How was your day yesterday?
                          </Text>

                      </View>
                      <View style={{height:100,width:width/1.32,backgroundColor:'#8bd3e5',flexDirection:'row',padding:5,marginTop:10,borderTopRightRadius:15,borderTopLeftRadius:15,borderBottomLeftRadius:15}}>
                          <Text style={{color:'#4d6e7a',fontSize:18}}>
                              Hello J. Doe! I was really hungry so I overcompensated with dinner. I feel sick.
                          </Text>
                          <View style={{backgroundColor:'#8bd3e5',width:10,height:10,marginTop:85,borderTopRightRadius:30,marginLeft:13}}></View>
                      </View>
                   </ScrollView>*/}
                   <GiftedChat
                        messages={this.state.messages}
                        onSend={(messages) => this.onSend(messages)}
                        user={{
                            _id: this.state.userEmail,
                        }}
                        renderBubble={this.renderBubble.bind(this)}
                      />
                 </View>
                 {/*<View style={{marginLeft:35,flexDirection:'column'}}>
                   <Image
                       style={localStyle.calenderIcon}
                       source={Images.calenderIcon}
                   />
                   <TouchableOpacity style={localStyle.replyBer} >
                       <Text style={localStyle.answer}>reply</Text>
                       <Image
                         source={Images.frontArrowIcon}
                       />
                   </TouchableOpacity>
                 </View>*/}
              </View>
        </KeyboardAwareScrollView>
    </View>
    );
  }

}

export default MessageHistoryView;
