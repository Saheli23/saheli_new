import React from 'react';
import {Text} from 'react-native';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import MessageHistoryView from './MessageHistoryView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';

class MessageHistoryContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: <Text style={{color:"#2799bb"}}>CONVERSATIONS</Text>,
        headerLeft: <BackButton color='#2799bb' onPress={() => navigation.dispatch({type: "Navigation/BACK"})}/>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0,
            borderBottomColor:'#2799bb',

        },
        headerTitleStyle: styles.headerTitleStyle
    });

    state = {
          visibleModal: false,
    };

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }
    onHomePress = () =>{
      this.resetRoute("Dashboard");
    }




    render() {

        return <MessageHistoryView
            {...this.state}
            onHomePress={this.onHomePress}
            onAddCoachesPress={this.onAddCoachesPress}
            {...this.props}

        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
});

export default connect(mapStateToProps, actions)(MessageHistoryContainer);
