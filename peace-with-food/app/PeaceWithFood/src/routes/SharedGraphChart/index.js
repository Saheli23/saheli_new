import React from 'react';
import {Text,TouchableOpacity,
Image,StyleSheet,Alert} from 'react-native';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import SharedGraphChartView from './SharedGraphChartView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
import {Images} from 'PeaceWithFood/src/components/icons';
import {NavigationActions} from 'react-navigation';

let previousRouteName ='SharedWithMe';
const localStyles = StyleSheet.create({
    button: {
        flex: 1,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

class SharedGraphChartContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: <Text style={{color:"white"}}>MY RHYTHMS</Text>,
        headerLeft: <TouchableOpacity
            style={localStyles.button}
            onPress={() =>
                navigation.dispatch(NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({routeName : previousRouteName})
                ]
              }))}
              >
            <Image
                source={Images.whiteBackIcon}
            />
        </TouchableOpacity>,
        headerRight:<TouchableOpacity style={{marginRight:10,height:40,width:40,justifyContent:'center',alignItems:'center'}} onPress={() =>
            navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName:'Dashboard' })
            ]
          }))}><Image source={Images.whiteCloseIcon}/></TouchableOpacity>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: '#4cc5e7',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTitleStyle: styles.headerTitleStyle,
    });

    state = {
          visibleModal: false,
    };

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }
    onHomePress = () =>{
      this.resetRoute("Dashboard");
    }

    onCalendarPress = ()=>{
      this.props.navigation.navigate('Charts');
    }

    onCheckMarkPress = ()=>{
      //remove navigation as per client feedback on 6/6/18
      //this.props.navigation.navigate('MessageQueue');
      Alert.alert('Thank you',
                  "Your reply has been sent!");
    }

    onMessagePress = ()=>{
      this.props.navigation.navigate('MessageHistory');
    }


    render() {

        return <SharedGraphChartView
            {...this.state}
            onHomePress={this.onHomePress}
            onAddCoachesPress={this.onAddCoachesPress}
            onCalendarPress={this.onCalendarPress}
            onCheckMarkPress={this.onCheckMarkPress}
            onMessagePress={this.onMessagePress}
            {...this.props}


        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
});

export default connect(mapStateToProps, actions)(SharedGraphChartContainer);
