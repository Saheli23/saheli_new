import React from 'react';
import {Text, TouchableOpacity, Image,Platform,
Dimensions,View } from 'react-native';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import TermsAndConditionsView from './TermsAndConditionsView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
import {Images} from 'PeaceWithFood/src/components/icons';
import {NavigationActions} from 'react-navigation';

let routeName='Dashboard';
var {height, width} = Dimensions.get('window');

class TermsAndConditionsContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: <Text style={{color:'#2799bb'}}>{'Terms & Conditions'}</Text>,
        headerLeft: <TouchableOpacity style={{flex: 1,
                                            paddingHorizontal: 20,
                                            alignItems: 'center',
                                            justifyContent: 'center'}}
                                       onPress={() => navigation.dispatch({type: "Navigation/BACK"})}><Image source={Images.blackBackIcon}/></TouchableOpacity>,
        headerRight:<View></View>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: '#edf1f2',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTitleStyle: {
            fontSize: 17,
            color: Colors.WHITE,
            alignSelf: 'center',
            textAlign: 'center',
            ...Platform.select({
              android: { width: width/1.8, },
            }),
        }
    });

    state = {
          visibleModal: false,
    };

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }
    onHomePress = () =>{
      this.resetRoute("Dashboard");
    }


    render() {

        return <TermsAndConditionsView
            {...this.state}
            onHomePress={this.onHomePress}
            {...this.props}



        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
});

export default connect(mapStateToProps, actions)(TermsAndConditionsContainer);