import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    AsyncStorage,
    Alert,
    ActivityIndicator

} from 'react-native';

import _         from 'underscore';
import moment    from 'moment';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';
import {connect} from 'react-redux';
import {actions as rhythmActions,selectors as rhythmSelectors} from 'PeaceWithFood/src/store/rhythm';
import {actions as connectActions, selectors as connectSelectors} from 'PeaceWithFood/src/store/pwfconnect';

const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dce0e2'
    },
    section: {
      //  flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? 70 : 50),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,

        alignItems: 'center',
        justifyContent:'center',
        flexDirection:'column',

      },
      borderPanel:{
        height:height/1.2,
        marginTop:(Platform.OS === 'ios' ? height/10 : height/8),
        marginBottom:(Platform.OS === 'ios' ? 10 : 10),
        marginLeft:15,
        marginRight:15,
        // borderBottomWidth:2,
        // borderBottomColor:'#2799bb',
        borderTopWidth:2,
        borderTopColor:'#2799bb'
      },
      answer:{
        color:'#627078',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#a2a4a5',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: 'white',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
      checkPanel:{
        // alignItems: 'center',
        // justifyContent:'center',
        paddingTop:20,
        paddingLeft:20,
        // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
        flexDirection:'row'
      },
      sharedBlock:{
        //padding:40,
        alignItems: 'center',
        justifyContent:'space-between',
        marginTop:5,
        height:35,
        width:width/1.2,
        flexDirection:'row',
        flexWrap: "wrap",
        backgroundColor:'#eaeced',

      },
      timeText:{
        color:'#2799bb',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
        marginRight:5,
      },
      nameText:{
        color:'#447990',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
      }

});



class ManageSupportersView extends Component {

  constructor(props, context) {
      super(props, context);
      this.state = {
         sharedInstances:[],
         coaches: [],
         dataFetch:false,

      };
  }

  componentWillMount(){
        AsyncStorage.getItem('@pwf/user').then((value) =>{
            if(value){
              user =JSON.parse(value)
              this.setState({user_id:user.id});
              //
              // Populate shared rhythm data
              const promise = this.props.getMyCoaches( user.id );
              const self    = this;
              promise.then(function(coaches){
                self.setState({'coaches':coaches,dataFetch:true});
              });
            }
        });
  }

  componentWillReceiveProps(nextProps) {
        //console.log('componentWillReceiveProps ok',nextProps.getSharedInstances.data);
  }

  onRemoveFriendPress(user_id,friend_id){
    let  self    = this;
    const removeCoach =this.props.removeCoachById(user_id,friend_id);
     removeCoach.then(function(data){
       console.log(data);
       if(data && data == 204){
         Alert.alert("", "Friend removed successfully" );
         const promise = self.props.getMyCoaches( user_id );
         promise.then(function(coaches){
           self.setState({'coaches':coaches});
         });
       }
       else{
         Alert.alert('Please try again', "Something went wrong" );
       }
     })
     .catch((error) => {
       console.log(error);
       Alert.alert('Please try again', "Something went wrong" );
       //onFinish();
     });
  }

  render (){
    let {

        onSharePress,
        onAddCoachesPress,
        onShareWithCoachPress,
        visibleModal,
        onModalClose
    } = this.props;

    function titleCase( string ){
        if ( ! string )
               return string;
        string = string + "";
        string = string.trim();
        string = (string.charAt(0).toUpperCase() + string.slice(1));
        return string;
    }
    var self = this;

    var blocks = _.map( this.state.coaches, function( coach, count ){
        var coach_name =    titleCase( coach.full_name||'' )
                         || titleCase( coach.first_name||'' )
                            + " " + titleCase( coach.last_name||'' );
            coach_name = coach_name.trim()
            coachKey   = ""+coach.id
            user_id    = self.state.user_id;
        return (
            <TouchableOpacity

                   key={coachKey}>
            <View  style={localStyle.sharedBlock}
                >
               <Text style={[localStyle.nameText,{flex:1}]}>
                 { coach_name }
                 { " " }
                 { " " }

               </Text>
               <TouchableOpacity onPress={self.onRemoveFriendPress.bind(self,user_id,coachKey)} style={{width:50,height:50,justifyContent:'center',alignItems:'center'}} >
                 <Image source={Images.orangeCloseIcon} />
               </TouchableOpacity>

            </View>
            </TouchableOpacity>
        );
    });
    //console.log('blocks',blocks);


    return (<View style={styles.container}>
        <StatusBar/>
        <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>

            <View style={[localStyle.section, localStyle.bottomSection]}>
             <View style={localStyle.borderPanel}>
              <ScrollView contentContainerStyle={localStyle.sharePanel}>
              {!this.state.dataFetch && (
                 <View style={{alignItems:'center',justifyContent:'center',marginTop:height/2}}>
                   <ActivityIndicator size="large" color="#00bcec" />
                 </View>

              )}
              {this.state.coaches && this.state.dataFetch && (
               blocks
              )}

              </ScrollView>
             </View>
                {/*<View style={localStyle.homeButtonPanel}>
                    <HomeButton
                        onPress={onSharePress}
                        style={localStyle.buttonStyle}
                        loadingColor='#07c4f0'
                        primary
                    />
                </View>*/}

            </View>
        </KeyboardAwareScrollView>
    </View>
    );
  }

}

const mapStateToProps = (state, props) => ({
  //getSharedInstances:rhythmSelectors.getSharedInstances
});


export default connect(mapStateToProps, {
    ...rhythmActions,
    ...connectActions
})(ManageSupportersView);
