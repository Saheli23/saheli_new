import React from 'react';

import {
    Text
} from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';

import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import AfterEatCheckInView from './AfterEatCheckInView';
import {connect} from 'react-redux';
import {actions as authActions, selectors} from 'PeaceWithFood/src/store/auth';
import {actions as userActions} from 'PeaceWithFood/src/store/user';
import styles from 'PeaceWithFood/src/styles';
import { Colors, Sizes } from 'PeaceWithFood/src/styles/constants';
import {NavigationActions} from 'react-navigation';
let routeName='Dashboard';

class AfterEatCheckInContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: <Text style={{color:'#53acc7'}}>WHERE ARE YOU NOW?</Text>,
        headerLeft: <BackButton color={Colors.WHITE} onPress={() =>
            navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName })
            ]
            }))}/>,
        headerStyle: {
            backgroundColor: Colors.RYTHM,
            borderBottomWidth: 0
        },
        headerTitleStyle: styles.headerTitleStyle,
        gesturesEnabled: false
    });

    componentWillReceiveProps(nextProps) {
        if(!nextProps.isLoggedIn) {
            this.resetRoute("Splash");
        }
    }

    async componentDidMount() {

    }

    render() {
        return <AfterEatCheckInView
            {...this.props}
        />
    }
}

const mapStateToProps = (state) => ({
    isLoggedIn: selectors.getIsLoggedIn(state)
});

export default connect(mapStateToProps, {
    ...authActions,
    ...userActions
})(AfterEatCheckInContainer);
