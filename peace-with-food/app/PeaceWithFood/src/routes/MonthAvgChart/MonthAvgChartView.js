import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    ImageBackground


} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';
import Dash from 'react-native-dash';
import { AreaChart,XAxis,YAxis } from 'react-native-svg-charts';
import { Circle} from 'react-native-svg';
import {actions as rhythmActions,selectors as rhythmSelectors} from 'PeaceWithFood/src/store/rhythm';
import {connect} from 'react-redux';
import { BarChart } from 'react-native-svg-charts'
const aspectRatio = height/width;

const localStyle = StyleSheet.create({
    container: {
      //  flex: 1,
      //  backgroundColor: '#dce0e2'
    },
    section: {
      //  flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? 70 : 50),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,

        //alignItems: 'center',
      //  justifyContent:'center',
        marginTop:50,
        flexDirection:'column',

      },
      borderPanel:{
        height:height/1.2,
        marginTop:height/10,
        marginBottom:(Platform.OS === 'ios' ? 10 : 10),
        marginLeft:15,
        marginRight:15,
        // borderBottomWidth:2,
        // borderBottomColor:'#2799bb',
        borderTopWidth:2,
        borderTopColor:'#2799bb'
      },
      answer:{
        color:'#627078',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#a2a4a5',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: 'white',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
      checkPanel:{
        // alignItems: 'center',
        // justifyContent:'center',
        paddingTop:20,
        paddingLeft:20,
        // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
        flexDirection:'row'
      },
      sharedBlock:{
        //padding:40,
        alignItems: 'center',
        justifyContent:'space-between',
        marginTop:5,
        height:35,
        width:width/1.2,
        flexDirection:'row',
        flexWrap: "wrap",
        backgroundColor:'#eaeced',

      },
      timeText:{
        color:'#2799bb',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
        marginRight:5,
      },
      nameText:{
        color:'#447990',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
      }

});

let getFullMonthName;
let fullMonth=[];

class MonthAvgChartView extends Component {

  constructor(props){
    super(props);
    this.state = {
      chatPanelOpen:false,
      visibleModal:false,
      page: true,
      selectedDate:{},
      user_id:null,
      rhythmEntries:[],
      dataFetch:false,

    }

  }

  componentWillMount() {
       //this.setState({selectedDate:this.props.navigation.state.params.dateObj});
       let shortMonth=['00','01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
           fullMonth=["",'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
           getFullMonthName = shortMonth.map((e)=>{
                      return e.indexOf(this.props.navigation.state.params.selectedStartDate.slice(5,7)) > -1;
                   }).indexOf(true);
       AsyncStorage.getItem('@pwf/user').then((value) =>{

         if(value){
           user =JSON.parse(value)
           this.setState({user_id:user.id});
            console.log('user',this.state.user_id);
            this.props.getClientMonthAvgRhythm(this.state.user_id , this.props.navigation.state.params.selectedStartDate,this.props.navigation.state.params.selectedEndDate );
         }
       });


  }
  componentWillReceiveProps(nextProps) {
    console.log('nextProps',nextProps.getAvgRhythmByClient);
    if(nextProps.getAvgRhythmByClient.length > 0){
        this.setState({
          rhythmEntries:nextProps.getAvgRhythmByClient[0].values,
          dataFetch:true
         });
    }
    else{
      this.setState({
        rhythmEntries:[],
        dataFetch:true
      });
    }
  }




  render (){
    let {

        onHomePress,
        onAddCoachesPress,
        onShareWithCoachPress,
        onCalendarPress,
        onCheckMarkPress,
        onMessagePress

    } = this.props;
    let customData=[];
    let NeutralData=[];
    let FamishedData=[];
    let SickData=[];
    let uniqueArray=[];

    FamishedData.push(this.state.rhythmEntries[0] ? this.state.rhythmEntries[0] : 0 );
    NeutralData.push(this.state.rhythmEntries[1] ? this.state.rhythmEntries[1] : 0);
    SickData.push(this.state.rhythmEntries[2] ? this.state.rhythmEntries[2] : 0);
    // this.state.rhythmEntries.map((e,i)=>{
    //
    // });
    const barData = [
      {
          value: FamishedData[0],
          svg: {
              fill: '#eac435',
          },
          // negative: {
          //     fill: 'rgb(244, 115, 65, 0.2)',
          // },
      },
      {
          value: NeutralData[0],
          svg: {
              fill: '#A9A9A9',
          },

      },
      {
          value: SickData[0],
          svg: {
              fill: '#f17d42',
          },

      },


    ]
    //console.log("api data",barData)
    //console.log('customDataX',reverseCustomDataY,customDataY.reverse());
    return (<View style={styles.container}>
        <StatusBar/>
        <ImageBackground  source={Images.chartBackground} resizeMode={Image.resizeMode.streach} style={{width:width, height:(Platform.OS === 'ios' ? (height-60) : (height-80))}}>
        <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>

           {!this.state.dataFetch && (
              <View style={{alignItems:'center',justifyContent:'center',marginTop:height/2}}>
                <ActivityIndicator size="large" color="#00bcec" />
              </View>

           )}
           {this.state.rhythmEntries.length == 0 && this.state.dataFetch && (
              <View style={{alignItems:'center',justifyContent:'center',marginTop:height/2}}>
                <Text style={{color:'#9ea0a3',fontSize:22}}>No records found for selected month!</Text>
              </View>

           )}
           {this.state.rhythmEntries.length > 0 && (
             <View>
             <View style={{flexDirection:'column',alignItems:'center',justifyContent:'center',marginTop:(Platform.OS === 'ios' ? (aspectRatio >2) ? height/8 : 65 : height/7),backgroundColor:'transparent'}}>
               <Text style={{color:'#9ea0a3',fontSize:22}}>Yellow - Under Eating</Text>
               <Text style={{color:'#9ea0a3',fontSize:22}}>Gray - Stayed in the Gray</Text>
               <Text style={{color:'#9ea0a3',fontSize:22}}>Orange - Over Eating</Text>
             </View>
              <BarChart
                 style={ { height: 200,top: (Platform.OS === 'ios' ? height/4 : 0), } }
                 data={ barData}
                 showGrid={false}
                 yAccessor={({ item }) => item.value}
                 contentInset={ { top: 30, bottom: 30 } }
             />
             </View>

                  )
                }


        </KeyboardAwareScrollView>
          </ImageBackground>

          <View style={{height: 60, backgroundColor: '#4cc5e7',alignItems:'center',justifyContent:'center',flexDirection:'row',justifyContent:'space-between'}} >
             <Text style={{color:'white',fontSize:20,marginLeft:10}}>{fullMonth[getFullMonthName]} Average</Text>
             <TouchableOpacity onPress={onCalendarPress}>
               <Image
                   source={Images.calenderIcon}
               />
              </TouchableOpacity>
          </View>

    </View>
    );
  }

}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
    getAvgRhythmByClient: rhythmSelectors.getClientAvgRhythm(state)
});

export default connect(mapStateToProps, {
    ...rhythmActions
})(MonthAvgChartView);
