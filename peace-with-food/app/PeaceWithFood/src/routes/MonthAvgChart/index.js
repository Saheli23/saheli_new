import React from 'react';
import {Text,TouchableOpacity,Image,StyleSheet,Platform,
Dimensions,
View} from 'react-native';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import MonthAvgChartView from './MonthAvgChartView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
import {Images} from 'PeaceWithFood/src/components/icons';
import {NavigationActions} from 'react-navigation';
let routeName='Dashboard';
var {height, width} = Dimensions.get('window');
const localStyles = StyleSheet.create({
    button: {
        flex: 1,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
class MonthAvgChartContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: <Text style={{color:"white"}}>Month Average</Text>,
        headerLeft: <TouchableOpacity
            style={localStyles.button}
            onPress={() => navigation.dispatch({type: "Navigation/BACK"})}
        >
            <Image
                source={Images.whiteBackIcon}
            />
        </TouchableOpacity>,
        headerRight:<TouchableOpacity  style={{marginRight:10,height:40,width:40,justifyContent:'center',alignItems:'center'}} onPress={() =>
                navigation.dispatch(NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName })
                ]
              }))}><Image source={Images.whiteCloseIcon}/></TouchableOpacity>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: '#4cc5e7',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTitleStyle: {
            fontSize: 17,
            color: Colors.WHITE,
            alignSelf: 'center',
            textAlign: 'center',
            ...Platform.select({
              android: { width: width/1.8, },
            }),
        }
    });

    state = {
          visibleModal: false,
    };

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }
    onHomePress = () =>{
      this.resetRoute("Dashboard");
    }

    onCalendarPress = ()=>{
      this.props.navigation.navigate('Charts');
    }

    onCheckMarkPress = ()=>{
      this.props.navigation.navigate('MessageQueue');
    }

    onMessagePress = ()=>{
      this.props.navigation.navigate('MessageHistory');
    }


    render() {

        return <MonthAvgChartView
            {...this.state}
            onHomePress={this.onHomePress}
            onAddCoachesPress={this.onAddCoachesPress}
            onCalendarPress={this.onCalendarPress}
            onCheckMarkPress={this.onCheckMarkPress}
            onMessagePress={this.onMessagePress}
            {...this.props}


        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
});

export default connect(mapStateToProps, actions)(MonthAvgChartContainer);
