import React ,{Component} from 'react';
import Expo from 'expo';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    TextInput,
    AsyncStorage,
    Alert,
    Button

} from 'react-native';


import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

//import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';
import RadioButton from 'react-native-radio-button';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import {actions as rhythmActions,selectors as rhythmSelectors} from 'PeaceWithFood/src/store/rhythm';
import {actions as connectActions, selectors as connectSelectors} from 'PeaceWithFood/src/store/pwfconnect';
import {NavigationActions} from 'react-navigation';
import _ from 'underscore';
 const firebase = require("firebase");
import { Permissions, Notifications } from 'expo';
var fetch = require('node-fetch');

const lo = require('lodash');
let messageId='';

let routeName='Dashboard';
const aspectRatio = height/width;

const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#00bded'
    },
    section: {
        flex: 1
    },
    bottomSection: {
        backgroundColor: '#00bded',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? height/12 : 25),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#009fc6',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,
        marginTop:height/6,
        alignItems: 'center',
        justifyContent:'center',
        flexDirection:'column'
      },
      answer:{
        //flex:0.9,
        color:'white',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#a2a4a5',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: '#ebf6f9',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
      checkPanel:{
        // alignItems: 'center',
        // justifyContent:'center',
        paddingTop:20,
        paddingLeft:20,
        // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
        flexDirection:'row'
      },
      sharedBlock:{
        alignItems: 'center',
        justifyContent:'center',
        marginTop:(Platform.OS === 'ios' ? height/40 : -height/8),
        height:(Platform.OS === 'ios' ? ((height < 700) ? height/8 :height/10) : height/5),
        marginBottom:(Platform.OS === 'ios' ? 0 : 10),
        width:width/1.1,
        flexDirection:'row',
        flexWrap: "wrap",
      },
      timeText:{
        color:'white',
        fontSize:(Platform.OS === 'ios' ? 30 : 30),
        //fontWeight:'500',
        fontFamily:'Sarabun-Bold'
      },
      shareWithCoach:{
        flexDirection:'row',
        backgroundColor:'#0ecbf3',
        width:300,
        height:40,
        padding:10,
        //marginTop:10,
        justifyContent:'space-between'
      },
      scrollPanel:{
        height:400
      },


});



class SickView extends Component {

  constructor(props){
    super(props);
    this.state={
      moodFamished:'',
      moodSick:'',
      consciously_restricting:'',
      social_pressure_to_eat:'',
      thoughts:'',
      connectThought:'',
      rhythm_id:null,
      options:{},
      famishedCheck:[],
      Busy_Forgot:false,
      Food_Unavailable:false,
      Mindless_Eating:false,
      Hunger:false,
      Tempting_Food:false,
      textVal:'',
      checkin_recorded:false,
      playedSound:false,
      userEmail:'',
      userName:'',
      messageId:'',
      coaches: [],
      selectedCoaches: {},
      notShareWithCoaches: {},
      selectedTimes:{
        "TODAY": true,
      },
      expoToken:'',
      isEmptyCoachArr:true,
      coach_ids:{}

    }

  }

  componentWillMount(){
    this.setState({checkin_recorded:false});
    AsyncStorage.getItem('@pwf/user').then((value) =>{
      if(value){
        user =JSON.parse(value);
        console.log(user);
        let firebasePassword ='firebase';
        let email=user.email
          this.setState({userEmail:user.email});
          this.setState({userName:user.first_name});
          // Populate shared rhythm data
          const promise = this.props.getMyCoaches( user.id );
          const self    = this;
          promise.then(function(coaches){
            self.setState({'coaches':coaches});
          });

        }
      });

    // console.log('navigate',this.props);
    //  const { params } = this.props.navigation.state;
  }
  componentDidMount() {
    this.registerForPushNotificationsAsync();
  }

  registerForPushNotificationsAsync = async () => {
    // console.log('noti', await Notifications.getExpoPushTokenAsync())
        const { existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
        let finalStatus = existingStatus;

        // only ask if permissions have not already been determined, because
        // iOS won't necessarily prompt the user a second time.
        if (existingStatus !== 'granted') {
            // Android remote notification permissions are granted during the app
            // install, so this will only ask on iOS
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }

        // Stop here if the user did not grant permissions
        if (finalStatus !== 'granted') {
            return;
        }

        // Get the token that uniquely identifies this device
        let token = await Notifications.getExpoPushTokenAsync();
      //  console.log('token',token);
        this.setState({expoToken:token});

        // // POST the token to our backend so we can use it to send pushes from there
        // var updates = {}
        // updates['/expoToken'] = token
        // await firebase.database().ref('/users/' + currentUser.uid).update(updates)
        //call the push notification
    }

  componentWillReceiveProps(nextProps) {
      // console.log('Rhythm Created',nextProps.addRhythmOptions);
       let self=this;
    //if(nextProps.addRhythmOptions && this.state.checkin_recorded && !this.state.isEmptyCoachArr){
    //commented out for jouranl/connect mixed up
      if(nextProps.addRhythmOptions && this.state.checkin_recorded ){
            let options={};
            this.setState({rhythm_id:nextProps.addRhythmOptions.id});

          //commented out for jouranl/connect mixed up
             // let coaches = Object.keys(this.state.coach_ids);
             // let sharedCoaches=[];
             // for(i=0; i<coaches.length; i++){
             //    sharedCoaches.push({userId:coaches[i]});
             // }

            const FIREBASE_REF_MESSAGES = firebase.database().ref('messages')
            let currentUser = firebase.auth().currentUser
            let createdAt = new Date().getTime()
            let chatMessage = {
              _id:createdAt,
              rhythm_id:this.props.navigation.state.params.healthStateId,
              text: this.state.thoughts,
              createdAt: createdAt,
              expoToken:this.state.expoToken,
              user: {
                name: self.state.userName,
                email: self.state.userEmail
              }
            };

            FIREBASE_REF_MESSAGES.push([chatMessage], (error) => {
              if (error) {
              console.log('error')
              } else {
                var query = firebase.database().ref().child("messages").limitToLast(1);
                query.on("child_added", function(snapshot) {
                   messageId = snapshot.key;
                  self.setState({messageId:snapshot.key});
                  var timestamp = snapshot.val();
                  //console.log('messageId',messageId,self.state.thoughts);
                  self.props.updateRhythmDetails(nextProps.addRhythmOptions.client_id,nextProps.addRhythmOptions.id,self.state.thoughts,options,messageId);
                  self.setState({connectThought:''});
                  messageId='';

                });

              }
            })


               //this.setState({thoughts:''});
               //commented for remove extra thought value
            //   let options={};
            //   let checks={}
            //   if(nextProps.addRhythmOptions.value<=3){
            //      //console.log({"Busy/ Forgot" :self.state.Busy_Forgot ,"Food Unavailable" :self.state.Food_Unavailable});
            //      checks={"Busy_Forgot" :self.state.Busy_Forgot,"Food_Unavailable" :self.state.Food_Unavailable}
            //   options = {checks,Mood:self.state.moodFamished,consciously_restricting:(self.state.consciously_restricting == 'Other' ? self.state.textVal : self.state.consciously_restricting)}
            //   }
            //   if(nextProps.addRhythmOptions.value >3 && nextProps.addRhythmOptions.value<=6){
            //   options = {}
            //   }
            //   if(nextProps.addRhythmOptions.value >=7){
            //   checks={"Mindless_Eating" :self.state.Mindless_Eating, "Hunger" :self.state.Hunger, 'Tempting_Food':self.state.Tempting_Food}
            //   options = {checks,Mood:(self.state.moodSick == 'Other' ? self.state.textVal : self.state.moodSick),social_pressure_to_eat:self.state.social_pressure_to_eat}
            //   }
            //   this.props.updateRhythmDetails(nextProps.addRhythmOptions.client_id,nextProps.addRhythmOptions.id,this.state.thoughts,options);
          }
        //Code commented out for journal/connect mixedup
       // if(nextProps.addRhythmOptions && this.state.checkin_recorded && this.state.isEmptyCoachArr){
       //   let options={};
       //   this.setState({rhythm_id:nextProps.addRhythmOptions.id});
       //   messageId='';
       //   self.props.updateRhythmDetails(nextProps.addRhythmOptions.client_id,nextProps.addRhythmOptions.id,self.state.thoughts,options,messageId);
       //   self.setState({thoughts:''});
       // }
  }

  setMoodValue(name,text){

    this.setState({[name]:text});

  }
  radioSelectionByLable(name,text){
    (this.state[name] == text ) ? true : false
  }

  setCheckValue(name){
    this.setState({[name]:!this.state[name]});
    //console.log("set",this.state[name]);
  }

   // saveThoughtValue(user_id,healthStateId,onModalClose,onShareWithCoachPress,time_periods, coach_ids,isJournal){
   //     let self=this;
   //     // console.log('user_id,healthStateId',user_id,healthStateId);
   //     // let checkvalue={"Busy_Forgot" :self.state.Busy_Forgot,"Food_Unavailable" :self.state.Food_Unavailable}
   //     // let checkingChecks=Object.keys(checkvalue).every((k) => !checkvalue[k]);
   //     //  if( healthStateId <=3){
   //     //       if(checkingChecks && this.state.thoughts =='' && self.state.moodFamished=='' &&  self.state.consciously_restricting ==''){
   //     //         Alert.alert("", "Please give some thoughts" );
   //     //     }
   //     //     else{
   //     //
   //     //            this.props.createRhythm(user_id,healthStateId);
   //     //            this.setState({checkin_recorded:true});
   //     //            AsyncStorage.setItem('CheckedIn','yes');
   //     //            onModalClose()
   //     //     }
   //     //   }
   //     // if(healthStateId >3 && healthStateId<=6){
   //     //    if(this.state.thoughts){
   //     //     this.props.createRhythm(user_id,healthStateId);
   //     //     this.setState({checkin_recorded:true});
   //     //     AsyncStorage.setItem('CheckedIn','yes');
   //     //     onModalClose()
   //     //   }
   //     //   else{
   //     //     Alert.alert("", "Please give some thoughts" );
   //     //   }
   //     // }
   //     // if(healthStateId >=7){
   //     //   let checkObj={"Mindless_Eating" :self.state.Mindless_Eating, "Hunger" :self.state.Hunger, 'Tempting_Food':self.state.Tempting_Food}
   //     //   let checkingCheckObj =Object.keys(checkObj).every((k) => !checkObj[k]);
   //     //     if(checkingCheckObj && this.state.thoughts =='' && self.state.moodSick=='' &&  self.state.social_pressure_to_eat ==''){
   //     //       Alert.alert("", "Please give some thoughts" );
   //     //   }
   //     // else{
   //     //
   //     //        this.props.createRhythm(user_id,healthStateId);
   //     //        this.setState({checkin_recorded:true});
   //     //        AsyncStorage.setItem('CheckedIn','yes');
   //     //        onModalClose()
   //     // } commented for remove extra thought value
   //     if(isJournal){
   //       if(self.state.thoughts ==''){
   //           if(Platform.OS === 'ios'){
   //           onModalClose()
   //           setTimeout(() => {
   //             //Alert.alert("", "Please give some thoughts" );
   //             Alert.alert(
   //                   '',
   //                   "Please give some thoughts",
   //                   [
   //                     {text: 'OK', onPress: () => onShareWithCoachPress('journal')},
   //                   ],
   //                 )
   //           }, 10);
   //         }
   //         if(Platform.OS != 'ios'){
   //           Alert.alert("", "Please give some thoughts" );
   //         }
   //       }
   //       else{
   //
   //
   //              this.props.createRhythm(user_id,healthStateId);
   //              this.setState({checkin_recorded:true,isEmptyCoachArr:_.isEmpty(coach_ids),coach_ids:coach_ids});
   //              AsyncStorage.setItem('CheckedIn','yes');
   //              //
   //              // if(_.isEmpty(coach_ids) == false){
   //              // var responses = []
   //              // var coach_count = 0;
   //              // var time_count  = 0;
   //              // _.each( time_periods, (number_of_days, time_key) => {
   //              //     _.each( coach_ids, (x, coach_id) => {
   //              //         var m = time_key.match(/DAYS_([0-9]+)/);
   //              //         if ( m ){
   //              //              time_key = 'DAYS';
   //              //              number_of_days = m[1];
   //              //         }
   //              //         var response = this.props.createSharedRhythms(
   //              //                      user_id,
   //              //                      time_key,
   //              //                      number_of_days,
   //              //                      coach_id
   //              //                    );
   //              //         responses.push(response)
   //              //         coach_count++;
   //              //    });
   //              //    time_count++;
   //              //
   //              // });
   //              //
   //              // if ( coach_count==0 ){
   //              //      Alert.alert('Please select a friend');
   //              //      return;
   //              // }
   //              //
   //              //
   //              // const promise = Promise.all(responses);
   //              // const c = coach_count==1? "coach":"coaches";
   //              // //var self = this;
   //              //
   //              // promise.then(function(){
   //              //
   //              //     self.setState({ visibleModal: false})
   //              //
   //              //     setTimeout(function(){
   //              //       Alert.alert('Thank you',
   //              //                   "Your data will be sent shortly.");
   //              //     },500);
   //              //
   //              // },function(){
   //              //
   //              //     setTimeout(function(){
   //              //       Alert.alert('Error',
   //              //                   "Oops, something went wrong. Sometimes "+
   //              //                   "this error message appears when you "+
   //              //                   "share the same data twice. If you haven't "+
   //              //                   "already gone through this process, " +
   //              //                   "please let us know what issues you are having.");
   //              //     },500);
   //              //
   //              // });
   //              // promise.finally(function(){
   //              //     self.setState({ loading: false})
   //              // });
   //              // }
   //
   //              onModalClose()
   //       }
   //    }
   //    else{
   //      if(self.state.connectThought ==''){
   //          if(Platform.OS === 'ios'){
   //          onModalClose()
   //          setTimeout(() => {
   //            //Alert.alert("", "Please give some thoughts" );
   //            Alert.alert(
   //                  '',
   //                  "Please give some thoughts",
   //                  [
   //                    {text: 'OK', onPress: () => onShareWithCoachPress('connect')},
   //                  ],
   //                )
   //          }, 10);
   //        }
   //        if(Platform.OS != 'ios'){
   //          Alert.alert("", "Please give some thoughts" );
   //        }
   //      }
   //      else{
   //
   //
   //             this.props.createRhythm(user_id,healthStateId);
   //             this.setState({checkin_recorded:true,isEmptyCoachArr:_.isEmpty(coach_ids),coach_ids:coach_ids});
   //             AsyncStorage.setItem('CheckedIn','yes');
   //
   //             if(_.isEmpty(coach_ids) == false){
   //             var responses = []
   //             var coach_count = 0;
   //             var time_count  = 0;
   //             _.each( time_periods, (number_of_days, time_key) => {
   //                 _.each( coach_ids, (x, coach_id) => {
   //                     var m = time_key.match(/DAYS_([0-9]+)/);
   //                     if ( m ){
   //                          time_key = 'DAYS';
   //                          number_of_days = m[1];
   //                     }
   //                     var response = this.props.createSharedRhythms(
   //                                  user_id,
   //                                  time_key,
   //                                  number_of_days,
   //                                  coach_id
   //                                );
   //                     responses.push(response)
   //                     coach_count++;
   //                });
   //                time_count++;
   //
   //             });
   //
   //             if ( coach_count==0 ){
   //                  Alert.alert('Please select a friend');
   //                  return;
   //             }
   //
   //
   //             const promise = Promise.all(responses);
   //             const c = coach_count==1? "coach":"coaches";
   //             //var self = this;
   //
   //             promise.then(function(){
   //
   //                 self.setState({ visibleModal: false})
   //
   //                 setTimeout(function(){
   //                   Alert.alert('Thank you',
   //                               "Your data will be sent shortly.");
   //                 },500);
   //
   //             },function(){
   //
   //                 setTimeout(function(){
   //                   Alert.alert('Error',
   //                               "Oops, something went wrong. Sometimes "+
   //                               "this error message appears when you "+
   //                               "share the same data twice. If you haven't "+
   //                               "already gone through this process, " +
   //                               "please let us know what issues you are having.");
   //                 },500);
   //
   //             });
   //             promise.finally(function(){
   //                 self.setState({ loading: false})
   //             });
   //             }
   //
   //             onModalClose()
   //      }
   //    }
   //
   //
   //
   //
   // }

   saveThoughtValue(user_id,healthStateId,onModalClose,onShareWithCoachPress,time_periods, coach_ids,isJournal){
       let self=this;

       if(self.state.thoughts ==''){
            if(Platform.OS === 'ios'){
            onModalClose()
            setTimeout(() => {
              //Alert.alert("", "Please give some thoughts" );
              Alert.alert(
                    '',
                    "Please give some thoughts",
                    [
                      {text: 'OK', onPress: () => onShareWithCoachPress('journal')},
                    ],
                  )
            }, 10);
          }
          if(Platform.OS != 'ios'){
            Alert.alert("", "Please give some thoughts" );
          }
        }
        else{


               this.props.createRhythm(user_id,healthStateId);
               this.setState({checkin_recorded:true,isEmptyCoachArr:_.isEmpty(coach_ids),coach_ids:coach_ids});
               AsyncStorage.setItem('CheckedIn','yes');

               // if(_.isEmpty(coach_ids) == false){
               // var responses = []
               // var coach_count = 0;
               // var time_count  = 0;
               // _.each( time_periods, (number_of_days, time_key) => {
               //     _.each( coach_ids, (x, coach_id) => {
               //         var m = time_key.match(/DAYS_([0-9]+)/);
               //         if ( m ){
               //              time_key = 'DAYS';
               //              number_of_days = m[1];
               //         }
               //         var response = this.props.createSharedRhythms(
               //                      user_id,
               //                      time_key,
               //                      number_of_days,
               //                      coach_id
               //                    );
               //         responses.push(response)
               //         coach_count++;
               //    });
               //    time_count++;
               //
               // });
               //
               // if ( coach_count==0 ){
               //      Alert.alert('Please select a friend');
               //      return;
               // }
               //
               //
               // const promise = Promise.all(responses);
               // const c = coach_count==1? "coach":"coaches";
               // //var self = this;
               //
               // promise.then(function(){
               //
               //     self.setState({ visibleModal: false})
               //
               //     setTimeout(function(){
               //       Alert.alert('Thank you',
               //                   "Your data will be sent shortly.");
               //     },500);
               //
               // },function(){
               //
               //     setTimeout(function(){
               //       Alert.alert('Error',
               //                   "Oops, something went wrong. Sometimes "+
               //                   "this error message appears when you "+
               //                   "share the same data twice. If you haven't "+
               //                   "already gone through this process, " +
               //                   "please let us know what issues you are having.");
               //     },500);
               //
               // });
               // promise.finally(function(){
               //     self.setState({ loading: false})
               // });
               // }

               onModalClose()
        }
    //  }




   }

   onDonePress = (user_id,healthStateId) =>{
     //console.log(this.state.user_id,this.state.healthStateId);
     if(!this.state.checkin_recorded){
           this.props.createRhythm(user_id,healthStateId);
           AsyncStorage.setItem('CheckedIn','yes');
           this.props.navigation.dispatch(NavigationActions.reset({
           index: 0,
           actions: [
               NavigationActions.navigate({ routeName })
           ]
         }))
    }
    else
    {
       //Alert.alert("", "Check in details already added" );
       this.props.navigation.dispatch(NavigationActions.reset({
       index: 0,
       actions: [
           NavigationActions.navigate({ routeName })
       ]
     }))
    }
   }

   setOption_coach( coach_id ){
         const key = ""+coach_id;
         if ( this.state.selectedCoaches[key] ){
              delete this.state.selectedCoaches[key];
         } else {
              this.state.selectedCoaches[key] = true;
              // force update
              this.setState({selectedCoaches: this.state.selectedCoaches });
         }
         //console.log( 'coach_id', coach_id );
   }

  _renderModalContent = (onModalClose,onReminderPress,healthState,user_id,healthStateId,onShareWithCoachPress,onAddCoachesPress,isJournal) => {
    let Reason1,Reason2,Reason3,Reason4;
    let healthStateArray=[['Famished', 'Starving', 'Very Hungry'], ['Ready', 'Neutral', 'Full & Satisfied'], ['Overly Full', 'Stuffed', 'Sick']];
    let getChangeVal = healthStateArray.map(function(e){
	                 return e.indexOf(healthState) > -1;
                }).indexOf(true);
              //  console.log('getChangeVal',healthStateArray[getChangeVal],getChangeVal);

    //if(getChangeVal ==0){
      let reasonArray=[[
                        {text:'Busy/ Forgot',type:'checkbox',name:'Busy_Forgot'},
                        {text:'Food Unavailable',type:'checkbox',name:'Food_Unavailable'},
                        {text:'Consciously Restricting',type:'label'},
                        {text:'Weight worries',type:'radio',name:'consciously_restricting'},
                        {text:'I ate too much last night',type:'radio',name:'consciously_restricting'},
                        {text:'Other',type:'input',name:'consciously_restricting'},
                        {text:'Mood',type:'label'},
                        {text:'Stressed',type:'radio',name:'moodFamished'},
                        {text:'Lonely',type:'radio',name:'moodFamished'},
                        {text:'Angry',type:'radio',name:'moodFamished'},
                        {text:'Anxious',type:'radio',name:'moodFamished'},
                        {text:'Sad',type:'radio',name:'moodFamished'},
                        {text:'Tired',type:'radio',name:'moodFamished'}],
                        [],
                        [{text:'Hunger',type:'checkbox',name:'Hunger'},
                        {text:'Tempting Food',type:'checkbox',name:'Tempting_Food'},
                        {text:'Mindless Eating',type:'checkbox',name:'Mindless_Eating'},
                        {text:'Mood',type:'label'},
                        {text:'Stressed',type:'radio',name:'moodSick'},
                        {text:'Lonely',type:'radio',name:'moodSick'},
                        {text:'Angry',type:'radio',name:'moodSick'},
                        {text:'Anxious',type:'radio',name:'moodSick'},
                        {text:'Sad',type:'radio',name:'moodSick'},
                        {text:'Tired',type:'radio',name:'moodSick'},
                        {text:'Other',type:'input',name:'moodSick'},
                        {text:'Social Pressure to Eat',type:'label'},
                        {text:'Friends',type:'radio',name:'social_pressure_to_eat'},
                        {text:'Family',type:'radio',name:'social_pressure_to_eat'}]]
      let userSelection = reasonArray[getChangeVal];
      // console.log('=======================================');
      // console.log(userSelection);
      // console.log('=======================================');
    //}
    function titleCase( string ){
        if ( ! string )
               return string;
        string = string + "";
        string = string.trim();
        string = (string.charAt(0).toUpperCase() + string.slice(1));
        return string;
    }
    var self = this;

    var blocks = _.map( this.state.coaches, function( coach, count ){
        var coach_name =    titleCase( coach.full_name||'' )
                         || titleCase( coach.first_name||'' )
                            + " " + titleCase( coach.last_name||'' );
            coach_name = coach_name.trim()
            coachKey   = ""+coach.id
            //user_id    = self.state.user_id;


        return (
            <View
                   style={localStyle.checkPanel}
                   key={count}>
                <CheckBox
                      style={{alignItems:'center',marginRight:10,flex:1}}
                      isChecked={self.state.selectedCoaches[coachKey]}
                      checkBoxColor='#e0e4e7'
                      onClick={self.setOption_coach.bind(self,coach.id)}
                      rightText={coach_name }
                      rightTextStyle={{color: '#7ec2d6',fontSize: 18}}
                  />
                </View>
        );
    });

    if ( ! blocks || blocks.length === 0 || !blocks[0] ){

        blocks = null;
    }

    if(!isJournal){
        return(
      <View style={localStyle.modalContent}>
      <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>
        <View>
         <View style={{flexDirection:'row',justifyContent:'space-between',}}>

          <TouchableOpacity style={{width:50,height:50,flex:2,justifyContent:'center'}} onPress={onModalClose}>
            <Image source={Images.backIcon} />
          </TouchableOpacity>
          <TextButton
              label={"Send"}
              onPress={this.saveThoughtValue.bind(this,user_id,healthStateId,onModalClose,onShareWithCoachPress,this.state.selectedTimes,
              this.state.selectedCoaches,isJournal)}
              style={[localStyle.buttonStyle,{borderRadius:5,justifyContent:'center',alignItems:'center'}]}
              loadingColor='transparent'
              primary
          />
        </View>
        {/*<KeyboardAwareScrollView enableAutoAutomaticScroll={true} style={{height:(userSelection.length > 0 ? 350 : 150)}}>*/}
        <View enableAutoAutomaticScroll={true} style={{height:150}}>
              <View style={{borderBottomColor:'#bdc0c1',borderBottomWidth:1,}}>
               <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                  <Text style={[localStyle.iconText,{marginTop:0}]}>Share with friends</Text>
               </View>

              </View>
          <ScrollView  style={{paddingBottom:100}}>

              { blocks || (

              <View style={{marginTop:50}}>
                  <TouchableOpacity style={localStyle.shareBox} onPress={() => {onAddCoachesPress.apply(this,arguments);onModalClose.apply(this,arguments);}}>
                    <Image
                      source={Images.coachIcon}
                    />
                  </TouchableOpacity>
              </View>


              )}

          </ScrollView>

          </View>
                  <View style={{flexDirection:'row',marginBottom:5,marginTop:10,justifyContent:'space-between',width:width/1.30,}}>
                  <ScrollView>


                      <View style={{marginTop:15}}>
                              <Text style={[localStyle.buttonText,{fontWeight:'500',marginBottom:5,marginLeft:10}]}>
                                 Thoughts for PwF Connect:
                               </Text>
                               <TextInput
                                   multiline={true}
                                   numberOfLines={5}
                                   style={{borderWidth:2,borderColor:'#2799bb',width:(Platform.OS === 'ios' ? ((aspectRatio >1.6) ? (width/1.38) :(width/1.19)) : (width/1.33)),height:(Platform.OS === 'ios' ? ((aspectRatio >1.6) ? (height/6) :(height/8)) : (height/6)),padding:5}}
                                   placeholder="Type thoughts/questions here (share in PwF Connect if you choose.) Find all of your notes in the Summary by tapping the yellow dots."
                                   placeholderTextColor="#446a7a"
                                   underlineColorAndroid='transparent'
                                   onChangeText={ (text) => this.setState({ connectThought: text }) }

                               />
                        </View>
                    </ScrollView>
                        {/* remove with respect to https://cn.teamwork.com/#tasks/16693463
                        <Text style={[localStyle.buttonText,{fontWeight:'500',flex:1}]}><Text style={{color:'#e87f47'}}>{healthState}?</Text> tell me more</Text>
                        <TouchableOpacity style={{width:50,marginLeft:10,height:50}} onPress={this.saveThoughtValue.bind(this,user_id,healthStateId,onModalClose)}>
                          <Image style={{marginRight:5,marginTop:2,marginLeft:20}} source={Images.checkMarkIcon} />
                        </TouchableOpacity>*/}
                       {/*<Text style={[localStyle.buttonText,{fontWeight:'500',flex:2}]}></Text>
                        <TextButton
                            label={"Save"}
                            onPress={this.saveThoughtValue.bind(this,user_id,healthStateId,onModalClose)}
                            style={[localStyle.buttonStyle,{borderRadius:5,justifyContent:'center',alignItems:'center'}]}
                            loadingColor='transparent'
                            primary
                        />*/}

                       {/*<TouchableOpacity onPress={onReminderPress} >
                       <Image
                         source={Images.blueClockIcon}
                       />
                       </TouchableOpacity>*/}

                     </View>
                </View>
              </KeyboardAwareScrollView>
        </View>
    )
    }
    else{
        return(
      <View style={localStyle.modalContent}>
      <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>
        <View>
         <View style={{flexDirection:'row',justifyContent:'space-between',}}>

          <TouchableOpacity style={{width:50,height:50,flex:2,justifyContent:'center'}} onPress={onModalClose}>
            <Image source={Images.backIcon} />
          </TouchableOpacity>
          <TextButton
              label={"Save"}
              onPress={this.saveThoughtValue.bind(this,user_id,healthStateId,onModalClose,onShareWithCoachPress,this.state.selectedTimes,
              this.state.notShareWithCoaches,isJournal)}
              style={[localStyle.buttonStyle,{borderRadius:5,justifyContent:'center',alignItems:'center'}]}
              loadingColor='transparent'
              primary
          />
        </View>
        {/*<KeyboardAwareScrollView enableAutoAutomaticScroll={true} style={{height:(userSelection.length > 0 ? 350 : 150)}}>*/}
        {/*<View enableAutoAutomaticScroll={true} style={{height:height/12,}}>
               <View style={{borderBottomColor:'#bdc0c1',borderBottomWidth:1,}}>
               <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                  <Text style={[localStyle.iconText,{marginTop:0}]}>Share with friends</Text>
               </View>

              </View>
          {/* <ScrollView  style={{paddingBottom:100}}>

              { blocks || (

              <View style={{marginTop:50}}>
                  <TouchableOpacity style={localStyle.shareBox} onPress={() => {onAddCoachesPress.apply(this,arguments);onModalClose.apply(this,arguments);}}>
                    <Image
                      source={Images.coachIcon}
                    />
                  </TouchableOpacity>
              </View>


              )}

          </ScrollView>

          </View>*/}
                  <View enableAutoAutomaticScroll={true} style={{flexDirection:'row',marginBottom:5,marginTop:10,justifyContent:'space-between',width:width/1.30,}}>
                  <ScrollView>


                      <View style={{marginTop:15}}>
                              <Text style={[localStyle.buttonText,{fontWeight:'500',marginBottom:5,marginLeft:10}]}>
                                 Thoughts:
                               </Text>
                               <TextInput
                                   multiline={true}
                                   numberOfLines={5}
                                   style={{borderWidth:2,borderColor:'#2799bb',width:(Platform.OS === 'ios' ? ((aspectRatio >1.6) ? (width/1.38) :(width/1.19)) : (width/1.33)),height:(Platform.OS === 'ios' ? ((aspectRatio >1.6) ? (height/6) :(height/8)) : (height/6)),padding:5}}
                                   placeholder="Type thoughts/questions here (share in PwF Connect if you choose.) Find all of your notes in the Summary by tapping the yellow dots."
                                   placeholderTextColor="#446a7a"
                                   underlineColorAndroid='transparent'
                                   onChangeText={ (text) => this.setState({ thoughts: text }) }

                               />
                        </View>
                    </ScrollView>
                        {/* remove with respect to https://cn.teamwork.com/#tasks/16693463
                        <Text style={[localStyle.buttonText,{fontWeight:'500',flex:1}]}><Text style={{color:'#e87f47'}}>{healthState}?</Text> tell me more</Text>
                        <TouchableOpacity style={{width:50,marginLeft:10,height:50}} onPress={this.saveThoughtValue.bind(this,user_id,healthStateId,onModalClose)}>
                          <Image style={{marginRight:5,marginTop:2,marginLeft:20}} source={Images.checkMarkIcon} />
                        </TouchableOpacity>*/}
                       {/*<Text style={[localStyle.buttonText,{fontWeight:'500',flex:2}]}></Text>
                        <TextButton
                            label={"Save"}
                            onPress={this.saveThoughtValue.bind(this,user_id,healthStateId,onModalClose)}
                            style={[localStyle.buttonStyle,{borderRadius:5,justifyContent:'center',alignItems:'center'}]}
                            loadingColor='transparent'
                            primary
                        />*/}

                       {/*<TouchableOpacity onPress={onReminderPress} >
                       <Image
                         source={Images.blueClockIcon}
                       />
                       </TouchableOpacity>*/}

                     </View>
                </View>
              </KeyboardAwareScrollView>
        </View>
    )
    }

};



  playSound(healthStateId){
      if ( ! healthStateId )
             return;
      const self = this;
      (async function(healthStateId){

        if ( self.state.playedSound )
             return;
        self.state.playedSound = true;
        const animation_length = 783;
        setTimeout(async() => {

          const soundObject = new Expo.Audio.Sound();
          if      (healthStateId < 4)
                   await soundObject.loadAsync(require('../../sound/glock-note-F1.wav'));
          else if (healthStateId > 6)
                   await soundObject.loadAsync(require('../../sound/glock-note-F1.wav'));
                   // when in the 'zone'
          else     await soundObject.loadAsync(require('../../sound/glock-note-F1.wav'));
          // play sound
          await soundObject.playAsync();

        }, animation_length );

      })(healthStateId);
  }



  render (){
    let {

        onHomePress,
        onAddCoachesPress,
        onShareWithCoachPress,
        visibleModal,
        onModalClose,
        onReminderPress,
        healthState,
        user_id,
        healthStateId,
        isJournal,
    } = this.props;
    let healthStateArray=[['Famished', 'Starving', 'Very Hungry'], ['Ready', 'Neutral', 'Full & Satisfied'], ['Overly Full', 'Stuffed', 'Sick']];
    let color=['#eac435', '#1e7891', '#e87f47'];
    let images=[Images.yellowCheckIcon,Images.greenCheckIcon,Images.whiteCheckIcon];

    let getChangeVal = healthStateArray.map(function(e){
	                 return e.indexOf(healthState) > -1;
                }).indexOf(true);


    this.playSound(healthStateId);

    return (<View style={styles.container}>
        <StatusBar/>
        <KeyboardAwareScrollView keyboardShouldPersistTaps='always' style={localStyle.container} contentContainerStyle={localStyle.container}>
            <Modal isVisible={visibleModal}>
             {this._renderModalContent(onModalClose,onReminderPress,healthState,user_id,healthStateId,onShareWithCoachPress,onAddCoachesPress,isJournal)}
           </Modal>
            <View style={[localStyle.section, localStyle.bottomSection, {backgroundColor:"#04B2E8"}]}>
              <View style={localStyle.sharePanel}>
                    <Image
                      source={images[getChangeVal]}
                      style={{width:235, height:235}}
                    />
                    <View style={localStyle.sharedBlock}>
                       <Text style={localStyle.timeText}>got it! you are <Text style={{color:color[getChangeVal],}}>{healthState.toLowerCase()}</Text></Text>
                    </View>
                    <View onPress={onShareWithCoachPress} style={[localStyle.shareWithCoach,{width:width/1.3,backgroundColor:'transparent',height:(Platform.OS === 'ios' ? 40 : height/8)}]} >
                        <TextButton
                            label={"Done"}
                            onPress={this.onDonePress.bind(this,user_id,healthStateId)}
                            style={localStyle.buttonStyle}
                            loadingColor='transparent'
                            primary
                            labelStyle={{fontSize:25}}
                        />
                        {/*<TouchableOpacity onPress={onReminderPress}>
                          <Image
                            source={Images.whiteClockIcon}
                          />
                        </TouchableOpacity>*/}

                    </View>

                </View>
                {/* SC  */}
                {/* <View style={localStyle.homeButtonPanel}>
                  <TouchableOpacity onPress={onShareWithCoachPress} style={localStyle.shareWithCoach} >
                  <Text style={localStyle.answer}>tell me more?</Text>
                      <Image
                        style={{marginTop:2}}
                        source={Images.whiteArrowIcon}
                      />
                </TouchableOpacity>
              </View> */}
              <View style={[localStyle.homeButtonPanel,{flexDirection: 'row',justifyContent:'center',alignItems:'center',}]}>
                  <TouchableOpacity onPress={onShareWithCoachPress.bind(this,'journal')} style={[localStyle.shareWithCoach,{justifyContent:'center',alignItems:'center',...Platform.select({
                    android: { padding: 0, },
                  })}]} >
                  {Platform.OS === 'ios' ? (
                    <Text style={[localStyle.answer,{textAlign:'center',fontSize:25,fontFamily:'Sarabun-Bold',}]}>Journal / Connect</Text>
                  ) : (
                    <Text style={[localStyle.answer,{textAlign:'center',fontSize:20,}]}>Journal / Connect</Text>
                  ) }


                </TouchableOpacity>
                {/*

                  <TouchableOpacity onPress={onShareWithCoachPress.bind(this,'connect')} style={[localStyle.shareWithCoach, {width:width/2.5}]} >
                  <Text style={[localStyle.answer,{textAlign:'center', fontWeight:'600'}]}>Connect</Text>
                      <Image
                        style={{marginTop:2}}
                      />
                </TouchableOpacity>*/}
              </View>
              {/* <View style={localStyle.homeButtonPanel}>

              </View> */}

            </View>
        </KeyboardAwareScrollView>
    </View>
    );
  }

}
const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
    addRhythmOptions: rhythmSelectors.addRhythmOptions(state)
});

export default connect(mapStateToProps, {
    ...actions,
    ...rhythmActions,
    ...connectActions
})(SickView);
