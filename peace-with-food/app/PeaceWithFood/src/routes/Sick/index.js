import React from 'react';
import {Text,AsyncStorage,TouchableOpacity,
Image,StyleSheet} from 'react-native';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import SickView from './SickView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import {actions as rhythmActions,selectors as rhythmSelectors} from 'PeaceWithFood/src/store/rhythm';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
import {Images} from 'PeaceWithFood/src/components/icons';
import {NavigationActions} from 'react-navigation';
let routeName='Dashboard';
const localStyles = StyleSheet.create({
    button: {
        flex: 1,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

class SickContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: '',
        headerLeft: <TouchableOpacity
            style={localStyles.button}
            onPress={() => navigation.dispatch({type: "Navigation/BACK"})}
        >
            <Image
                source={Images.whiteBackIcon}
            />
        </TouchableOpacity>,
        headerRight:<TouchableOpacity style={{marginRight:10,height:40,width:40,justifyContent:'center',alignItems:'center'}} onPress={() =>
            navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName })
            ]
          }))}><Image source={Images.whiteCloseIcon}/></TouchableOpacity>,
        headerTintColor: 'white',
        headerStyle: {
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTitleStyle: styles.headerTitleStyle
    });

    state = {
          visibleModal: false,
          healthState:'Sick',
          user_id:null,
          value:null,
          healthStateId:null,
          isJournal:true,
    };

    componentWillMount(){
      AsyncStorage.getItem('@pwf/user').then((value) =>{
        if(value){
          user =JSON.parse(value)
          this.setState({user_id:user.id});
           console.log('user',user.id);
        }
      });
    }

    componentDidMount() {
      // console.log('navigation',this.props.navigation.state.params.healthState);
      this.setState({
        healthState:this.props.navigation.state.params.healthState,
        healthStateId:this.props.navigation.state.params.healthStateId
      });
      // const { params } = this.props.navigation.state;
    }

    componentWillReceiveProps(nextProps) {
         console.log('Rhythm Created',nextProps.addRhythmOptions);
    }

    onHomePress = () =>{
      //console.log(this.state.user_id,this.state.healthStateId);
      this.props.createRhythm(this.state.user_id,this.state.healthStateId);
      AsyncStorage.setItem('CheckedIn','yes');
      this.resetRoute("Dashboard");
    }

    onShareWithCoachPress = (thoughtType) =>{
        // console.log(".......", thoughtType);
        if(thoughtType =='journal'){
            this.setState({isJournal:true})
        }
        else{
            this.setState({isJournal:false})
        }
      this.setState({ visibleModal: true})
    }

    onModalClose = () =>{
      this.setState({ visibleModal: false})
    }

    onReminderPress = () =>{
      this.props.navigation.navigate('Reminder');
      this.setState({ visibleModal: false})
    }

    onAddCoachesPress = () =>{
      this.props.navigation.navigate('Coaches');
    }

    render() {

        return <SickView
            {...this.state}
            onHomePress={this.onHomePress}
            onAddCoachesPress={this.onAddCoachesPress}
            onShareWithCoachPress={this.onShareWithCoachPress}
            onModalClose={this.onModalClose}
            onReminderPress={this.onReminderPress}
            {...this.props}

        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
    addRhythmOptions: rhythmSelectors.addRhythmOptions(state)
});

export default connect(mapStateToProps, {
    ...actions,
    ...rhythmActions
})(SickContainer);
