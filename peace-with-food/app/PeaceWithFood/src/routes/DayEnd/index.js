import React from 'react';
import {Text,AsyncStorage} from 'react-native';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import DayEndView from './DayEndView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
import { Notifications, Permissions, Constants } from 'expo';
const deviceDate =new Date();


function pad( date ){

        if ( date >= 10 || date < 0 )
             return date;
        if ( !date && date !== 0 )
             return date;
        else
             return '0' + date;
}



class DayEndContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: '',
        headerLeft: <BackButton color='#2799bb' onPress={() => navigation.dispatch({type: "Navigation/BACK"})}/>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTitleStyle: styles.headerTitleStyle
    });

    state = {
          visibleModal: false,
    };

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }
    onHomePress = () =>{
      AsyncStorage.setItem('DeviceTime','sun');
      Notifications.cancelAllScheduledNotificationsAsync();
      let year    =  pad(  deviceDate.getFullYear()  );
      let month   =  pad(  deviceDate.getMonth() + 1 );
      let day     =  pad(  deviceDate.getDate()      );
      let NewDateFormat  =  year + "-" + month + "-" + day;
      this.props.navigation.navigate('DayEndGraphChart',{dayEnd:true,selectedStartDate:NewDateFormat,selectedEndDate:NewDateFormat});
      //this.resetRoute("Dashboard");

    }


    render() {

        return <DayEndView
            {...this.state}
            onHomePress={this.onHomePress}
            onAddCoachesPress={this.onAddCoachesPress}

        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
});

export default connect(mapStateToProps, actions)(DayEndContainer);
