import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    AsyncStorage

} from 'react-native';

import moment from 'moment';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {connect} from 'react-redux';
import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import {actions as rhythmActions,selectors as rhythmSelectors} from 'PeaceWithFood/src/store/rhythm';
import { LinearGradient } from 'expo';



function pad( date ){

        if ( date >= 10 || date < 0 )
             return date;
        if ( !date && date !== 0 )
             return date;
        else
             return '0' + date;
}


const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dce0e2'
    },
    section: {
        flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? 50 : 30),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        // height: 200,
        //paddingLeft:30,
        //paddingRight:30,
        //flex: 1,
        height: 370,
        marginTop:height/6,
        alignItems: 'center',
        justifyContent:'center',
        flexDirection:'column'
      },
      answer:{
        color:'#627078',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#a2a4a5',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: 'white',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
  checkPanel:{
    // alignItems: 'center',
    // justifyContent:'center',
    paddingTop:20,
    paddingLeft:20,
    // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    flexDirection:'row'
  },
  sharedBlock:{
    padding:40,

    marginTop:30,
    height:150,
    width:width/1.1,
    flexDirection:'row',
    flexWrap: "wrap"
  },
  timeText:{
    color:'#53acc7',
    fontSize:18,
    fontWeight:'500',
  },
  calendar: {
    borderTopWidth: 0,
    paddingTop: 5,
    borderBottomWidth: 0,
    borderColor: '#eee',
    //height: 250,removed for calender square adjustment https://cn.teamwork.com/#tasks/16703322
    width:300
  },

});



class ChartsView extends Component {

  constructor(props){
    super(props);
    this.state = {
      user_id:null,
      start_date:'',
      end_date:'',
      start_month:'',
      rhythmEntries:[],
      markedDates:{},
      dataFetched:false,


    }

  }


  componentWillMount(){
    let date    =  new Date();
    let year    =  date.getFullYear();
    let month   =  pad(  date.getMonth() + 1 );
    let endDay  =  pad(  new Date(year, month, 0).getDate() );
    let newStartDate  = year+'-'+ month +'-'+'01';
    let newEndDate    = year+'-'+ month +'-'+ endDay;
    let newStartMonth = year+'-'+ month ;

    this.setState({
      start_date:newStartDate,
      end_data:newEndDate,
      start_month:newStartMonth,
    });
    AsyncStorage.getItem('@pwf/user').then((value) =>{
      if(value){
        user =JSON.parse(value)
        this.setState({user_id:user.id});
        // console.log('user',this.state.user_id);
         this.props.getClientRhythm(this.state.user_id , this.state.start_date , this.state.end_date);
      }
    });


  }

  getUserRhythm(monthObj){
      monthObj.month  =  pad(  monthObj.month  );
      let lastday     =  pad(  new Date(monthObj.year, monthObj.month, 0).getDate() );
      let StartDate   =  monthObj.year+'-'+ monthObj.month +'-'+'01';
      let EndDate     =  monthObj.year+'-'+ monthObj.month +'-'+ lastday;
      let StartMonth  =  monthObj.year+'-'+ monthObj.month ;

      this.setState({
        start_date:StartDate,
        end_data:EndDate,
        start_month:StartMonth,
      });
      this.props.getClientRhythm(this.state.user_id , StartDate , EndDate);

  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.getRhythmByClient){
       this.setState({
         rhythmEntries:nextProps.getRhythmByClient,
         dataFetched:true,

       });
       let markDate={};
       if(nextProps.getRhythmByClient.length > 0){
            nextProps.getRhythmByClient.map((e,i) => {
              // console.log(e.created_date.slice(0,10),e.value);
              // color: (e.value > 3 && e.value < 6) ? '#f17d42' : '#125572',
              //color: (e.value > 3 && e.value < 7) ? '#125572' : '#f17d42',
              if ( !e.created_unix ){
                    //console.log("skipped missing created date:",e); // do not delete this line of code
                    return;
              }
               //::  NOTE  `created_unix`  :://
               var key = moment(e.created_unix*1000).format('YYYY-MM-DD');  // e.g., 2018-05-18
               markDate[key] = {
                 color: (e.value >= 1 && e.value <= 3) ? '#125572' : (e.value >= 4 && e.value <= 6) ? '#125572' : (e.value >= 7 && e.value <= 9) ? '#125572' : 'WHITE',
                 value:e.value
               };


             //console.log('markDate',markDate);

         });
      }

      // console.log(markDate);

       this.setState({markedDates:markDate});

     }
  }

  onDayAvgPress(day){
    day.month   =  pad( day.month );
    let endDay  =  pad( day.day ); // same day
    day.day     =  pad( day.day );
    let selectedStartDate = day.year+'-'+ day.month +'-'+ day.day;
    let selectedEndDate   = day.year+'-'+ day.month +'-'+ endDay;
    this.props.navigation.navigate('GraphChart',{dateObj:day,selectedStartDate:selectedStartDate,selectedEndDate:selectedEndDate});
  }

  onViewAvgPress(){
    this.props.navigation.navigate('MonthAvgChart',{selectedStartDate:this.state.start_month, selectedEndDate:this.state.start_month});
  }



  render (){
    let {

        onHomePress,
        onAddCoachesPress,
        onShareWithCoachPress,


    } = this.props;

     //console.log('dt',this.state.markedDates);
    return (<LinearGradient
        colors={['#EFF3F4', '#E0E4E7']} style={styles.container}>
        <StatusBar/>
        <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>

            <View style={[localStyle.section, localStyle.bottomSection]}>
              <View style={localStyle.sharePanel}>

              <Calendar
                onDayPress={(day) => {this.onDayAvgPress(day)}}
                style={localStyle.calendar}
                onMonthChange={(month) => {this.getUserRhythm(month)}}
                hideExtraDays
                displayLoadingIndicator
                markingType={'period'}
                theme={{
                    calendarBackground: '#d6d9db',
                    textSectionTitleColor: 'white',
                    dayTextColor: 'white',
                    todayTextColor: '#00bded',
                    //selectedDayTextColor: '#a6a7a8',
                    //monthTextColor: '#a6a7a8',
                    //selectedDayBackgroundColor: '#a6a7a8',
                    arrowColor: '#a6a7a8',
                    textDayFontSize: 20,
                    textMonthFontSize: 20,
                    textDayHeaderFontSize: 14,
                    // textDisabledColor: 'red',
                    'stylesheet.calendar.header': {
                      week: {
                        marginTop: 5,
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                      }
                    }
                }}
                markedDates={this.state.markedDates}
              />

              </View>
                <View style={localStyle.homeButtonPanel} >
                    <TextButton
                        label={"View month's avg."}
                        onPress={this.onViewAvgPress.bind(this)}
                        style={[localStyle.buttonStyle,{width:width/1.4}]}
                        loadingColor='#07c4f0'
                        primary
                        labelStyle={{fontSize:(Platform.OS === 'ios' ? 30 : 20),fontFamily:(Platform.OS === 'ios' ? 'Sarabun-Bold' : '')}}
                    />
                </View>

            </View>
        </KeyboardAwareScrollView>
    </LinearGradient>
    );
  }

}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
    getRhythmByClient: rhythmSelectors.getRhythmByClient(state)
});

export default connect(mapStateToProps, {
    ...rhythmActions
})(ChartsView);
