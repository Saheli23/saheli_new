import React from 'react';
import {Text,TouchableOpacity,Image,Platform,
Dimensions,View} from 'react-native';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import ChartsView from './ChartsView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
import {Images} from 'PeaceWithFood/src/components/icons';
import {NavigationActions} from 'react-navigation';
let routeName='Dashboard';
var {height, width} = Dimensions.get('window');
class ChartsContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: 'MY RHYTHMS',
        headerLeft: <View/>,
        headerRight:<TouchableOpacity style={{marginRight:10,height:40,width:40,justifyContent:'center',alignItems:'center'}} onPress={() =>
            navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName })
            ]
          }))}><Image source={Images.whiteCloseIcon}/></TouchableOpacity>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: '#4cc5e7',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTitleStyle:{
            fontSize: 17,
            color: Colors.WHITE,
            alignSelf: 'center',
            textAlign: 'center',
            ...Platform.select({
              android: { width: width/1.8, },
            }),
        }
    });

    state = {
          visibleModal: false,
    };

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }
    onHomePress = () =>{
      //this.resetRoute("Dashboard");
      this.props.navigation.navigate('Connect');
    }

    // onViewAvgPress = () =>{
    //   //this.resetRoute("Dashboard");
    //   console.log('GraphChart');
    //   this.props.navigation.navigate('MonthAvgChart');
    // }


    render() {

        return <ChartsView
            {...this.state}
            onHomePress={this.onHomePress}
            onAddCoachesPress={this.onAddCoachesPress}
            {...this.props}

        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
});

export default connect(mapStateToProps, actions)(ChartsContainer);
