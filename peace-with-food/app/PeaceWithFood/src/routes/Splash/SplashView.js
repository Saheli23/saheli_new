import React from 'react';
import {ActivityIndicator, View} from 'react-native';
import styles from 'PeaceWithFood/src/styles';

const SplashView = () => (
    <View style={[styles.container, styles.center]}>
        <ActivityIndicator/>
    </View>
);

export default SplashView;