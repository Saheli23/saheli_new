import React from 'react';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import SplashView from './SplashView';
import {connect} from 'react-redux';
import {actions as authActions, selectors as authSelectors} from 'PeaceWithFood/src/store/auth';
import {selectors as userSelectors} from 'PeaceWithFood/src/store/user';
import {NavigationActions} from 'react-navigation';

class SplashContainer extends NavComponent {
    static navigationOptions = {
        header: false
    };

    async componentDidMount() {
        const isLoggedIn = await this.props.loadSession();

        if (isLoggedIn) {
            this.resetRoute("Dashboard");
        } else {
            this.resetRoute("Login");
        }
    }

    render() {
        return <SplashView {...this.props}/>
    }
}

const mapStateToProps = (state) => ({
    isLoggedIn: authSelectors.getIsLoggedIn(state),
    isSettingUp: userSelectors.getIsSettingUp(state),
    isSetupComplete: userSelectors.getIsSetupComplete(state),
});

export default connect(mapStateToProps, {
    ...authActions
})(SplashContainer);
