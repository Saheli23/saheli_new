import React,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    TextInput,
    AsyncStorage,

} from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

//import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons'
import styles from 'PeaceWithFood/src/styles';
import CheckBox from 'react-native-check-box';
import {Images} from 'PeaceWithFood/src/components/icons';
var {height, width} = Dimensions.get('window');
import DateTimePicker from 'react-native-modal-datetime-picker';
// import TimePicker from 'react-native-simple-time-picker';
import ModalPicker from 'react-native-modal-picker';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/user';
import _ from 'underscore';
var MessageBarAlert = require('react-native-message-bar').MessageBar;
var MessageBarManager = require('react-native-message-bar').MessageBarManager;
import { LinearGradient } from 'expo';
// const MinCount =[];
// for (i=0;i<=720;i++){
//        MinCount.push ({key:i,label:i});
//      }


const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dce0e2'
    },
    section: {
        flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 110 : 60)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',

        // paddingRight: 32,

        // borderWidth: .5,
        // borderColor: '#000',
         height: 40,
        // borderRadius: 5 ,
         //margin: 10,
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,

        // paddingVertical: 10,
        // paddingHorizontal: 6,

        marginBottom:25
    },
    inputText: {
      color:'#2799bb',
      fontSize: 17,
      // paddingVertical: 10,
       paddingHorizontal: 6,
    },
    timerInputText: {
      color:'#319dbd',
      fontSize: 60,
      paddingVertical: 10,
      //paddingHorizontal: 6,
      textAlign:'center',
      width:350,
    },
    button:{
        backgroundColor:'#00bded',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#446a7a',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 20 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? height/18 : height/24),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        marginLeft:(Platform.OS === 'ios' ? width/7.5 : width/8)
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/2,
      },
    accountInfoForm: {
        paddingHorizontal: 25
    }
});
let tips=false;

class AccountView extends Component {

  constructor(props){
    super(props);
    this.state = {
      page: true,
      isDateTimePickerVisible: false,
      displayTime:'00:20',
      selectedHours: 0,
      selectedMinutes: 0,
      textInputValue:'',
      showTimePicker:false,
      showDisplayTime:true,
      email:'',
      fullname:'',
      userid:'',
      last_name:'',
      role:1,
    }
  }

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
    _showTimePicker = () => this.setState({ showTimePicker:true,showDisplayTime:false});

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
      this.setState({displayTime:date.toString().split(' ')[4]})
      console.log('A date has been picked: ', date.toString().split(' ')[4]);
      this._hideDateTimePicker();
    };
    componentWillMount() {
      AsyncStorage.getItem('CheckInTime').then((value) =>{
        if(value){
         this.setState({displayTime:value});
        }
      });
      AsyncStorage.getItem('@pwf/user').then((value) =>{
        if(value){
          user =JSON.parse(value);
          let lastname =_.has(user, 'last_name') ? user.last_name :'';
          let role     =_.has(user, 'role') ? user.role :1;
          this.setState({email:user.email,fullname:user.first_name,userid:user.id,last_name:lastname,role:role});
           console.log('user',user,);
        }
      });

    }

    componentDidMount() {
          // Register the alert located on this master page
          // This MessageBar will be accessible from the current (same) component, and from its child component
          // The MessageBar is then declared only once, in your main component.
          MessageBarManager.registerMessageBar(this.refs.alert);
        }

    componentWillUnmount() {
            // Remove the alert located on this master page from the manager
            MessageBarManager.unregisterMessageBar();
          }

    componentWillReceiveProps(nextProps) {
      if(nextProps.updateClient) {
            MessageBarManager.showAlert({
                title: 'Success',
                message: 'Your profile edited successfully',
                alertType: 'success',
                position: 'bottom',
                animationType: 'SlideFromRight',

            });
        console.log('nextProps',nextProps.updateClient);
        let user ={
          "email": this.state.email,
           "first_name": nextProps.updateClient.first_name,
           "id": this.state.userid,
           "last_name": this.state.last_name,
           "role": this.state.role,
        }
        AsyncStorage.setItem('@pwf/user',JSON.stringify(user))

      }
    }

    editUser =()=>{

    this.props.updateClientById(this.state.userid,this.state.fullname,this.state.email);
    }

    // onCheckValchange =(option)=>{
    //   this.setState({displayTime:(option.label).toString()});
    //   AsyncStorage.setItem('CheckInTime',(option.label).toString());
    // }
    onCheckValchange =(option)=>{
      if(option>720){
        Alert.alert("Please enter les than 720 minutes")
      }
      else{
      this.setState({displayTime:(option).toString()});
      AsyncStorage.setItem('CheckInTime',(option).toString());
       }

    }

  render (){
    const { selectedHours, selectedMinutes } = this.state;
      let {
          onHomePress
          } = this.props;
          return (<LinearGradient
              colors={['#EFF3F4', '#E0E4E7']} style={styles.container}>
                <StatusBar/>
                  <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>
                     <View style={[localStyle.section, localStyle.bottomSection]}>
                        <View style={localStyle.form}>
                            {/*<View style={{justifyContent:'center',alignItems:'center',marginBottom:(Platform.OS === 'ios' ? 40 : 20)}}>*/}
                                {/*<Text style={[localStyle.buttonText,{color:'#319dbd',fontWeight:'500',marginBottom:(Platform.OS === 'ios' ? 20 : 0)}]}>
                                    Change Check-in Timing
                                </Text> */}
                                {/*<TouchableOpacity  onPress={this._showDateTimePicker}>
                                     <Text style={[localStyle.buttonText,{color:'#319dbd',fontSize:65,}]}>{this.state.displayTime}</Text>
                                </TouchableOpacity>*/}
                                {/*<TextInput
                                      style={[localStyle.timerInputText]}
                                      keyboardType = 'numeric'
                                      placeholderTextColor="#319dbd"
                                      onChangeText={(displayTime) => this.onCheckValchange(displayTime)}
                                      value={this.state.displayTime}
                                      underlineColorAndroid='transparent'
                                    />*/}

                                  {/* Platform.OS === 'ios' && (
                                  <ModalPicker
                                      data={MinCount}
                                      style={{marginBottom:20,width:(width/1.5),borderWidth:0,borderColor:'transparent'}}
                                      selectStyle={{borderWidth:0}}
                                      selectTextStyle={[localStyle.buttonText,{color:'#319dbd',fontSize:65,}]}
                                      initValue={this.state.displayTime}
                                      onChange={(option)=>{this.onCheckValchange(option)}} >
                                  </ModalPicker>
                                ) */}
                                  {/* Platform.OS === 'android' && (

                                  <ModalPicker
                                      data={MinCount}
                                      style={{marginBottom:5,width:(width/2),borderWidth:0,borderColor:'transparent'}}
                                      selectStyle={{borderWidth:0}}
                                      selectTextStyle={[localStyle.buttonText,{color:'#319dbd',fontSize:65,}]}
                                      initValue={this.state.displayTime}
                                      onChange={(option)=>{this.onCheckValchange(option)}} >
                                        <View style={{justifyContent:'center',alignItems:'center'}}>
                                          <Text style={[localStyle.buttonText,{color:'#319dbd',fontSize:65,justifyContent:'center',alignItems:'center'}]}>{this.state.displayTime}</Text>
                                       </View>
                                  </ModalPicker>

                                  )*/}
                                {/*  <Text style={{marginTop:(Platform.OS === 'ios' ? 5 : 5),color:'#929393',fontSize:18,fontWeight:'500',justifyContent:'center',alignItems:'center'}}>(tap to change)</Text>
                              </View>*/}
                             <DateTimePicker
                               mode='time'
                               titleIOS='Pick a time'
                               isVisible={false}
                               onConfirm={this._handleDatePicked}
                               onCancel={this._hideDateTimePicker}
                               minuteInterval={10}

                             />
                            {/* <Text>{selectedHours}:{selectedMinutes}</Text>
                             <TimePicker
                              selectedHours={selectedHours}
                              selectedMinutes={selectedMinutes}
                              onChange={(hours, minutes) => this.setState({ selectedHours: hours, selectedMinutes: minutes })}
                            /> */}
                              <View style={{marginTop:height/8,marginBottom:10,justifyContent:'center',alignItems:'center',}}>
                                <Text style={[localStyle.buttonText,{color:'#319dbd',fontWeight:'500',}]}>
                                    Change Account Information
                                </Text>
                              </View>
                            <View style={localStyle.accountInfoForm}>
                                <View style={localStyle.row}>
                                    <TextInput
                                        onChangeText={ (text) => this.setState({ fullname: text }) }
                                        numberOfLines={1}
                                        style={[localStyle.inputText,{flex:1}]}
                                        placeholder="Full Name"
                                        placeholderTextColor="#446a7a"
                                        underlineColorAndroid='transparent'
                                        value={this.state.fullname}

                                    />
                                    <Image style={{padding: 10,margin: 5,//height: 25,//width: 25,//resizeMode : 'stretch',
                                                  alignItems: 'center'}} source={Images.rightArrowIcon} />
                                  </View>
                                  <View style={localStyle.row}>
                                      <TextInput
                                          editable = {false}
                                          numberOfLines={1}
                                          style={[localStyle.inputText,{flex:1}]}
                                          placeholder="email"
                                          placeholderTextColor="#446a7a"
                                          underlineColorAndroid='transparent'
                                          value={this.state.email}

                                      />
                                    {/*  <Image style={{padding: 10,margin: 5,//height: 25,//width: 25,//resizeMode : 'stretch',
                                                    alignItems: 'center'}} source={Images.rightArrowIcon} />*/}
                                    </View>
                                  <View style={localStyle.row}>
                                      <TextInput
                                          fieldName="password"
                                          numberOfLines={1}
                                          style={[localStyle.inputText,{flex:1}]}
                                          placeholder="password"
                                          placeholderTextColor="#446a7a"
                                          secureTextEntry
                                          underlineColorAndroid='transparent'
                                      />
                                      <Image style={{padding: 10,margin: 5,//height: 25,//width: 25,//resizeMode : 'stretch',
                                                    alignItems: 'center'}} source={Images.rightArrowIcon} />
                                  </View>
                            </View>
                            <View style={[localStyle.tipsPanel,{width:width/1.5,marginLeft:width/15,}]}>
                                {/*<View style={{flexDirection:'column'}}>
                                  <Text style={localStyle.buttonText}>
                                      Get health tips and updates
                                  </Text>
                                  <Text style={localStyle.buttonText}>
                                   from Peace With Food.
                                  </Text>
                                </View>*/}
                              </View>
                              <View style={[localStyle.tipsPanel,{paddingTop:0,paddingBottom:10,width:width/1.3,marginLeft:width/15,}]}>
                                {/*<View style={{flexDirection:'column'}}>
                                  <Text style={localStyle.buttonText}>
                                      Allow instant messaging
                                  </Text>
                                </View>*/}
                              </View>
                              <View style={localStyle.homeButtonPanel}>
                                  <TextButton
                                      label={"Save"}
                                      onPress={this.editUser.bind(this)}
                                      style={localStyle.buttonStyle}
                                      loadingColor='#07c4f0'
                                      primary
                                  />
                              </View>
                          </View>
                      </View>
                  </KeyboardAwareScrollView>
                  <MessageBarAlert ref="alert" />
              </LinearGradient>
    );
  }
}
const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
    updateClient: selectors.updateClient(state)
});

export default connect(mapStateToProps, {
    ...actions
})(AccountView);
