import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    AsyncStorage

} from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';
import {connect} from 'react-redux';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';
import _ from 'underscore';
import { LinearGradient } from 'expo';
import {actions as connectActions, selectors as connectSelectors} from 'PeaceWithFood/src/store/pwfconnect';
import {actions as rhythmActions,selectors as rhythmSelectors} from 'PeaceWithFood/src/store/rhythm';
import {NavigationActions} from 'react-navigation';
import RadioButton from 'react-native-radio-button';

const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dce0e2'
    },
    section: {
        flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#ffffff',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? height/8.5 : height/12),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,
        marginTop:height/6,
        alignItems: 'center',
        justifyContent:'center',
        flexDirection:'column'
      },
      answer:{
        color:'#627078',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#00bded',
        marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: 'white',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
  checkPanel:{
    // alignItems: 'center',
    // justifyContent:'center',
    paddingTop:20,
    paddingLeft:20,
    // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    flexDirection:'row'
  },
  shareWithCoach:{
    flexDirection:'row',
    backgroundColor:'#ffffff',
    width:300,
    height:40,
    padding:10,
    marginTop:50,
    justifyContent:'space-between'
  },
  shareContainer:{
    marginTop:(Platform.OS === 'ios' ? 150 : 70),
    backgroundColor:'#eff3f4',
    flexDirection:'row',
    height:100,
    justifyContent:'space-around',
    alignItems:'center',
    width:width
  },
  borderSeparetor:{
    borderTopColor:'#4facc7',
    borderTopWidth:2,
    width:width/1.4,
    marginLeft:15,
    marginRight:20,
  }

});

let localMessageCounter=0;
let newMessage = false;
let localMessageCounterForOthers=0;
let newMessageForOthers = false;


class ConnectView extends Component {

  constructor(props){
    super(props);
    this.state = {
        page: true,
        coaches: [],
        selectedCoaches: {},
        selectedTimes:{},
        sharedInstances:[],
        sharedInstancesForOthers:[],
        diffBetweenMessages:0,
        diffBetweenMessagesForOthers:0,
    };
  }

  changePage(p){
     this.setState({page:p});
  }

  componentWillMount(){
        AsyncStorage.getItem('@pwf/user').then((value) =>{
            if(value){
              user =JSON.parse(value)
              this.setState({user_id:user.id});
              //
              // Populate shared rhythm data
              const promise = this.props.getMyCoaches( user.id );
              const self    = this;
              promise.then(function(coaches){
                self.setState({'coaches':coaches});
              });
            }
        });
  }

  componentDidMount(){
    AsyncStorage.getItem('@pwf/user').then((value) =>{
      if(value){
        user =JSON.parse(value)
     // Populate shared rhythm data
        let self    = this;
        const sharePromise = this.props.getSharedRhythmInstances( user.id );
        const shareOthersPromise = this.props.getOthersSharedRhythmInstances( user.id );
        sharePromise.then(function(instances){
          self.setState({sharedInstances:instances});
          localMessageCounter = instances.length;
          //console.log("New...",newMessage,"....DIFF....",self.state.diffBetweenMessages,"....Local.....",localMessageCounter);
          AsyncStorage.getItem('ShareCounter').then((val) =>{
          //  console.log("HJJHJJK.....",val);
            if(val){
              if(localMessageCounter > Number(val)){
                let diff = (localMessageCounter - Number(val));
                self.setState({diffBetweenMessages: diff});
                newMessage = true;
                //console.log("New...",newMessage,"....DIFF....",self.state.diffBetweenMessages,"....Local.....",localMessageCounter,"....Val....",val);
                AsyncStorage.setItem('ShareCounter', localMessageCounter.toString());
              }
            }
            if(val==null){
              //console.log("New...",newMessage,"....DIFF....",self.state.diffBetweenMessages,"....Local.....",localMessageCounter,"....Val....",val);
              AsyncStorage.setItem('ShareCounter', localMessageCounter.toString());
            }
          })
          // console.log("ASDFGT....", instances.length);
        });

        shareOthersPromise.then(function(instances){
          self.setState({sharedInstancesForOthers:instances});
          localMessageCounterForOthers = instances.length;
          //console.log("New11...",newMessage,"....DIFF11....",self.state.diffBetweenMessagesForOthers,"....Local11.....",localMessageCounterForOthers);
          AsyncStorage.getItem('ShareCounterForOthers').then((val) =>{
            //console.log("HJJHJJK11.....",val);
            if(val){
              if(localMessageCounterForOthers > Number(val)){
                let diff = (localMessageCounterForOthers - Number(val));
                self.setState({diffBetweenMessagesForOthers: diff});
                newMessageForOthers = true;
                //console.log("New11...",newMessageForOthers,"....DIFF11....",self.state.diffBetweenMessagesForOthers,"....Local11.....",localMessageCounterForOthers,"....Val111....",val);
                AsyncStorage.setItem('ShareCounterForOthers', localMessageCounterForOthers.toString());
              }
            }
            if(val==null){
              //console.log("New11...",newMessageForOthers,"....DIFF1.1...",self.state.diffBetweenMessagesForOthers,"....Local1.1....",localMessageCounterForOthers,"....Val1.1...",val);
              AsyncStorage.setItem('ShareCounterForOthers', localMessageCounterForOthers.toString());
            }
          })
          // console.log("ASDFGT....", instances.length);
        });
      }
    });
  }

  componentWillReceiveProps(nextProps) {
  }


  setOption_time( time_key, number_of_days ){
    console.log('TIME...',this.state.selectedTimes[time_key]);
    this.setState({selectedTimes: time_key });
        // if ( this.state.selectedTimes[time_key] ){
        //      delete this.state.selectedTimes[time_key];
        // } else {
        //      this.state.selectedTimes[time_key]  = number_of_days || true;
        //      // force update
        //      this.setState({selectedTimes: this.state.selectedTimes });
        // }
        // console.log( 'time_key', time_key, number_of_days ,this.state.selectedTimes);
  }

  setOption_coach( coach_id ){
        const key = ""+coach_id;
        if ( this.state.selectedCoaches[key] ){
             delete this.state.selectedCoaches[key];
        } else {
             this.state.selectedCoaches[key] = true;
             // force update
             this.setState({selectedCoaches: this.state.selectedCoaches });
        }
        console.log( 'coach_id', coach_id );
  }

  _renderModalContent = (onModalClose,onShareCoachesPress,onAddCoachesPress) => {
    function titleCase( string ){
        if ( ! string )
               return string;
        string = string + "";
        string = string.trim();
        string = (string.charAt(0).toUpperCase() + string.slice(1));
        return string;
    }
    var self = this;

    var blocks = _.map( this.state.coaches, function( coach, count ){
        var coach_name =    titleCase( coach.full_name||'' )
                         || titleCase( coach.first_name||'' )
                            + " " + titleCase( coach.last_name||'' );
            coach_name = coach_name.trim()
            coachKey   = ""+coach.id
            user_id    = self.state.user_id;


        return (
            <View
                   style={localStyle.checkPanel}
                   key={count}>
                <CheckBox
                      style={{alignItems:'center',marginRight:10,flex:1}}
                      isChecked={self.state.selectedCoaches[coachKey]}
                      checkBoxColor='#e0e4e7'
                      onClick={self.setOption_coach.bind(self,coach.id)}
                      rightText={coach_name }
                      rightTextStyle={{color: '#7ec2d6',fontSize: 18}}
                  />
                </View>
        );
    });

    if ( ! blocks || blocks.length === 0 || !blocks[0] ){

        blocks = null;
    }

    return (
      <View style={localStyle.modalContent}>
        { this.state.page ? (
        <View>
           <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
            <TouchableOpacity onPress={onModalClose}>
              <Image source={Images.closeIcon} />
            </TouchableOpacity>
          </View>
        <Text style={[localStyle.iconText,{color:'#446a7a'}]}>Share your rhythms/thoughts with friends:</Text>
        <View  style={localStyle.checkPanel}>
          <CheckBox
                style={{alignItems:'center',marginRight:10,flex: 1,}}
                onClick={this.setOption_time.bind(this,'TODAY')}
                isChecked={this.state.selectedTimes['TODAY']}
                checkBoxColor='#e0e4e7'
                rightText='Today so far'
                rightTextStyle={{color: '#7ec2d6',fontSize: 18}}
            />
          {/* <RadioButton
          size={10}
          animation={'bounceIn'}
          isSelected={this.state.selectedTimes=='TODAY' ? true : false }
          onPress={this.setOption_time.bind(this,'TODAY')}
          /><Text onPress={this.setOption_time.bind(this,'TODAY')} style={[{color: '#7ec2d6',fontSize: 18}]}>Today so far
          </Text> */}

        </View>
        <View  style={localStyle.checkPanel}>
          {/*<CheckBox
                style={{alignItems:'center',marginRight:10,flex: 1,}}
                onClick={this.setOption_time.bind(this,'YESTERDAY')}
                isChecked={this.state.selectedTimes['YESTERDAY']}
                checkBoxColor='#e0e4e7'
                rightText='Yesterday'
                rightTextStyle={{color: '#7ec2d6',fontSize: 18}}
            />*/}
            <RadioButton
            size={10}
            animation={'bounceIn'}
            isSelected={this.state.selectedTimes=='YESTERDAY' ? true : false}
            onPress={this.setOption_time.bind(this,'YESTERDAY')}
            /><Text onPress={this.setOption_time.bind(this,'YESTERDAY')} style={[{color: '#7ec2d6',fontSize: 18}]}>Yesterday
            </Text>
          </View>
          {/*<View  style={localStyle.checkPanel}>
            <CheckBox
                  style={{alignItems:'center',marginRight:10,flex: 1,}}
                  onClick={this.setOption_time.bind(this,'DAYS_3',3)}
                  isChecked={this.state.selectedTimes['DAYS_3']}
                  checkBoxColor='#e0e4e7'
                  rightText='Last 3 Days'
                  rightTextStyle={{color: '#7ec2d6',fontSize: 18}}

              />
            </View>
            <View  style={localStyle.checkPanel}>
              <CheckBox
                    style={{alignItems:'center',marginRight:10,flex: 1,}}
                    onClick={this.setOption_time.bind(this,'DAYS_5',5)}
                    isChecked={this.state.selectedTimes['DAYS_5']}
                    checkBoxColor='#e0e4e7'
                    rightText='Last 5 Days'
                    rightTextStyle={{color: '#7ec2d6',fontSize: 18}}

                />
              </View>*/}
              <View  style={localStyle.checkPanel}>
                {/*<CheckBox
                      style={{alignItems:'center',marginRight:10,flex: 1,}}
                      onClick={this.setOption_time.bind(this,'LAST_WEEK')}
                      isChecked={this.state.selectedTimes['LAST_WEEK']}
                      checkBoxColor='#e0e4e7'
                      rightText='Past 7 days'
                      rightTextStyle={{color: '#7ec2d6',fontSize: 18}}

                  />*/}
                  <RadioButton
                  size={10}
                  animation={'bounceIn'}
                  isSelected={this.state.selectedTimes=='LAST_WEEK' ? true : false}
                  onPress={this.setOption_time.bind(this,'LAST_WEEK')}
                  /><Text onPress={this.setOption_time.bind(this,'LAST_WEEK')} style={[{color: '#7ec2d6',fontSize: 18}]}>Past 7 days
                  </Text>
                </View>
                {/*
                <View  style={localStyle.checkPanel}>
                  <CheckBox
                        style={{alignItems:'center',marginRight:10}}
                        onClick={this.setOption_time.bind(this,'MONTH_AVERAGE')}
                        isChecked={this.state.selectedTimes['MONTH_AVERAGE']}
                        checkBoxColor='#e0e4e7'

                    />
                    <Text style={localStyle.buttonText}>
                        Month average
                    </Text>
                  </View>
                  */}
                  <View style={[localStyle.homeButtonPanel,{paddingLeft:0}]}>
                    <TextButton
                        label={"Next"}
                        style={[localStyle.buttonStyle,{width:width/3}]}
                        onPress={this.changePage.bind(this,false)}
                        loadingColor='#07c4f0'
                        primary
                        labelStyle={{fontSize:24}}
                    />
                  </View>
                  <TouchableOpacity style={{flexDirection:'row'}}  onPress={this.changePage.bind(this,false)}>
                    <Image style={{marginTop:2}}source={Images.rightArrowIcon} />
                    <Text style={[localStyle.iconText,{color:'#37a0c0',marginTop:0,marginLeft:5}]}>1 of 2</Text>
                  </TouchableOpacity>
              </View>
            ) : (
              <View>
                <View style={{borderBottomColor:'#bdc0c1',borderBottomWidth:1,}}>
                 <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <Text style={[localStyle.iconText,{marginTop:0}]}>Share with friends</Text>
                    <TouchableOpacity onPress={onModalClose}>
                        <Image source={Images.closeIcon} />
                    </TouchableOpacity>
                  </View>
                  <Text style={[localStyle.iconText,{color:'#446a7a',marginBottom:3}]}>
                  Share with whom?</Text>
                </View>
            <ScrollView  style={{paddingBottom:100}}>

                { blocks || (

                <View style={{marginTop:50}}>
                    <TouchableOpacity style={localStyle.shareBox} onPress={() => {onAddCoachesPress.apply(this,arguments);onModalClose.apply(this,arguments);}}>
                      <Image
                        source={Images.coachIcon}
                      />
                    </TouchableOpacity>
                </View>


                )}

            </ScrollView>

                { blocks && (

                <View>

                        <View style={localStyle.borderSeparetor}></View>

                        <View style={[localStyle.homeButtonPanel,{paddingLeft:0,marginTop:10,marginBottom:30}]}>
                          <TextButton
                              label={"SHARE"}
                              style={[localStyle.buttonStyle,{width:width/2.3}]}
                              onPress={onShareCoachesPress.bind(
                                this,
                                this.state.user_id,
                                this.state.selectedTimes,
                                this.state.selectedCoaches,
                                onModalClose
                              )}
                              loadingColor='#07c4f0'
                              primary
                              labelStyle={{fontSize:18}}
                          />
                        </View>
                       <TouchableOpacity style={{flexDirection:'row'}} onPress={this.changePage.bind(this,true)}>
                         <Image source={Images.smallBackIcon} />
                         <Text style={[localStyle.iconText,{color:'#37a0c0',marginTop:0,marginLeft:5}]}>2 of 2</Text>
                       </TouchableOpacity>

                </View>

                )}

            </View>
          )}

      </View>
    )
  };


  render (){
    let {

        onHomePress,
        onAddCoachesPress,
        onShareWithCoachPress,
        visibleModal,
        onModalClose,
        onShareCoachesPress,
        onShareMePress,
        onConversationPress
    } = this.props;

    const onModalClose_original = onModalClose;
    const self = this;
    function onModalClose_withPage(){
      // restore the modal to normal
      self.setState({page:true});
      onModalClose_original.apply(this,arguments);
    }
    onModalClose = onModalClose_withPage;


    return (<LinearGradient
        colors={['#EFF3F4', '#E0E4E7']} style={styles.container}>
        <StatusBar/>
        <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>
            <Modal isVisible={visibleModal}>
             {this._renderModalContent(onModalClose,onShareCoachesPress,onAddCoachesPress)}
           </Modal>
            <View style={[localStyle.section, localStyle.bottomSection]}>
              <View style={localStyle.sharePanel}>
                    <Image
                      source={Images.connectIcon}
                    />
                    <TouchableOpacity onPress={onShareWithCoachPress} style={localStyle.shareWithCoach} >
                        <Text style={localStyle.answer}>connect with my friends...</Text>
                        <Image
                          source={Images.frontArrowIcon}
                        />
                    </TouchableOpacity>
                    <View style={localStyle.shareContainer}>
                        <TouchableOpacity style={localStyle.shareBox} onPress={onAddCoachesPress}>
                          <Image
                            source={Images.coachIcon}
                          />
                          <Text style={localStyle.iconText}>Friends</Text>
                        </TouchableOpacity>

                        {/*<TouchableOpacity onPress={onConversationPress} style={localStyle.shareBox}>
                            <Image
                              source={Images.conversationIcon}
                            />
                            <Text style={[localStyle.iconText,{marginTop:18}]}>Sent</Text>
                          </TouchableOpacity>*/}
                          <TouchableOpacity onPress={()=>{newMessageForOthers =false;this.props.navigation.navigate('SharedWithOthers')}} style={localStyle.shareBox}>                            
                              <Image
                              source={Images.conversationIcon}
                              />
                              {(!newMessageForOthers) ? (<View >
                                </View>):(<View style={{position:'absolute', top:-15, right:-18, height:15, width:15, backgroundColor:'orange', borderRadius:50,margin:10,alignItems:'center'}}>
                                {/* <Text style={{marginTop:8,height:15, width:15,borderRadius:50,textAlign:'center'}}>
                                  {this.state.diffBetweenMessagesForOthers}
                                </Text> */}
                                </View>)}
                              <Text style={[localStyle.iconText,{marginTop:18}]}>Sent</Text>
                            </TouchableOpacity>

                          <TouchableOpacity onPress={()=>{newMessage =false;this.props.navigation.navigate('SharedWithMe')}} style={localStyle.shareBox}>
                              <Image
                              source={Images.shared_icon}
                              />
                              {(!newMessage) ? (<View>
                                  </View>):(<View style={{position:'absolute', top:-15, right:-16, height:15, width:15, backgroundColor:'orange', borderRadius:50,margin:10,alignItems:'center'}}>
                                  {/* <Text style={{marginTop:8,height:15, width:15,borderRadius:50,textAlign:'center'}}>
                                    {this.state.diffBetweenMessages}
                                  </Text> */}
                                  </View>)}
                              <Text style={localStyle.iconText}>Inbox</Text>
                          </TouchableOpacity>
                            {/*<TouchableOpacity onPress={onShareMePress} style={localStyle.shareBox}>
                                <Image
                                  source={Images.shared_icon}
                                />
                                <Text style={localStyle.iconText}>Shared</Text>
                             </TouchableOpacity>*/}
                        </View>

                </View>
                <View style={localStyle.homeButtonPanel}>
                    <HomeButton
                        onPress={onHomePress}
                        style={localStyle.buttonStyle}
                        loadingColor='#07c4f0'
                        primary
                    />
                </View>

            </View>
        </KeyboardAwareScrollView>
    </LinearGradient>
    );
  }

}


const mapStateToProps = (state, props) => ({
});


export default connect(mapStateToProps, {
    ...connectActions,
    ...rhythmActions

})(ConnectView);
