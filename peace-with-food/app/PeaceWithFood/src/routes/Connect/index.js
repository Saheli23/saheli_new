import React from 'react';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';
import {Text,TouchableOpacity,Image,View,Platform,
Dimensions} from 'react-native';
import _ from 'underscore';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import ConnectView from './ConnectView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
import {Images} from 'PeaceWithFood/src/components/icons';
import {NavigationActions} from 'react-navigation';
import {actions as rhythmActions,selectors as rhythmSelectors} from 'PeaceWithFood/src/store/rhythm';
let routeName='Dashboard';
var {height, width} = Dimensions.get('window');





const rules = {
    code: 'required|number',
};

const validate = values => {
    const validator = new Validator(values, rules);
    validator.passes();
    return validator.errors.all();
};


const ConnectViewWithForm = reduxForm({
    form: 'connectCoach',
    fields: ['code'],
    validate
})(ConnectView);




class ConnectContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: <Text style={{color:'#53acc7'}}>PWF CONNECT</Text>,
        headerLeft: <View/>,
        headerRight:<TouchableOpacity style={{marginRight:10,height:40,width:40,justifyContent:'center',alignItems:'center'}} onPress={() =>
                navigation.dispatch(NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName })
                ]
              }))}><Image source={Images.closeIcon}/></TouchableOpacity>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTitleStyle: {
            fontSize: 17,
            color: Colors.WHITE,
            alignSelf: 'center',
            textAlign:'center',
            ...Platform.select({
              android: { width: width/1.8, },
            }),

        }
    });

    state = {
          visibleModal: false,
    };

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }
    onHomePress = () =>{
      this.resetRoute("Dashboard");
    }
    onAddCoachesPress = () =>{
      this.props.navigation.navigate('Coaches');
    }

    onShareWithCoachPress = () =>{
      this.setState({ visibleModal: true})
    }
    onModalClose = () =>{
      this.setState({ visibleModal: false})
    }



    onShareCoachesPress = (user_id, time_periods, coach_ids, userNote,onModalClose) =>{
      this.onModalClose();
      var responses = []
      var coach_count = 0;
      var time_count  = 0;
      let new_time_periods ={};
      if(time_periods)
      {
        new_time_periods ={[time_periods]:true}
      }
      _.each( new_time_periods, (number_of_days, time_key) => {
          _.each( coach_ids, (x, coach_id) => {
              var m = time_key.match(/DAYS_([0-9]+)/);
              if ( m ){
                   time_key = 'DAYS';
                   number_of_days = m[1];
              }
              var response = this.props.createSharedRhythms(
                           user_id,
                           time_key,
                           number_of_days,
                           coach_id
                         );
              responses.push(response)
              coach_count++;
         });
         time_count++;

      });
      if ( time_count==0 ){
           Alert.alert('Please select a time period');
           return;
      }
      if ( coach_count==0 ){
           Alert.alert('Please select a friend');
           return;
      }


      const promise = Promise.all(responses);
      const c = coach_count==1? "coach":"coaches";
      var self = this;

      promise.then(function(){

          self.setState({ visibleModal: false})

          setTimeout(function(){
            Alert.alert('Thank you',
                        "Your data will be sent shortly.");
          },500);

      },function(){

          setTimeout(function(){
            Alert.alert('Error',
                        "Oops, something went wrong. Sometimes "+
                        "this error message appears when you "+
                        "share the same data twice. If you haven't "+
                        "already gone through this process, " +
                        "please let us know what issues you are having.");
          },500);

      });
      promise.finally(function(){
          self.setState({ loading: false})
      });
    }



    onShareMePress = () =>{
      this.props.navigation.navigate('SharedWithMe');
    }

    onConversationPress = () =>{
        this.props.navigation.navigate('SharedWithOthers');
      //this.props.navigation.navigate('Conversation');
      // this.props.navigation.navigate('MessageHistory');
    }

    render() {

        return <ConnectViewWithForm
            {...this.state}
            onHomePress={this.onHomePress}
            onAddCoachesPress={this.onAddCoachesPress}
            onShareWithCoachPress={this.onShareWithCoachPress}
            onModalClose={this.onModalClose}
            onShareCoachesPress={this.onShareCoachesPress}
            onShareMePress={this.onShareMePress}
            onConversationPress={this.onConversationPress}
            {...this.props}
        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
});

export default connect(mapStateToProps, {
    ...rhythmActions
})(ConnectContainer);
