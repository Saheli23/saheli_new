import React from 'react';
import {Text,Dimensions,Platform} from 'react-native';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import MealView from './MealView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
var {height, width} = Dimensions.get('window');

class MealContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: <Text style={{color:'#38a0bf',fontWeight:'normal'}}>ENJOYING A MEAL OR SNACK</Text>,
        headerLeft: <BackButton color='#2799bb' backgroundColor='yellow' onPress={() => navigation.dispatch({type: "Navigation/BACK"})}/>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0,
            width:width
        },
        headerTitleStyle: {width:width/1.3,left:3}
    });

    state = {
          visibleModal: false,
          healthState:'Neutral'
    };

    componentWillMount() {
         this.setState({healthState:this.props.navigation.state.params.healthState});
    }

    componentWillReceiveProps(nextProps) {

    }
    onHomePress = () =>{
      this.resetRoute("Dashboard");
    }

    onShareWithCoachPress = () =>{
      this.setState({ visibleModal: true})
    }

    onModalClose = () =>{
      this.setState({ visibleModal: false})
    }

    onFinishPress = () =>{
      //this.props.navigation.navigate('AfterEatCheckIn');
      this.props.navigation.navigate('Rhythm');
    }


    render() {

        return <MealView
            {...this.state}
            onHomePress={this.onHomePress}
            onAddCoachesPress={this.onAddCoachesPress}
            onShareWithCoachPress={this.onShareWithCoachPress}
            onModalClose={this.onModalClose}
            onFinishPress={this.onFinishPress}

        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
});

export default connect(mapStateToProps, actions)(MealContainer);
