import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    TextInput,
    Animated,
    Easing

} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

//import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';

const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dce0e2'
    },
    section: {
        flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:80,
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        //backgroundColor:'#4cc5e7',
        backgroundColor:'#2799bb',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,
        marginTop:height/4,
        alignItems: 'center',
        justifyContent:'center',
        flexDirection:'column'
      },
      answer:{
        flex:0.9,
        color:'#627078',
        fontSize:18,
        fontWeight:'500'
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#00bded',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: '#ebf6f9',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
      checkPanel:{
        // alignItems: 'center',
        // justifyContent:'center',
        paddingTop:20,
        paddingLeft:20,
        // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
        flexDirection:'row'
      },
      sharedBlock:{
        padding:40,
        alignItems: 'center',
        justifyContent:'center',
        marginTop:30,
        height:150,
        width:width/1.1,
        flexDirection:'row',
        flexWrap: "wrap"
      },
      timeText:{
        //color:'#0ecbf3',
        color:'#38a0bf',
        fontSize:39,
        fontWeight: (Platform.OS === 'ios' ? "600" : "500"),
        textAlign: 'center',

      },
      shareWithCoach:{
        flexDirection:'row',
        backgroundColor:'#d4d8db',
        width:300,
        height:40,
        padding:10,
        //marginTop:10,
        justifyContent:'space-between'
      },

});

const FADE_IN_TIME    =  2000;//1750;
const FADE_OUT_TIME   =  1000;//1750;
const TOTAL_SHOW_TIME =  6000;


class MealView extends Component {

  constructor(props){
    super(props);
    this.state = {
        animating: false,
        feeling:''
    }
    this.intervalVar = null;
    this.animated = new Animated.Value(0.0);
  }

  animateOut( onFinish ){
    Animated.timing(this.animated, {
        toValue:    1.0,
        duration:   FADE_OUT_TIME,
        easing:     Easing.linear //Easing.bezier(0.05,0.80,1,1)
    }).start( onFinish );
  }

  animateIn( onFinish ){
    Animated.timing(this.animated, {
        //toValue:  0.7,
        toValue:    0.0,
        duration:   FADE_IN_TIME,
        easing:     Easing.linear //Easing.bezier(0.80,0.05,1,1)
    }).start( onFinish );
  }


  componentWillMount() {
      // this.setState({healthState1:this.props.navigation.state.params.healthState});
  }

  componentWillUnmount(){
    console.log(this.intervalVar);
    clearInterval(this.intervalVar);
  }

  componentDidMount(){
    let healthStateArray=[['Famished', 'Starving', 'Very Hungry'],[ 'Ready', 'Neutral', 'Full & Satisfied'],[ 'Overly Full', 'Stuffed', 'Sick']];
    let feelings = [
        [
            'Breathe deep',
            'Go extra slow',
            'Feel the food hitting your tummy',
            'Relax',
            'Be mindful'
        ],
        [
            'Taste',
            'Go slowly',
            'Remember: food is a gift',
            'Be present'
        ],
        [
            'Can you allow time to pass?',
            'The food will taste so good when you are ready',
            'What are you really hungry for?'
        ]
    ];

    //console.log(this.props);
    let getChangeVal = feelings[healthStateArray.map((e)=>{
                   return e.indexOf(this.props.healthState) > -1;
                }).indexOf(true)];

    let i             = 0;
    let is_running    = false;
    let is_first_run  = true;
    let increment     = () => {
                           i++;
                           if ( i >= getChangeVal.length )
                                i -= getChangeVal.length;
                        };
    let duration      = TOTAL_SHOW_TIME - FADE_IN_TIME - FADE_OUT_TIME;

    let method = ()=>{

       if ( is_running )
            return;
       is_running = true;
       var self = this;
       var feeling = getChangeVal[i];
       feeling = feeling.replace( / ([^ ]+)$/, " $1" ); // prevent widow line

       if ( is_first_run ){
         self.animated = new Animated.Value(1.0);
         self.setState({  feeling:feeling,  animating:true  });
         increment(i);
         feeling = getChangeVal[i]; // prevent duplicate messages
         self.animateIn( () => {
                setTimeout(() =>
                    self.animateOut( () => {
                        is_running = false;
                        is_running = false;
                        increment(i);
                        self.setState({  feeling: feeling
                                      ,  animating: false  });

                    })
                  , duration
                );

         });
       } else
       {
         self.animated = new Animated.Value(1.0);
         self.setState(
           () => { return {animating: true } },
           () => {
               self.animateIn( () => {
                  setTimeout(() =>
                    self.animateOut( () => {
                        is_running = false;
                        increment(i);
                        self.setState({  feeling: feeling
                                      ,  animating: false  });

                    })
                  , TOTAL_SHOW_TIME - FADE_IN_TIME - FADE_OUT_TIME );

               });
           }
         );
       }
       is_first_run = false;

    }

    let self = this;
    setTimeout(() => {
      self.intervalVar = setInterval(method, 333 );
      method();
    }, 500 );
  }

  _renderModalContent = (onModalClose) => (
      <View style={localStyle.modalContent}>
        <View>
         <View style={{flexDirection:'row',justifyContent:'flex-end'}}>

          <TouchableOpacity onPress={onModalClose}>
            <Image source={Images.closeIcon} />
          </TouchableOpacity>
        </View>

        <View  style={localStyle.checkPanel}>
          <CheckBox
                style={{alignItems:'center',marginRight:10}}
                onClick={()=>console.log('checked')}
                isChecked={false}
                checkBoxColor='#e0e4e7'

            />
            <Text style={localStyle.buttonText}>
                Reason 1
            </Text>
          </View>
          <View  style={localStyle.checkPanel}>
            <CheckBox
                  style={{alignItems:'center',marginRight:10}}
                  onClick={()=>console.log('checked')}
                  isChecked={false}
                  checkBoxColor='#e0e4e7'

              />
              <Text style={localStyle.buttonText}>
                  Reason 2
              </Text>
            </View>
            <View  style={localStyle.checkPanel}>
              <CheckBox
                    style={{alignItems:'center',marginRight:10}}
                    onClick={()=>console.log('checked')}
                    isChecked={false}
                    checkBoxColor='#e0e4e7'

                />
                <Text style={localStyle.buttonText}>
                    Reason 3
                </Text>
              </View>
              <View  style={localStyle.checkPanel}>
                <CheckBox
                      style={{alignItems:'center',marginRight:10}}
                      onClick={()=>console.log('checked')}
                      isChecked={false}
                      checkBoxColor='#e0e4e7'

                  />
                  <Text style={localStyle.buttonText}>
                      Reason 4
                  </Text>
                </View>
                <View  style={localStyle.checkPanel}>
                  <CheckBox
                        style={{alignItems:'center',marginRight:10}}
                        onClick={()=>console.log('checked')}
                        isChecked={false}
                        checkBoxColor='#e0e4e7'

                    />
                    <Text style={localStyle.buttonText}>
                        Reason 5
                    </Text>
                  </View>
                  <View style={{marginTop:15}}>
                      <Text style={[localStyle.buttonText,{fontWeight:'500',marginBottom:5,marginLeft:10}]}>
                         Thoughts for PwF Connect:
                       </Text>
                       <TextInput
                           multiline={true}
                           numberOfLines={5}
                           style={{borderWidth:2,borderColor:'#2799bb',width:width/1.3,height:height/4}}
                           placeholder="Type thoughts/questions here (share in PwF Connect if you choose.) Find all of your notes in the Summary by tapping the yellow dots."
                           placeholderTextColor="#446a7a"

                       />

                   </View>
                   <View style={{flexDirection:'row',marginBottom:5,marginTop:10,justifyContent:'space-between'}}>
                     <Text style={[localStyle.buttonText,{fontWeight:'500',flex:1}]}><Text style={{color:'#ea783f'}}>Sick?</Text> tell me more</Text>
                     <Image style={{marginRight:5,marginTop:2}} source={Images.checkMarkIcon} />
                  </View>
              </View>
        </View>
    );



  render (){
    const opacity = this.animated.interpolate({
      inputRange: [0,1],
      outputRange: [1,0]
    });

    let {

        onHomePress,
        onAddCoachesPress,
        onShareWithCoachPress,
        visibleModal,
        onModalClose,
        onFinishPress,
        healthState,


    } = this.props;


    return (<View style={styles.container}>
        <StatusBar/>
        <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>
            <Modal isVisible={visibleModal}>
             {this._renderModalContent(onModalClose)}
           </Modal>
            <View style={[localStyle.section, localStyle.bottomSection]}>
              <View style={localStyle.sharePanel}>
                    <Image
                      source={Images.bigSpoonIcon}
                    />
                    <View style={localStyle.sharedBlock}>
                       <Animated.Text easing='InOut' style={[localStyle.timeText, {opacity}]}>{this.state.feeling}</Animated.Text>

                    </View>
                    {/*<TouchableOpacity  style={localStyle.shareWithCoach} >
                        <Text style={localStyle.answer}>Tap when finished</Text>
                        <Image
                          source={Images.frontArrowIcon}
                        />
                    </TouchableOpacity>*/}

                </View>
                <View style={localStyle.homeButtonPanel}>
                    <TextButton
                        label={"Tap when finished"}
                        onPress={()=>{clearInterval(this.intervalVar); onFinishPress()}}
                        style={localStyle.buttonStyle}
                        loadingColor='#2799bb'
                        primary
                        labelStyle={{fontSize:(Platform.OS === 'ios' ? (width/13) : (width/16)),fontFamily:(Platform.OS === 'ios' ? 'Sarabun-Bold' : '')}}
                    />
                </View>

            </View>
        </KeyboardAwareScrollView>
    </View>
    );
  }

}

export default MealView;
