import React from 'react';
import {Text,Platform,
Dimensions,View} from 'react-native';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import SocialView from './SocialView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
var {height, width} = Dimensions.get('window');
class SocialContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: <Text style={{color:'#53acc7'}}>TELL A FRIEND</Text>,
        headerLeft: <BackButton color='#2799bb' onPress={() => navigation.dispatch({type: "Navigation/BACK"})}/>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTitleStyle: {
            fontSize: 17,
            color: Colors.WHITE,
            alignSelf: 'center',
            textAlign: 'center',
            ...Platform.select({
              android: { width: width/1.8, },
            }),
        }
    });

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }
    onHomePress = () =>{
      this.resetRoute("Dashboard");
    }

   onSocialPress = () =>{

     this.props.navigation.navigate('NotConnected');
   //  this.setState({ visibleModal: true})
   }

    render() {

        return <SocialView
            {...this.state}
            onHomePress={this.onHomePress}
            onSocialPress={this.onSocialPress}
        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
});

export default connect(mapStateToProps, actions)(SocialContainer);
