import React,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    TextInput,
    AsyncStorage,
    Alert

} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

//import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons'
import styles from 'PeaceWithFood/src/styles';
import CheckBox from 'react-native-check-box';
import {Images} from 'PeaceWithFood/src/components/icons';
var {height, width} = Dimensions.get('window');
import DateTimePicker from 'react-native-modal-datetime-picker';
// import TimePicker from 'react-native-simple-time-picker';
import ModalPicker from 'react-native-modal-picker';
import moment from 'moment';
import { LinearGradient } from 'expo';


// const MinCount =[];
// for (i=0;i<=720;i++){
//        MinCount.push ({key:i,label:i});
//      }


const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dce0e2'
    },
    section: {
        flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 110 : height/6.5),
        flexDirection:"column",

    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',

        // paddingRight: 32,

        // borderWidth: .5,
        // borderColor: '#000',
         height: 40,
        // borderRadius: 5 ,
         //margin: 10,
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,

        // paddingVertical: 10,
        // paddingHorizontal: 6,

        marginBottom:25
    },
    inputText: {
      color:'#2799bb',
      fontSize: 17,
      paddingVertical: 10,
      paddingHorizontal: 6,
    },
    timerInputText: {
      color:'#319dbd',
      fontSize: 60,
      paddingVertical: 10,
      //paddingHorizontal: 6,
      textAlign:'center',
      width:350,
    },
    button:{
        backgroundColor:'#00bded',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#446a7a',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 20 : 20),
    },
    homeButtonPanel:{
        marginTop:40,
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
    accountInfoForm: {
        paddingHorizontal: 25
    }
});
let tips=false;

class FrequencyChangeView extends Component {

  constructor(props){
    super(props);
    this.state = {
      page: true,
      isDateTimePickerVisible: false,
      displayTime:'00:30',
      selectedHours: 0,
      selectedMinutes: 0,
      textInputValue:'',
      showTimePicker:false,
      showDisplayTime:true,
      email:'',
      fullname:'',
      displayTimeLable:'00:30',
      endDayTime:'00:00'
    }
  }

    _showDateTimePicker = () => {
      this.setState({ isDateTimePickerVisible: true,displayTime:'' });

    }

    _showEndDayTimePicker = () => {
      this.setState({ isEndDayTimePickerVisible: true,});

    }

    _showTimePicker = () => this.setState({ showTimePicker:true,showDisplayTime:false});

    _hideDateTimePicker = () => this.setState({ isEndDayTimePickerVisible: false });

    _handleDatePicked = (date) => {
      this.setState({endDayTime:date.toString().split(' ')[4].slice(0,5)});
      AsyncStorage.setItem('DayEndTime',date.toString().split(' ')[4]);
      AsyncStorage.setItem('NextDayEndTime',moment(date).add(1,'days').format('x'));
      AsyncStorage.setItem('DayEndTimeStamp',moment(date).format('x'));
      //console.log('A date has been picked: ', moment(date).format('x'),moment(date).add(1,'days').format('MMM DD hh:mm:ss'));
      this._hideDateTimePicker();
    };

    componentDidMount() {
      AsyncStorage.getItem('CheckInTime').then((value) =>{
        if(value){
          if(value !='00:30'){
            let numberTime=Number(value);
            Number.prototype.timeFormator = function(size) {
              var s = String(this.toFixed(2));
              while (s.length < (size + 3 || 5)) {s = "0" + s;}
              return s.replace('.', ':');
            }
            let newDisplayTime=(numberTime/100).timeFormator(2);
            this.setState({displayTimeLable:newDisplayTime});
          }
          else{
            this.setState({displayTimeLable:value});
          }

         this.setState({displayTime:value,});
        }
      });
      AsyncStorage.getItem('@pwf/user').then((value) =>{
        if(value){
          user =JSON.parse(value);
          this.setState({email:user.email,fullname:user.first_name});
           console.log('user',user);
        }
      });
      AsyncStorage.getItem('DayEndTime').then((value) =>{
        if(value){
          this.setState({endDayTime:value.slice(0,5),});
        }
      });
    }



    // onCheckValchange =(option)=>{
    //   this.setState({displayTime:(option.label).toString()});
    //   AsyncStorage.setItem('CheckInTime',(option.label).toString());
    // }
    onCheckValchange =(option)=>{
      this.setState({displayTime:(option).toString()});

    }

    endEdit =()=>{
      if(this.state.displayTime>720){
        Alert.alert("Please enter les than 720 minutes")
      }
      else{
          Alert.alert(
              'Confirm Message',
              'This time will set as your check in time interval',
            )
            Number.prototype.timeFormator = function(size) {
              var s = String(this.toFixed(2));
              while (s.length < (size + 3 || 5)) {s = "0" + s;}
              return s.replace('.', ':');
            }
          let newDisplayTime=(this.state.displayTime/100).timeFormator(2);
          this.setState({displayTimeLable:newDisplayTime,isDateTimePickerVisible:false});
          AsyncStorage.setItem('CheckInTime',(this.state.displayTime).toString());

        }
      }

      onEndDayValchange =(option)=>{
        this.setState({endDayTime:(option).toString()});
      }

  render (){
    const { selectedHours, selectedMinutes } = this.state;
      let {
          onHomePress
          } = this.props;
          return (
            <LinearGradient
                colors={['#EFF3F4', '#E0E4E7']} style={styles.container}>
                  <StatusBar/>
                  <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>
                     <View style={[localStyle.section, localStyle.bottomSection]}>
                        <View style={localStyle.form}>
                            <View style={{justifyContent:'center',alignItems:'center',marginBottom:(Platform.OS === 'ios' ? 40 : 20)}}>
                                <Text style={[localStyle.buttonText,{color:'#319dbd',fontWeight:'500',marginBottom:(Platform.OS === 'ios' ? 20 : 0)}]}>
                                    Change Check-in Timing
                                </Text>
                                {!this.state.isDateTimePickerVisible && (
                                <TouchableOpacity  onPress={this._showDateTimePicker}>
                                     <Text style={[localStyle.buttonText,{color:'#319dbd',fontSize:65,}]}>{this.state.displayTimeLable}</Text>
                                </TouchableOpacity>
                                )}
                                {this.state.isDateTimePickerVisible && (
                                <TextInput
                                      autoFocus={true}
                                      style={[localStyle.timerInputText]}
                                      keyboardType = 'numeric'
                                      placeholderTextColor="#319dbd"
                                      onChangeText={(displayTime) => this.onCheckValchange(displayTime)}
                                      value={this.state.displayTime}
                                      underlineColorAndroid='transparent'
                                      returnKeyType="done"
                                      onEndEditing={() => this.endEdit()}
                                    />
                                    )}

                                  {/* Platform.OS === 'ios' && (
                                  <ModalPicker
                                      data={MinCount}
                                      style={{marginBottom:20,width:(width/1.5),borderWidth:0,borderColor:'transparent'}}
                                      selectStyle={{borderWidth:0}}
                                      selectTextStyle={[localStyle.buttonText,{color:'#319dbd',fontSize:65,}]}
                                      initValue={this.state.displayTime}
                                      onChange={(option)=>{this.onCheckValchange(option)}} >
                                  </ModalPicker>
                                ) */}
                                  {/* Platform.OS === 'android' && (

                                  <ModalPicker
                                      data={MinCount}
                                      style={{marginBottom:5,width:(width/2),borderWidth:0,borderColor:'transparent'}}
                                      selectStyle={{borderWidth:0}}
                                      selectTextStyle={[localStyle.buttonText,{color:'#319dbd',fontSize:65,}]}
                                      initValue={this.state.displayTime}
                                      onChange={(option)=>{this.onCheckValchange(option)}} >
                                        <View style={{justifyContent:'center',alignItems:'center'}}>
                                          <Text style={[localStyle.buttonText,{color:'#319dbd',fontSize:65,justifyContent:'center',alignItems:'center'}]}>{this.state.displayTime}</Text>
                                       </View>
                                  </ModalPicker>

                                  )*/}
                                  <Text style={{marginTop:(Platform.OS === 'ios' ? 5 : 5),color:'#929393',fontSize:18,fontWeight:'500',justifyContent:'center',alignItems:'center'}}>(tap to change)</Text>
                              </View>
                                 <DateTimePicker
                                   mode='time'
                                   titleIOS='Pick a time'
                                   isVisible={false}
                                   onConfirm={this._handleDatePicked}
                                   onCancel={this._hideDateTimePicker}
                                   minuteInterval={10}

                                 />

                                 <View style={{justifyContent:'center',alignItems:'center',marginTop:(Platform.OS === 'ios' ? (height/5) : (height/4))}}>
                                     <Text style={[localStyle.buttonText,{color:'#319dbd',fontWeight:'500',marginBottom:(Platform.OS === 'ios' ? 20 : 0)}]}>
                                         Change End of Day Timing
                                     </Text>

                                     <TouchableOpacity  onPress={this._showEndDayTimePicker}>
                                          <Text style={[localStyle.buttonText,{color:'#319dbd',fontSize:65,}]}>{this.state.endDayTime}</Text>
                                     </TouchableOpacity>


                                         <DateTimePicker
                                           mode='time'
                                           titleIOS='Pick a time'
                                           isVisible={this.state.isEndDayTimePickerVisible}
                                           onConfirm={this._handleDatePicked}
                                           onCancel={this._hideDateTimePicker}
                                          />


                                       <Text style={{marginTop:(Platform.OS === 'ios' ? 5 : 5),color:'#929393',fontSize:18,fontWeight:'500',justifyContent:'center',alignItems:'center'}}>(tap to change)</Text>
                                   </View>
                            </View>
                      </View>
                  </KeyboardAwareScrollView>
              </LinearGradient>
    );
  }
}

export default FrequencyChangeView;
