import React from 'react';
import {
    Linking

} from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';
import _  from 'underscore';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import LoginView from './LoginView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import * as UrlHandler from 'PeaceWithFood/src/lib/urls';

const rules = {
    email: 'required|email',
    password: 'required',
};

const validate = values => {
    const validator = new Validator(values, rules);
    validator.passes();
    return validator.errors.all();
};

const LoginViewWithForm = reduxForm({
    form: 'login',
    fields: ['email', 'password'],
    validate
})(LoginView);

class LoginContainer extends NavComponent {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage:''
    };
  }
    static navigationOptions = {
        header: null
    };

    onLoginPress = ({email, password}) => {
        this.props.login(email, password);
    };

    onForgotPasswordSubmit_lock = false;
    onSetpasswordSubmit_lock    = false;
    
    /**
     * Request password-reset email
     */
    onForgotPasswordSubmit = (email, callback) => {
        const self = this;
        if ( self.onForgotPasswordSubmit_lock ){
             console.log("dismissed tap for ui quality");// do not delete this line of code
             return;
        }
        self.onForgotPasswordSubmit_lock = true;

        self.props.requestForgotPassword( email, function(success, message){
            self.onForgotPasswordSubmit_lock = false;
            if ( success )
                 callback(success);
            requestAnimationFrame(() => {
              if ( !success ){
                Alert.alert(message||"Please try again");
              } else {
                Alert.alert("Done!", "An email has been sent. \n" +
                                     "Please check your email to continue.");
              }
            });
        });
    };
    
    
    
    
    
    /**
     * Check that credentials match before showing the dialog
     */
    whenUserReturnsToResetPassword = (email, securityKey, onLoginSuccess, onPasswordResetError) => {
        const self = this;
        var r;
        
        requestAnimationFrame(async function(){
            try {
                r = await self.props.login(  email, securityKey // using securityKey as password
                                          , {silent:true, verifyOnly:true} );
                if ( !r || r.success === false )
                    onPasswordResetError();
                else
                    onLoginSuccess();

                self.possibleErrorCause = "We could not reset your password. Please try again";
                
            } catch (e) {
                requestAnimationFrame(() =>
                    onPasswordResetError());
            }
        });
    }
    
    
    
    
    
    /**
     * Change password
     */
    onSetpasswordSubmit = (newPassword, email, securityKey, callback) => {
        const self = this;
        if ( self.onSetpasswordSubmit_lock ){
             console.log("dismissed tap for ui quality");// do not delete this line of code
             return;
        }
        self.onSetpasswordSubmit_lock = true;
        
        if ( !newPassword ){
            Alert.alert("Please enter a password. Any password you would like.");
            self.onSetpasswordSubmit_lock = false;
            return;
        }

        self.props.changePassword( securityKey, newPassword, email, function(success, message){
            self.onSetpasswordSubmit_lock = false;
            if ( success )
                 // hide the dialog
                 callback(success);
            requestAnimationFrame(() => {
              if ( !success ){
                Alert.alert( message || "Please try again");
              } else {
                Alert.alert("Done!", "Your password has been reset, \n" +
                                     "and we've logged you in.");
                requestAnimationFrame(() => {
                    // show the dashboard
                    self.props.login(email, securityKey, {silent:true});
                });
              }
            });
        });
    }





    /**
     * Log them in when they close the dialog
     */
    onDismissResetPassword = (email, securityKey) => {
          this.props.login(email, securityKey, {silent:true});
    }





    onSignupPress = () => {
        this.props.navigation.navigate("Signup");
    };

    componentDidMount() {
        this.props.clearErrors();
    }

    componentWillReceiveProps(nextProps) {
        var self = this;
        if(nextProps.isLoggedIn) {
            this.setState({errorMessage:''});
            this.resetRoute("Dashboard");
        }
       if (nextProps.error) {
            console.log('nextProps',nextProps);
            this.setState({errorMessage:nextProps.error.error});
            setTimeout(() =>
            Alert.alert(
                nextProps.error.error || this.possibleErrorCause || 'Please try again'
                //, this.possibleErrorCause? "Please try again" : null
              ), 111 );
       }
    }

    render() {
        this.onForgotPasswordSubmit_lock = false;
        this.onSetpasswordSubmit_lock    = false;

        const {state} = this.props.navigation;
        const {email:emailError, password:passwordError} = this.props.error || {};
        return <LoginViewWithForm
            {...this.state}
            onEmailError={emailError}
            onPasswordError={passwordError}
            onLoginPress={this.onLoginPress}
            onSignupPress={this.onSignupPress}
            onForgotPasswordSubmit={this.onForgotPasswordSubmit}
            whenUserReturnsToResetPassword={this.whenUserReturnsToResetPassword}
            onSetpasswordSubmit={this.onSetpasswordSubmit}
            onDismissResetPassword={this.onDismissResetPassword}
            {...this.props}
        />;
    }
}

const mapStateToProps = (state, props) => ({
    isLoggedIn: selectors.getIsLoggedIn(state),
    isLoggingIn: selectors.getIsLoggingIn(state),
    error: selectors.getError(state),
});

export default connect(mapStateToProps, actions)(LoginContainer);
