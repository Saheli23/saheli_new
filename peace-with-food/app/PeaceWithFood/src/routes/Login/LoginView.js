import React,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    Animated,
    Keyboard,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';
import Prompt from 'react-native-prompt';
import SpinnerOverlay from 'react-native-loading-spinner-overlay';


import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import TextInput from 'PeaceWithFood/src/components/form/textinput';
import {TextButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
import CheckBox from 'react-native-check-box';
var {height, width} = Dimensions.get('window');
import { LogoIcon } from 'PeaceWithFood/src/components/icons';
import Toast, {DURATION} from 'react-native-easy-toast';
import _ from 'underscore'
import * as UrlHandler from 'PeaceWithFood/src/lib/urls';


const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#00bded'
    },
    section: {
        flex: 1
    },
    headerSection: {
        justifyContent: 'center',
        //paddingBottom: 20,
        backgroundColor:'red',

    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1,
        marginTop:(Platform.OS === 'ios' ? 40 : 30)

    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        //backgroundColor: '#FFFFFF'
        backgroundColor:'#07c4f0',
        borderBottomColor:'white',
        borderLeftColor:'#00bded',
        borderRightColor:'#00bded',
        borderTopColor:'#00bded',
        color:'white'

    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'white'
    },
    forgotPasswordButton: {
        alignItems: 'flex-end',
        paddingHorizontal: 25,
        paddingVertical: 20
    },
    registerButton: {
        alignItems: 'center',
        paddingBottom: 80,
        paddingTop:(Platform.OS === 'ios' ? 40 : 20)
    },
    buttonText: {
        color: '#FFF',
        fontSize: 16
    },
    bottomSection: {
        //backgroundColor: '#46addd'
        backgroundColor:'#00bded',

    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#00a5d0',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    },
    professionalPanel:{
      alignItems: 'center',
      justifyContent:'center',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
      flexDirection:'row'
    }

});
let professional=false;
class LoginView extends Component {


  constructor(props, context) {
      super(props, context);
      this.state = {
         toggle: false,
         buttonPanel:true,
         forgotPromptVisible:false,
         setpasswordPromptVisible:false,
         userCancelSetpassword: false,
         forgotpasswordLoadingShow: false,

      };
  }


  componentWillMount() {
      const self = this;
      UrlHandler.onResetPassword( (email, key) => {
           //
           //  Called initially when the login is displayed
           //
           //self.state.setpasswordPromptVisible = true;
           //self.state.forgotpasswordLoadingShow = true;
           self.state.passwordrecovery_email = email;
           self.state.passwordrecovery_key   = key;
      });

  }

  onInitSetpassword() {
        const self = this;
        requestAnimationFrame(() => {
           const self = this;
           //
           //  Called initially when the login is displayed
           //  only if the link is valid
           //
           self.state.setpasswordPromptVisible  = true;
           self.state.forgotpasswordLoadingShow = false;
           self.setState({
               setpasswordPromptVisible: true,
               forgotpasswordLoadingShow:false,
           });

           // woah look out
           UrlHandler.cancelAllResetRequests();
        })
  }


  onPasswordResetError(message){
        const self = this;
        requestAnimationFrame(() => {
            //
            //  Called when an external link is expired
            //
            self.setState({
                setpasswordPromptVisible:  false,
                forgotpasswordLoadingShow: false,
                userCancelSetpassword:     true,
            });
            requestAnimationFrame(() =>
            Alert.alert( "Could not reset password"
                       , "Perhaps the link has expired. \n" +
                         "Please try again by tapping 'forgot password'"+
                         (message? " \n" + message : "")
                       ));
        })
  }



render (){
    let {
    hasAccount = true,
    onEmailError,
    onPasswordError,
    onLoginPress,
      onForgotPasswordSubmit,
      onSetpasswordSubmit,
      whenUserReturnsToResetPassword,
      onDismissResetPassword,
    onSignupPress,
    isLoggingIn,
    handleSubmit,
    errorMessage
    } = this.props;
    const self = this;


    if ( !this.eventsMapped ){
        //
        //  Calls (onInitSetpassword) appropriately
        //  When a url is received (usually on launch)
        //
        const clear_buffer = true;
        const wrap = function(func){ // wraps a function to prevent debounce, etc.
                        return _.debounce((email, key) => {
                            requestAnimationFrame(() => func(email, key) );
                        }, 333, true );
                     };
        //
        //  When a url comes in, we want to show a loading spinner
        //  and then after verification, show the dialog.
        //
        UrlHandler.onResetPassword(wrap((email, key) => {
                    self.setState({
                        forgotpasswordLoadingShow:true
                    });
                    whenUserReturnsToResetPassword.call(this,
                            email, key,
                            self.onInitSetpassword.bind(self),
                            self.onPasswordResetError.bind(self)
                            );
                })
                , clear_buffer
        );
    }
    this.eventsMapped = true;



    const onForgotPasswordPress = () => {
            self.setState({forgotPromptVisible:true})
    }


    let commmonMarkup = (
          <View>
            <SpinnerOverlay  visible={ self.state.forgotpasswordLoadingShow
                                       && !self.state.userCancelSetpassword }
                             overlayColor="rgba(0, 0, 0, 0.5)"
                             textContent={"Please wait"}
                             textStyle={{color: '#FFF'}}
                             />
            <Prompt
                title="Please enter your email to reset your password."// with which you used to sign up with Peace with Food"
                placeholder="email"
                defaultValue={  self.state.forgotPromptDefault }
                textInputProps={{
                    "keyboardType":'email-address',
                    "autoCapitalize":'none',
                }}
                submitText="OK"
                cancelText="cancel"
                visible={ self.state.forgotPromptVisible }
                onCancel={ () => self.setState({
                  forgotPromptVisible: false,
                }) }
                onSubmit={ (email) => {
                    onForgotPasswordSubmit(
                      email,
                      () => self.setState({
                              forgotPromptVisible: false
                            })
                    );
                } }
                />
            <Prompt
                title="Please enter your new password:"
                placeholder="password"
                defaultValue=""
                textInputProps={{
                    "secureTextEntry":true,
                    "visible-password":true, // android only
                }}
                submitText="OK"
                cancelText="cancel"
                visible={ self.state.setpasswordPromptVisible
                          && !self.state.userCancelSetpassword }
                onCancel={ () => {
                        self.setState({
                              setpasswordPromptVisible: false,
                              userCancelSetpassword: true,
                        });
                        onDismissResetPassword(
                              self.state.passwordrecovery_email,
                              self.state.passwordrecovery_key,
                        );
                }}
                onSubmit={ (newPassword) => {

                        onSetpasswordSubmit(
                              newPassword,
                              self.state.passwordrecovery_email,
                              self.state.passwordrecovery_key,
                              () => self.setState({
                                      setpasswordPromptVisible: false,
                                      userCancelSetpassword: true,
                                    })
                        );
                }}
                />
          </View>
    );





if (Platform.OS === 'ios') {

    return (<View style={styles.container}>
          <StatusBar/>
          <KeyboardAvoidingView
            style={localStyle.container}
            behavior="padding"
          >
          <KeyboardAwareScrollView  resetScrollToCoords={{x: 0, y: 0}} style={localStyle.container} contentContainerStyle={localStyle.container}>
                <View style={[localStyle.section, localStyle.bottomSection]}>
                  <View style={localStyle.logoPanel}>
                    <LogoIcon/>
                  </View>
                  <View style={localStyle.form}>
                      <TouchableOpacity style={localStyle.forgotPasswordButton} onPress={onForgotPasswordPress}>
                          <Text style={localStyle.buttonText}>
                              forgot password?
                          </Text>
                      </TouchableOpacity>
                      <View style={[localStyle.row]}>
                          <TextInput
                              fieldName="email"
                              numberOfLines={1}
                              style={localStyle.inputText}
                              placeholder="email"
                              placeholderTextColor="white"
                              editable={!isLoggingIn}
                              error={onEmailError}
                              keyboardType='email-address'

                          />
                      </View>
                      <View style={[localStyle.row]}>
                          <TextInput
                              fieldName="password"
                              style={localStyle.inputText}
                              placeholder="password"
                              placeholderTextColor="white"
                              editable={!isLoggingIn}
                              error={onPasswordError}
                              secureTextEntry
                              returnKeyType={'done'}
                          />
                      </View>
                      {/*<View  style={localStyle.professionalPanel}>
                      <CheckBox
                            style={{alignItems:'center',marginRight:10}}
                            onClick={()=>professional ? false : true}
                            isChecked={professional}
                            checkBoxColor='#13d0f6'

                        />
                        <View style={{flexDirection:'column'}}>
                          <Text style={localStyle.buttonText}>
                              I am using this app as an
                          </Text>
                          <Text style={localStyle.buttonText}>
                              Intuitive Eating Professional
                          </Text>
                        </View>
                      </View>*/}
                      <View style={[localStyle.row,{marginTop:60}]}>
                          <TextButton
                              label={"Login"}
                              style={localStyle.buttonStyle}
                              onPress={handleSubmit(onLoginPress)}
                              isLoading={isLoggingIn}
                              loadingColor='#07c4f0'
                              primary
                              labelStyle={{fontSize:24}}
                          />
                      </View>
                  </View>
                  <TouchableOpacity style={localStyle.registerButton} onPress={onSignupPress}>
                      <Text style={localStyle.buttonText}>
                          register new account
                      </Text>
                  </TouchableOpacity>
              </View>
          </KeyboardAwareScrollView>
        </KeyboardAvoidingView>

        { commmonMarkup }

    </View>
  )}
    return (<View style={styles.container}>
          <StatusBar/>
          <KeyboardAvoidingView
          style={localStyle.container}
          behavior="padding"

        >
          <ScrollView>


                <View style={[localStyle.section, localStyle.bottomSection]}>
                    <View style={localStyle.logoPanel}>
                      <LogoIcon/>
                    </View>
                    <View style={localStyle.form}>
                        <TouchableOpacity style={localStyle.forgotPasswordButton} onPress={onForgotPasswordPress}>
                            <Text style={localStyle.buttonText}>
                                forgot password?
                            </Text>
                        </TouchableOpacity>
                        <View style={[localStyle.row]}>
                            <TextInput
                                fieldName="email"
                                numberOfLines={1}
                                style={localStyle.inputText}
                                placeholder="email"
                                placeholderTextColor="white"
                                editable={!isLoggingIn}
                                error={onEmailError}
                                keyboardType='email-address'

                            />
                        </View>
                        <View style={[localStyle.row]}>
                            <TextInput
                                fieldName="password"
                                style={localStyle.inputText}
                                placeholder="password"
                                placeholderTextColor="white"
                                editable={!isLoggingIn}
                                error={onPasswordError}
                                secureTextEntry
                                returnKeyType={'done'}
                            />
                        </View>
                        {/*<View  style={localStyle.professionalPanel}>
                        <CheckBox
                              style={{alignItems:'center',marginRight:10}}
                              onClick={()=>professional ? false : true}
                              isChecked={professional}
                              checkBoxColor='#13d0f6'

                          />
                          <View style={{flexDirection:'column'}}>
                            <Text style={localStyle.buttonText}>
                                I am using this app as an
                            </Text>
                            <Text style={localStyle.buttonText}>
                                Intuitive Eating Professional
                            </Text>
                          </View>
                        </View>*/}
                        <View style={[localStyle.row,{marginTop:60}]}>
                            <TextButton
                                label={"Login"}
                                style={localStyle.buttonStyle}
                                onPress={handleSubmit(onLoginPress)}
                                isLoading={isLoggingIn}
                                loadingColor='#07c4f0'
                                primary
                                labelStyle={{fontSize:24}}
                            />
                        </View>
                    </View>
                    <TouchableOpacity style={localStyle.registerButton} onPress={onSignupPress}>
                        <Text style={localStyle.buttonText}>
                            register new account
                        </Text>
                    </TouchableOpacity>
                </View>
              </ScrollView>
        </KeyboardAvoidingView>

        { commmonMarkup }

    </View>
  );
  }

  }
export default LoginView;
