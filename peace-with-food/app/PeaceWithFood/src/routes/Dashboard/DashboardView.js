import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Dimensions,
    Platform,
    WebView
} from 'react-native';

import styles from 'PeaceWithFood/src/styles';
import { Colors, Sizes } from 'PeaceWithFood/src/styles/constants';
import {Images} from 'PeaceWithFood/src/components/icons';
var {height, width} = Dimensions.get('window');
import * as Animatable from 'react-native-animatable';
//import AppIntro from 'react-native-app-intro';
import Modal from 'react-native-modal';
const aspectRatio = height/width;
import Swiper from 'react-native-swiper';

const localStyles = StyleSheet.create({
    blocksContainer: {
        flex: 1
    },
    blockRow: {
        flexDirection: 'row',
        flex: 1
    },
    firstBlockRow: {
        flex: 1.5
    },
    block: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    rythmBlock: {
        backgroundColor: Colors.RYTHM
    },
    foodBlock: {
        backgroundColor: Colors.TRACK
    },
    videosBlock: {
        backgroundColor: Colors.EDUCATE
    },
    shareBlock: {
        backgroundColor: Colors.SHARE
    },
    chartBlock: {
        backgroundColor: Colors.HISTORY
    },
    settingsBlock: {
        backgroundColor: Colors.SETTINGS
    },
    endBlock: {
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.EVENING
    },
    endMorningBlock: {
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.MORNING
    },
    rythmText:{
       fontSize:20,
       color:'#01bded',
       marginTop:20
    },
    blockText:{
      fontSize:20,
      color:'white',
      marginTop:20
    },
    modalContent: {
      backgroundColor: 'white',
      //padding: 22,
      //height:(Platform.OS === 'ios' ? height/1.4 : height/1.2),
      height:(Platform.OS === 'ios' ? (aspectRatio >1.78) ? height/1.4: height/1.1 : height/1.2),
      width:(Platform.OS === 'ios' ? width/1.12 : width/1.12),
      // justifyContent: 'center',
      // alignItems: 'center',
      borderRadius: 4,
      borderColor: 'rgba(0, 0, 0, 0.1)',

  },
    slide: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#9DD6EB',
      //padding: 15,
      height:(Platform.OS === 'ios' ? height/1.8 : height/1.6),
      width:width/1.12,





  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },

rectangle280: {
   width: width/1.5,
   height: 91,
   shadowColor: 'rgba(0, 0, 0, 0.16)',
   shadowOffset: { width: 3, height: 0 },
   shadowRadius: 6,
   backgroundColor: '#f2f2f2',
 },
 circle_one:{
    // width: 35,
    // height: 35,
    backgroundColor: '#00bcec',
 },
 subtraction9:{
    width: 117,
    height: 55,
    shadowColor: 'rgba(0, 0, 0, 0.16)',
    shadowOffset: { width: 6, height: 6 },
    shadowRadius: 6,
    backgroundColor: '#000000',
    opacity: 0.12,
 },
 rectangle282: {

    width: width/1.35,
    height: height/3.5,
    backgroundColor: '#eac435',
  },
  smallDevice:{
    ...Platform.select({
      ios: { position:'absolute',top:-14 },
    }),
  },
  highResolution:{

  },

});
onModalClose = () =>{
  this.setState({visibleModal:false})
}

_renderModalContent = (onModalClose,onClosePress) => {
  //console.log(".....gh... ", aspectRatio);
  let headerText='Setting your Rhythm';
  onSlideChangeHandle = (index, total) => {
      //console.log(index, total);
      if(index ==0){
        headerText='Setting your Rhythm';
      }
      else if(index ==1){
        headerText='Questions and Answers';
      }
      else if(index ==2){
        headerText='Peace with food connect'
      }
      else if(index ==3){
        headerText='You are almost there!'
      }

    }
return(
    <View style={localStyles.modalContent}>
      <View style={{flexDirection:'row',justifyContent:'space-between',paddingLeft: 22,paddingTop:22}}>
        <View style={{flexDirection:'column',justifyContent:'flex-start'}}>
          <Text style={{
                        opacity: 0.86,
                        color: '#125572',
                        fontSize: 20,
                        fontWeight: '400',}}>Getting Started...</Text>
          {/*<Text style={{color:'#00bcec',fontSize:20,}}>Setting your Rhythm</Text>*/}
        </View>
        <TouchableOpacity style={{width:50,height:50,justifyContent:'center',alignItems:'center'}} onPress={onModalClose}>
         <Image source={Images.orangeCloseIcon} />
       </TouchableOpacity>
    </View>
    <Swiper style={styles.wrapper}
            dot={<View style={{backgroundColor: '#add1e0', width: 13, height: 13, borderRadius: 7, marginLeft: 7, marginRight: 7}} />}
            activeDot={<View style={{backgroundColor: '#00bded', width: 13, height: 13, borderRadius: 7, marginLeft: 7, marginRight: 7}} />}
            paginationStyle={{
                bottom: (Platform.OS == 'android' ? (aspectRatio > 1.9) ?  10 : height/22 : height/22)
            }}
            loop={false}>
            <View style={[localStyles.slide,{ backgroundColor: '#fff',flexDirection:'column'}]}>
                  <View style={{position:'absolute',top:5,left:22}} >
                      <Text style={{color:'#00bcec',fontSize:20,marginBottom:5}}>Setting your Rhythm</Text>
                      <Image source={Images.first_slider} />
                  </View>
                </View>
                <View style={[localStyles.slide, { backgroundColor: '#fff', }]}>
                 {/*<View style={[localStyles.rectangle282,{width:width/1.12,justifyContent:'center',position:'absolute',top:5,alignItems:'center',}]}>
                   <Image style={{justifyContent:'center',alignItems:'center',}} source={Images.second_slider} />
                          <View style={{width: width/1.12,
                                        height: height/18,
                                        shadowColor: 'rgba(0, 0, 0, 0.16)',
                                        shadowOffset: { width: -2, height: 0 },
                                        shadowRadius: 3,
                                        backgroundColor: '#fff',position:'absolute',top:height/4}}>
                                        <Text style={{color: '#95989a',fontSize: 22,fontWeight: '400',padding:22}}>Go to MORE and browse through a specially crafted Q&A that will help you understand the paradigm shift toward peace with food and your body.</Text>

                          </View>
                      </View>*/}
                      <View style={Platform.OS == 'ios'&& (aspectRatio > 1.78) ? localStyles.smallDevice : localStyles.highResolution}>
                        <View>
                          <Text style={{color:'#00bcec',fontSize:(Platform.OS === 'ios' ? (aspectRatio >1.78) ? 20 : 20 : 20),padding:20}}>Questions and Answers</Text>
                        </View>
                        <View style={{width:width/1.12,height:(Platform.OS === 'ios' ? (aspectRatio >1.78) ? height/4 : height/3.5 : height/4),backgroundColor:'#eac435',justifyContent:'center',alignItems:'center'}}>
                          <Image style={{justifyContent:'center',alignItems:'center',}} source={Images.second_slider} />
                        </View>
                        <View>
                          <Text style={{color: '#95989a',fontSize: (Platform.OS === 'ios' ?(aspectRatio >1.78) ? 18 : height/32 : 18),fontWeight: '400',padding:22}}>Go to MORE and browse through a specially crafted Q&A that will help you understand the paradigm shift toward peace with food and your body.</Text>
                        </View>
                      </View>
                  </View>
                <View style={[localStyles.slide,{ backgroundColor: '#fff',flexDirection:'column'}]}>
                    <View style={Platform.OS == 'ios'&& (aspectRatio > 1.78) ? localStyles.smallDevice : localStyles.highResolution}>
                      <View>
                        <Text style={{color:'#00bcec',fontSize:(Platform.OS === 'ios' ? 20 : 20),padding:(Platform.OS === 'ios' ? 22 : 20)}}>PwF Connect</Text>
                      </View>
                    <View style={{width:width/1.12,height:(Platform.OS === 'ios' ? (aspectRatio >1.78) ? height/4 : height/3.6 : height/4),backgroundColor:'#e7ebed',justifyContent:'center',alignItems:'center'}}>
                      <Image source={Images.connectIcon} />
                    </View>
                    <View>
                        <Text style={{color:'#a5a7a9',fontSize:(Platform.OS === 'ios' ?(aspectRatio>1.78) ? 18 : height/32 : 16),padding:22}}>
                         PwF allows you to seamlessly communicate privately with others who are using the PwF app.
                         Share your daily rhythms, thoughts and questions. Enter the growing community of encouragement and support.
                        </Text>
                    </View>
                    </View>
                </View>
                <View style={[localStyles.slide,{ backgroundColor: '#fff',flexDirection:'column'}]}>
                  <View style={Platform.OS == 'ios'&& (aspectRatio > 1.78) ? localStyles.smallDevice : localStyles.highResolution}>
                    <View>
                      <Text style={{color:'#00bcec',fontSize:(Platform.OS === 'ios' ? 20 : 18),padding:(Platform.OS === 'ios' ? 22 : 20)}}>Link with Certified Professionals</Text>
                    </View>
                    <View style={{width:width/1.12,height:(Platform.OS === 'ios' ? (aspectRatio >1.78) ? height/4 : height/3.7 : height/4),backgroundColor:'#f17d42',justifyContent:'center',alignItems:'center'}}>
                      <Image source={Images.professional_slider} />
                    </View>
                    <View>
                        <Text style={{color:'#a5a7a9',fontSize:(Platform.OS === 'ios' ?(aspectRatio>1.78) ? 20 : height/32 : 16),padding:22}}>
                         Sometimes we need just a little extra help on our journey toward having peace with food. Locate a certified professional (coach, dietitian or counselor) and link up with them on PwF connect!
                        </Text>
                    </View>
                  </View>
                </View>
                <View style={[localStyles.slide, { backgroundColor: '#fff' }]}>
                  <View style={Platform.OS == 'ios'&& (aspectRatio > 1.78) ? localStyles.smallDevice : localStyles.highResolution}>
                    <View>
                      <Text style={{color:'#00bcec',fontSize:20,padding:22}}>You&apos;re almost there!</Text>
                    </View>
                    <View style={{width:width/1.12,height:(Platform.OS === 'ios' ? (aspectRatio >1.78) ? height/4 : height/3.5 : height/4)}}>
                        <WebView  javaScriptEnabled={true} source={{uri:'https://www.youtube.com/embed/uRdpKLHD0cg?rel=0&autoplay=0&showinfo=0&controls=1'}}  />
                    </View>
                    <View>
                        <Text style={{color:'#a5a7a9',fontSize:(Platform.OS === 'ios' ? (aspectRatio>1.78) ? 20 : height/32 : 16),padding:22}}>
                         Watch the 3-minute How-To Video to learn more about using the Rhythm Tracker.
                         Enjoy watching all of the PwF Videos...more added frequently. Stay tuned!
                        </Text>
                    </View>
                  </View>
                </View>

          </Swiper>
        </View>
  )};

const DashboardView = ({
    onRhythmPress,
    onFoodPress,
    onVideosPress,
    onSharePress,
    onChartsPress,
    onSettingsPress,
    onEveningPress,
    onMorningPress,
    timeIcon,
    onNeedHelpPress,
    needHelp,
    firstLogin,
    onClosePress,
    onModalClose,
    visibleModal

}) => (
    <View style={[styles.container]}>
        <View style={localStyles.blocksContainer}>
            <View style={[localStyles.blockRow, localStyles.firstBlockRow]}>
                <TouchableOpacity style={[localStyles.block, localStyles.rythmBlock]} onPress={timeIcon =='sun' ? null : onRhythmPress}>
                    <Image
                        source={Images.checkinIcon}
                    />
                    <Text style={localStyles.rythmText}>
                        CHECK IN
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={[localStyles.block, localStyles.foodBlock]} onPress={timeIcon =='sun' ? null : onFoodPress}>
                    <Image
                        source={Images.spoonIcon}
                    />
                    <Text style={localStyles.blockText}>
                        EAT
                    </Text>
                </TouchableOpacity>
            </View>
            {timeIcon =='sun' && Platform.OS === 'ios' &&
                  (
            <View style={[{backgroundColor: 'transparent',zIndex:1,top:(Platform.OS === 'ios' ? ((aspectRatio >1.6) ? (-height/2.7) :( -height/2.58)) : (-height/2.7)),}]}>
                <View style={{ height:((Platform.OS === 'ios') && (aspectRatio >1.6) ? height/2.7 : height/2.58 ) , position: 'absolute', left: 0, top: 0, opacity: 0.6, backgroundColor: 'black', width: width,}}>
                    {/* Based on client feedback
                      {needHelp &&
                    <TouchableOpacity onPress={onNeedHelpPress} style={{marginLeft:width/2.5}} >
                      <Text style={{ fontSize: 20, color: 'white', marginTop:height/4, }} onPress={onNeedHelpPress}>Need help getting started?</Text>
                    </TouchableOpacity>
                    }
                    {!needHelp && (
                        <Text style={{ fontSize: 20, textAlign: 'center', alignItems: 'center', color: 'white', marginTop:height/4, }}>
                          These options are unavailable. To continue tracking meal progress, begin a new day
                        </Text>
                      )}*/}
                </View>
            </View>
                  )
            }
            {timeIcon =='sun' && Platform.OS === 'android' &&
                  (
            <View style={[{backgroundColor: 'transparent',zIndex:1,top: 0,elevation: 1,position: 'absolute'}]}>
                <View style={{height: (height < 570 ? height/3 : height/2.90),position: 'relative',left: 0, top: 0, opacity: 0.7, backgroundColor: 'black', width: width,alignItems: 'center',justifyContent: 'center'}}>
                  {/* Based on client feedback
                    {needHelp &&
                <TouchableOpacity onPress={onNeedHelpPress} style={{marginLeft:width/3.5}} >
                  <Text style={{ fontSize: 20, color: 'white', marginTop:height/4, }} onPress={onNeedHelpPress}>Need help getting started?</Text>
                </TouchableOpacity>
                }
                {!needHelp && (
                    <Text style={{ fontSize: 20, textAlign: 'center', alignItems: 'center', color: 'white', marginTop:height/4, }}>
                      These options are unavailable. To continue tracking meal progress, begin a new day
                    </Text>
                  )} */}
                </View>
            </View>
                  )
            }
            <View style={[localStyles.blockRow]}>
                <TouchableOpacity style={[localStyles.block, localStyles.videosBlock]} onPress={onVideosPress}>
                    <Image
                        source={Images.videoIcon}
                    />
                    <Text style={localStyles.blockText}>
                        VIDEOS
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={[localStyles.block, localStyles.shareBlock]} onPress={onSharePress}>
                    <Image
                        source={Images.shareIcon}
                    />
                    <Text style={localStyles.blockText}>
                        CONNECT
                    </Text>
                </TouchableOpacity>
            </View>
            <View style={[localStyles.blockRow]}>
                <TouchableOpacity style={[localStyles.block, localStyles.chartBlock]} onPress={onChartsPress}>
                    <Image
                        source={Images.chartIcon}
                    />
                    <Text style={localStyles.blockText}>
                        SUMMARY
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={[localStyles.block, localStyles.settingsBlock]} onPress={onSettingsPress}>
                    <Image
                        source={Images.settingsIcon}
                    />
                    <Text style={localStyles.blockText}>
                        MORE
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
        {timeIcon =='sun' && !needHelp && (
          <View style={{position: 'absolute', alignItems:'center',justifyContent:'center',height:height,left:(width/2-25),top:height/1.2 }}>
              <Animatable.Text  style={{fontSize:65,color:'yellow',fontWeight:'800' ,textAlign: 'center', alignItems: 'center',justifyContent:'center',backgroundColor:'transparent',height:80,width:50}}  animation="slideInDown" iterationCount={3} direction="normal" easing={'ease-in-out'} delay={1000}>↓</Animatable.Text>
          </View>
        )}
        { timeIcon =='moon' &&
              (
                <TouchableOpacity style={localStyles.endBlock} onPress={onEveningPress}>
                    <Image
                  source={Images.eveningIcon}
                />
                </TouchableOpacity>
              )
        }
        { timeIcon =='sun' &&
              (
               <TouchableOpacity style={localStyles.endMorningBlock} onPress={onMorningPress}>
                       <Image
                     source={Images.sunIcon}
                   />
              </TouchableOpacity>
                )
          }

          {firstLogin ==true  && Platform.OS === 'ios' && (
          <View style={[{backgroundColor: 'transparent',zIndex:1,top: 0,elevation: 1,position: 'absolute'}]}>
              <View style={{ height: height, position: 'absolute', left: 0, top: 0, opacity: 0.6, backgroundColor: 'black', width: width,justifyContent:'center',alignItems:'center'}}>
                  <View style={{height:220,width:width,marginTop:height/15}}>
                  <Modal isVisible={visibleModal}>
                   {this._renderModalContent(onModalClose,onClosePress)}
                 </Modal>
                    {/*  <WebView  javaScriptEnabled={true} source={{uri:'https://www.youtube.com/embed/Jq0oW_UnmDw?rel=0&autoplay=0&showinfo=0&controls=1'}}  />*/}
                  </View>
                  {/*<TouchableOpacity style={{height:50,justifyContent:'center',alignItems:'center'}} onPress={onClosePress}>
                    <Text style={{fontSize:20,color:'white'}}>Close</Text>
                  </TouchableOpacity>*/}
                  {/* Based on client feedback
                    {needHelp &&
                  <TouchableOpacity onPress={onNeedHelpPress} style={{marginLeft:width/2.5}} >
                    <Text style={{ fontSize: 20, color: 'white', marginTop:height/4, }} onPress={onNeedHelpPress}>Need help getting started?</Text>
                  </TouchableOpacity>
                  }
                  {!needHelp && (
                      <Text style={{ fontSize: 20, textAlign: 'center', alignItems: 'center', color: 'white', marginTop:height/4, }}>
                        These options are unavailable. To continue tracking meal progress, begin a new day
                      </Text>
                    )}*/}
              </View>
          </View>
          )}
          {firstLogin ==true && Platform.OS === 'android' && (
          <View style={[{backgroundColor: 'transparent',zIndex:1,top: 0,elevation: 1,position: 'absolute'}]}>
              <View style={{height: height,position: 'relative',left: 0, top: 0, opacity: 0.6, backgroundColor: 'black', width: width,alignItems: 'center',justifyContent: 'center'}}>
                  <View style={{height:220,width:width,marginTop:height/15}}>
                    <Modal isVisible={visibleModal}>
                     {this._renderModalContent(onModalClose,onClosePress)}
                   </Modal>
                    {/*  <WebView  javaScriptEnabled={true} source={{uri:'https://www.youtube.com/embed/Jq0oW_UnmDw?rel=0&autoplay=0&showinfo=0&controls=1'}}  /> */}
                  </View>
                  {/*<TouchableOpacity style={{height:50,justifyContent:'center',alignItems:'center'}} onPress={onClosePress}>
                    <Text style={{fontSize:20,color:'white'}}>Close</Text>
                  </TouchableOpacity>*/}
                  {/* Based on client feedback
                    {needHelp &&
                  <TouchableOpacity onPress={onNeedHelpPress} style={{marginLeft:width/2.5}} >
                    <Text style={{ fontSize: 20, color: 'white', marginTop:height/4, }} onPress={onNeedHelpPress}>Need help getting started?</Text>
                  </TouchableOpacity>
                  }
                  {!needHelp && (
                      <Text style={{ fontSize: 20, textAlign: 'center', alignItems: 'center', color: 'white', marginTop:height/4, }}>
                        These options are unavailable. To continue tracking meal progress, begin a new day
                      </Text>
                    )}*/}
              </View>
          </View>
          )}
    </View>
);

export default DashboardView;
