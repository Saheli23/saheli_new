import React from 'react';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import DashboardView from './DashboardView';
import {ListView,AsyncStorage,Platform,AppState} from 'react-native';
import {Util, Alert} from 'PeaceWithFood/src/lib/util';
import {connect} from 'react-redux';
import {
    actions,
    selectors as userSelectors
} from 'PeaceWithFood/src/store/user';

import styles from 'PeaceWithFood/src/styles';
import { Notifications, Permissions, Constants } from 'expo';
import moment from 'moment';
const firebase = require("firebase");
// async function registerPushNotificationService() {
//
//   const { status: existingStatus } = await Permissions.getAsync(
//     Permissions.NOTIFICATIONS
//   );
//   let finalStatus = existingStatus;
//
//   // only ask if permissions have not already been determined, because
//   // iOS won't necessarily prompt the user a second time.
//   if (existingStatus !== 'granted') {
//     // Android remote notification permissions are granted during the app
//     // install, so this will only ask on iOS
//     const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
//     finalStatus = status;
//   }
//
//   // Stop here if the user did not grant permissions
//   if (finalStatus !== 'granted') {
//     return;
//   }
//
//   // Get the token that uniquely identifies this device
//   let token = await Notifications.getExpoPushTokenAsync();
//
//   return token;
// }

//import registerForPushNotificationsAsync from 'registerForPushNotificationsAsync';

let schedulingOptions = {
      time: Date.now() , // (date or number) — A Date object representing when to fire the notification or a number in Unix epoch time. Example: (new Date()).getTime() + 1000 is one second from now.


  };
  let inter = undefined;
  let count=[];
  const localNotification = {
      title: 'PWF',
      body: 'Please Check in', // (string) — body text of the notification.
      ios: { // (optional) (object) — notification configuration specific to iOS.
          sound: true // (optional) (boolean) — if true, play a sound. Default: false.
      },
      android: // (optional) (object) — notification configuration specific to Android.
      {
          sound: true, // (optional) (boolean) — if true, play a sound. Default: false.
          //icon (optional) (string) — URL of icon to display in notification drawer.
          icon: '../../images/blue_logo_icon.png',
          //color (optional) (string) — color of the notification icon in notification drawer.
          priority: 'high', // (optional) (min | low | high | max) — android may present notifications according to the priority, for example a high priority notification will likely to be shown as a heads-up notification.
          sticky: false, // (optional) (boolean) — if true, the notification will be sticky and not dismissable by user. The notification must be programmatically dismissed. Default: false.
          vibrate: true // (optional) (boolean or array) — if true, vibrate the device. An array can be supplied to specify the vibration pattern, e.g. - [ 0, 500 ].
          // link (optional) (string) — external link to open when notification is selected.
      }
  };
  async function getiOSNotificationPermission() {
  const { status } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  if (status !== 'granted') {
    await Permissions.askAsync(Permissions.NOTIFICATIONS);
  }
}

let newMessageEntryID =[];
class DashboardContainer extends NavComponent {
    // static navigationOptions = ({navigation}) => ({
    //     title: '',
    //     headerStyle: {
    //         position: 'absolute',
    //         backgroundColor: 'transparent',
    //         zIndex: 100,
    //         top: 0,
    //         left: 0,
    //         right: 0,
    //         borderBottomWidth: 0
    //     },
    //     headerTitleStyle: styles.headerTitleStyle
    // });  //commented out because header ber is showing in android device
    static navigationOptions = ({navigation}) => ({
      header:null
    });

    constructor(props, context) {
        super(props, context);
        this.state = {
           timeIcon:'sun',
           userTime:20,
           needHelp:true,
           appState: AppState.currentState,
           firstLogin:false,
           visibleModal:true,
          // notification: {},
        };
    }

    notiedInterval() {
      let self = this;

      AsyncStorage.getItem('DayEndTimeStamp').then((value) =>{
          if(value){
            AsyncStorage.getItem('NextDayEndTime').then((nextTime) =>{
                if(nextTime){
                  let deviceDate =new Date();
                  if(moment(deviceDate).format('x') >= value ){
                       //console.log('hi');
                       AsyncStorage.setItem('DeviceTime','sun');
                       this.setState({timeIcon:'sun'});
                       AsyncStorage.setItem('DayEndTimeStamp',nextTime);
                       AsyncStorage.setItem('NextDayEndTime',moment(parseInt(nextTime)).add(1,'days').format('x'));
                       Notifications.cancelAllScheduledNotificationsAsync();
                       if(inter !=undefined){
                         clearInterval(inter);
                         inter = undefined;
                     }
                   }
                }
            });
          }
        });
      AsyncStorage.getItem('DeviceTime').then((value) => {
        if (value == 'moon') {
            AsyncStorage.getItem('CheckedIn').then((val) => {
                if (val == 'no') {
                    if(inter == undefined && self.state.userTime > 0){
                      // AsyncStorage.setItem('CheckedInScreen','yes');
                    //  console.log('through',inter,)
                     inter = setInterval(() => {
                       Notifications.scheduleLocalNotificationAsync(localNotification, {time:Date.now(),});
                      }, self.state.userTime);
                    }
                } else {
                    clearInterval(inter);
                    inter = undefined;
                    AsyncStorage.setItem('CheckedIn','no');
                    // AsyncStorage.setItem('CheckedInScreen','no');
                  //  Notifications.dismissAllNotificationsAsync();
                    Notifications.cancelAllScheduledNotificationsAsync();
                }
                self.notiedInterval();
            });
        }else{
          if(inter !=undefined){
            clearInterval(inter);
            inter = undefined;
          }
          self.notiedInterval();
        }
      });
    }

    checkStartDay() {
      let self = this;

      AsyncStorage.getItem('DayEndTimeStamp').then((value) =>{
          if(value){
            AsyncStorage.getItem('NextDayEndTime').then((nextTime) =>{
                if(nextTime){
                  let deviceDate =new Date();
                  if(moment(deviceDate).format('x') >= value ){
                       //console.log('hi');
                       AsyncStorage.setItem('DeviceTime','sun');
                       this.setState({timeIcon:'sun'});
                       AsyncStorage.setItem('DayEndTimeStamp',nextTime);
                       AsyncStorage.setItem('NextDayEndTime',moment(parseInt(nextTime)).add(1,'days').format('x'));
                       Notifications.cancelAllScheduledNotificationsAsync();
                       if(inter !=undefined){
                         clearInterval(inter);
                         inter = undefined;
                     }
                   }
                }
            });
          }
        });
    }

    fireNotification() {
      let self = this;
      AsyncStorage.getItem('DeviceTime').then((value) => {
        if (value == 'moon') {
            AsyncStorage.getItem('CheckedIn').then((val) => {
                if (val == 'yes') {
                    if(self.state.userTime > 0){

                      AsyncStorage.getItem('DayEndTimeStamp').then((value) =>{
                          if(value){
                            if(value >(moment(Date.now() +self.state.userTime).format('x')))
                            {
                              Notifications.cancelAllScheduledNotificationsAsync();
                              Notifications.scheduleLocalNotificationAsync(localNotification, {time:Date.now() + self.state.userTime});
                            }
                            else{
                              Notifications.cancelAllScheduledNotificationsAsync();

                            }
                          }
                        })

                    }
                }
            });
        }
      });
    }

  //   _handleNotification = (notification) => {
  //   this.setState({notification: notification});
  //   console.log('origin',this.state.notification.origin)
  // };
    componentWillAppear()  {
         // Screen will entered
         console.log('Screen will entered');
    }
    componentWillMount() {
      getiOSNotificationPermission();
      //  registerForPushNotificationsAsync();
      // this._notificationSubscription = Notifications.addListener(this._handleNotification);
      AsyncStorage.getItem('DeviceTime').then((value) =>{
        if(value){
            if(value == 'sun'){
           this.setState({timeIcon:value});
             Notifications.cancelAllScheduledNotificationsAsync();
             // AsyncStorage.setItem('CheckedInScreen','no');
          }
          else{
            this.setState({timeIcon:'moon'});
            // AsyncStorage.getItem('CheckedInScreen').then((val) => {
            //     if (val == 'yes') {
            //       this.resetRoute("AskToCheckIn");
            //     }
            //     else{
            //       //console.log('new page');
            //       this.resetRoute("Dashboard");
            //     }
            //   });
          }
        }
      });
      AsyncStorage.getItem('CheckInTime').then((value) => {
        if (value) {
            if (value == '00:30') {
                this.setState({
                    userTime: 30 * 60000
                });

            } else {
              //  console.log("Number value", Number(value) * 60000);

                this.setState({
                    userTime: Number(value) * 60000
                });
            }
        }
        else{
         if(value == null){
            AsyncStorage.setItem('CheckInTime','00:30');
            this.setState({
                userTime: 30 * 60000
            });
          }
        }

    });
      //this.notiedInterval(); used for interval notification closed on https://cn.teamwork.com/#tasks/16018500?c=6700372
      this.fireNotification();
      inter = setInterval(() => {
      this.checkStartDay();
      }, 2000);


      Notifications.addListener((receivedNotification) => {
        let self = this;
        //console.log('receivedNotification',receivedNotification);
        count.push(1);
        if(count.length ==1){
            setTimeout(function(){
              count=[];
            },10000);
            if((receivedNotification.origin =='selected' || receivedNotification.origin === 'received') && receivedNotification.remote != true){
              self.props.navigation.navigate('AskToCheckIn');
            }

            if(receivedNotification.remote ==true){
              //console.log('receivedNotification',receivedNotification);
              if(receivedNotification.data !=null){
                newMessageEntryID.push(receivedNotification.data.entry_id);
              }
              AsyncStorage.setItem('newMessageEntryID',JSON.stringify(newMessageEntryID));
              AsyncStorage.setItem('NewMessage','yes');
              // Deep Linking to listing page as per client feedback..///
              self.props.navigation.navigate('SharedWithOthers');
            }
        }

      //   if (receivedNotification.origin === 'received' && Platform.OS === 'ios') {
      //     this.props.navigation.navigate('AskToCheckIn');
      // }
      // this.setState({
      //   lastNotificationId: receivedNotification.notificationId,
      // });
    });
      AsyncStorage.getItem('firstLogin').then((value) => {
        //console.log('firstLogin',value)
        if (value=='yes') {
            this.setState({firstLogin:true});
        }
        else{
          this.setState({firstLogin:false});
        }
    });
     //code commented out as I am working
    // AsyncStorage.getItem('@pwf/user').then((value) =>{
    //   if(value){
    //     user =JSON.parse(value);
    //     let firebasePassword ='firebase';
    //     let email=user.email
    //
    //     firebase.auth()
    //         .signInWithEmailAndPassword(email, firebasePassword);
    //
    //     }
    //   });
    }



    async componentDidMount() {
         AppState.addEventListener('change', this._handleAppStateChange);
        // let result = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        // if (Constants.lisDevice && result.status === 'granted') {
        // // console.log('Notification permissions granted.')
        // }
    }

    componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

    _handleAppStateChange = (nextAppState) => {
      if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
          //console.log('App has come to the foreground!',moment(deviceDate).format('x'));
          AsyncStorage.getItem('DayEndTimeStamp').then((value) =>{
              if(value){
                // let  today = moment();
                // let  tomorrow = today.add('days', 1);
                // let  nextDayEnd=moment(tomorrow).format('x');
                 // console.log(moment(deviceDate).format('x'));

                  AsyncStorage.getItem('NextDayEndTime').then((nextTime) =>{
                      if(nextTime){
                     //console.log(new Date(parseInt(nextTime)));
                    // console.log(value);
                    // console.log(moment(new Date(nextTime)).add(1,'days').format('x'));
                      let currentDeviceTime = new Date();
                      if(moment(currentDeviceTime).format('x') >= value){
                        //console.log('hello');
                        this.setState({timeIcon: 'sun'});
                        AsyncStorage.setItem('DeviceTime','sun');
                        AsyncStorage.setItem('DayEndTimeStamp',nextTime);
                        AsyncStorage.setItem('NextDayEndTime',moment(parseInt(nextTime)).add(1,'days').format('x'));
                      }
                    }
                  });
                }
                //moment(deviceTime).format('x')
              });

      }
      this.setState({appState: nextAppState});
     //  if (this.state.appState.match(/inactive|background/)){
     //
     //      Notifications.scheduleLocalNotificationAsync(localNotification, {time:new Date(Date.now() + this.state.userTime)});
     //
     //   // Schedule a notification
     //   //this.notiedInterval();
     //
     //  //  Notifications.scheduleLocalNotificationAsync(localNotification, {time:new Date(Date.now() + (3 * 1000))});
     // //   Notifications.scheduleLocalNotificationAsync({
     // //     message: 'Scheduled delay notification message', // (required)
     // //     date: new Date(Date.now() + (3 * 1000)) // in 3 secs
     // //   });
     //  }
    }


    onRhythmPress = () => {
        this.props.navigation.navigate('Rhythm');
    };

    onSettingsPress = () => {
        this.props.navigation.navigate('Settings');
    };

    onVideosPress = () => {
      //  this.props.navigation.navigate('AskToCheckIn');
      this.props.navigation.navigate('Videos');
    }

    onSharePress = () => {
        this.props.navigation.navigate('Connect');
    }

    onFoodPress = () => {
       this.props.navigation.navigate('Eat');
    }
    onChartsPress = () => {
       this.props.navigation.navigate('Charts');
    }
    onEveningPress=()=>{
       this.props.navigation.navigate('DayEnd');
    }
    onMorningPress=()=>{
       AsyncStorage.setItem('DeviceTime','moon');
       AsyncStorage.setItem('DayEndTimeDone','false');
       AsyncStorage.setItem('CheckedIn','yes');
       this.setState({timeIcon:'moon'});
       this.fireNotification();
      // Notifications.scheduleLocalNotificationAsync(localNotification, schedulingOptions);
      AsyncStorage.getItem('DayEndTimeStamp').then((value) =>{
        if(value == null){
             AsyncStorage.setItem('NextDayEndTime',moment().add(1,'days').endOf('day').format('x'));
             AsyncStorage.setItem('DayEndTimeStamp',moment().endOf('day').format('x'));
          }
        });




    }
    onNeedHelpPress=()=>{
       this.setState({needHelp:false});
    }
    onClosePress=()=>{

      AsyncStorage.setItem('firstLogin','false');
      this.setState({firstLogin:false});
    }
    onModalClose = () =>{

      this.setState({ visibleModal: false});
      AsyncStorage.setItem('firstLogin','false');
      this.setState({firstLogin:false});
    }

    render() {
        return <DashboardView
            {...this.state}
            onRhythmPress={this.onRhythmPress}
            onSettingsPress={this.onSettingsPress}
            onVideosPress={this.onVideosPress}
            onSharePress={this.onSharePress}
            onFoodPress={this.onFoodPress}
            onChartsPress={this.onChartsPress}
            onEveningPress={this.onEveningPress}
            onMorningPress={this.onMorningPress}
            onNeedHelpPress={this.onNeedHelpPress}
            onClosePress={this.onClosePress}
            onModalClose={this.onModalClose}
            {...this.props}
        />;
    }
}

const mapStateToProps = (state) => ({


});

export default connect(mapStateToProps, actions)(DashboardContainer);
