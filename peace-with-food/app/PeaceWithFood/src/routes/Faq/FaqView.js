import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    TextInput,
    WebView,
    ActivityIndicator

} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

//import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';
import {NavigationActions} from 'react-navigation';

const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:(height/22),
        backgroundColor: '#edf1f2'
    },
    section: {
      //  flex: 1
    },
    bottomSection: {
        backgroundColor: '#edf1f2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? 80 : 25),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#009fc6',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,
        //marginTop:height/6,
        alignItems: 'center',
        justifyContent:'center',
        flexDirection:'column'
      },
      answer:{
        flex:0.9,
        color:'white',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#a2a4a5',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: '#ebf6f9',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
      checkPanel:{
        // alignItems: 'center',
        // justifyContent:'center',
        paddingTop:20,
        paddingLeft:20,
        // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
        flexDirection:'row'
      },
      sharedBlock:{
        padding:(Platform.OS === 'ios' ? 40 : 10),
        alignItems: 'center',
        justifyContent:'center',
        marginTop:(Platform.OS === 'ios' ? 30 : 10),
        height:150,
        width:width/1.1,
        flexDirection:'row',
        flexWrap: "wrap"
      },
      timeText:{
        color:'white',
        fontSize:25,
        fontWeight:'500',
      },
      borderPanel:{
        //height:height/1.2,
        marginTop:(Platform.OS === 'ios' ? height/40 : height/40),
        marginBottom:(Platform.OS === 'ios' ? 10 : 5),
        marginLeft:15,
        marginRight:15,
        // borderBottomWidth:2,
        // borderBottomColor:'#2799bb',
        // borderTopWidth:2,
        // borderTopColor:'#2799bb'
      },


});



class FaqView extends Component {

  constructor(props){
    super(props);
    this.state ={
      support:false,
    }

  }

  componentWillMount(){
    if(this.props.navigation.state.params.support){
      this.setState({
        support:this.props.navigation.state.params.support
      })
    }
  }



  render (){
    let {
       onHomePress,
    } = this.props;
    return (<View style={styles.container}>
        <StatusBar/>
        <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>
          <View style={localStyle.borderPanel}>
            <View style={[localStyle.section, localStyle.bottomSection]}>
                <ScrollView contentContainerStyle={localStyle.sharePanel}>
                {!this.state.support && (
                    <WebView
                          style={{height:(Platform.OS === 'ios' ? height : height/1.3),width:width}}
                          javaScriptEnabled={true}
                          startInLoadingState={true}
                          renderLoading={()=>{return(<ActivityIndicator size="large" color="#00BDED" style = {{left: 0,right: 0, top: 0,bottom: 0,alignItems: 'center',justifyContent: 'center'}}/>)}}
                          source={{uri:'https://cms.hellopeacewithfood.com/faq'}}
                    />
                )}
                {this.state.support && (
                    <WebView
                          style={{height:(Platform.OS === 'ios' ? height : height/1.3),width:width}}
                          javaScriptEnabled={true}
                          startInLoadingState={true}
                          renderLoading={()=>{return(<ActivityIndicator size="large" color="#00BDED" style = {{left: 0,right: 0, top: 0,bottom: 0,alignItems: 'center',justifyContent: 'center'}}/>)}}
                          source={{uri:'https://cms.hellopeacewithfood.com/support'}}
                    />
                  )  }
                    {/*<TouchableOpacity style={localStyle.homeButtonPanel}
                      onPress={onHomePress}>
                      <Image
                        source={Images.homeIcon}
                      />
                    </TouchableOpacity> */}
                </ScrollView>
            </View>
          </View>
        </KeyboardAwareScrollView>
    </View>
    );
  }

}

export default FaqView;
