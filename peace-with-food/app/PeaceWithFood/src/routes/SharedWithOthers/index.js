import React from 'react';
import moment from   'moment';
import {Text,TouchableOpacity,Image,Platform,
Dimensions,
View} from 'react-native';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import SharedWithOthersView from './SharedWithOthersView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
import {Images} from 'PeaceWithFood/src/components/icons';
import {NavigationActions} from 'react-navigation';
var {height, width} = Dimensions.get('window');

function pad( date ){

        if ( !date && date !== 0 )
             return date;
        if ( date >= 0  &&  date < 10 )
             return '0' + date;
        else
             return date;
}


class SharedWithOthersContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: <Text style={{color:"#2799bb"}}>Rhythms I&apos;ve Shared</Text>,
        headerLeft: <BackButton color='#2799bb' onPress={() =>
                navigation.dispatch(NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({routeName : 'Connect' })
                ]
              }))}/>,
        headerRight:<TouchableOpacity style={{marginRight:10,height:40,width:40,justifyContent:'center',alignItems:'center'}} onPress={() =>
                navigation.dispatch(NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({ routeName:'Dashboard' })
                ]
              }))
              }><Image source={Images.closeIcon}/></TouchableOpacity>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0,
            borderBottomColor:'#2799bb',

        },
        headerTitleStyle: {
            fontSize: 17,
            color: Colors.WHITE,
            alignSelf: 'center',
            textAlign: 'center',
            ...Platform.select({
              android: { width: width/1.8, },
            }),
        }
    });

    state = {
          visibleModal: false,
    };

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }
    onSharePress = (from_time, end_time, client_id, sharedUserEmail) =>{
        var from = moment( from_time*1000 );
        var to   = moment(  end_time*1000 );

        let selectedStartDate = from.format("YYYY-MM-DD");
        let selectedEndDate   =   to.format("YYYY-MM-DD");

        let labelString  =  from.format("M/D")
                            + "–" // n dash
                            + to.format("M/D");

        this.props.navigation.navigate( 'OthersSharedGraphChart',
                              {
                                 //dateObj: day,
                                 selectedStartDate: selectedStartDate,
                                 selectedEndDate:   selectedEndDate,
                                 userId:            client_id,
                                 labelString:       labelString,
                                 sharedUserEmail:   sharedUserEmail
                              });
    }


    render() {

        return <SharedWithOthersView
            {...this.state}
            onSharePress={this.onSharePress}
            onAddCoachesPress={this.onAddCoachesPress}

        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
});

export default connect(mapStateToProps, actions)(SharedWithOthersContainer);
