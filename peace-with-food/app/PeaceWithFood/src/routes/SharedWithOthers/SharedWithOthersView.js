import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    Alert,
    AsyncStorage

} from 'react-native';

import _         from 'underscore';
import moment    from 'moment';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';
import {connect} from 'react-redux';
import {actions as rhythmActions,selectors as rhythmSelectors} from 'PeaceWithFood/src/store/rhythm';


const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dce0e2'
    },
    section: {
      //  flex: 1
    },
    bottomSection: {
        backgroundColor: '#dce0e2',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? 70 : 50),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#a3a7a8',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,

        alignItems: 'center',
        justifyContent:'center',
        flexDirection:'column',

      },
      borderPanel:{
        height:height/1.2,
        marginTop:(Platform.OS === 'ios' ? height/10 : height/8),
        marginBottom:(Platform.OS === 'ios' ? 10 : 10),
        marginLeft:15,
        marginRight:15,
        // borderBottomWidth:2,
        // borderBottomColor:'#2799bb',
        borderTopWidth:2,
        borderTopColor:'#2799bb'
      },
      answer:{
        color:'#627078',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#a2a4a5',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: 'white',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
      checkPanel:{
        // alignItems: 'center',
        // justifyContent:'center',
        paddingTop:20,
        paddingLeft:20,
        // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
        flexDirection:'row'
      },
      sharedBlock:{
        //padding:40,
        alignItems: 'center',
        justifyContent:'space-between',
        marginTop:5,
        height:35,
        width:width/1.2,
        flexDirection:'row',
        flexWrap: "wrap",
        backgroundColor:'#eaeced',

      },
      timeText:{
        color:'#2799bb',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
        marginRight:5,
      },
      nameText:{
        color:'#447990',
        fontSize:18,
        fontWeight:'400',
        marginLeft:5,
      },
      boldNameText:{
        color:'#447990',
        fontSize:18,
        fontWeight:'600',
        marginLeft:5,
      }

});


let  removeArray=[];
class SharedWithOthersView extends Component {

  constructor(props, context) {
      super(props, context);
      this.state = {
         sharedInstances:[],
         newMessageEntryID:[],
         removedEntry:[],

      };
  }

  componentWillMount(){
        //console.log('componentWillMount ok');
        AsyncStorage.getItem('@pwf/user').then((value) =>{
            if(value){
              user =JSON.parse(value)
              this.setState({user_id:user.id});
              //
              // Populate shared rhythm data
              const promise = this.props.getOthersSharedRhythmInstances( user.id );
              const self    = this;
              promise.then(function(instances){
                self.setState({sharedInstances:instances});
              });
            }
        });

        AsyncStorage.getItem('newMessageEntryID').then((value) =>{
            if(value){
              //console.log("newMessvalue",JSON.parse(value));
              let entries=JSON.parse(value)
              this.setState({newMessageEntryID:entries});
            }
            else{
                //console.log("newMessvaluepaini");
            }
        });
        AsyncStorage.getItem('removedSharedEntry').then((value) =>{
            if(value){
              //console.log("newMessvalue",JSON.parse(value));
              let entries=JSON.parse(value);
              for(i=0;i<entries.length;i++){
              removeArray.push(entries[i]);
              }
              this.setState({removedEntry:entries});
            }
            else{
                //console.log("newMessvaluepaini");
            }
        });
  }
  componentWillReceiveProps(nextProps) {
        //console.log('componentWillReceiveProps ok',nextProps.getSharedInstances.data);
  }

  componentWillUnmount(){
    AsyncStorage.setItem('removedSharedEntry',JSON.stringify(this.state.removedEntry));
  }

  deleteRhythm(entry_id, coach_id) {
    let  self    = this;

    removeArray.push(entry_id);
    this.setState({removedEntry:removeArray});
    Alert.alert("", "Rhythm removed successfully" );
    // const removeRhythm = this.props.deleteOthersSharedRhythmInstance(entry_id, coach_id);
    // removeRhythm.then(function(data){
    //     console.log(data);
    //     if(data && data == 204){
    //       Alert.alert("", "Rhythm removed successfully" );
    //       const promise = this.props.getOthersSharedRhythmInstances( user.id );
    //       promise.then(function(instances){
    //         self.setState({sharedInstances:instances});
    //       });
    //     }
    //     else{
    //       Alert.alert('Please try again', "Something went wrong" );
    //     }
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //     Alert.alert('Please try again', "Something went wrong" );
    //   });
 }

  render (){
    let {

        onSharePress,
        onAddCoachesPress,
        onShareWithCoachPress,
        visibleModal,
        onModalClose
    } = this.props;

    function titleCase( string ){
        if ( ! string )
               return string;
        string = string + "";
        string = string.trim();
        string = (string.charAt(0).toUpperCase() + string.slice(1));
        trimmedString = string.length > 10 ?
        string.substring(0, 10 - 3) + ".." :
        string
        return trimmedString;
    }

    var self = this;
    // console.log("instance, count",this.state.sharedInstances);
    var blocks = _.map( this.state.sharedInstances, function( instance, count ){
      //console.log("instance, count",instance, count);
        var client_name =   titleCase( instance.coach.full_name||'' )
                         || titleCase( instance.coach.first_name||'' )
                            + " " + titleCase( instance.coach.last_name||'' );
            client_name = client_name.trim();
        moment.updateLocale('en', {
            relativeTime : {
                future: "in %s",
                past:   "%s",
                s:  "just now",
                ss: "just now",
                m:  "just now",
                mm: "%d minutes ago",
                h:  "1 hour ago",
                hh: "1 d", // TODO review this one
                d:  "1 d",
                dd: "%d d",
                M:  "1 m",
                MM: "%d m",
                y:  "1 year",
                yy: "%d years"
            }
        });
        var date       = moment( instance.created_epoch * 1000 );
        var dateString = date.fromNow();
        if ( date.isSame(new Date(), "day") && dateString != 'just now' ){ // if today
              dateString = "today";
        }

        return !_.contains(self.state.removedEntry, instance.id) ?
         (
            <TouchableOpacity
                   onPress={() => onSharePress(instance.from_epoch, instance.to_epoch, instance.client_id, instance.coach.email)}
                   key={count}>
            <View  style={localStyle.sharedBlock}
                >
                { _.contains(self.state.newMessageEntryID, instance.id) ? (<View style={{position:'absolute',...Platform.select({
                  android: { top: -10, },
                }), left:(Platform.OS === 'ios' ? -22 : -10), height:10, width:10, backgroundColor:'orange', borderRadius:50,margin:10,alignItems:'center'}}></View> )
                : null }               
               {/*.......AS PER CLIENT'S REQUEST REMOVING PROFFESIONAL USER ROLE.......*/}
               {/* <Text style={(instance.coach.role == 1) ? localStyle.nameText : localStyle.boldNameText}> */}
               <Text style={localStyle.nameText}>
                 { client_name }
                 { " " }
                 { " " }
                 <Text style={localStyle.timeText}>
                 { instance.title }
                 </Text>
               </Text>
               <Text style={[localStyle.timeText,{color:'#a4a6a8'}]}>
                 { dateString }
               </Text>
               <TouchableOpacity onPress={self.deleteRhythm.bind(self,instance.id,instance.coach_id)} style={{width:50,height:50,justifyContent:'center',alignItems:'center'}}>
                <Image source={Images.orangeCloseIcon} />
               </TouchableOpacity>

            </View>
            </TouchableOpacity>
        )
        :null
    });
    //console.log('blocks',blocks);


    return (<View style={styles.container}>
        <StatusBar/>
        <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>

            <View style={[localStyle.section, localStyle.bottomSection]}>
             <View style={localStyle.borderPanel}>
              <ScrollView contentContainerStyle={localStyle.sharePanel}>
                    { blocks }
              </ScrollView>
             </View>
                {/*<View style={localStyle.homeButtonPanel}>
                    <HomeButton
                        onPress={onSharePress}
                        style={localStyle.buttonStyle}
                        loadingColor='#07c4f0'
                        primary
                    />
                </View>*/}

            </View>
        </KeyboardAwareScrollView>
    </View>
    );
  }

}

const mapStateToProps = (state, props) => ({
  //getSharedInstances:rhythmSelectors.getSharedInstances
});


export default connect(mapStateToProps, {
    ...rhythmActions
})(SharedWithOthersView);
