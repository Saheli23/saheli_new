import React from 'react';
import {Text,Image,TouchableOpacity,StyleSheet} from 'react-native';
import Validator from 'validatorjs';
import NavComponent from 'PeaceWithFood/src/containers/NavComponent';
import NotAllowToEatView from './NotAllowToEatView';
import {BackButton} from 'PeaceWithFood/src/components/form/buttons';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {actions, selectors} from 'PeaceWithFood/src/store/auth';
import styles from 'PeaceWithFood/src/styles';
import { Colors } from 'PeaceWithFood/src/styles/constants';
import {Images} from 'PeaceWithFood/src/components/icons';
import {NavigationActions} from 'react-navigation';
let routeName='Dashboard';
const localStyles = StyleSheet.create({
    button: {
        flex: 1,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

class NotAllowToEatContainer extends NavComponent {
    static navigationOptions = ({navigation}) => ({
        title: '',
        headerLeft: <TouchableOpacity
            style={localStyles.button}
            onPress={() => navigation.dispatch({type: "Navigation/BACK"})}
        >
            <Image
                source={Images.whiteBackIcon}
            />
        </TouchableOpacity>,
        headerRight:<TouchableOpacity style={{marginRight:10,height:40,width:40,justifyContent:'center',alignItems:'center'}} onPress={() =>
            navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName })
            ]
          }))}><Image source={Images.whiteCloseIcon}/></TouchableOpacity>,
        headerStyle: {
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTitleStyle: styles.headerTitleStyle
    });

    state = {
          visibleModal: false,
          healthState:''
    };

    componentDidMount() {

    }

    componentWillMount() {
         this.setState({healthState:this.props.navigation.state.params.healthState});
    }

    componentWillReceiveProps(nextProps) {

    }
    onHomePress = () =>{
      this.resetRoute("Dashboard");
    }

    onShareWithCoachPress = () =>{

      this.props.navigation.navigate('Meal');
    //  this.setState({ visibleModal: true})
    }

    onModalClose = () =>{
      this.setState({ visibleModal: false})
    }


    render() {

        return <NotAllowToEatView
            {...this.state}
            onHomePress={this.onHomePress}
            onAddCoachesPress={this.onAddCoachesPress}
            onShareWithCoachPress={this.onShareWithCoachPress}
            onModalClose={this.onModalClose}
            {...this.props}


        />;
    }
}

const mapStateToProps = (state, props) => ({
    // isLoggedIn: selectors.getIsLoggedIn(state),
    // isLoggingIn: selectors.getIsLoggingIn(state),
    // error: selectors.getError(state),
});

export default connect(mapStateToProps, actions)(NotAllowToEatContainer);
