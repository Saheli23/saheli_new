import React ,{Component} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Platform,
    ScrollView,
    TextInput

} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

//import TextInput from 'PeaceWithFood/src/components/form/textinput';
import { TextButton, BackButton } from 'PeaceWithFood/src/components/form/buttons';
import { LogoIconBlue } from 'PeaceWithFood/src/components/icons';
import {HomeButton} from 'PeaceWithFood/src/components/form/buttons';
import styles from 'PeaceWithFood/src/styles';
var {height, width} = Dimensions.get('window');
import {Images} from 'PeaceWithFood/src/components/icons';
import Modal from 'react-native-modal';
import CheckBox from 'react-native-check-box';

const localStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F17D42'
    },
    section: {
        flex: 1
    },
    bottomSection: {
        backgroundColor: '#F17D42',
    },
    topSection: {
        justifyContent: 'center'
    },
    logoPanel:{
      marginTop:height/5,
      alignItems:'center'
    },
    form: {
        justifyContent: 'center',
      //  flex: 1
        marginTop:(Platform.OS === 'ios' ? 80 : 30)
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 32,
        paddingRight: 32
    },
    inputText: {
        backgroundColor: '#e8e9ea',
        borderBottomColor:'#2799bb',
        borderLeftColor:'#d4d5d6',
        borderRightColor:'#d4d5d6',
        borderTopColor:'#d4d5d6',
        borderWidth:2,
        color:'#2799bb'
    },
    logoText: {
        textAlign: 'center',
        fontSize: 30,
        marginTop:height/5,
        color:'#2799bb'
    },
    buttonText: {
        color: '#7ec2d6',
        fontSize: 18
    },
    tipsPanel:{
      alignItems: 'center',
      justifyContent:'center',
      flexDirection:'row',
      paddingTop:(Platform.OS === 'ios' ? 40 : 20),
      paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
    },
    homeButtonPanel:{
        marginTop:(Platform.OS === 'ios' ? height/7 : height/7),
        height:45,
        width:width/1.3,
        alignItems: 'center',
        justifyContent:'center',
        paddingLeft:width/5
    },
    buttonStyle:{
        backgroundColor:'#4cc5e7',
        borderRadius:50,
        shadowColor: '#009fc6',
        borderTopWidth:0,
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        width:width/1.3,
      },
      sharePanel:{
        //height: 300,
        //paddingLeft:30,
        //paddingRight:30,
        marginTop:height/5,
        alignItems: 'center',
        justifyContent:'center',
        flexDirection:'column'
      },
      answer:{
        flex:0.9,
        color:'white',
        fontSize:16,
        //marginTop:10
      },
      socialIcon:{
        marginLeft:20
      },
      socialPanel:{
        flexDirection:'row',
        marginTop:20,
        marginBottom:20,
      },
      shareBox:{
        flexDirection:'column',
        alignItems:'center'
      },
      iconText:{
        color:'#a2a4a5',
        //marginTop:15,
        fontSize:16,
        fontWeight:'400'
      },
      modalContent: {
        backgroundColor: '#ebf6f9',
        padding: 22,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',

  },
      checkPanel:{
        // alignItems: 'center',
        // justifyContent:'center',
        paddingTop:20,
        paddingLeft:20,
        // paddingBottom: (Platform.OS === 'ios' ? 40 : 20),
        flexDirection:'row'
      },
      sharedBlock:{
        padding:(Platform.OS === 'ios' ? 10 : 0),
        // alignItems: 'center',
        // justifyContent:'center',
        marginTop:(Platform.OS === 'ios' ? 50 : 10),
        height:150,
        width:width/1.1,
        flexDirection:'row',
        flexWrap: "wrap",

      },
      timeText:{
        textAlign:'center',
        color:'white',
        fontSize:(Platform.OS === 'ios' ? 30 : 25),
        marginTop:(Platform.OS === 'ios' ? 0 : -50),
        //fontWeight:'500',
        fontFamily: 'Sarabun-Regular'
      },


});



class NotAllowToEatView extends Component {

  constructor(props){
    super(props);
    this.state = {
        feeling:''
    }

    this.intervalVar = null;
  }

  componentWillUnmount(){
    console.log(this.intervalVar);
    clearInterval(this.intervalVar);
  }

  componentDidMount(){
    let healthStateArray=[['Famished', 'Starving', 'Very Hungry'],[ 'Ready', 'Neutral'],['Full & Satisfied', 'Overly Full', 'Stuffed', 'Sick']];
    let feelings=[['Breathe deep', 'Go extra slow', 'Feel the food hitting your tummy','Relax','Be mindful'],['Taste','Go slowly','Remember: food is a gift','Be present'],['Can you allow time to pass?','The food will taste so good when you are ready','What are you really hungry for?']];
    let getChangeVal = feelings[healthStateArray.map((e)=>{
                   return e.indexOf(this.props.healthState) > -1;
                }).indexOf(true)];
    let i = 0;
    let method = ()=>{
        i = i < getChangeVal.length && i || 0;
        this.setState(()=>{return {feeling:getChangeVal[i]}}, ()=>{i++});
    }
        this.intervalVar = setInterval(method, 2500);
  }



  render (){
    let {

        onHomePress,
        onAddCoachesPress,
        onShareWithCoachPress,
        healthState


    } = this.props;
    return (<View style={styles.container}>
        <StatusBar/>
        <KeyboardAwareScrollView style={localStyle.container} contentContainerStyle={localStyle.container}>
          <View style={[localStyle.section, localStyle.bottomSection]}>
              <View style={localStyle.sharePanel}>
                    <Image
                      source={Images.crossSpoon}
                    />
                    <View style={localStyle.sharedBlock}>
                       <Text style={localStyle.timeText}>Looks like you are not ready to eat quite yet.</Text>
                       <Text style={localStyle.timeText}>Allow time to pass...you will enjoy the food so much more when you are ready.
			</Text>
                    </View>
                  </View>
                <TouchableOpacity style={localStyle.homeButtonPanel}
                  onPress={onHomePress}>
                  <Image
                    source={Images.homeIcon}
                  />
                </TouchableOpacity>

            </View>
        </KeyboardAwareScrollView>
    </View>
    );
  }

}

export default NotAllowToEatView;
