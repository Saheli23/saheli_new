import lang from 'validatorjs/src/lang';
import en from 'validatorjs/src/lang/en';
lang._set('en', en);

import MainComponent from './src/main';
export default MainComponent;
