var stf = angular.module('UploadControllers', []);

stf.controller('ProfilePictureUploadController', ['$scope', '$location', '$routeParams', '$rootScope', 'Team', 'Player', 'User', 'Upload', 'STFcache', 'Auth', function ($scope, $location, $routeParams, $rootScope, Team, Player, User, Upload, STFcache, Auth) {
	
	var owners = ['tea_', 'pla_', 'use_'];
	var owner = $routeParams.owner;
	var id_uniq = $routeParams.uniqid;
	var recentlyCreated = $routeParams.recently_created || undefined;
	var redirectURL;
	var cacheToDelete;

	$scope.canUpload = false;
	$scope.formdata = {
		owner: owner,
		id_uniq: id_uniq,
		userfile: '',
		xaxis: '',
		yaxis: '',
		width: '',
		height: ''
	};

	$scope.playerdata = {
		id_uniq: id_uniq,
		head: {
			file: '',
			xaxis: '',
			yaxis: '',
			width: '',
			height: ''
		},
		body: {
			file: '',
			xaxis: '',
			yaxis: '',
			width: '',
			height: ''
		}
	};

	$scope.getTeam = function (id_uniq) {

		$scope.loadingOwner = true;
		$scope.loadingOwnerAlert = false;

		Team.getTeam(id_uniq).then(function (res) {

			$scope.loadingOwner = false;

			if (angular.isObject(res.data.response)) {

				$scope.canUpload = true;
				$scope.isTeam = true;
				$scope.team = res.data.response;

			} else {

				$scope.loadingOwnerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingOwner = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingOwnerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingOwner = true;
		$scope.loadingOwnerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			$scope.loadingOwner = false;

			if (angular.isObject(res.data.response)) {

				$scope.isPlayer = true;
				$scope.player = res.data.response;

			} else {

				$scope.loadingOwnerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingOwner = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingOwnerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	}

	$scope.getUser = function (id_uniq) {

		$scope.loadingOwner = true;
		$scope.loadingOwnerAlert = false;

		User.getOne(id_uniq).then(function (res) {

			$scope.loadingOwner = false;

			if (angular.isObject(res.data.response)) {

				$scope.canUpload = true;
				$scope.isUser = true;
				$scope.user = res.data.response;

			} else {

				$scope.loadingOwnerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingOwner = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingOwnerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	}

	if (_.indexOf(owners, owner) == -1) { $location.path('dashboard'); } else {

		switch (owner) {

			case 'tea_':
				$scope.getTeam(id_uniq);
				redirectURL = (recentlyCreated !== undefined && recentlyCreated == 1) ? 'teams' : 'teams/' + id_uniq;
				$rootScope.tooltipMessageAlert = (recentlyCreated !== undefined && recentlyCreated == 1) ? { type: 'success', message: 'Team created successfully' } : false;
				cacheToDelete = {
					single: 'teams' + id_uniq,
					all: 'teams'
				};
				break;
			case 'pla_':
				$scope.getPlayer(id_uniq);
				redirectURL = (recentlyCreated !== undefined && recentlyCreated == 1) ? 'players' : 'players/' + id_uniq;
				$rootScope.tooltipMessageAlert = (recentlyCreated !== undefined && recentlyCreated == 1) ? { type: 'success', message: 'Player created successfully' } : false;
				cacheToDelete = {
					single: 'players' + id_uniq,
					all: 'players'
				};
				break;
			case 'use_': 
				$scope.getUser(id_uniq);
				redirectURL = (recentlyCreated !== undefined && recentlyCreated == 1) ? 'users' : 'users/' + id_uniq;
				$rootScope.tooltipMessageAlert = (recentlyCreated !== undefined && recentlyCreated == 1) ? { type: 'success', message: 'User created successfully' } : false;
				cacheToDelete = {
					single: 'users' + id_uniq,
					all: 'users'
				};
				break;
			default:
				break;

		}

	}

	$scope.cancelUpload = function () {

		STFcache.delete(cacheToDelete.single);
		STFcache.delete(cacheToDelete.all);

		$location.url(redirectURL);

	};

	$scope.uploadProfilePicture = function () {

		$scope.uploadingPicture = true;
		$scope.uploadingPictureAlert = false;

		var data = new FormData();

		data.append('owner', $scope.formdata.owner);
		data.append('id_uniq', $scope.formdata.id_uniq);
		data.append('userfile', $scope.formdata.userfile);
		data.append('xaxis', $scope.formdata.xaxis);
		data.append('yaxis', $scope.formdata.yaxis);
		data.append('width', $scope.formdata.width);
		data.append('height', $scope.formdata.height);

		Upload.uploadProfilePicture(data).then(function (res) {

			$scope.uploadingPicture = false;

			if (res.data.status == 'success') {

				STFcache.delete(cacheToDelete.single);
				STFcache.delete(cacheToDelete.all);

				$location.url(redirectURL);

			} else {

				$scope.uploadingPictureAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err){

			$scope.uploadingPicture = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.uploadingPictureAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});	

	};

	$scope.uploadPlayerPictures = function () {

		$scope.uploadingPlayerPictures = true;
		$scope.uploadingPlayerPicturesAlert = false;

		var data = new FormData();

		data.append('id_uniq', $scope.playerdata.id_uniq);
		data.append('head[file]', $scope.playerdata.head.file);
		data.append('head[xaxis]', $scope.playerdata.head.xaxis);
		data.append('head[yaxis]', $scope.playerdata.head.yaxis);
		data.append('head[width]', $scope.playerdata.head.width);
		data.append('head[height]', $scope.playerdata.head.height);
		data.append('body[file]', $scope.playerdata.body.file);
		data.append('body[xaxis]', $scope.playerdata.body.xaxis);
		data.append('body[yaxis]', $scope.playerdata.body.yaxis);
		data.append('body[width]', $scope.playerdata.body.width);
		data.append('body[height]', $scope.playerdata.body.height);

		Upload.uploadPlayerPictures(data).then(function (res) {

			$scope.uploadingPlayerPictures = false;

			if (res.data.status == "success") {

				STFcache.delete(cacheToDelete.single);
				STFcache.delete(cacheToDelete.all);

				$location.url(redirectURL);

			} else {

				$scope.uploadingPlayerPicturesAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.uploadingPlayerPictures = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.uploadingPlayerPicturesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}])