var stf = angular.module('EventControllers', []);

stf.controller('EventListController', ['$scope', 'Event', 'Auth', 'User', 'Access', 'STFcache', function ($scope, Event, Auth, User, Access, STFcache) {
	
	$scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};



	$scope.getEvents = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('events');
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.events = null;
		$scope.loadingEvents = true;
		$scope.loadingEventsAlert = false;

		Event.getAll().then(function (res) {
			$scope.loadingEvents = false;
			if (angular.isObject(res.data.response)) {
				$scope.events = res.data.response;
				
			} else {
				$scope.loadingEventsAlert = { type: 'success', message: res.data.response };
			}
			$scope.getAccess(1);
		}, function (err) {
			$scope.loadingEvents = false;
			err.status == 401 ? Auth.deleteToken() : $scope.loadingEventsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
		});

	};

	$scope.getEvents();

	$scope.deleteEvent = function( event_id, ind ){

		if(confirm("Are you sure to delete this Event?")){
			var data = $.param({id_event:event_id,type:'delete'});
			Event.delete(data).then(function (res) {
				$scope.loadingEvents = false;
				$scope.getEvents(true);
				if (angular.isObject(res.data.response)) {
					//console.log(res.data.response);
				} else {
					$scope.loadingEventsAlert = {type: res.data.status,message: res.data.response};
					$location.path('/events');
				}

			}, function (err) {
				$scope.loadingEvents = false;
				err.status == 401 ? Auth.deleteToken() : $scope.loadingEventsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
			});
		}
	};

	$scope.bulkDeleteEvent = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Event?")){
			var data = $.param({id_event:values,type:'delete',action:action_id});
			Event.multidelete(data).then(function (res) {
				$scope.loadingEvents = false;
				$scope.getEvents(true);
				if (angular.isObject(res.data.response)) {
					//console.log(res.data.response);
				} else {
					$scope.loadingEventsAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {
				$scope.loadingEvents = false;
				err.status == 401 ? Auth.deleteToken() : $scope.loadingEventsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
			});
		}
	 }
	 else{
	 	alert("Please select atleast one Event to delete.");
	 }
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
			changeAttr( $scope.events, 'events', $scope.access.bo_view );
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		})
	}

	$scope.restrictView = function(){
		$('#alertMessage').show();
		setTimeout(function(){ 
			$('#alertMessage').hide();
		}, 2000);
	}

	function compile(element){
		var el = angular.element(element);    
		$scope = el.scope();
		$injector = el.injector();
		$injector.invoke(function($compile){
			$compile(el)($scope);
		})     
	}

	function changeAttr( obj, slug, viewAccess ){
        for( var i=0; i < obj.length; i++ ){
            var el = document.getElementById((slug.substring(0, slug.length-1))+"_id_"+i);
            if( viewAccess ){
                el.removeAttribute("ng-click");
                el.setAttribute("ng-href",slug+"/"+obj[i].id_uniq);
                el.setAttribute("href",slug+"/"+obj[i].id_uniq);
            } else {
                el.removeAttribute("ng-href");
                el.removeAttribute("href");
                el.setAttribute("ng-click", "restrictView()");
                compile(el);
            }
        }
    }


	$scope.orderOptions = [
		{ value: 'tx_name', text: 'Name' },
		{ value: '-dt_event', text: 'Date' }
	];

}]);

stf.controller('EventCreateController', ['$scope', '$timeout', '$rootScope', '$location', 'Auth', 'Venue', 'Team', 'Measurement', 'Station', 'Event', 'STFcache', function ($scope, $timeout, $rootScope, $location, Auth, Venue, Team, Measurement, Station, Event, STFcache) {
	var minDate = new Date();

	$scope.formdata = {
		name: '',
		date: '',
		venue: '',
		team: '',
		measurement_system: '',
		configurations: []
	};

	$scope.configurationdata = {
		station: '',
		test: '',
		age_range: '',
		distance: '0'
	};

	$scope.date = null;

	//$scope.minDate = new Date();
	$scope.minDate = new Date(minDate.setFullYear(minDate.getFullYear()));
	$scope.dateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: 100,
		selectMonths: true
	};
   
	$scope.getVenues = function () {

		$scope.loadingVenues = true;
		$scope.loadingVenuesAlert = false;

		Venue.getVenues().then(function (res) {
			$scope.loadingVenues = false;

			if (angular.isObject(res.data.response)) {

				$scope.venues = res.data.response;

			} else {

				$scope.loadingVenuesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingVenues = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingVenuesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTeams = function () {

		$scope.loadingEvents = true;
		$scope.loadingEventsAlert = false;

		Team.getTeams().then(function (res) {

			$scope.loadingEvents = false;

			if (angular.isObject(res.data.response)) {

				$scope.teams = res.data.response;
				
			} else {

				$scope.loadingEventsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvents = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getMeasurements = function () {

		$scope.loadingMeasurements = true;
		$scope.loadingMeasurementsAlert = false;

		Measurement.getMeasurements().then(function (res) {

			$scope.loadingMeasurements = false;

			if (angular.isObject(res.data.response)) {

				$scope.measurements = res.data.response;
				
			} else {

				$scope.loadingMeasurementsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingMeasurements = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingMeasurementsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getStationsAbleToConfigure = function () {

		$scope.loadingStationsAbleToConfigure = true;
		$scope.loadingStationsAbleToConfigureAlert = false;

		Station.getStationsAbleToConfigure().then(function (res) {

			$scope.loadingStationsAbleToConfigure = false;

			if (angular.isObject(res.data.response)) {

				$scope.stations = res.data.response;

			} else {

				$scope.loadingStationsAbleToConfigureAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStationsAbleToConfigure = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStationsAbleToConfigureAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAllStationConfigurations = function () {

		$scope.loadingConfigurations = true;
		$scope.loadingConfigurationsAlert = false;

		Station.getConfigurations().then(function (res) {

			$scope.loadingConfigurations = false;

			if (angular.isObject(res.data.response)) {

				$scope.configurations = res.data.response;
				$scope.$emit('configurationsLoaded');

			} else {

				$scope.loadingConfigurationsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingConfigurations = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingConfigurationsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getVenues();
	$scope.getTeams();
	$scope.getMeasurements();
	$scope.getStationsAbleToConfigure();
	$scope.getAllStationConfigurations();

	$scope.$on('configurationsLoaded', function () {

		$scope.$watch('configurationdata.station', function () {

			$scope.tests = [];

			$scope.configurationdata.test = '';
			$scope.configurationdata.age_range = '';
			$scope.configurationdata.distance = '0';

			$scope.ConfigurationForm.test.$setPristine();
			$scope.ConfigurationForm.age_range.$setPristine();
			
			angular.forEach($scope.configurations, function (value, key) {

				if (value.id_station == $scope.configurationdata.station) {

					if (_.findIndex($scope.tests, { id: value.id_test, name: value.test_name }) == -1) {

						$scope.tests.push({
							id: value.id_test,
							name: value.test_name
						});	

					};

				};

			});

		});

		$scope.$watch('configurationdata.test', function () {

			$scope.ages = [];

			$scope.configurationdata.age_range = '';
			$scope.configurationdata.distance = '0';

			$scope.ConfigurationForm.age_range.$setPristine();
			
			angular.forEach($scope.configurations, function (value, key) {

				if (value.id_station == $scope.configurationdata.station && value.id_test == $scope.configurationdata.test) {

					if (_.indexOf($scope.ages, value.nu_age) == -1) {

						if (_.findIndex($scope.ages, { id: value.id_age_range, name: value.nu_min_age + ' - ' + value.nu_max_age }) == -1) {

							$scope.ages.push({
								id: value.id_age_range,
								name: value.nu_min_age + ' - ' + value.nu_max_age
							});	

						};


					};

				};

			});

		});

		$scope.$watch('configurationdata.age_range', function () {

			angular.forEach($scope.configurations, function (value, key) {

				if (value.id_station == $scope.configurationdata.station && value.id_test == $scope.configurationdata.test && value.id_age_range == $scope.configurationdata.age_range) {

					$scope.configurationdata.distance = value.nu_distance;

				};

			});

		});

	});

	$scope.addConfiguration = function () {

		$scope.showTooltip = false;

		var indexToLookFor = {
			id_station: $scope.configurationdata.station,
			id_test: $scope.configurationdata.test,
			id_age_range: $scope.configurationdata.age_range,
			nu_distance: $scope.configurationdata.distance
		};

		var index = _.findIndex($scope.configurations, indexToLookFor);

		var dataToPush = {
			id: $scope.configurations[index].id,
			station: $scope.configurations[index].station_name,
			test: $scope.configurations[index].test_name,
			age_range: $scope.configurations[index].nu_min_age + ' - ' + $scope.configurations[index].nu_max_age,
			distance: $scope.configurations[index].nu_distance
		};

		if (_.findIndex($scope.formdata.configurations, dataToPush) == -1) {

			$scope.formdata.configurations.push(dataToPush);

			$scope.configurationdata = {
				station: '',
				test: '',
				age_range: '',
				distance: '0'
			};

			$scope.ConfigurationForm.$setPristine();

		} else {

			$scope.showTooltip = true;

			$timeout(function () {

				$scope.showTooltip = false;				

			}, 2000);

		}

	};	

	$scope.deleteConfiguration = function (index) {

		$scope.formdata.configurations.splice(index, 1);

	};

	$scope.createEvent = function () {

		$scope.creatingEvent = true;
		$scope.creatingEventAlert = false;

		var data = $.param($scope.formdata);

		Event.create(data).then(function (res) {

			$scope.creatingEvent = false;

			if (res.data.status == "success") {

				STFcache.delete('events');
				STFcache.delete('eventUniqIDs');

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Event created successfully'
				};

				$location.path('/events');

			} else {

				$scope.creatingEventAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('EventDetailsController', ['$scope', '$routeParams', '$location', 'Event', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, $location, Event, Access, Auth, STFcache ) {
	$scope.id_uniq = $routeParams.id_uniq;
	$scope.getUniqIDs = function () {

		$scope.loadingUniqIDs = true;
		$scope.loadingUniqIDsAlert = false;

		Event.getUniqIDs().then(function (res) {

			$scope.loadingUniqIDs = false;

			$scope.result = res.data.response;
			$scope.totalItems = parseInt($scope.result.length);
			$scope.currentItem = parseInt(_.findIndex($scope.result, { id_uniq: $scope.id_uniq }));
			$scope.nextItem = $scope.currentItem + 1;	
			$scope.prevItem = $scope.currentItem - 1;

			$scope.nextID = ($scope.nextItem > -1 && $scope.nextItem < $scope.totalItems) ? $scope.result[$scope.nextItem].id_uniq : false;
			$scope.prevID = ($scope.prevItem >= 0) ? $scope.result[$scope.prevItem].id_uniq : false;

		}, function (err) {

			$scope.loadingUniqIDs = false;

			if (err.status == 401) {

				Auth.deleteToken();

			};

		});

	};

	$scope.prevEvent = function () { $location.path('/events/' + $scope.prevID); };
	$scope.nextEvent = function () { $location.path('/events/' + $scope.nextID); };

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {
				$scope.event = res.data.response;
				$scope.getUniqIDs();
				$scope.$emit('eventLoaded');
				

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}
			$scope.getAccess(1);
		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getEvents = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('events');

		$scope.events = null;
		$scope.loadingEvents = true;
		$scope.loadingEventsAlert = false;

		Event.getAll().then(function (res) {
			$scope.loadingEvents = false;
			if (angular.isObject(res.data.response)) {
				$scope.events = res.data.response;
				$scope.getAccess(1);
			} else {
				$scope.loadingEventsAlert = { type: 'success', message: res.data.response };
			}

		}, function (err) {
			$scope.loadingEvents = false;
			err.status == 401 ? Auth.deleteToken() : $scope.loadingEventsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
		});

	};

	$scope.deleteEvent = function( event_id, ind ){

		if(confirm("Are you sure to delete this Event?")){
			var data = $.param({id_event:event_id,type:'delete'});
			Event.delete(data).then(function (res) {
				$scope.loadingEvents = false;
				$scope.getEvents(true);
				if (angular.isObject(res.data.response)) {
					//console.log(res.data.response);
				} else {
					$scope.loadingEventsAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {
				$scope.loadingEvents = false;
				err.status == 401 ? Auth.deleteToken() : $scope.loadingEventsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
			});
		}
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		})
	}

	$scope.$on('eventLoaded', function () {
		$scope.checkStation = function (station) {
			var configurations = $scope.event.configurations.length
			for (var i = 0; i < configurations; i++) {	
				if ($scope.event.configurations[i].station == station) { return true; }
			};

		};

	});

	$scope.getEvent($scope.id_uniq);

}]);

stf.controller('EventEditController', ['$scope', '$timeout', '$rootScope', '$location', '$routeParams', '$filter', 'Auth', 'Venue', 'Team', 'Measurement', 'Station', 'Event', 'STFcache', function ($scope, $timeout, $rootScope, $location, $routeParams, $filter, Auth, Venue, Team, Measurement, Station, Event, STFcache) {
	var minDate = new Date();
	var id_uniq = $routeParams.id_uniq;

	$scope.formdata = {
		name: '',
		date: '',
		venue: '',
		team: '',
		measurement_system: '',
		configurations: []
	};

	$scope.configurationdata = {
		station: '',
		test: '',
		age_range: '',
		distance: '0'
	};

	$scope.date = null;
	$scope.minDate = new Date(minDate.setFullYear(minDate.getFullYear()));
	$scope.dateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: 100,
		selectMonths: true
	};

	$scope.getVenues = function () {

		$scope.loadingVenues = true;
		$scope.loadingVenuesAlert = false;

		Venue.getVenues().then(function (res) {

			$scope.loadingVenues = false;

			if (angular.isObject(res.data.response)) {

				$scope.venues = res.data.response;

			} else {

				$scope.loadingVenuesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingVenues = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingVenuesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTeams = function () {

		$scope.loadingEvents = true;
		$scope.loadingEventsAlert = false;

		Team.getTeams().then(function (res) {

			$scope.loadingEvents = false;

			if (angular.isObject(res.data.response)) {

				$scope.teams = res.data.response;
				
			} else {

				$scope.loadingEventsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvents = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getMeasurements = function () {

		$scope.loadingMeasurements = true;
		$scope.loadingMeasurementsAlert = false;

		Measurement.getMeasurements().then(function (res) {

			$scope.loadingMeasurements = false;

			if (angular.isObject(res.data.response)) {

				$scope.measurements = res.data.response;
				
			} else {

				$scope.loadingMeasurementsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingMeasurements = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingMeasurementsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getStationsAbleToConfigure = function () {

		$scope.loadingStationsAbleToConfigure = true;
		$scope.loadingStationsAbleToConfigureAlert = false;

		Station.getStationsAbleToConfigure().then(function (res) {

			$scope.loadingStationsAbleToConfigure = false;

			if (angular.isObject(res.data.response)) {

				$scope.stations = res.data.response;
				
			} else {

				$scope.loadingStationsAbleToConfigureAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStationsAbleToConfigure = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStationsAbleToConfigureAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAllStationConfigurations = function () {

		$scope.loadingConfigurations = true;
		$scope.loadingConfigurationsAlert = false;

		Station.getConfigurations().then(function (res) {

			$scope.loadingConfigurations = false;

			if (angular.isObject(res.data.response)) {

				$scope.configurations = res.data.response;
				$scope.$emit('configurationsLoaded');

			} else {

				$scope.loadingConfigurationsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingConfigurations = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingConfigurationsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;

				$scope.formdata.name = $scope.event.tx_name;
				$scope.formdata.date = $filter('date')($scope.event.dt_event, 'MM/dd/yyyy');
				$scope.formdata.venue = $scope.event.id_venue;
				$scope.formdata.team = $scope.event.id_team;
				$scope.formdata.measurement_system = $scope.event.id_measurement;
		
				angular.forEach($scope.event.configurations, function (value, key) {

					$scope.formdata.configurations.push({
						id: value.id_configuration,
						station: value.station,
						test: value.test,
						age_range: value.nu_min_age + ' - ' + value.nu_max_age,
						distance: value.nu_distance
					});

				});

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getVenues();
	$scope.getTeams();
	$scope.getMeasurements();
	$scope.getStationsAbleToConfigure();
	$scope.getAllStationConfigurations();
	$scope.getEvent(id_uniq);

	$scope.$on('configurationsLoaded', function () {

		$scope.$watch('configurationdata.station', function () {

			$scope.tests = [];

			$scope.configurationdata.test = '';
			$scope.configurationdata.age_range = '';
			$scope.configurationdata.distance = '0';

			$scope.ConfigurationForm.test.$setPristine();
			$scope.ConfigurationForm.age_range.$setPristine();
			
			angular.forEach($scope.configurations, function (value, key) {

				if (value.id_station == $scope.configurationdata.station) {

					if (_.findIndex($scope.tests, { id: value.id_test, name: value.test_name }) == -1) {

						$scope.tests.push({
							id: value.id_test,
							name: value.test_name
						});	

					};

				};

			});

		});

		$scope.$watch('configurationdata.test', function () {

			$scope.ages = [];

			$scope.configurationdata.age_range = '';
			$scope.configurationdata.distance = '0';

			$scope.ConfigurationForm.age_range.$setPristine();
			
			angular.forEach($scope.configurations, function (value, key) {

				if (value.id_station == $scope.configurationdata.station && value.id_test == $scope.configurationdata.test) {

					if (_.indexOf($scope.ages, value.nu_age) == -1) {

						if (_.findIndex($scope.ages, { id: value.id_age_range, name: value.nu_min_age + ' - ' + value.nu_max_age }) == -1) {

							$scope.ages.push({
								id: value.id_age_range,
								name: value.nu_min_age + ' - ' + value.nu_max_age
							});	

						};


					};

				};

			});

		});

		$scope.$watch('configurationdata.age_range', function () {

			angular.forEach($scope.configurations, function (value, key) {

				if (value.id_station == $scope.configurationdata.station && value.id_test == $scope.configurationdata.test && value.id_age_range == $scope.configurationdata.age_range) {

					$scope.configurationdata.distance = value.nu_distance;

				};

			});

		});

	});

	$scope.addConfiguration = function () {

		$scope.showTooltip = false;

		var indexToLookFor = {
			id_station: $scope.configurationdata.station,
			id_test: $scope.configurationdata.test,
			id_age_range: $scope.configurationdata.age_range,
			nu_distance: $scope.configurationdata.distance
		};

		var index = _.findIndex($scope.configurations, indexToLookFor);

		var dataToPush = {
			id: $scope.configurations[index].id,
			station: $scope.configurations[index].station_name,
			test: $scope.configurations[index].test_name,
			age_range: $scope.configurations[index].nu_min_age + ' - ' + $scope.configurations[index].nu_max_age,
			distance: $scope.configurations[index].nu_distance
		};

		if (_.findIndex($scope.formdata.configurations, dataToPush) == -1) {

			$scope.formdata.configurations.push(dataToPush);

			$scope.configurationdata = {
				station: '',
				test: '',
				age_range: '',
				distance: '0'
			};

			$scope.ConfigurationForm.$setPristine();

		} else {

			$scope.showTooltip = true;

			$timeout(function () {

				$scope.showTooltip = false;				

			}, 2000);

		}

	};

	$scope.deleteConfiguration = function (index) {

		$scope.formdata.configurations.splice(index, 1);

	};

	$scope.editEvent = function () {

		$scope.editingEvent = true;
		$scope.editingEventAlert = false;

		var data = $.param($scope.formdata);

		Event.edit(id_uniq, data).then(function (res) {

			$scope.editingEvent = false;

			if (res.data.status == "success") {

				STFcache.delete('events');
				STFcache.delete('events' + id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Event updated correctly'
				};

				$location.path('events');

			} else {

				$scope.editingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.editingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);