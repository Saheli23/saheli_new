var stf = angular.module('ChallengesControllers', []);

stf.controller('ChallengesListController', ['$rootScope','$scope','$location', 'Challenge', 'Access', 'Auth', 'STFcache','$modal',  function ($rootScope,$scope,$location, Challenge, Access, Auth, STFcache,$modal) {
   var uuid="eb4bed44-9034-42c0-b4a4-7412dfcca601";//static user
   $scope.bulk=false;
   $scope.data = {
   		challengeStatus : ""
   }
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};



	$scope.getPlayers = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('challenges');
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.challenges = null;
		$scope.loadingPlayers = true;
		$scope.loadingPlayersAlert = false;

		Challenge.getAll().then(function (res) {

			$scope.loadingPlayers = false;

			if (angular.isObject(res.data)) {
				$scope.challenges = res.data;

				//to change phonenumber string to number
                
				 // res.data.response.forEach(function(players){
	    //         players.tx_phone= parseFloat(players.tx_phone);
	    //         players.tx_phone_prefix=parseFloat(players.tx_phone_prefix);
	    //        });
				 console.log($scope.challenges);
				
			} else {
              
				$scope.loadingPlayersAlert = {
					type: 'success',
					//message: res.data
				};

			}
			//$scope.getAccess(2);
		}, function (err) {

			$scope.loadingPlayers = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayers();

	$scope.deleteChallenges = function( id, uuid ){

		

		if(confirm("Are you sure to delete this Challenge?")){
			//var data = $.param({id_player:player_id,type:'delete'});
			Challenge.delete(id,uuid).then(function (res) {
				console.log(res);
				$scope.loadingPlayers = false;
				$scope.getPlayers(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingPlayersAlert = {type: "success",message: "Challenge deleted successfully"};
				}

			}, function (err) {

				$scope.loadingPlayers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingPlayersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	}
    
   $scope.bulkDeletePlayerEvent = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:values,type:'delete',action:action_id});
			Challenge.multidelete(data).then(function (res) {
				$scope.loadingPlayers = false;
				$scope.getPlayers(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingPlayersAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {

				$scope.loadingPlayers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingPlayersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one Player to delete.");
	 }
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);

		//	changeAttr( $scope.players, 'players', $scope.access.bo_view );

		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		})
	}

	$scope.restrictView = function(){
		$('#alertMessage').show();
		setTimeout(function(){ 
			$('#alertMessage').hide();
		}, 2000);
	}

	function compile(element){
		var el = angular.element(element);    
		$scope = el.scope();
		$injector = el.injector();
		$injector.invoke(function($compile){
			$compile(el)($scope);
		})     
	}

	// function changeAttr( obj, slug, viewAccess ){
 //        for( var i=0; i < obj.length; i++ ){
 //            var el = document.getElementById((slug.substring(0, slug.length-1))+"_id_"+i);
 //            if( viewAccess ){
 //                el.removeAttribute("ng-click");
 //                el.setAttribute("ng-href",slug+"/"+obj[i].id_uniq);
 //                el.setAttribute("href",slug+"/"+obj[i].id_uniq);
 //            } else {
 //                el.removeAttribute("ng-href");
 //                el.removeAttribute("href");
 //                el.setAttribute("ng-click", "restrictView()");
 //                compile(el);
 //            }
 //        }
 //    }


	$scope.orderOptions = [
		{ value: 'name', text: 'Challenge Name' },
		{ value: 'handle_name', text: 'Challenge Creator' },
		
	];

	 $scope.items = [
	 					'open',
	 					'pending',
	 					'fund',
	 					'judge-open',
	 					'judge-vote',
	 					'judge-result',
	 					'win',
	 					'fail',
	 					'incomplete'
	 				];
  $scope.open = function (challenge_id,currentstatus,challengeName,user_id) {
    var modalInstance;
     $scope.currentStatus = currentstatus;
     $scope.challengeName = challengeName;
    var modalScope = $scope.$new();
    modalScope.ok = function () {
        modalInstance.close($scope.data.challengeStatus);
    };
    modalScope.cancel = function () {
        modalInstance.dismiss('cancel');
    };    
    
    modalInstance = $modal.open({
      templateUrl: '../../../dist/views/myModalContent.html',
      //size: size,
      scope: modalScope
    });

    modalInstance.result.then(function (selectedItem) {
      console.log(selectedItem);
      Challenge.changeChallengeStatus(user_id,challenge_id,selectedItem ).then(function (res) {
			if (res.status == 201) {
				$rootScope.tooltipMessageAlert = {
						type: 'success',
						message: "Challenge status changed successfully"
					}

				 	$location.path('/challenges');
			}
		}, function (err) {
		});
    }, function () {
     // $log.info('Modal dismissed at: ' + new Date());
    });
  };
}]);
stf.controller('ChallengeCreateController', ['$scope', '$filter', '$location', '$window', '$rootScope', 'Challenge', 'Gender', 'ChallengeType', 'Auth', 'STFcache', function ($scope, $filter, $location, $window, $rootScope, Challenge, Gender, ChallengeType, Auth, STFcache) {
	
	 angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
          	var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;


			});

	var minDate = new Date();
	var maxDate = new Date();

	$scope.formdata = {
		name: '',
		title: '',
		description: '',
		description_2: '',
		expirationdate: '',
		challenge_type_id: '',
		challenge_category_type_id:'',
		zipcode: '',
		
	};

	$scope.zipcodeMask = "?";
	$scope.date = null;
	$scope.minDate = new Date();
	$scope.maxDate = new Date();
	$scope.startDate=new Date();
	$scope.dateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: 100,
		selectMonths: true,
	};

	$scope.getGenders = function () {

		$scope.loadingGenders = true;
		$scope.loadingGendersAlert = false;

		Gender.getGenders().then(function (res) {

			$scope.loadingGenders = false;

			if (angular.isObject(res.data.response)) {

				$scope.genders = res.data.response;

			} else {

				$scope.loadingGendersAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingGenders = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingGendersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getChallengeType = function () {

		$scope.loadingCountries = true;
		$scope.loadingCountriesAlert = false;

		ChallengeType.getChallengeType().then(function (res) {

			$scope.loadingCountries = false;
            console.log(res.data.challenge_types);
			if (angular.isObject(res.data.challenge_types)) {

				$scope.countries = res.data.challenge_types;

			} else {

				$scope.loadingCountriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCountries = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {
                console.log(err.status);
				$scope.loadingCountriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getChallengeCategory = function () {

		$scope.loadingStates = true;
		$scope.loadingStatesAlert = false;

		ChallengeType.getChallengeCategory().then(function (res) {

			$scope.loadingStates = false;

			if (angular.isObject(res.data.challenge_category_types)) {

				$scope.categories = res.data.challenge_category_types;

			} else {

				$scope.loadingStatesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStates = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStatesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getGenders();
	$scope.getChallengeType();
    $scope.getChallengeCategory();
	$scope.$watch('formdata.country', function () {

		var countryIndex = _.findIndex($scope.countries, { id: $scope.formdata.country });

		if (countryIndex != -1) {

			$scope.zipcodeMask = $filter('zipcodeMask')($scope.countries[countryIndex].tx_zipcode_format);
			$scope.zipcodeRequired = ($scope.zipcodeMask.length > 0) ? true : false;			

			$scope.formdata.phoneprefix = $scope.countries[countryIndex].tx_phone_prefix;

			var id_country = $scope.formdata.country;
			$scope.getCountryStates(id_country);

		};

		$scope.states = null;
		$scope.formdata.state = '';
		$scope.CreatePlayerForm.state.$setPristine();

	});

	$scope.createChallenge = function () {

		$scope.creatingPlayer = true;
		$scope.creatingPlayerAlert = false;

		//var data = new FormData();

       	var data = {
       	name: $scope.formdata.name,
       	title:$scope.formdata.name,
       	description: $scope.formdata.description,
		description_2: $scope.formdata.description_2,
		expiration_date: $scope.formdata.expirationdate,
		start_date:$scope.startDate.toString(),
		funding_start_date:$scope.startDate.toString(),
		funding_expiration_date:$scope.formdata.expirationdate,
		challenge_type_id: $scope.formdata.challenge_type_id,
		challenge_category_type_id: $scope.formdata.challenge_category_type_id,
		price: $scope.formdata.price,
		zip_code: $scope.formdata.zipcode
       };
		// data.append('name', $scope.formdata.name);
		// data.append('title', $scope.formdata.name);
		// data.append('description', $scope.formdata.description);
		// data.append('description_2', $scope.formdata.description_2);
		// data.append('expiration_date', $scope.formdata.expirationdate);
		// data.append('start_date',$scope.startDate);
		// data.append('funding_expiration_date', $scope.formdata.expirationdate);
		// data.append('funding_start_date',$scope.startDate);
		// data.append('challenge_type_id', $scope.formdata.challenge_type_id);
		// data.append('challenge_category_type_id', $scope.formdata.challenge_category_type_id);
		// data.append('price', $scope.formdata.price);
		// data.append('zip_code', $scope.formdata.zipcode);
		
       var uuid="eb4bed44-9034-42c0-b4a4-7412dfcca601";//static user
       // var data="name="+$scope.formdata.name+"&title="+$scope.formdata.name+"&description="+$scope.formdata.description+"&description_2="+$scope.formdata.description_2+"&expiration_date="+$scope.formdata.expirationdate+"&start_date="+$scope.startDate+"&challenge_type_id="+$scope.formdata.challenge_type_id+"&challenge_category_type_id="+$scope.formdata.challenge_category_type_id+"&price="+$scope.formdata.price+"&zip_code="+$scope.formdata.zipcode+"&funding_expiration_date="+$scope.formdata.expirationdate+"&funding_start_date="+$scope.startDate;
		Challenge.create($.param(data),uuid).then(function (res) {

			$scope.creatingPlayer = false;
             console.log(res);
			if (res.status == 201) {

					$rootScope.tooltipMessageAlert = {
						type: 'success',
						message: "Challenge created successfully"
					}

				 	$location.path('/challenges');

				// }

			} else {

				$scope.creatingPlayerAlert = {
					type: 'danger',
					message: res.data.message
				};

			}

		}, function (err) {
          console.log(err);
			$scope.creatingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingPlayerAlert = {
					type: 'danger',
					message:  err.data.message
				};

			}

		});

	};

}]);

stf.controller('ChallengesDetailsController', ['$scope','$window', '$routeParams', '$location', '$filter','$timeout', 'Challenge', 'Access', 'Auth','STFcache', function ($scope,$window, $routeParams, $location, $filter, $timeout, Challenge, Access, Auth,STFcache) {
	
	$scope.id = $routeParams.id;
	uuid="eb4bed44-9034-42c0-b4a4-7412dfcca601";

	$scope.randomNum = Math.floor((Math.random() * 10) + 1);
   $scope.challengeMedia=[];
	$scope.startSlick = function(){
            $scope.slickConfig = {
              dots: false,
              autoplay: false,
              infinite: false,
              speed: 300,
              slidesToShow: 3,
              method: {},
              touchMove: false,
              initOnload: true,
              data: $scope.challengeMedia,
              enabled: true,
              mobileFirst: true,
              
            };
        };

  $scope.startSlick();
  $scope.slickConfig.enabled = false;

	$scope.getUniqIDs = function () {

		$scope.loadingUniqIDs = true;
		$scope.loadingUniqIDsAlert = false;

		Challenge.getUniqIDs().then(function (res) {

			$scope.loadingUniqIDs = false;

			$scope.result = res.data.response;
			$scope.totalItems = parseInt($scope.result.length);
			$scope.currentItem = parseInt(_.findIndex($scope.result, { id_uniq: $scope.id_uniq }));
			$scope.nextItem = $scope.currentItem + 1;	
			$scope.prevItem = $scope.currentItem - 1;

			$scope.nextID = ($scope.nextItem > -1 && $scope.nextItem < $scope.totalItems) ? $scope.result[$scope.nextItem].id_uniq : false;
			$scope.prevID = ($scope.prevItem >= 0) ? $scope.result[$scope.prevItem].id_uniq : false;

		}, function (err) {

			$scope.loadingUniqIDs = false;

			if (err.status == 401) {

				Auth.deleteToken();

			};

		});

	};

	$scope.prevPlayer = function () { $location.path('/players/' + $scope.prevID); };
	$scope.nextPlayer = function () { $location.path('/players/' + $scope.nextID); };

	$scope.getPlayer = function (id) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Challenge.getOne(id).then(function (res) {

			$scope.loadingPlayer = false;

			if (angular.isObject(res.data.challenges)) {

				$scope.player = res.data.challenges[0];

				$scope.getUniqIDs();
				$scope.getAccess(2);

			} else {

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer($scope.id);

	$scope.getPlayers = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('challenges');

		$scope.players = null;
		$scope.loadingPlayers = true;
		$scope.loadingPlayersAlert = false;

		Challenge.getAll().then(function (res) {

			$scope.loadingPlayers = false;

			if (angular.isObject(res.data.response)) {
				$scope.players = res.data.response;
				
			} else {

				$scope.loadingPlayersAlert = {
					type: 'success',
					message: res.data.response
				};

			}
			$scope.getAccess(2);
		}, function (err) {

			$scope.loadingPlayers = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	} 

	$scope.deletePlayerEvent = function( player_id, ind ){

		console.log(player_id);

		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:player_id,type:'delete'});
			Challenge.delete(data).then(function (res) {
				$scope.loadingPlayers = false;
				$scope.getPlayers(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;
                     
				} else {
					$scope.loadingPlayerAlert = {type: res.data.status,message: res.data.response};
					if(res.data.status=="success"){
					 $location.path('/players');
					}
				}

			}, function (err) {

				$scope.loadingPlayers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingPlayersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	},

	$scope.getChallengeMedia = function (id) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;
        var uuid="eb4bed44-9034-42c0-b4a4-7412dfcca601";//static user
		Challenge.getChallengeMedia(id,uuid).then(function (res) {

			$scope.loadingPlayer = false;

			if (angular.isObject(res.data.challenge_media)) {
				// //done for static image
				 // res.data.challenge_media.forEach(function(media){
     // 	            media.path= "oneup.us-west-2.elasticbeanstalk.com/uploads/users/291979e0-f936-47a1-97e2-3af8f11d07ac/challenge/57e0c6332a374727a914f8d1/471738-3x2-700x467.jpg";
     	          
     // 	           });
                    //
				$scope.challengeMedia = res.data.challenge_media;
				$timeout(function () {
					$scope.startSlick();
					$scope.slickConfig.enabled = true;
				}, 500);
				// $timeout(function(){
					// $scope.startSlick();
					// $scope.slickConfig.enabled = true;
				// },500);
                $scope.dataloaded=true;
				$scope.getUniqIDs();
				$scope.getAccess(2);

			} else {

				// $scope.loadingPlayerAlert = {
				// 	type: 'success',
				// 	message: res.data
				// };

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};
	$scope.getChallengeMedia($scope.id);

	
   $scope.deleteChallengeMedia = function(ind ){
         console.log(ind);
		if(confirm("Are you sure to delete this Media?")){
			
			Challenge.deleteChallengeMedia(ind,$scope.id,uuid).then(function (res) {
				$scope.loadingPlayers = false;
				//$scope.getPlayers(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;
                     
				} else {
					// $scope.loadingPlayerAlert = {type: res.data.status,message: res.data.response};
					// if(res.status==204){
                   $scope.loadingPlayersAlert = { type: 'success', message:"Media deleted successfully" };
                  $window.location.reload();
				   // $scope.startSlick();
				   // $scope.slickConfig.enabled = true;
				//}
				}

			}, function (err) {

				$scope.loadingPlayers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingPlayersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	}
}]);

stf.controller('ChallengeEditController', ['$scope', '$filter', '$location', '$routeParams', '$timeout', '$rootScope', '$window', 'Challenge','ChallengeType','Media', 'Gender', 'Country', 'Auth', 'STFcache', function ($scope, $filter, $location, $routeParams, $timeout, $rootScope, $window, Challenge,ChallengeType,Media, Gender, Country, Auth, STFcache) {
	
	 angular.element('input[type=number]').on("keypress",function (evt) {
    	    
    	    var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;

            
			});

	var minDate = new Date();
	var maxDate = new Date();

	$scope.formdata = {
		"name": '',
		"title": '',
		"description": '',
		"description_2": '',
		"expiration_date": '',
		"challenge_type_id": '',
		"challenge_category_type_id":'',
		"zip_code": '',
		"price": '',
		"start_date": ''
		
	};

	$scope.uploadformdata ={
    "name": '',
    "description": '',
    "file":'',
    "media_type_id":'',
    "media_default":''

	}

	$scope.zipcodeMask = "?";
	$scope.date = null;
	$scope.minDate = new Date();
	$scope.maxDate = new Date();
	$scope.startDate=new Date();
	$scope.dateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: 100,
		selectMonths: true
	};

	var id_uniq = $routeParams.id;

	$scope.getChallengeType = function () {

		$scope.loadingCountries = true;
		$scope.loadingCountriesAlert = false;

		ChallengeType.getChallengeType().then(function (res) {

			$scope.loadingCountries = false;
            console.log(res.data.challenge_types);
			if (angular.isObject(res.data.challenge_types)) {

				$scope.countries = res.data.challenge_types;

			} else {

				$scope.loadingCountriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCountries = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {
                console.log(err.status);
				$scope.loadingCountriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getChallengeCategory = function () {

		$scope.loadingStates = true;
		$scope.loadingStatesAlert = false;

		ChallengeType.getChallengeCategory().then(function (res) {

			$scope.loadingStates = false;

			if (angular.isObject(res.data.challenge_category_types)) {

				$scope.categories = res.data.challenge_category_types;

			} else {

				$scope.loadingStatesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStates = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStatesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getMediaType = function () {

		$scope.loadingMediaType = true;
		$scope.loadingMediaTypeAlert = false;

		Media.getMediaType().then(function (res) {

			$scope.loadingMediaType = false;
            console.log(res.data.media_types);

			if (angular.isObject(res.data.media_types)) {
				//for filtering
                // delete res.data.media_types[0];
                // delete res.data.media_types[1];
                // console.log(res.data.media_types);

				$scope.mediatypes = res.data.media_types;

			} else {

				$scope.loadingMediaTypeAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingMediaType = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {
                console.log(err.status);
				$scope.loadingMediaTypeAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Challenge.getOne(id_uniq).then(function (res) {

			$scope.loadingPlayer = false;
            
           
           if (angular.isObject(res.data.challenges)) {
                  
				$scope.player = res.data.challenges[0];
                
				$scope.formdata.name = $scope.player.name;
				$scope.formdata.title = $scope.player.title;
				$scope.formdata.description = $scope.player.description;
				$scope.formdata.description_2 = $scope.player.description_2;
				$scope.formdata.expiration_date = $filter('date')($scope.player.expiration_date, 'MM/dd/yyyy');
				$scope.formdata.challenge_type_id = parseInt($scope.player.challenge_type_id);
				$scope.formdata.challenge_category_type_id = parseInt($scope.player.challenge_category_type_id);
				$scope.formdata.price = $scope.player.price;
				$scope.formdata.zip_code = $scope.player.zip_code;
				$scope.formdata.start_date = $filter('date')($scope.player.start_date, 'MM/dd/yyyy');
				$scope.formdata.is_published=true;
				$scope.uuid=$scope.player.uuid;

			} else {

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getGenders = function () {

		$scope.loadingGenders = true;
		$scope.loadingGendersAlert = false;

		Gender.getGenders().then(function (res) {

			$scope.loadingGenders = false;

			if (angular.isObject(res.data.response)) {

				$scope.genders = res.data.response;

			} else {

				$scope.loadingGendersAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingGenders = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingGendersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountries = function () {

		$scope.loadingCountries = true;
		$scope.loadingCountriesAlert = false;

		Country.getCountries().then(function (res) {

			$scope.loadingCountries = false;

			if (angular.isObject(res.data.response)) {

				$scope.countries = res.data.response;
				$scope.$emit('countriesLoaded');

			} else {

				$scope.loadingCountriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCountries = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingCountriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountryStates = function (id_country) {

		$scope.loadingStates = true;
		$scope.loadingStatesAlert = false;

		Country.getCountryStates(id_country).then(function (res) {

			$scope.loadingStates = false;

			if (angular.isObject(res.data.response)) {

				$scope.states = res.data.response;

			} else {

				$scope.loadingStatesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStates = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStatesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer(id_uniq);
	$scope.getGenders();
	$scope.getChallengeType();
    $scope.getChallengeCategory();
    $scope.getMediaType();

	$scope.$on('countriesLoaded', function () {

		$scope.$watch('formdata.country', function () {

			var countryIndex = _.findIndex($scope.countries, { id: $scope.formdata.country });

			if (countryIndex != -1) {

				$scope.zipcodeMask = $filter('zipcodeMask')($scope.countries[countryIndex].tx_zipcode_format);
				$scope.zipcodeRequired = ($scope.zipcodeMask.length > 0) ? true : false;

				$timeout(function () { $scope.formdata.zipcode = $scope.player.tx_zipcode; }, 1000);

				$scope.formdata.phoneprefix = $scope.countries[countryIndex].tx_phone_prefix;

				var id_country = $scope.formdata.country;
				$scope.getCountryStates(id_country);

			} else {

				$scope.states = null;
				$scope.formdata.state = '';
				$scope.EditPlayerForm.state.$setPristine();

			}

		});

	});

	$scope.editChallenge = function () {

		$scope.editingPlayer = true;
		$scope.editingPlayerAlert = false;


		console.log($scope.formdata);

		// var data = new FormData();
        
		// data.append('name', $scope.formdata.name);
		// data.append('title', $scope.formdata.name);
		// data.append('description', $scope.formdata.description);
		// data.append('description_2', $scope.formdata.description_2);
		// //data.append('gender', $scope.formdata.gender);
		// data.append('expiration_date', $scope.formdata.expirationdate);
		// //data.append('start_date',$scope.start_date);
		// data.append('challenge_type_id', $scope.formdata.challenge_type_id);
		// //data.append('state', $scope.formdata.state);
		// data.append('challenge_category_type_id', $scope.formdata.challenge_category_type_id);
		// data.append('price', $scope.formdata.price);
		// data.append('zip_code', $scope.formdata.zipcode);
		
      // var uuid="eb4bed44-9034-42c0-b4a4-7412dfcca601";//static user
      
		Challenge.edit(id_uniq, JSON.stringify($scope.formdata),$scope.uuid).then(function (res) {

			$scope.editingPlayer = false;

			if (res.status == 200) {

				$rootScope.tooltipMessageAlert = {
						type: 'success',
						message: "Challenge updated successfully"
					}

				 	$location.path('/challenges');

			} else {

				$scope.editingPlayerAlert = {
					type: 'danger',
					message: res.data.message
				};

			}

		}, function (err) {

			$scope.editingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingPlayerAlert = {
					type: 'danger',
					message: err.data.message
				};

			}

		});

	};

	$scope.uploadMedia = function () {

		$scope.uploadingMedia = true;
		$scope.uploadingMediaAlert = false;


		//console.log($scope.uploadformdata);
        	 var data = new FormData();
        
		data.append('name', $scope.uploadformdata.name);
		data.append('description', $scope.uploadformdata.description);
		data.append('challenge_type_id', $scope.uploadformdata.challenge_type_id);
		data.append('media_type_id', $scope.uploadformdata.media_type_id);
		data.append('is_default', $scope.uploadformdata.media_default);
		data.append('file', $scope.uploadformdata.file);
	
       var uuid="eb4bed44-9034-42c0-b4a4-7412dfcca601";//static user
       
		Challenge.uploadMedia(data,uuid,id_uniq).then(function (res) {

			$scope.uploadingMedia = false;

			if (res.status == 201) {

			
				$scope.uploadingMediaAlert = {
						type: 'success',
						message: "Media uploaded successfully"
					}

				 //	$location.path('/challenges');

			} else {

				$scope.uploadingMediaAlert = {
					type: 'danger',
					message: res.data.message
				};

			}

		}, function (err) {

			$scope.uploadingMedia = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.uploadingMediaAlert = {
					type: 'danger',
					message: err.data.message
				};

			}

		});

	};

}]);

stf.controller('PlayerTeamsController', ['$scope', '$routeParams', 'Player', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, Player, Access, Auth, STFcache) {
	



	$scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};
    

	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			$scope.loadingPlayer = false;

			if (angular.isObject(res.data.response)) {

				$scope.player = res.data.response;
				$scope.getAccess(2);
			} else {

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.getPlayerTeams = function (id_uniq, refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('players' + id_uniq + 'teams');
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.teams = null;
		$scope.loadingTeams = true;
		$scope.loadingTeamsAlert = false;

		Player.getTeams(id_uniq).then(function (res) {

			$scope.loadingTeams = false;

			if (angular.isObject(res.data.response)) {

				$scope.teams = res.data.response;

			} else {

				$scope.loadingTeamsAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTeams = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer($scope.id_uniq);
	$scope.getPlayerTeams($scope.id_uniq);

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		})
	}

	$scope.orderOptions = [
		{ value: 'team', text: 'Name' },
		{ value: '-dt_admission', text: 'Admission Date' },
		{ value: '-dt_discharge', text: 'Discharge Date' }
	];

$scope.deletePlayerTeam = function( player_id, team_id ){
	//console.log(player_id);
		if(confirm("Are you sure to delete this Team of this Player?")){
			var data = $.param({
								id_player:player_id,
								id_team:team_id,
								type:'delete'
							});
			Player.deleteTeam(data).then(function( res ){
				$scope.loadingTeams = false;
				$scope.getPlayerTeams($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);

				} else {
					$scope.loadingTeamsAlert = {type: res.data.status,message: res.data.response};
				}
			}, function (err) {
				$scope.loadingTeams = false;

				if (err.status == 401) {

					Auth.deleteToken();

				} else {

					$scope.loadingTeamsAlert = {
						type: 'danger',
						message: err.status + ' ' + err.statusText
					};

				}
			});
		}
	};

	 $scope.bulkDeletePlayerTeam = function(){

       var id_team=angular.element('#teamid').val();
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Team of this Player?")){
			var data = $.param({
								id_player:values,
								id_team:id_team,
								action:action_id,
								type:'delete'
							});
			
			Player.multideleteTeam(data).then(function( res ){
				$scope.loadingTeams = false;
				$scope.getPlayerTeams($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);

				} else {
					$scope.loadingTeamsAlert = {type: res.data.status,message: res.data.response};
				}
			}, function (err) {
				$scope.loadingTeams = false;

				if (err.status == 401) {

					Auth.deleteToken();

				} else {

					$scope.loadingTeamsAlert = {
						type: 'danger',
						message: err.status + ' ' + err.statusText
					};

				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one Team to delete.");
	 }
	};


}]);

stf.controller('PlayerTeamCreateController', ['$scope', '$routeParams', '$timeout', '$filter', '$rootScope', '$location', 'Player', 'Team', 'Auth', 'STFcache', function ($scope, $routeParams, $timeout, $filter, $rootScope, $location, Player, Team, Auth, STFcache) {
	
	 angular.element('input[type=number]').on("keypress",function (evt) {
    	    var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;
           

			});


	var id_uniq = $routeParams.id_uniq;

	$scope.date = null;
	$scope.admissionDate = null;
	$scope.dischargeDate = null;
	$scope.maxDate = new Date();
	$scope.dateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: 100,
		selectMonths: true
	};

	$scope.formdata = {
		current_team: {},
		team: '',
		admission_date: '',
		discharge_date: '',
		number: '',
		positions: []
	};

	$scope.positionsdata = {
		id_position: '',
		position: ''
	};

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			$scope.loadingPlayer = false;

			if (angular.isObject(res.data.response)) {

				$scope.player = res.data.response;

			} else {

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.getPlayerCurrentTeam = function (id_uniq) {

		$scope.loadingCurrentTeam = true;
		$scope.loadingCurrentTeamAlert = false;

		Player.getCurrentTeam(id_uniq).then(function (res) {

			$scope.loadingCurrentTeam = false;

			if (angular.isObject(res.data.response)) {

				$scope.currentTeam = res.data.response;

				$scope.formdata.current_team.id_team_player = $scope.currentTeam.id_team_player;
				$scope.formdata.current_team.admission_date = $filter('date')($scope.currentTeam.dt_admission, 'MM/dd/yyyy');
				$scope.formdata.current_team.discharge_date = $filter('date')($scope.currentTeam.dt_discharge, 'MM/dd/yyyy');

				$scope.date = $scope.formdata.current_team.discharge_date;

			};

		}, function (err) {

			$scope.loadingCurrentTeam = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingCurrentTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTeams = function () {

		$scope.loadingTeams = true;
		$scope.loadingTeamsAlert = false;

		Team.getTeams().then(function (res) {

			$scope.loadingTeams = false;

			if (angular.isObject(res.data.response)) {

				$scope.teams = res.data.response;

			} else {

				$scope.loadingTeamsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTeams = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPositions = function () {

		$scope.loadingPositions = true;
		$scope.loadingPositionsAlert = false;

		Team.getPositions().then(function (res) {

			$scope.loadingPositions = false;

			if (angular.isObject(res.data.response)) {

				$scope.positions = res.data.response

			} else {

				$scope.loadingPositionsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPositions = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPositionsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer(id_uniq);
	$scope.getPlayerCurrentTeam(id_uniq);
	$scope.getTeams();
	$scope.getPositions();

	$scope.$watch('formdata.current_team.discharge_date', function () {

		if ($scope.formdata.current_team.discharge_date != null) { $scope.checkCurrentTeamDates(); };

	});

	$scope.checkCurrentTeamDates = function () {

		$scope.wrongCurrentTeamDischargeDate = false;

		var admission = new Date($scope.formdata.current_team.admission_date).getTime();
		var discharge = new Date($scope.formdata.current_team.discharge_date).getTime();

		if (discharge <= admission) { $scope.wrongCurrentTeamDischargeDate = true };

	};

	$scope.$watch('formdata.admission_date', function () {

		$scope.checkDates();

	});

	$scope.$watch('formdata.discharge_date', function () {

		$scope.checkDates();

	});

	$scope.checkDates = function () {

		$scope.wrongDischargeDate = false;
		$scope.wrongAdmissionDate= false;

		var admission = new Date($scope.formdata.admission_date).getTime();
		var discharge = new Date($scope.formdata.discharge_date).getTime();

		if (discharge <= admission) $scope.wrongDischargeDate = true;

		var currentTeamDischarge = new Date($scope.formdata.current_team.discharge_date).getTime();
        if (admission <= currentTeamDischarge) $scope.wrongAdmissionDate = true;
        if (discharge <= currentTeamDischarge) $scope.wrongDischargeDate = true;

	};

	$scope.addPosition = function () {

		$scope.showTooltip = false;

		var positionsData = {
			id_position: $scope.positionsdata.id_position,
			position: $scope.positions[$scope.positionsdata.id_position - 1].tx_name
		};

		var dataIndexToLookFor = {
			id_position: $scope.positionsdata.id_position
		};

		if (_.findIndex($scope.formdata.positions, dataIndexToLookFor) == -1) {

			$scope.formdata.positions.push(positionsData);
			$scope.positionsdata.id_position = '';

		} else {

			$scope.showTooltip = true;

			$timeout(function () {

				$scope.showTooltip = false;				

			}, 2000);

		}

	};

	$scope.deletePosition = function (index) {

		$scope.formdata.positions.splice(index, 1);

	};

	$scope.createPlayerTeam = function () {

		$scope.creatingTeam = true;
		$scope.creatingTeamAlert = false;

		var data = $.param($scope.formdata);

		Player.createTeam(id_uniq, data).then(function (res) {

			$scope.creatingTeam = false;

			if (res.data.status == "success") {

				STFcache.delete('players' + id_uniq + 'current_team');
				STFcache.delete('players' + id_uniq + 'teams');
				STFcache.delete('players' + id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Team created correctly'
				};

				$location.path('players/' + id_uniq + '/teams');

			} else {

				$scope.creatingTeamAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingTeam = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};
	
}]);

stf.controller('PlayerTeamEditController', ['$scope', '$routeParams', '$timeout', '$filter', '$rootScope', '$location', 'Player', 'Team', 'Auth', 'STFcache', function ($scope, $routeParams, $timeout, $filter, $rootScope, $location, Player, Team, Auth, STFcache) {
	
	 angular.element('input[type=number]').on("keypress",function (evt) {
    	    var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;
            

			});

	var id_uniq = $routeParams.id_uniq;
	var id_team_player = $routeParams.id_team_player;

	$scope.admissionDate = null;
	$scope.dischargeDate = null;
	$scope.maxDate = new Date();
	$scope.dateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: 100,
		selectMonths: true
	};

	$scope.formdata = {
		team: '',
		admission_date: '',
		discharge_date: '',
		number: '',
		positions: []
	};

	$scope.positionsdata = {
		id_position: '',
		position: ''
	};

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			$scope.loadingPlayer = false;

			if (angular.isObject(res.data.response)) {

				$scope.player = res.data.response;

			} else {

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.getPlayerTeam = function (id_uniq, id_team_player) {

		$scope.loadingPlayerTeam = true;
		$scope.loadingPlayerTeamAlert = false;

		Player.getTeam(id_uniq, id_team_player).then(function (res) {

			$scope.loadingPlayerTeam = false;

			if (angular.isObject(res.data.response)) {

				$scope.teamExist = true;
				$scope.playerTeam = res.data.response;
				
				$scope.formdata.team = $scope.playerTeam.id_team;
				$scope.formdata.admission_date = $filter('date')($scope.playerTeam.dt_admission, 'MM/dd/yyyy');
				$scope.formdata.discharge_date = $filter('date')($scope.playerTeam.dt_discharge, 'MM/dd/yyyy');
				$scope.formdata.number = parseInt($scope.playerTeam.nu_dorsal);

				angular.forEach($scope.playerTeam.positions, function (value, key) {

					$scope.formdata.positions.push({
						id_position: value.id_position,
						position: value.tx_name
					});

				})

			} else {

				$scope.loadingPlayerTeamAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayerTeam = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTeams = function () {

		$scope.loadingTeams = true;
		$scope.loadingTeamsAlert = false;

		Team.getTeams().then(function (res) {

			$scope.loadingTeams = false;

			if (angular.isObject(res.data.response)) {

				$scope.teams = res.data.response;

			} else {

				$scope.loadingTeamsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTeams = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPositions = function () {

		$scope.loadingPositions = true;
		$scope.loadingPositionsAlert = false;

		Team.getPositions().then(function (res) {

			$scope.loadingPositions = false;

			if (angular.isObject(res.data.response)) {

				$scope.positions = res.data.response

			} else {

				$scope.loadingPositionsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPositions = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPositionsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer(id_uniq);
	$scope.getPlayerTeam(id_uniq, id_team_player);
	$scope.getTeams();
	$scope.getPositions();

	$scope.$watch('formdata.admission_date', function () {

		if ($scope.formdata.discharge_date != null) { $scope.checkDates(); };

	});

	$scope.$watch('formdata.discharge_date', function () {

		if ($scope.formdata.discharge_date != null) { $scope.checkDates(); };

	});

	$scope.checkDates = function () {

		$scope.wrongDischargeDate = false;

		var admission = new Date($scope.formdata.admission_date).getTime();
		var discharge = new Date($scope.formdata.discharge_date).getTime();

		if (discharge <= admission) $scope.wrongDischargeDate = true;

	};

	$scope.addPosition = function () {

		$scope.showTooltip = false;

		var positionsData = {
			id_position: $scope.positionsdata.id_position,
			position: $scope.positions[$scope.positionsdata.id_position - 1].tx_name
		};

		var dataIndexToLookFor = {
			id_position: $scope.positionsdata.id_position
		};

		if (_.findIndex($scope.formdata.positions, dataIndexToLookFor) == -1) {

			$scope.formdata.positions.push(positionsData);
			$scope.positionsdata.id_position = '';

		} else {

			$scope.showTooltip = true;

			$timeout(function () {

				$scope.showTooltip = false;				

			}, 2000);

		}

	};

	$scope.deletePosition = function (index) {

		$scope.formdata.positions.splice(index, 1);

	};

	$scope.editPlayerTeam = function () {

		$scope.editingTeam = true;
		$scope.editingTeamAlert = false;

		var data = $.param($scope.formdata);

		Player.editTeam(id_uniq, id_team_player, data).then(function (res) {

			$scope.editingTeam = false;

			if (res.data.status == "success") {

				STFcache.delete('players' + id_uniq + 'teams' + id_team_player);
				STFcache.delete('players' + id_uniq + 'teams');
				STFcache.delete('players' + id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Team updated correctly'
				};

				$location.path('players/' + id_uniq + '/teams');

			} else {

				$scope.editingTeamAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.editingTeam = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};
	
}]);