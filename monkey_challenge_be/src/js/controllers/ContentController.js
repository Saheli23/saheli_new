var stf = angular.module('ContentControllers', []);

stf.controller('ContentCreateController', ['$rootScope','$scope','$location', 'Content', 'Access', 'Auth', 'STFcache',  function ($rootScope,$scope,$location, Content, Access, Auth, STFcache) {
	$scope.formdata = {
		title: '',
		content: ''
	};



	$scope.createContent = function () {
		$scope.creatingContent      = true;
		$scope.creatingContentAlert = false;
		var data = $.param($scope.formdata);

		Content.create( data ).then(function (res) {
				//console.log(res.data.response);
			$scope.creatingContent = false;
			$rootScope.tooltipMessageAlert  = {
				type: "success",
				message: res.data.response
			}
			$location.path('/content');
		}, function (err) {
			$scope.creatingContent = false;
			if (err.status == 401) {
				Auth.deleteToken();
			} else {
				$scope.creatingContentAlert = {
					type: 'danger',
					message:  err.data.message
				};
			}
		});
	};
}]);

stf.controller('ContentListController', ['$rootScope','$scope', 'Content', 'Access', 'Auth', 'STFcache',  function ($rootScope,$scope, Content, Access, Auth, STFcache) {
	
	$scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};



	$scope.getContents = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('challenges');
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.challenges = null;
		$scope.loadingPlayers = true;
		$scope.loadingPlayersAlert = false;

		Content.getAll().then(function (res) {
                console.log(res);
			$scope.loadingPlayers = false;

			if (angular.isObject(res.data)) {
				 // res.data.response.forEach(function(contents){
     // 	            contents.content= $.parseHTML(contents.content);
     	          
     //  	           });
				
				console.log("res.data:",res.data.response);		
				$scope.contents = res.data.response;
                
			
			} else {
              
				$scope.loadingPlayersAlert = {
					type: 'success',
					//message: res.data
				};

			}
			
		}, function (err) {

			$scope.loadingPlayers = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getContents();

	
   


	$scope.orderOptions = [
		{ value: 'tite', text: 'Content Title' }
		
		
	];

	$scope.deleteContents = function( id){

		

		if(confirm("Are you sure to delete this Content?")){
			//var data = $.param({id_player:player_id,type:'delete'});
			Content.delete(id).then(function (res) {
				
				$scope.loadingPlayers = false;
				$scope.getContents(true);
				if (angular.isObject(res.data)) {
					$scope.loadingPlayersAlert = {type: "success",message: "Content deleted successfully"};
				} else {
					$scope.loadingPlayersAlert = {type: "success",message: "Content deleted successfully"};
				}

			}, function (err) {

				$scope.loadingPlayers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingPlayersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	}
    

}]);


stf.controller('ContentDetailsController', ['$scope','$window', '$routeParams', '$location', '$filter','$timeout', 'Content', 'Access', 'Auth','STFcache', function ($scope,$window, $routeParams, $location, $filter, $timeout, Content, Access, Auth,STFcache) {
	
	$scope.id = $routeParams.id;
	
	$scope.getContent = function (id) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Content.getOne(id).then(function (res) {

			$scope.loadingPlayer = false;

			if (angular.isObject(res.data.challenges)) {

				$scope.contents = res.data.challenges[0];
         
			//	$scope.getUniqIDs();
			//	$scope.getAccess(2);

			} else {

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getContent($scope.id);

	$scope.deletePlayerEvent = function( player_id, ind ){

		console.log(player_id);

		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:player_id,type:'delete'});
			Challenge.delete(data).then(function (res) {
				$scope.loadingPlayers = false;
				$scope.getPlayers(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;
                     
				} else {
					$scope.loadingPlayerAlert = {type: res.data.status,message: res.data.response};
					if(res.data.status=="success"){
					 $location.path('/players');
					}
				}

			}, function (err) {

				$scope.loadingPlayers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingPlayersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	}

	
}]);

stf.controller('ContentEditController', ['$rootScope','$scope','$location','$routeParams', 'Content', 'Access', 'Auth', 'STFcache',  function ($rootScope,$scope,$location,$routeParams, Content, Access, Auth, STFcache) {
	$scope.formdata = {
		title: '',
		content: ''
	};

	console.log('call');

    $scope.id = $routeParams.id;
    
    $scope.getContents = function (refresh,id) {
     $scope.loadingContents = true;
	 $scope.loadingPlayerAlert = false;
	 $rootScope.tooltipMessageAlert=false;
		if (refresh !== undefined && refresh == true) STFcache.delete('challenges');
        

      
		Content.getOne(id).then(function (res) {

			$scope.loadingContents = false;

			if (angular.isObject(res.data.response)) {

				$scope.contents = res.data.response;
				$scope.formdata.title = $scope.contents.title;
				$scope.formdata.content = $scope.contents.content;
			$scope.getAccess(2);

			} else {

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data
				};

			}

		}, function (err) {

			$scope.loadingPlayers = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getContent = function (id) {
		
		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Content.getOne(id).then(function (res) {

			$scope.loadingPlayer = false;

			if (angular.isObject(res.data.response)) {

				$scope.contents = res.data.response;
				$scope.formdata.title = $scope.contents.title;
				$scope.formdata.content = $scope.contents.content;
			$scope.getAccess(2);

			} else {

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};
	$scope.getContent($scope.id);
    
	$scope.editContent = function () {
		$scope.creatingContent      = true;
		$scope.creatingContentAlert = false;
		var data = $.param($scope.formdata);

		Content.edit( $scope.id ,data ).then(function (res) {
				//console.log(res.data.response);
			$scope.creatingContent = false;
			$rootScope.tooltipMessageAlert  = {
				type: "success",
				message: res.data.response
			}
			//$location.path('/content');
		}, function (err) {
			$scope.creatingContent = false;
			if (err.status == 401) {
				Auth.deleteToken();
			} else {
				$scope.creatingContentAlert = {
					type: 'danger',
					message:  err.data.message
				};
			}
		});
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		})
	}
}]);