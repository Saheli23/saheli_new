var stf = angular.module('StationConfigControllers', []);

stf.controller('StationConfigListController', ['$scope', 'Station', 'Access', 'Auth', 'STFcache', function ($scope, Station, Access, Auth, STFcache) {
	 
   $scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};


	$scope.getAllConfigurations = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('stationsConfigAll');
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.configurations = null;
		$scope.loadingConfigurations = true;
		$scope.loadingConfigurationsAlert = false;

		Station.getConfigurations().then(function (res) {

			$scope.loadingConfigurations = false;

			if (angular.isObject(res.data.response)) {

				$scope.configurations = res.data.response;
				//converting age string into number
	               res.data.response.forEach(function(configurations){
	            configurations.nu_min_age= parseFloat(configurations.nu_min_age);
	           });
				console.log($scope.configurations);
				
			} else {

				$scope.loadingConfigurationsAlert = {
					type: 'success',
					message: res.data.response
				};

			}
			$scope.getAccess(1);
		}, function (err) {

			$scope.loadingConfigurations = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingConfigurationsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAllConfigurations();

	$scope.orderOptions = [
		{ value: 'station_name', text: 'Station' },
		{ value: 'test_name', text: 'Test' },
		{ value: '-nu_min_age', text: 'Age Range' },
		{ value: 'nu_distance', text: 'Distance' }
	];
    
    $scope.arrange="";
			$scope.$watch('o', function() {
		    //alert('changed');
		    $scope.arrange="";
		    console.log($scope.o);
		    if($scope.o=="-nu_min_age"){
		    	$scope.arrange="'-nu_min_age'";
		    	console.log($scope.arrange);
		    }
		});
	$scope.deleteStationConfiguration = function(id_config){

			if(confirm("Are you sure to delete this Configuration?")){
				var data = $.param({id_config:id_config,type:'delete'});
				Station.delete(data).then(function (res) {
					$scope.loadingConfigurations = false;
					
					$scope.getAllConfigurations(true);
					if (angular.isObject(res.data.response)) {
						//$scope.measurements = res.data.response;

					} else {
						$scope.loadingConfigurationsAlert = {type: 'success',message: res.data.response};
					}

				}, function (err) {

					$scope.loadingConfigurations = false;

					if (err.status == 401) {
						Auth.deleteToken();
					} else {
						$scope.loadingConfigurationsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
					}
				});
			}
		};

	$scope.bulkDeleteStationConfiguration = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Configuration?")){
			var data = $.param({id_config:values,type:'delete',action:action_id});
			Station.multidelete(data).then(function (res) {
				$scope.loadingConfigurations = false;
				
				$scope.getAllConfigurations(true);
				if (angular.isObject(res.data.response)) {
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingConfigurationsAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingConfigurations = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingConfigurationsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one to delete.");
	 }
	};


	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

}]);

stf.controller('StationConfigCreateController', ['$scope', '$location', '$rootScope', 'Station', 'Age', 'Auth', 'STFcache',  function ($scope, $location, $rootScope, Station, Age, Auth, STFcache) {
	
	$scope.formdata = {
		station: '',
		test: '',
		age_range: '',
		distance: ''
	};

	$scope.getStationsAbleToConfigure = function () {

		$scope.loadingStations = true;
		$scope.loadingStationsALert = false;

		Station.getStationsAbleToConfigure().then(function (res) {

			$scope.loadingStations = false;

			if (angular.isObject(res.data.response)) {

				$scope.stations = res.data.response;
				$scope.$emit('stationsLoaded');

				//only take numbers
					angular.element('input[type=number]').on("keypress",function (evt) {
	    	    // if (String.fromCharCode(evt.which) == "e"){
	        	// return false;
	         //    }
	         //    if (String.fromCharCode(evt.which) == "."){
	        	// return false;
	         //    }
	            var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;

				});

			} else {

				$scope.loadingStationsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStations = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStationsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAgeRange = function () {

		$scope.loadingRange = true;
		$scope.loadingRangeAlert = false;

		Age.getRange().then(function (res) {

			$scope.loadingRange = false;

			if (angular.isObject(res.data.response)) {

				$scope.range = res.data.response;

			} else {

				$scope.loadingRangeAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingRange = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingRangeAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTests = function (id_station) {

		$scope.loadingTests = true;
		$scope.loadingTestsAlert = false;

		Station.getTests(id_station).then(function (res) {

			$scope.loadingTests = false;

			if (angular.isObject(res.data.response)) {

				$scope.tests = res.data.response;

			} else {

				$scope.loadingTestsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTests = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTestsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getStationsAbleToConfigure();
	$scope.getAgeRange();

	$scope.$on('stationsLoaded', function () {

		$scope.$watch('formdata.station', function () {

			var stationIndex = _.findIndex($scope.stations, { id: $scope.formdata.station });

			if (stationIndex != -1) {

				var id_station = $scope.formdata.station;
				$scope.getTests(id_station);

			}

			$scope.tests = null;
			$scope.formdata.test = '';
			$scope.CreateStationConfigurationForm.test.$setPristine();

		});

	});

	$scope.createStationConfiguration = function () {

		$scope.creatingConfiguration = true;
		$scope.creatingConfigurationAlert = false;

		var data = $.param($scope.formdata);

		Station.createConfiguration(data).then(function (res) {

			$scope.creatingConfiguration = false;

			if (res.data.status == "success") {

				STFcache.delete('stationsConfigAll');

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Configuration created successfully'
				};

				$location.path('/events/stations/config');

			} else {

				$scope.creatingConfigurationAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingConfiguration = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingConfigurationAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('StationConfigEditController', ['$scope', '$routeParams', '$location', '$rootScope', 'Station', 'Age', 'Auth', 'STFcache',  function ($scope, $routeParams, $location, $rootScope, Station, Age, Auth, STFcache) {
	
	var id = $routeParams.id;

	$scope.formdata = {
		station: '',
		test: '',
		age_range: '',
		distance: ''
	};

	$scope.getConfiguration = function (id) {

		$scope.loadingConfiguration = true;
		$scope.loadingConfigurationAlert = false;

		Station.getConfiguration(id).then(function (res) {

			$scope.loadingConfiguration = false;

			if (angular.isObject(res.data.response)) {

				$scope.configuration = res.data.response;
				$scope.formdata.station = $scope.configuration.id_station;
				$scope.formdata.test = $scope.configuration.id_test;
				$scope.formdata.age_range = $scope.configuration.id_age_range;
				$scope.formdata.distance = parseFloat($scope.configuration.nu_distance);
				//only take numbers
					angular.element('input[type=number]').on("keypress",function (evt) {
	    	    // if (String.fromCharCode(evt.which) == "e"){
	        	// return false;
	         //    }
	         //    if (String.fromCharCode(evt.which) == "."){
	        	// return false;
	         //    }
	            var charCode = (evt.which) ? evt.which : event.keyCode;
				//if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
				 return true;
	            

				});

			} else {

				$scope.loadingConfigurationAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingConfiguration = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingConfigurationAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getStationsAbleToConfigure = function () {

		$scope.loadingStations = true;
		$scope.loadingStationsALert = false;

		Station.getStationsAbleToConfigure().then(function (res) {

			$scope.loadingStations = false;

			if (angular.isObject(res.data.response)) {

				$scope.stations = res.data.response;
				$scope.$emit('stationsLoaded');

			} else {

				$scope.loadingStationsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStations = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStationsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAgeRange = function () {

		$scope.loadingRange = true;
		$scope.loadingRangeAlert = false;

		Age.getRange().then(function (res) {

			$scope.loadingRange = false;

			if (angular.isObject(res.data.response)) {

				$scope.range = res.data.response;

			} else {

				$scope.loadingRangeAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingRange = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingRangeAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTests = function (id_station) {

		$scope.loadingTests = true;
		$scope.loadingTestsAlert = false;

		Station.getTests(id_station).then(function (res) {

			$scope.loadingTests = false;

			if (angular.isObject(res.data.response)) {

				$scope.tests = res.data.response;

			} else {

				$scope.loadingTestsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTests = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTestsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getConfiguration(id);
	$scope.getStationsAbleToConfigure();
	$scope.getAgeRange();

	$scope.$on('stationsLoaded', function () {

		$scope.$watch('formdata.station', function () {

			var stationIndex = _.findIndex($scope.stations, { id: $scope.formdata.station });

			if (stationIndex != -1) {

				var id_station = $scope.formdata.station;
				$scope.getTests(id_station);

			} else {

				$scope.tests = null;
				$scope.formdata.test = '';
				$scope.EditStationConfigurationForm.test.$setPristine();

			}

		});

	});

	$scope.editStationConfiguration = function () {

		$scope.editingConfiguration = true;
		$scope.editingConfigurationAlert = false;

		var data = $.param($scope.formdata);

		Station.editConfiguration(id, data).then(function (res) {

			$scope.editingConfiguration = false;

			if (res.data.status == "success") {

				STFcache.delete('stationsConfigAll');
				STFcache.delete('stationConfig' + id);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Configuration updated successfully'
				};

				$location.path('/events/stations/config');

			} else {

				$scope.editingConfigurationAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.editingConfiguration = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingConfigurationAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);