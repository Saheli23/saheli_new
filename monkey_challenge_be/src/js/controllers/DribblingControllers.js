var stf = angular.module('DribblingControllers', []);

stf.controller('DribblingListController', ['$scope', '$routeParams', '$location', 'Event', 'Dribbling', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, $location, Event, Dribbling, Access, Auth, STFcache) {
	
	$scope.bulk = false;
	var values = [];

	$scope.toggle = function() {
	    if (angular.element("#mastercheckbox").is(":checked")) {
	        angular.element(".childcheckbox").prop("checked", true);
	        values = [];
	        $(".childcheckbox").each(function() {
	            if ($(this).is(":checked")) values.push($(this).val());
	        });
	    } else {
	        values = [];
	        angular.element(".childcheckbox").prop("checked", false);
	    }
	}

	$scope.togglechild = function(e) {
	    values = [];
	    $(".childcheckbox").each(function() {
	        if ($(this).is(":checked")) values.push($(this).val());
	    });
	    if (angular.element(".childcheckbox").length == angular.element(".childcheckbox:checked").length) {
	        angular.element("#mastercheckbox").prop("checked", true);
	    } else {
	        angular.element("#mastercheckbox").prop("checked", false);
	    }
	};

	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getEvent = function (id_uniq) {
		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;
		Event.getOne(id_uniq).then(function (res) {
			$scope.loadingEvent = false;
			if (angular.isObject(res.data.response)) {
				$scope.event = res.data.response;
				$scope.getAccess(1);
				if (_.findIndex($scope.event.configurations, { station: 'Dribbling' }) >= 0) {
 					$scope.$emit('eventLoaded');
				} else {
					$location.path('/events/' + $scope.id_uniq);
				}
			} else {
				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};
			}

		}, function (err) {
			$scope.loadingEvent = false;
			if (err.status == 401) {
				Auth.deleteToken();
			} else {
				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};
			}
		});
	};

	$scope.getDribblings = function(id_uniq, refresh) {
	    if (refresh !== undefined && refresh == true) STFcache.delete('dribblingEvent' + $scope.id_uniq);
	    angular.element("#mastercheckbox").prop("checked", false);
	    $scope.measurements = null;
	    $scope.loadingDribblings = true;
	    $scope.loadingDribblingsAlert = false;

	    Dribbling.getAll($scope.id_uniq).then(function(res) {
	        $scope.loadingDribblings = false;
	        if (angular.isObject(res.data.response)) {
	            $scope.measurements = res.data.response;
	        } else {
	            $scope.loadingDribblingsAlert = {
	                type: 'success',
	                message: res.data.response
	            };
	        }
	        $scope.getAccess(1);
	    }, function(err) {
	        $scope.loadingDribblings = false;
	        if (err.status == 401) {
	            Auth.deleteToken();
	        } else {
	            $scope.loadingDribblingsAlert = {
	                type: 'danger',
	                message: err.status + ' ' + err.statusText
	            };
	        }
	    });
	};

	$scope.getEvent($scope.id_uniq);
	$scope.getDribblings($scope.id_uniq);

	$scope.deleteDribbling = function(id_player) {
	    if (confirm("Are you sure to delete this Player?")) {
	        var data = $.param({
	            id_player: id_player,
	            type: 'delete'
	        });
	        Dribbling.delete(data).then(function(res) {
	            $scope.loadingDribblings = false;
	            $scope.getDribblings($scope.id_uniq, true);
	            if (angular.isObject(res.data.response)) {
	                //$scope.measurements = res.data.response;
	            } else {
	                $scope.loadingDribblingsAlert = {
	                    type: 'success',
	                    message: res.data.response
	                };
	            }
	        }, function(err) {
	            $scope.loadingDribblings = false;
	            if (err.status == 401) {
	                Auth.deleteToken();
	            } else {
	                $scope.loadingDribblingsAlert = {
	                    type: 'danger',
	                    message: err.status + ' ' + err.statusText
	                };
	            }
	        });
	    }
	};

	$scope.bulkDeleteDribbling = function() {
	    //console.log(angular.element('#bulkaction').val());
	    console.log(values);
	    action_id = angular.element('#bulkaction').val();
	    if (action_id == 1 && values.length != 0) {
	        if (confirm("Are you sure to delete this Player?")) {
	            var data = $.param({
	                id_player: values,
	                type: 'delete',
	                action: action_id
	            });
	            Dribbling.multidelete(data).then(function(res) {
	                $scope.loadingDribblings = false;

	                $scope.getDribblings($scope.id_uniq, true);
	                if (angular.isObject(res.data.response)) {
	                    //$scope.measurements = res.data.response;
	                } else {
	                    $scope.loadingDribblingsAlert = {
	                        type: 'success',
	                        message: res.data.response
	                    };
	                }
	            }, function(err) {
	                $scope.loadingDribblings = false;
	                if (err.status == 401) {
	                    Auth.deleteToken();
	                } else {
	                    $scope.loadingDribblingsAlert = {
	                        type: 'danger',
	                        message: err.status + ' ' + err.statusText
	                    };
	                }
	            });
	        }
	    } else {
	        alert("Please select atleast one to delete.");
	    }
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

	$scope.getEvent($scope.id_uniq);
	
}]);

stf.controller('DribblingCreateController', ['$scope', '$routeParams', '$location', '$filter', '$rootScope', 'Event', 'Player', 'Dribbling', 'Auth', 'STFcache', function ($scope, $routeParams, $location, $filter, $rootScope, Event, Player, Dribbling, Auth, STFcache) {
	
	$scope.id_uniq = $routeParams.id_uniq;
	$scope.player = null;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	$scope.date = null;
	$scope.maxDate = new Date();
	$scope.pickDateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: true,
		selectMonths: true,
	};

	$scope.getEvent = function (id_uniq) {
		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;
		Event.getOne(id_uniq).then(function (res) {
			$scope.loadingEvent = false;
			if (angular.isObject(res.data.response)) {
				$scope.event = res.data.response;
				if (_.findIndex($scope.event.configurations, { station: 'Dribbling' }) >= 0) {
 					$scope.eventHasDribblingZigzag = (_.findIndex($scope.event.configurations, { station: 'Dribbling', test: 'Zigzag' }) >= 0) ? true : false;
 					$scope.eventHasDribblingCircular = (_.findIndex($scope.event.configurations, { station: 'Dribbling', test: 'Circular' }) >= 0) ? true : false;

 					console.log($scope.eventHasDribblingCircular);
				} else {
					$location.path('/events/' + $scope.id_uniq);
				}
			} else {
				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};
			}
		}, function (err) {
			$scope.loadingEvent = false;
			if (err.status == 401) {
				Auth.deleteToken();
			} else {
				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};
			}
		});
	};

	$scope.getPlayer = function (id_uniq) {
		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {
			if (angular.isObject(res.data.response)) {
				return res.data.response;	
			} else {
				$scope.loadingPlayer = false;
				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};
			}
		}, function (err) {
			$scope.loadingPlayer = false;
			if (err.status == 401) {
				Auth.deleteToken();
			} else {
				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};
			}
		}).then(function (args) {
			if (args !== undefined) {
				var player = args;
				var data   = [];
				var zigzagAgeRangeData = [];
				var circularAgeRangeData = [];

				for (var i = 0; i < $scope.event.configurations.length; i++) {
					if ($scope.event.configurations[i].station == "Dribbling") {
						if (_.findIndex(data, { value: $scope.event.configurations[i].test }) == -1) {
							data.push({
								name: 'tests[]',
								value: $scope.event.configurations[i].test
							});
						};

						if ($scope.event.configurations[i].test == "Zigzag") {
							zigzagAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	
						};

						if ($scope.event.configurations[i].test == "Circular") {
							circularAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	
						};
					};
				};

				data = $.param(data);
				//$scope.player = player;

				// $scope.showZigzag = true;
				// $scope.showCircular = true;


				Dribbling.check(player.id, $scope.event.id, data).then(function (res) {
					console.log(res);
					$scope.loadingPlayer = false;
					if (res.data.player_exists == 1) {
						$scope.loadingPlayerAlert = {
							type: 'success',
							message: res.data.response
						};
					} else {
						var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));

						$scope.playerCanPerformZigzagTest = false;
						$scope.playerCanPerformCircularTest = false;
						
						if (zigzagAgeRangeData.length > 0) {
							for (var i = 0; i < zigzagAgeRangeData.length; i++) {
								var min = parseInt(zigzagAgeRangeData[i].min);
								var max = parseInt(zigzagAgeRangeData[i].max);
								if (playerAge >= min && playerAge <= max) {
									$scope.playerCanPerformZigzagTest = true;
									break;
								};
							};
						};

						if (circularAgeRangeData.length > 0) {
							for (var i = 0; i < circularAgeRangeData.length; i++) {
								var min = parseInt(circularAgeRangeData[i].min);
								var max = parseInt(circularAgeRangeData[i].max);
								if (playerAge >= min && playerAge <= max) {
									$scope.playerCanPerformCircularTest = true;
									break;
								};
							};
						};

						$scope.player = player;

						
						$scope.ZigzagTestAlert   = ($scope.playerCanPerformZigzagTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
						$scope.CircularTestAlert = ($scope.playerCanPerformCircularTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;

						$scope.showZigzag        = (res.data.response.zigzag == 2) ? true : false;
						$scope.showCircular      = (res.data.response.circular == 2) ? true : false;
					}
				}, function (err) {
					$scope.loadingPlayer = false;
					$scope.loadingPlayerAlert = {
						type: 'danger',
						message: res.data.response
					};
				});
			};
		});
	};

	$scope.changePlayer = function () {
		$scope.playerIDuniq = null;
		$scope.player = null;
		$rootScope.$emit('dribblingTestPlayerChanged');
	};

	$scope.activateTab = function (tab) {
		switch (tab) {
			case 0:
				$scope.activetab = "Zigzag";
				break;
			case 1:
				$scope.activetab = "Circular";
				break;
			default: 
				$scope.activetab = null;
				break;
		}
	};

	$scope.getEvent($scope.id_uniq);
}]);

stf.controller('DribblingEditController', ['$scope',  '$routeParams', '$location', '$filter', '$rootScope', 'Event', 'Player', 'Dribbling', 'Auth', function ($scope, $routeParams, $location, $filter, $rootScope, Event, Player, Dribbling, Auth) {
	
	$scope.id_uniq        = $routeParams.id_uniq;
	$scope.player_id_uniq = $routeParams.playerid;
	$scope.player         = null;
	$scope.ball_weight    = 0;
	$scope.randomNum      = Math.floor((Math.random() * 10) + 1);

	$scope.getEvent = function (id_uniq) {
		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;
		Event.getOne(id_uniq).then(function (res) {
			$scope.loadingEvent = false;
			if (angular.isObject(res.data.response)) {
				$scope.event = res.data.response;
				$scope.measurement_system = ($scope.event.stf_measurement_abbr != "MKS") ? "YLS" : "MKS";
				if (_.findIndex($scope.event.configurations, { station: 'Dribbling' }) >= 0) {
 					
 					$scope.eventHasDribblingZigzag = (_.findIndex($scope.event.configurations, { station: 'Dribbling', test: 'Zigzag' }) >= 0) ? true : false;
 					$scope.eventHasDribblingCircular = (_.findIndex($scope.event.configurations, { station: 'Dribbling', test: 'Circular' }) >= 0) ? true : false;
 					$scope.getPlayer($scope.player_id_uniq);

 				} else $location.path('/events/' + $scope.id_uniq);
			} else $scope.loadingEventAlert = { type: 'success',message: res.data.response };
		}, function (err) {
			$scope.loadingEvent = false;
			if (err.status == 401) Auth.deleteToken();
			else {
				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};
			}
		});
	};

	$scope.getPlayer = function (id_uniq) {
		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;
		Player.getOne(id_uniq).then(function (res) {
			if (angular.isObject(res.data.response)) {
				return res.data.response;	
			} else {
				$scope.loadingPlayer = false;
				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};
			}
		}, function (err) {
			$scope.loadingPlayer = false;
			if (err.status == 401) {
				Auth.deleteToken();
			} else {
				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};
			}
		}).then(function (args) {
			if (args !== undefined) {
				var player               = args;
				var data                 = [];
				var zigzagAgeRangeData   = [];
				var circularAgeRangeData = [];

				for (var i = 0; i < $scope.event.configurations.length; i++) {
					if ($scope.event.configurations[i].station == "Dribbling") {
						if (_.findIndex(data, { value: $scope.event.configurations[i].test }) == -1) {
							data.push({
								name: 'tests[]',
								value: $scope.event.configurations[i].test
							});
						};

						if ($scope.event.configurations[i].test == "Zigzag") {
							zigzagAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	
						};

						if ($scope.event.configurations[i].test == "Circular") {
							circularAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	
						};
					};
				};

				data = $.param(data);

				var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));
				
				$scope.playerCanPerformZigzagTest = false;
				$scope.playerCanPerformCircularTest = false;

				if (zigzagAgeRangeData.length > 0) {
					for (var i = 0; i < zigzagAgeRangeData.length; i++) {
						var min = parseInt(zigzagAgeRangeData[i].min);
						var max = parseInt(zigzagAgeRangeData[i].max);
						if (playerAge >= min && playerAge <= max) {
							$scope.playerCanPerformZigzagTest = true;
							break;
						};
					};
				};

				if (circularAgeRangeData.length > 0) {
					for (var i = 0; i < circularAgeRangeData.length; i++) {
						var min = parseInt(circularAgeRangeData[i].min);
						var max = parseInt(circularAgeRangeData[i].max);
						if (playerAge >= min && playerAge <= max) {
							$scope.playerCanPerformCircularTest = true;
							break;
						};
					};
				};

				$scope.player = player;
			};
		});
	};

	$scope.changePlayer = function () {
		$scope.playerIDuniq = null;
		$scope.player = null;
		$rootScope.$emit('dribblingTestPlayerChanged');
	};

	$scope.activateTab = function (tab) {
		switch (tab) {
			case 0:
				$scope.activetab = "Zigzag";
				break;
			case 1:
				$scope.activetab = "Circular";
				break;
			default: 
				$scope.activetab = null;
				break;
		}
	};

	$scope.getEvent($scope.id_uniq);
}]);