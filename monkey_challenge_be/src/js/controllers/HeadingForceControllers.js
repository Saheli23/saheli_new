var stf = angular.module('HeadingForceControllers', []);

stf.controller('HeadingForceListController', ['$scope', '$routeParams', '$location', 'Event', 'HeadingForce', 'Auth', 'STFcache', function ($scope, $routeParams, $location, Event, HeadingForce, Auth, STFcache) {
	
	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;

				if (_.findIndex($scope.event.configurations, { station: 'Heading Force' }) >= 0) {

 					$scope.$emit('eventLoaded');

				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getHeadings = function (id_uniq, refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('headingEvent' + $scope.id_uniq);

		$scope.measurements = null;
		$scope.loadingHeadings = true;
		$scope.loadingHeadingsAlert = false;

		HeadingForce.getAll($scope.id_uniq).then(function (res) {

			$scope.loadingHeadings = false;

			if (angular.isObject(res.data.response)) {

				$scope.measurements = res.data.response;

			} else {

				$scope.loadingHeadingsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingHeadings = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingHeadingsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.deletePlayerEvent = function( player_id, ind ){

		if(confirm("Are you sure to delete this Player")){
			var data = $.param({id_player:player_id,type:'delete'});
			HeadingForce.deleteForce(data).then(function (res) {
				$scope.loadingHeadings = false;
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;
				} else {
					$scope.loadingHeadingsAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingHeadings = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingHeadingsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	}

	$scope.updatePlayerEvent = function( player_id ){
		var data = $.param({id_player:player_id,type:'update'});
		HeadingForce.deleteForce(data).then(function (res) {

			$scope.loadingHeadings = false;
			if (angular.isObject(res.data.response)) {

				//$scope.measurements = res.data.response;

			} else {
				$scope.loadingHeadingsAlert = {type: 'success',message: res.data.response};
			}

		}, function (err) {

			$scope.loadingHeadings = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingHeadingsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };

			}

		});
	}

	$scope.getEvent($scope.id_uniq);
	$scope.getHeadings($scope.id_uniq);
	
	/*$scope.orderOptions = [
		{ value: 'name', text: 'Name' },
		{ value: '-date', text: 'Date' }
	];*/
	
}]);

stf.controller('HeadingForceCreateController', ['$scope', '$routeParams', '$location', '$rootScope', '$filter', 'Event', 'Player', 'HeadingForce', 'Anthropometric', 'Auth', 'STFcache', function ($scope, $routeParams, $location, $rootScope, $filter, Event, Player, HeadingForce, Anthropometric, Auth, STFcache) {
	
	$scope.id_uniq   = $routeParams.id_uniq;
	$scope.player    = null;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);


	/* FORCE TEST CODE */
	$scope.forceTest = false;
	$scope.forcedata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		distance: '',
		attempt : [ '', '', '' ],
		result  : [ '', '', '' ]
	};

	$scope.headForcedata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		distance: '',
		attempt_one : '',
		attempt_two : '',
		attempt_three : '',
		result_one : '',
		result_two : '',
		result_three : ''
	};

	$scope.forceFieldValidated = [ false, false, false ];

	$scope.startForceTest = function () { 
		return $scope.forceTest = true; 
	};

	$scope.validateForceField = function (field) {
		$scope.forceFieldValidated[field] = true;

		var velocityInitial = 0;
		var velocityFinal   = parseFloat( $scope.forcedata.attempt[field] );
		var distance        = $scope.forcedata.distance;
		var mass            = $scope.ball_weight / 1000;
		var force           = mass * ( ( Math.pow( velocityFinal, 2 ) - velocityInitial ) / ( 2 * distance ) );

		$scope.forcedata.result[field] = force.toFixed(2);

		if( field === 2 ) $scope.CreateHeadingForceFormCompleted = true;
	};

	/* END FORCE TEST CODE */

	$scope.getEvent = function (id_uniq) {
		$scope.loadingEvent      = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event              = res.data.response;
				$scope.measurement_system = ($scope.event.stf_measurement_abbr != "MKS") ? "YLS" : "MKS";

				if (_.findIndex($scope.event.configurations, { station: 'Heading Force' }) >= 0) {

 					$scope.forcedata.id_event   = $scope.event.id;
 					$scope.eventHasHeadingForce = (_.findIndex($scope.event.configurations, { station: 'Heading Force', test: 'Force' }) >= 0) ? true : false;
 				
 				} else $location.path('/events/' + $scope.id_uniq);

			} else $scope.loadingEventAlert = {type: 'success',message: res.data.response};

		}, function (err) {

			$scope.loadingEvent = false;
			if (err.status == 401) Auth.deleteToken();
			else $scope.loadingEventAlert = {type: 'danger',message: err.status + ' ' + err.statusText};

		});
	};

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer      = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {
			if (angular.isObject(res.data.response)) {
				$scope.forcedata.id_player = res.data.response.id;
				return res.data.response;
			} else {
				$scope.loadingPlayer      = false;
				$scope.loadingPlayerAlert = { type: 'success', message: res.data.response };
			}
		}, function (err) {

			$scope.loadingPlayer = false;
			if (err.status == 401) Auth.deleteToken();
			else  $scope.loadingPlayerAlert = { type: 'danger',message: err.status + ' ' + err.statusText};

		}).then(function (args) {

			if (args != undefined) {

				var player            = args;
				var data              = [];
				var forceAgeRangeData = [];

				Anthropometric.getWeight(player.id, $scope.event.id).then(function (res) {

					if (angular.isObject(res.data.response)) {
						return res.data.response;
					} else {
						$scope.loadingPlayer = false;
						$scope.loadingPlayerAlert = { type: 'success', message: res.data.response };
					}

				}, function (err) {

					$scope.loadingPlayer = false;
					$scope.loadingPlayerAlert = { type: 'danger',message: err.status + ' ' + err.statusText};

				}).then(function (args) {
					if (args !== undefined) {
						$scope.weight = args.nu_weight;
						for (var i = 0; i < $scope.event.configurations.length; i++) {
							if ($scope.event.configurations[i].station == "Heading Force") {
								if (_.findIndex( data, { value: $scope.event.configurations[i].test } ) == -1 ) {
									data.push({ name: 'tests[]', value: $scope.event.configurations[i].test });
								};

								if ($scope.event.configurations[i].test == "Force") {
									forceAgeRangeData.push({
										test: $scope.event.configurations[i].test,
										min: $scope.event.configurations[i].nu_min_age,
										max: $scope.event.configurations[i].nu_max_age,
										ball_weight: $scope.event.configurations[i].nu_ball_weight
									});
								};
							};
						};
						console.log(data);
						data = $.param(data);


						HeadingForce.check(player.id, $scope.event.id, data).then(function (res) {

							$scope.loadingPlayer = false;

							if (res.data.player_exists == 1) {
								$scope.loadingPlayerAlert = { type: 'success', message: res.data.response };
							} else {

								var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));
								//$scope.playerCanPerformForceTest = false;

								if (forceAgeRangeData.length > 0) {

									console.log(forceAgeRangeData);

									for (var i = 0; i < forceAgeRangeData.length; i++) {

										var min = parseInt(forceAgeRangeData[i].min);
										var max = parseInt(forceAgeRangeData[i].max);

										if (playerAge >= min && playerAge <= max) {

											$scope.playerCanPerformForceTest = true;
											$scope.ball_weight = parseInt(forceAgeRangeData[i].ball_weight);
											break;
										} 
									};
								};

								$scope.playerCanPerformForceTest = true;

								$scope.player = player;
								$scope.ForceTestAlert = ($scope.playerCanPerformForceTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
								$scope.showForce = (res.data.response.force == 2) ? true : false;
							}
						}, function (err) {
							$scope.loadingPlayer = false;
							$scope.loadingPlayerAlert = { type: 'danger', message: res.data.response };
						});	
					};	
				});
			};
		});
	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$scope.forcedata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			distance: '',
			attempt : [],
			result : []
		};
		
		//$scope.CreateHeadingAimForm.$setPristine();
		$scope.CreateHeadingForceForm.$setPristine();
		//$scope.CreateHeadingJumpForm.$setPristine();

	};

	$scope.setForceData = function( source ){
		$scope.headForcedata.id_player        = source.id_player;
		$scope.headForcedata.id_event         = source.id_event;
		$scope.headForcedata.measurement_date = source.measurement_date; 
		$scope.headForcedata.distance         = source.distance; 
		$scope.headForcedata.attempt_one      = source.attempt[0]; 
		$scope.headForcedata.attempt_two      = source.attempt[1]; 
		$scope.headForcedata.attempt_three    = source.attempt[2];
	}

	$scope.createHeadingForce = function () {

		$scope.creatingHeadingForce = true;
		$scope.creatingHeadingForceAlert = false;

		$scope.setForceData( $scope.forcedata );
		
		var data = $.param($scope.headForcedata);

		HeadingForce.createForce(data).then(function (res) {

			$scope.creatingHeadingForce = false;

			if (res.data.status == "success") {

				STFcache.delete('headingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/headingforce');

			} else {

				$scope.creatingHeadingForceAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingHeadingForce = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingHeadingForceAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getEvent($scope.id_uniq);
}]);

stf.controller('HeadingForceResultsController', ['$scope', function ($scope) {
	
}]);

stf.controller('HeadingForceEditController', ['$scope', function ($scope) {
	
}]);