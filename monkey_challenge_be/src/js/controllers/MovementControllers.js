var stf = angular.module('MovementControllers', []);

stf.controller('MovementListController', ['$scope', '$routeParams', '$location', 'Event', 'Movement', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, $location, Event, Movement, Access, Auth, STFcache) {
	
	$scope.bulk=false;
   	var	values=[];
    $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
	    $(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
			 values=[];
			$(".childcheckbox").each(function(){
		        if($(this).is(":checked"))
		            values.push($(this).val());
		    });
		    //alert(values);
		   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
		   	angular.element("#mastercheckbox").prop("checked",true);
		   } 
		   else{
		
		    angular.element("#mastercheckbox").prop("checked",false);
		      }
	};
    

	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;

				if (_.findIndex($scope.event.configurations, { station: 'Movement' }) >= 0) {

 					$scope.$emit('eventLoaded');

				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getMovements = function (id_uniq, refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('movementEvent' + $scope.id_uniq);
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.measurements = null;
		$scope.loadingMovements = true;
		$scope.loadingMovementsAlert = false;

		Movement.getAll($scope.id_uniq).then(function (res) {

			$scope.loadingMovements = false;

			if (angular.isObject(res.data.response)) {

				$scope.measurements = res.data.response;
				

			} else {

				$scope.loadingMovementsAlert = {
					type: 'success',
					message: res.data.response
				};

			}
			$scope.getAccess(1);
		}, function (err) {

			$scope.loadingMovements = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingMovementsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getEvent($scope.id_uniq);
	$scope.getMovements($scope.id_uniq);

	$scope.deleteMovement = function(id_player){

			if(confirm("Are you sure to delete this Player?")){
				var data = $.param({id_player:id_player,type:'delete'});
				Movement.delete(data).then(function (res) {
					$scope.loadingMovements = false;
					
					$scope.getMovements($scope.id_uniq,true);
					if (angular.isObject(res.data.response)) {
						//$scope.measurements = res.data.response;

					} else {
						$scope.loadingMovementsAlert = {type: 'success',message: res.data.response};
					}

				}, function (err) {

					$scope.loadingMovements = false;

					if (err.status == 401) {
						Auth.deleteToken();
					} else {
						$scope.loadingMovementsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
					}
				});
			}
		};

	$scope.bulkDeleteMovement = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:values,type:'delete',action:action_id});
			Movement.multidelete(data).then(function (res) {
				$scope.loadingMovements = false;
				
				$scope.getMovements($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingMovementsAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingMovements = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingMovementsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one to delete.");
	 }
	};


	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

}]);

stf.controller('MovementCreateController', ['$scope',  '$routeParams', '$location', '$filter', '$rootScope', 'Event', 'Player', 'Movement', 'Auth', function ($scope, $routeParams, $location, $filter, $rootScope, Event, Player, Movement, Auth) {
	
	$scope.id_uniq = $routeParams.id_uniq;
	$scope.player = null;
	$scope.ball_weight = 0;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;
				$scope.measurement_system = ($scope.event.stf_measurement_abbr != "MKS") ? "YLS" : "MKS";

				if (_.findIndex($scope.event.configurations, { station: 'Movement' }) >= 0) {

 					/*$scope.aimdata.id_event = $scope.event.id;
 					$scope.forcedata.id_event = $scope.event.id;*/

 					$scope.eventHasMovementLinear = (_.findIndex($scope.event.configurations, { station: 'Movement', test: 'Lineal' }) >= 0) ? true : false;
 					$scope.eventHasMovementLateral = (_.findIndex($scope.event.configurations, { station: 'Movement', test: 'Lateral' }) >= 0) ? true : false;
 					$scope.eventHasMovementZigzag = (_.findIndex($scope.event.configurations, { station: 'Movement', test: 'Zigzag' }) >= 0) ? true : false;

 				} else {	

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			if (angular.isObject(res.data.response)) {

				return res.data.response;	

			} else {

				$scope.loadingPlayer = false;

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		}).then(function (args) {

			if (args !== undefined) {

				var player = args;
				var data = [];
				var linearAgeRangeData = [];
				var lateralAgeRangeData = [];
				var zigzagAgeRangeData = [];

				for (var i = 0; i < $scope.event.configurations.length; i++) {
					
					if ($scope.event.configurations[i].station == "Movement") {

						if (_.findIndex(data, { value: $scope.event.configurations[i].test }) == -1) {

							data.push({
								name: 'tests[]',
								value: $scope.event.configurations[i].test
							});

						};

						if ($scope.event.configurations[i].test == "Lineal") {

							linearAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	

						};

						if ($scope.event.configurations[i].test == "Lateral") {

							lateralAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	

						};

						if ($scope.event.configurations[i].test == "Zigzag") {

							zigzagAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	

						};

					};

				};

				data = $.param(data);

				Movement.check(player.id, $scope.event.id, data).then(function (res) {

					$scope.loadingPlayer = false;

					if (res.data.player_exists == 1) {

						$scope.loadingPlayerAlert = {
							type: 'success',
							message: res.data.response
						};

					} else {

						var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));

						$scope.playerCanPerformLinearTest = false;
						$scope.playerCanPerformLateralTest = false;
						$scope.playerCanPerformZigzagTest = false;

						if (linearAgeRangeData.length > 0) {

							for (var i = 0; i < linearAgeRangeData.length; i++) {

								var min = parseInt(linearAgeRangeData[i].min);
								var max = parseInt(linearAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformLinearTest = true;
									break;

								};

							};

						};

						if (lateralAgeRangeData.length > 0) {

							for (var i = 0; i < lateralAgeRangeData.length; i++) {

								var min = parseInt(lateralAgeRangeData[i].min);
								var max = parseInt(lateralAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformLateralTest = true;
									break;

								};

							};

						};

						if (zigzagAgeRangeData.length > 0) {

							for (var i = 0; i < zigzagAgeRangeData.length; i++) {

								var min = parseInt(zigzagAgeRangeData[i].min);
								var max = parseInt(zigzagAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformZigzagTest = true;
									break;

								};

							};

						};

						$scope.player = player;

						$scope.LinearTestAlert = ($scope.playerCanPerformLinearTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
						$scope.LateralTestAlert = ($scope.playerCanPerformLateralTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
						$scope.ZigzagTestAlert = ($scope.playerCanPerformZigzagTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;

						$scope.showLinear = (res.data.response.linear == 2) ? true : false;
						$scope.showLateral = (res.data.response.lateral == 2) ? true : false;
						$scope.showZigzag = (res.data.response.zigzag == 2) ? true : false;

					}

				}, function (err) {

					$scope.loadingPlayer = false;

					$scope.loadingPlayerAlert = {
						type: 'danger',
						message: res.data.response
					};

				});

			};

		});

	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$rootScope.$emit('movementTestPlayerChanged');

	};

	$scope.activateTab = function (tab) {

		switch (tab) {

			case 0:
				$scope.activetab = "Lineal";
				break;
			case 1:
				$scope.activetab = "Lateral";
				break;
			case 2:
				$scope.activetab = "Zigzag";
				break;
			default: 
				$scope.activetab = null;
				break;

		}

	};

	$scope.getEvent($scope.id_uniq);

}]);

stf.controller('MovementEditController', ['$scope',  '$routeParams', '$location', '$filter', '$rootScope', 'Event', 'Player', 'Movement', 'Auth', function ($scope, $routeParams, $location, $filter, $rootScope, Event, Player, Movement, Auth) {
	
	$scope.id_uniq        = $routeParams.id_uniq;
	$scope.player_id_uniq = $routeParams.playerid;
	$scope.player         = null;
	$scope.ball_weight    = 0;
	$scope.randomNum      = Math.floor((Math.random() * 10) + 1);

	$scope.getEvent = function (id_uniq) {
		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;
		Event.getOne(id_uniq).then(function (res) {
			$scope.loadingEvent = false;
			if (angular.isObject(res.data.response)) {
				$scope.event = res.data.response;
				$scope.measurement_system = ($scope.event.stf_measurement_abbr != "MKS") ? "YLS" : "MKS";
				if (_.findIndex($scope.event.configurations, { station: 'Movement' }) >= 0) {
 					$scope.eventHasMovementLinear = (_.findIndex($scope.event.configurations, { station: 'Movement', test: 'Lineal' }) >= 0) ? true : false;
 					$scope.eventHasMovementLateral = (_.findIndex($scope.event.configurations, { station: 'Movement', test: 'Lateral' }) >= 0) ? true : false;
 					$scope.eventHasMovementZigzag = (_.findIndex($scope.event.configurations, { station: 'Movement', test: 'Zigzag' }) >= 0) ? true : false;
 					$scope.getPlayer($scope.player_id_uniq);
 				} else $location.path('/events/' + $scope.id_uniq);
			} else $scope.loadingEventAlert = { type: 'success',message: res.data.response };
		}, function (err) {
			$scope.loadingEvent = false;
			if (err.status == 401) Auth.deleteToken();
			else {
				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};
			}
		});
	};

	$scope.getPlayer = function (id_uniq) {
		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;
		Player.getOne(id_uniq).then(function (res) {
			if (angular.isObject(res.data.response)) {
				return res.data.response;	
			} else {
				$scope.loadingPlayer = false;
				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};
			}
		}, function (err) {
			$scope.loadingPlayer = false;
			if (err.status == 401) {
				Auth.deleteToken();
			} else {
				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};
			}
		}).then(function (args) {
			if (args !== undefined) {
				var player = args;
				var data = [];
				var linearAgeRangeData = [];
				var lateralAgeRangeData = [];
				var zigzagAgeRangeData = [];

				for (var i = 0; i < $scope.event.configurations.length; i++) {
					if ($scope.event.configurations[i].station == "Movement") {
						if (_.findIndex(data, { value: $scope.event.configurations[i].test }) == -1) {
							data.push({
								name: 'tests[]',
								value: $scope.event.configurations[i].test
							});
						};

						if ($scope.event.configurations[i].test == "Lineal") {
							linearAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	
						};

						if ($scope.event.configurations[i].test == "Lateral") {
							lateralAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	
						};

						if ($scope.event.configurations[i].test == "Zigzag") {
							zigzagAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	
						};
					};
				};

				data = $.param(data);

				var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));
				$scope.playerCanPerformLinearTest = false;
				$scope.playerCanPerformLateralTest = false;
				$scope.playerCanPerformZigzagTest = false;
				if (linearAgeRangeData.length > 0) {
					for (var i = 0; i < linearAgeRangeData.length; i++) {
						var min = parseInt(linearAgeRangeData[i].min);
						var max = parseInt(linearAgeRangeData[i].max);
						if (playerAge >= min && playerAge <= max) {
							$scope.playerCanPerformLinearTest = true;
							break;
						};
					};
				};

				if (lateralAgeRangeData.length > 0) {
					for (var i = 0; i < lateralAgeRangeData.length; i++) {
						var min = parseInt(lateralAgeRangeData[i].min);
						var max = parseInt(lateralAgeRangeData[i].max);
						if (playerAge >= min && playerAge <= max) {
							$scope.playerCanPerformLateralTest = true;
							break;
						};
					};
				};

				if (zigzagAgeRangeData.length > 0) {
					for (var i = 0; i < zigzagAgeRangeData.length; i++) {
						var min = parseInt(zigzagAgeRangeData[i].min);
						var max = parseInt(zigzagAgeRangeData[i].max);
						if (playerAge >= min && playerAge <= max) {
							$scope.playerCanPerformZigzagTest = true;
							break;
						};
					};
				};

				$scope.player = player;
			};
		});
	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$rootScope.$emit('movementTestPlayerChanged');

	};

	$scope.activateTab = function (tab) {

		switch (tab) {

			case 0:
				$scope.activetab = "Lineal";
				break;
			case 1:
				$scope.activetab = "Lateral";
				break;
			case 2:
				$scope.activetab = "Zigzag";
				break;
			default: 
				$scope.activetab = null;
				break;

		}

	};

	$scope.getEvent($scope.id_uniq);

}]);