var stf = angular.module('stf', [
	'uiGmapgoogle-maps',
	'ui.bootstrap',
	'ui.mask',
	'wt.responsive',
	'pickadate',
	'mdo-angular-cryptography',
	'ngRoute',
	'ngMessages',
	'ngSanitize',
	'MainControllers',
	'MainDirectives',
	'MainFilters',
	'AuthServices',
	'CacheServices',
	'UserControllers',
	'UserServices',
	'CharityControllers',
	'EventControllers',
	'EventServices',
	'StationConfigControllers',
	'StationConfigServices',
	'AnthropometricControllers',
	'AnthropometricServices',
	'DribblingControllers',
	'DribblingDirectives',
	'DribblingServices',
	'KickingControllers',
	'KickingServices',
	'MovementControllers',
	'MovementDirectives',
	'MovementServices',
	'HeadingControllers',
	'HeadingServices',
	'PlayerControllers',
	'PlayerServices',
	'PlayerDirectives',
	'TeamControllers',
	'TeamServices',
	'VenueControllers',
	'VenueServices',
	'UploadControllers',
	'UploadServices',
	'CountryServices',
	'GenderServices',
	'MeasurementServices',
	'ImageServices',
	'AgeServices',
	'SectionServices',
	'AccessServices',
	'ForgotPasswordServices',
	'CommonServices',
	'ChallengesControllers',
	'ChallengesServices',
	'ChallengeTypeServices',
	'summernote',
	'ContentControllers',
	'ContentServices',
	'MediaServices',
	'slickCarousel',
	'ngCookies',
	'ui.bootstrap'
]);

stf.config(['$routeProvider', '$locationProvider', 'uiGmapGoogleMapApiProvider', '$cryptoProvider', function ($routeProvider, $locationProvider, uiGmapGoogleMapApiProvider, $cryptoProvider) {
	
	uiGmapGoogleMapApiProvider.configure({
		v: '3.20',
		libraries: 'places'
	});

	$cryptoProvider.setCryptographyKey('`I<a@4z5Od(7`W_');

	$routeProvider
		.when('/', {
			templateUrl: 'dist/views/login.html',
			controller: 'LoginController',
			resolve: ['Auth', function (Auth) {
				Auth.goToDashboardIfToken();
			}]
		})
		.when('/home', {
			redirectTo: '/'
		})
		.when('/forgot-password', {
			templateUrl: 'dist/views/password.forgot.html',
			controller: 'PasswordForgotController',
			resolve: ['Auth', function (Auth) {
				Auth.goToDashboardIfToken();
			}]
		})
		.when('/password-recover', {
			templateUrl: 'dist/views/password.recover.html',
			controller: 'PasswordRecoverController',
			resolve: ['Auth', function (Auth) {
				Auth.goToDashboardIfToken();
			}]
		})
		.when('/dashboard', {
			templateUrl: 'dist/views/dashboard.html',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Dashboard'
			}
		})
		.when('/upload/:owner/:uniqid', {
			templateUrl: 'dist/views/upload.html',
			controller: 'ProfilePictureUploadController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}]
		})
		.when('/set_image/:owner/:id_uniq', {
			templateUrl: 'dist/views/set.image.html',
			controller: 'setImageController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}]
		})
		.when('/users', {
			templateUrl: 'dist/views/user.list.html',
			controller: 'UserListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Users'
			}
		})
		.when('/users/create', {
			templateUrl: 'dist/views/user.create.html',
			controller: 'UserCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Users'
			}
		})
		.when('/users/:id_uniq', {
			templateUrl: 'dist/views/user.details.html',
			controller: 'UserDetailsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Users'
			}
		})
		.when('/users/:id_uniq/edit', {
			templateUrl: 'dist/views/user.edit.html',
			controller: 'UserEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Users'
			}
		})
		.when('/charity', {
			templateUrl: 'dist/views/charity.list.html',
			controller: 'CharityListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Charity'
			}
		})
		.when('/charity/create', {
			templateUrl: 'dist/views/charity.create.html',
			controller: 'CharityCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Charity'
			}
		})
		.when('/charity/:id_uniq', {
			templateUrl: 'dist/views/charity.details.html',
			controller: 'CharityDetailsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Charity'
			}
		})
		.when('/charity/:id_uniq/edit', {
			templateUrl: 'dist/views/charity.edit.html',
			controller: 'CharityEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Charity'
			}
		})
		.when('/events', {
			templateUrl: 'dist/views/event.list.html',
			controller: 'EventListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/create', {
			templateUrl: 'dist/views/event.create.html',
			controller: 'EventCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/stations/config', {
			templateUrl: 'dist/views/station.config.list.html',
			controller: 'StationConfigListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/stations/config/create', {
			templateUrl: 'dist/views/station.config.create.html',
			controller: 'StationConfigCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/stations/config/:id/edit', {
			templateUrl: 'dist/views/station.config.edit.html',
			controller: 'StationConfigEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq', {
			templateUrl: 'dist/views/event.details.html',
			controller: 'EventDetailsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/edit', {
			templateUrl: 'dist/views/event.edit.html',
			controller: 'EventEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/anthropometrics', {
			templateUrl: 'dist/views/anthropometrics.list.html',
			controller: 'AnthropometricListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/anthropometrics/create', {
			templateUrl: 'dist/views/anthropometrics.create.html',
			controller: 'AnthropometricCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/anthropometrics/:id_uniq_player', {
			templateUrl: 'dist/views/anthropometrics.results.html',
			controller: 'AnthropometricDetailController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/anthropometrics/:id_uniq/edit', {
			templateUrl: 'dist/views/anthropometrics.edit.html',
			controller: 'AnthropometricEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/dribbling', {
			templateUrl: 'dist/views/dribbling.list.html',
			controller: 'DribblingListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/dribbling/create', {
			templateUrl: 'dist/views/dribbling.create.html',
			controller: 'DribblingCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/dribbling/:playerid/edit', {
			templateUrl: 'dist/views/dribbling.edit.html',
			controller: 'DribblingEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/kicking', {
			templateUrl: 'dist/views/kicking.list.html',
			controller: 'KickingListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/kicking/create', {
			templateUrl: 'dist/views/kicking.create.html',
			controller: 'KickingCreateController',
			resolve: ['Auth', function (Auth){
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id/kicking/:playerid/edit', {
			templateUrl: 'dist/views/kicking.edit.html',
			controller: 'KickingEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/movement', {
			templateUrl: 'dist/views/movement.list.html',
			controller: 'MovementListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/movement/create', {
			templateUrl: 'dist/views/movement.create.html',
			controller: 'MovementCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/movement/:playerid/edit', {
			templateUrl: 'dist/views/movement.edit.html',
			controller: 'MovementEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/heading', {
			templateUrl: 'dist/views/heading.list.html',
			controller: 'HeadingListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id_uniq/heading/create', {
			templateUrl: 'dist/views/heading.create.html',
			controller: 'HeadingCreateController',
			resolve: ['Auth', function (Auth){
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id/heading/:playerid', {
			templateUrl: 'dist/views/heading.results.html',
			controller: 'HeadingResultsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/events/:id/heading/:playerid/edit', {
			templateUrl: 'dist/views/heading.edit.html',
			controller: 'HeadingEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})
		.when('/players', {
			templateUrl: 'dist/views/player.list.html',
			controller: 'PlayerListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})
		.when('/players/create', {
			templateUrl: 'dist/views/player.create.html',
			controller: 'PlayerCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})
		.when('/players/:id_uniq', {
			templateUrl: 'dist/views/player.details.html',
			controller: 'PlayerDetailsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})
		.when('/players/:id_uniq/edit', {
			templateUrl: 'dist/views/player.edit.html',
			controller: 'PlayerEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})
		.when('/players/:id_uniq/teams', {
			templateUrl: 'dist/views/player.teams.html',
			controller: 'PlayerTeamsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})
		.when('/players/:id_uniq/teams/create', {
			templateUrl: 'dist/views/player.team.create.html',
			controller: 'PlayerTeamCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})
		.when('/players/:id_uniq/teams/:id_team_player/edit', {
			templateUrl: 'dist/views/player.team.edit.html',
			controller: 'PlayerTeamEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})
		.when('/teams', {
			templateUrl: 'dist/views/team.list.html',
			controller: 'TeamListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Teams'
			}
		})
		.when('/teams/create', {
			templateUrl: 'dist/views/team.create.html',
			controller: 'TeamCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Teams'
			}
		})
		.when('/teams/:id_uniq', {
			templateUrl: 'dist/views/team.details.html',
			controller: 'TeamDetailsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Teams'
			}
		})
		.when('/teams/:id_uniq/edit', {
			templateUrl: 'dist/views/team.edit.html',
			controller: 'TeamEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Teams'
			}
		})
		.when('/venues', {
			templateUrl: 'dist/views/venue.list.html',
			controller: 'VenueListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Venues'
			}
		})
		.when('/venues/create', {
			templateUrl: 'dist/views/venue.create.html',
			controller: 'VenueCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Venues'
			}
		})
		.when('/venues/:id_uniq', {
			templateUrl: 'dist/views/venue.details.html',
			controller: 'VenueDetailsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Venues'
			}
		})
		.when('/venues/:id_uniq/edit', {
			templateUrl: 'dist/views/venue.edit.html',
			controller: 'VenueEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Venues'
			}
		})
		.when('/challenges', {
			templateUrl: 'dist/views/challenges.list.html',
			controller: 'ChallengesListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Challenges'
			}
		})
		.when('/challenges/:id', {
			templateUrl: 'dist/views/challenges.details.html',
			controller: 'ChallengesDetailsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Challenges'
			}
		})
		.when('/challenge/create', {
			templateUrl: 'dist/views/challenge.create.html',
			controller: 'ChallengeCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Challenges'
			}
		})
		.when('/challenge/:id/edit', {
			templateUrl: 'dist/views/challenge.edit.html',
			controller: 'ChallengeEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Challenges'
			}
		})
		.when('/content/create', {
			templateUrl: 'dist/views/content.html',
			controller: 'ContentCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Content'
			}
		})
		.when('/content', {
			templateUrl: 'dist/views/content.list.html',
			controller: 'ContentListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Content'
			}
		})
		.when('/content/:id', {
			templateUrl: 'dist/views/content.details.html',
			controller: 'ContentDetailsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Content'
			}
		})
		.when('/content/:id/edit', {
			templateUrl: 'dist/views/content.edit.html',
			controller: 'ContentEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Content'
			}
		})
		
		.otherwise({ redirectTo: '/' });

	$locationProvider.html5Mode(false);

}]);

stf.run(['$rootScope', '$http', '$window', '$crypto', function ($rootScope, $http, $window, $crypto) {

	$rootScope.$on('$routeChangeSuccess', function (e, current, previous) {
		$rootScope.currentlocation = (angular.isDefined(current.data) && angular.isDefined(current.data.title)) ? current.data.title : 'STF';
	});
	
	$rootScope.tooltipMessageAlert = false;
	$rootScope.currentYear         = new Date().getFullYear();
	
	$http.defaults.headers.post = { 'Content-Type': 'application/x-www-form-urlencoded' };
	$http.defaults.headers.put  = { 'Content-Type': 'application/x-www-form-urlencoded' };
}]);