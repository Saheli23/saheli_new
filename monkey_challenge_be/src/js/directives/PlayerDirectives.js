var stf = angular.module('PlayerDirectives', []);

stf.directive('readHeadImage', ['$timeout', function ($timeout) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			$(iElement).on('change', function () {

				scope.playerdata.head.file = '';

				var preview = $('#headpicture-preview');
				var file = $(iElement)[0].files[0];
				var reader = new FileReader();
				var types = ['image/png', 'image/jpeg'];

				preview.cropper('destroy');

				if (file) {

					if (_.indexOf(types, file.type) != -1) {

						scope.invalidHeadPictureFile = false;

						reader.onloadend = function () {

							preview.attr('src', reader.result);

						}
						reader.readAsDataURL(file);

						$timeout(function () {

							scope.$emit('headImgReady');

						}, 500);

					} else {

						scope.invalidHeadPictureFile = true;
						preview.attr('src', '');

					}

				} else {

					preview.attr('src', '');

				}

				scope.$apply();

			});

		}
	};
}]);

stf.directive('readBodyImage', ['$timeout', function ($timeout) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			$(iElement).on('change', function () {

				scope.playerdata.body.file = '';

				var preview = $('#bodypicture-preview');
				var file = $(iElement)[0].files[0];
				var reader = new FileReader();
				var types = ['image/png', 'image/jpeg'];

				preview.cropper('destroy');

				if (file) {

					if (_.indexOf(types, file.type) != -1) {

						scope.invalidBodyPictureFile = false;

						reader.onloadend = function () {

							preview.attr('src', reader.result);

						}
						reader.readAsDataURL(file);

						$timeout(function () {

							scope.$emit('bodyImgReady');

						}, 500);

					} else {

						scope.invalidBodyPictureFile = true;
						preview.attr('src', '');

					}

				} else {

					preview.attr('src', '');

				}

				scope.$apply();

			});

		}
	};
}]);

stf.directive('headpictureCropper', [function () {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			scope.$on('headImgReady', function () {

				$(iElement).cropper({
					aspectRatio: 1,
					dragCrop: true,
					cropBoxMovable: true,
					cropBoxResizable: true,
					minCropBoxWidth: 320,
					minCropBoxHeight: 320
				}).on('built.cropper', function () {

					var croppedData = $(iElement).cropper('getData');
					
					scope.playerdata.head.xaxis = parseInt(croppedData.x);
					scope.playerdata.head.yaxis = parseInt(croppedData.y);
					scope.playerdata.head.width = parseInt(croppedData.width);
					scope.playerdata.head.height = parseInt(croppedData.height);

				}).on('cropend.cropper', function (e) {

					var croppedData = $(iElement).cropper('getData');
					
					scope.playerdata.head.xaxis = parseInt(croppedData.x);
					scope.playerdata.head.yaxis = parseInt(croppedData.y);
					scope.playerdata.head.width = parseInt(croppedData.width);
					scope.playerdata.head.height = parseInt(croppedData.height);

				}).on('zoom.cropper', function (e) {

					var croppedData = $(iElement).cropper('getData');
					
					scope.playerdata.head.xaxis = parseInt(croppedData.x);
					scope.playerdata.head.yaxis = parseInt(croppedData.y);
					scope.playerdata.head.width = parseInt(croppedData.width);
					scope.playerdata.head.height = parseInt(croppedData.height);

				});

			});

		}
	};
}]);

stf.directive('bodypictureCropper', [function () {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			scope.$on('bodyImgReady', function () {

				$(iElement).cropper({
					aspectRatio: 2 / 3,
					dragCrop: true,
					cropBoxMovable: true,
					cropBoxResizable: true,
					minCropBoxWidth: 220,
					minCropBoxHeight: 320
				}).on('built.cropper', function () {

					var croppedData = $(iElement).cropper('getData');
					
					scope.playerdata.body.xaxis = parseInt(croppedData.x);
					scope.playerdata.body.yaxis = parseInt(croppedData.y);
					scope.playerdata.body.width = parseInt(croppedData.width);
					scope.playerdata.body.height = parseInt(croppedData.height);

				}).on('cropend.cropper', function (e) {

					var croppedData = $(iElement).cropper('getData');
					
					scope.playerdata.body.xaxis = parseInt(croppedData.x);
					scope.playerdata.body.yaxis = parseInt(croppedData.y);
					scope.playerdata.body.width = parseInt(croppedData.width);
					scope.playerdata.body.height = parseInt(croppedData.height);

				}).on('zoom.cropper', function (e) {

					var croppedData = $(iElement).cropper('getData');
					
					scope.playerdata.body.xaxis = parseInt(croppedData.x);
					scope.playerdata.body.yaxis = parseInt(croppedData.y);
					scope.playerdata.body.width = parseInt(croppedData.width);
					scope.playerdata.body.height = parseInt(croppedData.height);

				});

			});

		}
	};
}]);