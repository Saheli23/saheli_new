var stf = angular.module('DribblingDirectives', []);

stf.directive('dribblingZigzag', ['$interval', '$timeout', '$document', function ($interval, $timeout, $document) {
	return {
		templateUrl: 'dist/views/templates/dribbling.zigzag.html',
		restrict: 'E',
		scope: {
			activetab: '=',
			playerid: '=',
			eventid: '='
		},
		controller: function ($scope, $interval, $filter, $routeParams, $rootScope, $location, Dribbling, Auth, STFcache) {
			var id_uniq        = $routeParams.id_uniq;
			var player_id_uniq = $routeParams.playerid;
			var slug = $location.$$url.split('/')[$location.$$url.split('/').length-1];

			$scope.getData =  function(){
				Dribbling.getDetails(id_uniq, player_id_uniq).then(function(res) {
				    $scope.loadingEvent = false;
				    if (angular.isObject(res.data.response)) {
				        
				        if (res.data.response.zigzagStat == 1) {
				            $scope.zigzagdata.id_player        = res.data.response.zigzagPlayerID;
				            $scope.zigzagdata.id_event         = res.data.response.zigzagEvtID;
				            $scope.zigzagdata.measurement_date = res.data.response.zigzagMeasurementDt;

				            $scope.zigzagdata.checkpoints[0]   = parseInt(res.data.response.zigzagTargetOne);
				            $scope.zigzagdata.checkpoints[1]   = parseInt(res.data.response.zigzagTargetTwo);
				            $scope.zigzagdata.checkpoints[2]   = parseInt(res.data.response.zigzagTargetThree);
				            $scope.zigzagdata.checkpoints[3]   = parseInt(res.data.response.zigzagTargetFour);
				            $scope.zigzagdata.checkpoints[4]   = parseInt(res.data.response.zigzagTargetFive);
				            $scope.zigzagdata.checkpoints[5]   = parseInt(res.data.response.zigzagTargetSix);
				            $scope.zigzagdata.checkpoints[6]   = parseInt(res.data.response.zigzagTargetSeven);
				            $scope.zigzagdata.checkpoints[7]   = parseInt(res.data.response.zigzagTargetEight);
				            $scope.zigzagdata.checkpoints[8]   = parseInt(res.data.response.zigzagTargetNine);
				            $scope.zigzagdata.checkpoints[9]   = parseInt(res.data.response.zigzagTargetTen);
				        	$scope.totaltime                   = $scope.zigzagdata.checkpoints.reduce(function(a, b) { return a + b; }, 0);
				        } else {
				            $scope.zigzagdata.measurement_date = $filter('date')(new Date(), 'MM/dd/yyyy');
				        }
				    }
				}, function(err) {
				    $scope.loadingEvent = false;
				    if (err.status == 401) {
				        Auth.deleteToken();
				    } else {
				        $scope.loadingEventAlert = {
				            type: 'danger',
				            message: err.status + ' ' + err.statusText
				        };
				    }
				});
			}

			$scope.init = function () {
				$scope.interval = false;
				$scope.stopwatch = new Date(Date.UTC(99, 11, 1, 0, 0, 0));
				$scope.totaltime = 0;
				$scope.attempts = 0;
				$scope.timerCompleted = false;
				$scope.zigzagdata = {
					id_player: $scope.playerid,
					id_event: $scope.eventid,
					measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
					checkpoints: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				};

				$scope.getData();
			};

			$scope.resetTimer = function () {
				$interval.cancel($scope.interval);
				$scope.init();
			};

			$scope.$on('$destroy', function () { $interval.cancel($scope.interval); });
			$rootScope.$on('dribblingTestPlayerChanged', function () {
				$scope.resetTimer();
			});

			$scope.init();

			console.log("Hello!");

			$scope.createDribblingZigzag = function () {
				$scope.creatingDribblingZigzag = true;
				$scope.creatingDribblingZigzagAlert = false;
				var data = $.param($scope.zigzagdata);
				if( slug === 'create' ){
					Dribbling.createZigzag(data).then(function (res) {
						$scope.creatingDribblingZigzag = false;
						if (res.data.status == "success") {
							STFcache.delete('dribblingEvent' + id_uniq);
							$rootScope.tooltipMessageAlert = {
								type: 'success',
								message: res.data.response
							};

							$location.path('/events/' + id_uniq + '/dribbling');
						} else {
							$scope.creatingDribblingZigzagAlert = {
								type: 'danger',
								message: res.data.response
							};
						}
					}, function (err) {
						$scope.creatingDribblingZigzag = false;
						if (err.status == 401) {
							Auth.deleteToken();
						} else {
							$scope.creatingDribblingZigzagAlert = {
								type: 'danger',
								message: err.status + ' ' + err.statusText
							};
						}
					});
				} else{
					Dribbling.updateZigzag(data).then(function (res) {
						$scope.creatingDribblingZigzag = false;
						if (res.data.status == "success") {
							STFcache.delete('dribblingEvent' + id_uniq);
							$rootScope.tooltipMessageAlert = {
								type: 'success',
								message: res.data.response
							};

							$location.path('/events/' + id_uniq + '/dribbling');
						} else {
							$scope.creatingDribblingZigzagAlert = {
								type: 'danger',
								message: res.data.response
							};
						}
					}, function (err) {
						$scope.creatingDribblingZigzag = false;
						if (err.status == 401) {
							Auth.deleteToken();
						} else {
							$scope.creatingDribblingZigzagAlert = {
								type: 'danger',
								message: err.status + ' ' + err.statusText
							};
						}
					});
				}
				
			};
		},
		link: function (scope, iElement, iAttrs) {
			console.log(scope);
			$document.on('keypress', function (e) {
				console.log(e);
				if (e.charCode == 115 && scope.activetab === "Zigzag") {
					if (scope.interval == false) {
						scope.d = new Date();
						scope.interval = $interval(function () {
							scope.now = new Date();
							scope.ellapsed = scope.now.getTime() - scope.d.getTime();
							scope.stopwatch = scope.ellapsed;
						}, 33);	
					} else {
						if (scope.attempts < 10) {
							if (scope.attempts == 0) {
								scope.zigzagdata.checkpoints[scope.attempts] = scope.stopwatch;
							} else {
								var ellapsedTime = 0;
								for( var i=0; i<scope.attempts;i++){
									ellapsedTime += scope.zigzagdata.checkpoints[i];
								}
								scope.zigzagdata.checkpoints[scope.attempts] = scope.stopwatch - ellapsedTime;
							}
							scope.attempts++;
						};

						if (scope.attempts == 10) {
							scope.totaltime = 0;
							$interval.cancel(scope.interval);
							scope.totaltime += scope.stopwatch;
							scope.timerCompleted = true;
						};
					}
				};
			});
		}
	};
}]);

stf.directive('dribblingCircular', ['$interval', '$timeout', '$document', function ($interval, $timeout, $document) {
	return {
		templateUrl: 'dist/views/templates/dribbling.circular.html',
		restrict: 'E',
		scope: {
			activetab: '=',
			playerid: '=',
			eventid: '='
		},
		controller: function ($scope, $interval, $filter, $routeParams, $rootScope, $location, Dribbling, Auth, STFcache) {
			var id_uniq        = $routeParams.id_uniq;
			var player_id_uniq = $routeParams.playerid;
			var slug = $location.$$url.split('/')[$location.$$url.split('/').length-1];

			$scope.getData =  function(){
				Dribbling.getDetails(id_uniq, player_id_uniq).then(function(res) {
				    $scope.loadingEvent = false;
				    if (angular.isObject(res.data.response)) {

				    	console.log(res.data.response);
				        
				        if (res.data.response.circularStat == 1) {
				            $scope.circulardata.id_player        = res.data.response.circularPlayerID;
				            $scope.circulardata.id_event         = res.data.response.circularEvtID;
				            $scope.circulardata.measurement_date = res.data.response.circularMeasurementDt;

				            $scope.circulardata.checkpoints[0]   = parseInt(res.data.response.circularTargetOne);
				            $scope.circulardata.checkpoints[1]   = parseInt(res.data.response.circularTargetTwo);
				            $scope.circulardata.checkpoints[2]   = parseInt(res.data.response.circularTargetThree);
				            $scope.circulardata.checkpoints[3]   = parseInt(res.data.response.circularTargetFour);
				            $scope.circulardata.checkpoints[4]   = parseInt(res.data.response.circularTargetFive);
				            $scope.circulardata.checkpoints[5]   = parseInt(res.data.response.circularTargetSix);
				            $scope.circulardata.checkpoints[6]   = parseInt(res.data.response.circularTargetSeven);
				            $scope.circulardata.checkpoints[7]   = parseInt(res.data.response.circularTargetEight);
				            $scope.circulardata.checkpoints[8]   = parseInt(res.data.response.circularTargetNine);
				            $scope.circulardata.checkpoints[9]   = parseInt(res.data.response.circularTargetTen);
				        	$scope.totaltime                     = $scope.circulardata.checkpoints.reduce(function(a, b) { return a + b; }, 0);
				        }
				    }
				}, function(err) {
				    $scope.loadingEvent = false;
				    if (err.status == 401) {
				        Auth.deleteToken();
				    } else {
				        $scope.loadingEventAlert = {
				            type: 'danger',
				            message: err.status + ' ' + err.statusText
				        };
				    }
				});
			}
			$scope.init = function () {
				$scope.interval = false;
				$scope.stopwatch = new Date(Date.UTC(99, 11, 1, 0, 0, 0));
				$scope.totaltime = 0;
				$scope.attempts = 0;
				$scope.timerCompleted = false;
				$scope.circulardata = {
					id_player: $scope.playerid,
					id_event: $scope.eventid,
					measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
					checkpoints: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				};

				$scope.getData();
			};

			$scope.resetTimer = function () {
				$interval.cancel($scope.interval);
				$scope.init();
			};

			$scope.$on('$destroy', function () { $interval.cancel($scope.interval); });

			$rootScope.$on('dribblingTestPlayerChanged', function () {
				$scope.resetTimer();
			});

			$scope.init();

			$scope.createDribblingCircular = function () {
				$scope.creatingDribblingCircular = true;
				$scope.creatingDribblingCircularAlert = false;

				var data = $.param($scope.circulardata);
				if( slug === 'create' ){
					Dribbling.createCircular(data).then(function (res) {
						$scope.creatingDribblingCircular = false;
						if (res.data.status == "success") {
							STFcache.delete('dribblingEvent' + id_uniq);
							$rootScope.tooltipMessageAlert = {
								type: 'success',
								message: res.data.response
							};
							$location.path('/events/' + id_uniq + '/dribbling');
						} else {
							$scope.creatingDribblingCircularAlert = {
								type: 'danger',
								message: res.data.response
							};
						}
					}, function (err) {
						$scope.creatingDribblingCircular = false;
						if (err.status == 401) {
							Auth.deleteToken();
						} else {
							$scope.creatingDribblingCircularAlert = {
								type: 'danger',
								message: err.status + ' ' + err.statusText
							};
						}
					});
				} else {
					Dribbling.updateCircular(data).then(function (res) {
						$scope.creatingDribblingCircular = false;
						if (res.data.status == "success") {
							STFcache.delete('dribblingEvent' + id_uniq);
							$rootScope.tooltipMessageAlert = {
								type: 'success',
								message: res.data.response
							};
							$location.path('/events/' + id_uniq + '/dribbling');
						} else {
							$scope.creatingDribblingCircularAlert = {
								type: 'danger',
								message: res.data.response
							};
						}
					}, function (err) {
						$scope.creatingDribblingCircular = false;
						if (err.status == 401) {
							Auth.deleteToken();
						} else {
							$scope.creatingDribblingCircularAlert = {
								type: 'danger',
								message: err.status + ' ' + err.statusText
							};
						}
					});

				}
				
			};
		},
		link: function (scope, iElement, iAttrs) {
			$document.on('keypress', function (e) {
				if (e.charCode == 115 && scope.activetab === "Circular") {
					if (scope.interval == false) {
						scope.d = new Date();
						scope.interval = $interval(function () {
							scope.now = new Date();
							scope.ellapsed = scope.now.getTime() - scope.d.getTime();
							scope.stopwatch = scope.ellapsed;
						}, 33);	
					} else {
						if (scope.attempts < 10) {
							if (scope.attempts == 0) {
								scope.circulardata.checkpoints[scope.attempts] = scope.stopwatch;
							} else {
								var ellapsedTime = 0;
								for( var i=0; i<scope.attempts;i++){
									ellapsedTime += scope.circulardata.checkpoints[i];
								}
								scope.circulardata.checkpoints[scope.attempts] = scope.stopwatch - ellapsedTime;
							}
							scope.attempts++;
						};

						if (scope.attempts == 10) {
							scope.totaltime = 0;
							$interval.cancel(scope.interval);
							scope.totaltime += scope.stopwatch;
							scope.timerCompleted = true;
						};
					}
				};
			});
		}
	};
}]);