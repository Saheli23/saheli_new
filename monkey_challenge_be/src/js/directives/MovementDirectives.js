var stf = angular.module('MovementDirectives', []);

stf.directive('movementLinear', ['$interval', '$timeout', '$document', function ($interval, $timeout, $document) {
	return {
		templateUrl: 'dist/views/templates/movement.linear.html',
		restrict: 'E',
		scope: {
			activetab: '=',
			playerid: '=',
			eventid: '='
		},
		controller: function ($scope, $interval, $filter, $routeParams, $rootScope, $location, Movement, Auth, STFcache) {

			var id_uniq        = $routeParams.id_uniq;
			var player_id_uniq = $routeParams.playerid;
			var slug = $location.$$url.split('/')[$location.$$url.split('/').length-1];

			$scope.getData = function(){
				Movement.getDetails(id_uniq, player_id_uniq).then(function(res) {
				    if (angular.isObject(res.data.response)) {
				        if (res.data.response.lateralStat == 1) {
				            $scope.lineardata.id_player        = res.data.response.linearPlayerID;
				            $scope.lineardata.id_event         = res.data.response.linearEvtID;
				            $scope.lineardata.measurement_date = res.data.response.linearMeasurementDt;

				            $scope.lineardata.checkpoints[0]   = parseInt(res.data.response.linearTargetOne);
				            $scope.lineardata.checkpoints[1]   = parseInt(res.data.response.linearTargetTwo);
				            $scope.lineardata.checkpoints[2]   = parseInt(res.data.response.linearTargetThree);
				            $scope.lineardata.checkpoints[3]   = parseInt(res.data.response.linearTargetFour);
				            $scope.lineardata.checkpoints[4]   = parseInt(res.data.response.linearTargetFive);
				            $scope.lineardata.checkpoints[5]   = parseInt(res.data.response.linearTargetSix);
				            $scope.lineardata.checkpoints[6]   = parseInt(res.data.response.linearTargetSeven);
				            $scope.lineardata.checkpoints[7]   = parseInt(res.data.response.linearTargetEight);
				            $scope.lineardata.checkpoints[8]   = parseInt(res.data.response.linearTargetNine);
				            $scope.lineardata.checkpoints[9]   = parseInt(res.data.response.linearTargetTen);
				            $scope.totaltime                   = $scope.lineardata.checkpoints.reduce(function(a, b) { return a + b; }, 0);
				        } else {
				            $scope.lineardata.measurement_date = $filter('date')(new Date(), 'MM/dd/yyyy');
				        }
				    }
				}, function(err) {
				    $scope.loadingEvent = false;
				    if (err.status == 401) {
				        Auth.deleteToken();
				    } else {
				        $scope.loadingEventAlert = {
				            type: 'danger',
				            message: err.status + ' ' + err.statusText
				        };
				    }
				});
			}
			
			$scope.init = function () {
                console.log("Init Started!!");
				console.log($scope.playerid);
				$scope.interval = false;
				$scope.stopwatch = new Date(Date.UTC(99, 11, 1, 0, 0, 0));
				$scope.totaltime = 0;
				$scope.attempts = 0;

				$scope.timerCompleted = false;
				$scope.lineardata = {
					id_player: $scope.playerid,
					id_event: $scope.eventid,
					measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
					checkpoints: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				};

				$scope.getData();

			};

			$scope.resetTimer = function () {
              
				$interval.cancel($scope.interval);
				$scope.init();

			};

			$scope.$on('$destroy', function () { $interval.cancel($scope.interval); });

			$rootScope.$on('movementTestPlayerChanged', function () {

				$scope.resetTimer();

			});

			$scope.init();

			$scope.createMovementLinear = function () {

				$scope.creatingMovementLinear = true;
				$scope.creatingMovementLinearAlert = false;

				var data = $.param($scope.lineardata);

				if( slug === 'create' ){
					Movement.createLinear(data).then(function (res) {
						$scope.creatingMovementLinear = false;
						if (res.data.status == "success") {
							STFcache.delete('movementEvent' + id_uniq);
							$rootScope.tooltipMessageAlert = {
								type: 'success',
								message: res.data.response
							};
							$location.path('/events/' + id_uniq + '/movement');
						} else {
							$scope.creatingMovementLinearAlert = {
								type: 'danger',
								message: res.data.response
							};
						}
					}, function (err) {
						$scope.creatingMovementLinear = false;
						if (err.status == 401) Auth.deleteToken();
						else {
							$scope.creatingMovementLinearAlert = {
								type: 'danger',
								message: err.status + ' ' + err.statusText
							};
						}
					});
				} else {
					Movement.updateLinear(data).then(function (res) {
						if (res.data.status == "success") {
							STFcache.delete('movementEvent' + id_uniq);
							$rootScope.tooltipMessageAlert = {
								type: 'success',
								message: res.data.response
							};
							$location.path('/events/' + id_uniq + '/movement');
						} else {
							$scope.creatingMovementLinearAlert = {
								type: 'danger',
								message: res.data.response
							};
						}
					}, function (err) {
						if (err.status == 401) Auth.deleteToken();
						else {
							$scope.creatingMovementLinearAlert = {
								type: 'danger',
								message: err.status + ' ' + err.statusText
							};
						}
					});
				}
			};

		},
		link: function (scope, iElement, iAttrs) {

			$document.on('keypress', function (e) {

				if (e.charCode == 115 && scope.activetab === "Lineal") {
					if ( !scope.interval ) {
						scope.d = new Date();
						scope.interval = $interval(function () {
							scope.now = new Date();
							scope.ellapsed = scope.now.getTime() - scope.d.getTime();
							scope.stopwatch = scope.ellapsed;
						}, 33);	
					} else {
						if (scope.attempts < 10) {
							if (scope.attempts == 0) {
								scope.lineardata.checkpoints[scope.attempts] = scope.stopwatch;
							} else {
								console.log(scope.lineardata.checkpoints);
								var ellapsedTime = 0;
								for( var i=0; i<scope.attempts;i++){
									ellapsedTime += scope.lineardata.checkpoints[i];
								}
								scope.lineardata.checkpoints[scope.attempts] = scope.stopwatch - ellapsedTime;
							}
							scope.attempts++;
						};

						if (scope.attempts == 10) {
							scope.totaltime = 0;
							$interval.cancel(scope.interval);
							scope.totaltime += scope.stopwatch;
							scope.timerCompleted = true;
						};

					}

				};

			});

		}
	};
}]);

stf.directive('movementLateral', ['$interval', '$timeout', '$document', function ($interval, $timeout, $document) {
	return {
		templateUrl: 'dist/views/templates/movement.lateral.html',
		restrict: 'E',
		scope: {
			activetab: '=',
			playerid: '=',
			eventid: '='
		},
		controller: function ($scope, $interval, $filter, $routeParams, $rootScope, $location, Movement, Auth, STFcache) {

			var id_uniq        = $routeParams.id_uniq;
			var player_id_uniq = $routeParams.playerid;
			var slug = $location.$$url.split('/')[$location.$$url.split('/').length-1];

			$scope.getData = function(){
				Movement.getDetails(id_uniq, player_id_uniq).then(function(res) {
				    if (angular.isObject(res.data.response)) {
				        if (res.data.response.lateralStat == 1) {
				            $scope.lateraldata.id_player        = res.data.response.lateralPlayerID;
				            $scope.lateraldata.id_event         = res.data.response.lateralEvtID;
				            $scope.lateraldata.measurement_date = res.data.response.lateralMeasurementDt;

				            $scope.lateraldata.checkpoints[0]   = parseInt(res.data.response.lateralTargetOne);
				            $scope.lateraldata.checkpoints[1]   = parseInt(res.data.response.lateralTargetTwo);
				            $scope.lateraldata.checkpoints[2]   = parseInt(res.data.response.lateralTargetThree);
				            $scope.lateraldata.checkpoints[3]   = parseInt(res.data.response.lateralTargetFour);
				            $scope.lateraldata.checkpoints[4]   = parseInt(res.data.response.lateralTargetFive);
				            $scope.lateraldata.checkpoints[5]   = parseInt(res.data.response.lateralTargetSix);
				            $scope.lateraldata.checkpoints[6]   = parseInt(res.data.response.lateralTargetSeven);
				            $scope.lateraldata.checkpoints[7]   = parseInt(res.data.response.lateralTargetEight);
				            $scope.lateraldata.checkpoints[8]   = parseInt(res.data.response.lateralTargetNine);
				            $scope.lateraldata.checkpoints[9]   = parseInt(res.data.response.lateralTargetTen);
				       
				       		$scope.totaltime = $scope.lateraldata.checkpoints.reduce(function(a, b) { return a + b; }, 0);
				        } else {
				            $scope.lateraldata.measurement_date = $filter('date')(new Date(), 'MM/dd/yyyy');
				        }
				    }
				}, function(err) {
				    $scope.loadingEvent = false;
				    if (err.status == 401) {
				        Auth.deleteToken();
				    } else {
				        $scope.loadingEventAlert = {
				            type: 'danger',
				            message: err.status + ' ' + err.statusText
				        };
				    }
				});
			}

			$scope.init = function () {
              
				$scope.interval = false;
				$scope.stopwatch = new Date(Date.UTC(99, 11, 1, 0, 0, 0));
				$scope.totaltime = 0;
				$scope.attempts = 0;

				$scope.timerCompleted = false;
				$scope.lateraldata = {
					id_player: $scope.playerid,
					id_event: $scope.eventid,
					measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
					checkpoints: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				};

				$scope.getData();

			};

			$scope.resetTimer = function () {

				$interval.cancel($scope.interval);
				$scope.init();

			};

			$scope.$on('$destroy', function () { $interval.cancel($scope.interval); });

			$rootScope.$on('movementTestPlayerChanged', function () {

				$scope.resetTimer();

			})

			$scope.init();

			$scope.createMovementLateral = function () {

				$scope.creatingMovementLateral = true;
				$scope.creatingMovementLateralAlert = false;

				var data = $.param($scope.lateraldata);


				if( slug === 'create' ){
					Movement.createLateral(data).then(function (res) {
						$scope.creatingMovementLateral = false;
						if (res.data.status == "success") {
							STFcache.delete('movementEvent' + id_uniq);
							$rootScope.tooltipMessageAlert = {
								type: 'success',
								message: res.data.response
							};
							$location.path('/events/' + id_uniq + '/movement');
						} else {
							$scope.creatingMovementLateralAlert = {
								type: 'danger',
								message: res.data.response
							};
						}
					}, function (err) {
						$scope.creatingMovementLateral = false;
						if (err.status == 401) Auth.deleteToken();
						else {
							$scope.creatingMovementLateralAlert = {
								type: 'danger',
								message: err.status + ' ' + err.statusText
							};
						}
					});
				} else {
					Movement.updateLateral(data).then(function (res) {
						if (res.data.status == "success") {
							STFcache.delete('movementEvent' + id_uniq);
							$rootScope.tooltipMessageAlert = {
								type: 'success',
								message: res.data.response
							};
							$location.path('/events/' + id_uniq + '/movement');
						} else {
							$scope.creatingMovementLateralAlert = {
								type: 'danger',
								message: res.data.response
							};
						}
					}, function (err) {
						if (err.status == 401) Auth.deleteToken();
						else {
							$scope.creatingMovementLateralAlert = {
								type: 'danger',
								message: err.status + ' ' + err.statusText
							};
						}
					});
				}
			};

		},
		link: function (scope, iElement, iAttrs) {

			$document.on('keypress', function (e) {

				if (e.charCode == 115 && scope.activetab === "Lateral") {

					if (scope.interval == false) {

						scope.d = new Date();

						scope.interval = $interval(function () {

							scope.now = new Date();
							scope.ellapsed = scope.now.getTime() - scope.d.getTime();
							scope.stopwatch = scope.ellapsed;

						}, 33);	

					} else {

						if (scope.attempts < 10) {
							if (scope.attempts == 0) {
								scope.lateraldata.checkpoints[scope.attempts] = scope.stopwatch;
							} else {
								var ellapsedTime = 0;
								for( var i=0; i<scope.attempts;i++){
									ellapsedTime += scope.lateraldata.checkpoints[i];
								}
								scope.lateraldata.checkpoints[scope.attempts] = scope.stopwatch - ellapsedTime;
							}
							scope.attempts++;
						};

						if (scope.attempts == 10) {
							scope.totaltime = 0;
							$interval.cancel(scope.interval);
							scope.totaltime += scope.stopwatch;
							scope.timerCompleted = true;

						};

					}

				};

			});

		}
	};
}]);

stf.directive('movementZigzag', ['$interval', '$timeout', '$document', function ($interval, $timeout, $document) {
	return {
		templateUrl: 'dist/views/templates/movement.zigzag.html',
		restrict: 'E',
		scope: {
			activetab: '=',
			playerid: '=',
			eventid: '='
		},
		controller: function ($scope, $interval, $filter, $routeParams, $rootScope, $location, Movement, Auth, STFcache) {

			var id_uniq        = $routeParams.id_uniq;
			var player_id_uniq = $routeParams.playerid;
			var slug = $location.$$url.split('/')[$location.$$url.split('/').length-1];

			$scope.getData =  function(){
				Movement.getDetails(id_uniq, player_id_uniq).then(function(res) {
				    $scope.loadingEvent = false;
				    if (angular.isObject(res.data.response)) {
				        
				        if (res.data.response.zigzagStat == 1) {
				            $scope.zigzagdata.id_player        = res.data.response.zigzagPlayerID;
				            $scope.zigzagdata.id_event         = res.data.response.zigzagEvtID;
				            $scope.zigzagdata.measurement_date = res.data.response.zigzagMeasurementDt;

				            $scope.zigzagdata.checkpoints[0]   = parseInt(res.data.response.zigzagTargetOne);
				            $scope.zigzagdata.checkpoints[1]   = parseInt(res.data.response.zigzagTargetTwo);
				            $scope.zigzagdata.checkpoints[2]   = parseInt(res.data.response.zigzagTargetThree);
				            $scope.zigzagdata.checkpoints[3]   = parseInt(res.data.response.zigzagTargetFour);
				            $scope.zigzagdata.checkpoints[4]   = parseInt(res.data.response.zigzagTargetFive);
				            $scope.zigzagdata.checkpoints[5]   = parseInt(res.data.response.zigzagTargetSix);
				            $scope.zigzagdata.checkpoints[6]   = parseInt(res.data.response.zigzagTargetSeven);
				            $scope.zigzagdata.checkpoints[7]   = parseInt(res.data.response.zigzagTargetEight);
				            $scope.zigzagdata.checkpoints[8]   = parseInt(res.data.response.zigzagTargetNine);
				            $scope.zigzagdata.checkpoints[9]   = parseInt(res.data.response.zigzagTargetTen);
				        	$scope.totaltime                   = $scope.zigzagdata.checkpoints.reduce(function(a, b) { return a + b; }, 0);
				        } else {
				            $scope.zigzagdata.measurement_date = $filter('date')(new Date(), 'MM/dd/yyyy');
				        }
				    }
				}, function(err) {
				    $scope.loadingEvent = false;
				    if (err.status == 401) {
				        Auth.deleteToken();
				    } else {
				        $scope.loadingEventAlert = {
				            type: 'danger',
				            message: err.status + ' ' + err.statusText
				        };
				    }
				});
			}

			$scope.init = function () {

				$scope.interval = false;
				$scope.stopwatch = new Date(Date.UTC(99, 11, 1, 0, 0, 0));
				$scope.totaltime = 0;
				$scope.attempts = 0;

				$scope.timerCompleted = false;
				$scope.zigzagdata = {
					id_player: $scope.playerid,
					id_event: $scope.eventid,
					measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
					checkpoints: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				};

				$scope.getData();
			};

			$scope.resetTimer = function () {

				$interval.cancel($scope.interval);
				$scope.init();

			};

			$scope.$on('$destroy', function () { $interval.cancel($scope.interval); });

			$rootScope.$on('movementTestPlayerChanged', function () {

				$scope.resetTimer();

			})

			$scope.init();

			$scope.createMovementZigzag = function () {

				$scope.creatingMovementZigzag = true;
				$scope.creatingMovementZigzagAlert = false;

				var data = $.param($scope.zigzagdata);

				if( slug === "create" ){
					Movement.createZigzag(data).then(function (res) {
						$scope.creatingMovementZigzag = false;
						if (res.data.status == "success") {
							STFcache.delete('movementEvent' + id_uniq);
							$rootScope.tooltipMessageAlert = {
								type: 'success',
								message: res.data.response
							};
							$location.path('/events/' + id_uniq + '/movement');
						} else {
							$scope.creatingMovementZigzagAlert = {
								type: 'danger',
								message: res.data.response
							};
						}
					}, function (err) {
						$scope.creatingMovementZigzag = false;
						if (err.status == 401) Auth.deleteToken();
						else {
							$scope.creatingMovementZigzagAlert = {
								type: 'danger',
								message: err.status + ' ' + err.statusText
							};
						}
					});
				} else {
					Movement.updateZigzag(data).then(function (res) {
						if (res.data.status == "success") {
							STFcache.delete('movementEvent' + id_uniq);
							$rootScope.tooltipMessageAlert = {
								type: 'success',
								message: res.data.response
							};
							$location.path('/events/' + id_uniq + '/movement');
						} else {
							$scope.creatingMovementZigzagAlert = {
								type: 'danger',
								message: res.data.response
							};
						}
					}, function (err) {
						if (err.status == 401) Auth.deleteToken();
						else {
							$scope.creatingMovementZigzagAlert = {
								type: 'danger',
								message: err.status + ' ' + err.statusText
							};
						}
					});
				}
			};
		},
		link: function (scope, iElement, iAttrs) {

			$document.on('keypress', function (e) {

				if (e.charCode == 115 && scope.activetab === "Zigzag") {

					if (scope.interval == false) {

						scope.d = new Date();

						scope.interval = $interval(function () {

							scope.now = new Date();
							scope.ellapsed = scope.now.getTime() - scope.d.getTime();
							scope.stopwatch = scope.ellapsed;

						}, 33);	

					} else {

						if (scope.attempts < 10) {

							if (scope.attempts == 0) {
								scope.zigzagdata.checkpoints[scope.attempts] = scope.stopwatch;
							} else {
								var ellapsedTime = 0;
								for( var i=0; i<scope.attempts;i++){
									ellapsedTime += scope.zigzagdata.checkpoints[i];
								}
								scope.zigzagdata.checkpoints[scope.attempts] = scope.stopwatch - ellapsedTime;
							}
							scope.attempts++;

						};

						if (scope.attempts == 10) {
							scope.totaltime = 0;
							$interval.cancel(scope.interval);
							scope.totaltime += scope.stopwatch;
							scope.timerCompleted = true;

						};

					}

				};

			});

		}
	};
}]);