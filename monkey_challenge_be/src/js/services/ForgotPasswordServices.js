var stf = angular.module('ForgotPasswordServices', []);

stf.factory('Forgot', ['$http', '$window', '$location','CommonServices', function ($http, $window, $location,CommonServices) {
	

	return {
		passwordforgot: function (data) {

			return $http.post(CommonServices.url()+'api/forgot/passwordforgot', data);

		}
	};
}]);