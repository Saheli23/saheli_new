var stf = angular.module('KickingServices', []);

stf.factory('Kicking', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	

	return {
		check: function (id_player, id_event, data) {

			return $http.post(CommonServices.url()+'api/kicking/check', data, {
				params: {
					id_player: id_player,
					id_event: id_event
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		createAim: function (data) {

			return $http.post(CommonServices.url()+'api/kicking/aim', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		updateAim: function (data) {

			return $http.put(CommonServices.url()+'api/kicking/aim', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		createForce: function (data) {

			return $http.post(CommonServices.url()+'api/kicking/force', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		updateForce: function (data) {

			return $http.put(CommonServices.url()+'api/kicking/force', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getAll: function (id_uniq_event) {

			return $http.get(CommonServices.url()+'api/kicking/event', {
				params: {
					id_uniq_event: id_uniq_event
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('kickingEvent' + id_uniq_event)
			});

		},

		delete: function (data) {
			return $http.put(CommonServices.url()+'api/kicking/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put(CommonServices.url()+'api/kicking/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		getDetails: function (id_uniq_event, id_uniq_player ) {

			return $http.get(CommonServices.url()+'api/kicking/aim', {
				params: {
					id_uniq_event: id_uniq_event,
					id_uniq_player: id_uniq_player
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('kickingEvent' + id_uniq_event)
			});

		}

	};
}])