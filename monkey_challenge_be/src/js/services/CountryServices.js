var stf = angular.module('CountryServices', []);

stf.factory('Country', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	

	return {
		getCountries: function () {

			return $http.get(CommonServices.url()+'api/country', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('countries')
			});

		},
		getCountryStates: function (id_country) {

			return $http.get(CommonServices.url()+'api/country/states', {
				params: {
					id_country: id_country
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('states')
			});

		}
	};
}]);