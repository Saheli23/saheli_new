var stf = angular.module('SectionServices', []);
stf.factory('Section', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	return {
		getSection: function () {
			return $http.get(CommonServices.url()+'api/section/section', {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}])