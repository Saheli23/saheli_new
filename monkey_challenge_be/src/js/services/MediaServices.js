var stf = angular.module('MediaServices', []);

stf.factory('Media', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	

	return {
		getMediaType: function () {

			return $http.get(CommonServices.target()+'api/types/media', {
				headers: {
					'authorization': $window.localStorage.authorization
				},
				cache: STFcache.save('mediatype')
			});

		}
		
	};
}]);