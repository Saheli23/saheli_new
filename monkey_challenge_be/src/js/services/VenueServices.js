var stf = angular.module('VenueServices', []);

stf.factory('Venue', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	

	return {
		createVenue: function (data) {

			return $http.post(CommonServices.url()+'api/venue', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getVenues: function () {

			return $http.get(CommonServices.url()+'api/venue', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('venues')
			});

		},
		getVenue: function (id_uniq) {

			return $http.get(CommonServices.url()+'api/venue', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('venues' + id_uniq)
			});

		},
		editVenue: function (id_uniq, data) {

			return $http.put(CommonServices.url()+'api/venue', data, {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getUniqIDs: function () {

			return $http.get(CommonServices.url()+'api/venue/uniqids', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('venueUniqIDs')
			});

		},

		delete: function (data) {
			return $http.put(CommonServices.url()+'api/venue/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		multidelete: function (data) {
			return $http.put(CommonServices.url()+'api/venue/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
	};
}]);