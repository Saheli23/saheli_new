var stf = angular.module('EventServices', []);
stf.factory('Event', ['$http', '$window', '$rootScope', 'STFcache','CommonServices', function ($http, $window, $rootScope,STFcache,CommonServices) {
	return {
		create: function (data) {
			return $http.post(CommonServices.url()+'api/event', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		getAll: function () {
			return $http.get(CommonServices.url()+'api/event', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('events')
			});
		},
		getOne: function (id_uniq) {
			return $http.get(CommonServices.url()+'api/event', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('events' + id_uniq)
			});
		},
		edit: function (id_uniq, data) {

			return $http.put(CommonServices.url()+'api/event', data, {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getUniqIDs: function () {

			return $http.get(CommonServices.url()+'api/event/uniqids', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('eventUniqIDs')
			});

		},
		delete: function (data) {
			return $http.put(CommonServices.url()+'api/event/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put(CommonServices.url()+'api/event/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}])