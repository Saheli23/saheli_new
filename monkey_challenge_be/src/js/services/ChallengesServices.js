var stf = angular.module('ChallengesServices', []);

stf.factory('Challenge', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	

	return {
		create: function (data,uuid) {
             console.log(data);
			return $http.post(CommonServices.target()+'internal/users/'+uuid+'/challenges',data, {
				transformRequest: angular.identity,
				headers: {
					'Content-Type': "application/x-www-form-urlencoded",
					'authorization': $window.localStorage.authorization
				}
			});

		},
		uploadMedia: function (data,uuid,challenge_id) {
          
			return $http.post(CommonServices.target()+'internal/api/users/'+uuid+'/challenges/'+challenge_id+'/media', data, {
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'authorization': $window.localStorage.authorization
				}
			});

		},

		getOne: function (id) {

			return $http.get(CommonServices.target()+'api/challenges/'+id, {
				
				headers: {
					'authorization': $window.localStorage.authorization
				},
				//cache: STFcache.save('challenges' + id)
			});

		},
		getAll: function () {
            //http://oneup.us-west-2.elasticbeanstalk.com:80/api/challenges
			return $http.get(CommonServices.target()+'api/challenges?filter_by=new', {
				headers: {
					'authorization': $window.localStorage.authorization
				},
				cache: STFcache.save('challenges')
			});

		},
		edit: function (id_uniq, data,uuid) {
            console.log(data);
			return $http.put(CommonServices.target()+'internal/users/'+uuid+'/challenges/'+id_uniq, data, {
				
				transformRequest: angular.identity,
				headers: {
					'Content-Type': "application/json",
					'authorization': $window.localStorage.authorization
				}
			});

		},
		getChallengeMedia: function (id,uuid) {
           
			return $http.get(CommonServices.target()+'internal/users/'+uuid+'/challenges/'+id+'/media', {
				headers: {
					'authorization': $window.localStorage.authorization
				},
				cache: STFcache.save('challengemedia')
			});

		},
		deleteChallengeMedia: function (id,challenge_id,uuid) {
			return $http.delete(CommonServices.target()+'api/users/'+uuid+'/challenges/'+challenge_id+'/media/'+id,{
				headers: {
					'authorization': $window.localStorage.authorization
				}
			});
		},
		changeChallengeStatus: function (uuid,id_uniq, status) {
			var data="uuid="+uuid+"&id="+id_uniq+"&status="+status;
            var val=null;
			return $http.post(CommonServices.target()+'internal/users/'+uuid+'/challenges/'+id_uniq+'/status?'+data,val, {

				transformRequest: angular.identity,
				headers: {
					'Content-Type': "application/json",
					'authorization': $window.localStorage.authorization
				}
			});

		},
		getCurrentTeam: function (id_uniq) {

			return $http.get(CommonServices.url()+'api/player/current_team', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('players' + id_uniq + 'current_team')
			});

		},
		createTeam: function (id_uniq, data) {

			return $http.post(CommonServices.url()+'api/player/team', data, {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getTeams: function (id_uniq) {

			return $http.get(CommonServices.url()+'api/player/teams', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('players' + id_uniq + 'teams')
			});

		},
		getTeam: function (id_uniq, id_team_player) {

			return $http.get(CommonServices.url()+'api/player/team', {
				params:{
					id_uniq: id_uniq,
					id_team_player: id_team_player
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('players' + id_uniq + 'teams' + id_team_player)
			});

		},
		editTeam: function (id_uniq, id_team_player, data) {

			return $http.put(CommonServices.url()+'api/player/team', data, {
				params:{
					id_uniq: id_uniq,
					id_team_player: id_team_player
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getUniqIDs: function () {

			return $http.get(CommonServices.url()+'api/player/uniqids', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('playerUniqIDs')
			});

		},
		getBaseImages: function (id_uniq) {

			return $http.get(CommonServices.url()+'api/player/base_images', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getBaseHeadImage: function (id_uniq) {

			return $http.get(CommonServices.url()+'api/player/base_head_image', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getBaseBodyImage: function (id_uniq) {

			return $http.get(CommonServices.url()+'api/player/base_body_image', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},

		delete: function (id,uuid) {
			return $http.delete(CommonServices.target()+'internal/users/'+uuid+'/challenges/'+id,{
				headers: {
					'Content-Type': "application/x-www-form-urlencoded",
					'authorization': $window.localStorage.authorization
				}
			});
		},
		multidelete: function (data) {
			return $http.put(CommonServices.target()+'api/player/multidelete', data, {
				headers: {
					'Content-Type': "application/x-www-form-urlencoded",
					'authorization': $window.localStorage.authorization
				}
			});
		},

		deleteTeam : function (data) {
			return $http.put(CommonServices.url()+'api/player/deleteteam', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multideleteTeam : function (data) {
			return $http.put(CommonServices.url()+'api/player/multideleteteam', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}]);