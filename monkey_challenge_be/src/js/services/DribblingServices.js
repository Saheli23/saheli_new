var stf = angular.module('DribblingServices', []);

stf.factory('Dribbling', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	return {
		check: function (id_player, id_event, data) {
			return $http.post(CommonServices.url()+'api/dribbling/check', data, {
				params: {
					id_player: id_player,
					id_event: id_event
				},
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		createZigzag: function (data) {
			return $http.post(CommonServices.url()+'api/dribbling/zigzag', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		createCircular: function (data) {
			return $http.post(CommonServices.url()+'api/dribbling/circular', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		updateZigzag: function (data) {
			return $http.put(CommonServices.url()+'api/dribbling/zigzag', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		updateCircular: function (data) {
			return $http.put(CommonServices.url()+'api/dribbling/circular', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		
		getAll: function (id_uniq_event) {
			return $http.get(CommonServices.url()+'api/dribbling/event', {
				params: {
					id_uniq_event: id_uniq_event
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('dribblingEvent' + id_uniq_event)
			});
		},
		delete: function (data) {
			return $http.put(CommonServices.url()+'api/dribbling/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put('api/dribbling/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		getDetails: function (id_uniq_event, id_uniq_player ) {
			return $http.get('api/dribbling/circular', {
				params: {
					id_uniq_event: id_uniq_event,
					id_uniq_player: id_uniq_player
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('dribblingEvent' + id_uniq_event)
			});
		}
	};
}])