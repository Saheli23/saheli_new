var stf = angular.module('GenderServices', []);

stf.factory('Gender', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	

	return {
		getGenders: function () {

			return $http.get(CommonServices.url()+'api/gender', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('genders')
			});

		}
	};
}]);