var stf = angular.module('MeasurementServices', []);

stf.factory('Measurement', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	

	return {
		getMeasurements: function () {

			return $http.get(CommonServices.url()+'api/measurement', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.delete('measurements')
			});

		}
	};
}])