var stf = angular.module('ContentServices', []);

stf.factory('Content', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	return {
		create: function (data) {
			console.log(JSON.stringify(data ))
			return $http.post(CommonServices.url()+'api/content', data, {
				// transformRequest: angular.identity,
				headers: {
					// 'Content-Type': 'application/json',
					'token': $window.localStorage.token
				}
			});
		},
		getOne: function (id) {

			return $http.get(CommonServices.url()+'api/content?id_uniq='+id
				, 
			{
				
				headers: {
					token: $window.localStorage.token
				}
				// cache: STFcache.save('content' + id)
			}
			);

		},
		getAll: function () {
			
			return $http.get(CommonServices.url()+'api/content', {
				// transformRequest: angular.identity,
				headers: {
					'Content-Type': 'application/json',
					'token': $window.localStorage.token
				}
			});
		},
		edit: function (id_uniq, data) {
           
			return $http.put(CommonServices.url()+'api/content', data, {
				params: {
					id_uniq: id_uniq
				},
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'token': $window.localStorage.token
				}
			});

		},
		delete: function (id) {
			
			return $http.delete(CommonServices.url()+'api/content?id_uniq='+id, {
				// transformRequest: angular.identity,
				headers: {
					//'Content-Type': 'application/json',
					'token': $window.localStorage.token
				}
			});
		}
	};
}]);