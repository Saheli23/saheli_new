var stf = angular.module('PlayerServices', []);

stf.factory('Player', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	

	return {
		create: function (data) {

			return $http.post(CommonServices.url()+'api/player', data, {
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'token': $window.localStorage.token
				}
			});

		},
		getOne: function (id_uniq) {

			return $http.get(CommonServices.url()+'api/player', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('players' + id_uniq)
			});

		},
		getAll: function () {

			return $http.get(CommonServices.url()+'api/player', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('players')
			});

		},
		edit: function (id_uniq, data) {

			return $http.post(CommonServices.url()+'api/player', data, {
				params: {
					id_uniq: id_uniq
				},
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'token': $window.localStorage.token
				}
			});

		},
		getCurrentTeam: function (id_uniq) {

			return $http.get(CommonServices.url()+'api/player/current_team', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('players' + id_uniq + 'current_team')
			});

		},
		createTeam: function (id_uniq, data) {

			return $http.post(CommonServices.url()+'api/player/team', data, {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getTeams: function (id_uniq) {

			return $http.get(CommonServices.url()+'api/player/teams', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('players' + id_uniq + 'teams')
			});

		},
		getTeam: function (id_uniq, id_team_player) {

			return $http.get(CommonServices.url()+'api/player/team', {
				params:{
					id_uniq: id_uniq,
					id_team_player: id_team_player
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('players' + id_uniq + 'teams' + id_team_player)
			});

		},
		editTeam: function (id_uniq, id_team_player, data) {

			return $http.put(CommonServices.url()+'api/player/team', data, {
				params:{
					id_uniq: id_uniq,
					id_team_player: id_team_player
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getUniqIDs: function () {

			return $http.get(CommonServices.url()+'api/player/uniqids', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('playerUniqIDs')
			});

		},
		getBaseImages: function (id_uniq) {

			return $http.get(CommonServices.url()+'api/player/base_images', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getBaseHeadImage: function (id_uniq) {

			return $http.get(CommonServices.url()+'api/player/base_head_image', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getBaseBodyImage: function (id_uniq) {

			return $http.get(CommonServices.url()+'api/player/base_body_image', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},

		delete: function (data) {
			return $http.put(CommonServices.url()+'api/player/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put(CommonServices.url()+'api/player/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		deleteTeam : function (data) {
			return $http.put(CommonServices.url()+'api/player/deleteteam', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multideleteTeam : function (data) {
			return $http.put(CommonServices.url()+'api/player/multideleteteam', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}]);