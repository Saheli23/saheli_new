var stf = angular.module('CommonServices', []);
stf.factory('CommonServices', ['$location', function ($location) {
	return {
		url: function () {
			//return '/stf-web/';
			//return '/monkey_challenge/';
			return '/STF/';
			//return 'http://oneup.us-west-2.elasticbeanstalk.com:80/';
            //return 'http://demoseat.com/STF/';
		},
        
		target: function () {
			return '/MKC/';
			//return 'http://oneup.us-west-2.elasticbeanstalk.com:80/';
            //return 'http://demoseat.com/STF/';
		}
        
	};
}]);