var stf = angular.module('AuthServices', []);

stf.factory('Auth', ['$http', '$window', '$location','CommonServices', function ($http, $window, $location,CommonServices) {


	return {
		login: function (data) {

			return $http.post(CommonServices.url()+'api/auth', data);

		},
		getAccessToken: function (data) {


			return $http.post('/oauth/access_token', data, {
					transformRequest: angular.identity,
					headers: {
						'Content-Type': "application/x-www-form-urlencoded",

					}
			});


            // return $http.post(CommonServices.target()+'oauth/access_token', data, {
						// 		transformRequest: angular.identity,
						// 		headers: {
						// 			'Content-Type': "application/x-www-form-urlencoded",
						//
						// 		}
						// });
		},

		createToken: function (data, data2) {

			$window.localStorage.setItem('token', data);
			$window.localStorage.setItem('authorization', data2);

		},
		checkToken: function () {

			if ($window.localStorage.token === undefined) { $location.path('/'); };

		},
		goToDashboardIfToken: function () {

			if ($window.localStorage.token !== undefined) { $location.path('/dashboard'); };

		},
		deleteToken: function () {
			$window.localStorage.removeItem('token');
			$window.localStorage.removeItem('authorization');
			$location.path('/');
		}
	};
}]);
