var stf = angular.module('MovementServices', []);

stf.factory('Movement', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	return {
		check: function (id_player, id_event, data) {
			return $http.post(CommonServices.url()+'api/movement/check', data, {
				params: {
					id_player: id_player,
					id_event: id_event
				},
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		createLinear: function (data) {
			return $http.post(CommonServices.url()+'api/movement/linear', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		createLateral: function (data) {
			return $http.post(CommonServices.url()+'api/movement/lateral', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		createZigzag: function (data) {
			return $http.post(CommonServices.url()+'api/movement/zigzag', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		updateLinear: function (data) {
			return $http.put(CommonServices.url()+'api/movement/linear', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		updateLateral: function (data) {
			return $http.put(CommonServices.url()+'api/movement/lateral', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		updateZigzag: function (data) {
			return $http.put(CommonServices.url()+'api/movement/zigzag', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		getAll: function (id_uniq_event) {
			return $http.get(CommonServices.url()+'api/movement/event', {
				params: {
					id_uniq_event: id_uniq_event
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('movementEvent' + id_uniq_event)
			});
		},
		delete: function (data) {
			return $http.put(CommonServices.url()+'api/movement/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put(CommonServices.url()+'api/movement/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		getDetails: function (id_uniq_event, id_uniq_player ) {
			return $http.get(CommonServices.url()+'api/movement/linear', {
				params: {
					id_uniq_event: id_uniq_event,
					id_uniq_player: id_uniq_player
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('movementEvent' + id_uniq_event)
			});
		}
	};
}])