var stf = angular.module('HeadingServices', []);

stf.factory('Heading', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	

	return {
		check: function (id_player, id_event, data) {
			return $http.post(CommonServices.url()+'api/heading/check', data, {
				params: {
					id_player: id_player,
					id_event: id_event
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		createForce: function (data) {

			return $http.post(CommonServices.url()+'api/heading/force', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},

		updateForce: function (data) {
			return $http.put(CommonServices.url()+'api/heading/force', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		updateAim: function (data) {
			return $http.put(CommonServices.url()+'api/heading/aim', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		updateJump: function (data) {
			return $http.put(CommonServices.url()+'api/heading/jump', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		createAim: function (data) {

			return $http.post(CommonServices.url()+'api/heading/aim', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},

		createJump: function (data) {
			return $http.post(CommonServices.url()+'api/heading/jump', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		
		delete: function (data) {
			return $http.put(CommonServices.url()+'api/heading/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put(CommonServices.url()+'api/heading/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		getAll: function (id_uniq_event) {

			return $http.get(CommonServices.url()+'api/heading/event', {
				params: {
					id_uniq_event: id_uniq_event
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('headingEvent' + id_uniq_event)
			});

		}, 

		getDetails: function (id_uniq_event, id_uniq_player ) {

			return $http.get(CommonServices.url()+'api/heading/force', {
				params: {
					id_uniq_event: id_uniq_event,
					id_uniq_player: id_uniq_player
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('headingEvent' + id_uniq_event)
			});

		}
	};
}])