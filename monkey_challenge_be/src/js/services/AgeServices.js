var stf = angular.module('AgeServices', []);

stf.factory('Age', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window, STFcache) {
	

	return {
		getRange: function () {

			return $http.get(CommonServices.url()+'api/age/range', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('agerange')
			});

		}
	};
}])