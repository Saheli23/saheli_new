var stf = angular.module('UserServices', []);

stf.factory('User', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	

	return {
		createUser: function (data,authorizationCookie) {
           
			return $http.post(CommonServices.target()+'internal/users', data, {
				/*params: {
					token: $window.localStorage.token
				},*/
				transformRequest: angular.identity,
				headers: {
					'Content-Type': 'application/json',
					'Authorization':authorizationCookie
				}
			});

		},
		getLoggedInUser: function () {
			console.log($window.localStorage.token);
			return $http.get(CommonServices.url()+'api/user/logged_in_user', {
				headers: {
					'token': $window.localStorage.token
				},
				cache: STFcache.save('logged_in_user')
			});

		},
		getAll: function (authorizationCookie) {

			return $http.get(CommonServices.target()+'internal/users', {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Authorization':authorizationCookie

				},
				cache: STFcache.save('users')
			});

		},
		getOne: function (id_uniq) {

			return $http.get(CommonServices.target()+'internal/users/'+id_uniq, {
				
				headers: {
					'Content-Type': 'application/json',
					'Authorization':authorizationCookie
				},
				cache: STFcache.save('users' + id_uniq)
			});

		},
		editUser: function (id_uniq, data,authorizationCookie) {

			return $http.put(CommonServices.target()+'internal/users/'+id_uniq, data, {
				
				transformRequest: angular.identity,
				headers: {
					'Content-Type': 'application/json',
					'Authorization':authorizationCookie
				}
			});

		},
		getUserChallenges: function (id_uniq,authorizationCookie) {

			return $http.get(CommonServices.target()+'internal/users/'+id_uniq+'/challenges', {
				
				headers: {
					'Content-Type': 'application/json',
					'Authorization':authorizationCookie
				},
				
			});

		},
		getUniqIDs: function () {

			return $http.get(CommonServices.url()+'api/user/uniqids', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('userUniqIDs')
			});

		},
		getBaseImage: function (id_uniq) {

			return $http.get(CommonServices.url()+'api/user/base_image', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},

		delete: function (uuid,authorizationCookie) {
			return $http.delete(CommonServices.target()+'/internal/users/'+uuid, {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Authorization':authorizationCookie

				}
			});
		},

		multiDelete: function (data) {
			return $http.put(CommonServices.url()+'api/user/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}]);