var stf = angular.module('StationConfigServices', []);

stf.factory('Station', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	

	return {
		getStationsAbleToConfigure: function () {

			return $http.get(CommonServices.url()+'api/station/able', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('stationsAbleToConfigure')
			});

		},
		createConfiguration: function (data) {

			return $http.post(CommonServices.url()+'api/station/configuration', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		editConfiguration: function (id, data) {

			return $http.put(CommonServices.url()+'api/station/configuration', data, {
				params: {
					id: id
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getConfiguration: function (id) {

			return $http.get(CommonServices.url()+'api/station/configuration', {
				params: {
					id: id
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('stationConfig' + id)
			});

		},
		getConfigurations: function () {

			return $http.get(CommonServices.url()+'api/station/configuration', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('stationsConfigAll')
			});

		},
		getTests: function (id_station) {

			return $http.get(CommonServices.url()+'api/station/tests', {
				params: {
					id_station: id_station
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('tests' + id_station)
			});

		},

		delete: function (data) {
			return $http.put(CommonServices.url()+'api/station/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		multiDelete: function (data) {
			return $http.put(CommonServices.url()+'api/station/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}]);