var stf = angular.module('ImageServices', []);

stf.factory('Image', ['$http', '$window','CommonServices', function ($http, $window,CommonServices) {
	

	return {
		set: function (data) {

			return $http.post(CommonServices.url()+'api/image/set', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		playerSet: function (data) {

			return $http.post(CommonServices.url()+'api/image/player_set', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		}
	};
}])