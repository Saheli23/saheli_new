var stf = angular.module('ChallengeTypeServices', []);

stf.factory('ChallengeType', ['$http', '$window', 'STFcache','CommonServices', function ($http, $window,STFcache,CommonServices) {
	

	return {
		getChallengeType: function () {

			return $http.get(CommonServices.target()+'api/types/challenge', {
				headers: {
					'authorization': $window.localStorage.authorization
				},
				cache: STFcache.save('challengetype')
			});

		},
		getChallengeCategory: function (id_country) {

			return $http.get(CommonServices.target()+'api/types/category', {
				
				headers: {
					'authorization': $window.localStorage.authorization
				},
				cache: STFcache.save('challengecategory')
			});

		},
		getCharityCategory: function () {

			return $http.get(CommonServices.target()+'api/types/charitycategory', {
				
				headers: {
					'authorization': $window.localStorage.authorization
				},
				cache: STFcache.save('challengecategory')
			});

		}
	};
}]);