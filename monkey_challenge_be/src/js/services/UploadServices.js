var stf = angular.module('UploadServices', []);

stf.factory('Upload', ['$http', '$rootScope', '$window', 'CommonServices', function ($http, $rootScope, $window,CommonServices) {
	

	return {
		uploadProfilePicture: function (data) {

			return $http.post(CommonServices.url() + 'upload', data, {

				params: {
					token: $window.localStorage.token
				},
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined
				}

			});

		},
		uploadPlayerPictures: function (data) {

			return $http.post(CommonServices.url() + 'upload/player', data, {

				params: {
					token: $window.localStorage.token
				},
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined
				}

			});

		}
	};
}]);