var express    = require('express');
var path       = require('path');
var proxy      = require('express-http-proxy');
var _          = require('lodash');
var bodyParser = require('body-parser');

var app        = express();

app.use('/dist', express.static('dist'));
app.use('/src', express.static('src'));
app.use('/bower_components', express.static('bower_components'));
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname,'index.html'));
});

var mode = "demoseat";
var url,slug;

var clientSecret = {client_id: 'gsqm2F7Yxv7fcJaux20D', client_secret: '6eLhtyD9KgEuXyq6qVef'};

if( mode == "dev" ){
  url = 'localhost';
  slug = 'monkey_challenge'
} else if ( mode == "demoseat") {
  url = 'demoseat.com';
  slug = 'STF';
} else if ( mode == "live") {
  url = 'dev01.developer24x7.com';
  slug = 'stf-web';
}

// app.use('/'+slug, proxy(url, {
//   reqBodyEncoding: null,
//   reqAsBuffer: true,
//   limit: "40mb",
//   forwardPath: function(req, res) {
//     console.log(req.url, '-------','/'+slug+require('url').parse(req.url).path);
//     return '/'+slug+require('url').parse(req.url).path;
//   },
//   decorateRequest: function(proxyReq, originalReq) {
//     return proxyReq;
//   }
// }));




app.use('/MKC', proxy('https://api.monkeychallenges.com', {
  reqBodyEncoding: null,
  reqAsBuffer: true,
  limit: "40mb",
  forwardPath: function(req, res) {
    console.log(req.url, '-------','/MKC'+require('url').parse(req.url).path);
    return require('url').parse(req.url).path;
  },
  decorateRequest: function(proxyReq, originalReq) {
   //console.log(proxyReq);
    _.assign(proxyReq.headers, clientSecret);
    console.log(proxyReq);
    // proxyReq.bodyContent = _.keys(_.mapKeys(_.defaults(proxyReq.bodyContent,clientSecret), function(value, key) {
    //   return key +'='+ value;
    // })).join('&');
    return proxyReq;

  }
}));


app.use('/monkey_challenge/api/auth',bodyParser.urlencoded({extended: true}));

app.use('/monkey_challenge/api/auth', proxy('localhost', {
  reqBodyEncoding: null,
  reqAsBuffer: true,
  forwardPath: function(req, res) {
    return '/monkey_challenge/api/auth';
  },
  decorateRequest: function(proxyReq, originalReq) {
    if(proxyReq.method == "POST" && proxyReq.headers['content-type'] == "application/x-www-form-urlencoded" && _.isObject(proxyReq.bodyContent)){
        proxyReq.bodyContent = _.keys(_.mapKeys(proxyReq.bodyContent, function(value, key) {
          return key +'='+ value;
        })).join('&');
    }
    return proxyReq;
  }
}));

app.use('/oauth',bodyParser.urlencoded({extended: true}));

app.use('/oauth', proxy('oneup-env.us-west-2.elasticbeanstalk.com', {
  reqBodyEncoding: null,
  reqAsBuffer: true,
  forwardPath: function(req, res) {
    //return '/monkey_challenge/api/auth';
    console.log(req.url, 'forward',require('url').parse(req.url).path);
    return '/oauth'+require('url').parse(req.url).path;
  },
  decorateRequest: function(proxyReq, originalReq) {
    if(proxyReq.method == "POST" && proxyReq.headers['content-type'] == "application/x-www-form-urlencoded" && _.isObject(proxyReq.bodyContent)){
        proxyReq.bodyContent = _.keys(_.mapKeys(proxyReq.bodyContent, function(value, key) {
          return key +'='+ value;
        })).join('&');
    }
    return proxyReq;
  }
}));





app.listen(3030, function () {
    console.log('Example app listening on port 3030!');
});
