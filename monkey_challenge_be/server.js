var server   = require('./bootstrap.js');
var path     = require('path')
var express  = require('express');
var proxy    = require('express-http-proxy');

server.setup(function (runningApp) {

    app.use('/dist', express.static('dist'));
    app.use('/src', express.static('src'));

    app.get('/', function (req, res) {
        res.sendFile(path.join(__dirname,'index.html'));
    });

    runningApp.use('/api', proxy('localhost/monkey_challenge/api', {
        reqBodyEncoding: null,
        reqAsBuffer: true,
        limit: "40mb",
        forwardPath: function(req, res) {
          console.log(req.url, '-------',require('url').parse(req.url).path);
          return '/api'+require('url').parse(req.url).path;
        },
        decorateRequest: function(proxyReq, originalReq) {
          return proxyReq;
        }
    }));    
});
