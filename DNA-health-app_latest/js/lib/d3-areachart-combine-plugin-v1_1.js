
/* 
 *  Document   : D3 area Chart Plugin.
 *  Created on : 7 Jul, 2015, 11:50:59 PM
 *  Author     : ujjal sannyal
 *  Version    : 1.00
 */

var d3areachart = (function(){
    var _d3areachart  = function(param){
        var defaults = {
            width: 200,
            height: 200,
            data: null,
            originalData: {},
            color: null,
            colorGenerate: "false",
            startingColor: "B",
            padding: 5,
            marginTop: 20,
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 20,
            yaxisLabel: "Number",
            container: "body",
            xAxisField: "name",
            yAxisField: "amount",
            interpolate: "basis",    // basis / linear
            isTip: "false",
            usePathColor: "false",
            tipColor: "#FFFFFF",
            tipTextColor: "#000000",
            dataAlignXaxix: "break",  // rotate / break
            isComaseparetedVal: "false"
        }
        //merging parameter object to default object
        var opts = $.extend({},defaults, param);
        //replace default to "this" obj....
        $.extend(this, opts);
        try{
            this.width = parseInt(this.width, 10);
            this.height = parseInt(this.height, 10);
            this.padding = parseInt(this.padding, 10);
            this.marginTop = parseInt(this.marginTop, 10);
            this.marginLeft = parseInt(this.marginLeft, 10);
            this.marginRight = parseInt(this.marginRight, 10);
            this.marginBottom = parseInt(this.marginBottom, 10);
            this.yaxisLabel = this.yaxisLabel.toString();
            this.xAxisField = this.xAxisField.toString();
            this.yAxisField = this.yAxisField.toString();
            this.interpolate = this.interpolate.toString();
            this.isTip = $.type(this.isTip) === "string" ? returnBoolean(this.isTip) : this.isTip;
            this.usePathColor = $.type(this.usePathColor) === "string" ? returnBoolean(this.usePathColor) : this.usePathColor;
            this.tipColor = this.tipColor.toString();
            this.tipTextColor = this.tipTextColor.toString();
            this.dataAlignXaxix = this.dataAlignXaxix.toString();
            this.colorGenerate = $.type(this.colorGenerate) === "string" ? returnBoolean(this.colorGenerate) : this.colorGenerate;
            this.startingColor = this.startingColor.toString();
            this.isComaseparetedVal = $.type(this.isComaseparetedVal) === "string" ? returnBoolean(this.isComaseparetedVal) : this.isComaseparetedVal;
            this.init();
        }catch(error){
            $.error(error);
            console.log("Some Error Found: "+error);
        }
        return this; 
    }
    _d3areachart.prototype = {
        init: function(){
//Calculate Height & Width..
            this.width = this.width - this.marginLeft - this.marginRight;
            this.height = this.height - this.marginTop - this.marginBottom;
//Set Week Format and Time format Function..             
            this.week = d3.time.format("%Y-%b-%d").parse;
            this.customTimeFormat = d3.time.format("%b"); 
            
            
            if(this.colorGenerate){
                var colorPar = {
                        "length": 1,
                        "colortype": "gen",
                        "startingColor": this.startingColor
                    };
                    this.color = getColor(colorPar);
            }else if(this.color !== null){
//this.color array set.
                // var color = this.color;
                // this.color = [];
                // this.color[0] = color;
               var colorAr = this.color.split(",");
                this.color = d3.scale.ordinal()
                .range(colorAr);
            }else{
                var colorPar = {
                    "length": 1,
                    "colortype": "gen",
                    "startingColor": this.startingColor
                };
                this.color = getColor(colorPar);
            }
            
            this.x = d3.time.scale()
            .range([0, this.width]);

            this.y = d3.scale.linear()
                .range([this.height, 0]);
        
            this.xAxis = d3.svg.axis()
                .scale(this.x)
                .tickFormat(this.customTimeFormat)
                .orient("bottom")
                .ticks(this.data.length);

            this.yAxis = d3.svg.axis()
                .scale(this.y)
                .orient("left")
                // .tickFormat(d3.format(".2s"))
                .ticks(Math.ceil( this.height / 60));

            this.stack = d3.layout.stack()
                .values(function(d) { return d.values; });
                
                this.render();
        },
        render: function(){
            var self = this;
            
            this.area = d3.svg.area()
                .interpolate(this.interpolate)
                .x(function(d) {
                    return self.x(d.date);})
                .y0(function(d) {return self.y(d.y0);})
                .y1(function(d) {return self.y(d.y0 + d.y);});
                
            this.svg = d3.select(this.container).append("svg")
                .attr("width", this.width + this.marginLeft + this.marginRight)
                .attr("height", this.height + this.marginTop + this.marginBottom)
                .append("g")
                .attr("transform", "translate(" + this.marginLeft  + "," + this.marginTop + ")");


                
            this.afterRender();
        },
        afterRender: function(){
//If data is valid or not.
            if(this.data != null && this.data.length > 0){
                this.showGraph();
            }else{
                
                this.callNotAvailable();
            }

//Tip Function Calling..
            if(this.isTip){
                this.callTip();
            }  
        },
        prepareData: function(){
            var self = this;
            this.originalData = [];
            this.originalData = $.extend(true, [], this.data);
            
//            alert("pre:this.originalData: "+JSON.stringify(this.originalData));

            this.data.forEach(function(d) {
                    d[self.xAxisField] = self.week(d[self.xAxisField]);
                });

            this.color.domain(d3.keys(this.data[0]).filter(function(key) { return key !== self.xAxisField; }));
 
            var stackData = this.color.domain().map(function(name) {
                var obj = {
                    name: name,
                    values: self.data.map(function(d) {
                        return {date: d[self.xAxisField], y: d[name] };
                    })
                };
                return obj;
            });
            this.browsers = this.stack(stackData);
        },
        showGraph: function(){
            var self = this;
            
            this.prepareData();

            var yMax = d3.max(this.data, function(d) {
                return eval(self.color.domain().map(function(name){
                    return d[name];
                }).join("+"));
            });

//Set Y domain and X domain value.
            this.x.domain(d3.extent(this.data, function(d) {return d[self.xAxisField];}));
            this.y.domain([0, yMax]);
//Set Style and format of text shown in X axis..
            this.text = this.svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + this.height + ")")
                .call(this.xAxis)
                .selectAll("text");
// Set style(rotate or break) text on x-axis..
            if(this.dataAlignXaxix == "break"){
//                this.text.call(dataAlignXaxix, this.x.rangeBand());
            }else if(this.dataAlignXaxix == "rotate"){
                this.text.style("text-anchor", "end")
                .attr("dx", "-.8em")
                .attr("dy", ".15em")
                .attr("transform", function(d) {
                        return "rotate(-41)"
                    });
            }
//Append @g for Y-axis, and append lable text for y-axis..
            this.svg.append("g")
                .attr("class", "y axis")
                .call(this.yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text(this.yaxisLabel);

                var grid = [];
                this.svg.select(".y.axis").selectAll(".tick").each(function(data) {
                  var tick = d3.select(this);
                  // pull the transform data out of the tick
                  var transform = d3.transform(tick.attr("transform")).translate;
                  // passed in "data" is the value of the tick, transform[0] holds the X value
                  grid.push({
                    'x1':0,
                    'y1':transform[1],
                    'x2':self.width,
                    'y2':transform[1],
                    'data':data
                  });
                });
                var grids = this.svg.append('g')
                      .attr('id','grid')
                      .attr("transform","translate(0,0)")
                      .selectAll('line')
                      .data(grid)
                      .enter()
                      .append('line')
                      .attr({'x1':function(d,i){ return d.x1; },
                             'y1':function(d,i){ return d.y1; },
                             'x2':function(d,i){ return d.x2; },
                             'y2':function(d,i){ return d.y2; },
                        })
                      .style({'stroke':'#adadad','stroke-width':'1px'});
                      
//Set path for the area..
            this.browser = this.svg.selectAll(".browser")
                    .data(this.browsers)
                    .enter().append("g")
                    .attr("class", "browser");

            this.path = this.browser.append("path")
                .attr("class", "area dynamic_chart")
                .attr("d", function(d){
                    return self.area(d.values)})
                .style("fill", function(d) { return self.color(d.name); });
        },
        callTip: function(){
/* 
 * Append a div to the body for tip use............. 
 */
            var self = this;
            this.tip = "";
            if($(".toolTip").length === 0){
                this.tip = d3.select("body").append("div").attr("class", "toolTip").style("background", "#fff");
            }else{
                this.tip = d3.select(".toolTip");
            }
// Get Next Value..
            var bisectDate = d3.bisector(function(d) {return d[self.xAxisField];}).left;

            this.path.on("mousemove", function(d){
                var name = d.name,
                    x0 = self.x.invert(d3.mouse(this)[0]),
                    i = bisectDate(self.data, x0, 1),
                    d0 = self.data[i - 1],
                    d1 = self.data[i],
                    d = x0 - d0[self.xAxisField] > d1[self.xAxisField] - x0 ? d1 : d0,
                    value = d[name];

                    self.tip.style("display", "inline-block");
                    self.tip.style("white-space", "nowrap");
                    if(self.isComaseparetedVal){
                        value = addCommaSeparator(d[name], "l", ",", "n");
                    }
                    // var html = self.customTimeFormat(d[self.xAxisField])+" : <span><strong> "+value+"</strong></span>";                        
                    var html = "<span class='tooltip-value'><strong> "+value+"</strong></span></br><span>"+self.customTimeFormat(d[self.xAxisField])+"</span>";
                    self.tip.html(html);
                    var divWidth = d3.event.pageX+10 + $(".toolTip:visible").width(),
                        tipHeight = parseFloat($(".toolTip").height())+10; 
                    if(divWidth > $(window).width()){
                        var pos = d3.event.pageX - 10 - $(".toolTip:visible").width();
                        self.tip.style("left", pos+"px"); 
                    }else{
                        self.tip.style("left", (d3.event.pageX -($(".toolTip:visible").width() / 2) - 10)+"px");
                    }
                    var topY = (parseFloat(d3.event.pageY-tipHeight) < 10) ? parseFloat(d3.event.pageY + tipHeight) : parseFloat(d3.event.pageY-tipHeight);
                    self.tip.style("top", (topY - 30)+"px");
                    if(self.usePathColor){
                        self.tip.style("background", self.color(name));
                    }else{
                        self.tip.style("background", self.tipColor);
                    }
                    self.tip.style("color", self.tipTextColor);
                    self.tip.style("z-index", "99999999");
            })
            .on("mouseout", function(d){
                self.tip.style("display", "none");
            });
        },
        callNotAvailable: function(){
            this.svg.append("g")
                .attr("transform", "translate(" + ((this.width / 2) - this.marginLeft) + "," + ((this.height / 2) - this.marginTop) + ")")
                .append("text")
                .attr("class", "na-class")
                .attr("x",25)
                .attr("y", 9)
                .attr("dy", ".35em")
                .text("N / A");
        }
    };
    return _d3areachart;
})();
