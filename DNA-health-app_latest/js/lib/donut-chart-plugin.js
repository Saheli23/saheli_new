/* 
 *  Document   : D3 Pie and Donut Chart Plugin.
    Created on : 12 Feb, 2015, 12:17:59 PM
    Author     : ujjal sannyal
    version    : 2
 */


/* 
 * @param: {object}
 * @object: 
 * (1) useTransition: set true for transition the donut path, set false for normal view(@default: true),
 * (2) width: set width(@default: 200),
 * (3) height: set height(@default: 200),
 * (4) transitionDuration: set duration for the transition in milisecond(@default: 500),
 * (5) transitionDelay: set delay for the transition in milisecond(@default: 500),
 * (6) data: data of main donut chart(@default: null),
 * (7) color: main donut color(@default: "#738AFF"),
 * (8) padding: All side padding from border(@default: 0px),
 * (9) thickness: Main donut thickness(@default: 10px),
 * (10) innerRadius: Main donut inner radius(@default: 28px),
 * (11) outerPadding: Main donut outer thickness(@default: 15px),
 * (12) centerLabelPrefix: Center value's prefix(@default: "") ex(Rs, Usd, ect),
 * (13) centerLabelSuffix: Center value's suffix(@default: "") ex(%, ect),
 * (14) centerLabelFontSize: Center Value's font size(@default: 12px),
 * (15) container: Container for donut(@default: "body") use {container: $("id") / container: $("class")},
 * (16) showTotalCount: Show Total Count In the middle(@default: true).
 * (17) centerLabelPrefix: 
 * (18) centerLabelSuffix: "",
   (19) centerLabelFontSize: "12px",
   (20) container: "body",
   (21) subChart: false,
   (22) subChartData: null,
   (23) subChartCenterLabelPrefix: "",
   (24) subChartCenterLabelSuffix: "",
   (25) subChartCenterLabelFontSize: "10px",
   (26) subChartTransition: true,
   (27) subChartTransitionWait: true,
   (28) subChartColor: "#F1F1F1",
   (29) isTip: false,
   (30) afterTransition: function(){}
 **/

var donutchart = (function(){
    var _donutchart = function(param){
/**
* Default Value
**/
        var defaults = {
            width: 200,
            height: 200,
            data: null,
            container: "body",
            chartType: "donut",             //pi / donut
            chartStyle: "full",              //full/half
            useTransition: true,
            transitionDuration: 500,
            transitionDelay: 500,
            color: null,                // auto / ['#00FF00', '#']
            colorGenerate: "false",
            startingColor: "B",
            padding: 0,
            thickness: 10,
            innerRadius: 28,
            outerPadding: 15,
            showTotalCount: "true",
            centerLabelValue: "",
            centerLabelPrefix: "",
            centerLabelSuffix: "",
            centerLabelFontSize: "12",
            isLabelOnPath: "false",
            isAnchorLabel: "false",
            anchorDistance: 10,
            dataPercent: false,
            subChart: "false",
            subChartData: null,
            subChartInnerRadius: 20,
            subChartCenterLabelPrefix: "",
            subChartCenterLabelSuffix: "",
            subChartCenterLabelFontSize: "10",
            isSubChartTotal: "true",
            subChartTransition: "true",
            subChartTransitionWait: "true",
            subChartColor: "#F1F1F1",
            isTip: "false",
            usePathColor: "false",
            tipColor: "#FFFFFF",
            tipTextColor: "#000000",
            tipFunction: "null",
            isComaseparetedVal: "false"
        };
        //merging parameter object to default object
        var opts = $.extend({},defaults, param);
        //replace default to "this" obj....
        $.extend(this, opts);
        try{

            this.width = parseInt(this.width, 10);
            this.height = parseInt(this.height, 10);
            this.container = this.container.toString();
            this.chartType = this.chartType.toString();
            this.chartStyle = this.chartStyle.toString();
            this.useTransition = $.type(this.useTransition) === "string" ? returnBoolean(this.useTransition) : this.useTransition;
            this.transitionDuration = parseInt(this.transitionDuration, 10);
            this.transitionDelay = parseInt(this.transitionDelay, 10);
            this.colorGenerate = $.type(this.colorGenerate) === "string" ? returnBoolean(this.colorGenerate) : this.colorGenerate;
            this.startingColor = this.startingColor.toString();
            this.padding = parseInt(this.padding, 10);
            this.thickness = parseInt(this.thickness, 10);
            this.innerRadius = parseInt(this.innerRadius, 10);
            this.outerPadding = parseInt(this.outerPadding, 10);
            this.showTotalCount = $.type(this.showTotalCount) === "string" ? returnBoolean(this.showTotalCount) : this.showTotalCount;
            this.centerLabelValue = this.centerLabelValue.toString();
            this.centerLabelPrefix = this.centerLabelPrefix.toString();
            this.centerLabelSuffix = this.centerLabelSuffix.toString();
            this.centerLabelFontSize = this.centerLabelFontSize.toString();
            this.isLabelOnPath = $.type(this.isLabelOnPath) === "string" ? returnBoolean(this.isLabelOnPath) : this.isLabelOnPath;
            this.isAnchorLabel = $.type(this.isAnchorLabel) === "string" ? returnBoolean(this.isAnchorLabel) : this.isAnchorLabel;
            this.dataPercent = $.type(this.dataPercent) === "string" ? returnBoolean(this.dataPercent) : this.dataPercent;
            this.anchorDistance = parseInt(this.anchorDistance, 10);
            this.subChart = $.type(this.subChart) === "string" ? returnBoolean(this.subChart) : this.subChart;
            this.subChartInnerRadius = parseInt(this.subChartInnerRadius, 10);
            this.subChartCenterLabelPrefix = this.subChartCenterLabelPrefix.toString();
            this.subChartCenterLabelSuffix = this.subChartCenterLabelSuffix.toString();
            this.subChartCenterLabelFontSize = this.subChartCenterLabelFontSize.toString();
            this.isSubChartTotal = $.type(this.isSubChartTotal) === "string" ? returnBoolean(this.isSubChartTotal) : this.isSubChartTotal;
            this.subChartTransition = $.type(this.subChartTransition) === "string" ? returnBoolean(this.subChartTransition) : this.subChartTransition;
            this.subChartTransitionWait = $.type(this.subChartTransitionWait) === "string" ? returnBoolean(this.subChartTransitionWait) : this.subChartTransitionWait;
            this.subChartColor = this.subChartColor.toString();
            this.isTip = $.type(this.isTip) === "string" ? returnBoolean(this.isTip) : this.isTip;
            this.usePathColor = $.type(this.usePathColor) === "string" ? returnBoolean(this.usePathColor) : this.usePathColor;
            this.tipColor = this.tipColor.toString();
            this.tipTextColor = this.tipTextColor.toString();
            this.isComaseparetedVal = $.type(this.isComaseparetedVal) === "string" ? returnBoolean(this.isComaseparetedVal) : this.isComaseparetedVal;
            this.tipFunction = (this.tipFunction == null || this.tipFunction === "null") ? null : this.tipFunction;

            this.init();
        }catch(error){
            console.log("Donut Chart Error Found: "+error);
        }
        return this;
    }

    _donutchart.prototype = {
        init: function(){
            var self = this;
            
            this.PiRadians = 2 * Math.PI;
            this.outerWidth = parseFloat(this.width) - (parseFloat(this.padding) * 2);
            this.outerHeight = parseFloat(this.height) - (parseFloat(this.padding) * 2);
            if(this.chartStyle.toLowerCase() === "half"){
                this.radius = Math.min(parseFloat(this.outerWidth), (parseFloat(this.outerHeight)*2)) / 2;
            }else{
                this.radius = Math.min(parseFloat(this.outerWidth), parseFloat(this.outerHeight)) / 2;
            }

            self.innerRadius = (self.innerRadius >= self.radius || self.innerRadius < 0) ? (self.radius - self.thickness) : self.innerRadius;
            var outerRadius = (self.outerPadding > self.radius || self.outerPadding < 0) ? (self.radius - 5) : (self.radius - self.outerPadding);

            self.innerRadius = self.innerRadius > outerRadius ?  
                (self.innerRadius - (self.innerRadius - outerRadius) - 10) : 
                (self.innerRadius == outerRadius) ? 
                (self.innerRadius - 10) : self.innerRadius;
                
            self.thickness = (outerRadius - self.innerRadius);
            this.ancLabel = outerRadius + this.anchorDistance;
//Set Color Arr..
            if(this.colorGenerate){
                var colorPar = {
                    "length": this.data.length,
                    "colortype": "gen",
                    "startingColor": this.startingColor
                };
                this.colorArr = d3.scale.ordinal()
                .range(getColor(colorPar));
            }else if(this.color !== null){
                if(this.color.toLowerCase() == "auto"){
                    this.colorArr = d3.scale.category20c();
                }else{
                    var colorAr = this.color.split(",");
                    this.colorArr = d3.scale.ordinal()
                    .range(colorAr);
                }
            }else{
                var colorPar = {
                    "length": this.data.length,
                    "colortype": "gen",
                    "startingColor": this.startingColor
                };
                this.colorArr = d3.scale.ordinal()
                .range(getColor(colorPar));
            }
            
            this.data = this.prepareData(this.data);
            
            this.arc = d3.svg.arc()
            .outerRadius(function(d){
                return d.outerRadius || outerRadius;
            })
            .innerRadius(function(d){
                if(self.chartType.toLowerCase() == "donut"){
                    return d.innerRadius || self.innerRadius;
                }else if(self.chartType.toLowerCase() == "pi"){
                    return 0;
                }
            })
            .startAngle(function(d){
                return d.startAngle || 0;
            })
            .endAngle(function(d, i){
                return d.endAngle || 0;
            });

            this.pie = d3.layout.pie()
            .sort(null)
            .value(function(d) {
                return d.value;
            });

            if(this.chartStyle.toLowerCase() === "half"){
                this.pie.startAngle(-90 * (Math.PI/180))
                .endAngle(90 * (Math.PI/180));
            }
            

            this.render();
        },
        render: function(){
            var self = this;
            this.svg = d3.select(this.container).append("svg")
            .attr("width", parseFloat(this.width))
            .attr("height", parseFloat(this.height))
            .append("g")
            .attr("transform", "translate(" + parseFloat(this.width) / 2 + "," + parseFloat(this.height - 40) + ")");
            
            if(this.data.details.length > 0){
                this.g = this.svg.selectAll(".arc")
                .data(this.pie(this.data.details))
                .enter().append("g")
                .attr("class", "arc");

                this.path = this.g.append("path")
                .style("fill", function(d){
                    return self.colorArr(d.data.name);
                });
//Set Transition if applicable..
                if(this.useTransition){
                    this.path.transition().delay( parseFloat(self.transitionDelay) ).duration( parseFloat(self.transitionDuration) )
                    .attr("d", this.arc)
                    .attrTween("d", function(d) {
                        var interpolate = d3.interpolate(d.startAngle, d.endAngle);
                        return function(t) {
                            d.endAngle = interpolate(t);
                            return self.arc(d);
                        }
                    });
                }else{
                    this.path.attr("d", this.arc);
                }

                this.afterRender();
            }else{
                this.notAvailable();
            }
        },
        afterRender: function(){
            var self = this;
//Label On Path.
            if(this.isLabelOnPath){
                this.labelOnPath();
            }
//Call Sub Chart if Applicable
            if(this.subChart){
                this.subChartRender();
            }
//Show Total Count if Applicable
            if(this.showTotalCount){
                self.centerLabelFontSize = isNaN(self.centerLabelFontSize) ? self.centerLabelFontSize : self.centerLabelFontSize+"px";
                this.text1 = this.svg
                .select("g")
                .append("text")
                .attr("class", "total")
                .attr("y", 0)
                .attr("x", 0)
                .attr("dy", ".35em")
                .style("text-anchor", "middle")
                .style("font-size", self.centerLabelFontSize);
//Add Transition to 'text' if Applicable.
                if(this.useTransition){
                    var centerLabelValue = self.centerLabelValue != "" ? parseFloat(self.centerLabelValue) : self.data.total;
//                    this.text1.text("")
//                    .datum(function(d){
//                        var obj = {
//                            prefix: self.centerLabelPrefix,
//                            value :Math.round(centerLabelValue),
//                            suffix: self.centerLabelSuffix
//                        }
//                        return obj;
//                    })
//                    .transition().delay( parseFloat(self.transitionDelay) ).duration( parseFloat(self.transitionDuration) )
//                    .tween("text",  self.labelTween_T)

                    this.text1                    
                    .call(dataAlignCenter, self.centerLabelPrefix, self.centerLabelSuffix)
                    .select(".center_val")
                    .datum(function(d){
                        var obj = {
                            prefix: self.centerLabelPrefix,
                            value :Math.round(centerLabelValue),
                            suffix: self.centerLabelSuffix
                        }
                        return obj;
                    })
                    .transition().delay( parseFloat(self.transitionDelay) ).duration( parseFloat(self.transitionDuration) )
                    .tween("text",  self.labelTween_T);
                    
                    this.text1.select(".prefix")
                    .style("font-size", "1.4vw")
                    .transition().delay( parseFloat(self.transitionDelay) )
                    .duration( parseFloat(self.transitionDuration) )
                    .text(self.centerLabelPrefix);
                    
                    this.text1.select(".suffix")
                    .style("font-size", "1.4vw")
                    .transition().delay( parseFloat(self.transitionDelay) )
                    .duration( parseFloat(self.transitionDuration) )
                    .text(self.centerLabelSuffix);
                    
                }else{
                    this.text1.text(self.centerLabelPrefix+Math.round(centerLabelValue)+self.centerLabelSuffix);
                }
            }
//Add Tip if isTip is 'True'.
            if(this.isTip){
                this.showTip(this.path);
            }
//Add label on anchor text if Anchor Label is Applicable.
            if(this.isAnchorLabel){
                this.anchorLabel();
            }
             if(this.dataPercent){
                this.attachedDataPercent();
            }
        },
        subChartRender: function(){
            var self = this;
            this.subChartData = this.prepareData(this.subChartData);
            this.ratio = this.subChartData.total / this.data.total;
            this.subChartInnerRadius = this.subChartInnerRadius > this.innerRadius ? (this.innerRadius - 10) : 
            this.subChartInnerRadius < 0 ? 0 : this.subChartInnerRadius;
            this.arc_2 = d3.svg.arc()
            .outerRadius(this.innerRadius)
            .innerRadius(this.subChartInnerRadius)
            .startAngle(function(d){
                return d.startAngle || 0;
            })
            .endAngle(function(d){
                return d.endAngle || 0;
            });

            this.g_2 = this.svg.selectAll(".arc_2")
            .data(this.pie(this.subChartData.details))
            .enter()
            .append("g")
            .attr("class", "arc_2");

            this.path_2 = this.g_2.append("path")
            .datum(function(d){
                d.endAngle = self.ratio * self.PiRadians;
                return d;
            })
            .style("fill", self.subChartColor);

            if(this.subChartTransition){
                var delay = this.subChartTransitionWait ? (parseFloat(self.transitionDelay) + parseFloat(self.transitionDuration)) : self.transitionDelay;
                this.path_2.transition().delay(delay).duration(self.transitionDuration)
                .attr("d", this.arc_2)
                .attrTween("d", function(d){
                    var interpolate = d3.interpolate(d.startAngle, d.endAngle);
                    return function(t) {
                        d.endAngle = interpolate(t);
                        return self.arc_2(d);
                    }
                });
            }else{
                this.path_2.attr("d", this.arc_2);
            }

            if(this.isSubChartTotal){
                this.total_2 = this.svg.selectAll("g")
                .append("text")
                .attr("class", "total-2")
                .attr("y", 32)
                .attr("x", 3)
                .attr("dy", ".35em")
                .style("text-anchor", "middle")
                .style("font-size", self.subChartCenterLabelFontSize);

                if(this.subChartTransition){
                    this.total_2.datum(function(){
                        var obj = {
                            prefix: self.subChartCenterLabelPrefix,
                            value : Math.round(self.ratio*100),
                            suffix: self.subChartCenterLabelSuffix
                        }
                        return obj;
                    })
                    .transition().delay(delay).duration(self.transitionDuration)
                    .tween("text",self.labelTween_T);
                }else{
                    this.total_2.text(self.subChartCenterLabelPrefix+Math.round(self.ratio*100)+self.subChartCenterLabelSuffix);
                }
            }

            if(this.isTip){
                this.showTip(this.path_2);
            }
        },
        labelTween_T: function (a) {
            var i = d3.interpolate(0, a.value);
            return function(t) {
                this.textContent = a.prefix+ " " + Math.round(i(t));
            }
        },
        prepareData: function(data){
            var index_arr = [],
            total = 0,
            self = this;
            data.forEach(function(d, i) {
                self.dataRange = d3.keys(data[i]).filter(function(key){
                    return key;
                });
                self.colorArr(self.dataRange[0]);
                if(d[self.dataRange[0]] != 0){
                    d.name = self.dataRange[0];
                    d.value = d[self.dataRange[0]];
                }else{                                                          // if the value is zero then index is store in array
                    index_arr.push(i);
                }
            });

            // those value is zero they are deleted

            var arr = $.grep(data, function(n, i) {
                return $.inArray(i, index_arr) == -1;
            });

            data = {},
            data.details = [];
            if(arr.length > 0){
                data.details = arr;                                     //saperate values for different chart
            }

            $.each(data.details, function(key, v){
                total = parseInt(total, 10) + parseInt(v.value, 10);
            });

            data.total = total;
            return data;

        },
        showTip: function(pathObj){
            
            var self = this;
            if($(".toolTip").length === 0){
                this.tip = d3.select("body").append("div").attr("class", "toolTip").style("background", "#fff");
            }else{
                this.tip = d3.select(".toolTip");
            }

            pathObj.on("mousemove", function(d){
                var amt = parseFloat(d.data.value) < 0 ? "0" : d.data.value;
                if(self.isComaseparetedVal){
                    amt = addCommaSeparator(amt, "l", ",", "n");
                }
                // var html = d.data.name+" : <span><strong> "+amt+"</strong></span>";
                var html = "<span class='tooltip-value'><strong> "+amt+"</strong></span></br><span>"+d.data.name+"</span>";
                self.tip.style("left", d3.event.pageX+10+"px");
                self.tip.style("top", d3.event.pageY-25+"px");
                self.tip.style("display", "inline-block");
                self.tip.style("white-space", "nowrap");
                self.tip.html(html);

                var totalWidth = d3.event.pageX + $(".toolTip").outerWidth(),
                    actWidth = 0;
                var windWdth = $(window).width(),
                    tipHeight = parseFloat($(".toolTip").height())+10;
                if(windWdth < totalWidth){
                    actWidth = windWdth - $(".toolTip").width() - 15;
                    self.tip.style("left", actWidth+"px");
                }else{
                    self.tip.style("left", (d3.event.pageX - $(".toolTip").outerWidth() / 2)+"px");
                }
                var topY = (parseFloat(d3.event.pageY-tipHeight) < 10) ? parseFloat(d3.event.pageY + 35) : parseFloat(d3.event.pageY-tipHeight-35);
                self.tip.style("top", topY+"px");

                if(self.usePathColor){
                    self.tip.style("background", self.colorArr(d.data.name));
                }else{
                    self.tip.style("background", self.tipColor);
                }
                self.tip.style("color", self.tipTextColor);
                self.tip.style("z-index", "99999999");
            })
            .on("mouseout", function(d){
                self.tip.style("display", "none");
            });
        },
        notAvailable: function(){
            this.svg.append("g")
            .attr("transform", "translate(0, 0)")
            .append("text")
            .attr("class", "na-class")
            .attr("x","-18") 
            .attr("y", "0")
            .text("N / A");
        },
        labelOnPath: function(){
            var self = this;
            if(this.useTransition){
                this.g.append("text")
                .attr("transform", function(d) {
                    return "translate(" + self.arc.centroid(d) + ")";
                })
                .attr("dy", ".35em")
                .style("text-anchor", "middle")
                .text(function(d) {
                    return d.data.name;
                });
            }else{
                this.g.append("text")
                .attr("transform", function(d) {
                    return "translate(" + self.arc.centroid(d) + ")";
                })
                .attr("dy", ".35em")
                .style("text-anchor", "middle")
                .text(function(d) {
                    return d.data.name;
                });
            }
        },
        anchorLabel: function(){
            var self = this;
            this.g.append("svg:text")
            .attr("transform", function(d) {
                var c = self.arc.centroid(d),
                x = c[0],
                y = c[1],
                // pythagorean theorem for hypotenuse
                h = Math.sqrt(x*x + y*y);
                return "translate(" + (x/h * self.ancLabel) +  ',' +
                (y/h * self.ancLabel) +  ")"; 
            })
            .attr("dy", ".35em")
            .attr("text-anchor", function(d) {
                // are we past the center?
                return (d.endAngle + d.startAngle)/2 > Math.PI ?
                "end" : "start";
            })
            .text(function(d, i) {
                return d.data.name;
            });
        },
        attachedDataPercent: function(){
            var self = this,
                total = this.data.total;
            this.g.append("svg:text")
            .attr("transform", function(d) {
                var c = self.arc.centroid(d),
                x = c[0],
                y = c[1],
                // pythagorean theorem for hypotenuse
                h = Math.sqrt(x*x + y*y);
                console.log("radious: "+ self.radius);
                console.log("innerRadius: "+ self.innerRadius);
                var xVal = self.thickness / 2 + self.innerRadius;
                xVal = (x/h * self.ancLabel) > 0 ? xVal : xVal * -1;
                return "translate(" + xVal +  ',' +
                10 +  ")"; 
            })
            .attr("dy", ".35em")
            .attr("text-anchor", "middle")
            .attr("class", "percentage-label")
            .text(function(d, i) {
                return parseInt(d.value/total * 100) + "%";
            });
        }
    }
    return _donutchart;
})();

function dataAlignCenter(text, prefix,suffix) {
    text.append("tspan").attr("class", "prefix");
    text.append("tspan").attr("class", "center_val");
    text.append("tspan").attr("class", "suffix");
}