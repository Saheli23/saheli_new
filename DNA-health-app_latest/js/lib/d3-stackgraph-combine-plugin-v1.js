/* 
 *  Document   : D3 Single and Gropup Stack Graph Plugin.
 *  Created on : 15 Jul, 2015, 11:17:59 PM
 *  Author     : ujjal sannyal
 *  Version    : 1.1
 */

/*
 * @NOTE: Change Data Format compare to pevious plugin.....
 **/

/*
 *********Demo Setting************
 var stackgraph = new d3stackgraph({
        width: 400,
        height: 400,
        data: data,
        color: "#33ADAD,#66C2C2",                          //"#AFE1CB,#FF8585",
        marginTop: 20,
        marginLeft: 40,
        marginRight: 20,
        marginBottom: 40,
        container: "#barchartDemoStack",
        isLegend: true,
        yaxisLabel:"Amount",
        xaxisAtttribute: "month",
        isTip: true,
        tipFormat: "slice",
        stackOrder: "edit,entry"
    });
 ******* Demo Data ********/

var d3stackgraph = (function(){
    var _d3stackgraph = function(param){
        var defaults = {
                width: 200,
                height: 200,
                data: null,
                color: null,
                colorGenerate: "false",
                startingColor: "B",
                padding: 5,
                marginTop: 20,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                yaxisLabel: "Number",
                stackOrder: null,
                xaxisAtttribute: "month",
                xAxisPosition: "bottom",       //bottom / top,
                yAxisTickAlignment: "left",  //right / left
                tipFormat: "SLICE",       //  whole / slice
                container: "body",
                isTip: "true",
                usePathColor: "false",
                tipColor: "#FFFFFF",
                tipTextColor: "#000000",
                isLegend: "false",
                dataAlignXaxix: "break",
                isComaseparetedVal: true, 
                alignment: "vertical",               //vertical / horizontal
                afterRenderFn: null,
                onClickFn: null,
                isGrid: false,
                rangeBandPadding: "0.1"
        };
        //merging parameter object to default object
        var opts = $.extend({},defaults, param);
        //replace default to "this" obj....
        $.extend(this, opts);

//Backup Options value..        
        this.optsBackUp = $.extend({}, opts);
//Prepare option
        this.prepareOption();
        
        return this;
    };
    _d3stackgraph.prototype = {
        prepareOption: function(){
            try{
                this.width = parseInt(this.width, 10);
                this.height = parseInt(this.height, 10);
                this.padding = parseInt(this.padding, 10);
                this.marginTop = parseInt(this.marginTop, 10);
                this.marginLeft = parseInt(this.marginLeft, 10);
                this.marginRight = parseInt(this.marginRight, 10);
                this.marginBottom = parseInt(this.marginBottom, 10);
                this.colorGenerate = $.type(this.colorGenerate) === "string" ? returnBoolean(this.colorGenerate) : this.colorGenerate;
                this.startingColor = this.startingColor.toString();
                this.yaxisLabel = this.yaxisLabel.toString();
                this.xAxisPosition = this.xAxisPosition.toString();
                this.yAxisTickAlignment = this.yAxisTickAlignment.toString();
                this.isTip = $.type(this.isTip) === "string" ? returnBoolean(this.isTip) : this.isTip;
                this.usePathColor = $.type(this.usePathColor) === "string" ? returnBoolean(this.usePathColor) : this.usePathColor;
                this.isLegend = $.type(this.isLegend) === "string" ? returnBoolean(this.isLegend) : this.isLegend;
                this.tipColor = this.tipColor.toString();
                this.tipTextColor = this.tipTextColor.toString();
                this.dataAlignXaxix = this.dataAlignXaxix.toString();
                this.tipFormat = this.tipFormat.toString();
                this.stackOrder = this.stackOrder !== null ? this.stackOrder.toString().split(",") : [];
                this.xaxisAtttribute= this.xaxisAtttribute.toString();
                this.isComaseparetedVal = $.type(this.isComaseparetedVal) === "string" ? returnBoolean(this.isComaseparetedVal) : this.isComaseparetedVal;
                this.alignment= this.alignment.toString();
                this.afterRenderFn= this.afterRenderFn !== null ? this.afterRenderFn.toString() : null;
                this.onClickFn= this.onClickFn !== null ? this.onClickFn.toString() : null;
                this.isGrid = $.type(this.isGrid) === "string" ? returnBoolean(this.isGrid) : this.isGrid;
                this.rangeBandPadding = this.rangeBandPadding !== null ? this.rangeBandPadding.toString() : null;

                this.init();
            }catch(error){
                console.log("Stack Graph Some Error Found: "+error);
                $.error(error);
            }

        },
        init: function(){
            var self = this;
            this.flag = false;
            
            this.width = this.width - this.marginLeft - this.marginRight,
            this.height = this.height - this.marginTop - this.marginBottom;
            
            if(this.isLegend){
                this.height -= 25;
            }

            if(this.alignment.toLowerCase() == "vertical"){
                this.x0 = d3.scale.ordinal()
                    .rangeRoundBands([0, this.width], parseFloat(this.rangeBandPadding));

                this.x1 = d3.scale.ordinal();

                this.y = d3.scale.linear()
                    .range([this.height, 0]);

                this.xAxis = d3.svg.axis()
                    .scale(this.x0)
                    .orient("bottom");

                this.yAxis = d3.svg.axis()
                    .scale(this.y)
                    .orient("left")
                    .ticks(Math.ceil( this.height / 60));
                    // .tickFormat(d3.format(".2s"));
            }else if(this.alignment.toLowerCase() == "horizontal"){
                
                this.x0 = d3.scale.ordinal()
                    .rangeRoundBands([0, this.height], 0.1);

                this.x1 = d3.scale.ordinal();

                this.y = d3.scale.linear()
                    .range([0, this.width]);

                this.xAxis = d3.svg.axis()
                    .scale(this.x0)
                    .orient("left");

                this.yAxis = d3.svg.axis()
                    .scale(this.y)
                    .orient("bottom")
                    .ticks(Math.ceil( this.width / 60));
                    // .tickFormat(d3.format(".2s"));
            }

            this.render();
        },
        render: function(){
            var self = this;
            this.svg = d3.select(this.container).append("svg")
                .attr("width", this.width + this.marginLeft + this.marginRight)
                .attr("height", this.height + this.marginTop + this.marginBottom)
                .append("g")
                .attr("transform", "translate(" + this.marginLeft + "," + this.marginTop + ")");
           

            this.afterRender();
        },
        afterRender: function(){
            if(this.data !== null && this.data.length > 0){
                var showGraphReturn = this.showGraph();
                if(showGraphReturn){
                    if(this.isTip){
                        this.callTip();
                    }
                    if(this.isLegend){
                        this.attachedLegend();
                    }
                }
            }else{
                this.callNotAvailable();
            }
        },
        prepareData: function(){
            var self = this;
            this.dataRange = [];
            this.columnHeaders = d3.keys(this.data[0]).filter(function(key) {
                return $.type(self.data[0][key]) === "object";
            });
            this.columnHeaders.reverse();
            this.data.forEach(function(d) {
                d.columnDetails = [];
                for(c in self.columnHeaders){
                var yColumn = new Array();
                    // @columnHeadersObj = {"Entry": 5, "Edit": 3}
                    var columnHeadersObj = d[self.columnHeaders[c]];
                    if(self.stackOrder != null){
                        var stackBlockKeys = self.stackOrder;
                    }else{
                        var stackBlockKeys = d3.keys(columnHeadersObj).filter(function(key) {return true;});  // @stackBlockKeys = [Entry, Edit].
                    }
                        self.dataRange[c] = "column"+(c+1);         // ["column01", "column02"]
                        
                    for(k in stackBlockKeys){
                        var obj = {};
                        obj.name = self.columnHeaders[c];
                        obj.stackPeriod = stackBlockKeys[k];                 // period: "name-Entry"
                        obj.val = parseInt(columnHeadersObj[stackBlockKeys[k]], 10);
                        //Calculate Begining value and End Value.
                        if (!yColumn[obj.name]){
                            yColumn[obj.name] = 0;
                        }
                        var yBegin = yColumn[obj.name];
                        yColumn[obj.name] += +obj.val;
                        
                        obj.yBegin = yBegin;
                        obj.yEnd = +obj.val + yBegin;
                        obj.column = self.dataRange[c];
                        //Set Object to columnDetails..
                        d.columnDetails.push(obj);
                    }                   
                }
                d.total = d3.max(d.columnDetails, function(d) { 
                            return d.yEnd; 
                        });
            });
        },
        showGraph: function(){
            var self = this;
            var colorAr = [],
                stackOrderAr = self.stackOrder;
            this.prepareData();
            var maxTotal = d3.max(this.data, function(d) { 
                return d.total; 
            });
            
            if(maxTotal != 0){
                if(this.colorGenerate){
                    var colorPar = {
                            "length": stackOrderAr.length,
                            "colortype": "gen",
                            "startingColor": this.startingColor
                        };
                    this.color = d3.scale.ordinal()
                    .range(getColor(colorPar));
                }else if(this.color !== null){
                    colorAr = this.color.split(",");
                    this.color = d3.scale.ordinal()
                    .range(colorAr);
                }else{
                    var colorPar = {
                            "length": stackOrderAr.length,
                            "colortype": "gen",
                            "startingColor": this.startingColor
                        };
                    this.color = d3.scale.ordinal()
                    .range(getColor(colorPar));
                }
                this.x0.domain(this.data.map(function(d){return d[self.xaxisAtttribute];}));
                this.x1.domain(this.dataRange).rangeRoundBands([0, this.x0.rangeBand()]);

                this.y.domain([0, maxTotal]);
                var groupSpacing = 1;
                if(this.alignment.toLowerCase() == "vertical"){
                    this.text = this.svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(15," + this.height + ")")
                    .call(this.xAxis)
                    .selectAll("text");

                    if(this.dataAlignXaxix == "break"){
                        this.text.call(self.dataAlignXaxixFn, this.x0.rangeBand());
                        if(!self.XaxixAlignCheck(this.text, this.x0.rangeBand())){
                            return;
                        }
                    }else if(this.dataAlignXaxix == "rotate"){
                        this.text.style("text-anchor", "end")
                        .attr("dx", "-.8em")
                        .attr("dy", ".15em")
                        .attr("transform", function(d){
                                return "rotate(-41)";
                            });
                    }

                    this.svg.append("g")
                        .attr("class", "y axis")
                        .call(this.yAxis)
                        .append("text")
                        .attr("transform", "rotate(-90)")
                        .attr("y", 6)
                        .attr("dy", ".7em")
                        .style("text-anchor", "end")
                        .text(this.yaxisLabel);

                    var grid = [];
                    this.svg.select(".y.axis").selectAll(".tick").each(function(data) {
                      var tick = d3.select(this);
                      // pull the transform data out of the tick
                      var transform = d3.transform(tick.attr("transform")).translate;
                      // passed in "data" is the value of the tick, transform[0] holds the X value
                      grid.push({
                        'x1':0,
                        'y1':transform[1],
                        'x2':self.width,
                        'y2':transform[1],
                        'data':data
                      });
                    });
                    var grids = this.svg.append('g')
                          .attr('id','grid')
                          .attr("transform","translate(0,0)")
                          .selectAll('line')
                          .data(grid)
                          .enter()
                          .append('line')
                          .attr({'x1':function(d,i){ return d.x1; },
                                 'y1':function(d,i){ return d.y1; },
                                 'x2':function(d,i){ return d.x2; },
                                 'y2':function(d,i){ return d.y2; },
                            })
                          .style({'stroke':'#adadad','stroke-width':'1px'});

                    this.stackedbar = this.svg.selectAll(".stackedbar")
                        .data(this.data)
                        .enter().append("g")
                        .attr("class", "g")
                        .attr("transform", function(d) {
                            return "translate(" + self.x0(d[self.xaxisAtttribute]) + ",0)";
                        });

                    this.rect = this.stackedbar.selectAll("rect")
                        .data(function(d) {return d.columnDetails;})
                        .enter().append("rect")
                        .attr("width", function(){
                            return ( self.x1.rangeBand() - groupSpacing);
                        })
                        .attr("x", function(d) {
                            return self.x1(d.column) + 15;
                        })
                        .attr("y", function(d) {
                            return self.y(d.yEnd); 
                        })
                        .attr("height", function(d) {
                            return self.y(d.yBegin) - self.y(d.yEnd); 
                        })
                        .style("fill", function(d) {return self.color(d.stackPeriod);});



                }else if(this.alignment.toLowerCase() == "horizontal"){
                    this.tick = this.svg.append("g")
                        .attr("class", "x axis horizontal-axis")
                        .attr("transform", "translate(0, 0)")
                        .call(this.xAxis)
                        .selectAll("g.tick");

                        if(this.yAxisTickAlignment === "right"){
                            this.tick.each(function(d, i){
                                d3.select(this).attr("transform", "translate(-230,"+(self.x0.rangeBand() / 2 + self.x0.range()[i])+")")
                                .select("text")
                                .attr("x", "0")
                                .style("text-anchor", "start");
                            });
                        }

                    if(this.xAxisPosition === "top"){
                        this.svg.append("g")
                            .attr("class", "y axis")
                            .attr("transform", "translate(0,-20)")
                            .call(this.yAxis)
                            .append("text")
                            .attr("x", this.width)
                            .attr("y", "-4")
                            .attr("dx", ".7em")
                            .style("text-anchor", "end")
                            .text(this.yaxisLabel);
                    }else if(this.xAxisPosition === "bottom"){

                        this.svg.append("g")
                            .attr("class", "y axis")
                            .attr("transform", "translate(0," + (this.height + 10) + ")")
                            .call(this.yAxis)
                            .append("text")
                            .attr("x", this.width)
                            .attr("y", "-4")
                            .attr("dx", ".7em")
                            .style("text-anchor", "end")
                            .text(this.yaxisLabel);
                    }

                    var grid = [];
                    this.svg.select(".y.axis").selectAll(".tick").each(function(data) {
                      var tick = d3.select(this);
                      // pull the transform data out of the tick
                      var transform = d3.transform(tick.attr("transform")).translate;
                      // passed in "data" is the value of the tick, transform[0] holds the X value
                      grid.push({
                        'x1':transform[0],
                        'y1':0,
                        'x2':transform[0],
                        'y2':self.height,
                        'data':data
                      });
                    });
                    var grids = this.svg.append('g')
                          .attr('id','grid')
                          .attr("transform","translate(0,0)")
                          .selectAll('line')
                          .data(grid)
                          .enter()
                          .append('line')
                          .attr({'x1':function(d,i){ return d.x1; },
                                 'y1':function(d,i){ return d.y1; },
                                 'x2':function(d,i){ return d.x2; },
                                 'y2':function(d,i){ return d.y2; },
                            })
                          .style({'stroke':'#adadad','stroke-width':'1px'});


                    this.stackedbar = this.svg.selectAll(".stackedbar")
                        .data(this.data)
                        .enter().append("g")
                        .attr("class", "g")
                        .attr("transform", function(d) {
                            return "translate(0, "+ self.x0(d[self.xaxisAtttribute])+")";
                        });

                    this.rect = this.stackedbar.selectAll("rect")
                        .data(function(d) {return d.columnDetails;})
                        .enter().append("rect")
                        .attr("height", function(){
                            return ( self.x1.rangeBand() - groupSpacing);
                        })
                        .attr("y", function(d) {
                            return self.x1(d.column);
                        })
                        .attr("x", function(d) {
                            return self.y(d.yBegin); 
                        })
                        .attr("width", function(d) {
                            return self.y(d.yEnd) - self.y(d.yBegin); 
                        })
                        .style("fill", function(d) {return self.color(d.stackPeriod);});  
                    }

                    if(this.afterRenderFn !== null){
                        if(window.hasOwnProperty(this.afterRenderFn)){
                            window[this.afterRenderFn](this.data, self.container, this.color);
                        }
                    }

                    if(this.onClickFn !== null){
                        this.stackedbar.on("click", function(d){
                            if(window.hasOwnProperty(self.onClickFn)){
                                window[self.onClickFn](d, self.container, self.color);
                            }
                        });
                    }
            }else{
                this.callNotAvailable();
            }
            return true;
        },
        callTip: function(){
/* 
 * Append a div to the body for tip use............. 
 */
            var self = this;
            this.tip = "";
            if($(".toolTip").length === 0){
                this.tip = d3.select("body").append("div").attr("class", "toolTip").style("background", "#fff");
            }else{
                this.tip = d3.select(".toolTip");
            }         
                this.stackedbar.on("mousemove", function(da){
                    self.stackedbar.selectAll("rect").on("mousemove", function(d){                        
                        if(self.tipFormat.toUpperCase() == "WHOLE"){
                            var dataRangeVal = d.column;
                            var stackbarDetail = $.grep(da.columnDetails, function(m){
                                return dataRangeVal == m.column;
                            });
                            var html = "",
                                total_bal = 0;
                            for(var i = 0; i<stackbarDetail.length; i++){
                                if(self.isComaseparetedVal){
                                    html += stackbarDetail[i].name+"-"+stackbarDetail[i].stackPeriod+" : <span><strong> "+
                                        addCommaSeparator(parseInt(stackbarDetail[i].val, 10), "l", ",", "n")
                                        +"</strong></span></br>";
                                }else{
                                    html += stackbarDetail[i].name+"-"+stackbarDetail[i].stackPeriod+" : <span><strong> "+stackbarDetail[i].val+"</strong></span></br>";
                                }
                                total_bal += parseInt(stackbarDetail[i].val, 10);
                            }
                            if(self.isComaseparetedVal){
                                html += "Total : <span><strong> "+addCommaSeparator(total_bal, "l", ",", "n")+"</strong></span></br>";
                            }else{
                                html += "Total : <span><strong> "+total_bal+"</strong></span></br>";
                            }
                        }else if(self.tipFormat.toUpperCase() == "SLICE"){
                            var value = self.isComaseparetedVal ? addCommaSeparator(d.val, "l", ",", "n") : d.val;
                            // var html = d.name+"-"+d.stackPeriod+" : <span><strong> "+value+"</strong></span></br>";
                            // new style
                            var html = "<span class='tooltip-value'><strong> "+value+"</strong></span></br><span>"+d.stackPeriod+"</span>";
                        }
                        self.tip.style("display", "inline-block");
                        self.tip.style("white-space", "nowrap");
                        self.tip.style("color", self.tipTextColor);
                        self.tip.style("z-index", "99999999");
                        self.tip.html(html);
                        
                        var totalWidth = d3.event.pageX + $(".toolTip").outerWidth() / 2,
                            actWidth = 0;
                        var windWdth = $(window).width(),
                            tipHeight = parseFloat($(".toolTip").height())+35;
                        if(windWdth < totalWidth){
                            actWidth = windWdth - $(".toolTip").outerWidth() - 15;
                            self.tip.style("left", actWidth+"px");
                        }else{
                            self.tip.style("left", (d3.event.pageX - $(".toolTip").outerWidth() / 2)+"px");
                        }
                        var topY = (parseFloat(d3.event.pageY-tipHeight) < 10) ? parseFloat(d3.event.pageY + tipHeight) : parseFloat(d3.event.pageY-tipHeight);
                        self.tip.style("top", topY+"px");
                        
                        if(self.usePathColor){
                            self.tip.style("background", self.color(d.stackPeriod));
                        }else{
                            self.tip.style("background", self.tipColor);
                        }
                    });
                })
                .on("mouseout", function(d){
                    self.tip.style("display", "none");
                });            
        },
        attachedLegend: function(){
            var arr = [],
                self = this;
            this.data[0].columnDetails.forEach(function(d){
                if(d.column === self.dataRange[0]){
                    arr.push(d.stackPeriod);
                }
            });
            
            var svg2 = d3.select(this.container)
                        .append("svg")
                        .attr("width", this.width + this.marginLeft + this.marginRight)
                        .attr("height", 25);
            var svg2_g = svg2.append("g")
                        .attr("transform", "translate(" + 40 + "," + 6 + ")");
                           
            var legend = svg2_g.selectAll(".legend")
                            .data(arr.slice())
                            .enter().append("g")
                            .attr("class", "legend")
                            .attr("transform", function(d, i) {
                                return "translate("+ i * 100 +",0)";
                            });

            legend.append("rect")
                .attr("x", 0)
                .attr("y", 0)
                .attr("width", 14)
                .attr("height", 14)
                .style("fill", this.color);

            legend.append("text")
                .attr("x",20)
                .attr("y", 9)
                .attr("dy", ".35em")
                .text(function(d) {
                    return d;
                });
        },
        callNotAvailable: function(){
            this.svg.append("g")
                .attr("transform", "translate(" + ((this.width / 2) - this.marginLeft) + "," + ((this.height / 2) - this.marginTop) + ")")
                .append("text")
                .attr("class", "na-class")
                .attr("x",25)
                .attr("y", 9)
                .attr("dy", ".35em")
                .text("N / A");
        },
        XaxixAlignCheck: function(text, width){
            var self = this,
                XaxixAlignCheckFlag = true,
                htAr = [];
            text.each(function() {
                var text = d3.select(this);
                if(text.node().getComputedTextLength() > width){
                    htAr.push(text.node().getComputedTextLength());
                    XaxixAlignCheckFlag = false;
                }
            });
            // if(!XaxixAlignCheckFlag){
            //     var ht = d3.max(htAr);
            //     self.optsBackUp.dataAlignXaxix = "rotate";
            //     self.optsBackUp.marginBottom = (ht + 6);
            //     self.reRender();
            //     return false;
            // }else{
                return true;
            // }
        },
        reRender: function(){
            $(this.container).html("");
            $.extend(this, this.optsBackUp);
            this.prepareOption();
        },        
/*************************************************Breaking x axis text****************************************************************/
        dataAlignXaxixFn: function(text, width){
            text.each(function() {
                var text = d3.select(this),
                    words = text.text().split(/\s+/).reverse(),
                    currentText = text.text(),
                    word,
                    line = [],
                    lineNumber = 0,
                    lineHeight = 1.1, // ems
                    y = text.attr("y"),
                    dy = parseFloat(text.attr("dy")),
                    tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
                while (word = words.pop()) {
                    line.push(word);
                    tspan.text(line.join(" "));
                    if (tspan.node().getComputedTextLength() > width) {
                        line.pop();
                        tspan.text(line.join(" "));
                        line = [word];
                        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
                    }
                }
            });
        }
    };
    return _d3stackgraph;
})();

