function prepareAssignmentSummary(){
	// Harp  Non-Harp
	var assignmentSummary = [
		{
			XAxis1: "MEDICAID",
			XAxis2: null,
			YAxis1: "157",			//Non-Harp  #4fd4d9
			YAxix2: "2685", 		//Harp  #6ee8ed
			YAxis3: ""
		},
		{
			XAxis1: "AFFINITY HEALTH PLAN INC",
			XAxis2: null,
			YAxis1: "193",
			YAxix2: "443",
			YAxis3: ""
		},
		{
			XAxis1: "AMERIGROUP NEW YORK LLC",
			XAxis2: null,
			YAxis1: "22",
			YAxix2: "419",
			YAxis3: ""
		},
		{
			XAxis1: "AMIDA CARE INC",
			XAxis2: null,
			YAxis1: "53",
			YAxix2: "193",
			YAxis3: ""
		},
		{
			XAxis1: "HEALTH FIRST PHSP INC",
			XAxis2: null,
			YAxis1: "648",
			YAxix2: "2125",
			YAxis3: ""
		},
		{
			XAxis1: "HEALTH INSURENCE PLAN OF GTR NY",
			XAxis2: null,
			YAxis1: "74",
			YAxix2: "144",
			YAxis3: ""
		},
		{
			XAxis1: "METEROPLUS HEALTH PLAN INC",
			XAxis2: null,
			YAxis1: "158",
			YAxix2: "248",
			YAxis3: ""
		},
		{
			XAxis1: "METROPLUS PARTNERSHIP CARE SN",
			XAxis2: null,
			YAxis1: "13",
			YAxix2: "18",
			YAxis3: ""
		},
		{
			XAxis1: "HEALTH FIRST PHSP INC",
			XAxis2: null,
			YAxis1: "648",
			YAxix2: "2125",
			YAxis3: ""
		},
		{
			XAxis1: "NEW YORK STATE CATHOLIC HEALTH PLAN",
			XAxis2: null,
			YAxis1: "1",
			YAxix2: "0",
			YAxis3: ""
		},
		{
			XAxis1: "NYS CATHOLIC HEALTH PLAN INC",
			XAxis2: null,
			YAxis1: "189",
			YAxix2: "649",
			YAxis3: ""
		},
		{
			XAxis1: "UNITED HEALTH CARE OF NY INC",
			XAxis2: null,
			YAxis1: "10",
			YAxix2: "76",
			YAxis3: ""
		},
		{
			XAxis1: "VNS CHOICE SELETE HEALTHSNP",
			XAxis2: null,
			YAxis1: "5",
			YAxix2: "15",
			YAxis3: ""
		},
		{
			XAxis1: "WELLCARE OF NEW YORK INC",
			XAxis2: null,
			YAxis1: "52",
			YAxix2: "183",
			YAxis3: ""
		}
	]

	var assignmentSummaryData = [];
	for(var i=0; i<assignmentSummary.length; i++){
		var obj = {},
			curObj = assignmentSummary[i];
		obj.xaxis = curObj.XAxis1;
		obj['assignment'] = {};
		obj['assignment']['Non-Harp'] = curObj.YAxis1;
		obj['assignment']['Harp'] = curObj.YAxix2;
		// console.log("curObj: ", curObj)
		// console.log("obj: ", obj)
		assignmentSummaryData.push(obj);
	}
	assignmentSummaryDataForTable=assignmentSummaryData;

	var wt = $("#assignmentSummaryGraph").width(),
		ht = $("#assignmentSummaryGraph").height();
	var stackgraph = new d3stackgraph({
        width: wt,
        height: ht,
        data: assignmentSummaryData,
        color: "#6ee8ed,#4fd4d9",                         
        marginTop: 40,
        marginLeft: 240,
        marginRight: 10,
        marginBottom: 20,
        container: "#assignmentSummaryGraph",
        isLegend: false,
        yaxisLabel:"",
        xaxisAtttribute: "xaxis",
        isTip: true,
        tipFormat: "slice",
        stackOrder: "Harp,Non-Harp",
        alignment: "horizontal",
        isComaseparetedVal: false,
        xAxisPosition: "top",
        yAxisTickAlignment: "right",
        isGrid: true,
        tipTextColor: "#5e6578"
    });
}
function prepareEnrollmentSummary(){
	var enrollSummary = [
		{
			MMSSID: "03009074",
			AgencyName: "UPTOWN HEALTHCARE MANAGEMENT INC",
			DisEnrolled: "3390",
			Enrolled: "1287",
			OutReach: "0",
			GrandTotal: "4677"
		},
		{
			MMSSID: "00476022",
			AgencyName: "BRONX LEBANON HOSPITAL CENTER",
			DisEnrolled: "2104",
			Enrolled: "663",
			OutReach: "814",
			GrandTotal: "3581"
		},
		{
			MMSSID: "01222191",
			AgencyName: "NARCO FREEDOM CN AI",
			DisEnrolled: "2028",
			Enrolled: "1446",
			OutReach: "0",
			GrandTotal: "3474"
		},
		{
			MMSSID: "01136130",
			AgencyName: "DOMINICAN SISTERS FAMILY LTHH",
			DisEnrolled: "1466",
			Enrolled: "354",
			OutReach: "2",
			GrandTotal: "1822"
		},
		{
			MMSSID: "01315695",
			AgencyName: "BRONEXWORK INC AI",
			DisEnrolled: "1107",
			Enrolled: "275",
			OutReach: "0",
			GrandTotal: "1382"
		}
	];


	var enrollmentSummaryData = [];
	for(var i=0; i<enrollSummary.length; i++){
		var obj = {},
			curObj = enrollSummary[i];
		obj.AgencyName = curObj.AgencyName;
		obj['enrollment'] = {};
		obj['enrollment']['DisEnrolled'] = curObj.DisEnrolled;
		obj['enrollment']['Enrolled'] = curObj.Enrolled;
		obj['enrollment']['OutReach'] = curObj.OutReach;
		// console.log("curObj: ", curObj)
		// console.log("obj: ", obj)
		enrollmentSummaryData.push(obj);
	}
	enrollmentSummaryDataForTable=enrollmentSummaryData;
	var wt = $("#enrollSummeryGraph").width(),
		ht = $("#enrollSummeryGraph").height();
	var stackgraph = new d3stackgraph({
        width: wt,
        height: ht,
        data: enrollmentSummaryData,
        color: "#4b8ccc,#4fd4d9,#e85366",                         
        marginTop: 10,
        marginLeft: 40,
        marginRight: 10,
        marginBottom: 40,
        container: "#enrollSummeryGraph",
        isLegend: false,
        yaxisLabel:"",
        xaxisAtttribute: "AgencyName",
        isTip: true,
        tipFormat: "slice",
        stackOrder: "OutReach,DisEnrolled,Enrolled",
        isComaseparetedVal: false,
        tipTextColor: "#5e6578",
        dataAlignXaxix: "break",
        rangeBandPadding: "0.6",
        isGrid: true,
        onClickFn: "clickonbar"
    });
	
}

function clickonbar(d, container, color){
console.log(d);
//console.log(d.total);
total=d.total;

OutReach=d.columnDetails[0].val;
DisEnrolled=d.columnDetails[1].val;
Enrolled=d.columnDetails[2].val;

calculatePercentage(total,OutReach,DisEnrolled,Enrolled);

console.log(Enrolled);

}



 function calculatePercentage(total,Outreach,Disenrolled,Enrolled){
     //set default value
     
      
      if (typeof(total)==='undefined') total =107660;
      if (typeof(Outreach)==='undefined') Outreach =84975;
      if (typeof(Disenrolled)==='undefined') Disenrolled =0;
       if (typeof(Enrolled)==='undefined') Enrolled =22685;

         var   outreachpercentage=(Outreach/total)*100;

           
            $("#outreach").text(Math.round(outreachpercentage)+'%');

            var Disenrolledpercentage=(Disenrolled/total)*100;
       $("#disenrolled").text(Math.round(Disenrolledpercentage)+'%');

         var Enrolledpercentage=(Enrolled/total)*100;
       $("#enrolled").text(Math.round(Enrolledpercentage)+'%');
    }

function enrollSummeryGenerateTable() {
  // console.log(enrollmentSummaryDataForTable);
       var table='';

  table ='<div class="table-responsive" ><table class="table table-bordered table-striped table-hover">'+
             '<th>Name</th><th>Enrolled</th><th>Outreached</th><th>Disenrolled</th></table><div class="enroll-wraper"><table class="table table-bordered table-striped table-hover"><tbody >';

    for(var i=0; i<enrollmentSummaryDataForTable.length; i++){
    	table +='<tr><td>'+enrollmentSummaryDataForTable[i].AgencyName+'</td><td>'+enrollmentSummaryDataForTable[i].columnDetails[0].val+'</td>'+
          '<td>'+enrollmentSummaryDataForTable[i].columnDetails[1].val+'</td>'+
          '<td>'+enrollmentSummaryDataForTable[i].columnDetails[2].val+'</td></tr>';
    }
	table +='</tbody></table></div></div>';
	$('#enrollSummeryTable').html(table);
  
}


function assignmentGenerateTable() {
   console.log(assignmentSummaryDataForTable);
       var table='';

    table ='<div class="table-responsive"><table class="table table-bordered table-striped table-hover">'+
             '<th>Name</th><th>Harp</th><th>Non-Harp</th><tbody>';

        for(var i=0; i<assignmentSummaryDataForTable.length; i++){ 

        table +='<tr><td>'+assignmentSummaryDataForTable[i].xaxis+'</td><td>'+assignmentSummaryDataForTable[i].columnDetails[0].val+'</td>'+
              '<td>'+assignmentSummaryDataForTable[i].columnDetails[1].val+'</td></tr>';
              
            }
          table +='</tbody></table></div>';
          // alert(table);
          $('#assignmentSummaryTable').html(table);
  
}
function donutChartHarpNonHarp(){

	var HarpNonHarpData = [
		{
			XAxis1: "Enrolled",
			XAxis2: null,
			YAxis1: "1524",
			YAxis2: "5113",
			YAxis3: null
		},
		{
			XAxis1: "OutReach",
			XAxis2: null,
			YAxis1: "375",
			YAxis2: "456",
			YAxis3: null
		}
	];
	var enrolledDonut = [],
		outreachDonut = [];
		for(i=0; i<HarpNonHarpData.length; i++){
			var obj = HarpNonHarpData[i];
			if(obj.XAxis1 === "Enrolled"){
				enrolledDonut = [{
					"Harp": obj.YAxis1
				},{
					"Non-Harp": obj.YAxis2
				}];
			}else{
				outreachDonut = [{
					"Harp": obj.YAxis1
				},{
					"Non-Harp": obj.YAxis2
				}]
			}
		}
		var wt = $("#enrolledDonutChart").width(),
			ht = $("#enrolledDonutChart").height();
	var enrolledDonutChart = new donutchart({
		width: wt,
        height: ht,
        data: enrolledDonut,
        color: "#FFFFFF,#25517d",
        container: "#enrolledDonutChart",
        showTotalCount: false,
        chartStyle: "half",
        innerRadius: 35,
        tipTextColor: "#5e6578",
        outerPadding: 10,
        isTip: true,
        dataPercent: true
	});
	var outreachDonutChart = new donutchart({
		width: wt,
        height: ht,
        data: outreachDonut,
        color: "#FFFFFF,#932634",
        container: "#outeachDonutChart",
        showTotalCount: false,
        tipTextColor: "#5e6578",
        chartStyle: "half",
        innerRadius: 35,
        outerPadding: 10,
        isTip: true,
        dataPercent: true
	});
}

function monthlyEnrollmentSummary(){
	var mnthEnSum = [
		{
			XAxis1: "2016-Jan-01",
			XAxis2: null,
			YAxis1: "6055",
			YAxis2: "15977",
			YAxis3: "4560"
		},
		{
			XAxis1: "2016-Feb-01",
			XAxis2: null,
			YAxis1: "6522",
			YAxis2: "1677",
			YAxis3: "6321"
		},
		{
			XAxis1: "2016-Mar-01",
			XAxis2: null,
			YAxis1: "6637",
			YAxis2: "16757",
			YAxis3: "12501"
		},
		{
			XAxis1: "2016-Apr-01",
			XAxis2: null,
			YAxis1: "5321",
			YAxis2: "16757",
			YAxis3: "8506"
		},
		{
			XAxis1: "2016-May-01",
			XAxis2: null,
			YAxis1: "1625",
			YAxis2: "16757",
			YAxis3: null
		}
	];
	var mnthEnSumData = [];

	for(var i=0; i<mnthEnSum.length; i++){
		var obj = {
			Month: mnthEnSum[i].XAxis1,
			OutReach: mnthEnSum[i].YAxis1 === null ? 0 : parseInt(mnthEnSum[i].YAxis1),
			Disenrolled: mnthEnSum[i].YAxis2 === null ? 0 : parseInt(mnthEnSum[i].YAxis2),
			Enrolled: mnthEnSum[i].YAxis3 === null ? 0 : parseInt(mnthEnSum[i].YAxis3)
		};

		mnthEnSumData.push(obj);

	}
	var wt = $("#monthlyEnrollmentContainer").width(),
		ht = $("#monthlyEnrollmentContainer").height();
	var d3Areachart = new d3areachart({
		width: wt,
        height: ht,
        container: "#monthlyEnrollmentContainer",
        data: mnthEnSumData,
        xAxisField: "Month",
        color: "#e85366,#4fd4d9,#4b8ccc",
        marginLeft: 40,
        marginRight: 10,
        marginBottom: 40,
        yaxisLabel: "",
        tipTextColor: "#5e6578",
        isTip: true,
        interpolate: "basis"   // basis / linear
	});

}

function compositeAcuityScore(){
	var firstData = [
		{
			XAxis1: "0-10",
			XAxis2: null,
			YAxis1: "2481",
			YAxis2: "1528",
			YAxis3: "null"
		},
		{
			XAxis1: "11-20",
			XAxis2: null,
			YAxis1: "707",
			YAxis2: "376",
			YAxis3: null
		},
		{
			XAxis1: "21-30",
			XAxis2: null,
			YAxis1: "14",
			YAxis2: "16",
			YAxis3: null
		}
	],
	secondData = [
		{
			XAxis1: "0-10",
			XAxis2: null,
			YAxis1: "1121",
			YAxis2: "929",
			YAxis3: "null"
		},
		{
			XAxis1: "11-20",
			XAxis2: null,
			YAxis1: "534",
			YAxis2: "546",
			YAxis3: null
		},
		{
			XAxis1: "21-30",
			XAxis2: null,
			YAxis1: "64",
			YAxis2: "87",
			YAxis3: null
		}
	];
	var firstDataList = [];
	for(var i=0; i<firstData.length; i++){
		var obj = {
			xAxis: firstData[i].XAxis1,
			men: firstData[i].YAxis1 === null ? 0 : parseInt(firstData[i].YAxis1),
			women: firstData[i].YAxis2 === null ? 0 : parseInt(firstData[i].YAxis2)
		};
		firstDataList.push(obj);
	}

	var secondDataList = [];
	for(var i=0; i<secondData.length; i++){
		var obj = {
			xAxis: secondData[i].XAxis1,
			men: secondData[i].YAxis1 === null ? 0 : parseInt(secondData[i].YAxis1),
			women: secondData[i].YAxis2 === null ? 0 : parseInt(secondData[i].YAxis2)
		};
		secondDataList.push(obj);
	}

	var wt = $("#compositeAcuityFirst").width(),
		ht = $("#compositeAcuityFirst").height() - 5;

	var d3lineordinalchart = new d3lineOrdinalChart({
		width: wt,
		height: ht,
        container: "#compositeAcuityFirst",
        data: firstDataList,
        xAxisField: "xAxis",
        color: "#e85366,#4b8ccc",
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 20,
        yaxisLabel: ""
        // isTip: true
	});

	var d3lineordinalchart = new d3lineOrdinalChart({
		width: wt,
		height: ht,
        container: "#compositeAcuitySecond",
        data: secondDataList,
        xAxisField: "xAxis",
        color: "#e85366,#4b8ccc",
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 20,
        yaxisLabel: ""
	});

}

$(function(){
	// prepareAssignmentSummary();
	// prepareEnrollmentSummary();
	// donutChartHarpNonHarp();
	// monthlyEnrollmentSummary();
	// compositeAcuityScore();
});


function returnBoolean(param){
    if(param.toString() === "true" || param.toString() === "1"){
        return true;
    }else if(param.toString() === "false" || param.toString() === "0"){
        return false;
    }
}

