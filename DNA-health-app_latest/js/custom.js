function collapsLeftPannel(){
	$('.r-panel').removeClass('expand-right-panel').addClass('collaps-right-panel');
	$('.l-panel').removeClass('expand-left-panel').addClass('collaps-left-panel');
	$('.l-nav').removeClass('left-nav-area').addClass('left-mobile-nav-area');	
	$('.logo-background a img').attr('src', 'images/logo-s.jpg');
}
function expandLeftPannel(){
	$('.r-panel').removeClass('collaps-right-panel').addClass('expand-right-panel');
	$('.l-panel').removeClass('collaps-left-panel').addClass('expand-left-panel');
	$('.l-nav').removeClass('left-mobile-nav-area').addClass('left-nav-area');	
	$('.logo-background a img').attr('src', 'images/logo-l.jpg');
}

// Collaps left pannel on window size below 991px
function collapsExpand(){
	if(window.innerWidth <= 991){
    	collapsLeftPannel();
    }else{
    	expandLeftPannel();
    }
	setTimeout(function(){
		
		$("#assignmentSummaryGraph").html("");
		$("#enrollSummeryGraph").html("");
		$("#enrolledDonutChart").html("");
		$("#outeachDonutChart").html("");
		$("#monthlyEnrollmentContainer").html("");
		$("#compositeAcuityFirst").html("");
		$("#compositeAcuitySecond").html("");

	    prepareAssignmentSummary();
		prepareEnrollmentSummary();
		donutChartHarpNonHarp();
		monthlyEnrollmentSummary();
		compositeAcuityScore();
	}, 1200);
}

//Hide search text box on window size less than 991
function showHideSearch(){
	if(window.innerWidth <= 991){
		$('#head_search_txt').hide();
	}else{
		$('#head_search_txt').show();
	}
}

function documentClick(){
	element: $('.topright-toggle-section .trhttoggle').next('.toprht-togglenav').fadeOut(200);
	if(window.innerWidth <= 991){
		$('#head_search_txt').hide();
	    $('#head_left_buttons').show();
	}
}

$(document).ready(function () {
	// Check window size for left pannel position
	collapsExpand();
	// Check window size for search box position
	showHideSearch();

	// For toggle left menu
	$('.left-nav-area ul li').on('click',function() {
		var __self = $(this);
		var __selfChildern = __self.children('ul');

		if(__selfChildern.is(':visible')){
			__selfChildern.length && __selfChildern.hide();
			__self.children('a').removeClass('active');
		}else{
			__selfChildern.length && __selfChildern.show();
			__self.children('a').addClass('active');
		}

		//Hide all other subitem
		$('.left-nav-area ul li').not(__self).each(function(){
			$(this).children('ul').hide();
			$(this).children('a').removeClass('active');
		});
		
	});
	// For top right toggle menu
	$('.topright-toggle-section .trhttoggle').on('click',function(event) {
		event.stopPropagation();
		var __self = $(this);
		var visibleSection = __self.next('.toprht-togglenav');
		if(visibleSection.is(':visible'))
			__self.next('.toprht-togglenav').fadeOut(200);
		else
			__self.next('.toprht-togglenav').fadeIn(200);
    });

	$('.topright-toggle-section .trhttoggle').next('.toprht-togglenav').on('click',function(event) {
		event.stopPropagation();
	});

	// For left panel menu
	$('.left-panel-button').on('click',function() {
		if($('.r-panel').hasClass('expand-right-panel')){
			collapsLeftPannel();
		}else{
			expandLeftPannel();
		}
    });

    $('#head_search_icon').on('click', function(){
    	event.stopPropagation();
    	if(window.innerWidth <= 991){
    		$('#head_search_txt').show();
    		$('#head_left_buttons').hide();
    	}
    });

    $('#head_search_txt').on('click focus', function(event){
    	event.stopPropagation();
    });

    $(window).resize(function(){
        collapsExpand();
        showHideSearch();
    });

    $(document).on('click', function(){
    	console.log('called');
    	documentClick();
    });
});