/**
 * Created by Jeepeng on 2016/11/20.
 */

import React, { Component, PropTypes } from 'react'
import _ from 'lodash'
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity
} from 'react-native'

const DEFAULT_HEIGHT = 500;
const DEFAULT_COLUMN_WIDTH = 60;

class Table extends Component {

  static propTypes = {
    columns: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string.isRequired,
      dataIndex: PropTypes.string.isRequired,
      width: PropTypes.number
    })).isRequired,
    columnWidth: PropTypes.number,
    height: PropTypes.number,
    dataSource: React.PropTypes.array.isRequired,
    renderCell: React.PropTypes.func,
  };

  static defaultProps = {
    columns: [],
    dataSource: [],
    columnWidth: DEFAULT_COLUMN_WIDTH,
    height: DEFAULT_HEIGHT,
    renderCell: undefined
  };

  constructor(props){
    super(props);
    this.state = {
      dataSource: this.props.dataSource,
      order: 'asc',
      orderby: ''
    }
  }

  _renderCell(cellData, col) {
    let style = {width: col.width || this.props.columnWidth || DEFAULT_COLUMN_WIDTH};
    return (
      <View key={col.dataIndex} style={[styles.cell, style]}>
        <Text numberOfLines={1} style={{fontFamily:'Roboto',fontSize:16,}}>{cellData}</Text>
      </View>
    )
  }

  _sort(orderBy){
    let {dataSource, order, orderby} = this.state;
    dataSource = _.orderBy(dataSource, [orderBy], [order]);
    if(orderby === orderBy){
      if(order === 'asc'){
        order = 'desc';
      }else{
        order = 'asc';
      }
    }else{
      order = 'asc';
    }
    this.setState({dataSource, order, orderby: orderBy });
  }

  _renderHeader() {
    let { columns, columnWidth } = this.props;
    return columns.map((col, index) => {
      let style = {width: col.width || columnWidth || DEFAULT_COLUMN_WIDTH};
      return (
        <View key={index} style={[styles.headerItem, style]}>
        {col.sortable ? (
          <TouchableOpacity onPress={this._sort.bind(this, col.dataIndex)}><Text style ={styles.headerText}>{col.title}</Text></TouchableOpacity>
        ) : (
          <Text style ={styles.headerText}>{col.title}</Text>
        )}

        </View>
      )
    })
  }

  _renderRow(rowData, index) {
    let { columns, renderCell } = this.props;
    if(!renderCell) {
      renderCell = this._renderCell.bind(this, );
    }
    return (
      <View key={index} style={[styles.row,{ backgroundColor: (index % 2 == 0) ? '#ecf0f1' : '#fff'}]}>
        {
          columns.map(col => renderCell(rowData[col.dataIndex], col))
        }
      </View>
    );
  }

  render() {
    let { dataSource } = this.state;
    let { height } = this.props;
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={[styles.contentContainer , { height }]}
        horizontal={true}
        bounces={false} >
        <View>
          <View style={styles.header}>
            { this._renderHeader() }
          </View>
          <ScrollView
            style={styles.dataView}
            contentContainerStyle={styles.dataViewContent} >
            { dataSource.map((rowData, index) => this._renderRow(rowData, index)) }
          </ScrollView>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
  },
  contentContainer: {
    height: 240
  },
  header: {
    flexDirection: 'row',
  },
  headerItem: {
    minHeight: 60,
    width: DEFAULT_COLUMN_WIDTH,
    backgroundColor: '#3D3B3C',
    // borderRightWidth: 1,
    // borderRightColor: '#dfdfdf',
    alignItems: 'center',
    justifyContent: 'center',

  },
  headerText:{
    color:'white',
    fontFamily:'Roboto',
    fontSize:18,
  },
  dataView: {
    flexGrow: 1,
  },
  dataViewContent: {
  },
  row: {
    flexDirection: 'row',
    backgroundColor: '#F7F7F7',
    borderBottomWidth: 1,
    borderBottomColor: '#dfdfdf',
  },
  cell: {
    minHeight: 60,
    width: DEFAULT_COLUMN_WIDTH,
    backgroundColor: 'transparent',
    // borderRightWidth: 1,
    // borderRightColor: '#dfdfdf',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default Table
