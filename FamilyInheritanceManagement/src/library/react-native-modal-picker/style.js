'use strict';

import { StyleSheet, Dimensions } from 'react-native';

const {height, width} = Dimensions.get('window');

const PADDING = 8;
const BORDER_RADIUS = 5;
//const FONT_SIZE = (width/56.88);
const FONT_SIZE = (width/56.88) > 18 ? 18: 18 ;

// const HIGHLIGHT_COLOR = 'rgba(0,118,255,0.9)';
const HIGHLIGHT_COLOR = '#4d4d4d';
const OPTION_CONTAINER_HEIGHT = 400;

export default StyleSheet.create({

    overlayStyle: {
        width: width,
        height: height,
        backgroundColor: 'rgba(0,0,0,0.7)'
    },

    optionContainer: {
        borderRadius:BORDER_RADIUS,
        width:width*0.5,
        height:OPTION_CONTAINER_HEIGHT,
        backgroundColor:'rgba(255,255,255,0.8)',
        left:width*0.25,
        top:(height-OPTION_CONTAINER_HEIGHT)/2
    },

    cancelContainer: {
      width:width*0.5,
      left:width*0.25,
      top:(height-OPTION_CONTAINER_HEIGHT)/2 + 10
    },
    /* COMMENTED FOR DESIGN ISSUE IN SELECT BOX
     selectStyle: {
         flex: 1,
         borderColor: '#ccc',
         borderWidth: 1,
         padding: 8,
         borderRadius: BORDER_RADIUS
     },*/
    selectStyle: {
        backgroundColor:'white',
        borderColor: '#ddd',
        borderWidth: 1.5,
        padding: 8,
        borderRadius: 3
    },

    selectTextStyle: {
        //textAlign: 'center',
        color: '#4d4d4d',
        fontSize: FONT_SIZE,
        fontFamily:'Roboto',
    },

    cancelStyle: {
        borderRadius: BORDER_RADIUS,
        //width: width * 0.8,
        backgroundColor: 'rgba(255,255,255,0.8)',
        padding: PADDING
    },

    cancelTextStyle: {
        textAlign: 'center',
        color: '#333',
        fontSize: FONT_SIZE,
        fontFamily:'Roboto',
    },

    optionStyle: {
        padding: PADDING,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc'
    },

    optionTextStyle: {
        textAlign: 'center',
        fontSize: FONT_SIZE,
        color: HIGHLIGHT_COLOR,
        fontFamily:'Roboto',
    },

    sectionStyle: {
        padding: PADDING * 2,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc'
    },

    sectionTextStyle: {
        textAlign: 'center',
        fontSize: FONT_SIZE,
        fontFamily:'Roboto',
    }
});
