import {StyleSheet,Dimensions } from 'react-native';
var {height, width} = Dimensions.get('window');

let style = StyleSheet.create({
  dateTouch: {
    width: 142
  },
  dateTouchBody: {
    flexDirection: 'row',
    height: 55,
    alignItems: 'center',
    justifyContent: 'center'
  },
  dateIcon: {
    width: 22,
    height: 20,
    marginLeft: 5,
    marginRight: 5,
    bottom: 4
  },
  dateInput: {
    flex: 1,
    height: 40,
    borderWidth: 1,
    borderColor: '#aaa',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',


  },
  dateText: {
    color: '#333',
    fontSize: (width/56.88),
    fontFamily:'Roboto',
  },
  placeholderText: {
    color: '#595959',
    fontSize: (width/56.88),
    fontFamily:'Roboto',
  },
  datePickerMask: {
    flex: 1,
    alignItems: 'flex-end',
    flexDirection: 'row',
    backgroundColor: '#00000077'
  },
  datePickerCon: {
    backgroundColor: '#fff',
    height: 0,
    overflow: 'hidden'
  },
  btnText: {
    position: 'absolute',
    top: 0,
    height: 42,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  btnTextText: {
    fontSize: 20,
    //color: '#46cf98',
    color: 'green',
    fontFamily:'Roboto',
    fontWeight:'600'
  },
  btnTextCancel: {
    color: '#666',
    fontWeight:'600'
  },
  btnCancel: {
    left: 0
  },
  btnConfirm: {
    right: 0,

  },
  datePicker: {
    marginTop: 42,
    borderTopColor: '#ccc',
    borderTopWidth: 1
  },
  disabled: {
    backgroundColor: '#eee'
  }
});

export default style;
