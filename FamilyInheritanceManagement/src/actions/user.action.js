import React from 'react';
import {
  AsyncStorage
} from 'react-native';
import SQLite from 'react-native-sqlite-storage';
SQLite.DEBUG(true);

function userDataActionSuccess(res){
  console.log('called userDataActionSuccess ----');
  return {
    type: 'GETUSERINFO_SUCCESS',
    res: res
  };
}

function userDataActionError(res){
  console.log('called userDataActionError ----');
  return {
    type: 'GETUSERINFO_ERROR',
    res: res
  };
}

export function getUserInfo(userid){

  return function (dispatch) {

    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);
    function errorCB(err) {
      console.log("SQL Error: " + err);
    };

    function successTable(){
        //console.log("Table found");
    //  console.log("SQL Error: " + Object.values(err));
    }

    function successCB() {
      console.log("SQL executed fine");
    };

    function openCB() {
      console.log("Database OPENED");
    };

    function successInsert() {
      console.log("SQL inserted fine");
      //this.props.navigation.dispatch({ type: 'RegistrationSuccess', })}
    };


      db.transaction((tx) => {


                      tx.executeSql('SELECT * FROM users WHERE id ="' + userid + '"', [], (tx, results) => {
                    //  console.log("Query completed");
                      let len = results.rows.length;
                      let userName;
                      let userObj = {
                        userid:'',
                        username:'',
                      }
                      if (len > 0){
                        console.log("found");

                        for (let i = 0; i < len; i++) {
                          let row = results.rows.item(i);

                          userName              = row.fullname;
                          userObj.userid        = row.id;
                          userObj.firstname     = row.fullname;
                          userObj.middlename    = row.middlename;
                          userObj.lastname      = row.lastname;
                          userObj.email         = row.email;
                          userObj.address       = row.address;
                          userObj.city          = row.city;
                          userObj.state         = row.state;
                          userObj.zip           = row.zip;
                          userObj.phone         = row.phone;
                          userObj.is_stepcompleted = row.reg_step_completed;

                        }
                        console.log(userObj);
                          dispatch(userDataActionSuccess({userDetails:userObj}));
                    }
             });

    });
  }
}
