import React from 'react';
import {
  AsyncStorage
} from 'react-native';
import SQLite from 'react-native-sqlite-storage';
SQLite.DEBUG(true);
import RNFetchBlob from 'react-native-fetch-blob';
const dirs = RNFetchBlob.fs.dirs;
 const DEST_PATH =dirs.DocumentDir;
let  jsonContactPanel;
let  FinalDocpath;
let SRC_PATH ;


function addFinancialInfoActionSuccess(res){
  console.log('called addMemberActionSuccess ----');
  return {
    type: 'ADDFINANCIALINFORMATION_SUCCESS',
    res: res
  };
}

export function addFinancialInformation(userid,businessName,note, address, city, state,zip,phone,email,dateOpened,dateClosed,fax,website,originalPath,contactPanel,documentPath,assests,financialCategory,accountStatus,accountNumber){

  return function (dispatch) {
    if(documentPath !=''){

       SRC_PATH         = documentPath.uri;}
    let documentFilename = documentPath.fileName;
    let documentSize     = documentPath.fileSize;
  //  console.log(dirs);
  //  console.log(userid,businessName,note, address, city, state,zip,phone,email,dateOpened,dateClosed,fax,website,originalPath,contactPanel,documentPath);
    // RNFetchBlob.fs.mkdir(DEST_PATH)
    // .then((file) => { console.log("new folder create",file) })
    // .catch((err) => {  })
    // console.log(DEST_PATH);

    //console.log(SRC_PATH);
    // RNFetchBlob.fs.writeFile(DEST_PATH, SRC_PATH, 'MyFile')
    //           .then((file)=>{ console.log("folder create",file); })
  //   RNFetchBlob.fs.cp(SRC_PATH.split(':///')[1], DEST_PATH)
  //   .then((file) => { console.log("folder create",file);  })
  //   .catch((err) => { console.log(err); })
  //   console.log(RNFetchBlob);
  //   let roomImageDir="userFiles1";
  //
  //   RNFetchBlob.fs.isDir(roomImageDir)
	// .then((isDir) => {
  //   console.log(isDir);
	// 	if(isDir === false) {
	// 		RNFetchBlob.fs.mkdir(roomImageDir);
  //     console.log('new file created')
	// 		.catch((err) => {
	// 			// do nothing
	// 			 console.log(err);
	// 		})
	// 	}
	//  })
  //   return false;
  let fileUri;
  if(documentPath !=''){
      RNFetchBlob
      .config({ path : RNFetchBlob.fs.dirs.DocumentDir + '/FamilyInheritanceManagementFiles/'+SRC_PATH.split('/').slice(-1)  })
      .fetch('GET', SRC_PATH, {
          'Cache-Control' : 'no-store'
      })
      .then((res) => {
          res.path() // where the file is
          console.log(res.path());
          fileUri      = res.path();
          FinalDocpath ={
                     uri       : fileUri,
                     fileName  : documentFilename,
                     fileSize  : documentSize,
                   }
          DBProcess(FinalDocpath)

      })
    }else{
      DBProcess('')
    }

   function DBProcess(FinalDocpath){
    console.log(FinalDocpath);

    let jsonContactPanel = JSON.stringify(contactPanel);

    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);

    function errorCB(err) {
      console.log('errorCB');
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
      console.log("SQL executed fine");
      //updateStorage();
    };

    function openCB() {
      console.log("Database OPENED");
    };

    function successInsert() {
      console.log("SQL inserted fine");
     dispatch(addFinancialInfoActionSuccess({addFinancialInfo:'success'}));

    };


    db.transaction((tx) => {
      //console.log('table creation',tx);
      tx.executeSql('CREATE TABLE IF NOT EXISTS financial_information( '
                + 'id INTEGER PRIMARY KEY NOT NULL,'
                + 'creator_userid INTEGER ,'
                + 'businessname VARCHAR(100),'
                + 'account_number VARCHAR(100),'
                + 'assests VARCHAR(100),'
                + 'financial_category VARCHAR(100),'
                + 'account_status VARCHAR(100),'
                + 'email VARCHAR(100),'
                + 'address TEXT(100),'
                + 'city VARCHAR(20),'
                + 'state VARCHAR(20),'
                + 'zip   VARCHAR(20),'
                + 'phone VARCHAR(20),'
                + 'fax VARCHAR(30),'
                + 'website VARCHAR(100),'
                + 'file_path VARCHAR(150),'
                + 'document_path VARCHAR(150),'
                + 'contacts_details TEXT,'
                + 'note TEXT(100),'
                + 'date_opened VARCHAR(100),'
                + 'date_closed VARCHAR(100),'
                + 'status VARCHAR(20)); ', [], successCB, errorCB);

       tx.executeSql('INSERT INTO financial_information (creator_userid, businessname, email, address, city, state, zip, phone,fax, website, file_path,document_path, contacts_details,note,date_opened,date_closed,status,assests,financial_category,account_status,account_number)'
      +'VALUES ('+ userid +',"' + businessName + '", "' + email.toLowerCase() + '", "' + address + '", "' + city + '", "' + state + '", "' + zip + '", "' + phone + '", "' + fax + '", "' + website + '", \'' + JSON.stringify(originalPath) + '\', \''
       + JSON.stringify(FinalDocpath) + '\', \'' + jsonContactPanel + '\', "' + note + '", "' + dateOpened + '" ,"' + dateClosed + '" , "active","'+assests+'","'+financialCategory+'","'+accountStatus+'","'+accountNumber+'");', [], successInsert, errorCB);

              });
            }
  }
}

function getFinancialInfoActionSuccess(res){
  console.log('called getFamilyMemberActionSuccess ----');
  return {
    type: 'GETFINANCIALINFORMATION_SUCCESS',
    res: res
  };
}
export function getFinancialInformation(userid){

  return function (dispatch) {
    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);

    function errorCB(err) {
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
      console.log("SQL executed fine");

    };

    function openCB() {
      console.log("Database OPENED");
    };
      db.transaction((tx) => {
        tx.executeSql('CREATE TABLE IF NOT EXISTS financial_information( '
                  + 'id INTEGER PRIMARY KEY NOT NULL,'
                  + 'creator_userid INTEGER ,'
                  + 'businessname VARCHAR(100),'
                  + 'account_number VARCHAR(100),'
                  + 'assests VARCHAR(100),'
                  + 'financial_category VARCHAR(100),'
                  + 'account_status VARCHAR(100),'
                  + 'email VARCHAR(100),'
                  + 'address TEXT(100),'
                  + 'city VARCHAR(20),'
                  + 'state VARCHAR(20),'
                  + 'zip   VARCHAR(20),'
                  + 'phone VARCHAR(20),'
                  + 'fax VARCHAR(30),'
                  + 'website VARCHAR(100),'
                  + 'file_path VARCHAR(150),'
                  + 'document_path VARCHAR(150),'
                  + 'contacts_details TEXT,'
                  + 'note TEXT(100),'
                  + 'date_opened VARCHAR(100),'
                  + 'date_closed VARCHAR(100),'
                  + 'status VARCHAR(20)); ', [], successCB, errorCB);

        let financialArray = [];
        tx.executeSql('SELECT * FROM financial_information WHERE creator_userid ="' + userid + '"', [], (tx, results) => {
          let length = results.rows.length;

                  for (let i = 0; i < length; i++) {
                    let row = results.rows.item(i);

                    FinancialObj = {
                            businessid:row.id,
                            businessname:row.businessname,
                            total_price:row.assests,
                            account_number:row.account_number,
                            financialCategory:row.financial_category,
                            account_status:row.account_status,
                            email:row.email,
                            address:row.address,
                            city:row.city,
                            state:row.state,
                            zip:row.zip,
                            phone:row.phone,
                            fax:row.fax,
                            website:row.website,
                            filePath:row.file_path,
                            documentPath:row.document_path,
                            contacts_details:row.contacts_details,
                            date_opened:row.date_opened,
                            date_closed:row.date_closed,
                            note:row.note,
                            status:row.status
                    };
                    financialArray.push(FinancialObj)

                  }

               console.log('financialArray',financialArray);
              dispatch(getFinancialInfoActionSuccess({financialInformation:financialArray}));
        });
      });

    }
}
function editFinancialInformation(res){
  console.log('called editFinancialInformation ----');
  return {
    type: 'UPDATEFINANCIALINFO_SUCCESS',
    res: res
  };
}
export function updateFinancialInformation(userid,businessid,businessName,note, address, city, state,zip,phone,email,dateOpened,dateClosed,fax,website,originalPath,contactPanel,assests,financialCategory,accountStatus,accountNumber){

  return function (dispatch) {
    console.log(userid,businessid,businessName,note, address, city, state,zip,phone,email,dateOpened,dateClosed,fax,website,originalPath,contactPanel,assests,financialCategory,accountStatus,accountNumber);
    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);
    let jsonContactPanel = JSON.stringify(contactPanel);

    function errorCB(err) {
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
      console.log("SQL executed fine");

    };

    function openCB() {
      console.log("Database OPENED");
    };

      db.transaction((tx) => {

        tx.executeSql('UPDATE financial_information SET businessname ="' + businessName + '", email ="' + email + '", address ="' + address + '",  city = "' + city + '",  state = "' + state + '", zip = "' + zip + '", phone = "' + phone + '", fax = "'
        + fax + '",website = "' + website+ '",note = "' + note + '",date_opened = "' + dateOpened + '",date_closed = "' + dateClosed + '",assests = "' + assests + '",financial_category = "' + financialCategory + '",account_status = "' + accountStatus + '",account_number = "' + accountNumber + '",contacts_details = \'' + jsonContactPanel + '\' WHERE creator_userid = ' + userid+' AND id ='+businessid, [],successCB, errorCB);
        dispatch(editFinancialInformation({update: true}));
      });

    }
}
function deleteFinancialInformationActionSuccess(res){
  console.log('called deleteFinancialInformationActionSuccess ----');
  return {
    type: 'DELETEFINANCIALINFO_SUCCESS',
    res: res
  };
}

export function deleteFinancialInformation(userid,businessid){

  return function (dispatch) {
    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);

    function errorCB(err) {
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
      console.log("SQL executed fine");

    };

    function openCB() {
      console.log("Database OPENED");
    };
      db.transaction((tx) => {

        tx.executeSql('DELETE FROM financial_information WHERE creator_userid = ' + userid+' AND id ='+businessid, [],successCB, errorCB);
        dispatch(deleteFinancialInformationActionSuccess({deleted: true}));
      });

    }
}

function getTotalAssestsActionSuccess(res){
  console.log('called getTotalAssestsActionSuccess ----');
  return {
    type: 'GETTOTALASSESTS_SUCCESS',
    res: res
  };
}

export function getTotalAssests(userid){

  return function (dispatch) {
    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);

    function errorCB(err) {
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
      console.log("SQL executed fine");

    };

    function openCB() {
      console.log("Database OPENED");
    };
      db.transaction((tx) => {
        let total_assests;
        tx.executeSql('SELECT sum(assests) as total_assests FROM financial_information WHERE creator_userid = ' + userid, [], (tx, results) => {
          let length = results.rows.length;

                  for (let i = 0; i < length; i++) {
                    let row = results.rows.item(i);
                    total_assests=row.total_assests;
                    //console.log(total_assests);
                  }
                  dispatch(getTotalAssestsActionSuccess({total_assests: total_assests}));
                  });

      });

    }
}
