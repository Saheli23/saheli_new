import React from 'react';
import {
  AsyncStorage
} from 'react-native';
import SQLite from 'react-native-sqlite-storage';
SQLite.DEBUG(true);

function stepCompletedActionSuccess(res){
  console.log('called stepCompletedActionSuccess ----');
  return {
    type: 'STEPCOMPLETED_SUCCESS',
    res: res
  };
}

function stepCompletedActionError(res){
  console.log('called stepCompletedActionError ----');
  return {
    type: 'STEPCOMPLETED_ERROR',
    res: res
  };
}

export function regStepCompleted(userid){

  return function (dispatch) {

    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);

    function errorCB(err) {
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
      console.log("SQL executed fine");
      updateStorage();
    };

    function openCB() {
      console.log("Database OPENED");


    };

    function successInsert() {
      console.log("SQL inserted fine");
      //this.props.navigation.dispatch({ type: 'RegistrationSuccess', })}
    };

      db.transaction((tx) => {
         tx.executeSql('UPDATE users SET reg_step_completed = 1 WHERE id ='+ userid , [],successCB, errorCB);

       });

      function updateStorage(){
        let userData;
        let userName;
        AsyncStorage.getItem('userDetails').then((value) =>{
          if(value){
            userData                  = JSON.parse(value);
            userName                  = value.username;
            userData.is_stepcompleted = 1;
            AsyncStorage.setItem( 'userDetails', JSON.stringify( userData ) );
          }
        }).done();
        dispatch(stepCompletedActionSuccess({loggedIn: true,userName:userName,userDetails:userData}));
    //  console.log('last data',last_insert_row_id);

      }


    }

}
