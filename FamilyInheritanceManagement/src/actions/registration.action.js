import React from 'react';
import {
  AsyncStorage
} from 'react-native';
import SQLite from 'react-native-sqlite-storage';
SQLite.DEBUG(true);

function registerDataActionSuccess(res){
  //console.log('called registerDataActionSuccess ----');
  return {
    type: 'REGISTER_SUCCESS',
    res: res
  };
}

function registerDataActionError(res){
  //console.log('called registerDataActionError ----');
  return {
    type: 'REGISTER_ERROR',
    res: res
  };
}

export function registerData(fullname,middlename,lastname,password, address, city, state, zip, phone, email, sex, age, income, maritalStatus){

  return function (dispatch) {

    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);
    function errorCB(err) {
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
    //  console.log("SQL executed fine");
    };

    function openCB() {
    //  console.log("Database OPENED");
    };

    function successInsert() {
    //  console.log("SQL inserted fine");
      //this.props.navigation.dispatch({ type: 'RegistrationSuccess', })}
    };


      db.transaction((tx) => {

        tx.executeSql('CREATE TABLE IF NOT EXISTS users( '
                  + 'id INTEGER PRIMARY KEY NOT NULL,'
                  + 'fullname VARCHAR(100),'
                  + 'middlename VARCHAR(100),'
                  + 'lastname VARCHAR(100),'
                  + 'password VARCHAR(100),'
                  + 'email VARCHAR(100),'
                  + 'address TEXT(100),'
                  + 'city VARCHAR(20),'
                  + 'state VARCHAR(20),'
                  + 'zip   VARCHAR(20),'
                  + 'phone VARCHAR(20),'
                  + 'age VARCHAR(30),'
                  + 'income VARCHAR(100),'
                  + 'marital_status VARCHAR(150),'
                  + 'sex VARCHAR(20),'
                  + 'reg_step_completed INTEGER DEFAULT (0)); ', [], successCB, errorCB);

                  tx.executeSql('SELECT * FROM users WHERE email ="' + email + '"', [], (tx, results) => {
                    //  console.log("Query completed");

                      // Get rows with Web SQL Database spec compliance.
                      // console.log(results.rows);
                      let len = results.rows.length;

                      if (len == 0){
                        tx.executeSql('INSERT INTO users (fullname,middlename,lastname, password, email, address, city, state, zip, phone, sex, age, income, marital_status) VALUES ("' + fullname + '", "' + middlename + '", "' + lastname + '", "' + password + '", "' + email.toLowerCase() + '", "' + address + '", "' + city + '", "' + state + '", "' + zip + '", "' + phone + '", "' + sex + '","'+ age +'","'+ income +'","'
                        + maritalStatus +'");', [],successInsert, errorCB);
                        let userName;
                        let userObj = {
                          userid:'',
                          username:'',
                        }
                        /* Get just enserted user details */
                        tx.executeSql('SELECT * FROM users WHERE email ="' + email + '"', [], (tx, results) => {
                          let length = results.rows.length;


                          if (length > 0){
                          //  console.log("found");

                                for (let i = 0; i < length; i++) {
                                  let row = results.rows.item(i);

                                  userName              = row.fullname;
                                  userObj.userid        = row.id;
                                  userObj.username      = row.fullname;
                                  userObj.is_stepcompleted = row.reg_step_completed;

                                }
                                AsyncStorage.setItem('userDetails',JSON.stringify(userObj));
                             }
                          });
                          tx.executeSql('CREATE TABLE IF NOT EXISTS modules( '
                                    + 'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
                                    + 'modulename VARCHAR(100),'
                                    + 'status VARCHAR(100)); ', [], successCB, errorCB);

                                     tx.executeSql('SELECT * FROM modules WHERE modulename ="funeral"', [], (tx, results) => {
                                     let length = results.rows.length;
                                     let module_array=['funeral',
                                                       'hospice' ,
                                                       'assisted_living' ,
                                                       'travel' ,
                                                       'real_estate' ,
                                                       'business' ,
                                                       'vehicles' ,
                                                       'moving_storage',
                                                       'charitable_giving',
                                                       'will' ,
                                                       'trust' ,
                                                       'insurance',
                                                       'lodging_meals'  ];
                                     if(length == 0){
                                                      for (let i = 0; i < module_array.length; i++) {
                                                          tx.executeSql('INSERT INTO modules (modulename, status) VALUES ( "' + module_array[i] + '","active");', [],successInsert, errorCB);
                                                   }
                                                 }
                                                });
                        dispatch(registerDataActionSuccess({loggedIn: true,userName:userName,userDetails:userObj}));
                      }
                      else{
                        //  console.log("insertnot");
                        dispatch(registerDataActionError({loggedIn: false}));
                      }
                      // for (let i = 0; i < len; i++) {
                      //   let row = results.rows.item(i);
                      //   console.log(`Employee name: ${row.name}, Dept Name: ${row.deptName}`);
                      // }

                      // Alternatively, you can use the non-standard raw method.

                      /*
                        let rows = results.rows.raw(); // shallow copy of rows Array

                        rows.map(row => console.log(`Employee name: ${row.name}, Dept Name: ${row.deptName}`));
                      */
                    });




  // tx.executeSql('SELECT * FROM Employees a, Departments b WHERE a.department = b.department_id', [], (tx, results) => {
  //     console.log("Query completed");
  //
  //     // Get rows with Web SQL Database spec compliance.
  //
  //     var len = results.rows.length;
  //     for (let i = 0; i < len; i++) {
  //       let row = results.rows.item(i);
  //       console.log(`Employee name: ${row.name}, Dept Name: ${row.deptName}`);
  //     }
  //
  //     // Alternatively, you can use the non-standard raw method.
  //
  //     /*
  //       let rows = results.rows.raw(); // shallow copy of rows Array
  //
  //       rows.map(row => console.log(`Employee name: ${row.name}, Dept Name: ${row.deptName}`));
  //     */
  //   });
      });


    }


  //ajax
  //this.props.navigation.dispatch({ type: 'Login', data: apiRes })}
  //this.props.navigation.dispatch({ type: 'Login_Error' })}
}
