import React from 'react';
import {
  AsyncStorage
} from 'react-native';
import SQLite from 'react-native-sqlite-storage';
SQLite.DEBUG(true);

function loginDataActionSuccess(res){
  //console.log('called LoginDataActionSuccess ----');
  return {
    type: 'LOGIN_SUCCESS',
    res: res
  };
}

function loginActionSuccessSubmitStep(res){
  //console.log('called LOGIN_SUCCESS_COMPLETE_STEP ----');
  return {
    type: 'LOGIN_SUCCESS_COMPLETE_STEP',
    res: res
  };
}

function loginDataActionError(res){
  //console.log('called LoginDataActionError ----');
  return {
    type: 'LOGIN_ERROR',
    res: res
  };
}

export function login(email,password){

  return function (dispatch) {

    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);
    function errorCB(err) {
      console.log("SQL Error: " + err);
    };
    function errorTable(err){
      dispatch(loginDataActionError({loggedIn: false,invalid:true}));
    //  console.log("SQL Error: " + Object.values(err));
    }
    function successTable(){
        //console.log("Table found");
    //  console.log("SQL Error: " + Object.values(err));
    }

    function successCB() {
      console.log("SQL executed fine");
    };

    function openCB() {
      console.log("Database OPENED");
    };

    function successInsert() {
      console.log("SQL inserted fine");
      //this.props.navigation.dispatch({ type: 'RegistrationSuccess', })}
    };


      db.transaction((tx) => {
                      tx.executeSql('SELECT count(*) FROM sqlite_master WHERE name="users"',successTable, errorTable);

                      tx.executeSql('SELECT * FROM users WHERE email ="' + email + '" AND password="' + password + '"', [], (tx, results) => {
                    //  console.log("Query completed");
                      let len = results.rows.length;
                      let userName;
                      let userObj = {
                        userid:'',
                        username:'',
                      }
                      if (len > 0){
                        console.log("found");

                        for (let i = 0; i < len; i++) {
                          let row = results.rows.item(i);

                          userName              = row.fullname;
                          userObj.userid        = row.id;
                          userObj.username      = row.fullname;
                          userObj.is_stepcompleted = row.reg_step_completed;

                        }
                        console.log(userObj);
                        if(userObj.is_stepcompleted == 0){
                          AsyncStorage.setItem('userDetails',JSON.stringify(userObj));
                          dispatch(loginActionSuccessSubmitStep({loggedIn: true,userName:userName,userDetails:userObj}));
                        }
                        else{
                          AsyncStorage.setItem('userDetails',JSON.stringify(userObj));
                      //AsyncStorage.setItem('userDetails',JSON.stringify(userObj));
                      //  tx.executeSql('INSERT INTO users (fullname, password, email, address, city, state, zip, phone, sex) VALUES ("' + fullname + '", "' + password + '", "' + email.toLowerCase() + '", "' + address + '", "' + city + '", "' + state + '", "' + zip + '", "' + phone + '", "' + sex + '");', [],successInsert, errorCB);
                        dispatch(loginDataActionSuccess({loggedIn: true,userName:userName,userDetails:userObj}));
                       }
                      }
                      else{
                        console.log("not found");
                        dispatch(loginDataActionError({loggedIn: false,invalid:true}));
                      }

                    });

      });


    }


  //ajax
  //this.props.navigation.dispatch({ type: 'Login', data: apiRes })}
  //this.props.navigation.dispatch({ type: 'Login_Error' })}
}
