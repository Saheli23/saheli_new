import React from 'react';
import {
  AsyncStorage
} from 'react-native';
import SQLite from 'react-native-sqlite-storage';
SQLite.DEBUG(true);

function answerDataActionSuccess(res){
  console.log('called answerDataActionSuccess ----');
  return {
    type: 'QUSANS_SUCCESS',
    res: res
  };
}

function answerDataActionError(res){
  console.log('called LoginDataActionError ----');
  return {
    type: 'QUSANS_ERROR',
    res: res
  };
}

export function answerDataSubmit(answerData,userid){

  return function (dispatch) {

    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);
    console.log('answerData',answerData);
    function errorCB(err) {
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
      console.log("SQL executed fine");
    };

    function openCB() {
      console.log("Database OPENED");


    };
    function successReg() {
      console.log("SQL executed fine");
      updateStorage();
    };

    function successInsert() {
      console.log("SQL inserted fine");
      //this.props.navigation.dispatch({ type: 'RegistrationSuccess', })}
    };

      db.transaction((tx) => {
      //  console.info("into tran");
        answerData.funeral         = answerData.funeral == true ? 1 : 0;
        answerData.hospice         = answerData.hospice == true ? 1 : 0;
        answerData.assisted_living = answerData.assisted_living == true ? 1 : 0;
        answerData.travel          = answerData.travel == true ? 1 : 0;
        answerData.real_estate     = answerData.real_estate == true ? 1 : 0;
        answerData.business        = answerData.business == true ? 1 : 0;
        answerData.vehicles        = answerData.vehicles == true ? 1 : 0;
        answerData.moving_storage  = answerData.moving_storage == true ? 1 : 0;
        answerData.charitable_giving = answerData.charitable_giving == true ? 1 : 0;
        answerData.will            = answerData.will == true ? 1 : 0;
        answerData.trust           = answerData.trust == true ? 1 : 0;
        answerData.insurance       = answerData.insurance == true ? 1 : 0;

        tx.executeSql('CREATE TABLE IF NOT EXISTS question_answer( '
                  + 'id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,'
                  + 'userid INTEGER,'
                  + 'funeral BOOL,'
                  + 'hospice BOOL,'
                  + 'assisted_living BOOL,'
                  + 'travel BOOL,'
                  + 'real_estate BOOL,'
                  + 'business BOOL,'
                  + 'vehicles BOOL,'
                  + 'moving_storage BOOL,'
                  + 'charitable_giving BOOL,'
                  + 'will BOOL,'
                  + 'trust BOOL,'
                  + 'insurance BOOL,'
                  + 'lodging_meals BOOL); ', [], successCB, errorCB);

          tx.executeSql('SELECT * FROM question_answer WHERE userid =' + userid , [], (tx, results) => {
            let len = results.rows.length;

            if (len == 0){
              tx.executeSql('INSERT INTO question_answer (userid, funeral, hospice, assisted_living, travel, real_estate, business, vehicles, moving_storage, charitable_giving,will,trust,insurance,lodging_meals) VALUES (' + userid + ', ' + answerData.funeral + ', ' + answerData.hospice + ', ' + answerData.assisted_living + ', ' + answerData.travel + ', ' + answerData.real_estate + ', ' + answerData.business + ', ' + answerData.vehicles + ','
               + answerData.moving_storage + ', ' + answerData.charitable_giving + ', ' + answerData.will + ', ' + answerData.trust +', ' + answerData.insurance + ', ' + answerData.travel + ');', [],successInsert, errorCB);

                // db.transaction((tx) => {
                    tx.executeSql('UPDATE users SET reg_step_completed = 1 WHERE id ='+ userid , [],successCB, errorCB);

                //  });
                  let userData;
                  let userName;
                  AsyncStorage.getItem('userDetails').then((value) =>{
                    console.log('localdata',value);
                    if(value){
                      userData                  = JSON.parse(value);
                      userName                  = value.username;
                      userData.is_stepcompleted = 1;
                      AsyncStorage.setItem( 'userDetails', JSON.stringify( userData ) );
                    }
                    dispatch(answerDataActionSuccess({insert: true,userName:userName,userDetails:userData}));
                  }).done();

            }
            else{
              tx.executeSql('UPDATE question_answer SET funeral =' + answerData.funeral + ', hospice =' + answerData.hospice + ', assisted_living =' + answerData.assisted_living + ',  travel = ' + answerData.travel + ',  real_estate = ' + answerData.real_estate + ', business = ' + answerData.business + ', vehicles = ' + answerData.vehicles + ', moving_storage = '
              + answerData.moving_storage + ',charitable_giving = ' + answerData.charitable_giving + ',will = ' + answerData.will + ',trust = ' + answerData.trust + ',insurance =' + answerData.insurance +',lodging_meals =' + answerData.travel + ' WHERE userid = ' + userid, [],successInsert, errorCB);
            //  dispatch(answerDataActionSuccess({update: true}));
            tx.executeSql('UPDATE users SET reg_step_completed = 1 WHERE id ='+ userid , [],successCB, errorCB);

        //  });
          let userData;
          let userName;
          AsyncStorage.getItem('userDetails').then((value) =>{
            console.log('localdata',value);
            if(value){
              userData                  = JSON.parse(value);
              userName                  = value.username;
              userData.is_stepcompleted = 1;
              AsyncStorage.setItem( 'userDetails', JSON.stringify( userData ) );
            }
            dispatch(answerDataActionSuccess({update: true,userName:userName,userDetails:userData}));
          }).done();
            }
          });


        //  console.log('last data',last_insert_row_id);
                    });

    }

}
