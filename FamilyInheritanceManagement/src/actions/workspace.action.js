import React from 'react';
import {
  AsyncStorage
} from 'react-native';
import SQLite from 'react-native-sqlite-storage';
SQLite.DEBUG(true);

function workspaceInfoActionSuccess(res){
  //console.log('called workspaceInfoActionSuccess ----');
  return {
    type: 'WORKSPACEINFO_SUCCESS',
    res: res
  };
}

function workspaceInfoActionError(res){
  //console.log('called workspaceInfoActionError ----');
  return {
    type: 'WORKSPACEINFO_ERROR',
    res: res
  };
}

export function workspaceInfo(userid){

  return function (dispatch) {

    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);

    function errorCB(err) {
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
    //  console.log("SQL executed fine");
      updateStorage();
    };

    function openCB() {
    //  console.log("Database OPENED");


    };

    function successInsert() {
    //  console.log("SQL inserted fine");
      //this.props.navigation.dispatch({ type: 'RegistrationSuccess', })}
    };
  db.transaction((tx) => {
    tx.executeSql('SELECT * FROM question_answer WHERE userid =' + userid , [], (tx, results) => {
      let len = results.rows.length;
      let workspaceObj = {
              userid:'',
              funeral:'',
              hospice:'',
              assisted_living:'',
              travel:'',
              real_estate:'',
              business:'',
              vehicles:'',
              moving_storage:'',
              charitable_giving:'',
              will:'',
              trust:'',
              insurance:'',
              lodging_meals:''

      }
      if (len > 0){
      //  console.log("found");

        for (let i = 0; i < len; i++) {
            let row = results.rows.item(i);
            workspaceObj.userid           = row.userid;
            workspaceObj.funeral          = row.funeral;
            workspaceObj.hospice          = row.hospice;
            workspaceObj.assisted_living  = row.assisted_living;
            workspaceObj.travel           = row.travel;
            workspaceObj.real_estate      = row.real_estate;
            workspaceObj.business         = row.business;
            workspaceObj.vehicles         = row.vehicles;
            workspaceObj.moving_storage   = row.moving_storage;
            workspaceObj.charitable_giving= row.charitable_giving;
            workspaceObj.will             = row.will;
            workspaceObj.trust            = row.trust;
            workspaceObj.insurance        = row.insurance;
            workspaceObj.lodging_meals    = row.lodging_meals;

          dispatch(workspaceInfoActionSuccess({workspaceDetails:workspaceObj}));
        }
    //  console.log('last data',last_insert_row_id);

        }
      });
    });

  }
}

export function updateWorkspace(userid,workspaceModule){
  function addWorkspaceActionSuccess(res){
  //  console.log('called addWorkspaceActionSuccess ----');
    return {
      type: 'ADDWORKSPACE_SUCCESS',
      res: res
    };
  }
  return function(dispatch){
    function errorCB(err) {
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
    //  console.log("SQL updated fine");
      dispatch(addWorkspaceActionSuccess({addWorkspace:'success'}));
    };

    function openCB() {
    //  console.log("Database OPENED");


    };
      var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);
      db.transaction((tx) => {
        tx.executeSql('UPDATE question_answer SET '+ workspaceModule +' = 1 WHERE userid ='+ userid , [],successCB, errorCB);
      });
  }

}
