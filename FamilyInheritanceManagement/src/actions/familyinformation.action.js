import React from 'react';
import {
  AsyncStorage
} from 'react-native';
import SQLite from 'react-native-sqlite-storage';
SQLite.DEBUG(true);

function addMemberActionSuccess(res,navigateToAnotherPage){
//  console.log('called addMemberActionSuccess ----',navigateToAnotherPage);
  if(navigateToAnotherPage){
    return {
      type: 'ADDMEMBER_SUCCESS_NAVIGATE',
      res: res
    };
  }
  else{
    return {
      type: 'ADDMEMBER_SUCCESS',
      res: res
    };
  }
}

function addMemberActionError(res){
  //console.log('called addMemberActionError ----');
  return {
    type: 'ADDMEMBER_ERROR',
    res: res
  };
}

export function addFamilyMember(userid,membername,relation,ssn,note, address, city, state,zip,phone,email,birthday,samepage){

  return function (dispatch) {

    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);

    function errorCB(err) {
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
    //  console.log("SQL executed fine");
    //  updateStorage();
    };

    function openCB() {
    //  console.log("Database OPENED");


    };

    function successInsert(samepage) {
    //  console.log("SQL inserted fine");
      let navigateToAnotherPage = samepage;
      dispatch(addMemberActionSuccess({addFamilyMember:'success'},navigateToAnotherPage));
      //this.props.navigation.dispatch({ type: 'RegistrationSuccess', })}
    };

    db.transaction((tx) => {
      tx.executeSql('CREATE TABLE IF NOT EXISTS family_information( '
                + 'id INTEGER PRIMARY KEY NOT NULL,'
                + 'creator_userid INTEGER ,'
                + 'membername VARCHAR(100),'
                + 'email VARCHAR(100),'
                + 'address TEXT(100),'
                + 'city VARCHAR(20),'
                + 'state VARCHAR(20),'
                + 'zip   VARCHAR(20),'
                + 'phone VARCHAR(20),'
                + 'birthday VARCHAR(100),'
                + 'age VARCHAR(30),'
                + 'income VARCHAR(100),'
                + 'marital_status VARCHAR(150),'
                + 'sex VARCHAR(20),'
                + 'ssn VARCHAR(20),'
                + 'note TEXT(100),'
                + 'relation VARCHAR(20)); ', [], successCB, errorCB);

       tx.executeSql('INSERT INTO family_information (creator_userid, membername, email, address, city, state, zip, phone,birthday, ssn, note, relation) VALUES ('+ userid +',"' + membername + '", "' + email.toLowerCase() + '", "' + address + '", "' + city + '", "' + state + '", "' + zip + '", "' + phone + '", "'+birthday+'" , "' + ssn + '", "'+note+'" , "'+ relation +'");', [],successInsert(samepage), errorCB);
              });



  }
}

function getFamilyMemberActionSuccess(res){
  //console.log('called getFamilyMemberActionSuccess ----');
  return {
    type: 'GETMEMBER_SUCCESS',
    res: res
  };
}
export function getFamilyMember(userid){

  return function (dispatch) {
    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);

    function errorCB(err) {
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
    //  console.log("SQL executed fine");

    };

    function openCB() {
      console.log("Database OPENED");
    };
      db.transaction((tx) => {

        tx.executeSql('CREATE TABLE IF NOT EXISTS family_information( '
                  + 'id INTEGER PRIMARY KEY NOT NULL,'
                  + 'creator_userid INTEGER ,'
                  + 'membername VARCHAR(100),'
                  + 'email VARCHAR(100),'
                  + 'address TEXT(100),'
                  + 'city VARCHAR(20),'
                  + 'state VARCHAR(20),'
                  + 'zip   VARCHAR(20),'
                  + 'phone VARCHAR(20),'
                  + 'birthday VARCHAR(100),'
                  + 'age VARCHAR(30),'
                  + 'income VARCHAR(100),'
                  + 'marital_status VARCHAR(150),'
                  + 'sex VARCHAR(20),'
                  + 'ssn VARCHAR(20),'
                  + 'note TEXT(100),'
                  + 'relation VARCHAR(20)); ', [], successCB, errorCB);

        let memberArray = [];
        tx.executeSql('SELECT * FROM family_information WHERE creator_userid ="' + userid + '"', [], (tx, results) => {
          let length = results.rows.length;

                  for (let i = 0; i < length; i++) {
                    let row = results.rows.item(i);

                    memberObj = {
                            memberid:row.id,
                            membername:row.membername,
                            email:row.email,
                            address:row.address,
                            city:row.city,
                            state:row.state,
                            zip:row.zip,
                            phone:row.phone,
                            birthday:row.birthday,
                            ssn:row.ssn,
                            note:row.note,
                            relation:row.relation
                    };
                    memberArray.push(memberObj)

                  }

               console.log('memberArray',memberArray);
              dispatch(getFamilyMemberActionSuccess({familyInformation:memberArray}));
        });
      });

    }
}

function updateFamilyMemberActionSuccess(res){
  //console.log('called getFamilyMemberActionSuccess ----');
  return {
    type: 'UPDATEMEMBER_SUCCESS',
    res: res
  };
}

export function updateFamilyMember(userid,memberid,membername,relation,ssn,note, address, city, state,zip,phone,email,birthday){

  return function (dispatch) {
    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);

    function errorCB(err) {
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
    //  console.log("SQL executed fine");

    };

    function openCB() {
    //  console.log("Database OPENED");
    };
      db.transaction((tx) => {

        tx.executeSql('UPDATE family_information SET membername ="' + membername + '", email ="' + email + '", address ="' + address + '",  city = "' + city + '",  state = "' + state + '", zip = "' + zip + '", phone = "' + phone + '", birthday = "'
        + birthday + '",ssn = "' + ssn+ '",note = "' + note + '",relation = "' + relation + '" WHERE creator_userid = ' + userid+' AND id ='+memberid, [],successCB, errorCB);
        dispatch(updateFamilyMemberActionSuccess({update: true}));
      });

    }
}
function deleteFamilyMemberActionSuccess(res){
//  console.log('called getFamilyMemberActionSuccess ----');
  return {
    type: 'DELETEMEMBER_SUCCESS',
    res: res
  };
}

export function deleteFamilyMember(userid,memberid){

  return function (dispatch) {
    var db = SQLite.openDatabase("inheritance.sqlite", "1.0", "Test Database", 200000, openCB, errorCB);

    function errorCB(err) {
      console.log("SQL Error: " + Object.values(err));
    };

    function successCB() {
    //  console.log("SQL executed fine");

    };

    function openCB() {
    //  console.log("Database OPENED");
    };
      db.transaction((tx) => {

        tx.executeSql('DELETE FROM family_information WHERE creator_userid = ' + userid+' AND id ='+memberid, [],successCB, errorCB);
        dispatch(updateFamilyMemberActionSuccess({deleted: true}));
      });

    }
}
