import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity,
  AsyncStorage


} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,} from 'native-base';
import Switch from 'react-native-customisable-switch';
import styles from '../styles/questionAnswerStyle';
var {height, width} = Dimensions.get('window');
var logo = require('../images/logo.png');
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { NavigationActions } from 'react-navigation';
var Textlogo = require('../images/text_logo.png');
import { Images} from '../Themes';


class QuestionOnePageScreen extends React.Component {
  constructor(props) {
    super(props);
    Text.defaultProps.allowFontScaling=false;
    this.state = {
      funeral:false,
      hospice:false,
      assisted_living:false,
      travel:false,
      real_estate:false,
      business:false,
      vehicles:false,
      moving_storage:false,
      charitable_giving:false,
      will:false,
      trust:false,
      insurance:false,
      userid:0,

    };

  }
  static navigationOptions = {
    title:'',
    header:<View style={{height:(height/9),marginLeft:20,alignItems:'center'}}>
              <View style={{flexDirection:'row',marginTop:10}} >
                  <Image
                   style={styles.headerLogoPanel}
                   source={Images.modifiedLogo}/>
              </View>
            </View>  ,
    };

    answerSubmit(navigate){
      // navigate('QuestionTwoPage');
      let answerData = {funeral:this.state.funeral,hospice:this.state.hospice,assisted_living:this.state.assisted_living,travel:this.state.travel,moving_storage:this.state.moving_storage,charitable_giving:this.state.charitable_giving};
      console.log('answerDataSet1',JSON.stringify(answerData));
      AsyncStorage.setItem('answerDataSet',JSON.stringify(answerData));
      navigate('QuestionTwoPage',{ answerDataSet: answerData});


    }
  render() {

        const activeButtonBackgroundColor = '#fff';
        const activeBackgroundColor       = '#A1C1E5';
        const activeTextColor             = '#000';//'#39414B';
        const inactiveBackgroundColor     = '#ddd';
        const inactiveTextColor           = '#444';
        const fontSize                    =     18;


        const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Content>
           <View style={styles.mainLogoPanel}>
               {/*<View style={styles.logoPanel}>
                    <Image
                    style={{width: 90, height: 150,}}
                    source={logo}
                    />
                </View>*/}
            </View>
                <View style={styles.mainQuesPanel}>
                  <View style={{flexDirection:'row'}}>
                        <Text style={styles.questions}>Is your family member in Assited Living?</Text>
                        <View style={styles.answerSwitch}>
                          <Switch
                              defaultValue={false}
                              activeText={'Yes'}
                              inactiveText={'No'}
                              fontSize={fontSize}
                              activeTextColor={activeTextColor}
                              inactiveTextColor={inactiveTextColor}
                              activeBackgroundColor={activeBackgroundColor}
                              inactiveBackgroundColor={inactiveBackgroundColor}
                              activeButtonBackgroundColor={activeButtonBackgroundColor}
                              inactiveButtonBackgroundColor={'rgba(255, 255, 255, 1)'}
                              switchWidth={85}
                              switchHeight={40}
                              switchBorderRadius={20}
                              switchBorderColor={'rgba(0, 0, 0, 1)'}
                              switchBorderWidth={0}
                              buttonWidth={35}
                              buttonHeight={35}
                              buttonBorderRadius={20}
                              buttonBorderColor={'rgba(0, 0, 0, 1)'}
                              buttonBorderWidth={0}
                              animationTime={150}
                              padding={true}
                              onChangeValue={(value) => {
                                this.setState({assisted_living:value});
                                console.log(value);
                              }}
                            />
                          </View>
                  </View>
                    <View style={styles.eachQuestionSection}>
                      <Text style={styles.questions}>Is your family member currently in Hospice?</Text>
                      <View style={styles.answerSwitch}>
                        <Switch
                            defaultValue={false}
                            activeText={'Yes'}
                            inactiveText={'No'}
                            fontSize={fontSize}
                            activeTextColor={activeTextColor}
                            inactiveTextColor={inactiveTextColor}
                            activeBackgroundColor={activeBackgroundColor}
                            inactiveBackgroundColor={inactiveBackgroundColor}
                            activeButtonBackgroundColor={activeButtonBackgroundColor}
                            inactiveButtonBackgroundColor={'rgba(255, 255, 255, 1)'}
                            switchWidth={85}
                            switchHeight={40}
                            switchBorderRadius={20}
                            switchBorderColor={'rgba(0, 0, 0, 1)'}
                            switchBorderWidth={0}
                            buttonWidth={35}
                            buttonHeight={35}
                            buttonBorderRadius={20}
                            buttonBorderColor={'rgba(0, 0, 0, 1)'}
                            buttonBorderWidth={0}
                            animationTime={150}
                            padding={true}
                            onChangeValue={(value) => {
                              this.setState({hospice:value});
                              console.log(value);
                            }}
                          />
                        </View>
                      </View>
                  <View style={styles.eachQuestionSection}>
                    <Text style={styles.questions}>Has your family member passed?</Text>
                      <View style={styles.answerSwitch}>
                          <Switch
                              defaultValue={false}
                              activeText={'Yes'}
                              inactiveText={'No'}
                              fontSize={fontSize}
                              activeTextColor={activeTextColor}
                              inactiveTextColor={inactiveTextColor}
                              activeBackgroundColor={activeBackgroundColor}
                              inactiveBackgroundColor={inactiveBackgroundColor}
                              activeButtonBackgroundColor={activeButtonBackgroundColor}
                              inactiveButtonBackgroundColor={'rgba(255, 255, 255, 1)'}
                              switchWidth={85}
                              switchHeight={40}
                              switchBorderRadius={20}
                              switchBorderColor={'rgba(0, 0, 0, 1)'}
                              switchBorderWidth={0}
                              buttonWidth={35}
                              buttonHeight={35}
                              buttonBorderRadius={20}
                              buttonBorderColor={'rgba(0, 0, 0, 1)'}
                              buttonBorderWidth={0}
                              animationTime={150}
                              padding={true}
                              onChangeValue={(value) => {
                                this.setState({funeral:value});
                                console.log(value);
                              }}
                            />
                        </View>
                    </View>
                        <View style={styles.eachQuestionSection}>
                          <Text style={styles.questions}>Will you or your family be traveling while organizing the estate?</Text>
                          <View style={styles.answerSwitch}>
                            <Switch
                                defaultValue={false}
                                activeText={'Yes'}
                                inactiveText={'No'}
                                fontSize={fontSize}
                                activeTextColor={activeTextColor}
                                inactiveTextColor={inactiveTextColor}
                                activeBackgroundColor={activeBackgroundColor}
                                inactiveBackgroundColor={inactiveBackgroundColor}
                                activeButtonBackgroundColor={activeButtonBackgroundColor}
                                inactiveButtonBackgroundColor={'rgba(255, 255, 255, 1)'}
                                switchWidth={85}
                                switchHeight={40}
                                switchBorderRadius={20}
                                switchBorderColor={'rgba(0, 0, 0, 1)'}
                                switchBorderWidth={0}
                                buttonWidth={35}
                                buttonHeight={35}
                                buttonBorderRadius={20}
                                buttonBorderColor={'rgba(0, 0, 0, 1)'}
                                buttonBorderWidth={0}
                                animationTime={150}
                                padding={true}
                                onChangeValue={(value) => {
                                  this.setState({travel:value});
                                  console.log(value);
                                }}
                              />
                            </View>
                          </View>
                    <View style={styles.eachQuestionSection}>
                      <Text style={styles.questions}>Will you be moving family items and/or storing them?</Text>
                      <View style={styles.answerSwitch}>
                        <Switch
                            defaultValue={false}
                            activeText={'Yes'}
                            inactiveText={'No'}
                            fontSize={fontSize}
                            activeTextColor={activeTextColor}
                            inactiveTextColor={inactiveTextColor}
                            activeBackgroundColor={activeBackgroundColor}
                            inactiveBackgroundColor={inactiveBackgroundColor}
                            activeButtonBackgroundColor={activeButtonBackgroundColor}
                            inactiveButtonBackgroundColor={'rgba(255, 255, 255, 1)'}
                            switchWidth={85}
                            switchHeight={40}
                            switchBorderRadius={20}
                            switchBorderColor={'rgba(0, 0, 0, 1)'}
                            switchBorderWidth={0}
                            buttonWidth={35}
                            buttonHeight={35}
                            buttonBorderRadius={20}
                            buttonBorderColor={'rgba(0, 0, 0, 1)'}
                            buttonBorderWidth={0}
                            animationTime={150}
                            padding={true}
                            onChangeValue={(value) => {
                              this.setState({moving_storage:value});
                            }}
                          />
                        </View>
                      </View>
                      <View style={styles.eachQuestionSection}>
                        <Text style={styles.questions}>Will you be donating to charity?</Text>
                        <View style={styles.answerSwitch}>
                          <Switch
                              defaultValue={false}
                              activeText={'Yes'}
                              inactiveText={'No'}
                              fontSize={fontSize}
                              activeTextColor={activeTextColor}
                              inactiveTextColor={inactiveTextColor}
                              activeBackgroundColor={activeBackgroundColor}
                              inactiveBackgroundColor={inactiveBackgroundColor}
                              activeButtonBackgroundColor={activeButtonBackgroundColor}
                              inactiveButtonBackgroundColor={'rgba(255, 255, 255, 1)'}
                              switchWidth={85}
                              switchHeight={40}
                              switchBorderRadius={20}
                              switchBorderColor={'rgba(0, 0, 0, 1)'}
                              switchBorderWidth={0}
                              buttonWidth={35}
                              buttonHeight={35}
                              buttonBorderRadius={20}
                              buttonBorderColor={'rgba(0, 0, 0, 1)'}
                              buttonBorderWidth={0}
                              animationTime={150}
                              padding={true}
                              onChangeValue={(value) => {
                                this.setState({charitable_giving:value});
                              }}
                            />
                          </View>
                        </View>

                            <View style = {styles.bottomPanel}>
                                  <Text style = {styles.pageNoText}>
                                   Page 1 of 2
                                   </Text>
                                  <Button style={styles.continueBtn} rounded  onPress={() => this.answerSubmit(navigate)}>
                                     <Text style={styles.btnText}  >Continue</Text>
                                   </Button>
                            </View>
                    </View>
            </Content>
      </View>

    );
  }
}
function mapStateToProps(state, ownProps) {
  return {
  //  user: state.auth.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
  //  actions: bindActionCreators(registration, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(QuestionOnePageScreen);
