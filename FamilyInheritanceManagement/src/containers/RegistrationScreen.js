import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity


} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,Spinner,CheckBox } from 'native-base';
import styles from '../styles/registrationStyle';
import ModalPicker from '../library/react-native-modal-picker'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import * as registration from '../actions/registration.action';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import Toast, {DURATION} from 'react-native-easy-toast';
import * as EmailValidator from 'email-validator';
import States from '../_global/States';
import MaritalStatus from '../_global/MaritalStatus';
import IncomeOption from '../_global/IncomeOption';
import AgeOption from '../_global/AgeOption';
import GenderOption from '../_global/GenderOption';
import { Images} from '../Themes';


var {height, width} = Dimensions.get('window');
var logo = require('../images/logo.png');
var radio_props = [
{label: 'Male', value: 'M' },
{label: 'Female', value: 'F' }
];


class RegistrationScreen extends React.Component {
  constructor(props) {
    super(props);
    Text.defaultProps.allowFontScaling=false;
    this.state = {
      ageRange:'',
      pressStatus1:false,
      pressStatus2:false,
      pressStatus3:false,
      pressStatus4:false,
      fullname:'',
      middleName:'',
      lastName:'',
      password:'',
      conpassword:'',
      address:'',
      city:'',
      state:'',
      zip:'',
      phone:'',
      email:'',
      sex:'',
      age:'',
      income:'',
      maritalStatus:'',
      isLoading:false,
      terms:false
    };
  }

  static navigationOptions = {
    title:'',
    header:null,

  };
  componentWillMount(){
   //console.log(Text.defaultProps);
  }
  componentWillReceiveProps (newProps) {
    //console.log('newProps',newProps);
    if(newProps &&  newProps.user && newProps.user.loggedIn == false){
      this.refs.toast.show('You have already registered, please Cancel and return to the login page.',7000);
      this.setState({isLoading:false});
    }
  }

selectAge(ageRange){
    if (ageRange =='range1'){
      this.setState({pressStatus1:true,pressStatus2:false,pressStatus3:false,pressStatus4:false,ageRange:ageRange})
    }
    else if(ageRange =='range2'){
      this.setState({pressStatus1:false,pressStatus2:true,pressStatus3:false,pressStatus4:false,ageRange:ageRange})
    }
    else if(ageRange =='range3'){
    this.setState({pressStatus1:false,pressStatus2:false,pressStatus3:true,pressStatus4:false,ageRange:ageRange})
  }
  else if (ageRange =='range4') {
    this.setState({pressStatus1:false,pressStatus2:false,pressStatus3:false,pressStatus4:true,ageRange:ageRange})
  }
}
checkBoxChanged()
    {
    this.state.terms ? this.setState({terms: false}):this.setState({terms: true});
    }

    onPhoneFormat(val){
      //console.log(val);
      value = val
         .match(/\d*/g).join('')
         .match(/(\d{0,3})(\d{0,3})(\d{0,4})/).slice(1).join('-')
         .replace(/-*$/g, '')
     ;
      this.setState({ phone: value })

    }

 regiterSubmit(navigate){
   //console.log('sex',sex);
    const {fullname,middleName,lastName,password,conpassword, address, city, state,zip,phone,email,sex,age,income,maritalStatus,terms} = this.state;
    let emailValidate = EmailValidator.validate(email);
     if(fullname == ""){
          this.refs.toast.show('Please provide Full Name!',DURATION.LENGTH_SHORT);
        }
     else if(password == ""){
            this.refs.toast.show('Please provide Password!',DURATION.LENGTH_SHORT);
        }
     else if(password != conpassword){
               this.refs.toast.show('Password and confirm password not matching!',DURATION.LENGTH_SHORT);
           }
     else if(email == ""){
          this.refs.toast.show('Please provide Email Address!',DURATION.LENGTH_SHORT);
        }
      else if(!emailValidate){
          this.refs.toast.show('Please provide Valid Email Address!',DURATION.LENGTH_SHORT);
        }
      // else if(sex == ''){
      //     this.refs.toast.show('Please select your Gender!',DURATION.LENGTH_SHORT);
      //   }
        // else if(!terms){
        //      this.refs.toast.show('Please agree with Terms and Conditions!',DURATION.LENGTH_SHORT);
        //    }
     else{
          this.setState({isLoading:true});
          this.props.actions.registerData(fullname,middleName,lastName,password, address, city, state,zip,phone,email,sex,age,income,maritalStatus);
        }
  }

  render() {
  //  console.log(this.props.user);
    //console.log(States);

    const { navigate } = this.props.navigation;
      // let index = 0;
      //     const data = [
      //         { key: index++, label: 'WB' },
      //         { key: index++, label: 'US' },
      //         { key: index++, label: 'AS' },
      //         { key: index++, label: 'KY' },
      //
      //     ];

    const  RADIUS = 6;

    return (
      <View style={styles.container}>
            <Toast
               ref="toast"
               style={{backgroundColor:'#ff704d',zIndex:999,}}
               position='top'
               positionValue={145}
               fadeInDuration={750}
               fadeOutDuration={750}
               opacity={0.9}
               textStyle={{color:'white'}}
           />
             <Content>
               <View style={styles.mainContainer}>
                   <View style={{flexDirection:'row', flex:1,alignItems:'center',justifyContent: 'center',marginTop:35}}>
                    {/*  <Image
                         style={{width: 80, height: 130,right:10}}
                         source={logo}
                       />
                       <Text style={{right:10,fontSize:40,marginBottom:5,fontFamily:'Roboto',fontWeight: '400',}} >
                       Family Inheritance Management</Text> */}

                       <Image

                        source={Images.largeLogo}
                      />
                   </View>
                  {this.state.isLoading &&
                    <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
                    <Spinner color='#737373' size='large' />
                    </View>
                  }
                  <View style={[styles.devideTextbox,{marginTop:40}]}>
                    <Item regular style={[styles.inputBox,{marginTop:(height/51.2),width:(width/3.45),borderRadius: RADIUS}]}>
                       <Input autoCorrect={false} style={styles.placeholderFont} returnKeyType="next"  onChangeText={ (text) => this.setState({ fullname: text }) }  onSubmitEditing={(event) => { this.refs.middlename._root.focus(); }} placeholder='First Name' />
                     </Item>
                     <Item regular style={[styles.inputBox,{marginTop:(height/51.2),width:(width/4.5),marginRight:3.5,marginLeft:6,borderRadius: RADIUS}]}>
                        <Input autoCorrect={false} style={styles.placeholderFont} returnKeyType="next"  ref='middlename' onChangeText={ (text) => this.setState({ middleName: text }) } onSubmitEditing={(event) => { this.refs.lastname._root.focus(); }}   placeholder='Middle Name' />
                      </Item>
                      <Item regular style={[styles.inputBox,{marginTop:(height/51.2),width:(width/3.5),borderRadius: RADIUS}]}>
                         <Input autoCorrect={false} style={styles.placeholderFont} returnKeyType="next"  ref='lastname' onChangeText={ (text) => this.setState({ lastName: text }) } onSubmitEditing={(event) => { this.refs.password._root.focus(); }}   placeholder='Last Name' />
                       </Item>
                   </View>
                   <View style={[styles.devideTextbox,]}>
                       <Item regular style={{marginTop:(height/48),width:(width/2.49),height:42.95,backgroundColor:'white',borderRadius: RADIUS,marginRight:5,borderColor:'#ddd'}}>
                         <Input autoCorrect={false} style={styles.placeholderFont} returnKeyType="next"  ref='password' onChangeText={ (text) => this.setState({ password: text }) }  onSubmitEditing={(event) => { this.refs.conpassword._root.focus(); }}   placeholder='Password' secureTextEntry={true} />
                       </Item>
                       <Item regular style={{marginTop:(height/51.2),width:(width/2.50),height:42.95,backgroundColor:'white',borderRadius: RADIUS,borderColor:'#ddd'}}>
                         <Input autoCorrect={false} style={styles.placeholderFont} returnKeyType="next"  ref='conpassword' onChangeText={ (text) => this.setState({ conpassword: text }) } onSubmitEditing={(event) => { this.refs.address._root.focus(); }}   placeholder='Confirm Password' secureTextEntry={true} />
                       </Item>
                   </View>
                   <Item regular style={[styles.inputBox,{marginTop:(height/51.2),borderRadius: RADIUS}]}>
                     <Input autoCorrect={false} style={styles.placeholderFont} returnKeyType="next"  ref='address' onChangeText={ (text) => this.setState({ address: text }) }  onSubmitEditing={(event) => { this.refs.city._root.focus(); }}  placeholder='Street address'  />
                   </Item>
                   <View style={[styles.devideTextbox,{marginTop:5}]}>
                     <Item regular style={{marginTop:10,width:(width/3.5),height:42.95,backgroundColor:'white',borderRadius: RADIUS,borderColor:'#ddd'}}>
                       <Input autoCorrect={false} style={styles.placeholderFont}  ref='city'   onChangeText={ (text) => this.setState({ city: text }) } placeholder='City' />
                     </Item>
                     <View  style={{width:(width/4.5),marginRight:4,marginLeft:6,marginTop:10,}}>
                         <ModalPicker

                            data={States}
                            style={{height:42.95,}}
                            initValue="State"
                            onChange={(option)=>{
                              this.setState({state:option.label})
                              //console.log(this.refs.zip._root)
                              let self=this;
                              setTimeout(function(){  self.refs.zip._root.focus() }, 800);
                              //this.refs.zip._root.focus();
                             }}>
                           </ModalPicker>
                      </View>
                       <Item regular style={{marginTop:(height/76.8),width:(width/3.5),height:42.95,backgroundColor:'white',borderRadius: RADIUS,borderColor:'#ddd'}}>
                         <Input ref='zip' autoCorrect={false} style={styles.placeholderFont} returnKeyType="next" onChangeText={ (text) => this.setState({ zip: text }) } onSubmitEditing={(event) => { this.refs.phone._root.focus(); }}  placeholder='Zip' keyboardType = 'numeric' />
                       </Item>
                     </View>
                 <View style={[styles.devideTextbox,]}>
                     <Item regular style={{marginTop:(height/51.2),width:(width/2.49),height:42.95,backgroundColor:'white',borderRadius: RADIUS,marginRight:5,borderColor:'#ddd'}}>
                       <Input autoCorrect={false} style={styles.placeholderFont} returnKeyType="next" ref='phone' onChangeText={ this.onPhoneFormat.bind(this) } onSubmitEditing={(event) => { this.refs.email._root.focus(); }} value={this.state.phone}  placeholder='Phone Number' keyboardType = 'numeric' />
                     </Item>
                     <Item regular style={{marginTop:(height/51.2),width:(width/2.50),height:42.95,backgroundColor:'white',borderRadius: RADIUS,borderColor:'#ddd'}}>
                       <Input autoCorrect={false} style={styles.placeholderFont} autoCapitalize='none'  ref='email' onChangeText={ (text) => this.setState({ email: text }) } placeholder='Email Address' keyboardType = 'email-address'  />
                     </Item>
                 </View>
                 <View style={{flex:1,flexDirection:'row',width:(width/1.24),marginTop:(height/51.2),}}>
                     <ModalPicker
                        data={AgeOption}
                        style={{width:(width/5.87),marginRight:4,marginTop:2, borderRadius:RADIUS}}
                        initValue="Age"
                        onChange={(option)=>{
                          this.setState({age:option.label})
                          //console.log(option)
                         }}>
                     </ModalPicker>
                     <ModalPicker
                        data={IncomeOption}
                        style={{width:(width/4),marginRight:4,marginLeft:2,marginTop:2,borderRadius:RADIUS}}
                        initValue="Income"
                        onChange={(option)=>{
                          this.setState({income:option.label})
                          //console.log(option)
                         }}>
                       </ModalPicker>
                       <ModalPicker
                          data={MaritalStatus}
                          style={{width:(width/4.1),marginTop:2,marginRight:4,marginLeft:2,borderRadius:RADIUS}}
                          initValue="Marital status"
                          onChange={(option)=>{
                            this.setState({maritalStatus:option.label})
                            //console.log(option)
                           }}>
                         </ModalPicker>
                         <ModalPicker
                            data={GenderOption}
                            style={{width:(width/8), marginRight:2,marginTop:2,marginLeft:2,borderRadius:RADIUS}}
                            initValue="Gender"
                            onChange={(option)=>{
                              this.setState({sex:option.label})
                            //  console.log(option)
                             }}>
                         </ModalPicker>
                     {/*
                     Commented out for client requirement
                    <TouchableOpacity  onPress={()=>{this.selectAge('range1');}} ><View style={ this.state.pressStatus1 ? styles.selectedAgeRange : styles.ageRange }><Text style={{fontSize:20,marginTop:5}}>Under 30 </Text></View></TouchableOpacity>
                     <TouchableOpacity onPress={()=>{this.selectAge('range2');}} ><View style={this.state.pressStatus2 ? styles.selectedAgeRange : styles.ageRange}><Text style={{fontSize:20,marginTop:5}}>30-40 </Text></View></TouchableOpacity>
                     <TouchableOpacity onPress={()=>{this.selectAge('range3');}} ><View style={this.state.pressStatus3 ? styles.selectedAgeRange : styles.ageRange}><Text style={{fontSize:20,marginTop:5}}>40-50 </Text></View></TouchableOpacity>
                     <TouchableOpacity onPress={()=>{this.selectAge('range4');}} ><View style={this.state.pressStatus4 ? styles.selectedAgeRange : styles.ageRange}><Text style={{fontSize:20,marginTop:5}}>Over 50 </Text></View></TouchableOpacity>
                     */}
                 </View>
              {/*   <View style={{alignItems:'flex-start',marginLeft:-370,flexDirection:'row'}}>
                     <Label style={{marginTop:20,fontSize:20,marginRight:20,}}>Gender:</Label>
                      <View style={{marginTop:20}}>
                         <RadioForm
                          radio_props={radio_props}
                          initial={3}
                          buttonSize={15}
                          formHorizontal={true}
                          labelHorizontal={true}
                          buttonColor={'#c2c2c2'}
                          labelStyle={{left: -6,fontSize:20,marginTop:5}}
                          animation={true}
                          onPress={(value) => {this.setState({sex:value})}}
                        />
                  </View>
                </View> */}
              {/*  <View style={{flexDirection:'row',marginLeft:18}}>
                   <CheckBox checked={this.state.terms}
                             onPress={this.checkBoxChanged.bind(this)}
                             style={{marginTop:22,marginRight:15,borderColor:'#8c8c8c',borderWidth:3,width:20,height:20}} />
                   <Text style={{fontSize:18,flexWrap:'wrap',color: 'black',marginRight:20,marginTop:20}} >
                      Agree to
                      <Text  style={{color: '#FF007F',marginLeft:20,}} > Terms and Conditions
                      </Text>
                   </Text>
                 </View> */}
                 <View style={[styles.buttonPanel,{alignItems:'center',}]}>

                   <Button onPress={() => this.regiterSubmit(navigate)} style={styles.regBtn} rounded >
                      <Text style={styles.regText}>Register</Text>
                   </Button>
                   <View onPress={() => navigate('Login')} style={styles.loginLinkView} >
                      <Text onPress={() => navigate('Login')} style={styles.cancelButtonText}>Cancel</Text>
                   </View>
                  </View>
                </View>
             </Content>

      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    user: state.auth.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(registration, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationScreen);
