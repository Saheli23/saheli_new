import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity,
  AsyncStorage


} from 'react-native';
import * as answerAction from '../actions/quesans.action';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,} from 'native-base';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import Switch from 'react-native-customisable-switch';
import styles from '../styles/questionAnswerStyle';
var {height, width} = Dimensions.get('window');
var logo = require('../images/logo.png');
var Textlogo = require('../images/text_logo.png');
import { Images} from '../Themes';


class QuestionTwoPageScreen extends React.Component {
  constructor(props) {
    super(props);
    Text.defaultProps.allowFontScaling=false;
    this.state = {
      funeral:false,
      hospice:false,
      assisted_living:false,
      travel:false,
      real_estate:false,
      business:false,
      vehicles:false,
      moving_storage:false,
      charitable_giving:false,
      will:false,
      trust:false,
      insurance:false,
      userid:0,

    };
  }
  static navigationOptions = ({ navigation }) => ( {
    title:'',
    header:<View style={{height:(height/9),marginLeft:20,alignItems:'center'}}>
              <View style={{flexDirection:'row',marginTop:10}} >
                  <Image
                   style={styles.headerLogoPanel}
                   source={Images.modifiedLogo}/>
              </View>
            </View>,

  });
  componentWillMount(){
  //  console.log(this.props.navigation.state.params.answerDataSet);
    const { params } = this.props.navigation.state;
  //  console.log(params.answerDataSet);

     AsyncStorage.getItem('userDetails').then((value) =>{
       if(value){
       let userData = JSON.parse(value)
       console.log('userDetails',value);
       console.log('userDetails',userData.userid);
     this.setState({userid:userData.userid});
       }
     })
    this.setState({
              funeral:this.props.navigation.state.params.answerDataSet.funeral,
              hospice:this.props.navigation.state.params.answerDataSet.hospice,
              assisted_living:this.props.navigation.state.params.answerDataSet.assisted_living,
              travel:this.props.navigation.state.params.answerDataSet.travel,
              moving_storage:this.props.navigation.state.params.answerDataSet.moving_storage,
              charitable_giving:this.props.navigation.state.params.answerDataSet.charitable_giving,
            });


  }

  answerSubmit(navigate){
    //navigate('ThankYou');
    let answerData = {
            funeral:this.state.funeral,
            hospice:this.state.hospice,
            assisted_living:this.state.assisted_living,
            travel:this.state.travel,
            real_estate:this.state.real_estate,
            business:this.state.business,
            vehicles:this.state.vehicles,
            moving_storage:this.state.moving_storage,
            charitable_giving:this.state.charitable_giving,
            will:this.state.will,
            trust:this.state.trust,
            insurance:this.state.insurance

          };
    console.log('2nd page',answerData);
    console.log('userid',this.state.userid);
    this.props.actions.answerDataSubmit(answerData,this.state.userid);
    //navigate('ThankYou');

    //props call
  }



  render() {

        const activeButtonBackgroundColor = '#fff';
        const activeBackgroundColor       = '#A1C1E5';
        const activeTextColor             = '#000';//'#39414B';
        const inactiveBackgroundColor     = '#ddd';
        const inactiveTextColor           = '#444';
        const fontSize                    =     18;


        var DOES_DID = "Does";

        if ( this.state.funeral ){
             DOES_DID = "Did";
        }



        const { navigate } = this.props.navigation;
        // const { params } = this.props.navigation.state;
        // console.log(params);
    return (
      <View style={styles.container}>
        <Content>
           <View style={styles.mainLogoPanel}>
               {/*<View style={styles.logoPanel}>
                    <Image
                    style={{width: 90, height: 150,}}
                    source={logo}
                    />
                </View>*/}
            </View>
                <View style={styles.mainQuesPanel}>
                  <View style={{flexDirection:'row'}}>
                                <Text style={styles.questions}>{DOES_DID} your family member have Insurance?</Text>
                                <View style={styles.answerSwitch}>
                                  <Switch
                                      defaultValue={false}
                                      activeText={'Yes'}
                                      inactiveText={'No'}
                                      fontSize={fontSize}
                                      activeTextColor={activeTextColor}
                                      inactiveTextColor={inactiveTextColor}
                                      activeBackgroundColor={activeBackgroundColor}
                                      inactiveBackgroundColor={inactiveBackgroundColor}
                                      activeButtonBackgroundColor={activeButtonBackgroundColor}
                                      inactiveButtonBackgroundColor={'rgba(255, 255, 255, 1)'}
                                      switchWidth={85}
                                      switchHeight={40}
                                      switchBorderRadius={20}
                                      switchBorderColor={'rgba(0, 0, 0, 1)'}
                                      switchBorderWidth={0}
                                      buttonWidth={35}
                                      buttonHeight={35}
                                      buttonBorderRadius={20}
                                      buttonBorderColor={'rgba(0, 0, 0, 1)'}
                                      buttonBorderWidth={0}
                                      animationTime={150}
                                      padding={true}
                                      onChangeValue={(value) => {
                                        this.setState({insurance:value});
                                      }}
                                    />
                                  </View>
                              </View>
                            <View style={styles.eachQuestionSection}>
                    <Text style={styles.questions}>{DOES_DID} your family member own any Vehicles?</Text>
                      <View style={styles.answerSwitch}>
                          <Switch
                              defaultValue={false}
                              activeText={'Yes'}
                              inactiveText={'No'}
                              fontSize={fontSize}
                              activeTextColor={activeTextColor}
                              inactiveTextColor={inactiveTextColor}
                              activeBackgroundColor={activeBackgroundColor}
                              inactiveBackgroundColor={inactiveBackgroundColor}
                              activeButtonBackgroundColor={activeButtonBackgroundColor}
                              inactiveButtonBackgroundColor={'rgba(255, 255, 255, 1)'}
                              switchWidth={85}
                              switchHeight={40}
                              switchBorderRadius={20}
                              switchBorderColor={'rgba(0, 0, 0, 1)'}
                              switchBorderWidth={0}
                              buttonWidth={35}
                              buttonHeight={35}
                              buttonBorderRadius={20}
                              buttonBorderColor={'rgba(0, 0, 0, 1)'}
                              buttonBorderWidth={0}
                              animationTime={150}
                              padding={true}
                              onChangeValue={(value) => {
                                this.setState({vehicles:value});
                              }}
                            />
                        </View>
                    </View>

                          <View style={styles.eachQuestionSection}>
                              <Text style={styles.questions}>{DOES_DID} your family member own any Real Estate?</Text>
                              <View style={styles.answerSwitch}>
                                <Switch
                                    defaultValue={false}
                                    activeText={'Yes'}
                                    inactiveText={'No'}
                                    fontSize={fontSize}
                                    activeTextColor={activeTextColor}
                                    inactiveTextColor={inactiveTextColor}
                                    activeBackgroundColor={activeBackgroundColor}
                                    inactiveBackgroundColor={inactiveBackgroundColor}
                                    activeButtonBackgroundColor={activeButtonBackgroundColor}
                                    inactiveButtonBackgroundColor={'rgba(255, 255, 255, 1)'}
                                    switchWidth={85}
                                    switchHeight={40}
                                    switchBorderRadius={20}
                                    switchBorderColor={'rgba(0, 0, 0, 1)'}
                                    switchBorderWidth={0}
                                    buttonWidth={35}
                                    buttonHeight={35}
                                    buttonBorderRadius={20}
                                    buttonBorderColor={'rgba(0, 0, 0, 1)'}
                                    buttonBorderWidth={0}
                                    animationTime={150}
                                    padding={true}
                                    onChangeValue={(value) => {
                                      this.setState({real_estate:value});
                                      console.log(value);
                                    }}
                                  />
                                </View>
                            </View>
                            <View style={styles.eachQuestionSection}>
                                <Text style={styles.questions}>{DOES_DID} your family member own a Business?</Text>
                                <View style={styles.answerSwitch}>
                                  <Switch
                                      defaultValue={false}
                                      activeText={'Yes'}
                                      inactiveText={'No'}
                                      fontSize={fontSize}
                                      activeTextColor={activeTextColor}
                                      inactiveTextColor={inactiveTextColor}
                                      activeBackgroundColor={activeBackgroundColor}
                                      inactiveBackgroundColor={inactiveBackgroundColor}
                                      activeButtonBackgroundColor={activeButtonBackgroundColor}
                                      inactiveButtonBackgroundColor={'rgba(255, 255, 255, 1)'}
                                      switchWidth={85}
                                      switchHeight={40}
                                      switchBorderRadius={20}
                                      switchBorderColor={'rgba(0, 0, 0, 1)'}
                                      switchBorderWidth={0}
                                      buttonWidth={35}
                                      buttonHeight={35}
                                      buttonBorderRadius={20}
                                      buttonBorderColor={'rgba(0, 0, 0, 1)'}
                                      buttonBorderWidth={0}
                                      animationTime={150}
                                      padding={true}
                                      onChangeValue={(value) => {
                                        this.setState({business:value});
                                        console.log(value);
                                      }}
                                    />
                                  </View>
                              </View>
                        <View style={styles.eachQuestionSection}>
                          <Text style={styles.questions}>{DOES_DID} your family member have a Will?</Text>
                          <View style={styles.answerSwitch}>
                            <Switch
                                defaultValue={false}
                                activeText={'Yes'}
                                inactiveText={'No'}
                                fontSize={fontSize}
                                activeTextColor={activeTextColor}
                                inactiveTextColor={inactiveTextColor}
                                activeBackgroundColor={activeBackgroundColor}
                                inactiveBackgroundColor={inactiveBackgroundColor}
                                activeButtonBackgroundColor={activeButtonBackgroundColor}
                                inactiveButtonBackgroundColor={'rgba(255, 255, 255, 1)'}
                                switchWidth={85}
                                switchHeight={40}
                                switchBorderRadius={20}
                                switchBorderColor={'rgba(0, 0, 0, 1)'}
                                switchBorderWidth={0}
                                buttonWidth={35}
                                buttonHeight={35}
                                buttonBorderRadius={20}
                                buttonBorderColor={'rgba(0, 0, 0, 1)'}
                                buttonBorderWidth={0}
                                animationTime={150}
                                padding={true}
                                onChangeValue={(value) => {
                                  this.setState({will:value});
                                }}
                              />
                            </View>
                          </View>
                          <View style={styles.eachQuestionSection}>
                              <Text style={styles.questions}>{DOES_DID} your Family Member have a Trust?</Text>
                              <View style={styles.answerSwitch}>
                                <Switch
                                    defaultValue={false}
                                    activeText={'Yes'}
                                    inactiveText={'No'}
                                    fontSize={fontSize}
                                    activeTextColor={activeTextColor}
                                    inactiveTextColor={inactiveTextColor}
                                    activeBackgroundColor={activeBackgroundColor}
                                    inactiveBackgroundColor={inactiveBackgroundColor}
                                    activeButtonBackgroundColor={activeButtonBackgroundColor}
                                    inactiveButtonBackgroundColor={'rgba(255, 255, 255, 1)'}
                                    switchWidth={85}
                                    switchHeight={40}
                                    switchBorderRadius={20}
                                    switchBorderColor={'rgba(0, 0, 0, 1)'}
                                    switchBorderWidth={0}
                                    buttonWidth={35}
                                    buttonHeight={35}
                                    buttonBorderRadius={20}
                                    buttonBorderColor={'rgba(0, 0, 0, 1)'}
                                    buttonBorderWidth={0}
                                    animationTime={150}
                                    padding={true}
                                    onChangeValue={(value) => {
                                      this.setState({trust:value});
                                    }}
                                  />
                                </View>
                            </View>
                            <View style={styles.bottomPanel}>
                            <Text style={styles.pageNoText}>
                             Page 2 of 2
                             </Text>
                                <Button style={styles.continueBtn} rounded onPress={() =>  this.answerSubmit(navigate)}>
                                   <Text style={styles.btnText}  >Continue</Text>
                                 </Button>
                            </View>

                  </View>


          </Content>
      </View>

    );
  }
}
function mapStateToProps(state, ownProps) {
  return {
  //  user: state.auth.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(answerAction, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(QuestionTwoPageScreen);
