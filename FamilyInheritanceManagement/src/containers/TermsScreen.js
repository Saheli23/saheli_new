import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity,
  ScrollView


} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,CheckBox} from 'native-base';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import styles from '../styles/welcomeStyle';
import Toast, {DURATION} from 'react-native-easy-toast';
var {height, width} = Dimensions.get('window');
var logo = require('../images/logo.png');
var Textlogo = require('../images/text_logo.png');
var yScroll = 0;
var yScroll_max = Infinity;
import { Images} from '../Themes';



 class TermsScreen extends React.Component {
  constructor(props) {
    super(props);
    Text.defaultProps.allowFontScaling=false;
    this.state = {
     buttonText:"Continue",
     terms:false,
     showScrollButton: true
    };
  }
  static navigationOptions = {
    title:'',
    header:<View style={{height:(height/9),marginLeft:20,alignItems:'center'}}>
              <View style={{flexDirection:'row',marginTop:10}} >
                <Image
                 style={styles.headerLogoPanel}
                 source={Images.modifiedLogo}/>
                 </View>
            </View> ,

  };
   componentWillMount () {
    //console.log(this.props);
    console.log(this.props);
    //this.refs.toast.show('Thank you for registation',DURATION.LENGTH_SHORT);
  }
  componentWillReceiveProps (newProps) {
    console.log('newProps');
  }

  checkBoxChanged()
      {
      this.state.terms ? this.setState({terms: false}):this.setState({terms: true});
      }
  setYposition( y ){
          yScroll = y;
          console.log("scroll", yScroll);
          if ( y > yScroll_max - 220  &&    this.state.showScrollButton ){
               this.setState({showScrollButton: false});
          }
          if ( y < yScroll_max - 220  &&  ! this.state.showScrollButton ){
               this.setState({showScrollButton: true });
          }
          /*
          console.log(y);
          if ( y > 0 && this.state.showScrollButton ){

               this.state.showScrollButton = false;
               this.render();

               this.refs.scrollButtonView;
               this.refs.scrollButtonView.props.style.height = 0;
          }
          */
  }
  scrollButton(navigate){
          this.refs.scrollView.flashScrollIndicators();
          this.refs.scrollView.scrollTo({x:0,y:(yScroll + height/2)});
          return;
  }
  agreeTerms(navigate){
          if(!this.state.terms){
               this.refs.toast.show('Please check the box to Agree to Terms and Conditions',2500);
                  }
          else{
            navigate('Welcome')
          }
        }

  render() {
  //  this.refs.toast.show('Thank you for registation',DURATION.LENGTH_SHORT);
    //console.log(this.props);
        const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
            <Toast
               ref="toast"
               style={{backgroundColor:'#ff704d',zIndex:999,}}
               position='top'
               positionValue={10}
               fadeInDuration={750}
               fadeOutDuration={750}
               opacity={0.9}
               textStyle={{color:'white'}}
           />

               <View style={[styles.mainLogoPanel,]}>

                <Text style={[styles.largeText,]} >
                Terms and Conditions
                </Text>
                <ScrollView style={styles.contentPanelTerms}
                            showsVerticalScrollIndicator={true}
                            ref={ref => this.refs.scrollView = ref}
                            onMomentumScrollEnd={(event) => {
                               this.setYposition( event.nativeEvent.contentOffset.y );
                            }}
                            onScroll={(event) => {
                               this.setYposition( event.nativeEvent.contentOffset.y );
                            }}
                            onLayout={(event) => {
                              var {x, y, width, height} = event.nativeEvent.layout;
                              yScroll_max = height;
                              console.log("terms height",height);
                              console.log( event.nativeEvent.layout );
                            }}
                >
                  <Text style={styles.infoTextJustified} >

                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever , when an unknown printer took a galley of type it to make a type specimen book.
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever , when an unknown printer took a galley of type it to make a type specimen book.
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever , when an unknown printer took a galley of type it to make a type specimen book.
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever , when an unknown printer took a galley of type it to make a type specimen book.
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever , when an unknown printer took a galley of type it to make a type specimen book.
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever , when an unknown printer took a galley of type it to make a type specimen book.
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever , when an unknown printer took a galley of type it to make a type specimen book.
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever , when an unknown printer took a galley of type it to make a type specimen book.
                  </Text>
                <View style={{ justifyContent:'center'
                             , alignItems:'center',
                             }}>
                <View style={{flexDirection:'row',marginLeft:18}}>
                     <CheckBox checked={this.state.terms}
                               onPress={this.checkBoxChanged.bind(this)}
                               style={{marginTop:32,marginRight:19,borderColor:'#8c8c8c',borderWidth:3,width:25,height:25}} />
                     <Text style={{fontSize:25,flexWrap:'wrap',color: 'black',marginRight:20,marginTop:30}} >
                        Agree to
                        <Text  style={{color: 'black',marginLeft:20,}} > Terms and Conditions
                        </Text>
                     </Text>
                   </View>
                <View style={{alignItems:'center',marginTop:(height/50),marginBottom:(height/8)}}>
                    <Button onPress={this.agreeTerms.bind(this,navigate)} style={styles.termBtn} rounded dark>
                        <Text style={[styles.btnText, {fontSize:25}]}>
                        {this.state.buttonText}
                        </Text>
                    </Button>
                    <View onPress={() => navigate('Login')} style={styles.loginLinkView} >
                        <Text onPress={() => navigate('Login')} style={styles.cancelButtonText}>Cancel</Text>
                    </View>

                    {/*<Button onPress={() => navigate('Login')} style={{alignItems:'center',marginTop:10,height:60,width:250,justifyContent:'center'}} rounded dark>
                       <Text style={{color:'white',justifyContent:'center',fontSize:20}}>Logout</Text>
                    </Button>*/}
                  </View>
                  </View>
                </ScrollView>


                { this.state.showScrollButton &&
                <View style={{ marginTop:(-65/* - styles.bottomOffset*/),marginBottom:(0)
                             , marginRight:(width/10)
                             , alignSelf:     'flex-end'
                             , alignItems:    'flex-end'
                             , justifyContent:'flex-end'
                             }}
                      ref={ref => this.refs.scrollButtonView = ref}
                    >
                    <Button onPress={this.scrollButton.bind(this,navigate)}
                            style={[ styles.scrollBtn, { marginTop:0 }]}
                            rounded dark
                            >
                        <Text style={[styles.btnText, {fontSize:25, color:'#39414B'}]}>
                        ↓
                        </Text>
                    </Button>
                </View>
                }


                </View>


            </View>
    );

  }

}
function mapStateToProps(state, ownProps) {
  console.log(state);
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //actions: bindActionCreators(registration, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TermsScreen);
