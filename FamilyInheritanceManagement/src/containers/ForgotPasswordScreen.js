import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image
} from 'react-native';
import { addNavigationHelpers,StackNavigator } from 'react-navigation';
import { Container, Header, Content, Form, Item, Input,Label,Button,Spinner } from 'native-base';
import Toast, {DURATION} from 'react-native-easy-toast';
var logo = require('../images/logo.png');
var {height, width} = Dimensions.get('window');
import { Images} from '../Themes';

export default class ForgotPasswordScreen extends React.Component {
  constructor(props) {
    super(props);
    Text.defaultProps.allowFontScaling=false;
    this.state = {
      email:'',
    isLoading:false,
    };
  }
  static navigationOptions = {
    title: '',
    headerTintColor: '#ddd',
    header:null,


  };
  render() {
    const { navigate } = this.props.navigation;
    console.log(this.props.navigation);
    const  RADIUS = 6;
    return (
      <View style={{backgroundColor:'#f2f2f2',height:height}}>
            <Toast
               ref="toast"
               style={{backgroundColor:'#ff704d'}}
               position='top'
               positionValue={50}
               fadeInDuration={750}
               fadeOutDuration={750}
               opacity={0.9}
               textStyle={{color:'white'}}
           />
            <Content>

               <View style={{alignItems:'center',marginTop:(height/9)}}>
                 <View style={{flexDirection:'row', flex:1,alignItems:'center',justifyContent: 'center',}}>

                   {/*<Image
                     style={{width: 80, height: 130,}}
                     source={logo} />
                   <Text style={{fontSize:40,marginBottom:5,fontFamily:'Roboto',fontWeight: '400',}} >
                   Family Inheritance Management</Text>*/}
                   <Image
                   source={Images.largeLogo}
                  />
                  </View>
                 {this.state.isLoading &&
                   <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: -20, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
                   <Spinner color='#737373' />
                   </View>
                 }
                <Item regular style={{marginTop:90,width:(width/1.28),height:60,alignItems:'center',backgroundColor:'white',borderRadius: 3,borderRadius: RADIUS}}>
                   <Input placeholder='Email' autoCapitalize='none' keyboardType = 'email-address' returnKeyType="done"  ref='email' onChangeText={ (text) => this.setState({ email: text }) } />
                 </Item>
                 <View style={{alignItems:'center',marginTop:15}}>
                   <Button style={{alignItems:'center',marginTop:30,height:60,width:250,justifyContent:'center',backgroundColor:'#A1C1E5'}} rounded light>
                      <Text style={{color:'white',justifyContent:'center',fontSize:25}}  >Reset Password</Text>
                   </Button>
                   <Button onPress={() => navigate('Login')} style={{alignItems:'center',marginTop:20,height:60,width:250,justifyContent:'center',borderColor:'#C22E93',borderWidth: 1,backgroundColor:'#C24A9A'}} rounded light>
                      <Text style={{color:'black',justifyContent:'center',fontSize:20}}>Back to Login</Text>
                   </Button>

                  </View>
              </View>
             </Content>

      </View>);
  }
}
