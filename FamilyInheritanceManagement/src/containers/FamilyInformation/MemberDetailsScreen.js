import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity,
  AsyncStorage,
  FlatList,
  ScrollView


} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,ActionSheet,Left,Icon} from 'native-base';
import * as updateFamilyMember from '../../actions/familyinformation.action';
import { StackNavigator } from 'react-navigation';
import { Switch } from 'react-native-switch';
import styles from '../../styles/FamilyInformationStyle';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { Images} from '../../Themes'
import { Col, Row, Grid } from "react-native-easy-grid";
var {height, width} = Dimensions.get('window');
var logo = require('../../images/text_logo.png');
const FONT_SIZE = 20;

 class MemberDetailsScreen extends React.Component {
   constructor(props) {
     super(props);
     Text.defaultProps.allowFontScaling=false;
     this.state = {
       userName : '',
       userid   : ''

     };
   }
  static navigationOptions = ({ navigation }) => ({
    title:<Text>Family Inheritance Management</Text>,
    headerMode: 'screen',
    headerTintColor: '#ddd',
    header: <View style={styles.mainHeaderPanel}>
              <View style={styles.subHeader} >

                <Button style={styles.backBtn} transparent onPress={() => navigation.goBack()}>
                {/*<Icon name="arrow-back" />*/}
                <Image
                   style={styles.backImage}
                   source={Images.backArrow}/>
                  <Text style={styles.backText}>Back</Text>
                </Button>
                {/*<Image
                 style={{width: width/2.9, height: 60,}}
                 source={logo}/>*/}
                 <Image
                 style={styles.logoPanel}
                 source={Images.modifiedLogo}/>
                 <View style={styles.mainMenuPanel}>
                   <TouchableOpacity onPress={() =>  navigation.navigate('UserProfile')}>
                     <Image
                      style={styles.userIconStyle}
                      source={Images.userIcon}/>
                    </TouchableOpacity>
                    <Image
                     style={styles.menuIconStyle}
                     source={Images.menuIcon}/>
                    </View>
               </View>
            </View> ,


  });
  componentWillMount(){

    AsyncStorage.getItem('userDetails').then((value) =>{
      if(value){
      let userData = JSON.parse(value)
      console.log('userDetails',value);
      console.log('userDetails',userData.userid);
    this.setState({userid:userData.userid});
  //  this.props.actions.workspaceInfo(userData.userid);
      }
    });
    //console.log('userid',this.state.userid);

    //  console.log('newPropsWorkspace',width,height);
    //  console.log('user name',this.state.userName);
  }

  componentWillReceiveProps (newProps) {
    console.log('newPropsWorkspace');
    if(newProps && newProps.response){

      console.log(newProps.response);
    }

  }

  render() {
       console.log('workspaceprops',this.props)
        const { navigate } = this.props.navigation;
        const { params } = this.props.navigation.state;
    return (
     <View style={styles.container}>
        <Content scrollEnabled={false}>

         <View style={styles.workspaceContainer}>
             <View style={styles.centerLabel}>
               <Text style={styles.centerLabelText}>Family Information</Text>
             </View>
             <View style={styles.memberInformation}>
               <View style={[styles.memberInnerContain,]}>
                   <Text style={[styles.memberName,]}>{params.memberInfo.membername}</Text>
                   <Text style={[styles.memberRelation,{marginLeft:(width/34.13),marginTop:(height/256)}]}>{params.memberInfo.relation}</Text>
                   <View style={{flexDirection:'row',marginLeft:(width/2.2)}}>
                     <TouchableOpacity onPress={() =>navigate('EditMemberDetails', { memberInfo: params.memberInfo })}>
                       <Image
                        style ={styles.editIcons}
                        source={Images.editIcon}
                       />
                     </TouchableOpacity>
                     <TouchableOpacity onPress={() => this.props.actions.deleteFamilyMember(this.state.userid,params.memberInfo.memberid)}>
                         <Image
                          style ={[styles.deleteIcons,{marginLeft:(width/68.26)}]}
                          source={Images.deleteIcon}
                         />
                      </TouchableOpacity>
                     </View>
               </View>
               <View style={styles.birthDayPanel}>
                   <Image
                    style ={[styles.birthDateIcon,]}
                    source={Images.birthDateIcon}
                   />
                   <Text style={styles.detailsText}>{params.memberInfo.birthday}</Text>
               </View>
               <View style ={styles.underLine} />
               <View style={styles.fullDetails}>
                   <View style={[styles.innerDetailsFirst,]}>
                     <View style={{flexDirection:'column',width:(width/2.28),}}>
                        <View style={{flexDirection:'row',marginBottom:(width/51.2)}}>
                         <Image
                          style ={[styles.locationIcons,]}
                          source={Images.blueLocationIcon}
                         />
                         <Text style={[styles.detailsText,{fontWeight:'800',marginLeft:(width/68.26),}]}>Address:</Text>
                         <Text style={[styles.detailsText,{marginLeft:(width/204.8),flexWrap:'wrap',width:(width/3)}]}>{params.memberInfo.address}</Text>
                         </View>
                         <View style={{flexDirection:'row',}}>
                           <Image
                            style ={[styles.bluePhoneIcon,]}
                            source={Images.bluePhoneIcon}
                           />
                           <Text style={[styles.detailsText,{fontWeight:'800',marginLeft:(width/85.33)}]}>Phone:</Text>
                           <Text style={[styles.detailsText,{marginLeft:(width/204.8),flexWrap:'wrap'}]}>{params.memberInfo.phone}</Text>
                         </View>
                     </View>
                     <View style={{flexDirection:'column',width:(width/2.50)}}>
                       <View style={{flexDirection:'row',marginBottom:(width/51.2)}}>
                         <Image
                          style ={[styles.blueMailIcon,{marginTop:(width/204.8),}]}
                          source={Images.blueMailIcon}
                         />
                         <Text style={[styles.detailsText,{fontWeight:'800',marginLeft:(width/85.33)}]}>Email:</Text>
                         <Text style={[styles.detailsText,{marginLeft:(width/204.8),flexWrap:'wrap'}]}>{params.memberInfo.email}</Text>
                        </View>
                         <View style={{flexDirection:'row',}}>
                           <Image
                            style ={[styles.blueSSNIcon,{marginTop:(width/204.8)}]}
                            source={Images.blueSSNIcon}
                           />
                           <Text style={[styles.detailsText,{fontWeight:'800',marginLeft:(width/64)}]}>SSN:</Text>
                           <Text style={[styles.detailsText,{flexWrap:'wrap'}]}>{params.memberInfo.ssn}</Text>
                         </View>
                     </View>


                   </View>

                   <View style={styles.noteSection}>
                   {params.memberInfo.note !='' &&
                   <Text style={[styles.detailsText,]}>{params.memberInfo.note}</Text>
                 }
                   </View>
               </View>
             </View>

        </View>
        <View style={{alignItems:'center',marginLeft:(width/2.60),marginTop:(height/180)}}>
          <Button onPress={() =>navigate('AddFamilyMember')}  style={{alignItems:'center',marginTop:10,height:(height/18),width:(width/4),justifyContent:'center',backgroundColor:'#6791C1',}} rounded light>
             <Text style={{color:'white',justifyContent:'center',fontSize:20}}>Add Another Member</Text>
          </Button>
        </View>
      </Content>
    </View>
    );
  }
}
function mapStateToProps(state, ownProps) {
  console.log(state);
  return {
    // nav:  state.nav,
    // user: state.auth.user,
    // workspaceInfo: state.auth.response
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(updateFamilyMember, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MemberDetailsScreen);
