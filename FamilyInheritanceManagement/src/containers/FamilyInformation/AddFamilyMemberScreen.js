import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity,
  AsyncStorage,
  FlatList,
  ScrollView


} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,ActionSheet,Left,Icon,Spinner} from 'native-base';
 import * as addFamilyMember from '../../actions/familyinformation.action';
import { StackNavigator } from 'react-navigation';
import { Switch } from 'react-native-switch';
import styles from '../../styles/FamilyInformationStyle';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { Images} from '../../Themes'
import { Col, Row, Grid } from "react-native-easy-grid";
import Toast, {DURATION} from 'react-native-easy-toast';
import * as EmailValidator from 'email-validator';
import States from '../../_global/States';
import ModalPicker from '../../library/react-native-modal-picker';
import DatePicker from '../../library/react-native-datepicker';
var {height, width} = Dimensions.get('window');
var logo = require('../../images/text_logo.png');

const FONT_SIZE = 20;

 class AddFamilyMemberScreen extends React.Component {
   constructor(props) {
     super(props);
     Text.defaultProps.allowFontScaling=false;
     this.state = {
       userName : '',
       userid   : '',
       membername:'',
       relation:'',
       ssn:'',
       note:'',
       address  :'',
       city     :'',
       state    :'',
       zip      :'',
       phone    :'',
       email    :'',
       birthday:'',
       maxDate:new Date(),
       isLoading:false,
       personalInfoPanel:true,
       addressPanel:false,
       notePanel:false

     };
   }
  static navigationOptions = ({ navigation }) => ({
    title:<Text>Family Inheritance Management</Text>,
    headerMode: 'screen',
    headerTintColor: '#ddd',
    header: <View style={styles.mainHeaderPanel}>
              <View style={styles.subHeader} >

                <Button style={styles.backBtn} transparent onPress={() => navigation.navigate('FamilyInformation')}>
                {/*<Icon name="arrow-back" />*/}
                <Image
                   style={styles.backImage}
                   source={Images.backArrow}/>
                  <Text style={styles.backText}>Back</Text>
                </Button>
                {/*<Image
                 style={{width: width/2.9, height: 60,}}
                 source={logo}/>*/}
                 <Image
                 style={styles.logoPanel}
                 source={Images.modifiedLogo}/>
                 <View style={styles.mainMenuPanel}>
                 <TouchableOpacity onPress={() =>  navigation.navigate('UserProfile')}>
                   <Image
                    style={styles.userIconStyle}
                    source={Images.userIcon}/>
                  </TouchableOpacity>
                  <Image
                   style={styles.menuIconStyle}
                   source={Images.menuIcon}/>
                    </View>
               </View>
            </View> ,


  });
  componentWillMount(){

    AsyncStorage.getItem('userDetails').then((value) =>{
      if(value){
      let userData = JSON.parse(value)
      console.log('userDetails',value);
      console.log('userDetails',userData.userid);
    this.setState({userid:userData.userid});
  //  this.props.actions.workspaceInfo(userData.userid);
      }
    });
    //console.log('userid',this.state.userid);

    //  console.log('newPropsWorkspace',width,height);

  }

  componentWillReceiveProps (newProps) {
    console.log('newPropsAddFamilyInfo',newProps);
    if(newProps && newProps.addMemberStatus && newProps.addMemberStatus.addFamilyMember == 'success'  ){
         this.refs.toast.show('Member added successfully!',2000);
         this.setState({
           isLoading:false,
           membername:'',
           relation:'Relation',
           ssn:'',
           note:'',
           address:'',
           city:'',
           state:'',
           zip:'',
           phone:'',
           email:'',
           birthday:''
           });
      console.log(newProps.addMemberStatus);
    }

  }

  onSSNFormat(val){
    console.log(val);
    value = val
       .match(/\d*/g).join('')
       .match(/(\d{0,3})(\d{0,2})(\d{0,4})/).slice(1).join('-')
       .replace(/-*$/g, '')
   ;
    this.setState({ ssn: value })

  }
  onPhoneFormat(val){
    console.log(val);
    value = val
       .match(/\d*/g).join('')
       .match(/(\d{0,3})(\d{0,3})(\d{0,4})/).slice(1).join('-')
       .replace(/-*$/g, '')
   ;
    this.setState({ phone: value })

  }

  addFamilyMemberSubmit(navigate,samepage){

     const {userid,membername,relation,ssn,note, address, city, state,zip,phone,email,birthday} = this.state;
     let emailValidate = EmailValidator.validate(email);
      if(membername == ""){
           this.refs.toast.show('Please provide Full Name!',DURATION.LENGTH_SHORT);
         }
      else if(relation ==""){
          this.refs.toast.show('Please select relation!',DURATION.LENGTH_SHORT);
      }
      // else if(state ==""){
      //     this.refs.toast.show('Please select state!',DURATION.LENGTH_SHORT);
      // }
      else if(email == ""){
           this.refs.toast.show('Please provide Email Address!',DURATION.LENGTH_SHORT);
         }
      else if(!emailValidate){
           this.refs.toast.show('Please provide Valid Email Address!',DURATION.LENGTH_SHORT);
         }
      else{
           this.setState({isLoading:true});
           this.props.actions.addFamilyMember(userid,membername,relation,ssn,note, address, city, state,zip,phone,email,birthday,samepage);

         }
   }

  render() {
      // console.log('workspaceprops',this.props)
        const { navigate } = this.props.navigation;
        let relationkey = 0;
        const RelationData = [
            { key: relationkey++, label: 'Grandfather' },
            { key: relationkey++, label: 'Grandmother' },
            { key: relationkey++, label: 'Father' },
            { key: relationkey++, label: 'Mother' },
            { key: relationkey++, label: 'Wife' },
            { key: relationkey++, label: 'Sister' },
            { key: relationkey++, label: 'Brother' },

        ];
    return (
     <View style={styles.container}>

        <Content scrollEnabled={false}>

         <View style={[styles.workspaceContainer,{height:height}]}>
             <View style={{margin:22,marginBottom:34}}>
               <Text style={{fontSize:25,fontWeight:'600'}}>Family Information</Text>
             </View>
             <Toast
                ref="toast"
                style={{backgroundColor:'#ff704d',zIndex:999,}}
                position='top'
                positionValue={50}
                fadeInDuration={750}
                fadeOutDuration={750}
                opacity={0.9}
                textStyle={{color:'white'}}
            />
             <ScrollView>
             {this.state.isLoading &&
               <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
               <Spinner color='#737373' size='large' />
               </View>
             }
             <View style={styles.expandablePanel}>
                <Text style={styles.panelText}>Personal Information</Text>
                {(this.state.personalInfoPanel) ?
                (<TouchableOpacity onPress={()=>{ this.setState({personalInfoPanel:false})}}>
                  <Image
                   style={styles.arrowIcon}
                   source={Images.upArrow}
                 />
               </TouchableOpacity>)
               :
               (<TouchableOpacity onPress={()=>{ this.setState({personalInfoPanel:true})}}>
                 <Image
                  style={styles.arrowIcon}
                  source={Images.downArrow}
                />
              </TouchableOpacity>)}

             </View>
             <View style={styles.sepLine}>
                 <Image
                  style={{width:(width/1.1),}}
                  source={Images.sepLine}
                />
               </View>
             {this.state.personalInfoPanel &&
             <View style={[styles.devideTextbox,]}>
                 <Item regular style={styles.inputBox}>
                   <Input style={styles.placeholderFont} returnKeyType="next" value={this.state.membername}   onChangeText={ (text) => this.setState({ membername: text }) } placeholder='Name ( First, Middle, Last)' />
                 </Item>

                 <Item regular style={styles.inputBox}>
                 <ModalPicker
                     data={RelationData}
                     style={{height:253,width:(width/2.32),marginTop:210,marginRight:10,marginLeft:2}}
                     initValue="Relation"
                     onChange={(option)=>{
                       this.setState({relation:option.label})
                       console.log(option)
                      }}>
                    </ModalPicker>
                 </Item>
             </View>
           }
            {this.state.personalInfoPanel &&
             <View style={[styles.devideTextbox,]}>
                 <Item regular style={styles.inputBox}>
                   {/*<Input style={styles.placeholderFont} returnKeyType="next"  ref='password' onSubmitEditing={(event) => { this.refs.conpassword._root.focus(); }}  onChangeText={ (text) => this.setState({ password: text }) } placeholder='Birthday' />*/}
                   <DatePicker
                       style={{width: 450,height:90,marginTop:40,alignItems:'flex-start',marginLeft:5}}
                       date={this.state.birthday}
                       mode="date"
                       placeholder="Birthdate"
                       format="MM/DD/YYYY"

                       maxDate={this.state.maxDate}
                       confirmBtnText="Confirm"
                       cancelBtnText="Cancel"
                       customStyles={{
                         dateIcon: {
                          // position: 'absolute',
                           //left: 0,
                            marginTop: 5,
                           //marginLeft: 0
                         },
                         dateInput: {
                           marginLeft: 5,
                           borderColor: 'white',


                         },

                         // ... You can check the source to find the other keys.
                       }}
                       onDateChange={(date) => {this.setState({birthday: date})}}
           />
                 </Item>
                 <Item regular style={styles.inputBox}>
                  <Input style={styles.placeholderFont} returnKeyType="next" value={this.state.ssn}   onSubmitEditing={(event) => { this.refs.phone._root.focus(); }} onChangeText={ this.onSSNFormat.bind(this) } placeholder='Social Security Number' keyboardType = 'numeric' />
                  </Item>
             </View>
           }
            {this.state.personalInfoPanel &&
             <View style={[styles.devideTextbox,]}>
                 <Item regular style={styles.inputBox}>
                   <Input style={styles.placeholderFont} returnKeyType="next" ref='phone' value={this.state.phone}  onSubmitEditing={(event) => { this.refs.email._root.focus(); }} onChangeText={ this.onPhoneFormat.bind(this) } placeholder='Phone Number' keyboardType = 'numeric' />
                  {/* <TextInputMask style={styles.placeholderFont} returnKeyType="next" ref='phone' value={this.state.phone}  onSubmitEditing={(event) => { this.refs.email._root.focus(); }}
                     refInput={ref => { this.input = ref }}
                     onChangeText={(formatted, extracted) => {
                     console.log(formatted) // +1 (123) 456-78-90
                     console.log(extracted) // 1234567890
                     this.setState({ phone: formatted })
                     }}
                     mask={"[000]-[000]-[0000]"}
                    placeholder=' Phone Number' placeholderTextColor="#595959" keyboardType = 'numeric' /> */}
                 </Item>
                 <Item regular style={styles.inputBox}>
                   <Input style={styles.placeholderFont} autoCapitalize='none'  ref='email' value={this.state.email}   onChangeText={ (text) => this.setState({ email: text }) } placeholder='Email Address' keyboardType = 'email-address'  />
                 </Item>
             </View>
             }
             <View style={styles.expandablePanel}>
                <Text style={styles.panelText}>Address</Text>
                {(this.state.addressPanel) ?
                (<TouchableOpacity onPress={()=>{ this.setState({addressPanel:false})}}>
                  <Image
                   style={styles.arrowIcon}
                   source={Images.upArrow}
                 />
               </TouchableOpacity>)
               :
               (<TouchableOpacity onPress={()=>{ this.setState({addressPanel:true})}}>
                 <Image
                  style={styles.arrowIcon}
                  source={Images.downArrow}
                />
              </TouchableOpacity>)}

             </View>
             <View style={styles.sepLine}>
                 <Image
                  style={{width:(width/1.1),}}
                  source={Images.sepLine}
                />
               </View>
               {this.state.addressPanel &&
             <View style={[styles.devideTextbox,]}>
                 <Item regular style={styles.inputBox}>
                   <Input style={styles.placeholderFont} returnKeyType="next"  ref='address' value={this.state.address} onSubmitEditing={(event) => { this.refs.city._root.focus(); }}  onChangeText={ (text) => this.setState({ address: text }) } placeholder='Address'  />
                 </Item>
                 <Item regular style={styles.inputBox}>
                   <Input style={styles.placeholderFont}  ref='city' value={this.state.city}    onChangeText={ (text) => this.setState({ city: text }) } placeholder='City' />
                 </Item>
             </View>
            }
            {this.state.addressPanel &&
             <View style={[styles.devideTextbox,]}>
                <Item regular style={styles.inputBox}>
                  <ModalPicker
                      data={States}
                      style={{height:253,width:(width/2.22),marginTop:209,marginRight:10,marginLeft:1,}}
                      initValue="State"
                      onChange={(option)=>{
                        this.setState({state:option.label})
                        console.log(option)
                       }}>
                     </ModalPicker>
                  </Item>
                  <Item regular style={styles.inputBox}>
                    <Input style={styles.placeholderFont}  value={this.state.zip}   onChangeText={ (text) => this.setState({ zip: text }) } placeholder='Zip' keyboardType = 'numeric' />
                 </Item>
             </View>
            }
            <View style={styles.expandablePanel}>
               <Text style={styles.panelText}>Note</Text>
               {(this.state.notePanel) ?
               (<TouchableOpacity onPress={()=>{ this.setState({notePanel:false})}}>
                 <Image
                  style={styles.arrowIcon}
                  source={Images.upArrow}
                />
              </TouchableOpacity>)
              :
              (<TouchableOpacity onPress={(event)=>{ this.setState({notePanel:true});}}>
                <Image
                 style={styles.arrowIcon}
                 source={Images.downArrow}
               />
             </TouchableOpacity>)}

            </View>
            <View style={styles.sepLine}>
                <Image
                 style={styles.sepLineIcon}
                 source={Images.sepLine}
               />
              </View>

              { this.state.notePanel &&
             <Item regular style={styles.noteInputBox}>
               <Input style={styles.placeholderFont} returnKeyType="done"  ref='note' value={this.state.note}  onChangeText={ (text) => this.setState({ note: text }) } placeholder='Note'  />
             </Item>
             }
             <View style={{height:50}}></View>
               <View style={{alignItems:'center',marginLeft:(width/2.60),marginTop:(height/130),flexDirection:'row'}}>
               <Button onPress={()=> navigate('FamilyInformation')}   style={{left:-150,alignItems:'center',marginTop:10,height:(height/18),width:(width/8),justifyContent:'center',backgroundColor:'white',borderColor:'#6791C1',borderWidth:1}} rounded light>
                  <Text style={{color:'black',justifyContent:'center',fontSize:20}}>Cancel</Text>
               </Button>
               <Button onPress={() => this.addFamilyMemberSubmit(navigate,true)} style={{left:-135,alignItems:'center',marginTop:10,height:(height/18),width:(width/8),justifyContent:'center',backgroundColor:'#464348',}} rounded light>
                  <Text style={{color:'white',justifyContent:'center',fontSize:20}}>Save</Text>
               </Button>
                 <Button onPress={() => this.addFamilyMemberSubmit(navigate,false)}  style={{left:-120,alignItems:'center',marginTop:10,height:(height/18),width:(width/4),justifyContent:'center',backgroundColor:'#464348',}} rounded light>
                    <Text style={{color:'white',justifyContent:'center',fontSize:20}}>Save and Add Another</Text>
                 </Button>
               </View>
               <View style={{height:height/6}}></View>
             </ScrollView>

        </View>

      </Content>
    </View>
    );
  }
}
function mapStateToProps(state, ownProps) {
  console.log(state);
  return {
    // nav:  state.nav,
    // user: state.auth.user,
     addMemberStatus: state.auth.addmember
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(addFamilyMember, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddFamilyMemberScreen);
