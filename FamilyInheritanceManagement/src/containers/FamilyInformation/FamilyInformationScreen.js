import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity,
  AsyncStorage,
  FlatList,
  ScrollView


} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,ActionSheet,Left,Icon,Spinner} from 'native-base';
import * as showFamilyMember from '../../actions/familyinformation.action';
import { StackNavigator } from 'react-navigation';
import { Switch } from 'react-native-switch';
import styles from '../../styles/FamilyInformationStyle';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { Images} from '../../Themes'
import { Col, Row, Grid } from "react-native-easy-grid";
import AlertMessage from '../../components/AlertMessage'
var {height, width} = Dimensions.get('window');
var logo = require('../../images/text_logo.png');
const FONT_SIZE = 20;
import Table from '../../library/react-native-simple-table'
const columns = [
  {
    title: 'Name',
    dataIndex: 'membername',
    width: 105,
    sortable:true

  },
  {
    title: 'Relation',
    dataIndex: 'relation',
    width: 80,
  },
  {
    title: 'Address',
    dataIndex: 'address',
    width: 140,

  },
  {
    title: 'City',
    dataIndex: 'city',
    width: 140,

  },
  {
    title: 'State',
    dataIndex: 'state',
    width: 80,

  },
  {
    title: 'Birthday',
    dataIndex: 'birthday',
    width: 140,

  },
  {
    title: 'Phone',
    dataIndex: 'phone',
    width: 140,

  },
  {
    title: 'Email',
    dataIndex: 'email',
    width: 140,

  }
];

 class FamilyInformationScreen extends React.Component {
   constructor(props) {
     super(props);
     Text.defaultProps.allowFontScaling=false;
     this.state = {
       userName : '',
       userid   : '',
       dataSource:[],
       isTableView:false,
       gridImageSource:'',
       isFetching:true,

     };
   }
  static navigationOptions = ({ navigation }) => ({
    title:<Text>Family Inheritance Management</Text>,
    headerMode: 'screen',
    headerTintColor: '#ddd',
    header: <View style={styles.mainHeaderPanel}>
              <View style={styles.subHeader} >

                <Button style={styles.backBtn} transparent onPress={() =>  navigation.navigate('Workspace')}>
                    {/*<Icon name="arrow-back" />*/}
                    <Image
                       style={styles.backImage}
                       source={Images.backArrow}/>
                     <Text style={styles.backText}>Back</Text>
                </Button>
                {/*<Image
                 style={{width: width/2.9, height: 60,}}
                 source={logo}/>*/}
                 <Image
                 style={styles.logoPanel}
                 source={Images.modifiedLogo}/>
                 <View style={styles.mainMenuPanel}>
                 <TouchableOpacity onPress={() =>  navigation.navigate('UserProfile')}>
                   <Image
                    style={styles.userIconStyle}
                    source={Images.userIcon}/>
                  </TouchableOpacity>
                    <Image
                     style={styles.menuIconStyle}
                     source={Images.menuIcon}/>
                    </View>
               </View>
            </View> ,


  });
  componentWillMount(){

    AsyncStorage.getItem('userDetails').then((value) =>{
      if(value){
      let userData = JSON.parse(value)
      console.log('userDetails',value);
      console.log('userDetails',userData.userid);
      this.setState({userid:userData.userid});
      this.props.actions.getFamilyMember(userData.userid);
      }
    });
    //console.log('userid',this.state.userid);

    //  console.log('newPropsWorkspace',width,height);
    //  console.log('user name',this.state.userName);
  }

  componentWillReceiveProps (newProps) {
    console.log('newPropsfamilyInformation');
    if(newProps && newProps.familyInfo){
      this.setState({

        dataSource: newProps.familyInfo.familyInformation,
        isFetching:false,
      })
      // console.log(newProps.response);
    }

  }
  //  data={[
  //    {key: 'Devin Doe',relation:'Grandfather',address:'44, Shirley Ave, West Chicago, FL 32904',phone:'335 505 1786',email:'devin@gmail.com'},
  //    {key: 'Jackson Doe',relation:'Father',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com'},
  //    {key: 'James Doe',relation:'Brother',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com'},
  //    {key: 'Joel Doe',relation:'Grandmother',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com'},
  //    {key: 'John Doe',relation:'Brother',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com'},
  //    {key: 'Jillian Doe',relation:'Sister',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com'},
  //    {key: 'Jimmy Doe',relation:'Mother',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com'},
  //    {key: 'Julie Doe',relation:'Wife',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com'},
  //    {key: 'Ray Doe',relation:'Brother',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com'},
  //    {key: 'Gan Doe',relation:'Sister',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com'},
  //    {key: 'Van Doe',relation:'Brother',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com'},
  //    {key: 'Dan Doe',relation:'Brother',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com'},
   //
  //  ]}
  noRowData () {
    return this.state.dataSource.length === 0
  }
  manageView(view){
    if(view == 'table'){
      this.setState({
        isTableView:true,
      });
    }
    else{
      this.setState({
        isTableView:false,
      });
    }

  }
  _keyExtractor = (item, index) => item.memberid;

  render() {
       console.log('workspaceprops',this.props)
        const { navigate } = this.props.navigation;
    return (
     <View style={styles.container}>
        <Content scrollEnabled={false}>
        {this.state.isFetching &&
          <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
          <Spinner color='#737373' size='large' />
          </View>
        }
         <View style={styles.workspaceContainer}>
             <View style={styles.centerLabel}>
               <Text style={styles.centerLabelText}>Family Information</Text>
               { (this.state.dataSource.length >0) &&

               <TouchableOpacity onPress={() =>this.manageView('grid')} >
                {!this.state.isTableView &&
                 <Image
                  style ={styles.gridViewIcon}
                  source={Images.gridViewIcon}
                 />
                }
                {this.state.isTableView &&
                 <Image
                  style ={styles.gridViewIcon}
                  source={Images.gridViewDisableIcon}
                 />
                }
               </TouchableOpacity>
             }
             { (this.state.dataSource.length >0) &&
               <TouchableOpacity onPress={() =>this.manageView('table')} >
                 {this.state.isTableView &&
                 <Image
                  style ={styles.tableViewIcon}
                  source={Images.tableViewIcon}
                 />
                 }
                 {!this.state.isTableView &&
                   <Image
                    style ={styles.tableViewIcon}
                    source={Images.tableViewDisableIcon}
                   />
                   }
              </TouchableOpacity>
             }
             </View>
             { this.state.dataSource.length === 0 &&
         <AlertMessage title='No family member added yet' show={this.noRowData()} />
      }
      {!this.state.isTableView &&
         <FlatList numColumns={3} contentContainerStyle={{flexDirection: 'column',


        }}

           data={this.state.dataSource}
           keyExtractor={this._keyExtractor}
           renderItem={({item}) =>
                      <View style={{backgroundColor: 'white',
                        margin:10,
                        width: (width/3.41),
                        height: (height/2.25)}}>
                          <View style ={styles.memberDetails}>
                            <Text numberOfLines={1} style ={styles.memberName}>{item.membername}</Text>
                            <Text style ={styles.memberRelation}>{item.relation}</Text>
                            <View style ={styles.sepLine} />
                            {item && item.address !='' &&
                            <View style ={styles.addressPanel}>
                              <Image
                               style ={styles.locationIcons}
                               source={Images.locationIcon}
                              />
                               <Text numberOfLines={2} style={[styles.infoText,{marginLeft:22}]}>{item.address}</Text>
                            </View>
                            }
                            {item && item.phone !='' &&
                            <View style ={styles.addressPanel}>
                              <Image
                               style ={styles.callIcons}
                               source={Images.callIcon}
                              />
                               <Text style={styles.infoText}>{item.phone}</Text>
                            </View>
                           }
                             {item && item.email !='' &&
                            <View style ={styles.addressPanel}>
                              <Image
                               style ={styles.mailIcon}
                               source={Images.mailIcon}
                              />
                               <Text numberOfLines={1} style={[styles.infoText,{marginLeft:16}]}>{item.email}</Text>
                            </View>
                           }
                          </View>
                          <View style={{alignItems:'center',left:70,marginTop:15}}>
                            <Button onPress={() =>navigate('MemberDetails', { memberInfo: item })}  style={{borderColor:'#ddd',borderWidth:1,height:(height/20),width:(width/7.5),justifyContent:'center',backgroundColor:'white'}} rounded light>
                               <Text style={{color:'#666666',justifyContent:'center',fontSize:16,fontWeight:'600'}}>View More</Text>
                            </Button>
                          </View>
                        </View>}
         />
       }
         {this.state.isTableView &&
         <Table  columnWidth={80} columnHeight={500} columns={columns} dataSource={this.state.dataSource} />
       }
        </View>
        <View style={{alignItems:'center',marginLeft:(width/2.60),marginTop:(height/130)}}>
          <Button onPress={() =>navigate('AddFamilyMember')}  style={{alignItems:'center',marginTop:10,height:(height/18),width:(width/4),justifyContent:'center',backgroundColor:'#464348',}} rounded light>
             <Text style={{color:'white',justifyContent:'center',fontSize:20}}>Add Member</Text>
          </Button>
        </View>
      </Content>
    </View>
    );
  }
}
function mapStateToProps(state, ownProps) {
  console.log(state);
  return {
    // nav:  state.nav,
    // user: state.auth.user,
     familyInfo: state.auth.getmember
  };
}

function mapDispatchToProps(dispatch) {
  return {
  actions: bindActionCreators(showFamilyMember, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FamilyInformationScreen);
