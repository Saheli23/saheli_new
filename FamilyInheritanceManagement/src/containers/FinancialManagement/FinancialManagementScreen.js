import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity,
  AsyncStorage,
  FlatList,
  ScrollView


} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,ActionSheet,Left,Icon,Spinner} from 'native-base';
import * as showFinancialInformation from '../../actions/financialinformation.action';
import { StackNavigator } from 'react-navigation';
import { Switch } from 'react-native-switch';
import styles from '../../styles/FinancialManagementStyle';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { Images} from '../../Themes'
import { Col, Row, Grid } from "react-native-easy-grid";
import AlertMessage from '../../components/AlertMessage';
var {height, width} = Dimensions.get('window');
var logo = require('../../images/text_logo.png');
const FONT_SIZE = 20;
import Table from '../../library/react-native-simple-table';
const columns = [
  {
    title: 'Business Name',
    dataIndex: 'businessname',
    width: 245,
    sortable:true

  },
  {
    title: 'Financial Category',
    dataIndex: 'financialCategory',
    width: 140,

  },
  {
    title: 'Account Number',
    dataIndex: 'account_number',
    width: 140,
    sortable:true

  },
  {
    title: 'Date Opened',
    dataIndex: 'date_opened',
    width: 100,
    sortable:true

  },
  {
    title: 'Date Closed',
    dataIndex: 'date_closed',
    width: 100,
    sortable:true

  },
  {
    title: 'Status',
    dataIndex: 'account_status',
    width: 100,

  },
  {
    title: 'Total Assests',
    dataIndex: 'total_price',
    width: 140,
    sortable:true

  },

];

 class FinancialManagementScreen extends React.Component {
   constructor(props) {
     super(props);
     Text.defaultProps.allowFontScaling=false;
     this.state = {
       userName : '',
       userid   : '',
       dataSource:[],
       isTableView:false,
       gridImageSource:'',
       totalAssests:'',
       isFetching:true,

     };
   }
  static navigationOptions = ({ navigation }) => ({
    title:<Text>Family Inheritance Management</Text>,
    headerMode: 'screen',
    headerTintColor: '#ddd',
    header: <View style={styles.mainHeaderPanel}>
              <View style={styles.subHeader} >

                <Button style={styles.backBtn} transparent onPress={() =>  navigation.navigate('Workspace')}>
                {/*<Icon name="arrow-back" />*/}
                <Image
                   style={styles.backImage}
                   source={Images.backArrow}/>
                 <Text style={styles.backText}>Back</Text>
                </Button>
                {/*<Image
                 style={{width: width/2.9, height: 60,}}
                 source={logo}/>*/}
                 <Image
                 style={styles.logoPanel}
                 source={Images.modifiedLogo}/>
                 <View style={styles.mainMenuPanel}>
                 <TouchableOpacity onPress={() =>  navigation.navigate('UserProfile')}>
                   <Image
                    style={styles.userIconStyle}
                    source={Images.userIcon}/>
                  </TouchableOpacity>
                    <Image
                     style={styles.menuIconStyle}
                     source={Images.menuIcon}/>
                    </View>
               </View>
            </View> ,


  });
  componentWillMount(){

    AsyncStorage.getItem('userDetails').then((value) =>{
      if(value){
      let userData = JSON.parse(value)
      console.log('userDetails',value);
      console.log('userDetails',userData.userid);
      this.setState({userid:userData.userid});
      this.props.actions.getFinancialInformation(userData.userid);
      this.props.actions.getTotalAssests(userData.userid);
      }
    });
    //console.log('userid',this.state.userid);

    //  console.log('newPropsWorkspace',width,height);
    //  console.log('user name',this.state.userName);
  }

  componentWillReceiveProps (newProps) {
    console.log('newPropsfamilyInformation');
    if(newProps && newProps.financialInfo){
      this.setState({

        dataSource: newProps.financialInfo.financialInformation,
        isFetching:false
      })
      if(newProps && newProps.totalAssests){
        console.log(newProps.totalAssests);
        this.setState({
             totalAssests:newProps.totalAssests.total_assests
        })
      }
      // console.log(newProps.response);
    }

  }
  // data={[
  //   {businessid:1,businessname: 'Wildebar Credit',total_price:'36,000',account_number:'0346790',address:'44, Shirley Ave, West Chicago, FL 32904',phone:'335 505 1786',email:'devin@gmail.com',website:'www.wildebar.com',fax:'2435069834'},
  //   {businessid:2,businessname: 'Damvy',total_price:'36,000',account_number:'0346790',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com',website:'www.damvy.com',fax:'2435069834'},
  //   {businessid:3,businessname: 'Rallyspace',total_price:'36,000',account_number:'0346790',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com',website:'www.rallyspace.com',fax:'2435069834'},
  //   {businessid:4,businessname: 'Demogo',total_price:'36,000',account_number:'0346790',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com',website:'www.demogo.com',fax:'2435069834'},
  //   {businessid:5,businessname: 'Beta Track',total_price:'36,000',account_number:'0346790',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com',website:'www.beta Track.com',fax:'2435069834'},
  //   {businessid:6,businessname: 'Goldemo',total_price:'36,000',account_number:'0346790',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com',website:'www.goldemo.com',fax:'2435069834'},
  //   {businessid:7,businessname: 'Demolysh',total_price:'36,000',account_number:'0346790',address:'44, Shirley Ave',phone:'335 505 1786',email:'devin@gmail.com',website:'www.demolysh.com',fax:'2435069834'},
  //
  // ]}
  noRowData () {
    return this.state.dataSource.length === 0
  }
  manageView(view){
    if(view == 'table'){
      this.setState({
        isTableView:true,
      });
    }
    else{
      this.setState({
        isTableView:false,
      });
    }

  }
  _keyExtractor = (item, index) => item.businessid;

  render() {
       console.log('workspaceprops',this.props)
        const { navigate } = this.props.navigation;
    return (
     <View style={styles.container}>
        <Content scrollEnabled={false}>
        {this.state.isFetching &&
          <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
          <Spinner color='#737373' size='large' />
          </View>
        }
         <View style={styles.workspaceContainer}>
             <View style={(this.state.dataSource.length >0) ? styles.centerLabelListing :styles.centerLabel}>
               <Text style={(this.state.dataSource.length >0) ? styles.centerLabelTextListing :styles.centerLabelText}>Financial Management</Text>
               { (this.state.totalAssests !=null) && (this.state.totalAssests !='') &&
               <Text style={{marginRight:(width/38.4),fontSize:(width/45),fontWeight:'600',fontFamily:'Iowan Old Style',}}>${this.state.totalAssests.toLocaleString()}</Text>
               }
               { (this.state.totalAssests !=null) && (this.state.totalAssests =='') &&
               <Text style={{marginRight:(width/38.4),fontSize:(width/45),fontWeight:'600',fontFamily:'Iowan Old Style',}}>$0</Text>
               }
               { (this.state.dataSource.length >0) &&

               <TouchableOpacity onPress={() =>this.manageView('grid')} >
                {!this.state.isTableView &&
                 <Image
                  style ={styles.gridViewIcon}
                  source={Images.gridViewIcon}
                 />
                }
                {this.state.isTableView &&
                 <Image
                  style ={styles.gridViewIcon}
                  source={Images.gridViewDisableIcon}
                 />
                }
               </TouchableOpacity>
             }
             { (this.state.dataSource.length >0) &&
               <TouchableOpacity onPress={() =>this.manageView('table')} >
                 {this.state.isTableView &&
                 <Image
                  style ={styles.tableViewIcon}
                  source={Images.tableViewIcon}
                 />
                 }
                 {!this.state.isTableView &&
                   <Image
                    style ={styles.tableViewIcon}
                    source={Images.tableViewDisableIcon}
                   />
                   }
              </TouchableOpacity>
             }
         </View>

             { this.state.dataSource.length === 0 &&
         <AlertMessage title='No Financial Information added yet' show={this.noRowData()} />
      }
      {!this.state.isTableView &&
         <FlatList numColumns={3} contentContainerStyle={{flexDirection: 'column',


        }}

           data={this.state.dataSource}
           keyExtractor={this._keyExtractor}
           renderItem={({item}) =>
                      <View style={{backgroundColor: 'white',
                        margin:10,
                        width: 300,
                        height: (height/1.50)}}>
                          <View style ={styles.memberDetails}>
                            <Text numberOfLines={1} style ={styles.businessName}>{item.businessname}</Text>
                            {(item.total_price !='') &&
                            <Text style ={[styles.memberRelation,{marginTop:15}]}>${Number(item.total_price).toLocaleString()}</Text>
                            }
                            {(item.total_price =='') &&
                            <Text style ={[styles.memberRelation,{marginTop:15}]}>$0</Text>
                            }
                            <View style={{flexDirection:'row'}}>
                             <Text style ={[styles.infoText,{marginLeft:0}]}>Account no: </Text>
                             <Text style ={styles.accountNumber}>{item.account_number}</Text>
                            </View>
                            <View style ={styles.sepLine} />
                            {item && item.address !="" &&
                            <View style ={styles.addressPanel}>
                              <Image
                               style ={styles.locationIcons}
                               source={Images.locationIcon}
                              />
                               <Text numberOfLines={2} style={[styles.infoText,{marginLeft:22}]}>{item.address}</Text>
                            </View>
                            }
                              {item && item.phone !="" &&
                            <View style ={styles.addressPanel}>
                              <Image
                               style ={styles.callIcons}
                               source={Images.callIcon}
                              />
                               <Text style={styles.infoText}>{item.phone}</Text>
                            </View>
                            }
                              {item && item.email !="" &&
                            <View style ={styles.addressPanel}>
                              <Image
                               style ={styles.mailIcon}
                               source={Images.mailIcon}
                              />
                               <Text style={[styles.infoText,{marginLeft:16}]}>{item.email}</Text>
                            </View>
                           }
                           {item && item.website !="" &&
                            <View style ={styles.addressPanel}>
                              <Image
                               style ={styles.websiteIcon}
                               source={Images.websiteIcon}
                              />
                               <Text numberOfLines={1} style={[styles.infoText,{marginLeft:16}]}>{item.website}</Text>
                            </View>
                          }
                          {item && item.fax !="" &&
                            <View style ={styles.addressPanel}>
                              <Image
                               style ={styles.faxIcon}
                               source={Images.faxIcon}
                              />
                               <Text style={[styles.infoText,{marginLeft:16}]}>{item.fax}</Text>
                            </View>
                          }
                          </View>
                          <View style={{alignItems:'center',left:70,marginTop:15}}>
                            <Button onPress={() =>navigate('FinancialDetails', { financialInfo: item })}   style={{borderColor:'#ddd',borderWidth:1,height:(height/20),width:(width/7.5),justifyContent:'center',backgroundColor:'white'}} rounded light>
                               <Text style={{color:'#666666',justifyContent:'center',fontSize:16,fontWeight:'600'}}>View More</Text>
                            </Button>
                          </View>
                        </View>}
         />
       }
         {this.state.isTableView &&
         <Table  columnWidth={80} columnHeight={500} columns={columns} dataSource={this.state.dataSource} />
       }
        </View>
        <View style={{alignItems:'center',marginLeft:(width/2.60),marginTop:(height/130)}}>
          <Button onPress={() =>navigate('AddFinancialManagement')}  style={{alignItems:'center',marginTop:10,height:(height/18),width:(width/4),justifyContent:'center',backgroundColor:'#464348',}} rounded light>
             <Text style={{color:'white',justifyContent:'center',fontSize:20}}>Add Another</Text>
          </Button>
        </View>
      </Content>
    </View>
    );
  }
}
function mapStateToProps(state, ownProps) {
  console.log(state);
  return {
    // nav:  state.nav,
    // user: state.auth.user,
     totalAssests:  state.auth.getTotalAssests,
     financialInfo: state.auth.getFinancialInformation
  };
}

function mapDispatchToProps(dispatch) {
  return {
  actions: bindActionCreators(showFinancialInformation, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FinancialManagementScreen);
