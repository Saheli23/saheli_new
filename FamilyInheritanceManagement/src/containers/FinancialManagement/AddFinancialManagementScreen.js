import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity,
  AsyncStorage,
  FlatList,
  ScrollView


} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,Left,Icon,Spinner} from 'native-base';
import * as addFinancialInformation from '../../actions/financialinformation.action';
import { StackNavigator } from 'react-navigation';
import { Switch } from 'react-native-switch';
import styles from '../../styles/FinancialManagementStyle';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { Images} from '../../Themes'
import { Col, Row, Grid } from "react-native-easy-grid";
import Toast, {DURATION} from 'react-native-easy-toast';
import * as EmailValidator from 'email-validator';
import States from '../../_global/States';
import AccountStatus from '../../_global/AccountStatus';
import FinancialCategory from '../../_global/FinancialCategory';
import ModalPicker from '../../library/react-native-modal-picker';
import DatePicker from '../../library/react-native-datepicker';
import  {GooglePlacesAutocomplete}  from '../../library/react-native-google-places-autocomplete';
// import ImagePicker from 'react-native-image-picker'
//import ImagePicker from 'react-native-image-crop-picker';
import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
var {height, width} = Dimensions.get('window');
var logo = require('../../images/text_logo.png');
const FONT_SIZE = 20;
const CANCEL_INDEX = 0
const options = [
  'Cancel',
  'Take Photo… ',



];


 class AddFinancialManagementScreen extends React.Component {
   constructor(props) {
     super(props);
     Text.defaultProps.allowFontScaling=false;
     this.state = {
       userName : '',
       userid   : '',
       membername:'',
       relation:'',
       ssn:'',
       note:'',
       address  :'',
       city     :'',
       state    :'',
       zip      :'',
       phone    :'',
       email    :'',
       birthday:'',
       maxDate:new Date(),
       isLoading:false,
       contactPanel:[{}],
       website:'',
       fax:'',
       accountNumber:'',
       assests:'',
       imageSource:null,
       originalPath:null,
       documentPath:'',
       images:null,
       selected:'',
       businessName:'',
       financialCategory:'',
       dateOpened:'',
       dateClosed:'',
       accountStatus:'',
       showFileUpload:false

     };
   }
  static navigationOptions = ({ navigation }) => ({
    title:<Text>Family Inheritance Management</Text>,
    headerMode: 'screen',
    headerTintColor: '#ddd',
    header: <View style={styles.mainHeaderPanel}>
              <View style={styles.subHeader} >

                <Button style={styles.backBtn} transparent onPress={() => navigation.goBack()}>
                {/*<Icon name="arrow-back" />*/}
                <Image
                   style={styles.backImage}
                   source={Images.backArrow}/>
                 <Text style={styles.backText}>Back</Text>
                </Button>
                {/*<Image
                 style={{width: width/2.9, height: 60,}}
                 source={logo}/>*/}
                 <Image
                 style={styles.logoPanel}
                 source={Images.modifiedLogo}/>
                 <View style={styles.mainMenuPanel}>
                 <TouchableOpacity onPress={() =>  navigation.navigate('UserProfile')}>
                   <Image
                    style={styles.userIconStyle}
                    source={Images.userIcon}/>
                  </TouchableOpacity>
                    <Image
                     style={styles.menuIconStyle}
                     source={Images.menuIcon}/>
                    </View>
               </View>
            </View> ,


  });
  componentWillMount(){


    AsyncStorage.getItem('userDetails').then((value) =>{
      if(value){
      let userData = JSON.parse(value)
      console.log('userDetails',value);
      console.log('userDetails',userData.userid);
    this.setState({userid:userData.userid});
  //  this.props.actions.workspaceInfo(userData.userid);
      }
    });
    //console.log('userid',this.state.userid);

    //  console.log('newPropsWorkspace',width,height);
    //  console.log('user name',this.state.userName);
  }

  componentWillReceiveProps (newProps) {
    console.log('newPropsAddFamilyInfo',newProps);
    if(newProps && newProps.addFinancialInformation && newProps.addFinancialInformation.addFinancialInfo == 'success'  ){
         this.refs.toast.show('Financial information added successfully!',2000);
         this.setState({
           isLoading:false,
           //membername:'',
           //relation:'',
           //ssn:'',
           note:'',
           address:'',
           city:'',
           state:'',
           zip:'',
           phone:'',
           email:'',
           //birthday:''

            });
      console.log(newProps.addFinancialInformation);
    }

  }



  _addRow(){
    let contactPanel = this.state.contactPanel;
    contactPanel.push({});
    this.setState({contactPanel});
  };
  _removeRow(rowId){
    console.log('rowId',rowId);
    let contactPanel = this.state.contactPanel;
    contactPanel.splice(rowId, 1);
    this.setState({contactPanel});

  };

  addFinancialInformationSubmit(navigate){
     console.log(this.state.contactPanel);
     const {userid,businessName,note, address, city, state,zip,phone,email,dateOpened,dateClosed,fax,website,originalPath,contactPanel,documentPath,assests,financialCategory,accountStatus,accountNumber} = this.state;
     let emailValidate = EmailValidator.validate(email);
      if(businessName == ""){
           this.refs.toast.show('Please provide Business Name!',DURATION.LENGTH_SHORT);
         }
      else if(email == ""){
           this.refs.toast.show('Please provide Email Address!',DURATION.LENGTH_SHORT);
         }
      else if(!emailValidate){
           this.refs.toast.show('Please provide Valid Email Address!',DURATION.LENGTH_SHORT);
         }
      else{
           this.setState({isLoading:true});
           this.props.actions.addFinancialInformation(userid,businessName,note, address, city, state,zip,phone,email,dateOpened,dateClosed,fax,website,originalPath,contactPanel,documentPath,assests,financialCategory,accountStatus,accountNumber);

         }
   }

   onChangeContactPanel(index, text, name, e){
     //console.log(index);
     let contactPanel = this.state.contactPanel;
     contactPanel[index][name] = text;
    //  console.log(contactPanel[i][field],v)
     this.setState({contactPanel:contactPanel});
   }

   getBusinessNameDetails(data,details){
     console.log('data',data);
     console.log('details',details);
    // let address = data.terms[1].value+ ' , ' +data.terms[2].value;
     let address = details.formatted_address;
     let city   ;
     let zip    ;
     let website = (details.website !=undefined) ? details.website :'';
     let phone   = (details.international_phone_number!=undefined) ? details.international_phone_number :'';
     let state   ;
     let businessName = details.name;

     for (var i = 0; i < details.address_components.length; i++) {
          var addressType = details.address_components[i].types[0];
          if(addressType == 'postal_code'){
            zip  = details.address_components[i].long_name;
          }
          else if(addressType == 'administrative_area_level_1'){
           state = details.address_components[i].long_name;
          }
          else if(addressType == 'administrative_area_level_2'){
           city = details.address_components[i].long_name;
          }
          console.log('addressType',addressType);
          // if (componentForm[addressType]) {
          //   var val = place.address_components[i][componentForm[addressType]];
          //   document.getElementById(addressType).value = val;
          // }
        }
     this.setState({businessName:businessName,address:address,city:city,zip:zip,website:website,phone:phone,state:state});
   }

     //      ImagePicker.showImagePicker(options, (response) => {
     //        console.log('Response = ', response);
     //
     //        if (response.didCancel) {
     //          console.log('User cancelled image picker');
     //        }
     //        else if (response.error) {
     //          console.log('ImagePicker Error: ', response.error);
     //        }
     //        else if (response.customButton) {
     //          console.log('User tapped custom button: ', response.customButton);
     //        }
     //        else {
     //         console.log(response.uri);
     //          console.log("uploading started!!");
     //          console.log(self.imageSource);
     //           let source = { uri: response.uri };
     //          self.setState({
     //            imageSource: source,
     //            originalPath:response
     //          });
     //          //this.profileImageUpload(response,itemData.uuid,2);
     //        }
     //      });
     // }

  //  handleUploadFile(){
  //     //alert('image profile upload!')
  //   var self = this;
  //  //console.log(self.imageSource);
  //     const options = {
  //        title: 'Profile image Picker',
  //        takePhotoButtonTitle: 'Take Image...',
  //        mediaType: 'image',
  //        allowsEditing:true,
  //        imageQuality: 'medium',
   //
  //      };
  showActionSheet = () => {
     this.ActionSheet.show()
   }
   pickDocument(event){
       const {pageX, pageY} = event.nativeEvent;
    //   console.log(pageX,pageY);285,622
     DocumentPicker.show({
     top: pageY,
     left: pageX,
     filetype: ['public.content'],
     }, (error, url) => {
       console.log(url);
       this.setState({documentPath:url});
       alert('file captured');
   });
   }

  // pickSingleBase64(cropit) {
  //      try {
  //        ImagePicker.openPicker({
  //            width: 300,
  //            height: 300,
  //            cropping: cropit,
  //            includeBase64: true,
  //            includeExif: true,
  //            mediaType:'any'
  //          }).then(image => {
  //              console.log('received base64 image');
  //              //this.refs.toast.show('Please wait while uploading your Image',2000);
  //              this.setState({
  //                originalPath:{uri: image.path, width: image.width, height: image.height},
  //                imageSource: {uri: `data:${image.mime};base64,`+ image.data, width: image.width, height: image.height},
  //                images: null
  //            });
  //            //console.log(this.state.imageSource.uri);
  //          }).catch(e => console.log(e)).then(s => console.log(s));
  //
  //      } catch (e) {
  //        console.log("Open Log ImagePicker, ", e);
  //      } finally {
  //        console.log("Done ImagePicker");
  //      }
  //   }
  //
  //   pickSingleWithCamera(cropping) {
  //       ImagePicker.openCamera({
  //         cropping: cropping,
  //         width: 500,
  //         height: 500,
  //         includeExif: true,
  //       }).then(image => {
  //           console.log('received image', image);
  //           this.setState({
  //             imageSource: {uri: image.path, width: image.width, height: image.height},
  //             images: null
  //           });
  //       }).catch(e => alert(e));
  // }


  render() {
    //  console.log(this.state.contactPanel);
        const { navigate } = this.props.navigation;
        let relationkey = 0;
        const RelationData = [
            { key: relationkey++, label: 'Grandfather' },
            { key: relationkey++, label: 'Grandmother' },
            { key: relationkey++, label: 'Father' },
            { key: relationkey++, label: 'Mother' },
            { key: relationkey++, label: 'Wife' },
            { key: relationkey++, label: 'Sister' },
            { key: relationkey++, label: 'Brother' },

        ];

    return (
     <View style={styles.container}>
       <ActionSheet
         ref={o => this.ActionSheet = o}
         options={options}
         cancelButtonIndex={CANCEL_INDEX}
         onPress={(i)=>{
                   this.setState({
             selected: i
           },() => {
             console.log(this.state.selected);
             if(this.state.selected == 1){
               console.log("Take image");
               this.pickSingleWithCamera(false);
             }
            //  else if(this.state.selected == 2){
             //
            //    this.pickSingleBase64(false);
            //  }
            //  else if(this.state.selected == 3){
            //    this.pickDocument();
            //   this.setState({showFileUpload:true})
            //  }
             else{
               console.log("cancled....");
             }
           })

         }}
       />

        <Content scrollEnabled={false}>

         <View style={styles.workspaceContainer}>
             <View style={{margin:22}}>
               <Text style={{fontSize:25,fontWeight:'600'}}>Add Item</Text>
             </View>
             <Toast
                ref="toast"
                style={{backgroundColor:'#ff704d',zIndex:999,}}
                position='top'
                positionValue={48}
                fadeInDuration={750}
                fadeOutDuration={750}
                opacity={0.9}
                textStyle={{color:'white'}}
            />
             <ScrollView>
             {this.state.isLoading &&
               <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
               <Spinner color='#737373' size='large' />
               </View>
             }

             <View style={[styles.devideTextbox,{zIndex:1}]}>
                            <View style={{width:(width/2.13)}}>
                                 <GooglePlacesAutocomplete
                                      placeholder='Business Name'
                                      placeholderTextColor='#595959'
                                      minLength={2} // minimum length of text to search
                                      autoFocus={false}
                                      returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                                      listViewDisplayed='auto'    // true/false/undefined
                                      fetchDetails={true}
                                      renderDescription={row => row.description} // custom description render
                                      onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                        console.log(data, details);
                                        this.getBusinessNameDetails(data, details);
                                      }}

                                      getDefaultValue={() => ''}

                                      query={{
                                        // available options: https://developers.google.com/places/web-service/autocomplete
                                        key: 'AIzaSyDtSHrAJ6ubbcD_dLnKLUHxSeu_qF_PnEQ',
                                        language: 'en', // language of the results
                                        //types: '(cities)' ,// default: 'geocode'only cities will come
                                        types:'establishment',
                                      //  componentRestrictions: {country: "us"}
                                      }}

                                      styles={{
                                        textInputContainer:{marginTop:(height/48),
                                        width:(width/2.2),
                                        height:48,
                                        backgroundColor:'white',
                                        borderRadius: 3,
                                        marginBottom:10,
                                        marginRight:15,
                                        borderColor:'#ddd'},
                                        description: {
                                          fontWeight: 'bold'
                                        },
                                        predefinedPlacesDescription: {
                                          color: '#1faadb'
                                        },


                                      }}

                                      //currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
                                      //currentLocationLabel="Current location"
                                      nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                                      GoogleReverseGeocodingQuery={{
                                        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                                      }}
                                      GooglePlacesSearchQuery={{
                                        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                        rankby: 'distance',
                                        types: 'accounting',

                                      }}

                                      filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                                      //predefinedPlaces={[homePlace, workPlace]}

                                      debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                                      //renderLeftButton={()  => <Image source={require('path/custom/left-icon')} />}
                                      //renderRightButton={() => <Text>Custom text after the input</Text>}
                        />
                  </View>
                {/* <Item regular style={styles.inputBox}>
                   <Input style={styles.placeholderFont} returnKeyType="next" value={this.state.membername}   onChangeText={ (text) => this.setState({ membername: text }) } placeholder='Business Name' />
                 </Item> */}
                {/* <Item regular style={styles.inputBox}>
                 <ModalPicker
                     data={RelationData}
                     style={{height:250,width:(width/2.32),marginTop:210,marginRight:10,marginLeft:2}}
                     initValue="Relation"
                     onChange={(option)=>{
                       this.setState({relation:option.label})
                       console.log(option)
                      }}>
                    </ModalPicker>
                 </Item>*/}
                 <Item regular style={[styles.inputBox,]}>
                   <Input style={styles.placeholderFont} returnKeyType="next"  ref='address' value={this.state.address} onSubmitEditing={(event) => { this.refs.city._root.focus(); }}  onChangeText={ (text) => this.setState({ address: text }) } placeholder='Address'  />
                 </Item>
             </View>
             <View style={[styles.devideTextbox,]}>
             <Item regular style={styles.inputBox}>
              <ModalPicker
                  data={FinancialCategory}
                  style={{height:250,width:(width/2.22),marginTop:205,marginRight:10,marginLeft:1,}}
                  initValue="Financial Category"
                  onChange={(option)=>{
                    this.setState({financialCategory:option.label})
                    console.log(option)
                   }}>
                 </ModalPicker>
              </Item>
              {/*
              <Item regular style={styles.inputBox}>
                <Input style={styles.placeholderFont}  ref='city' value={this.state.otherFinancialCategory}    onChangeText={ (text) => this.setState({ city: text }) } placeholder='City' />
              </Item> */}
                 <Item regular style={styles.inputBox}>
                   <Input style={styles.placeholderFont}  ref='city' value={this.state.city} onSubmitEditing={(event) => { this.refs.accountNumber._root.focus(); }}    onChangeText={ (text) => this.setState({ city: text }) } placeholder='City' />
                 </Item>
             </View>
             <View style={[styles.devideTextbox,]}>
             <Item regular style={styles.inputBox}>
               <Input style={styles.placeholderFont}  ref='accountnumber' value={this.state.accountNumber} onSubmitEditing={(event) => { this.refs.state._root.focus(); }}    onChangeText={ (text) => this.setState({ accountNumber: text }) } placeholder='Account Number' />
             </Item>
                <Item regular style={[styles.inputBox,{width:(width/4.60)}]}>
                  {/*<ModalPicker
                      data={States}
                      style={{height:250,width:(width/4.8),marginTop:210,marginRight:10,marginLeft:2}}
                      initValue="State"
                      onChange={(option)=>{
                        this.setState({state:option.label})
                        console.log(option)
                       }}>
                     </ModalPicker>*/}
                     <Input style={styles.placeholderFont}  ref='state' value={this.state.state}    onChangeText={ (text) => this.setState({ state: text }) } placeholder='State' />

                  </Item>
                  <Item regular style={styles.inputBox}>
                    <Input style={styles.placeholderFont} returnKeyType="next" value={this.state.zip}  onSubmitEditing={(event) => { this.refs.assests._root.focus(); }} onChangeText={ (text) => this.setState({ zip: text }) } placeholder='Zip' keyboardType = 'numeric' />
                 </Item>
             </View>
             <View style={[styles.devideTextbox,]}>
             <Item regular style={styles.inputBox}>
               <Input style={styles.placeholderFont}  ref='assests' value={this.state.assests}  onSubmitEditing={(event) => { this.refs.phone._root.focus(); }}   onChangeText={ (text) => this.setState({ assests: text }) } placeholder='Account Assets' />
             </Item>
             {/*<Item regular style={styles.inputBox}>
              <ModalPicker
                  data={RelationData}
                  style={{height:250,width:(width/2.34),marginTop:210,marginRight:10,marginLeft:2}}
                  initValue="Country"
                  onChange={(option)=>{
                    this.setState({relation:option.label})
                    console.log(option)
                   }}>
                 </ModalPicker>
              </Item>*/}
              <Item regular style={styles.inputBox}>
                <Input style={styles.placeholderFont} returnKeyType="next" ref='phone' value={this.state.phone}  onSubmitEditing={(event) => { this.refs.email._root.focus(); }} onChangeText={ (text) => this.setState({ phone: text }) } placeholder='Phone Number' keyboardType = 'numeric' />
              </Item>
             </View>

             <View style={[styles.devideTextbox,]}>
                 <Item regular style={[styles.inputBox,{width: (width/4.6)}]}>
                   {/*<Input style={styles.placeholderFont} returnKeyType="next"  ref='password' onSubmitEditing={(event) => { this.refs.conpassword._root.focus(); }}  onChangeText={ (text) => this.setState({ password: text }) } placeholder='Birthday' />*/}
                   <DatePicker
                       style={{width: (width/5),height:90,marginTop:40,alignItems:'flex-start',marginLeft:5}}
                       date={this.state.dateOpened}
                       mode="date"
                       placeholder="Date Opened"
                       format="MM/DD/YYYY"

                       maxDate={this.state.maxDate}
                       confirmBtnText="Confirm"
                       cancelBtnText="Cancel"
                       customStyles={{
                         dateIcon: {
                          // position: 'absolute',
                          // left: 0,
                           marginTop: 2,
                           //marginLeft: 0
                         },
                         dateInput: {
                           marginLeft: 5,
                           borderColor: 'white',


                         },

                         // ... You can check the source to find the other keys.
                       }}
                       onDateChange={(date) => {this.setState({dateOpened: date})}}
           />
                 </Item>
                 <Item regular style={[styles.inputBox,{width: (width/4.5)}]}>
                   {/*<Input style={styles.placeholderFont} returnKeyType="next"  ref='password' onSubmitEditing={(event) => { this.refs.conpassword._root.focus(); }}  onChangeText={ (text) => this.setState({ password: text }) } placeholder='Birthday' />*/}
                   <DatePicker
                       style={{width: (width/5),height:90,marginTop:40,alignItems:'flex-start',marginLeft:5}}
                       date={this.state.dateClosed}
                       mode="date"
                       placeholder="Date Closed"
                       format="MM/DD/YYYY"

                       //maxDate={this.state.maxDate}
                       confirmBtnText="Confirm"
                       cancelBtnText="Cancel"
                       customStyles={{
                         dateIcon: {
                          // position: 'absolute',
                          // left: 0,
                           marginTop: 2,
                           //marginLeft: 0
                         },
                         dateInput: {
                           marginLeft: 5,
                           borderColor: 'white',


                         },

                         // ... You can check the source to find the other keys.
                       }}
                       onDateChange={(date) => {this.setState({dateClosed: date})}}
           />
                 </Item>
                 <Item regular style={styles.inputBox}>
                   <Input style={styles.placeholderFont} autoCapitalize='none' returnKeyType="next" ref='email' value={this.state.email} onSubmitEditing={(event) => { this.refs.note._root.focus(); }}  onChangeText={ (text) => this.setState({ email: text }) } placeholder='Email Address' keyboardType = 'email-address'  />
                 </Item>
             </View>
             <View style={[styles.devideTextbox,]}>
             <Item regular style={styles.inputBox}>
              <ModalPicker
                  data={AccountStatus}
                  style={{height:253,width:(width/2.22),marginTop:208,marginLeft:1}}
                  initValue="Account Status"
                  onChange={(option)=>{
                    this.setState({accountStatus:option.label})
                    console.log(option)
                   }}>
                 </ModalPicker>
              </Item>
              <Item regular style={styles.inputBox}>
                <Input style={styles.placeholderFont} autoCapitalize='none' returnKeyType="next" ref='website' value={this.state.website} onSubmitEditing={(event) => { this.refs.note._root.focus(); }}  onChangeText={ (text) => this.setState({ website: text }) } placeholder='Website'  />
              </Item>
             </View>
             <View style={[styles.devideTextbox,]}>
               <Item regular style={[styles.inputBox,{marginLeft:(width/2.1)}]}>
                 <Input style={styles.placeholderFont} autoCapitalize='none' returnKeyType="next" ref='fax' value={this.state.fax} onSubmitEditing={(event) => { this.refs.note._root.focus(); }}  onChangeText={ (text) => this.setState({ fax: text }) } placeholder='Fax'   />
               </Item>

             </View>

             {this.state.contactPanel.length && this.state.contactPanel.map((row, i) => {
               return (
                 <View key={'cntp_'+i} style={styles.devideTextbox}>
                   <Item regular style={[styles.inputBox,{width:(width/3.65)}]}>
                     <Input style={styles.placeholderFont} autoCapitalize='none' returnKeyType="next" ref='contactName' value={this.state.contactPanel[i]['contactName']} onSubmitEditing={(event) => { this.refs.contactEmail._root.focus(); }}  onChangeText={ (text) => {this.onChangeContactPanel.call(this, i, text, 'contactName') } } placeholder='Contact&apos;s Name'   />
                   </Item>
                   <Item regular style={[styles.inputBox,{width:(width/3.6)}]}>
                     <Input style={styles.placeholderFont} autoCapitalize='none' returnKeyType="next" ref='contactEmail' value={this.state.contactPanel[i]['contactEmail']} onSubmitEditing={(event) => { this.refs.contactPhone._root.focus(); }}  onChangeText={ (text) => {this.onChangeContactPanel.call(this, i,text, 'contactEmail')} } placeholder='Contact&apos;s Email' keyboardType = 'email-address'  />
                   </Item>
                   <Item regular style={[styles.inputBox,{width:(width/3.6)}]}>
                     <Input style={styles.placeholderFont} autoCapitalize='none' returnKeyType="next" ref='contactPhone' value={this.state.contactPanel[i]['contactPhone']} onSubmitEditing={(event) => { this.refs.note._root.focus(); }}  onChangeText={ (text) => {this.onChangeContactPanel.call(this, i,text, 'contactPhone') }} placeholder='Contact&apos;s Phone'   />
                   </Item>
                   {(this.state.contactPanel.length-1 === i) ?
                    (<TouchableOpacity onPress={ this._addRow.bind(this) } style={{marginTop:(height/28)}}>
                       <Image
                        style ={styles.addIcon}
                        source={Images.addIcon}
                       />
                     </TouchableOpacity>)
                   :
                   (<TouchableOpacity onPress={ this._removeRow.bind(this,i) } style={{marginTop:(height/28)}}>
                     <Image
                      style ={styles.addIcon}
                      source={Images.removeIcon}
                     />
                   </TouchableOpacity>)}

               </View>);
             })}

             <Item regular style={styles.noteInputBox}>
               <Input style={styles.placeholderFont} returnKeyType="done"  ref='note' value={this.state.note}  onChangeText={ (text) => this.setState({ note: text }) } placeholder='Comment'  />
             </Item>
             <View style={{flexDirection:'row'}}>
               {(this.state.imageSource !=null) &&
               <Image style={{width:(width/7), height: (height/8), resizeMode: 'contain',marginRight:10,marginTop:15}} source={this.state.imageSource} />
             }
             {/*<TouchableOpacity onPress={(event) => this.showActionSheet()}  style={styles.uploadBox} >
                 <Image
                  style ={[styles.addIcon,{marginTop:(height/25)}]}
                  source={Images.uploadIcon}
                 />
                 <Text style={styles.infoText}>Upload Image</Text>
             </TouchableOpacity>*/}
             {//this.state.showFileUpload &&
               <TouchableOpacity onPress={(event) => this.pickDocument(event)}  style={[styles.uploadBox,]} >
                   <Image
                    style ={[styles.addIcon,{marginTop:(height/25)}]}
                    source={Images.uploadIcon}
                   />
                   <Text style={styles.infoText}>Upload File</Text>
               </TouchableOpacity>
               }
             </View>
             </ScrollView>

        </View>
        <View style={{alignItems:'center',marginLeft:(width/2.60),marginTop:(height/110),flexDirection:'row'}}>
        <Button onPress={()=> navigate('FinancialManagement')}   style={{left:-150,alignItems:'center',marginTop:10,height:(height/18),width:(width/8),justifyContent:'center',backgroundColor:'white',borderColor:'#6791C1',borderWidth:1}} rounded light>
           <Text style={{color:'black',justifyContent:'center',fontSize:20}}>Cancel</Text>
        </Button>
        <Button onPress={() => this.addFinancialInformationSubmit(navigate)}   style={{left:-135,alignItems:'center',marginTop:10,height:(height/18),width:(width/8),justifyContent:'center',backgroundColor:'#6791C1',}} rounded light>
           <Text style={{color:'white',justifyContent:'center',fontSize:20}}>Save</Text>
        </Button>
          <Button   style={{left:-120,alignItems:'center',marginTop:10,height:(height/18),width:(width/4),justifyContent:'center',backgroundColor:'#6791C1',}} rounded light>
             <Text style={{color:'white',justifyContent:'center',fontSize:20}}>Save and Add Another</Text>
          </Button>
        </View>
      </Content>
    </View>
    );
  }
}
function mapStateToProps(state, ownProps) {
  console.log(state);
  return {
    // nav:  state.nav,
    // user: state.auth.user,
     addFinancialInformation: state.auth.addFinancialInformation
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(addFinancialInformation, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddFinancialManagementScreen);
