import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity,
  AsyncStorage,
  FlatList,
  ScrollView


} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,ActionSheet,Left,Icon} from 'native-base';
import * as userInfomationAction from '../../actions/user.action';
import { StackNavigator } from 'react-navigation';
import { Switch } from 'react-native-switch';
import styles from '../../styles/UserProfileStyle';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { Images} from '../../Themes'
var {height, width} = Dimensions.get('window');
var logo = require('../../images/text_logo.png');
import * as _ from 'lodash';
const FONT_SIZE = 20;

 class UserProfileScreen extends React.Component {
   constructor(props) {
     super(props);
     Text.defaultProps.allowFontScaling=false;
     this.state = {
       userName : '',
       userid   : '',
       email:'',
       phone:'',
       address:'',
       city:'',
       state:'',
       zip:'',
       username:'',
       firstname:'',
       lastname:''

     };
   }
  static navigationOptions = ({ navigation }) => ({
    title:<Text>Family Inheritance Management</Text>,
    headerMode: 'screen',
    headerTintColor: '#ddd',
    header: <View style={{height:(height/9),marginLeft:(width/51.2),alignItems:'center'}}>
              <View style={{flexDirection:'row',marginTop:(width/102.4)}} >

              <Button style={styles.backBtn} transparent onPress={() => navigation.goBack()}>
              {/*<Icon name="arrow-back" />*/}
              <Image
                 style={styles.backImage}
                 source={Images.backArrow}/>
                <Text style={styles.backText}>Back</Text>
              </Button>
              {/*<Image
               style={{width: width/2.9, height: 60,}}
               source={logo}/>*/}
               <Image
               style={styles.logoPanel}
               source={Images.modifiedLogo}/>
                 <View style={{marginTop:20,height:30,flexDirection:'row',left:200}}>
                   {/*<Image
                    style={{width: width/40, height: 30,marginRight:20}}
                    source={Images.userIcon}/>*/}
                    <Image
                     style={{width: width/40, height: 20,marginTop:5}}
                     source={Images.menuIcon}/>
                    </View>
               </View>
            </View> ,


  });
  componentWillMount(){

    AsyncStorage.getItem('userDetails').then((value) =>{
      if(value){
      let userData = JSON.parse(value)
      console.log('userDetails',value);
      console.log('userDetails',userData.userid);
    this.setState({userid:userData.userid});
   this.props.actions.getUserInfo(userData.userid);
      }
    });

  }

  componentWillReceiveProps (newProps) {
    console.log('newPropsWorkspace');
    if(newProps && newProps.userInfo){
         //let firstname = newProps.userInfo.userDetails.username.split(' ')[0];
         //let lastname  = newProps.userInfo.userDetails.username.split(' ')[1];
         this.setState({
           email:newProps.userInfo.userDetails.email,
           phone:newProps.userInfo.userDetails.phone,
           address:newProps.userInfo.userDetails.address,
           city:newProps.userInfo.userDetails.city,
           state:newProps.userInfo.userDetails.state,
           zip:newProps.userInfo.userDetails.zip,
           username:newProps.userInfo.userDetails.firstname,
           firstname:newProps.userInfo.userDetails.firstname,
           lastname:newProps.userInfo.userDetails.lastname,
         })
      console.log(newProps.userInfo.userDetails);
    }

  }

  render() {
       console.log('workspaceprops',this.props)
        const { navigate } = this.props.navigation;


    return (
     <View style={styles.container}>
        <Content scrollEnabled={false}>

         <View style={styles.workspaceContainer}>
             <View style={styles.centerLabel}>
               <Text style={styles.centerLabelText}>User Profile</Text>
             </View>
             <View style={styles.memberInformation}>
             <View style={styles.editPanel}>
                <TouchableOpacity>
                  <Image
                   style ={[styles.editIcons,{marginLeft:570}]}
                   source={Images.editIcon}
                  />
                </TouchableOpacity>

               </View>
               <View style={styles.memberInnerContain}>
                  <View style={{flexDirection:'row',height:(height/15),width:(width/2.80)}}>
                      <Image
                       style ={[styles.userFirstNameIcon,{}]}
                       source={Images.userFirstName}
                      />
                     <Text style={styles.userFirstName}>First Name: {this.state.firstname} </Text>
                     </View>
                    <View style={{flexDirection:'row',height:30,height:(height/15)}}>
                     <Image
                      style ={[styles.userFirstNameIcon,{}]}
                      source={Images.userFirstName}
                     />
                       <Text style={[styles.userFirstName,{}]}>Last Name: {this.state.lastname}</Text>

                     </View>

               </View>

               <View style ={styles.underLine} />
                 <View style={styles.fullDetails}>
                  <Text style={{fontWeight:'600',fontSize:(width/51.2),color:"#666666",marginBottom:10,fontFamily:'Iowan Old Style',}}>User Information</Text>
                    <View style={{flexDirection:'row'}}>
                       <View style={{flexDirection:'column',width:(width/2.75)}}>
                         <View style={{flexDirection:'row',marginBottom:25}}>
                             <Image
                              style ={[styles.blueMailIcon,{marginTop:5,}]}
                              source={Images.mailIcon}
                             />

                             <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>{this.state.email}</Text>
                          </View>

                       </View>
                       <View style={{flexDirection:'column',width:(width/4)}}>
                         <View style={{flexDirection:'row',marginBottom:20}}>
                             <Image
                              style ={[styles.passwordIcon,{}]}
                              source={Images.passwordIcon}
                             />

                             <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}></Text>
                          </View>

                       </View>
                       <View style={{flexDirection:'column',width:(width/4)}}>
                         <View style={{flexDirection:'row',marginBottom:25}}>
                             <Image
                              style ={[styles.callIcon,{marginTop:5,}]}
                              source={Images.callIcon}
                             />

                             <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>{this.state.phone}</Text>
                          </View>

                       </View>

                    </View>
                    <View style ={styles.secondUnderLine} />
                     <Text style={{fontWeight:'600',fontSize:(width/51.2),color:"#666666",marginBottom:10,fontFamily:'Iowan Old Style',}}>Address</Text>
                     <View style={{flexDirection:'row',marginBottom:(height/30.72),}}>
                         <Text style={[styles.detailsText,{flexWrap:'wrap',width:(width/4)}]}>{this.state.address} </Text>
                         <Text style={[styles.detailsText,{flexWrap:'wrap',width:(width/5)}]}>City: {this.state.city}</Text>
                         <Text style={[styles.detailsText,{flexWrap:'wrap',width:(width/5)}]}>State: {this.state.state}</Text>
                         <Text style={[styles.detailsText,{flexWrap:'wrap',width:(width/5)}]}>Zip Code: {this.state.zip}</Text>
                      </View>
               </View>


             </View>

        </View>
        <View style={{alignItems:'center',marginLeft:(width/2.60),marginTop:(height/130)}}>
          {/*<Button onPress={() =>navigate('AddFinancialManagement')}  style={{alignItems:'center',marginTop:10,height:(height/18),width:(width/4),justifyContent:'center',backgroundColor:'#6791C1',}} rounded light>
             <Text style={{color:'white',justifyContent:'center',fontSize:20}}>Add Another</Text>
          </Button>*/}
        </View>
      </Content>
    </View>
    );
  }
}
function mapStateToProps(state, ownProps) {
  console.log(state);
  return {
    // nav:  state.nav,
    // user: state.auth.user,
    // workspaceInfo: state.auth.response
    userInfo:state.auth.getUserInfo
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(userInfomationAction, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileScreen);
