import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity


} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,CheckBox} from 'native-base';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import styles from '../styles/welcomeStyle';
import Toast, {DURATION} from 'react-native-easy-toast';
var {height, width} = Dimensions.get('window');
var logo = require('../images/logo.png');
var Textlogo = require('../images/text_logo.png');
import { Images} from '../Themes';



 class WelcomeScreen extends React.Component {
  constructor(props) {
    super(props);
    Text.defaultProps.allowFontScaling=false;
    this.state = {
     buttonText:"Let’s Get Started",
     terms:false
    };
  }
  static navigationOptions = {
    title:'',
    header:<View style={{height:(height/9),marginLeft:20,alignItems:'center'}}>
              <View style={{flexDirection:'row',marginTop:10}} >
                <Image
                 style={styles.headerLogoPanel}
                 source={Images.modifiedLogo}/>
                 </View>
            </View> ,

  };
   componentWillMount () {
    //console.log(this.props);
    console.log(this.props);
    //this.refs.toast.show('Thank you for registation',DURATION.LENGTH_SHORT);
  }
  componentWillReceiveProps (newProps) {
    console.log('newProps');
  }

  checkBoxChanged()
      {
      this.state.terms ? this.setState({terms: false}):this.setState({terms: true});
      }

  render() {
  //  this.refs.toast.show('Thank you for registation',DURATION.LENGTH_SHORT);
    //console.log(this.props);
        const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
            <Toast
               ref="toast"
               style={{backgroundColor:'#ff704d',zIndex:999,}}
               position='top'
               positionValue={10}
               fadeInDuration={750}
               fadeOutDuration={750}
               opacity={0.9}
               textStyle={{color:'white'}}
           />
            <Content>
               <View style={[ styles.mainLogoPanel ]}>
                 <View style={styles.logoPanel}>
                  {/*<Image
                   style={styles.logo}
                   source={logo}
                 />*/}
                  </View>
                <Text style={styles.largeWithoutFontText} >
                  <Text style={{fontFamily:'Roboto'}}>Welcome to </Text>
                  <Text style={{fontFamily:'Roboto'}} >Family </Text>
                  <Text style={{fontFamily:'Roboto'}}>Inheritance </Text>
                  <Text style={{fontFamily:'Roboto'}}>Management!</Text>
                </Text>
                
                <View style={[styles.contentPanel,{marginTop:30}]}>
                
                <Text style={styles.infoTextLarge} >
                  <Text style={{color:'#C22E93'}}>►  </Text>
                  <Text style={{fontStyle:'italic'}}>Family Inheritance Management</Text>
                  <Text  style={{}}>, easily guides you through the difficult times of managing the challenges of
                  your family’s inheritance. </Text>
                </Text>

                <Text style={styles.infoTextLarge} >
                   <Text style={{}}>
                        <Text style={{color:'#C22E93'}}>►  </Text>
                        This application saves you from spending excessive amounts of time and money, by not having to hire expensive professionals before it’s absolutely necessary.
                    </Text>
                  </Text>

                  <Text style={styles.infoTextLarge} >
                        <Text style={{color:'#C22E93'}}>►  </Text>
                        We are going to quickly walk you through a short series of questions to setup your Workspace which will apply to your unique situation.
                  </Text>

                  <Text style={styles.infoTextLarge} >
                        <Text style={{color:'#C22E93'}}>►  </Text>
                        We will automatically set up several basic categories for you.
                        You are free to add more categories, at any time.
                  </Text>
                </View>
                <View style={{alignItems:'center',marginTop:(height/14)}}>
                    <Button onPress={() => navigate('QuestionOnePage')} 
                            style={[styles.btn, {
                                      backgroundColor:'#A1C1E5'
                                    , paddingLeft:  0
                                    , paddingRight: 0
                                    }]}
                            rounded
                            >
                        <Text style={[styles.btnText, {fontSize:25, color:'#39414B'}]}>{this.state.buttonText}</Text>
                    </Button>

                    {/*<Button onPress={() => navigate('Login')} style={{alignItems:'center',marginTop:10,height:60,width:250,justifyContent:'center'}} rounded dark>
                       <Text style={{color:'white',justifyContent:'center',fontSize:20}}>Logout</Text>
                    </Button>*/}
                </View>
                </View>

                </Content>
            </View>
    );

  }

}
function mapStateToProps(state, ownProps) {
  console.log(state);
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //actions: bindActionCreators(registration, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeScreen);
