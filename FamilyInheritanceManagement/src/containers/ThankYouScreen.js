import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity,
  AsyncStorage


} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,} from 'native-base';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as regStepCompleteAction from '../actions/stepcompleted.action';
import Switch from 'react-native-customisable-switch';
import styles from '../styles/questionAnswerStyle';
var {height, width} = Dimensions.get('window');
var logo = require('../images/logo.png');


class ThankYouScreen extends React.Component {
  constructor(props) {
    super(props);
    Text.defaultProps.allowFontScaling=false;
    this.state = {
      userid:0,

    };
  }
  componentWillMount(){

     AsyncStorage.getItem('userDetails').then((value) =>{
       if(value){
       let userData = JSON.parse(value)
       console.log('userDetails',value);
       console.log('userDetails',userData.userid);
       this.setState({userid:userData.userid});
          }
        })
      }

  static navigationOptions = {
    title:'',
    header:null,

  };
  stepCompleted(navigate){
      //navigate('Workspace')
      this.props.actions.regStepCompleted(this.state.userid);
  }

  render() {

        const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Content>
           <View style={styles.mainLogoPanel}>
               <View style={styles.logoPanel}>
                    <Image
                    style={{width: 90, height: 150,}}
                    source={logo}
                    />
                </View>
            </View>
                <View style={{alignItems:'center',marginTop:50,}}>
                  <Text style={{fontSize:35,fontFamily:'Roboto',marginBottom:55,fontWeight:'500'}}>Thank you for your help</Text>
                  <Text style={{fontSize:25,fontFamily:'Roboto',}}>We now know enough about your family members estate to help you </Text>
                  <Text style={{fontSize:25,fontFamily:'Roboto',}}>understand how to execute it. </Text>
                        <View style = {{marginTop:200}}>
                              <Button style={{
                                alignItems:'center',
                                marginLeft:(width/20),
                                height:60,width:300,
                                justifyContent:'center'}} onPress={() => this.stepCompleted(navigate)} rounded dark>
                                 <Text style={styles.btnText}  >Completed, Continue</Text>
                               </Button>
                        </View>
                </View>
            </Content>
      </View>

    );
  }
}
function mapStateToProps(state, ownProps) {
  return {
   response: state.auth.response
  };
}

function mapDispatchToProps(dispatch) {
  return {
   actions: bindActionCreators(regStepCompleteAction, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ThankYouScreen);
