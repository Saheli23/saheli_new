import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image
} from 'react-native';
import * as login from '../actions/auth.action';
import { addNavigationHelpers,StackNavigator } from 'react-navigation';
import { Container, Header, Content, Form, Item, Input,Label,Button,Spinner } from 'native-base';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import Toast, {DURATION} from 'react-native-easy-toast'
import { NavigationActions } from 'react-navigation';
import * as EmailValidator from 'email-validator';
// import RegistrationScreen from './containers/RegistrationScreen';
// import WelcomeScreen from './containers/WelcomeScreen';
// import QuestionOnePageScreen from './containers/QuestionOnePageScreen';

var {height, width} = Dimensions.get('window');
var logo = require('../images/logo.png');
import { Images} from '../Themes';

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    Text.defaultProps.allowFontScaling=false;
    this.state = {
      email:'',
      password:'',
      isLoading:false,
    };
  }
  static navigationOptions = {
    title: '',
    headerTintColor: '#ddd',
    header:null,


  };
  componentWillReceiveProps (newProps) {
    console.log('newPropslogin', newProps);
    if(newProps &&  newProps.user && newProps.user.loggedIn == false && newProps.user.invalid == true){
      this.refs.toast.show('Invalid Credentials',4000);
      this.setState({isLoading:false});
    }
    if(newProps &&  newProps.user && newProps.user.loggedIn == true ){
      this.refs.toast.show('',50);
      //this.setState({isLoading:false});
    }


  }

  loginSubmit(navigate){
     const {password,email} = this.state;

     let emailValidate = EmailValidator.validate(email);
     if(email == ""){
          this.refs.toast.show('Please provide Email Address!',DURATION.LENGTH_SHORT);
      }
      else if(!emailValidate){
        this.refs.toast.show('Please provide Valid Email Address!',DURATION.LENGTH_SHORT);
      }
      else if(password == ""){
             this.refs.toast.show('Please provide Password!',DURATION.LENGTH_SHORT);
      }
     else{
           this.setState({isLoading:true});
           this.props.actions.login(email,password);

         }
   }

  render() {
    const { navigate } = this.props.navigation;
  //  console.log(this.props.navigation);
    return (
      <View style={{backgroundColor:'#eeeeee',height:height}}>
            <Toast
               ref="toast"
               style={{backgroundColor:'#ff704d'}}
               position='top'
               positionValue={50}
               fadeInDuration={750}
               fadeOutDuration={750}
               opacity={0.9}
               textStyle={{color:'white'}}
           />
            <Content>

               <View style={{alignItems:'center',marginTop:(height/8)}}>
                 <View style={{flexDirection:'row', flex:1,alignItems:'center',justifyContent: 'center',}}>

                   {/*<Image
                     style={{width: 80, height: 130,}}
                     source={logo} />
                   <Text style={{fontSize:40,marginBottom:5,fontFamily:'Roboto',fontWeight: '400',}} >
                   Family Inheritance Management</Text>*/}
                   <Image
                   source={Images.largeLogo}
                  />
                  </View>
                 {this.state.isLoading &&
                   <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: -20, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
                   <Spinner color='#737373' />
                   </View>
                 }
                 <View style={{marginTop:40}}>
                  <Item regular style={{marginTop:(35),width:(width/1.28),height:(height/14),alignItems:'center',backgroundColor:'white',borderRadius: 12
                                        , paddingLeft:15
                                        , paddingRight:15
                                        , paddingTop:30
                                        , paddingBottom:30
                                        }}>
                     <Input autoCorrect={false} placeholder='Email' autoCapitalize='none' keyboardType = 'email-address' onSubmitEditing={(event) => { this.refs.password._root.focus(); }} returnKeyType="next" ref='email' onChangeText={ (text) => this.setState({ email: text }) }
                     style={{ fontSize:25 }}
                     />
                   </Item>
                   <Item regular style={{
                                          marginTop:10
                                        , width:(width/1.28)
                                        , height:(height/14)
                                        , alignItems:'center',backgroundColor:'white',borderRadius: 12
                                        , paddingLeft:15
                                        , paddingRight:15
                                        , paddingTop:30
                                        , paddingBottom:30
                                        }}>
                     <Input autoCorrect={false} returnKeyType="done"  ref='password'  onChangeText={ (text) => this.setState({ password: text }) } placeholder='Password' secureTextEntry={true}
                     style={{ fontSize:25 }}
                     />
                   </Item>
                  </View>
                 <View style={{ alignItems:'center'
                              , marginTop:60
                              }}>

                   <Button onPress={() => this.loginSubmit(navigate)} style={{alignItems:'center',marginTop:0,height:(height/12.8),width:170,justifyContent:'center',backgroundColor:'#A1C1E5'}} rounded >
                      <Text style={{ color:'#39414B',justifyContent:'center'
                                   , fontSize:25
                                   }}  >Login</Text>
                   </Button>
                   <Button onPress={() => navigate('Registration')} style={{
                           alignItems:'center',marginTop:20,height:(height/12.8),width:170
                         , justifyContent:'center',borderColor:'#C22E93'
                         //, backgroundColor:'#E9D0F2'
                         , backgroundColor:'#C24A9A'
                         , borderWidth: 0}} rounded light>
                      <Text style={{color:'#ffffff',justifyContent:'center',fontSize:25}}>New Account</Text>
                   </Button>
                   <Text style={{color: '#4A7FBA',marginTop:25,fontSize:25}} onPress={() =>navigate('ForgotPassword')}>
                    Forgot Password
                    </Text>
                  </View>
              </View>
             </Content>

      </View>);
  }
}

function mapStateToProps(state, ownProps) {
  console.log(state);
  return {
    nav:  state.nav,
    user: state.auth.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(login, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
// const FamilyInheritanceManagement = StackNavigator({
//   Home: { screen: HomeScreen },
//   Registration: { screen: RegistrationScreen },
//   Welcome:{screen:WelcomeScreen},
//   QuestionOnePage:{screen:QuestionOnePageScreen}
// });
//
//  AppRegistry.registerComponent('FamilyInheritanceManagement', () => FamilyInheritanceManagement);
