import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity,
  AsyncStorage,
  FlatList,
  ScrollView


} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,ActionSheet,Drawer} from 'native-base';
import * as workspaceInfo from '../actions/workspace.action';
import { StackNavigator } from 'react-navigation';
import { Switch } from 'react-native-switch';
import styles from '../styles/workspaceStyle';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { Images} from '../Themes'
import { Col, Row, Grid } from "react-native-easy-grid";
var {height, width} = Dimensions.get('window');
var logo = require('../images/text_logo.png');
import SideBar from '../SideBar/SideBar';
//const FONT_SIZE = (width/51.2);
//const FONT_SIZE = (width/51.2) > 20 ? 20: 20;
const FONT_SIZE = (width/76.8) > 16 ? 16: 16;

 class WorkspaceScreen extends React.Component {
   constructor(props) {
     super(props);
     Text.defaultProps.allowFontScaling=false;
     this.state = {
       userName : '',
       userid   : ''

     };
   }
  static navigationOptions  = ({ navigation }) => ({
    title:<Text>Family Inheritance Management</Text>,
    headerMode: 'screen',
    headerTintColor: '#ddd',
    header: <View style={{height:(height/9),marginLeft:20,alignItems:'center'}}>
              <View style={{flexDirection:'row',marginTop:10}} >
                {/*<Image
                 style={{width: width/2.9, height: 60,}}
                 source={logo}/>*/}
                 <Image
                  style={styles.logoPanel}
                  source={Images.modifiedLogo}/>
                 <View style={{marginTop:20,height:30,flexDirection:'row',left:250}}>
                   <TouchableOpacity onPress={() =>  navigation.navigate('UserProfile')}>
                      <Image
                        style={{width: width/40, height: 30,marginRight:20}}
                        source={Images.userIcon}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() =>  this.openDrawer()}>
                    <Image
                     style={{width: width/40, height: 20,marginTop:5}}
                     source={Images.menuIcon}/>
                     </TouchableOpacity>
                    </View>
               </View>
            </View> ,

  });
  componentWillMount(){

    AsyncStorage.getItem('userDetails').then((value) =>{
      if(value){
      let userData = JSON.parse(value)
      // console.log('userDetails',value);
      // console.log('userDetails',userData.userid);
      this.setState({userid:userData.userid});
      this.props.actions.workspaceInfo(userData.userid);
      }
    });
    //console.log('userid',this.state.userid);

    //  console.log('newPropsWorkspace',width,height);
    //  console.log('user name',this.state.userName);
  }

  componentWillReceiveProps (newProps) {
  //  console.log('newPropsWorkspace');
    if(newProps && newProps.response){

    //  console.log(newProps.response);
    }
    if(newProps && newProps.regStepComplte){
      //console.log(newProps.regStepComplte);
    }
  }

  render() {
      // console.log('workspaceprops',this.props)
        const { navigate } = this.props.navigation;
        closeDrawer = () => {
      this.drawer._root.close()
    };
    openDrawer = () => {
    //  console.log(this.drawer._root);
      this.drawer._root.toggle()
    };
    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<SideBar navigator={this.props.navigation} />}
        onClose={() => this.drawer._root.close()} >

     <View style={styles.container}>
        <Content scrollEnabled={false}>

         <View style={styles.workspaceContainer}>
          <View style={styles.defaultModules}>
           <TouchableOpacity style={styles.modulesDefault} onPress={() =>navigate('FamilyInformation')}>
                <Image
                 style={styles.familyInfoIcon}
                 source={Images.familyInfoIcon}
                />
                <View style={styles.sepIcon}></View>
                  <View style={styles.moduleLabel}>
                    <Text style={styles.moduleName}>Family</Text>
                    <Text style={styles.moduleName}>Information</Text>
                  </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() =>navigate('FinancialManagement')} style={styles.modulesDefault}>
                <Image
                 style={styles.financialIcon}
                 source={Images.financialIcon}
                />
                <View style={styles.sepIcon}></View>
                {/*<Image
                 style={styles.sepIcon}
                 source={Images.sepIcon}
                />*/}
                  <View style={styles.moduleLabel}>
                    <Text style={styles.moduleName}>Financial</Text>
                    <Text style={styles.moduleName}>Management</Text>
                  </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.modulesDefault}>
                {FONT_SIZE >20 &&

                  <Image
                   style={styles.financialReportIcon}
                   source={Images.financialReportIcon}
                  />
                }
                {FONT_SIZE <=20 &&

                <Image
                 style={styles.financialReportIconSmall}
                 source={Images.financialReportIcon}
                />
              }
                {FONT_SIZE >20 &&
                 <View style={{marginLeft:5}}></View>
                }
                {FONT_SIZE <=20 &&
                  <View style={styles.sepIcon}></View>
                 }
                {/*<Image
                 style={styles.sepIcon}
                 source={Images.sepIcon}
                />*/}
                  <View style={styles.moduleLabel}>
                    <Text style={styles.moduleName}>Summary</Text>
                    <Text style={styles.moduleName}>Financial</Text>
                    <Text style={styles.moduleName}>Report</Text>

                  </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.modulesDefault}>
                <Image
                 style={styles.attachIcon}
                 source={Images.attachIcon}
                />
                <View style={styles.sepIcon}></View>
                {/*<Image
                 style={styles.sepIcon}
                 source={Images.sepIcon}
                />*/}
                  <View style={styles.moduleLabel}>
                    <Text style={styles.moduleName}>Documents &</Text>
                    <Text style={styles.moduleName}>Attachments</Text>
                  </View>
              </TouchableOpacity>
          {/* <View style={{flexDirection:'row',alignItems:'center',justifyContent: 'center',}}>

            <Image
             style={{width: 80, height: 130,}}
             source={logo}
            />
            <Text style={{fontSize:40,marginBottom:5,fontFamily:'Roboto',fontWeight: '400',}}>Welcome to workspace</Text>

           </View>
           <Text style={{fontSize:30,}}>{this.props.user.userDetails.username}</Text>*/}
           </View>
          <ScrollView>
             <View style={styles.extraModules}>

             {this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.travel == 1 &&
                 <TouchableOpacity style={styles.modules}>
                   <Image
                    style={styles.travelIcon}
                    source={Images.travelIcon}
                   />
                   <View style={[styles.sepIcon,{marginLeft:1}]}></View>
                   {/*<Image
                    style={styles.sepIcon}
                    source={Images.sepIcon}
                   />*/}
                   <View style={styles.moduleLabel}>
                     <Text style={styles.moduleName}>Travel</Text>
                  </View>
               </TouchableOpacity>
             }
             {this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.real_estate == 1 &&
               <TouchableOpacity style={styles.modules}>
                 <Image
                  style={styles.estateIcon}
                  source={Images.estateIcon}
                 />
                 <View style={styles.sepIcon}></View>
                 {/*<Image
                  style={{height:45,marginLeft:20,marginRight:10}}
                  source={Images.sepIcon}
                 />*/}
                   <View style={styles.moduleLabel}>
                     <Text style={styles.moduleName}>Real Estate</Text>
                  </View>
               </TouchableOpacity>
             }
             {this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.will == 1 &&
               <TouchableOpacity style={styles.modules}>
                 <Image
                  style={styles.willIcon}
                  source={Images.willIcon}
                 />
                  <View style={styles.sepIcon}></View>
                 {/*<Image
                  style={styles.sepIcon}
                  source={Images.sepIcon}
                 />*/}
                   <View style={styles.moduleLabel}>
                     <Text style={styles.moduleName}>Will</Text>
                  </View>
               </TouchableOpacity>
             }
             {this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.trust == 1 &&
               <TouchableOpacity style={styles.modules}>
                 <Image
                  style={styles.trustIcon}
                  source={Images.trustIcon}
                 />
                 <View style={styles.sepIcon}></View>
                {/*<Image
                 style={styles.sepIcon}
                 source={Images.sepIcon}
                />*/}
                   <View style={styles.moduleLabel}>
                     <Text style={styles.moduleName}>Trust</Text>
                  </View>
               </TouchableOpacity>
             }
             { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.funeral == 1 &&
                   <TouchableOpacity onPress={() =>navigate('FuneralDetails')} style={styles.modules}>
                     <Image
                      style={styles.funeralIcon}
                      source={Images.funeralIcon}
                     />
                     <View style={styles.sepIcon}></View>
                    {/*<Image
                     style={styles.sepIcon}
                     source={Images.sepIcon}
                    />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Funeral</Text>
                       </View>
                   </TouchableOpacity>
                 }
                   { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.vehicles == 1 &&
                   <TouchableOpacity style={styles.modules}>
                     <Image
                      style={styles.vehiclesIcon}
                      source={Images.vehiclesIcon}
                     />
                     <View style={styles.sepIcon}></View>
                     {/*<Image
                      style={{height:45,marginLeft:5,marginRight:10}}
                      source={Images.sepIcon}
                     />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Vehicles</Text>
                       </View>
                   </TouchableOpacity>
                 }
                 { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.charitable_giving == 1 &&
                   <TouchableOpacity style={styles.modules}>
                     <Image
                      style={styles.charityIcon}
                      source={Images.charityIcon}
                     />
                     <View style={styles.sepIcon}></View>
                    {/*<Image
                     style={styles.sepIcon}
                     source={Images.sepIcon}
                    />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Charitable</Text>
                         <Text style={styles.moduleName}>Giving</Text>
                       </View>
                   </TouchableOpacity>
                 }
                 {this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.lodging_meals == 1 &&
                   <TouchableOpacity style={styles.modules}>
                   <Image
                    style={styles.lodgingIcon}
                    source={Images.lodgingIcon}
                   />
                    <View style={styles.sepIcon}></View>
                     <View style={styles.moduleLabel}>
                       <Text style={styles.moduleName}>Lodging &</Text>
                       <Text style={styles.moduleName}>Meals</Text>
                     </View>
                 </TouchableOpacity>
               }
                 { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.assisted_living == 1 &&
                   <TouchableOpacity onPress={() =>navigate('AssistedLiving')} style={styles.modules}>
                     <Image
                      style={styles.charityIcon}
                      source={Images.assistedLivingIcon}
                     />
                     <View style={styles.sepIcon}></View>
                    {/*<Image
                     style={styles.sepIcon}
                     source={Images.sepIcon}
                    />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Assisted </Text>
                         <Text style={styles.moduleName}>Living</Text>
                       </View>
                   </TouchableOpacity>
                 }
                 { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.insurance == 1 &&
                   <TouchableOpacity style={styles.modules}>
                     <Image
                      style={styles.insuranceIcon}
                      source={Images.insuranceIcon}
                     />
                     <View style={styles.sepIcon}></View>
                    {/*<Image
                     style={styles.sepIcon}
                     source={Images.sepIcon}
                    />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Insurance</Text>
                       </View>
                   </TouchableOpacity>
                 }
                 { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.business == 1 &&
                   <TouchableOpacity style={styles.modules}>
                     <Image
                      style={styles.charityIcon}
                      source={Images.businessIcon}
                     />
                     <View style={styles.sepIcon}></View>
                    {/*<Image
                     style={styles.sepIcon}
                     source={Images.sepIcon}
                    />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Business</Text>
                       </View>
                   </TouchableOpacity>
                 }
                 { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.moving_storage == 1 &&
                   <TouchableOpacity style={styles.modules}>
                     <Image
                      style={styles.movingStorageIcon}
                      source={Images.movingStorageIcon}
                     />
                     <View style={styles.sepIcon}></View>
                    {/*<Image
                     style={styles.sepIcon}
                     source={Images.sepIcon}
                    />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Moving &</Text>
                         <Text style={styles.moduleName}>Storage </Text>
                       </View>
                   </TouchableOpacity>
                 }
                 { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.hospice == 1 &&
                   <TouchableOpacity onPress={() =>navigate('HospiceDetails')}  style={styles.modules}>
                     <Image
                      style={styles.hospiceIcon}
                      source={Images.hospiceIcon}
                     />
                     <View style={styles.sepIcon}></View>
                    {/*<Image
                     style={styles.sepIcon}
                     source={Images.sepIcon}
                    />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Hospice</Text>
                       </View>
                   </TouchableOpacity>
                 }
            </View>
          </ScrollView>
          {/*  <View style={styles.secondRowModules}>
          { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.funeral == 1 &&
                <TouchableOpacity style={styles.modules}>
                  <Image
                   style={styles.funeralIcon}
                   source={Images.funeralIcon}
                  />
                  <Image
                   style={styles.sepIcon}
                   source={Images.sepIcon}
                  />
                    <View style={styles.moduleLabel}>
                      <Text style={{fontSize:FONT_SIZE}}>Funeral</Text>
                    </View>
                </TouchableOpacity>
              }
                { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.vehicles == 1 &&
                <TouchableOpacity style={styles.modules}>
                  <Image
                   style={styles.vehiclesIcon}
                   source={Images.vehiclesIcon}
                  />
                  <Image
                   style={{height:45,marginLeft:5,marginRight:10}}
                   source={Images.sepIcon}
                  />
                    <View style={styles.moduleLabel}>
                      <Text style={{fontSize:FONT_SIZE}}>Vehicles</Text>
                    </View>
                </TouchableOpacity>
              }

                <TouchableOpacity style={styles.modules}>
                  <Image
                   style={styles.lodgingIcon}
                   source={Images.lodgingIcon}
                  />
                  <Image
                   style={styles.sepIcon}
                   source={Images.sepIcon}
                  />
                    <View style={styles.moduleLabel}>
                      <Text style={{fontSize:FONT_SIZE}}>Lodging and </Text>
                      <Text style={{fontSize:FONT_SIZE}}>Meals</Text>
                    </View>
                </TouchableOpacity>

              { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.charitable_giving == 1 &&
                <TouchableOpacity style={styles.modules}>
                  <Image
                   style={styles.charityIcon}
                   source={Images.charityIcon}
                  />
                  <Image
                   style={styles.sepIcon}
                   source={Images.sepIcon}
                  />
                    <View style={styles.moduleLabel}>
                      <Text style={{fontSize:FONT_SIZE}}>Charitable</Text>
                      <Text style={{fontSize:FONT_SIZE}}>Giving</Text>
                    </View>
                </TouchableOpacity>
              }
             </View>*/}

        </View>
        <View style={styles.btnPanel}>
          <Button onPress={() =>navigate('AddWorkspaceModule')}  style={styles.addAnotherBtn} rounded dark>
             <Text style={styles.btnText}>Add Another</Text>
          </Button>
        </View>
      </Content>
    </View>

      </Drawer>
    );
  }
}
function mapStateToProps(state, ownProps) {
//  console.log(state);
  return {
    nav:  state.nav,
    user: state.auth.user,
    workspaceInfo: state.auth.response,
    regStepComplte:state.auth.stepCompleted
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(workspaceInfo, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkspaceScreen);
