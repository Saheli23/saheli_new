import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity,
  AsyncStorage,
  FlatList,
  ScrollView


} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,ActionSheet} from 'native-base';
import * as workspaceInfo from '../actions/workspace.action';
import { StackNavigator } from 'react-navigation';
import { Switch } from 'react-native-switch';
import styles from '../styles/workspaceStyle';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { Images} from '../Themes'
import { Col, Row, Grid } from "react-native-easy-grid";
var {height, width} = Dimensions.get('window');
var logo = require('../images/text_logo.png');
import AlertMessage from '../components/AlertMessage';
const FONT_SIZE = 20;

 class AddWorkspaceModuleScreen extends React.Component {
   constructor(props) {
     super(props);
     Text.defaultProps.allowFontScaling=false;
     this.state = {
       userName : '',
       userid   : ''

     };
   }
  static navigationOptions  = ({ navigation }) => ({
    title:<Text>Family Inheritance Management</Text>,
    headerMode: 'screen',
    headerTintColor: '#ddd',
    header: <View style={{height:(height/9),marginLeft:20,alignItems:'center'}}>
              <View style={{flexDirection:'row',marginTop:10}} >
              {/*<Image
               style={{width: width/2.9, height: 60,}}
               source={logo}/>*/}
               <Image
                style={styles.logoPanel}
                source={Images.modifiedLogo}/>
                 <View style={{marginTop:20,height:30,flexDirection:'row',left:250}}>
                   <Image
                    style={{width: width/40, height: 30,marginRight:20}}
                    source={Images.userIcon}/>
                    <Image
                     style={{width: width/40, height: 20,marginTop:5}}
                     source={Images.menuIcon}/>
                    </View>
               </View>
            </View> ,

  });
  componentWillMount(){

    AsyncStorage.getItem('userDetails').then((value) =>{
      if(value){
      let userData = JSON.parse(value)
    //  console.log('userDetails',value);
    //  console.log('userDetails',userData.userid);
    this.setState({userid:userData.userid});
    this.props.actions.workspaceInfo(userData.userid);
      }
    });
    //console.log('userid',this.state.userid);

    //  console.log('newPropsWorkspace',width,height);
    //  console.log('user name',this.state.userName);
  }

  componentWillReceiveProps (newProps) {
  //  console.log('newPropsWorkspace');
    if(newProps && newProps.response){

    //  console.log(newProps.response);
    }

  }

  render() {
    //   console.log('workspaceprops',this.props)
        const { navigate } = this.props.navigation;
    return (
     <View style={styles.container}>
        <Content scrollEnabled={false}>

         <View style={styles.workspaceContainer}>

           <View style={{margin:20}}>
           {(this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails)
             && (this.props.workspaceInfo.workspaceDetails.travel ==0
             || this.props.workspaceInfo.workspaceDetails.funeral ==0
             || this.props.workspaceInfo.workspaceDetails.hospice ==0
             || this.props.workspaceInfo.workspaceDetails.assisted_living ==0
             || this.props.workspaceInfo.workspaceDetails.real_estate ==0
             || this.props.workspaceInfo.workspaceDetails.business ==0
             || this.props.workspaceInfo.workspaceDetails.vehicles ==0
             || this.props.workspaceInfo.workspaceDetails.moving_storage ==0
             || this.props.workspaceInfo.workspaceDetails.charitable_giving ==0
             || this.props.workspaceInfo.workspaceDetails.will ==0
             || this.props.workspaceInfo.workspaceDetails.trust ==0
             || this.props.workspaceInfo.workspaceDetails.insurance ==0) &&


                <Text style={{fontSize:25,fontWeight:'600'}}>Tap to add any modules in Workspace</Text>
           }

             {this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails
               && this.props.workspaceInfo.workspaceDetails.travel !=0
               && this.props.workspaceInfo.workspaceDetails.funeral !=0
               && this.props.workspaceInfo.workspaceDetails.hospice !=0
               && this.props.workspaceInfo.workspaceDetails.assisted_living !=0
               && this.props.workspaceInfo.workspaceDetails.real_estate !=0
               && this.props.workspaceInfo.workspaceDetails.business !=0
               && this.props.workspaceInfo.workspaceDetails.vehicles !=0
               && this.props.workspaceInfo.workspaceDetails.moving_storage !=0
               && this.props.workspaceInfo.workspaceDetails.charitable_giving !=0
               && this.props.workspaceInfo.workspaceDetails.will !=0
               && this.props.workspaceInfo.workspaceDetails.trust !=0
               && this.props.workspaceInfo.workspaceDetails.insurance !=0 &&


                  <AlertMessage title='No more modules to add'  />
             }
           </View>
           <ScrollView>
           <View style={styles.extraModules}>

           {this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.travel == 0 &&
               <TouchableOpacity onPress={() => this.props.actions.updateWorkspace(this.state.userid,'travel')} style={styles.modules}>
                 <Image
                  style={styles.travelIcon}
                  source={Images.travelIcon}
                 />
                 <View style={styles.sepIcon}></View>
                {/*<Image
                 style={styles.sepIcon}
                 source={Images.sepIcon}
                />*/}
                   <View style={styles.moduleLabel}>
                     <Text style={styles.moduleName}>Travel</Text>
                  </View>
               </TouchableOpacity>
             }
              {this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.lodging_meals == 0 &&
             <TouchableOpacity onPress={() => this.props.actions.updateWorkspace(this.state.userid,'lodging_meals')} style={styles.modules}>
               <Image
                style={styles.lodgingIcon}
                source={Images.lodgingIcon}
               />
               <View style={styles.sepIcon}></View>
                 <View style={styles.moduleLabel}>
                   <Text style={styles.moduleName}>Lodging &</Text>
                   <Text style={styles.moduleName}>Meals</Text>
                 </View>
             </TouchableOpacity>
           }
             {this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.real_estate == 0 &&
               <TouchableOpacity onPress={() => this.props.actions.updateWorkspace(this.state.userid,'real_estate')} style={styles.modules}>
                 <Image
                  style={styles.estateIcon}
                  source={Images.estateIcon}
                 />
                 {/*<Image
                  style={{height:45,marginLeft:20,marginRight:10}}
                  source={Images.sepIcon}
                 />*/}
                  <View style={styles.sepIcon}></View>
                   <View style={styles.moduleLabel}>
                     <Text style={styles.moduleName}>Real Estate</Text>
                  </View>
               </TouchableOpacity>
             }
             {this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.will == 0 &&
               <TouchableOpacity onPress={() => this.props.actions.updateWorkspace(this.state.userid,'will')} style={styles.modules}>
                 <Image
                  style={styles.willIcon}
                  source={Images.willIcon}
                 />
                 <View style={styles.sepIcon}></View>
                {/*<Image
                 style={styles.sepIcon}
                 source={Images.sepIcon}
                />*/}
                   <View style={styles.moduleLabel}>
                     <Text style={styles.moduleName}>Will</Text>
                  </View>
               </TouchableOpacity>
             }
             {this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.trust == 0 &&
               <TouchableOpacity onPress={() => this.props.actions.updateWorkspace(this.state.userid,'trust')} style={styles.modules}>
                 <Image
                  style={styles.trustIcon}
                  source={Images.trustIcon}
                 />
                 <View style={styles.sepIcon}></View>
                {/*<Image
                 style={styles.sepIcon}
                 source={Images.sepIcon}
                />*/}
                   <View style={styles.moduleLabel}>
                     <Text style={styles.moduleName}>Trust</Text>
                  </View>
               </TouchableOpacity>
             }
             { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.funeral == 0 &&
                   <TouchableOpacity onPress={() => this.props.actions.updateWorkspace(this.state.userid,'funeral')} style={styles.modules}>
                     <Image
                      style={styles.funeralIcon}
                      source={Images.funeralIcon}
                     />
                     <View style={styles.sepIcon}></View>
                    {/*<Image
                     style={styles.sepIcon}
                     source={Images.sepIcon}
                    />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Funeral</Text>
                       </View>
                   </TouchableOpacity>
                 }
                   { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.vehicles == 0 &&
                   <TouchableOpacity onPress={() => this.props.actions.updateWorkspace(this.state.userid,'vehicles')} style={styles.modules}>
                     <Image
                      style={styles.vehiclesIcon}
                      source={Images.vehiclesIcon}
                     />
                     <View style={styles.sepIcon}></View>
                     {/*<Image
                      style={{height:45,marginLeft:5,marginRight:10}}
                      source={Images.sepIcon}
                     />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Vehicles</Text>
                       </View>
                   </TouchableOpacity>
                 }
                 { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.charitable_giving == 0 &&
                   <TouchableOpacity onPress={() => this.props.actions.updateWorkspace(this.state.userid,'charitable_giving')} style={styles.modules}>
                     <Image
                      style={styles.charityIcon}
                      source={Images.charityIcon}
                     />
                     <View style={styles.sepIcon}></View>
                    {/*<Image
                     style={styles.sepIcon}
                     source={Images.sepIcon}
                    />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Charitable</Text>
                         <Text style={styles.moduleName}>Giving</Text>
                       </View>
                   </TouchableOpacity>
                 }

                 { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.assisted_living == 0 &&
                   <TouchableOpacity onPress={() => this.props.actions.updateWorkspace(this.state.userid,'assisted_living')} style={styles.modules}>
                     <Image
                      style={styles.charityIcon}
                      source={Images.assistedLivingIcon}
                     />
                     <View style={styles.sepIcon}></View>
                    {/*<Image
                     style={styles.sepIcon}
                     source={Images.sepIcon}
                    />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Assisted </Text>
                         <Text style={styles.moduleName}>Living</Text>
                       </View>
                   </TouchableOpacity>
                 }
                 { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.insurance == 0 &&
                   <TouchableOpacity onPress={() => this.props.actions.updateWorkspace(this.state.userid,'insurance')} style={styles.modules}>
                     <Image
                      style={styles.insuranceIcon}
                      source={Images.insuranceIcon}
                     />
                     <View style={styles.sepIcon}></View>
                    {/*<Image
                     style={styles.sepIcon}
                     source={Images.sepIcon}
                    />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Insurance</Text>
                       </View>
                   </TouchableOpacity>
                 }
                 { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.business == 0 &&
                   <TouchableOpacity onPress={() => this.props.actions.updateWorkspace(this.state.userid,'business')} style={styles.modules}>
                     <Image
                      style={styles.charityIcon}
                      source={Images.businessIcon}
                     />
                     <View style={styles.sepIcon}></View>
                    {/*<Image
                     style={styles.sepIcon}
                     source={Images.sepIcon}
                    />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Business</Text>
                       </View>
                   </TouchableOpacity>
                 }
                 { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.moving_storage == 0 &&
                   <TouchableOpacity onPress={() => this.props.actions.updateWorkspace(this.state.userid,'moving_storage')} style={styles.modules}>
                     <Image
                      style={styles.movingStorageIcon}
                      source={Images.movingStorageIcon}
                     />
                     <View style={styles.sepIcon}></View>
                    {/*<Image
                     style={styles.sepIcon}
                     source={Images.sepIcon}
                    />*/}
                       <View style={styles.moduleLabel}>
                         <Text style={styles.moduleName}>Moving &</Text>
                         <Text style={styles.moduleName}>Storage </Text>
                       </View>
                   </TouchableOpacity>
                 }
                 { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.hospice == 0 &&
                   <TouchableOpacity onPress={() => this.props.actions.updateWorkspace(this.state.userid,'hospice')} style={styles.modules}>
                   <Image
                    style={styles.hospiceIcon}
                    source={Images.hospiceIcon}
                   />
                   <View style={styles.sepIcon}></View>
                  {/*<Image
                   style={styles.sepIcon}
                   source={Images.sepIcon}
                  />*/}
                     <View style={styles.moduleLabel}>
                       <Text style={styles.moduleName}>Hospice</Text>
                     </View>
                   </TouchableOpacity>
                 }
            </View>
          </ScrollView>
          {/*  <View style={styles.secondRowModules}>
          { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.funeral == 1 &&
                <TouchableOpacity style={styles.modules}>
                  <Image
                   style={styles.funeralIcon}
                   source={Images.funeralIcon}
                  />
                  <Image
                   style={styles.sepIcon}
                   source={Images.sepIcon}
                  />
                    <View style={styles.moduleLabel}>
                      <Text style={styles.moduleName}>Funeral</Text>
                    </View>
                </TouchableOpacity>
              }
                { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.vehicles == 1 &&
                <TouchableOpacity style={styles.modules}>
                  <Image
                   style={styles.vehiclesIcon}
                   source={Images.vehiclesIcon}
                  />
                  <Image
                   style={{height:45,marginLeft:5,marginRight:10}}
                   source={Images.sepIcon}
                  />
                    <View style={styles.moduleLabel}>
                      <Text style={styles.moduleName}>Vehicles</Text>
                    </View>
                </TouchableOpacity>
              }

                <TouchableOpacity style={styles.modules}>
                  <Image
                   style={styles.lodgingIcon}
                   source={Images.lodgingIcon}
                  />
                  <Image
                   style={styles.sepIcon}
                   source={Images.sepIcon}
                  />
                    <View style={styles.moduleLabel}>
                      <Text style={styles.moduleName}>Lodging and </Text>
                      <Text style={styles.moduleName}>Meals</Text>
                    </View>
                </TouchableOpacity>

              { this.props.workspaceInfo && this.props.workspaceInfo.workspaceDetails && this.props.workspaceInfo.workspaceDetails.charitable_giving == 1 &&
                <TouchableOpacity style={styles.modules}>
                  <Image
                   style={styles.charityIcon}
                   source={Images.charityIcon}
                  />
                  <Image
                   style={styles.sepIcon}
                   source={Images.sepIcon}
                  />
                    <View style={styles.moduleLabel}>
                      <Text style={styles.moduleName}>Charitable</Text>
                      <Text style={styles.moduleName}>Giving</Text>
                    </View>
                </TouchableOpacity>
              }
             </View>*/}

        </View>
        <View style={styles.btnPanel}>
          <Button onPress={() =>this.props.navigation.goBack()}  style={styles.addAnotherBtn} rounded light>
             <Text style={styles.btnText}>Cancel</Text>
          </Button>
        </View>
      </Content>
    </View>
    );
  }
}
function mapStateToProps(state, ownProps) {
  //console.log(state);
  return {
    nav:  state.nav,
    user: state.auth.user,
    workspaceInfo: state.auth.response
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(workspaceInfo, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddWorkspaceModuleScreen);
