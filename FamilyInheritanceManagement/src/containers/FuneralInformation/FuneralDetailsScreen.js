import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image,
  Picker,
  Keyboard,
  TouchableOpacity,
  AsyncStorage,
  FlatList,
  ScrollView


} from 'react-native';
import { Container, Header, Content, Form, Item, Input,Label,Button,Radio,ActionSheet,Left,Icon} from 'native-base';
import * as actionFinancialInformation from '../../actions/financialinformation.action';
import { StackNavigator } from 'react-navigation';
import { Switch } from 'react-native-switch';
import styles from '../../styles/FuneralInformationStyle';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { Images} from '../../Themes'
import { Col, Row, Grid } from "react-native-easy-grid";
var {height, width} = Dimensions.get('window');
var logo = require('../../images/text_logo.png');
import OpenFile from 'react-native-doc-viewer';
import * as _ from 'lodash';
const FONT_SIZE = 20;

 class FuneralDetailsScreen extends React.Component {
   constructor(props) {
     super(props);
     Text.defaultProps.allowFontScaling=false;
     this.state = {
       userName : '',
       userid   : ''

     };
   }
  static navigationOptions = ({ navigation }) => ({
    title:<Text>Family Inheritance Management</Text>,
    headerMode: 'screen',
    headerTintColor: '#ddd',
    header: <View style={styles.mainHeaderPanel}>
              <View style={styles.subHeader} >

                <Button style={styles.backBtn} transparent onPress={() => navigation.goBack()}>
                {/*<Icon name="arrow-back" />*/}
                <Image
                   style={styles.backImage}
                   source={Images.backArrow}/>
                 <Text style={styles.backText}>Back</Text>
                </Button>
                {/*<Image
                 style={{width: width/2.9, height: 60,}}
                 source={logo}/>*/}
                 <Image
                 style={styles.logoPanel}
                 source={Images.modifiedLogo}/>
                 <View style={styles.mainMenuPanel}>
                 <TouchableOpacity onPress={() =>  navigation.navigate('UserProfile')}>
                   <Image
                    style={styles.userIconStyle}
                    source={Images.userIcon}/>
                  </TouchableOpacity>
                    <Image
                     style={styles.menuIconStyle}
                     source={Images.menuIcon}/>
                    </View>
               </View>
            </View> ,


  });
  componentWillMount(){

    AsyncStorage.getItem('userDetails').then((value) =>{
      if(value){
      let userData = JSON.parse(value)
      console.log('userDetails',value);
      console.log('userDetails',userData.userid);
    this.setState({userid:userData.userid});
  //  this.props.actions.workspaceInfo(userData.userid);
      }
    });
    //console.log('userid',this.state.userid);

    //  console.log('newPropsWorkspace',width,height);
    //  console.log('user name',this.state.userName);
  }

  componentWillReceiveProps (newProps) {
    console.log('newPropsWorkspace');
    if(newProps && newProps.response){

      console.log(newProps.response);
    }

  }
  handlePress (documentPath) {
     console.log('inside fn',documentPath.uri);
    OpenFile.openDoc([{
      url:documentPath.uri,
      fileName:"sample"
      }], (error, url) => {
        if (error) {
          console.error(error);
        } else {
          console.log(url)
        }
    })
  }
  render() {
       console.log('workspaceprops',this.props)
        const { navigate } = this.props.navigation;
        const { params } = this.props.navigation.state;
        //let contacts_details= JSON.parse(params.financialInfo.contacts_details);
        //let filePath=JSON.parse(params.financialInfo.filePath);
        //console.log(filePath);
        //let documentPath=JSON.parse(params.financialInfo.documentPath);

    return (
     <View style={styles.container}>
        <Content scrollEnabled={false}>

         <View style={styles.workspaceContainer}>
             <View style={styles.centerLabel}>
               <Text style={styles.centerLabelText}>Funeral</Text>
             </View>
             <ScrollView style={styles.memberInformation}>
             <View style={styles.editPanel}>
                <TouchableOpacity >
                  <Image
                   style ={[styles.editIcons,{marginLeft:570}]}
                   source={Images.editIcon}
                  />
                </TouchableOpacity>
                <TouchableOpacity >
                    <Image
                     style ={[styles.deleteIcons,{marginLeft:15}]}
                     source={Images.deleteIcon}
                    />
                 </TouchableOpacity>
               </View>
               <View style={styles.memberInnerContain}>
                  <View style={styles.memberNamePanel}>
                     <Text style={styles.memberName}>Alex Rob William</Text>
                     <Text style={styles.priceText}>$36,000</Text>
                   </View>

                     <View style={styles.datePanel}>
                       <Text style={[styles.detailsText,{}]}>Date: 10/20/2017</Text>
                       <Text style={[styles.detailsText,{}]}>Time: 10.30 AM</Text>
                     </View>
                      <View style={styles.typeServicePanel}>
                        <Text style={[styles.detailsText,{}]}>Time of service: Church</Text>
                      </View>
               </View>

               <View style ={styles.underLine} />
                 <View style={styles.fullDetails}>
                  <Text style={{fontSize:20,color:"black",marginBottom:10,fontFamily:'Roboto',fontWeight:'600'}}>Alton Funeral Home</Text>
                    <View style={{flexDirection:'row'}}>
                       <View style={{flexDirection:'column',width:(width/2.75)}}>
                         <View style={{flexDirection:'row',marginBottom:25}}>
                             <Image
                              style ={[styles.blueMailIcon,{marginTop:5,}]}
                              source={Images.mailIcon}
                             />

                             <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>alex@gmail.com</Text>
                          </View>
                          <View style={{flexDirection:'row',width:(width/3.50)}}>
                              <Image
                               style ={[styles.websiteIcon,{marginTop:5,}]}
                               source={Images.websiteIcon}
                              />

                              <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>www.damvy.com</Text>
                           </View>
                       </View>
                       <View style={{flexDirection:'column',width:(width/4)}}>
                         <View style={{flexDirection:'row',marginBottom:25}}>
                             <Image
                              style ={[styles.callIcon,{marginTop:5,}]}
                              source={Images.callIcon}
                             />

                             <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>234-234-1245</Text>
                          </View>
                          <View style={{flexDirection:'row'}}>
                              <Image
                               style ={[styles.faxIcon,{marginTop:5,}]}
                               source={Images.faxIcon}
                              />

                              <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>1234567</Text>
                           </View>
                       </View>
                       <View style={{flexDirection:'column',width:(width/4)}}>
                         <View style={{flexDirection:'row',marginBottom:20}}>
                             <Image
                              style ={[styles.locationIcon,{marginTop:5,}]}
                              source={Images.locationIcon}
                             />

                             <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>44, Shirley Ave, West Chicago, FL 32904</Text>
                          </View>

                       </View>
                    </View>


                   <View style ={styles.secondUnderLine} />
                   {/*{  !_.isEmpty(contacts_details[0]) &&
                    <Text style={{fontSize:20,color:"#666666",marginBottom:10,marginTop:20}}>Contact Person</Text>
                   }
                   {contacts_details.length >0  && contacts_details.map((row, i) => {
                     console.log('contacts_details',contacts_details);


                       console.log('epmty object');
                       { if( !_.isEmpty(contacts_details[i]))
                     return (
                       <View key={'cntp_'+i} style={{flexDirection:'row',marginBottom:15}}>

                         <View style={{flexDirection:'row',width:(width/2.80)}}>
                           <Image
                            style ={[styles.userIcon,{marginTop:5,}]}
                            source={Images.userIcon}
                           />
                           <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>{contacts_details[i].contactName}</Text>
                         </View>
                       <View style={{flexDirection:'row',width:(width/4)}}>
                           <Image
                            style ={[styles.callIcon,{marginTop:5,}]}
                            source={Images.callIcon}
                           />
                          <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>{contacts_details[i].contactPhone}</Text>
                       </View>
                       <View style={{flexDirection:'row',width:(width/3.20)}}>
                         <Image
                          style ={[styles.blueMailIcon,{marginTop:5,}]}
                          source={Images.mailIcon}
                         />

                         <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>{contacts_details[i].contactEmail}</Text>
                         </View>

                       </View>
                     );}})}
                     <View style={{flexDirection:'row',marginBottom:10,marginTop:10}}>

                     {(filePath !=null) &&
                        <View><Text style={[styles.detailsText]}>File Details: </Text>
                       <TouchableOpacity onPress={this.handlePress.bind(this,filePath)} >
                         <Image style={{width:(width/7), height: (height/8), resizeMode: 'contain',marginRight:10,marginTop:15}} source={filePath} />

                     </TouchableOpacity>
                     </View>
                     }

                     {(documentPath !='') &&
                       <View><Text style={[styles.detailsText]}>File Details: </Text>
                       <TouchableOpacity onPress={this.handlePress.bind(this,documentPath)}>
                        <Text style={[styles.detailsText]}>{documentPath.fileName}</Text>
                       </TouchableOpacity>
                       </View>
                     }
                     </View>*/}

                     <Text style={{fontSize:20,color:"black",marginBottom:10,fontFamily:'Roboto',fontWeight:'600',marginTop:20}}>Burial Services</Text>
                     <View style={{flexDirection:'row'}}>
                        <View style={{flexDirection:'column',width:(width/2.75)}}>
                          <View style={{flexDirection:'row',marginBottom:25}}>
                              <Image
                               style ={[styles.blueMailIcon,{marginTop:5,}]}
                               source={Images.mailIcon}
                              />

                              <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>devin@gmail.com</Text>
                           </View>
                           <View style={{flexDirection:'row',width:(width/3.50)}}>
                               <Image
                                style ={[styles.websiteIcon,{marginTop:5,}]}
                                source={Images.websiteIcon}
                               />

                               <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>www.devin.com</Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'column',width:(width/4)}}>
                          <View style={{flexDirection:'row',marginBottom:25}}>
                              <Image
                               style ={[styles.callIcon,{marginTop:5,}]}
                               source={Images.callIcon}
                              />

                              <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>234-453-4343</Text>
                           </View>
                           <View style={{flexDirection:'row'}}>
                               <Image
                                style ={[styles.faxIcon,{marginTop:5,}]}
                                source={Images.faxIcon}
                               />

                               <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>345678</Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'column',width:(width/4)}}>
                          <View style={{flexDirection:'row',marginBottom:20}}>
                              <Image
                               style ={[styles.locationIcon,{marginTop:5,}]}
                               source={Images.locationIcon}
                              />

                              <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>44, Shirley Ave</Text>
                           </View>

                        </View>
                     </View>

                     <Text style={{fontSize:20,color:"black",marginBottom:10,fontFamily:'Roboto',fontWeight:'600',marginTop:20}}>Special and Specific Intrtuction</Text>
                     <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
                     <View style ={styles.secondUnderLine} />
                     <Text style={{fontSize:20,color:"#74A8E1",marginBottom:10,fontFamily:'Roboto',fontWeight:'600',marginTop:20}}>Cost of Burial Services: $10,000</Text>
                     <View style ={styles.secondUnderLine} />
                     <Text style={{fontSize:20,color:"black",marginBottom:10,fontFamily:'Roboto',fontWeight:'600',marginTop:20}}>Officiation of the Ceremony</Text>
                     <View style={{flexDirection:'row'}}>
                        <View style={{flexDirection:'column',width:(width/2.75)}}>
                          <View style={{flexDirection:'row',marginBottom:25}}>
                              <Image
                               style ={[styles.userIcon,{marginTop:5,}]}
                               source={Images.userIcon}
                              />

                              <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>Jon William</Text>
                           </View>
                           <View style={{flexDirection:'row',width:(width/3.50)}}>
                               <Image
                                style ={[styles.websiteIcon,{marginTop:5,}]}
                                source={Images.websiteIcon}
                               />

                               <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>www.devin.com</Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'column',width:(width/4)}}>
                          <View style={{flexDirection:'row',marginBottom:25}}>
                              <Image
                               style ={[styles.callIcon,{marginTop:5,}]}
                               source={Images.callIcon}
                              />

                              <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>345-245-3546</Text>
                           </View>
                           <View style={{flexDirection:'row'}}>
                               <Image
                                style ={[styles.faxIcon,{marginTop:5,}]}
                                source={Images.faxIcon}
                               />

                               <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>2345672</Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'column',width:(width/4)}}>
                        <View style={{flexDirection:'row',marginBottom:25}}>
                            <Image
                             style ={[styles.blueMailIcon,{marginTop:5,}]}
                             source={Images.mailIcon}
                            />

                            <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>devin@gmail.com</Text>
                         </View>
                          <View style={{flexDirection:'row',marginBottom:20}}>
                              <Image
                               style ={[styles.locationIcon,{marginTop:5,}]}
                               source={Images.locationIcon}
                              />

                              <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>44, Shirley Ave</Text>
                           </View>

                        </View>
                     </View>
                      <View style ={styles.secondUnderLine} />
                      <Text style={{fontSize:20,color:"black",marginBottom:10,fontFamily:'Roboto',fontWeight:'600',marginTop:20}}>Death Certificate</Text>
                      <View style={{flexDirection:'row'}}>
                         <View style={{flexDirection:'column',width:(width/2.75)}}>
                           <View style={{flexDirection:'row',marginBottom:25}}>
                               <Image
                                style ={[styles.blueMailIcon,{marginTop:5,}]}
                                source={Images.mailIcon}
                               />

                               <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>devin@gmail.com</Text>
                            </View>
                            <View style={{flexDirection:'row',width:(width/3.50)}}>
                                <Image
                                 style ={[styles.websiteIcon,{marginTop:5,}]}
                                 source={Images.websiteIcon}
                                />

                                <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>www.luria.com</Text>
                             </View>
                         </View>
                         <View style={{flexDirection:'column',width:(width/4)}}>
                           <View style={{flexDirection:'row',marginBottom:25}}>
                               <Image
                                style ={[styles.callIcon,{marginTop:5,}]}
                                source={Images.callIcon}
                               />

                               <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>345-243-2567</Text>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <Image
                                 style ={[styles.faxIcon,{marginTop:5,}]}
                                 source={Images.faxIcon}
                                />

                                <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>2355672</Text>
                             </View>
                         </View>
                         <View style={{flexDirection:'column',width:(width/4)}}>
                           <View style={{flexDirection:'row',marginBottom:20}}>
                               <Image
                                style ={[styles.locationIcon,{marginTop:5,}]}
                                source={Images.locationIcon}
                               />

                               <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>44, Shirley Ave</Text>
                            </View>

                         </View>
                      </View>

                      <Text style={{fontSize:20,color:"black",marginBottom:10,fontFamily:'Roboto',fontWeight:'600',marginTop:20}}>Coroner for the City or Country of</Text>
                      <Text style={[styles.detailsText,{marginLeft:10,flexWrap:'wrap'}]}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
                      <View style ={styles.secondUnderLine} />
               </View>
           </ScrollView>

        </View>
        <View style={{alignItems:'center',marginLeft:(width/2.60),marginTop:(height/130)}}>
          <Button  style={{alignItems:'center',marginTop:10,height:(height/18),width:(width/4),justifyContent:'center',backgroundColor:'#6791C1',}} rounded light>
             <Text style={{color:'white',justifyContent:'center',fontSize:20}}>Add Another</Text>
          </Button>
        </View>
      </Content>
    </View>
    );
  }
}
function mapStateToProps(state, ownProps) {
  console.log(state);
  return {
    // nav:  state.nav,
    // user: state.auth.user,
    // workspaceInfo: state.auth.response
  };
}

function mapDispatchToProps(dispatch) {
  return {
  //  actions: bindActionCreators(actionFinancialInformation, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FuneralDetailsScreen);
