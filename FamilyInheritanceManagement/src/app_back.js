import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Dimensions,
  Image
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Container, Header, Content, Form, Item, Input,Label,Button } from 'native-base';
import RegistrationScreen from './containers/RegistrationScreen';
import WelcomeScreen from './containers/WelcomeScreen';
import QuestionOnePageScreen from './containers/QuestionOnePageScreen';

var {height, width} = Dimensions.get('window');
var logo = require('./images/logo.png');
class HomeScreen extends React.Component {
  static navigationOptions = {
    title: '',
    headerTintColor: '#ddd',
    header: null,

  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{backgroundColor:'#f2f2f2',height:height}}>

             <Content >
               <View style={{alignItems:'center',marginTop:(height/9)}}>


                 <View style={{flexDirection:'row', flex:1,alignItems:'center',justifyContent: 'center',}}>

                 <Image
                   style={{width: 80, height: 130,}}
                   source={logo}
                 />
                 <Text style={{fontSize:40,marginBottom:5,fontFamily:'Iowan Old Style',fontWeight: '400',}} >
                 Family Inheritance Management</Text>

                 </View>
                <Item regular style={{marginTop:50,width:(width/1.6),height:60,alignItems:'center',backgroundColor:'white',borderRadius: 3}}>
                   <Input placeholder='Email' />
                 </Item>
                 <Item regular style={{marginTop:10,width:(width/1.6),height:60,alignItems:'center',backgroundColor:'white',borderRadius: 3}}>
                   <Input placeholder='Password' />
                 </Item>
                 <View style={{alignItems:'center'}}>
                   <Button style={{alignItems:'center',marginTop:30,height:60,width:190,justifyContent:'center'}} rounded dark>
                      <Text style={{color:'white',justifyContent:'center',fontSize:25}}>Login</Text>
                   </Button>
                   <Button onPress={() => navigate('Registration')} style={{alignItems:'center',marginTop:10,height:60,width:190,justifyContent:'center',borderColor:'black',borderWidth: 2}} rounded light>
                      <Text style={{color:'black',justifyContent:'center',fontSize:20}}>New Account</Text>
                   </Button>
                   <Text style={{color: '#FF007F',marginTop:20,fontSize:25}}>
                    Forgot Password
                    </Text>
                  </View>
              </View>
             </Content>

      </View>);
  }
}

const FamilyInheritanceManagement = StackNavigator({
  Home: { screen: HomeScreen },
  Registration: { screen: RegistrationScreen },
  Welcome:{screen:WelcomeScreen},
  QuestionOnePage:{screen:QuestionOnePageScreen}
});

 AppRegistry.registerComponent('FamilyInheritanceManagement', () => FamilyInheritanceManagement);
