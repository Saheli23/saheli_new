import { Platform, StyleSheet,Dimensions } from 'react-native';
var {height, width} = Dimensions.get('window');
import { Fonts} from '../Themes';
const styles = StyleSheet.create({
	container:{
    backgroundColor:'#f2f2f2',
    height:height
  },
  mainLogoPanel:{
    alignItems:'center',
    marginTop:(height/18)
  },
  logoPanel:{
    flexDirection:'row',
    flex:1,
    alignItems:'center',
    justifyContent: 'center',
  },
	headerLogoPanel:{
    marginTop:10
	},
	mainQuesPanel:{
		marginLeft:(width/20.48),
		// marginTop:35,
		marginTop:(width/29),
		marginRight:(width/14.6285714)
	},
	eachQuestionSection:{
		flexDirection:'row',
		marginTop:(width/35)
	},
  questions:{
    flex:1,
    fontSize:25,
    fontFamily:Fonts.type.Roboto,
    justifyContent: 'flex-start',
    alignItems:'flex-start'
  },
  answerSwitch:{
    justifyContent: 'flex-end',
    alignItems:'flex-end'
  },
  bottomPanel:{
    flexDirection:'row',
    marginTop:(width/10)
  },
  continueBtn:{
    alignItems:'center',
    marginLeft:(width/4),
    height:(height/12.8),
		width:(width/5.38),
    justifyContent:'center',
		backgroundColor:'#A1C1E5'
  },
  btnText:{
    color:'#39414B',
    justifyContent:'center',
    fontSize:25
  },
  pageNoText:{
    color: 'black',
		marginTop:30,
    fontSize:20
  }
});

export default styles;
