import { Platform, StyleSheet,Dimensions } from 'react-native';
var {height, width} = Dimensions.get('window');
import { Fonts} from '../Themes';
const styles = StyleSheet.create({
	container:{
    //backgroundColor:'#aecbf9',
		backgroundColor:'#f2f2f2',
    height:height,

  },
	logoPanel:{
    marginTop:10
	},
	workspaceContainer:{
		alignItems:'center',
		height:(height/1.27),
		width:width,
		backgroundColor:'#70A9DC'
	},
	centerLabel:{
		marginTop:20,
		marginBottom:20,
		//marginLeft:80,
		flexDirection:'row',
    justifyContent:'center'
	},
	priceText:{
		marginRight:(width/38.4),
		fontSize:(width/45),
		fontWeight:'600',
		fontFamily:Fonts.type.Roboto,
	},
	centerLabelListing:{
		marginTop:20,
		marginBottom:20,
		//marginLeft:40,
		flexDirection:'row',
		width:width/1.10,
		justifyContent:'flex-end'
	},
	centerLabelText:{
		fontSize:25,
		fontWeight:'600',
		fontFamily:Fonts.type.Roboto,
	},
	centerLabelTextListing:{
		fontSize:25,
		fontWeight:'600',
		fontFamily:Fonts.type.Roboto,
		width:(width/3.80),
		marginRight:(width/7.87)
	},
	memberDetails:{
		marginTop:30,
		marginLeft:30,
		marginRight:30,
		height:(height/1.85),

	},
	infoText:{
		flexWrap:'wrap',
		marginLeft:18,
		color:'#666666',
		fontSize:18,
		fontWeight:'500',
		fontFamily:Fonts.type.Roboto,
	},
	memberName:{
		fontSize:20,
		fontWeight:'600',
		fontFamily:Fonts.type.Roboto,
	},
	totalPrice:{
		fontSize:18,
		color:'#74A8E1',
		fontFamily:Fonts.type.Roboto,
	},
	accountNumber:{
		fontSize:18,
		color:'#666666',
		fontFamily:Fonts.type.Roboto,
	},
	sepLine:{
		borderBottomColor: '#ddd',
		borderBottomWidth: 2,
		marginTop:15,
		marginBottom:20
	},
	locationIcons:{
		height:(height/26),
		width:(width/59)
	},
	callIcons:{
		height:(height/36),
		width:(width/49)
	},
	mailIcon:{
		height:(height/40.42),
		width:(width/39)
	},
	userIcon:{
		height:28,
		width:25
	},
	faxIcon:{
		height:25,
		width:30
	},
	addIcon:{
		height:30,
		width:30
	},
	websiteIcon:{
		height:28,
		width:29
	},
	gridViewIcon:{
		height:25,
		width:25,
		marginRight:10
		//left:275
	},
	tableViewIcon:{
		height:25,
		width:28,
		//left:290
	},
	addressPanel:{
		flexDirection:'row',
		margin:10,
    marginLeft:1
	},
  devideTextbox:{
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    //alignItems: 'center',
    width:(width/1.1)
  },
  inputBox:{
    marginTop:(height/48),
    width:(width/2.2),
    height:48,
    backgroundColor:'white',
    borderRadius: 3,
    marginBottom:10,
    marginRight:15,
    borderColor:'#ddd'
  },
  noteInputBox:{
    marginTop:(height/48),
    width:(width/1.10),
    height:100,
    backgroundColor:'white',
    borderRadius: 3,
    //margin:10,
    borderColor:'#ddd'
  },
	uploadBox:{
		marginTop:(height/48),
    width:(width/7),
    height:(height/8),
    backgroundColor:'white',
    borderRadius: 3,
    //margin:10,
    borderColor:'#ddd',
		alignItems:'center'
	},
  placeholderFont:{
    fontSize:(width/56.88),
    color:'#595959',
		fontFamily:Fonts.type.Roboto,
  },
	memberInformation:{
		backgroundColor:'#FFFFFF',
		margin:10,
		width:(width/1.05),
		//height:(height/1.56),
		//flex:1
	},
	memberInnerContain:{
		marginLeft:40,
		marginRight:40,
		marginBottom:40,
		flexDirection:'row',
		height:20
	},
	editIcons:{
		height:25,
		width:25,

	},
	deleteIcons:{
		height:25,
		width:20,
	},
	birthDateIcon:{
		height:25,
		width:25,
	},
	birthDayPanel:{
		marginLeft:30,
		marginTop:-20,
		flexDirection:'row'
	},
	detailsText:{
		fontSize:18,
		color:'#666666',
		marginTop:5,
		marginLeft:5,
		fontFamily:Fonts.type.Roboto,
	},
	priceText:{
		fontSize:20,
		color:'#74A8E1',
		marginTop:5,
		//marginLeft:5,
		fontFamily:Fonts.type.Roboto,
	},
	underLine:{
		borderBottomColor: '#ddd',
		borderBottomWidth: 2,
		marginLeft:35,
		marginRight:35,
		marginTop:15

	},
	secondUnderLine:{
		borderBottomColor: '#ddd',
		borderBottomWidth: 2,
		//marginLeft:30,
		marginRight:35,
		marginTop:20
	},
	fullDetails:{
		marginLeft:40,
		marginTop:20
	},
	innerDetailsFirst:{
		flexDirection:'row',
		width:(width/4)
	},
	innerDetailsSecond:{
		flexDirection:'row',
		marginTop:20
	},
	blueMailIcon:{
		height:22,
		width:30
	},
	callIcon:{
		height:28,
		width:28
	},
	locationIcon:{
		height:34,
		width:20
	},

	faxIcon:{
		height:25,
		width:28
	},
	bluePhoneIcon:{
		height:25,
		width:25
	},
	blueSSNIcon:{
		height:30,
		width:25
	},
	noteSection:{
		marginTop:10,
		flexWrap:'wrap',
		marginRight:25,

	},
	accountPanel:{
		flexDirection:'column',
		width:(width/3.5)
	},
	memberNamePanel:{
		flexDirection:'column',
		width:(width/2.80)
	},
	editPanel:{
		flexDirection:'row',
		marginLeft:(width/3.14),
		marginTop:10
	},
	datePanel:{
		flexDirection:'column',
		width:(width/4.2)
	},
  typeServicePanel:{
		flexDirection:'column',
		width:(width/5)
	},
	mainHeaderPanel:{
 	 height:(height/9),
 	 marginLeft:20,
 	 alignItems:'center'
  },
	subHeader:{
 	 flexDirection:'row',
 	 marginTop:10
  },
	backBtn:{
 	 right:240,
 	 marginTop:12
  },
  backImage:{
 	 width: 15,
 	 height: 15,
 	 marginTop:1,
  },
  backText:{
 	 fontSize:20,
 	 fontWeight:'600',
 	 fontFamily:Fonts.type.Roboto,
 	 color:'#666666',
  },
  textLogo:{
 	 width: width/2.9,
 	 height: 60,
  },
  mainMenuPanel:{
 	 marginTop:18,
 	 height:30,
 	 flexDirection:'row',
 	 left:200
  },
  userIconStyle:{
 	 width: width/39,
 	 height: 32,
 	 marginRight:20
  },
  menuIconStyle:{
 	 width: width/40,
 	 height: 20,
 	 marginTop:8
  }

});

export default styles;
