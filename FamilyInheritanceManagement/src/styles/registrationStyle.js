import { Platform, StyleSheet,Dimensions } from 'react-native';
var {height, width} = Dimensions.get('window');
import { Fonts} from '../Themes';
const styles = StyleSheet.create({
	container:{
    backgroundColor:'#f2f2f2',
    height:height
  },
	mainContainer:{
		alignItems:'center',
		marginTop:(height/48)
	},
	devideTextbox:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    //width:(width/1.8)
  },
  inputBox:{
    //width:(width/6),
    width:(width/1.24),
    height:42.95,
    alignItems:'center',
    backgroundColor:'white',
    borderRadius: 3,
    borderColor:'#ddd'
  },
  placeholderFont:{
    fontSize:18,
		fontFamily:Fonts.type.Roboto,
  },
  buttonPanel:{
    marginTop:50
  },
	ageRange:{
		borderWidth:2,
		borderColor:'#ddd',
		height:40,
		width:150,
		marginTop:20,
		marginRight:10,
		alignItems:'center'
	},
	selectedAgeRange:{
   backgroundColor: '#aecbf9',
	 borderWidth:2,
	 borderColor:'#ddd',
	 height:40,
	 width:150,
	 marginTop:20,
	 marginRight:10,
	 alignItems:'center'
 },
 regBtn:{
	 alignItems:'center',
	 marginTop:5,
	 height:60,
	 width:190,
	 justifyContent:'center',
	//  borderColor:'black',
	//  borderWidth: 2,
	 backgroundColor:'#A1C1E5'
 },
 regText:{
	 color:'#39414B',
	 justifyContent:'center',
	 fontSize:25,
	 fontFamily:Fonts.type.Roboto,
	 //fontWeight:'600'
 },
 loginLinkView:{
	 alignItems:'center',
	 marginTop:10,
	 height:60,
	 width:190,
	 justifyContent:'center'
 },
 cancelButtonText:{
	 //color:'#FF007F',
	 color:'#39414B',
	 justifyContent:'center',
	 fontSize:25,
	 //fontFamily:Fonts.type.Roboto,
	 //fontWeight:'600'
 }

});

export default styles;
