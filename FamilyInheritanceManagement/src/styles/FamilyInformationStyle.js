import { Platform, StyleSheet,Dimensions } from 'react-native';
var {height, width} = Dimensions.get('window');
import { Fonts} from '../Themes';
const styles = StyleSheet.create({
	container:{
    //backgroundColor:'#aecbf9',
		backgroundColor:'#f2f2f2',
    height:height,

  },
	logoPanel:{

		marginTop:10
	},
	workspaceContainer:{
		alignItems:'center',
		height:(height/1.25),
		//height:height,
		width:width,
		backgroundColor:'#70A9DC'
	},
	centerLabel:{
		margin:(width/51.2),
		flexDirection:'row'
	},
	centerLabelText:{
		fontSize:(width/40.96),
		fontWeight:'600',
		fontFamily:Fonts.type.Roboto,
	},
	memberDetails:{

		height:(height/3.2),
		marginTop:(height/25.6),
		marginLeft:(height/25.6),
		marginRight:(height/25.6)
	},
	infoText:{
		flexWrap:'wrap',
		marginLeft:(width/56.88),
		color:'#666666',
		fontSize:(width/64),
		fontWeight:'500',
		fontFamily:Fonts.type.Roboto,
	},
	memberName:{
		fontSize:(width/46.54),
		fontWeight:'600',
		fontFamily:Fonts.type.Roboto,
	},
	memberRelation:{
		fontSize:(width/56.88),
		color:'#74A8E1',
		fontFamily:Fonts.type.Roboto,
	},
	sepLine:{
		borderBottomColor: '#ddd',
		borderBottomWidth: 2,
		marginTop:(width/68.26),
		marginBottom:(height/38.4)
	},
	locationIcons:{
		height:(height/26),
		width:(width/59)
	},
	callIcons:{
		height:(height/37),
		width:(width/51)
	},
	mailIcon:{
		height:(height/40.42),
		width:(width/40.96)
	},
	gridViewIcon:{
		height:25,
		width:(width/40.96),
		left:320
	},
	tableViewIcon:{
		height:25,
		width:28,
		left:335
	},
	addressPanel:{
		flexDirection:'row',
		margin:10
	},
  devideTextbox:{
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    //alignItems: 'center',
    width:(width/1.1)
  },
  inputBox:{
    marginTop:(height/48),
    width:(width/2.2),
    height:48,
    backgroundColor:'white',
    borderRadius: 3,
    marginBottom:10,
    marginRight:(width/68.26),
    borderColor:'#ddd'
  },
  noteInputBox:{
    marginTop:(height/48),
    width:(width/1.10),
    height:100,
    backgroundColor:'white',
    borderRadius: 3,
    //margin:10,
    borderColor:'#ddd'
  },
  placeholderFont:{
    fontSize:(width/56.88),
    color:'#595959',
		fontFamily:Fonts.type.Roboto,
  },
	memberInformation:{
		backgroundColor:'#FFFFFF',
		margin:10,
		width:(width/1.10),
		//height:(height/2.45)
	},
	memberInnerContain:{
		margin:40,
		flexDirection:'row',
		height:(height/25.6)
	},
	editIcons:{
		height:25,
		width:(width/40.96),

	},
	deleteIcons:{
		height:25,
		width:20,
	},
	birthDateIcon:{
		height:25,
		width:(width/40.96),
	},
	birthDayPanel:{
		marginLeft:40,
		marginTop:-20,
		flexDirection:'row'
	},
	detailsText:{
		fontSize:(width/56.88),
		color:'#666666',
		marginTop:5,
		marginLeft:5,
		fontFamily:Fonts.type.Roboto,
	},
	underLine:{
		borderBottomColor: '#ddd',
		borderBottomWidth: 1,
		marginLeft:35,
		marginRight:35,
		marginTop:15

	},
	fullDetails:{
		marginLeft:50,
		marginTop:20
	},
	innerDetailsFirst:{
		flexDirection:'row',
		width:(width/4)
	},
	innerDetailsSecond:{
		flexDirection:'row',
		marginTop:20
	},
	blueMailIcon:{
		height:22,
		width:29.50
	},
	bluePhoneIcon:{
		height:23.50,
		width:24
	},
	blueSSNIcon:{
		height:(height/25.6),
		width:28
	},
	noteSection:{
		marginTop:(width/68.26),
		flexWrap:'wrap',
		marginRight:(width/40.96),
		marginBottom:(height/25.6)
 },
 mainHeaderPanel:{
	 height:(height/9),
	 marginLeft:20,
	 alignItems:'center'
 },
 subHeader:{
	 flexDirection:'row',
	 marginTop:10
 },
 backBtn:{
	 right:240,
	 marginTop:12
 },
 backImage:{
	 width: 15,
	 height: 15,
	 marginTop:1,
 },
 backText:{
	 fontSize:20,
	 fontWeight:'600',
	 fontFamily:Fonts.type.Roboto,
	 color:'#666666',
 },
 textLogo:{
	 width: width/2.9,
	 height: 60,
 },
 mainMenuPanel:{
	 marginTop:18,
	 height:30,
	 flexDirection:'row',
	 left:200
 },
 userIconStyle:{
	 width: width/39,
	 height: 32,
	 marginRight:20
 },
 menuIconStyle:{
	 width: width/40,
	 height: 20,
	 marginTop:8
 },
 expandablePanel:{
	 marginTop:15,
   flexDirection:'row',
 },
 panelText:{
	 flex:1,
	 color:'white',
	 fontSize:(width/51.2),
	 fontFamily:Fonts.type.Roboto,
	 fontWeight:'600'
 },
 arrowIcon:{
	 justifyContent:'flex-end',
	 alignItems:'flex-end'
 },
 sepLine:{
	 marginTop:(width/57),
	 marginBottom:(width/68)
 },
 sepLineIcon:{
	 width:(width/1.1)
 }

});

export default styles;
