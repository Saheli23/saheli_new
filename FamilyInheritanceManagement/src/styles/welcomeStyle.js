import { Platform, StyleSheet,Dimensions } from 'react-native';
var {height, width} = Dimensions.get('window');
const BOTTOM_OFFSET = 0;
import { Fonts} from '../Themes';
const styles = StyleSheet.create({
	container:{
    backgroundColor:'#f2f2f2',
    height:height
  },
  mainLogoPanel:{
    alignItems:'center',
    marginTop:(height/18)
  },
  logoPanel:{
    flexDirection:'row',
    flex:1,
    alignItems:'center',
    justifyContent: 'center',
  },
  logo:{
    width: (width/11.37),
    height: (height/5.12)
  },
	headerLogoPanel:{
    marginTop:10
	},
  btnText:{
    //color:'white',
    color:'#464348',
    justifyContent:'center',
    fontSize:(width/51.2)
  },
  largeText:{
    fontSize:(width/34.13),
    marginBottom:(width/51.2),
    fontFamily:Fonts.type.Roboto,
    fontWeight: '400',
  },
	largeWithoutFontText:{
    fontSize:(width/34.13)*1.15,
    marginBottom:(width/51.2),
    //fontFamily:Fonts.type.Roboto,
    fontWeight: '400',
  },
  contentPanel:{
    width:width/1.28,
    height:(height/2.20),
    marginTop:15
  },
  contentPanelTerms:{ // for terms and conditions
    width:width/1.28,
    //height:(height/2.20),
    //height:(height/1.5),
    //height:(height/1.6),
    height:(height/1.36 - BOTTOM_OFFSET),
    marginTop:15
  },
  infoText:{
    fontSize:(width/51.2),
    fontFamily:Fonts.type.Roboto,
    marginBottom:(width/51.2)*1.5,
    //paddingLeft:(height/19.2), // warning - we may need this
    lineHeight:(width/51.2)*1.5,
    //fontFamily:'Roboto',
  },
  infoTextLarge:{
    fontSize:25,//(width/51.2),
    marginBottom:(width/51.2)*1.7,
    //paddingLeft:(height/19.2), // warning - we may need this
    lineHeight:(width/51.2)*1.5,
    //fontFamily:'Roboto',
  },
  infoTextWithoutfont:{
    fontSize:(width/51.2),
    marginBottom:(width/51.2)*1.5,
    //fontFamily:'Roboto',
    paddingLeft:(height/19.2),
    lineHeight:(width/51.2)*1.5,
  },
  infoTextJustified:{
    fontSize:(width/51.2),
    marginBottom:(width/51.2)*1.5,
    paddingLeft:0,
    lineHeight:(width/51.2)*1.5,
    //fontFamily:'Roboto',
    textAlign:'justify'
  },
  btn:{
    alignItems:'center',
    marginTop:(width/51.2),
    height:(height/12.8),
    //width:(width/4.096),
    width:(width/4.5),
    justifyContent:'center',
		backgroundColor:'#464348'
  },
	termBtn:{
		alignItems:'center',
		marginTop:(width/51.2),
    //marginLeft:(width/4),
    height:(height/12.8),
		width:(width/5.38),
    justifyContent:'center',
		backgroundColor:'#A1C1E5'
 },
 scrollBtn:{
    alignItems:'center',
    //marginTop:(width/51.2),
    //marginLeft:(width/4),
    height:(height/12.8),
    width:(60),
    justifyContent:'center',
    backgroundColor:'#A1C1E5',
    borderWidth:1,
    borderColor:'#5E7085',
    shadowColor:'#3E4A58',
    shadowOffset:{ width:  0,
                   height: 2
                 },
    shadowOpacity:0.5,
    shadowRadius:3,
 },
 loginLinkView:{
	 alignItems:'center',
	 marginTop:10,
	 height:60,
	 width:190,
	 justifyContent:'center'
 },
 cancelButtonText:{
	 //color:'#FF007F',
	 color:'#39414B',
	 justifyContent:'center',
	 fontSize:25,
	 //fontFamily:Fonts.type.Roboto,
	 //fontWeight:'600'
 },
 bottomOffset: BOTTOM_OFFSET

});

export default styles;
