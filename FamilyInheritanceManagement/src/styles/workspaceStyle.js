import { Platform, StyleSheet,Dimensions } from 'react-native';
var {height, width} = Dimensions.get('window');
import { Fonts} from '../Themes';
const FONT_SIZE = (width/76.8) > 16 ? 16: 16;
const styles = StyleSheet.create({
	container:{
    //backgroundColor:'#aecbf9',
	//backgroundColor:'#f2f2f2',
	backgroundColor:'#C0D3D9',
    height:height,

  },
	logoPanel:{
		left:25,
		marginTop:15
	},
	workspaceContainer:{
		alignItems:'center',
		height:(height/1.25),
		width:width,
		backgroundColor:'#E4EBED'
	},
	defaultModules:{
		alignItems:'center',
		//height:(height/4),
		height:(height/5.4),
		marginBottom:(height/60),
		width:width,
		//backgroundColor:'#6791C1',
		//backgroundColor:'#C1C1C1',
		//backgroundColor:'#54A5C1',
		backgroundColor:'#AABBC1',
		flexDirection:'row'
	},
	modules:{
		alignItems:'center',
		//backgroundColor:'#EFF6FF',
		//height:(height/6.4),
		//width:(width/4.65),
		//marginLeft:(width/34.13),
		//marginTop:10, //new addition
		backgroundColor:'#F2F4F5',
		height: (height/ 9.6 ),
		width:  (width / 6   ),
		marginLeft:(width/34),
		marginTop:25,
		flexDirection:'row',
		borderRadius:5
	},
	modulesDefault:{
		alignItems:'center',
		backgroundColor:'#EDF3F5',
		height: (height/ 9.6  ),
		width:  (width / 6 ),   // (220/180)*4.65
		marginLeft:(width/16),
		marginTop:25,
		flexDirection:'row',
		borderRadius:5
	},
	extraModules:{
		flexDirection:'row',
		// alignItems:'center',

 		height:(height/1.2),
 		width:width,
		flexWrap: 'wrap'  //new addition

	},
	secondRowModules:{
    flexDirection:'row',
		alignItems:'center',
 		height:(height/6),
 		width:width
	},
	familyInfoIcon:{
		//width: (width/25.6),
		//height: (height/16.95),
		//marginLeft:(width/51.2)
		marginLeft:(width/200)
	},
	financialIcon:{
		//width: (width/22.75),
		//height:(height/16.95),
		//marginLeft:(width/56.88),
		marginLeft:(width/82)
	 },
	financialReportIcon:{
		//  width: (width/34.13),
		//  height:(height/19.2),
		//  marginLeft:(width/67.90)
		width: (width/36.5),
		height:(height/19),
		marginLeft:(width/85)
	 },
	 financialReportIconSmall:{
		 width: (width/36.5),
 		 height:(height/18),
 		 //marginLeft:(width/67.90)
 		 marginLeft:(width/75),
 		 marginRight:(width/104)
	 },
	 attachIcon:{
		// width: (width/25),
		// height:(height/16.90),
		//marginLeft:(width/51.2)
		width: (width/30),
		height:(height/18.5),
		//marginLeft:(width/66)
		marginLeft:(width/140),
		marginRight:(width/260)

		},
	 travelIcon:{
		 //width: (width/29.25),
		// height:(height/15),
		 marginLeft:(width/51.2)
	 },
	 estateIcon:{
     //width: (width/25.6),
		 //height:(height/16.95),
		 marginLeft:(width/56.88)
	 },
	 willIcon:{
		 width: (width/30),
		 height:(height/20),
		//width: (width/30),
		//height:(height/18),
		marginLeft:(width/67.90)
		},
	 trustIcon:{
		//width: (width/29.25),
		//height:(height/16.95),
		marginLeft:(width/58)
		},
	funeralIcon:{
		//width: (width/37),
		height: (height/15.36),
		marginLeft:(width/51.2)
	 },
	 vehiclesIcon:{
		 width: (width/20),
		 //height: (height/20),
		 marginLeft:(width/67.90)
	 },
	 lodgingIcon:{
			//width: (width/25.6),
			//height: (height/19.2),
			marginLeft:(width/67.90)
		},
	 charityIcon:{
		 width: (width/24),
		 height:(height/18),
		 marginLeft:(width/102.4)
	 },
	 insuranceIcon:{
		 width: (width/22),
		 height:(height/18),
		 marginLeft:(width/102.4)
	 },
	 movingStorageIcon:{
		 width: (width/22),
		 height:(height/18),
		 marginLeft:(width/60)
	 },
	 hospiceIcon:{
		 width: (width/24),
		 height:(height/18),
		 marginLeft:(width/100)
	 },
	sepIcon:{
		height:(height/16.95),
		// marginLeft:(width/67.90),
		//marginLeft:(width/150),
		//marginRight:(width/102.4)
		marginLeft:0,
		marginRight:width/200
	},
  moduleLabel:{
		flexDirection:'column'
	},
	addAnotherBtn:{
		alignItems:'center',
		marginTop:10,
		height:(height/18),
		width:(width/4),
		justifyContent:'center',
		//backgroundColor:'#d1d0d1',
		//backgroundColor:'#464348'
		backgroundColor:'#003648'
	},
	btnText:{
		fontFamily:Fonts.type.Roboto,
		color:'white',
		justifyContent:'center',
		//fontSize:(width/51.2),
		fontSize:(width/51.2),
		fontWeight:'600'

	},
	btnPanel:{
		alignItems:'center',
		marginLeft:(width/2.60),
		marginTop:(height/180)
	},
	moduleName:{
		fontSize:FONT_SIZE,
		//fontFamily:Fonts.type.Roboto,
	}

});

export default styles;
