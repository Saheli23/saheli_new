import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, StackNavigator } from 'react-navigation';

import LoginScreen from './containers/LoginScreen';
import RegistrationScreen from './containers/RegistrationScreen';
import WelcomeScreen from './containers/WelcomeScreen';
import QuestionOnePageScreen from './containers/QuestionOnePageScreen';
import QuestionTwoPageScreen from './containers/QuestionTwoPageScreen';
import WorkspaceScreen from './containers/WorkspaceScreen';
import ThankYouScreen from './containers/ThankYouScreen';
import ForgotPasswordScreen from './containers/ForgotPasswordScreen';
import FamilyInformationScreen from './containers/FamilyInformation/FamilyInformationScreen';
import AddFamilyMemberScreen from './containers/FamilyInformation/AddFamilyMemberScreen';
import AddWorkspaceModuleScreen from './containers/AddWorkspaceModuleScreen';
import MemberDetailsScreen from './containers/FamilyInformation/MemberDetailsScreen';
import EditFamilyMemberScreen from './containers/FamilyInformation/EditFamilyMemberScreen';
import FinancialManagementScreen from './containers/FinancialManagement/FinancialManagementScreen';
import AddFinancialManagementScreen from './containers/FinancialManagement/AddFinancialManagementScreen';
import FinancialDetailsScreen from './containers/FinancialManagement/FinancialDetailsScreen';
import EditFinancialManagementScreen from './containers/FinancialManagement/EditFinancialManagementScreen';
import UserProfileScreen from './containers/UserProfile/UserProfileScreen';
import TermsScreen from './containers/TermsScreen';
import FuneralInformationScreen from './containers/FuneralInformation/FuneralInformationScreen';
import FuneralDetailsScreen from './containers/FuneralInformation/FuneralDetailsScreen';
import AssistedLivingScreen from './containers/AssistedLiving/AssistedLivingScreen';
import HospiceInformationScreen from './containers/Hospice/HospiceInformationScreen';
import HospiceDetailsScreen from './containers/Hospice/HospiceDetailsScreen';


export const AppNavigator = StackNavigator({
  Login: { screen: LoginScreen },
  Registration: { screen: RegistrationScreen },
  Welcome:{screen:WelcomeScreen,navigationOptions: {
        gesturesEnabled: false,
    }},
  QuestionOnePage:{screen:QuestionOnePageScreen},
  QuestionTwoPage:{screen:QuestionTwoPageScreen},
  ThankYou:{screen:ThankYouScreen},
  Workspace:{screen:WorkspaceScreen,navigationOptions: {
        gesturesEnabled: false,
    }},
  ForgotPassword:{screen:ForgotPasswordScreen},
  FamilyInformation:{screen:FamilyInformationScreen},
  AddFamilyMember:{screen:AddFamilyMemberScreen},
  AddWorkspaceModule:{screen:AddWorkspaceModuleScreen},
  MemberDetails:{screen:MemberDetailsScreen},
  EditMemberDetails:{screen:EditFamilyMemberScreen},
  FinancialManagement:{screen:FinancialManagementScreen},
  AddFinancialManagement:{screen:AddFinancialManagementScreen},
  FinancialDetails:{screen:FinancialDetailsScreen},
  EditFinancialManagement:{screen:EditFinancialManagementScreen},
  UserProfile:{screen:UserProfileScreen},
  Terms:{screen:TermsScreen},
  FuneralInformation:{screen:FuneralInformationScreen},
  FuneralDetails:{screen:FuneralDetailsScreen},
  AssistedLiving:{screen:AssistedLivingScreen},
  HospiceInformation:{screen:HospiceInformationScreen},
  HospiceDetails:{screen:HospiceDetailsScreen},
},{ headerMode: 'screen',mode:'card' });

const AppWithNavigationState = ({ dispatch, nav }) => (
  <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />
);

AppWithNavigationState.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nav: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  nav: state.nav,
});

export default connect(mapStateToProps)(AppWithNavigationState);
