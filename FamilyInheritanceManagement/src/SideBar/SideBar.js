import React from "react";
import { AppRegistry, Image, StatusBar ,Dimensions} from "react-native";
import {
  Button,
  Text,
  Container,
  List,
  ListItem,
  Content,
  Icon
} from "native-base";
import { StackNavigator } from 'react-navigation';
import { Images} from '../Themes';
var {height, width} = Dimensions.get('window');
const routes = [{router:"Workspace",displayName:'Workspace',image:Images.workspaceIcon},
                {router:"FamilyInformation",displayName:"Family Information",image:Images.familyInfoIcon},
                {router:"FinancialManagement",displayName:"Financial Management",image:Images.financialIcon}];
export default class SideBar extends React.Component {
  render() {
    //console.log('this.props',this.props);
    return (
      <Container style={{backgroundColor:'#AABBC1',width:width/3.2}} >
        <Content>

          <List
            dataArray={routes}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigator.navigate(data.router)}
                >
                <Image
                 style={{marginRight:5,}}
                 source={data.image}/>
                  <Text style={{fontSize: 20,
                        textAlign: 'center',
                        fontFamily:'Roboto',
                        color:'#004055',
                        fontWeight:'bold',marginLeft:10}}>{data.displayName}</Text>
                </ListItem>
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}
