import { combineReducers } from 'redux';
import { NavigationActions } from 'react-navigation';

import { AppNavigator } from '../AppNavigator';

// Start with two routes: The Main screen, with the Login screen on top.
 const firstAction = AppNavigator.router.getActionForPathAndParams('Registration');
 const tempNavState = AppNavigator.router.getStateForAction(firstAction);
 const secondAction = AppNavigator.router.getActionForPathAndParams('Login');
 const initialNavState = AppNavigator.router.getStateForAction(
   secondAction,
  // tempNavState
 );

function nav(state = initialNavState, action) {
  let nextState;
  switch (action.type) {
    // case 'Login':
    //   nextState = AppNavigator.router.getStateForAction(
    //     NavigationActions.back(),
    //     state
    //   );
    //   break;
    // case 'Registration':
    //   nextState = AppNavigator.router.getStateForAction(
    //     NavigationActions.navigate({ routeName: 'Registration' }),
    //     state
    //   );
    //   break;
      case 'LOGIN_SUCCESS':
       nextState = AppNavigator.router.getStateForAction(
          NavigationActions.navigate({ routeName: 'Workspace' }),
          state
        );
        break;
      case 'LOGIN_SUCCESS_COMPLETE_STEP':
       nextState = AppNavigator.router.getStateForAction(
          NavigationActions.navigate({ routeName: 'Terms' }),
          state
        );
        break;
      case 'REGISTER_SUCCESS':
        nextState = AppNavigator.router.getStateForAction(
          NavigationActions.navigate({ routeName: 'Terms' }),
          state
        );
        break;
      case 'QUSANS_SUCCESS':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Workspace' }),
        state
      );
      break;
      case 'STEPCOMPLETED_SUCCESS':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Workspace' }),
        state
      );
      break;
      case 'ADDWORKSPACE_SUCCESS':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Workspace' }),
        state
      );
      break;
      case 'ADDMEMBER_SUCCESS_NAVIGATE':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'FamilyInformation' }),
        state
      );
      break;
      case 'UPDATEMEMBER_SUCCESS':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'FamilyInformation' }),
        state
      );
      break;
      case 'DELETEMEMBER_SUCCESS':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'FamilyInformation' }),
        state
      );
      break;
      case 'ADDFINANCIALINFORMATION_SUCCESS':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'FinancialManagement' }),
        state
      );
      break;
      case 'UPDATEFINANCIALINFO_SUCCESS':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'FinancialManagement' }),
        state
      );
      break;
      case 'DELETEFINANCIALINFO_SUCCESS':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'FinancialManagement' }),
        state
      );
      break;
    default:
      nextState = AppNavigator.router.getStateForAction(action, state);
      break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}

const initialAuthState = {};

function auth(state = initialAuthState, action) {
  switch (action.type) {
    // case 'Login':
    //   //return { ...state, isLoggedIn: true, user: action.data };
    //     return { ...state, user: true, };
    case 'LOGIN_SUCCESS':
    case 'LOGIN_ERROR':
    case 'LOGIN_SUCCESS_COMPLETE_STEP':
      console.log('login success');
      return { ...state, user: action.res };
      break;
    case 'REGISTER_SUCCESS':
    case 'REGISTER_ERROR':
      return { ...state, user: action.res };
    case 'QUSANS_SUCCESS':
      return { ...state, stepCompleted: action.res };
    case 'STEPCOMPLETED_SUCCESS':
      return { ...state, response: action.res };
    case 'WORKSPACEINFO_SUCCESS':
    case 'WORKSPACEINFO_ERROR':
      return { ...state,response:action.res};
    case 'ADDWORKSPACE_SUCCESS':
        return { ...state,response:action.res};
    case 'ADDMEMBER_SUCCESS':
        return { ...state,addmember:action.res};
    case 'GETMEMBER_SUCCESS':
        return { ...state,getmember:action.res};
    case 'UPDATEMEMBER_SUCCESS':
        return { ...state,updatemember:action.res};
    case 'DELETEMEMBER_SUCCESS':
            return { ...state,deletedmember:action.res};
    case 'ADDFINANCIALINFORMATION_SUCCESS':
            return { ...state,addFinancialInformation:action.res};
    case 'GETFINANCIALINFORMATION_SUCCESS':
            return { ...state,getFinancialInformation:action.res};
    case 'UPDATEFINANCIALINFO_SUCCESS':
            return { ...state,updateFinancialInformation:action.res};
    case 'DELETEFINANCIALINFO_SUCCESS':
            return { ...state,updateFinancialInformation:action.res};
    case 'GETTOTALASSESTS_SUCCESS':
            return { ...state,getTotalAssests:action.res};
    case 'GETUSERINFO_SUCCESS':
            return { ...state,getUserInfo:action.res};


    default:
      return state;
  }
}

const AppReducer = combineReducers({
  nav,
  auth,
});

export default AppReducer;
