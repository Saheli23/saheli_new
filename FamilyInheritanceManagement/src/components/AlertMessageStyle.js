// @flow

import { StyleSheet } from 'react-native'
import { Colors,Fonts } from '../Themes/'

export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginVertical: 25
  },
  contentContainer: {
    alignSelf: 'center',
    alignItems: 'center'
  },
  message: {
    marginTop: 10,
    marginHorizontal: 10,
    textAlign: 'center',
    fontFamily: Fonts.type.base,
    fontSize: Fonts.size.large,
    fontWeight: 'bold',
    color: '#CCCCCC'
  },
  icon: {
    color: '#CCCCCC'
  }
})
