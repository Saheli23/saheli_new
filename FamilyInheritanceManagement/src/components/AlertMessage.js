// @flow

import React from 'react'
import { View, Text } from 'react-native'
import styles from './AlertMessageStyle'
import * as Animatable from 'react-native-animatable'

import Icon from 'react-native-vector-icons/Ionicons'


type AlertMessageProps = {
  title: string,
  icon?: string,
  style?: Object,
  show?: bool
}

export default class AlertMessage extends React.Component {
  static defaultProps: { show: boolean }

  props: AlertMessageProps

  render () {
    let messageComponent = null
    if (this.props.show) {
      const { title } = this.props
      return (
        <Animatable.View
          style={[styles.container, this.props.style]}
          delay={800}
          animation='bounceIn'
        >
          <View style={styles.contentContainer}>
            <Icon
              name={this.props.icon || 'ios-alert'}
              size={45}
              style={styles.icon}
            />
            <Text allowFontScaling={false} style={styles.message}>{title }</Text>
          </View>
        </Animatable.View>
      )
    }

    return messageComponent
  }
}

AlertMessage.defaultProps = {
  show: true
}
