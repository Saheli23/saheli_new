import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import AppReducer from './reducers';
import AppWithNavigationState from './AppNavigator';
import thunk from 'redux-thunk';

let middleware = [thunk];
middleware = [...middleware];

class FamilyInheritanceManagementApp extends React.Component {
  store = createStore(AppReducer, applyMiddleware(...middleware));

  render() {
    return (
      <Provider store={this.store}>
        <AppWithNavigationState />
      </Provider>
    );
  }
}
AppRegistry.registerComponent('FamilyInheritanceManagement', () => FamilyInheritanceManagementApp);
export default FamilyInheritanceManagementApp;
