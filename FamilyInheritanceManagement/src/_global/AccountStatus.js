import React from 'react';

  const AccountStatus =[];
  const label = [
                 {id:1, status:"Open"},
                 {id:2, status:"Action Needed"},
                 {id:3, status:"Awaiting Transfer"},
                 {id:4, status:"Awaiting Closure"},
                 {id:5, status:"Closed"},

        ];
       for (i=0;i<label.length;i++){
         AccountStatus.push ({key:label[i].id,label:label[i].status});
       }

export default AccountStatus;
