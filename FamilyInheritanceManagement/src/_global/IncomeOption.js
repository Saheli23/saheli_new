import React from 'react';

  const IncomeOption =[];
  let incomekey = 0;
  const label = [
                 {id:incomekey++, value:"Less than $20,000"},
                 {id:incomekey++, value:"$20,000 to $34,999"},
                 {id:incomekey++, value:"$35,000 to $49,999"},
                 {id:incomekey++, value:"$50,000 to $74,999"},
                 {id:incomekey++, value:"$75,000 to $99,999"},
                 {id:incomekey++, value:"Over $100,000"},
                 {id:incomekey++, value:"Over $150,000"},

        ];

       for (i=0;i<label.length;i++){
         IncomeOption.push ({key:label[i].id,label:label[i].value});
       }

export default IncomeOption;
