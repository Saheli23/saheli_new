import React from 'react';

  const FinancialCategory =[];
  const label = [
                 {id:1, name:"Banking-CD"},
                 {id:2, name:"Banking-Checking"},
                 {id:3, name:"Banking-Credit Card"},
                 {id:4, name:"Banking-Savings"},
                 {id:5, name:"Banking-Safe Deposit Box"},
                 {id:6, name:"Credit Union-CD"},
                 {id:7, name:"Credit Union-Checking"},
                 {id:8, name:"Credit Union-Credit Card"},
                 {id:9, name:"Investments-Stocks"},
                 {id:10, name:"Investments-Stocks Options"},
                 {id:11, name:"Investments-Bonds"},
                 {id:12, name:"Investments-401k"},
                 {id:13, name:"Insurance-Term Life Policy"},
                 {id:14, name:"Retirement- Private Pension"},
                 {id:15, name:"Retirement-Social Security"},
                 {id:16, name:"Retirement-Government Agency"},
                 {id:17, name:"Other"},



        ];
       for (i=0;i<label.length;i++){
         FinancialCategory.push ({key:label[i].id,label:label[i].name});
       }

export default FinancialCategory;
