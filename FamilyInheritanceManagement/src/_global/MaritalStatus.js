import React from 'react';

  const MaritalStatus =[];
  const label = [
                 {id:1, status:"Single"},
                 {id:2, status:"Married"},
                 {id:3, status:"Domestic Partnership"},
                 {id:4, status:"Separated"},
                 {id:5, status:"Divorced"},
                 {id:6, status:"Widowed"},

        ];
       for (i=0;i<label.length;i++){
         MaritalStatus.push ({key:label[i].id,label:label[i].status});
       }

export default MaritalStatus;
