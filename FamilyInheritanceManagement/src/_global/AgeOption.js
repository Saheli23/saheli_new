import React from 'react';

  const AgeOption =[];
  let agekey = 0;
  const label = [
                 {id:agekey++, value:"Under 18"},
                 {id:agekey++, value:"18-24 years old"},
                 {id:agekey++, value:"25-34 years old"},
                 {id:agekey++, value:"35-44 years old"},
                 {id:agekey++, value:"45-54 years old"},
                 {id:agekey++, value:"55-64 years old"},
                 {id:agekey++, value:"65-74 years old"},

        ];


       for (i=0;i<label.length;i++){
         AgeOption.push ({key:label[i].id,label:label[i].value});
       }

export default AgeOption;
