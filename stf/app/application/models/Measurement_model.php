<?php

/**
* 
*/
class Measurement_model extends CI_Model
{
	public function getAll()
	{
		$query = $this->db->get('stf_measurement');

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}
}