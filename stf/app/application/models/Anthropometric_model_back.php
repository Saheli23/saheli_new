<?php

/**
* 
*/
class Anthropometric_model extends CI_Model
{
	public function check($id_player, $id_event)
	{
		$query = $this->db->limit(1)->select('id')->get_where('stf_anthropometric', array('id_player' => $id_player, 'id_event' => $id_event));

		return ($query->num_rows() == 1) ? TRUE : FALSE;
	}

	public function create()
	{
		$id_uniq = uniqid(mt_rand());

		$data = array(
			'id_uniq' => $id_uniq,
			'id_player' => $this->input->post('id_player'),
			'id_event' => $this->input->post('id_event') ,
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_size' => $this->input->post('size'),
			'nu_weight' => $this->input->post('weight'),
			'nu_chest_diameter' => $this->input->post('chest_diameter'),
			'nu_hip_diameter' => $this->input->post('hip_diameter'),
			'nu_left_thigh_diameter' => $this->input->post('left_thigh_diameter'),
			'nu_right_thigh_diameter' => $this->input->post('right_thigh_diameter'),
			'nu_left_ankle_diameter' => $this->input->post('left_ankle_diameter'),
			'nu_right_ankle_diameter' => $this->input->post('right_ankle_diameter'),
			'nu_head_diameter' => $this->input->post('head_diameter'),
			'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
		);

		$this->db->insert('stf_anthropometric', $data);
	}

	public function getAll($id_event)
	{
		$query = $this->db->select('stf_anthropometric.id_uniq, stf_anthropometric.dt_measurement, stf_player.tx_firstname, stf_player.tx_secondname, stf_player.tx_lastname, stf_player.tx_secondlastname')
						  ->from('stf_anthropometric')
						  ->join('stf_player', 'stf_player.id = stf_anthropometric.id_player')
						  ->where('stf_anthropometric.id_event', $id_event)
						  ->order_by('stf_anthropometric.id', 'desc')
						  ->get();

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}

	public function getOne($id_uniq)
	{
		$anthropometric = NULL;

		$query = $this->db->limit(1)
						  ->select('stf_anthropometric.dt_measurement, stf_anthropometric.nu_size, stf_anthropometric.nu_weight, stf_anthropometric.nu_chest_diameter, stf_anthropometric.nu_hip_diameter, stf_anthropometric.nu_left_thigh_diameter, stf_anthropometric.nu_right_thigh_diameter, stf_anthropometric.nu_left_ankle_diameter, stf_anthropometric.nu_right_ankle_diameter, stf_anthropometric.nu_head_diameter, stf_player.id AS id_player, stf_player.id_uniq AS player_id_uniq, stf_player.tx_firstname, stf_player.tx_secondname, stf_player.tx_lastname, stf_player.tx_secondlastname, stf_player.dt_birthdate, stf_gender.tx_name AS gender, stf_event.tx_name AS event_name')
		                  ->from('stf_anthropometric')
		                  ->join('stf_player', 'stf_player.id = stf_anthropometric.id_player')
		                  ->join('stf_gender', 'stf_gender.id = stf_player.id_gender')
		                  ->join('stf_event', 'stf_event.id = stf_anthropometric.id_event')
		                  ->where('stf_anthropometric.id_uniq', $id_uniq)
		                  ->get();

		if ($query->num_rows() == 1) 
		{
        	$anthropometric = $query->row();

        	$query = $this->db->limit(1)
        	                  ->select('stf_image.tx_name AS image')
        	                  ->from('stf_image')
        	                  ->join('stf_player_image', 'stf_player_image.id_image = stf_image.id', 'left outer')
        	                  ->where('stf_player_image.id_type_image', 1)
        	                  ->where('stf_player_image.id_player', $anthropometric->id_player)
        	                  ->order_by('stf_image.id', 'desc')
        	                  ->get();

        	$anthropometric->image_path = ($query->row() != NULL) ? $query->row()->image : NULL;
	    }               

		return $anthropometric;
	}

	public function edit($id_uniq)
	{
		$data = array(
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->input_stream('measurement_date'))),
			'nu_size' => $this->input->input_stream('size'),
			'nu_weight' => $this->input->input_stream('weight'),
			'nu_chest_diameter' => $this->input->input_stream('chest_diameter'),
			'nu_hip_diameter' => $this->input->input_stream('hip_diameter'),
			'nu_left_thigh_diameter' => $this->input->input_stream('left_thigh_diameter'),
			'nu_right_thigh_diameter' => $this->input->input_stream('right_thigh_diameter'),
			'nu_left_ankle_diameter' => $this->input->input_stream('left_ankle_diameter'),
			'nu_right_ankle_diameter' => $this->input->input_stream('right_ankle_diameter'),
			'nu_head_diameter' => $this->input->input_stream('head_diameter')
		);

		$this->db->limit(1)->where('id_uniq', $id_uniq)->update('stf_anthropometric', $data);
	}

	public function getEventIdUniq($id_uniq)
	{
		return $this->db->limit(1)
		                ->select('stf_event.id_uniq')
		                ->from('stf_event')
		                ->join('stf_anthropometric', 'stf_anthropometric.id_event = stf_event.id')
		                ->where('stf_anthropometric.id_uniq', $id_uniq)
		                ->get()
		                ->row()->id_uniq;
	}

	public function getPlayerWeight($id_player, $id_event)
	{
		$query = $this->db->limit(1)->select('nu_weight')->get_where('stf_anthropometric', array('id_player' => $id_player, 'id_event' => $id_event));

		return ($query->num_rows() == 1) ? $query->row() : NULL;
	}
}