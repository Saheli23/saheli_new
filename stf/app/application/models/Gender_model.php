<?php

/**
* 
*/
class Gender_model extends CI_Model
{
	public function getAll()
	{
		$query = $this->db->order_by('tx_name', 'asc')->get('stf_gender');

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}
}