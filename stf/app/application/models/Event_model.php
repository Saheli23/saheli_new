<?php

/**
* 
*/
class Event_model extends CI_Model{
	public function getUniqIDs() {
		return $this->db->order_by('dt_event', 'desc')->select('id_uniq')->get('stf_event')->result();
	}

	public function getID($id_uniq) {
		$query = $this->db->limit(1)->select('id')->get_where('stf_event', array('id_uniq' => $id_uniq));
		return ($query->num_rows() == 1) ? $query->row()->id : NULL;
	}

	public function create() {
		$id_uniq                = uniqid(mt_rand());
		$configurations         = $this->input->post('configurations');
		$configurations_to_save = array();
		$dt_event               = ($this->input->post('date') != "") ? mdate("%Y-%m-%d", strtotime($this->input->post('date'))) : $this->input->post('date');

		$data = array(
			'id_uniq' => $id_uniq,
			'tx_name' => $this->input->post('name'),
			'dt_event' => $dt_event,
			'id_venue' => $this->input->post('venue'),
			'id_team' => $this->input->post('team'),
			'id_measurement' => $this->input->post('measurement_system'),
			'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
		);

		$this->db->trans_start();
		$this->db->insert('stf_event', $data);
		$id_event = $this->db->insert_id();
		foreach ($configurations as $key => $config) {
			array_push($configurations_to_save, array(
				'id_event' => $id_event,
				'id_configuration' => $config['id']
			));
		}

		$this->db->insert_batch('stf_event_configuration', $configurations_to_save);
		$this->db->trans_complete();
	}

	public function getAll() {
		$query = $this->db->order_by('dt_event', 'desc')->select('id,id_uniq, tx_name, dt_event')->where('bo_status', '1')->get('stf_event');
		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}

	public function getOne($id_uniq) {
		$event = NULL;
		$query = $this->db->limit(1)
		                  ->select('stf_event.id, stf_event.id_uniq, stf_event.tx_name, stf_event.dt_event, stf_venue.id AS id_venue, stf_venue.id_uniq AS id_uniq_venue, stf_venue.tx_name AS venue, stf_team.id AS id_team, stf_team.id_uniq AS id_uniq_team, stf_team.tx_name AS team, stf_measurement.id AS id_measurement, stf_measurement.tx_name AS measurement, stf_measurement.tx_abbr AS stf_measurement_abbr')
		                  ->from('stf_event')
		                  ->join('stf_venue', 'stf_venue.id = stf_event.id_venue', 'left')
		                  ->join('stf_team', 'stf_team.id = stf_event.id_team', 'left')
		                  ->join('stf_measurement', 'stf_measurement.id = stf_event.id_measurement')
		                  ->where('stf_event.id_uniq', $id_uniq)
		                  ->get();

		if ($query->num_rows() == 1) {
			$event = $query->row();
			$query = $this->db->select('stf_station_configuration.id AS id_configuration, stf_station_configuration.id_station, stf_station_configuration.id_test, stf_station_configuration.id_age_range, stf_station_configuration.nu_distance, stf_station.tx_name AS station, stf_test.tx_name AS test, stf_age_range.nu_min_age, stf_age_range.nu_max_age, stf_age_range.nu_ball_weight')
			                  ->from('stf_event_configuration')
			                  ->join('stf_station_configuration', 'stf_station_configuration.id = stf_event_configuration.id_configuration')
			                  ->join('stf_station', 'stf_station.id = stf_station_configuration.id_station')
			                  ->join('stf_test', 'stf_test.id = stf_station_configuration.id_test')
			                  ->join('stf_age_range', 'stf_age_range.id = stf_station_configuration.id_age_range')
			                  ->where('stf_event_configuration.id_event', $event->id)
			                  ->get();
			$event->configurations = $query->result();
		}

		return $event;
	}

	public function edit($id_uniq) {
		$id_event = $this->getID($id_uniq);

		if ($id_event != NULL) {
			$configurations         = $this->input->input_stream('configurations');
			$configurations_to_save = array();
			$dt_event = ($this->input->input_stream('date') != "") ? mdate("%Y-%m-%d", strtotime($this->input->input_stream('date'))) : $this->input->input_stream('date');

			$data = array(
				'id_uniq' => $id_uniq,
				'tx_name' => $this->input->input_stream('name'),
				'dt_event' => $dt_event,
				'id_venue' => $this->input->input_stream('venue'),
				'id_team' => $this->input->input_stream('team'),
				'id_measurement' => $this->input->input_stream('measurement_system'),
			);

			$this->db->trans_start();
			$this->db->limit(1)->where('id_uniq', $id_uniq)->update('stf_event', $data);
			$this->db->delete('stf_event_configuration', array('id_event' => $id_event));
			foreach ($configurations as $key => $config) {
				array_push($configurations_to_save, array(
					'id_event' => $id_event,
					'id_configuration' => $config['id']
				));
			}

			$this->db->insert_batch('stf_event_configuration', $configurations_to_save);
			$this->db->trans_complete();
		}
	}

	public function delete() {
		$this->db->where('id_uniq', $this->input->input_stream('id_event'));
		$this->db->update('stf_event', array('bo_status' => '0'));
	}

	public function check_child(){
		$count  = $this->db->select('*')
					   ->where('id_event' , $this->getID($this->input->input_stream('id_event')))
					   ->get('stf_event_configuration')
					   ->num_rows();
		return $count;
	}

	public function deleteconfig(){
		$id = $this->input->input_stream('id_event');
		$query = $this->db->query("UPDATE stf_event_configuration SET bo_status='0' WHERE id_event IN (SELECT id FROM stf_event WHERE id_uniq = '".$id."')");
	}


	public function check_child_multidelete(){
		foreach($this->input->input_stream('id_event') as $id){
			$count  = $this->db->select('*')
		   	->where('id_event' , $this->getID($id))
		   	->get('stf_event_configuration')
		   	->num_rows();
		 	return $count;
		}
	}

	public function multidelete()
	{
		foreach($this->input->input_stream('id_event') as $id)
		{
			$this->db->where('id_uniq', $id);
    	$this->db->update('stf_event', array('bo_status' => '0'));
		}
	}

	public function multideleteconfig(){
		foreach($this->input->input_stream('id_event') as $id){
			$query = $this->db->query("UPDATE stf_event_configuration SET bo_status='0' WHERE id_event IN (SELECT id FROM stf_event WHERE id_uniq = '".$id."')");
		}
	}
}