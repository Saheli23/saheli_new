<?php

/**
* 
*/
class Age_model extends CI_Model
{
	public function getRange()
	{
		$query = $this->db->get('stf_age_range');

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}					
}