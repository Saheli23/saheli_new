<?php

/**
* 
*/
class Access_model extends CI_Model{
	public function getAccess( $user_id, $section_id ) {
		$whereClause = array("id_user" => $user_id,"id_section" => $section_id );
		$query = $this->db->get_where("stf_access", $whereClause );
		return ($query->num_rows() > 0) ? $query->row() : NULL;
	}					
}