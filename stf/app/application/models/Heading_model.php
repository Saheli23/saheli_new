<?php

/**
* 
*/
class Heading_model extends CI_Model{
  public function check($id_player, $id_event, $tests, $type){
    /*
     *  0 => La prueba no existe
     *  1 => La prueba existe y el jugador fue evaluado
     *  2 => La prueba existe y el jugador no ha sido evaluado
    */
    $arr = array('id_player' => $id_player, 'id_event' => $id_event );
    if( $type === 1 ) $arr['bo_status'] = $type;

    $checks = array(
      'aim' => 0,
      'force' => 0,
      'jump' => 0
    );

    foreach ($tests as $key => $test) {
      if ($test == "Aim") {
        $query = $this->db->limit(1)->select('id')->get_where('stf_heading_aim', $arr);
        $checks['aim'] = ($query->num_rows() == 1) ? 1 : 2;
      } else if ($test == "Force") {
        $query = $this->db->limit(1)->select('id')->get_where('stf_heading_force', $arr);
        $checks['force'] = ($query->num_rows() == 1) ? 1 : 2;
      } elseif ($test == "Jump") {
        $query = $this->db->limit(1)->select('id')->get_where('stf_heading_jump', $arr);
        $checks['jump'] = ($query->num_rows() == 1) ? 1 : 2;
      }
    }
    return $checks;
  }

  public function check_for_insert($player_id, $event_id, $type){
  	$arr = array('id_player' => $player_id, 'id_event' => $event_id);

  	if($type == "aim"){

			$query = $this->db->limit(1)->select('bo_status')->get_where('stf_heading_aim', $arr )->row()->bo_status;
			return $query;

  	}else if($type == "force"){

			$query = $this->db->limit(1)->select('id')->get_where('stf_heading_force', $arr )->row()->bo_status;
			return $query;
  	
  	}else{

  		$query = $this->db->limit(1)->select('id')->get_where('stf_heading_jump', $arr )->row()->bo_status;
			return $query;
  	
  	}
  }

  public function createAim() {
    $id_uniq = uniqid(mt_rand());
    $data = array(
      'id_uniq' => $id_uniq,
      'id_player' => $this->input->post('id_player'),
      'id_event' => $this->input->post('id_event'),
      'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
      'nu_request_one' => $this->input->post('request_one'),
      'nu_destination_one' => $this->input->post('destination_one'),
      'nu_request_two' => $this->input->post('request_two'),
      'nu_destination_two' => $this->input->post('destination_two'),
      'nu_request_three' => $this->input->post('request_three'),
      'nu_destination_three' => $this->input->post('destination_three'),
      'nu_request_four' => $this->input->post('request_four'),
      'nu_destination_four' => $this->input->post('destination_four'),
      'nu_request_five' => $this->input->post('request_five'),
      'nu_destination_five' => $this->input->post('destination_five'),
      'nu_request_six' => $this->input->post('request_six'),
      'nu_destination_six' => $this->input->post('destination_six'),
      'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
    );

    $this->db->insert('stf_heading_aim', $data);
  }


  public function createForce() {
    $id_uniq = uniqid(mt_rand());

    $data = array(
      'id_uniq' => $id_uniq,
      'id_player' => $this->input->post('id_player'),
      'id_event' => $this->input->post('id_event'),
      'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
      'nu_distance_one' => $this->input->post('distance_one'),
      'nu_distance_two' => $this->input->post('distance_two'),
      'nu_distance_three' => $this->input->post('distance_three'),
      'nu_head_attempt_one'   => $this->input->post('attempt_one'),
      'nu_head_attempt_two'   => $this->input->post('attempt_two'),
      'nu_head_attempt_three' => $this->input->post('attempt_three'),
      'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
    );

    $this->db->insert('stf_heading_force', $data);
  }

  public function createJump() {
    $id_uniq = uniqid(mt_rand());

    $data = array(
      'id_uniq' => $id_uniq,
      'id_player' => $this->input->post('id_player'),
      'id_event' => $this->input->post('id_event'),
      'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
      'nu_extended_arm_height' => $this->input->post('extended_arm_height'),
      'nu_jump_high_one' => $this->input->post('jump_high_one'),
      'nu_jump_high_two' => $this->input->post('jump_high_two'),
      'nu_jump_high_three' => $this->input->post('jump_high_three'),
      'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
    );

    $this->db->insert('stf_heading_jump', $data);
  }


  public function createAimtwo() {
    $id_uniq = uniqid(mt_rand());
    $data = array(
      'id_uniq' 							=> $id_uniq,
      'id_player' 						=> $this->input->input_stream('id_player'),
      'id_event' 							=> $this->input->input_stream('id_event'),
      'dt_measurement' 				=> mdate("%Y-%m-%d", strtotime($this->input->input_stream('measurement_date'))),
      'nu_request_one'				=> $this->input->input_stream('request_one'),
      'nu_destination_one'		=> $this->input->input_stream('destination_one'),
      'nu_request_two' 				=> $this->input->input_stream('request_two'),
      'nu_destination_two' 		=> $this->input->input_stream('destination_two'),
      'nu_request_three' 			=> $this->input->input_stream('request_three'),
      'nu_destination_three' 	=> $this->input->input_stream('destination_three'),
      'nu_request_four' 			=> $this->input->input_stream('request_four'),
      'nu_destination_four' 	=> $this->input->input_stream('destination_four'),
      'nu_request_five' 			=> $this->input->input_stream('request_five'),
      'nu_destination_five' 	=> $this->input->input_stream('destination_five'),
      'nu_request_six' 				=> $this->input->input_stream('request_six'),
      'nu_destination_six' 		=> $this->input->input_stream('destination_six'),
      'dt_register' 					=> mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
    );

    $this->db->insert('stf_heading_aim', $data);
  }

  public function createJumptwo() {
    $id_uniq = uniqid(mt_rand());
    $data = array(
      'id_uniq' 								=> $id_uniq,
      'id_player' 							=> $this->input->input_stream('id_player'),
      'id_event' 								=> $this->input->input_stream('id_event'),
      'dt_measurement' 					=> mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
      'nu_extended_arm_height' 	=> $this->input->input_stream('extended_arm_height'),
      'nu_jump_high_one' 				=> $this->input->input_stream('jump_high_one'),
      'nu_jump_high_two' 				=> $this->input->input_stream('jump_high_two'),
      'nu_jump_high_three' 			=> $this->input->input_stream('jump_high_three'),
      'dt_register' 						=> mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
    );

    $this->db->insert('stf_heading_jump', $data);
  }

  public function createForcetwo() {
    $id_uniq = uniqid(mt_rand());

    $data = array(
      'id_uniq' 							=> $id_uniq,
      'id_player' 						=> $this->input->input_stream('id_player'),
      'id_event' 							=> $this->input->input_stream('id_event'),
      'dt_measurement'				=> mdate("%Y-%m-%d", strtotime($this->input->input_stream('measurement_date'))),
      'nu_distance_one' 			=> $this->input->input_stream('distance_one'),
      'nu_distance_two' 			=> $this->input->input_stream('distance_two'),
      'nu_distance_three' 		=> $this->input->input_stream('distance_three'),
      'nu_head_attempt_one'	 	=> $this->input->input_stream('attempt_one'),
      'nu_head_attempt_two' 	=> $this->input->input_stream('attempt_two'),
      'nu_head_attempt_three'	=> $this->input->input_stream('attempt_three'),
      'dt_register' 					=> mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
    );

    $this->db->insert('stf_heading_force', $data);
  }

  public function updateExistingAim( $player_id, $event_id )
  {
    $data = array(
      'dt_measurement'      => mdate("%Y-%m-%d", strtotime($this->input->input_stream('measurement_date'))),
      'nu_request_one'      => $this->input->input_stream('request_one'),
      'nu_destination_one'  => $this->input->input_stream('destination_one'),
      'nu_request_two'      => $this->input->input_stream('request_two'),
      'nu_destination_two'  => $this->input->input_stream('destination_two'),
      'nu_request_three'    => $this->input->input_stream('request_three'),
      'nu_destination_three'=> $this->input->input_stream('destination_three'),
      'nu_request_four'     => $this->input->input_stream('request_four'),
      'nu_destination_four' => $this->input->input_stream('destination_four'),
      'nu_request_five'     => $this->input->input_stream('request_five'),
      'nu_destination_five' => $this->input->input_stream('destination_five'),
      'nu_request_six'      => $this->input->input_stream('request_six'),
      'nu_destination_six'  => $this->input->input_stream('destination_six'),
      'bo_status'           => 1
    );

    $array = array('id_player' => $player_id, 'id_event' => $event_id);
    $this->db->limit(1)->where($array)->update('stf_heading_aim', $data);
  }

  public function updateExistingForce( $player_id, $event_id )
  {
    $data = array(
      'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->input_stream('measurement_date'))),
      'nu_distance_one' => $this->input->input_stream('distance_one'),
      'nu_distance_two' => $this->input->input_stream('distance_two'),
      'nu_distance_three' => $this->input->input_stream('distance_three'),
      'nu_head_attempt_one'   => $this->input->input_stream('attempt_one'),
      'nu_head_attempt_two'   => $this->input->input_stream('attempt_two'),
      'nu_head_attempt_three' => $this->input->input_stream('attempt_three'),
      'bo_status'          => 1
    );

    $array = array('id_player' => $player_id, 'id_event' => $event_id);
    $this->db->limit(1)->where($array)->update('stf_heading_force', $data);
  }

  public function updateExistingJump( $player_id, $event_id )
  {
    $data = array(
      'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->input_stream('measurement_date'))),
      'nu_extended_arm_height' => $this->input->input_stream('extended_arm_height'),
      'nu_jump_high_one' => $this->input->input_stream('jump_high_one'),
      'nu_jump_high_two' => $this->input->input_stream('jump_high_two'),
      'nu_jump_high_three' => $this->input->input_stream('jump_high_three'),
      'bo_status'          => 1
    );

    $array = array('id_player' => $player_id, 'id_event' => $event_id);
    $this->db->limit(1)->where($array)->update('stf_heading_jump', $data);
  }

  public function updateForce() {

    $where = array(
        'id_player' => $this->input->input_stream('id_player'),
        'id_event' => $this->input->input_stream('id_event')
    );
  
    $data = array(
      'nu_distance_one'       => $this->input->input_stream('distance_one'),
      'nu_distance_two'       => $this->input->input_stream('distance_two'),
      'nu_distance_three'     => $this->input->input_stream('distance_three'),
      'nu_head_attempt_one'   => $this->input->input_stream('attempt_one'),
      'nu_head_attempt_two'   => $this->input->input_stream('attempt_two'),
      'nu_head_attempt_three' => $this->input->input_stream('attempt_three')
    );

    $this->db->where($where);
    $this->db->update('stf_heading_force', $data);
  }

   public function updateJump() {
    $where = array(
        'id_player' => $this->input->input_stream('id_player'),
        'id_event' => $this->input->input_stream('id_event')
    );
  
    $data = array(
      'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->input_stream('measurement_date'))),
      'nu_extended_arm_height' => $this->input->input_stream('extended_arm_height'),
      'nu_jump_high_one' => $this->input->input_stream('jump_high_one'),
      'nu_jump_high_two' => $this->input->input_stream('jump_high_two'),
      'nu_jump_high_three' => $this->input->input_stream('jump_high_three')
    );

    $this->db->where($where);
    $this->db->update('stf_heading_jump', $data);
  }

   public function updateAim() {
    $where = array(
        'id_player' => $this->input->input_stream('id_player'),
        'id_event' => $this->input->input_stream('id_event')
    );
  
    $data = array(
      'dt_measurement'      => mdate("%Y-%m-%d", strtotime($this->input->input_stream('measurement_date'))),
      'nu_request_one'      => $this->input->input_stream('request_one'),
      'nu_destination_one'  => $this->input->input_stream('destination_one'),
      'nu_request_two'      => $this->input->input_stream('request_two'),
      'nu_destination_two'  => $this->input->input_stream('destination_two'),
      'nu_request_three'    => $this->input->input_stream('request_three'),
      'nu_destination_three'=> $this->input->input_stream('destination_three'),
      'nu_request_four'     => $this->input->input_stream('request_four'),
      'nu_destination_four' => $this->input->input_stream('destination_four'),
      'nu_request_five'     => $this->input->input_stream('request_five'),
      'nu_destination_five' => $this->input->input_stream('destination_five'),
      'nu_request_six'      => $this->input->input_stream('request_six'),
      'nu_destination_six'  => $this->input->input_stream('destination_six')
    );

    $this->db->where($where);
    $this->db->update('stf_heading_aim', $data);
  }

  public function delete($type) {
    $this->db->where('id_player', $this->input->input_stream('id_player'));
      $this->db->update('stf_heading_'.$type, array('bo_status' => '0'));
  }

  public function getAll($id_event) {
    $sql = "SELECT stf_player.id_uniq, 
             stf_player.id, 
             stf_player.tx_firstname, 
             stf_player.tx_secondname, 
             stf_player.tx_lastname, 
             stf_player.tx_secondlastname 
          FROM stf_player
     LEFT OUTER JOIN stf_heading_aim
          ON stf_heading_aim.id_player = stf_player.id 
     LEFT OUTER JOIN stf_heading_force
            ON stf_heading_force.id_player = stf_player.id
     LEFT OUTER JOIN stf_heading_jump
            ON stf_heading_jump.id_player = stf_player.id
           WHERE (stf_heading_force.bo_status = 1 OR stf_heading_aim.bo_status = 1 OR stf_heading_jump.bo_status = 1)
             AND ( stf_heading_aim.id_event = ".$id_event."
              OR stf_heading_jump.id_event = ".$id_event."
              OR stf_heading_force.id_event = ".$id_event." )
          GROUP BY stf_player.id
            ORDER BY stf_player.tx_firstname ASC";
      $query = $this->db->query($sql);        
    return ($query->num_rows() > 0) ? $query->result() : NULL;
  }

  public function getDetails( $id_event, $id_player){
    $sql = "SELECT stf_heading_aim.id_uniq aimUniqID,
             stf_heading_aim.id_player aimPlayerID,
             stf_heading_aim.id_event aimEvtID,
             stf_heading_aim.dt_measurement aimMeasurementDt,
             stf_heading_aim.nu_request_one aimReqOne,
             stf_heading_aim.nu_request_two aimReqTwo,
             stf_heading_aim.nu_request_three aimReqThree,
             stf_heading_aim.nu_request_four aimReqFour,
             stf_heading_aim.nu_request_five aimReqFive,
             stf_heading_aim.nu_request_six aimReqSix,
             stf_heading_aim.nu_destination_one aimDesOne,
             stf_heading_aim.nu_destination_two aimDesTwo,
             stf_heading_aim.nu_destination_three aimDesThree,
             stf_heading_aim.nu_destination_four aimDesFour,
             stf_heading_aim.nu_destination_five aimDesFive,
             stf_heading_aim.nu_destination_six aimDesSix,
             stf_heading_aim.bo_status aimStat,
             stf_heading_jump.id_uniq jumpUniqID,
             stf_heading_jump.id_player jumpPlayerID,
             stf_heading_jump.id_event jumpEvtID,
             stf_heading_jump.dt_measurement jumpMeasurementDt,
             stf_heading_jump.nu_extended_arm_height jumpArmHeight,
             stf_heading_jump.nu_jump_high_one jumpHighOne,
             stf_heading_jump.nu_jump_high_two jumpHighTwo,
             stf_heading_jump.nu_jump_high_three jumpHighThree,
             stf_heading_jump.bo_status jumpStat,
             stf_heading_force.id_uniq forceUniqID,
             stf_heading_force.id_player forcePlayerID,
             stf_heading_force.id_event forceEvtID,
             stf_heading_force.dt_measurement forceMeasurementDt,
             stf_heading_force.nu_distance_one forceDisOne,
             stf_heading_force.nu_distance_two forceDisTwo,
             stf_heading_force.nu_distance_three forceDisThree,
             stf_heading_force.nu_head_attempt_one forceAttemptOne,
             stf_heading_force.nu_head_attempt_two forceAttemptTwo,
             stf_heading_force.nu_head_attempt_three forceAttemptThree,
             stf_heading_force.bo_status forceStat,
             stf_player.id_uniq, 
             stf_player.id, 
             stf_player.tx_firstname, 
             stf_player.tx_secondname, 
             stf_player.tx_lastname, 
             stf_player.tx_secondlastname 
          FROM stf_player
     LEFT OUTER JOIN stf_heading_aim
          ON stf_heading_aim.id_player = stf_player.id 
     LEFT OUTER JOIN stf_heading_force
            ON stf_heading_force.id_player = stf_player.id
     LEFT OUTER JOIN stf_heading_jump
            ON stf_heading_jump.id_player = stf_player.id
           WHERE (stf_heading_force.bo_status = 1 OR stf_heading_aim.bo_status = 1 OR stf_heading_jump.bo_status = 1)
             AND ( stf_heading_aim.id_event = ".$id_event."
              OR stf_heading_jump.id_event = ".$id_event."
              OR stf_heading_force.id_event = ".$id_event." )
             AND ( stf_heading_aim.id_player= ".$id_player."
              OR stf_heading_jump.id_player = ".$id_player."
              OR stf_heading_force.id_player = ".$id_player." )";
      $query = $this->db->query($sql);        
    return ($query->num_rows() > 0) ? $query->row() : NULL;

  }

  public function multidelete($type) {
    foreach($this->input->input_stream('id_player') as $id){
      $this->db->where('id_player', $id );
      $this->db->update('stf_heading_'.$type, array('bo_status' => '0'));
    }
  }
}