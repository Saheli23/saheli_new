<?php

/**
* 
*/
class Kicking_model extends CI_Model
{
	public function check($id_player, $id_event, $tests, $type)
	{
		/*
		 *  0 => La prueba no existe
		 *  1 => La prueba existe y el jugador fue evaluado
		 *  2 => La prueba existe y el jugador no ha sido evaluado
		*/
		$arr = array('id_player' => $id_player, 'id_event' => $id_event );
		if( $type === 1 ) $arr['bo_status'] = $type;

		$checks = array(
			'aim' => 0,
			'force' => 0
		);

		foreach ($tests as $key => $test)
		{
			if ($test == "Aim")
			{
				$query = $this->db->limit(1)->select('id')->get_where('stf_kicking_aim', $arr);

				$checks['aim'] = ($query->num_rows() == 1) ? 1 : 2;
			}
			else if ($test == "Power")
			{
				$query = $this->db->limit(1)->select('id')->get_where('stf_kicking_force', $arr);

				$checks['force'] = ($query->num_rows() == 1) ? 1 : 2;
			}
		}

		return $checks;
	}

   public function check_for_insert($player_id, $event_id, $type){
    $arr = array('id_player' => $player_id, 'id_event' => $event_id);

    if($type == "aim"){

      $query = $this->db->limit(1)->select('bo_status')->get_where('stf_kicking_aim', $arr )->row()->bo_status;
      return $query;

    }else if($type == "force"){

      $query = $this->db->limit(1)->select('bo_status')->get_where('stf_kicking_force', $arr )->row()->bo_status;
      
      if($query != '' || $query != null){
        return $query;
      } else {
        return "nf";
      }
    
    }
  }


	public function createAim()
	{
		$id_uniq = uniqid(mt_rand());

		$data = array(
			'id_uniq' => $id_uniq,
			'id_player' => $this->input->post('id_player'),
			'id_event' => $this->input->post('id_event'),
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_right_leg_request_one' => $this->input->post('right_leg[request_one]'),
			'nu_right_leg_destination_one' => $this->input->post('right_leg[destination_one]'),
			'nu_right_leg_request_two' => $this->input->post('right_leg[request_two]'),
			'nu_right_leg_destination_two' => $this->input->post('right_leg[destination_two]'),
			'nu_right_leg_request_three' => $this->input->post('right_leg[request_three]'),
			'nu_right_leg_destination_three' => $this->input->post('right_leg[destination_three]'),
			'nu_left_leg_request_one' => $this->input->post('left_leg[request_one]'),
			'nu_left_leg_destination_one' => $this->input->post('left_leg[destination_one]'),
			'nu_left_leg_request_two' => $this->input->post('left_leg[request_two]'),
			'nu_left_leg_destination_two' => $this->input->post('left_leg[destination_two]'),
			'nu_left_leg_request_three' => $this->input->post('left_leg[request_three]'),
			'nu_left_leg_destination_three' => $this->input->post('left_leg[destination_three]'),
			'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
		);

		$this->db->insert('stf_kicking_aim', $data);
	}

	public function createForce($player_id, $event_id)
	{
		$id_uniq = uniqid(mt_rand());
    $data = array(
      'id_uniq'                     => $id_uniq,
      'id_player'                   => $player_id,
      'id_event'                    => $event_id,
      'dt_measurement'              => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
      'nu_right_leg_attempt_one'    => $this->input->post('right_leg[attempt_one]'),
      'nu_right_leg_attempt_two'    => $this->input->post('right_leg[attempt_two]'),
      'nu_right_leg_attempt_three'  => $this->input->post('right_leg[attempt_three]'),
      'nu_left_leg_attempt_one'     => $this->input->post('left_leg[attempt_one]'),
      'nu_left_leg_attempt_two'     => $this->input->post('left_leg[attempt_two]'),
      'nu_left_leg_attempt_three'   => $this->input->post('left_leg[attempt_three]'),
      'dt_register'                 => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
    );

		$this->db->insert('stf_kicking_force', $data);
	}

	public function updateAim()
	{
		$where = array(
        'id_player' => $this->input->input_stream('id_player'),
        'id_event' => $this->input->input_stream('id_event')
    );

		$data = array(
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->input_stream('measurement_date'))),
			'nu_right_leg_request_one' => $this->input->input_stream('right_leg[request_one]'),
			'nu_right_leg_destination_one' => $this->input->input_stream('right_leg[destination_one]'),
			'nu_right_leg_request_two' => $this->input->input_stream('right_leg[request_two]'),
			'nu_right_leg_destination_two' => $this->input->input_stream('right_leg[destination_two]'),
			'nu_right_leg_request_three' => $this->input->input_stream('right_leg[request_three]'),
			'nu_right_leg_destination_three' => $this->input->input_stream('right_leg[destination_three]'),
			'nu_left_leg_request_one' => $this->input->input_stream('left_leg[request_one]'),
			'nu_left_leg_destination_one' => $this->input->input_stream('left_leg[destination_one]'),
			'nu_left_leg_request_two' => $this->input->input_stream('left_leg[request_two]'),
			'nu_left_leg_destination_two' => $this->input->input_stream('left_leg[destination_two]'),
			'nu_left_leg_request_three' => $this->input->input_stream('left_leg[request_three]'),
			'nu_left_leg_destination_three' => $this->input->input_stream('left_leg[destination_three]'),
		);

				$this->db->where($where);
		    $this->db->update('stf_kicking_aim', $data);
    	}


	public function updateForce()
	{
		$where = array(
        'id_player' => $this->input->input_stream('id_player'),
        'id_event' => $this->input->input_stream('id_event')
    );

		$data = array(
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->input_stream('measurement_date'))),
			'nu_right_leg_attempt_one' => $this->input->input_stream('right_leg[attempt_one]'),
			'nu_right_leg_attempt_two' => $this->input->input_stream('right_leg[attempt_two]'),
			'nu_right_leg_attempt_three' => $this->input->input_stream('right_leg[attempt_three]'),
			'nu_left_leg_attempt_one' => $this->input->input_stream('left_leg[attempt_one]'),
			'nu_left_leg_attempt_two' => $this->input->input_stream('left_leg[attempt_two]'),
			'nu_left_leg_attempt_three' => $this->input->input_stream('left_leg[attempt_three]'),
		);

		$this->db->where($where);
    $this->db->update('stf_kicking_force', $data);
	}


	public function updateExistingAim( $player_id, $event_id )
	{

		$data = array(
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_right_leg_request_one' => $this->input->post('right_leg[request_one]'),
			'nu_right_leg_destination_one' => $this->input->post('right_leg[destination_one]'),
			'nu_right_leg_request_two' => $this->input->post('right_leg[request_two]'),
			'nu_right_leg_destination_two' => $this->input->post('right_leg[destination_two]'),
			'nu_right_leg_request_three' => $this->input->post('right_leg[request_three]'),
			'nu_right_leg_destination_three' => $this->input->post('right_leg[destination_three]'),
			'nu_left_leg_request_one' => $this->input->post('left_leg[request_one]'),
			'nu_left_leg_destination_one' => $this->input->post('left_leg[destination_one]'),
			'nu_left_leg_request_two' => $this->input->post('left_leg[request_two]'),
			'nu_left_leg_destination_two' => $this->input->post('left_leg[destination_two]'),
			'nu_left_leg_request_three' => $this->input->post('left_leg[request_three]'),
			'nu_left_leg_destination_three' => $this->input->post('left_leg[destination_three]'),
			'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York")),
			'bo_status'		=> 1
		);

		$array = array('id_player' => $player_id, 'id_event' => $event_id);
		$this->db->limit(1)->where($array)->update('stf_kicking_aim', $data);
	}

	public function updateExistingForce( $player_id, $event_id )
	{
		$data = array(
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_right_leg_attempt_one' => $this->input->post('right_leg[attempt_one]'),
			'nu_right_leg_attempt_two' => $this->input->post('right_leg[attempt_two]'),
			'nu_right_leg_attempt_three' => $this->input->post('right_leg[attempt_three]'),
			'nu_left_leg_attempt_one' => $this->input->post('left_leg[attempt_one]'),
			'nu_left_leg_attempt_two' => $this->input->post('left_leg[attempt_two]'),
			'nu_left_leg_attempt_three' => $this->input->post('left_leg[attempt_three]'),
			'bo_status'		=> 1
		);

		$array = array('id_player' => $player_id, 'id_event' => $event_id);
		$this->db->limit(1)->where($array)->update('stf_kicking_force', $data);
	}

	public function getAll($id_event) {
		/*$query = $this->db->select('stf_player.id, stf_player.tx_firstname, stf_player.tx_secondname, stf_player.tx_lastname, stf_player.tx_secondlastname')
		                  ->from('stf_player')
		                  ->join('stf_kicking_aim', 'stf_kicking_aim.id_player = stf_player.id', 'left outer')
		                  ->join('stf_kicking_force', 'stf_kicking_force.id_player = stf_player.id', 'left outer')
		                  ->where('stf_kicking_aim.id_event', $id_event)
		                  ->or_where('stf_kicking_force.id_event', $id_event)
		                  ->group_by('stf_player.id')
		                  ->order_by('stf_player.tx_firstname', 'asc')
		                  ->get();*/


		$sql = "SELECT stf_player.id_uniq, 
					   stf_player.id, 
					   stf_player.tx_firstname, 
					   stf_player.tx_secondname, 
					   stf_player.tx_lastname, 
					   stf_player.tx_secondlastname 
				  FROM stf_player
	   LEFT OUTER JOIN stf_kicking_aim
					ON stf_kicking_aim.id_player = stf_player.id 
	   LEFT OUTER JOIN stf_kicking_force
	   				ON stf_kicking_force.id_player = stf_player.id
	   			 WHERE (stf_kicking_aim.bo_status=1 OR stf_kicking_force.bo_status=1)
	   			   AND ( stf_kicking_aim.id_event = ".$id_event." 
             OR stf_kicking_force.id_event = ".$id_event.")
	   		  GROUP BY stf_player.id
	   	      ORDER BY stf_player.tx_firstname ASC";
	   	$query = $this->db->query($sql);		    
		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}

	  public function getDetails( $id_event, $id_player){
    $sql = "SELECT stf_kicking_aim.id_uniq aimUniqID,
             stf_kicking_aim.id_player aimPlayerID,
             stf_kicking_aim.id_event aimEvtID,
             stf_kicking_aim.dt_measurement aimMeasurementDt,
             stf_kicking_aim.nu_right_leg_request_one aimRightReqOne,
             stf_kicking_aim.nu_right_leg_destination_one aimRightDestOne,
             stf_kicking_aim.nu_right_leg_request_two aimRightReqTwo,
             stf_kicking_aim.nu_right_leg_destination_two aimRightDestTwo,
             stf_kicking_aim.nu_right_leg_request_three aimRightReqThree,
             stf_kicking_aim.nu_right_leg_destination_three aimRightDestThree,
             stf_kicking_aim.nu_left_leg_request_one aimLeftReqOne,
             stf_kicking_aim.nu_left_leg_destination_one aimLeftDestOne,
             stf_kicking_aim.nu_left_leg_request_two aimLeftReqTwo,
             stf_kicking_aim.nu_left_leg_destination_two aimLeftDestTwo,
             stf_kicking_aim.nu_left_leg_request_three aimLeftReqThree,
             stf_kicking_aim.nu_left_leg_destination_three aimLeftDestThree,
             stf_kicking_aim.bo_status aimStat,
             stf_kicking_force.id_uniq forceUniqID,
             stf_kicking_force.id_player forcePlayerID,
             stf_kicking_force.id_event forceEvtID,
             stf_kicking_force.dt_measurement forceMeasurementDt,
             stf_kicking_force.nu_right_leg_attempt_one forceRightAttemptOne,
             stf_kicking_force.nu_right_leg_attempt_two forceRightAttemptTwo,
             stf_kicking_force.nu_right_leg_attempt_three forceRightAttemptThree,
             stf_kicking_force.nu_left_leg_attempt_one forceLeftAttemptOne,
             stf_kicking_force.nu_left_leg_attempt_two forceLeftAttemptTwo,
             stf_kicking_force.nu_left_leg_attempt_three forceLeftAttemptThree,
             stf_kicking_force.bo_status forceStat,
             stf_player.id_uniq, 
             stf_player.id, 
             stf_player.tx_firstname, 
             stf_player.tx_secondname, 
             stf_player.tx_lastname, 
             stf_player.tx_secondlastname 
          FROM stf_player
     LEFT OUTER JOIN stf_kicking_aim
          ON stf_kicking_aim.id_player = stf_player.id 
     LEFT OUTER JOIN stf_kicking_force
            ON stf_kicking_force.id_player = stf_player.id
           WHERE (stf_kicking_aim.bo_status = 1 OR stf_kicking_force.bo_status = 1)
             AND ( stf_kicking_aim.id_event = ".$id_event."
              OR stf_kicking_force.id_event = ".$id_event." )
             AND ( stf_kicking_aim.id_player= ".$id_player."
              OR stf_kicking_force.id_player = ".$id_player." )";
      $query = $this->db->query($sql);        
    return ($query->num_rows() > 0) ? $query->row() : NULL;

  }

	public function delete($type) {
		$this->db->where('id_player', $this->input->input_stream('id_player'));
    	$this->db->update('stf_kicking_'.$type, array('bo_status' => '0'));
	}

	public function multidelete($type) {
		foreach($this->input->input_stream('id_player') as $id){
			$this->db->where('id_player', $id);
    	$this->db->update('stf_kicking_'.$type, array('bo_status' => '0'));
		}	
	}
}