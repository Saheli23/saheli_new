<?php

/**
* 
*/
class Venue_model extends CI_Model
{
	public function getUniqIDs()
	{
		$query = $this->db->order_by('tx_name', 'asc')->select('id_uniq')->get('stf_venue');

		return $query->result();
	}

	public function create() {
		$id_uniq = uniqid(mt_rand());

		$phone_prefix_two = ($this->input->post('phonenumber_two') != "") ? $this->input->post('phoneprefix_two') : "";
		
		$data = array(
			'id_uniq' => $id_uniq,
			'tx_name' =>  $this->input->post('name'),
			'id_country' => $this->input->post('country'),
			'id_state' => $this->input->post('state'),
			'id_city' => $this->input->post('city'),
			'tx_address' => $this->input->post('address'),
			'tx_zipcode' => $this->input->post('zipcode'),
			'tx_phone_prefix' => $this->input->post('phoneprefix_one'),
			'tx_phone' => $this->input->post('phonenumber_one'),
			'tx_phone_prefix_2' => $phone_prefix_two,
			'tx_phone_2' => $this->input->post('phonenumber_two'),
			'tx_email' => $this->input->post('email'),
			'tx_website' => $this->input->post('website'),
			'tx_facebook' => $this->input->post('facebook'),
			'tx_twitter' => $this->input->post('twitter'),
			'tx_instagram' => $this->input->post('instagram'),
			'tx_contact_name' => $this->input->post('contact_person'),
			'tx_contact_phone_prefix' => $this->input->post('contact_person_phoneprefix'),
			'tx_contact_phone' => $this->input->post('contact_person_phonenumber'),
			'tx_latitude' => $this->input->post('latitude'),
			'tx_longitude' => $this->input->post('longitude'),
			'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
		);

		$this->db->insert('stf_venue', $data);
	}

	public function getAll()
	{
		$query = $this->db->order_by('tx_name', 'asc')->select('id, id_uniq, tx_name, tx_email, tx_phone_prefix, tx_phone, tx_contact_name')->where('bo_status', '1')->get('stf_venue');

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}

	public function getOne($id_uniq) 
	{
		$query = $this->db->limit(1)
		                  ->select('stf_venue.id,stf_venue.id_uniq, stf_venue.tx_name AS venue_name, stf_venue.id_country, stf_venue.id_state,stf_venue.id_city, stf_venue.tx_address, stf_venue.tx_zipcode, stf_venue.tx_phone_prefix, stf_venue.tx_phone, stf_venue.tx_phone_prefix_2, stf_venue.tx_phone_2, stf_venue.tx_email, stf_venue.tx_website, stf_venue.tx_facebook, stf_venue.tx_twitter, stf_venue.tx_instagram, stf_venue.tx_contact_name, stf_venue.tx_contact_phone_prefix, stf_venue.tx_contact_phone, stf_venue.tx_latitude, stf_venue.tx_longitude, stf_country.tx_name as country, stf_state.tx_name as state')
		                  ->from('stf_venue')
		                  ->join('stf_country', 'stf_country.id = stf_venue.id_country')
		                  ->join('stf_state', 'stf_state.id = stf_venue.id_state')
		                  ->where('stf_venue.id_uniq', $id_uniq)
		                  ->get();

		return ($query->num_rows() == 1) ? $query->row() : NULL;		                  
	}

	public function edit($id_uniq)
	{
		$phone_prefix_two = ($this->input->input_stream('phonenumber_two') != "") ? $this->input->input_stream('phoneprefix_two') : "";
		
		$data = array(
			'tx_name' => $this->input->input_stream('name'),
			'id_country' => $this->input->input_stream('country'),
			'id_state' => $this->input->input_stream('state'),
			'id_city' => $this->input->input_stream('city'),
			'tx_address' => $this->input->input_stream('address'),
			'tx_zipcode' => $this->input->input_stream('zipcode'),
			'tx_phone_prefix' => $this->input->input_stream('phoneprefix_one'),
			'tx_phone' => $this->input->input_stream('phonenumber_one'),
			'tx_phone_prefix_2' => $phone_prefix_two,
			'tx_phone_2' => $this->input->input_stream('phonenumber_two'),
			'tx_email' => $this->input->input_stream('email'),
			'tx_website' => $this->input->input_stream('website'),
			'tx_facebook' => $this->input->input_stream('facebook'),
			'tx_twitter' => $this->input->input_stream('twitter'),
			'tx_instagram' => $this->input->input_stream('instagram'),
			'tx_contact_name' => $this->input->input_stream('contact_person'),
			'tx_contact_phone_prefix' => $this->input->input_stream('contact_person_phoneprefix'),
			'tx_contact_phone' => $this->input->input_stream('contact_person_phonenumber'),
			'tx_latitude' => $this->input->input_stream('latitude'),
			'tx_longitude' => $this->input->input_stream('longitude'),
		);

		$this->db->limit(1)->where('id_uniq', $id_uniq)->update('stf_venue', $data);
	}

	public function checkEmailForEdit($id_uniq)
	{
		$query = $this->db->limit(1)
						  ->where_not_in('id_uniq', $id_uniq)
						  ->where('tx_email', $this->input->input_stream('email'))
						  ->select('tx_email')
						  ->get('stf_venue');

		return ($query->num_rows() == 1) ? TRUE : FALSE;
	}

	public function delete() {
		$this->db->where('id', $this->input->input_stream('id_venue'));
    	$this->db->update('stf_venue', array('bo_status' => '0'));
	}

	public function multidelete() {
		foreach ($this->input->input_stream('id_venue') as $id) {
			$this->db->where('id', $id);
    	$this->db->update('stf_venue', array('bo_status' => '0'));
		}	
	}

	public function check_child( $type ){
		$this->db->where('id_venue' , $this->input->input_stream('id_venue'));
		return $this->db->get('stf_'.$type)->num_rows();
	}

	public function check_child_multiple( $type ){
		foreach($this->input->input_stream('id_venue') as $venue_id)
		{
			$this->db->where('id_venue' , $venue_id );
			return $this->db->get('stf_'.$type)->num_rows();
		}
	}


}