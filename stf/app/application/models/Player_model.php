<?php 

/**
* 
*/
class Player_model extends CI_Model
{
	public function getUniqIDs()
	{
		$query = $this->db->order_by('tx_firstname', 'asc')->select('id_uniq')->get('stf_player');

		return $query->result();
	}

	public function getID($id_uniq)
	{
		$query = $this->db->limit(1)->select('id')->get_where('stf_player', array('id_uniq' => $id_uniq));

		return ($query->num_rows() == 1) ? $query->row()->id : NULL;
	}

	public function create($images)
	{
		$id_uniq = uniqid(mt_rand());
		
		$phone_prefix = ($this->input->post('phonenumber') != "") ? $this->input->post('phoneprefix') : "";
		
		$data = array(
			'id_uniq' => $id_uniq,
			'tx_firstname' => $this->input->post('firstname'),
			'tx_secondname' => $this->input->post('secondname'),
			'tx_lastname' => $this->input->post('first_lastname'),
			'tx_secondlastname' => $this->input->post('second_lastname'),
			'dt_birthdate' => mdate("%Y-%m-%d", strtotime($this->input->post('birthdate'))),
			'id_gender' => $this->input->post('gender'),
			'id_country' => $this->input->post('country'),
			'id_state' => $this->input->post('state'),
			'tx_address' => $this->input->post('address'),
			'tx_city'		 => $this->input->post('city'),
			'tx_zipcode' => $this->input->post('zipcode'),
			'tx_phone_prefix' => $phone_prefix,
			'tx_phone' => $this->input->post('phonenumber'),
			'tx_email' => $this->input->post('email'),
			'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now('America/New_York'))
		);

		$this->db->trans_start();
		$this->db->insert('stf_player', $data);

		$id_player = $this->db->insert_id();

		if (!empty($images))
		{
			$data = array();

			foreach ($images as $key => $image)
			{
				$this->db->insert('stf_image', array('tx_name' => $image['path'], 'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))));
				array_push($data, array('id_player' => $id_player, 'id_image' => $this->db->insert_id(), 'id_type_image' => $image['type']));
			}

			$this->db->insert_batch('stf_player_image', $data);
		}

		$this->db->trans_complete();
		
		return $id_uniq;
	}

	public function getOne($id_uniq)
	{
		$player = NULL;

		$query = $this->db->limit(1)
		                  ->select('stf_player.*, stf_gender.tx_name AS gender, stf_country.tx_name AS country, stf_state.tx_name AS state')
		                  ->from('stf_player')
		                  ->join('stf_gender', 'stf_gender.id = stf_player.id_gender')
		                  ->join('stf_country', 'stf_country.id = stf_player.id_country')
		                  ->join('stf_state', 'stf_state.id = stf_player.id_state')
		                  ->where('stf_player.id_uniq', $id_uniq)
		                  ->get();

		if ($query->num_rows() == 1)
		{
			$images_type = array(1, 2);

			$player = $query->row();
			$player->images = array();

			$query = $this->db->select('stf_team_player.*, stf_team.id_uniq, stf_team.tx_name, stf_team.tx_image AS team_logo')
			                  ->from('stf_team_player')
			                  ->join('stf_team', 'stf_team.id = stf_team_player.id_team')
			                  ->where('stf_team_player.id_player', $player->id)
			                  ->order_by('stf_team_player.id', 'desc')
			                  ->get();

			$player->teams = $query->result();

			$query = $this->db->limit(1)
			                  ->select('stf_image.id AS id_image, stf_image.tx_name AS route, stf_type_image.id AS id_type_image, stf_type_image.tx_name AS type')
			                  ->from('stf_player_image')
			                  ->join('stf_image', 'stf_image.id = stf_player_image.id_image')
			                  ->join('stf_type_image', 'stf_type_image.id = stf_player_image.id_type_image')
			                  ->where('stf_player_image.id_type_image', 1)
			                  ->where('stf_player_image.id_player', $player->id)
			                  ->order_by('stf_player_image.id', 'desc')
			                  ->get();

			if ($query->num_rows() == 1) { array_push($player->images, $query->row()); }

			$query = $this->db->limit(1)
			                  ->select('stf_image.id AS id_image, stf_image.tx_name AS route, stf_type_image.id AS id_type_image, stf_type_image.tx_name AS type')
			                  ->from('stf_player_image')
			                  ->join('stf_image', 'stf_image.id = stf_player_image.id_image')
			                  ->join('stf_type_image', 'stf_type_image.id = stf_player_image.id_type_image')
			                  ->where('stf_player_image.id_type_image', 2)
			                  ->where('stf_player_image.id_player', $player->id)
			                  ->order_by('stf_player_image.id', 'desc')
			                  ->get();

			if ($query->num_rows() == 1) { array_push($player->images, $query->row()); }
		}
		
		return $player;
	}

	public function getAll() {
		$query = $this->db->order_by('tx_firstname', 'asc')->select('id,id_uniq, tx_firstname, tx_lastname, tx_phone_prefix, tx_phone, tx_email')->where('bo_status', '1')->get('stf_player');

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}

	public function edit($id_uniq, $images)
	{
		$phone_prefix = ($this->input->post('phonenumber') != "") ? $this->input->post('phoneprefix') : "";
		
		$data = array(
			'tx_firstname' => $this->input->post('firstname'),
			'tx_secondname' => $this->input->post('secondname'),
			'tx_lastname' => $this->input->post('first_lastname'),
			'tx_secondlastname' => $this->input->post('second_lastname'),
			'dt_birthdate' => mdate("%Y-%m-%d", strtotime($this->input->post('birthdate'))),
			'id_gender' => $this->input->post('gender'),
			'id_country' => $this->input->post('country'),
			'id_state' => $this->input->post('state'),
			'tx_address' => $this->input->post('address'),
			'tx_city'		 => $this->input->post('city'), 
			'tx_zipcode' => $this->input->post('zipcode'),
			'tx_phone_prefix' => $phone_prefix,
			'tx_phone' => $this->input->post('phonenumber'),
			'tx_email' => $this->input->post('email'),
		);

		$this->db->trans_start();
		$this->db->limit(1)->where('id_uniq', $id_uniq)->update('stf_player', $data);

		$id_player = $this->getID($id_uniq);

		if (!empty($images))
		{
			$data = array();

			foreach ($images as $key => $image)
			{
				$this->db->insert('stf_image', array('tx_name' => $image['path'], 'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))));
				array_push($data, array('id_player' => $id_player, 'id_image' => $this->db->insert_id(), 'id_type_image' => $image['type']));
			}

			$this->db->insert_batch('stf_player_image', $data);
		}

		$this->db->trans_complete();

		return $id_uniq;
	}

	public function checkEmailForEdit($id_uniq)
	{
		$query = $this->db->limit(1)
						  ->where_not_in('id_uniq', $id_uniq)
						  ->where('tx_email', $this->input->input_stream('email'))
						  ->select('tx_email')
						  ->get('stf_player');

		return ($query->num_rows() == 1) ? TRUE : FALSE;
	}

	public function getCurrentTeam($id_uniq) 
	{
		$query = $this->db->limit(1)->select('id')->get_where('stf_player', array('id_uniq' => $id_uniq));

		if ($query->num_rows() == 1)
		{
			$id_player = $query->row()->id;

			$query = $this->db->limit(1)
			                  ->select('stf_team_player.id AS id_team_player, stf_team_player.nu_dorsal, stf_team_player.dt_admission, stf_team_player.dt_discharge, stf_team_player.dt_register, stf_team.tx_name')
			                  ->from('stf_team_player')
			                  ->join('stf_team', 'stf_team.id = stf_team_player.id_team')
			                  ->where('stf_team_player.id_player', $id_player)
			                  ->order_by('stf_team_player.id', 'desc')
			                  ->get();

			return $query->row();
		}
	}

	public function createPlayerTeam($id_uniq, $current_team_dates)
	{
		$query = $this->db->limit(1)->select('id')->get_where('stf_player', array('id_uniq' => $id_uniq));

		if ($query->num_rows() == 1)
		{
			$id_player = $query->row()->id;
			$discharge_date = ($this->input->post('discharge_date') != "") ? mdate("%Y-%m-%d", strtotime($this->input->post('discharge_date'))) : NULL;
			$positions = $this->input->post('positions');
			$positions_to_save = array();

			$data = array(
				'id_team' => $this->input->post('team'),
				'id_player' => $id_player,
				'nu_dorsal' => $this->input->post('number'),
				'dt_admission' => mdate("%Y-%m-%d", strtotime($this->input->post('admission_date'))),
				'dt_discharge' => $discharge_date,
				'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
			);

			$this->db->trans_start();

			if ($current_team_dates == TRUE)
			{
				$current_team_data = array(
					'dt_discharge' => mdate("%Y-%m-%d", strtotime($this->input->post('current_team[discharge_date]')))
				);

				$this->db->limit(1)->where('id', $this->input->post('current_team[id_team_player]'))->update('stf_team_player', $current_team_data);
			}

			$this->db->insert('stf_team_player', $data);
			$id_team_player = $this->db->insert_id();

			foreach ($positions as $key => $position)
			{
				array_push($positions_to_save, array(
					'id_team_player' => $id_team_player,
					'id_position' => $position['id_position']
				));
			}

			$this->db->insert_batch('stf_team_player_position', $positions_to_save);

			$this->db->trans_complete();
		}
	}

	public function getPlayerTeams($id_uniq)
	{
		$result = NULL;
		$team_player_ids = array();
		
		$query = $this->db->limit(1)->select('id')->get_where('stf_player', array('id_uniq' => $id_uniq));

		if ($query->num_rows() == 1)
		{
			$id_player = $query->row()->id;

			$query = $this->db->select('stf_team_player.id,stf_team_player.id_team, stf_team_player.nu_dorsal, stf_team_player.dt_admission, stf_team_player.dt_discharge, stf_team_player.dt_register, stf_team.tx_name AS team')
			                  ->from('stf_team_player')
			                  ->join('stf_team', 'stf_team.id = stf_team_player.id_team')
			                  ->where('stf_team_player.id_player', $id_player)
			                  ->where('stf_team_player.bo_status', '1')
			                  ->order_by('stf_team_player.id', 'desc')
			                  ->get();

			if ($query->num_rows() > 0)
			{
				$result = $query->result();
				
				foreach ($result as $key => $row)
				{
					array_push($team_player_ids, $row->id);
				}

				$query = $this->db->select('stf_team_player_position.id_team_player, stf_position.tx_name AS position, stf_position.tx_abbr AS position_abbr')
				                  ->from('stf_team_player_position')
				                  ->join('stf_position', 'stf_position.id = stf_team_player_position.id_position')
				                  ->where_in('stf_team_player_position.id_team_player', $team_player_ids)
				                  ->get();

				foreach ($query->result() as $key => $row)
				{
					$index = array_search($row->id_team_player, $team_player_ids);
					$result[$index]->positions[] = $row;
				}
			}
		}

		return $result;
	}

	public function getPlayerTeam($id_uniq, $id_team_player)
	{
		$result = NULL;

		$query = $this->db->limit(1)->select('id')->get_where('stf_player', array('id_uniq' => $id_uniq));

		if ($query->num_rows() == 1)
		{
			$id_player = $query->row()->id;

			$query = $this->db->limit(1)->select('id_team, nu_dorsal, dt_admission, dt_discharge')->get_where('stf_team_player', array('id' => $id_team_player, 'id_player' => $id_player));

			if ($query->num_rows() == 1)
			{
				$result = $query->row();

				$query = $this->db->select('stf_team_player_position.id_position, stf_position.tx_name, stf_position.tx_abbr')
				                  ->from('stf_team_player_position')
				                  ->join('stf_position', 'stf_position.id = stf_team_player_position.id_position')
				                  ->where('stf_team_player_position.id_team_player', $id_team_player)
				                  ->get();	

				$result->positions = $query->result();
			}
		}

		return $result;
	}

	public function editPlayerTeam($id_uniq, $id_team_player)
	{
		$query = $this->db->limit(1)->select('id')->get_where('stf_player', array('id_uniq' => $id_uniq));

		if ($query->num_rows() == 1)
		{
			$id_player = $query->row()->id;
			$discharge_date = ($this->input->input_stream('discharge_date') != "") ? mdate("%Y-%m-%d", strtotime($this->input->input_stream('discharge_date'))) : NULL;
			$positions = $this->input->input_stream('positions');
			$positions_to_save = array();

			$data = array(
				'id_team' => $this->input->input_stream('team'),
				'nu_dorsal' => $this->input->input_stream('number'),
				'dt_admission' => mdate("%Y-%m-%d", strtotime($this->input->input_stream('admission_date'))),
				'dt_discharge' => $discharge_date
			);


			$this->db->trans_start();

			$this->db->limit(1)->where(array('id' => $id_team_player, 'id_player' => $id_player))->update('stf_team_player', $data);

			$this->db->delete('stf_team_player_position', array('id_team_player' => $id_team_player));

			foreach ($positions as $key => $position)
			{
				array_push($positions_to_save, array(
					'id_team_player' => $id_team_player,
					'id_position' => $position['id_position']
				));
			}

			$this->db->insert_batch('stf_team_player_position', $positions_to_save);

			$this->db->trans_complete();
		}
	}

	public function getPlayerImages($id_uniq)
	{
		$images = array();

		$id_player = $this->getID($id_uniq);

		if ($id_player != NULL)
		{
			$query = $this->db->limit(1)
			                  ->select('stf_image.id AS id_image, stf_image.tx_name AS route, stf_type_image.id AS id_type_image, stf_type_image.tx_name AS type')
			                  ->from('stf_player_image')
			                  ->join('stf_image', 'stf_image.id = stf_player_image.id_image')
			                  ->join('stf_type_image', 'stf_type_image.id = stf_player_image.id_type_image')
			                  ->where('stf_player_image.id_type_image', 1)
			                  ->where('stf_player_image.id_player', $id_player)
			                  ->order_by('stf_player_image.id', 'desc')
			                  ->get();

			if ($query->num_rows() == 1) { array_push($images, $query->row()); }

			$query = $this->db->limit(1)
			                  ->select('stf_image.id AS id_image, stf_image.tx_name AS route, stf_type_image.id AS id_type_image, stf_type_image.tx_name AS type')
			                  ->from('stf_player_image')
			                  ->join('stf_image', 'stf_image.id = stf_player_image.id_image')
			                  ->join('stf_type_image', 'stf_type_image.id = stf_player_image.id_type_image')
			                  ->where('stf_player_image.id_type_image', 2)
			                  ->where('stf_player_image.id_player', $id_player)
			                  ->order_by('stf_player_image.id', 'desc')
			                  ->get();

			if ($query->num_rows() == 1) { array_push($images, $query->row()); }
		}

		return $images;
	}

	public function getPlayerHeadImage($id_uniq)
	{
		$images = NULL;

		$id_player = $this->getID($id_uniq);

		if ($id_player != NULL)
		{
			$query = $this->db->limit(1)
			                  ->select('stf_image.id AS id_image, stf_image.tx_name AS route, stf_type_image.id AS id_type_image, stf_type_image.tx_name AS type')
			                  ->from('stf_player_image')
			                  ->join('stf_image', 'stf_image.id = stf_player_image.id_image')
			                  ->join('stf_type_image', 'stf_type_image.id = stf_player_image.id_type_image')
			                  ->where('stf_player_image.id_type_image', 1)
			                  ->where('stf_player_image.id_player', $id_player)
			                  ->order_by('stf_player_image.id', 'desc')
			                  ->get();

			$images = $query->row();
		}

		return $images;
	}

	public function getPlayerBodyImage($id_uniq)
	{
		$images = NULL;

		$id_player = $this->getID($id_uniq);

		if ($id_player != NULL)
		{
			$query = $this->db->limit(1)
			                  ->select('stf_image.id AS id_image, stf_image.tx_name AS route, stf_type_image.id AS id_type_image, stf_type_image.tx_name AS type')
			                  ->from('stf_player_image')
			                  ->join('stf_image', 'stf_image.id = stf_player_image.id_image')
			                  ->join('stf_type_image', 'stf_type_image.id = stf_player_image.id_type_image')
			                  ->where('stf_player_image.id_type_image', 2)
			                  ->where('stf_player_image.id_player', $id_player)
			                  ->order_by('stf_player_image.id', 'desc')
			                  ->get();

			$images = $query->row();
		}

		return $images;
	}

	public function getPlayerSpecificImage($id)
	{
		return $this->db->limit(1)->select('tx_name')->get_where('stf_image', array('id' => $id))->row()->tx_name;
	}

	public function delete() {
		$this->db->where('id_uniq', $this->input->input_stream('id_player'));
    	$this->db->update('stf_player', array('bo_status' => '0'));
	}

	public function deleteTeam() {
		$data = array("id_team"=>$this->input->input_stream('id_team'),"id_player"=>$this->input->input_stream('id_player'));
		$this->db->where($data);
    	$this->db->update('stf_team_player', array('bo_status' => '0'));
	}

	public function multidelete() {
		foreach($this->input->input_stream('id_player') as $id)
		{
			$this->db->where('id_uniq', $id);
    	$this->db->update('stf_player', array('bo_status' => '0'));
		}
	}

	public function multideleteTeam() {
		foreach($this->input->input_stream('id_player') as $id){
		$data = array("id_team"=>$this->input->input_stream('id_team'),"id_player"=>$id);
			$this->db->where($data);
    	$this->db->update('stf_team_player', array('bo_status' => '0'));
    }
	}

	public function check_child( $type ){
		$this->db->where('id_player' , $this->getID($this->input->input_stream('id_player')));
		return $this->db->get('stf_'.$type)->num_rows();
	}

	public function check_child_multiple( $type ){
		foreach($this->input->input_stream('id_player') as $id){
			$this->db->where('id_player' , $this->getID($id));
			return $this->db->get('stf_'.$type)->num_rows();
		}
	}

}