<?php

/**
* 
*/
class Movement_model extends CI_Model
{
	public function check($id_player, $id_event, $tests, $type)
	{
		$arr = array('id_player' => $id_player, 'id_event' => $id_event );
		if( $type === 1 ) $arr['bo_status'] = $type;
		/*
		 *  0 => La prueba no existe
		 *  1 => La prueba existe y el jugador fue evaluado
		 *  2 => La prueba existe y el jugador no ha sido evaluado
		*/

		$checks = array(
			'linear' => 0,
			'lateral' => 0,
			'zigzag' => 0
		);

		foreach ($tests as $key => $test)
		{
			if ($test == "Linear")
			{
				$query = $this->db->limit(1)->select('id')->get_where('stf_movement_linear', $arr);

				$checks['linear'] = ($query->num_rows() == 1) ? 1 : 2;
			}
			else if ($test == "Lateral")
			{
				$query = $this->db->limit(1)->select('id')->get_where('stf_movement_lateral', $arr);

				$checks['lateral'] = ($query->num_rows() == 1) ? 1 : 2;
			}
			elseif ($test == "Zigzag")
			{
				$query = $this->db->limit(1)->select('id')->get_where('stf_movement_zigzag', $arr);

				$checks['zigzag'] = ($query->num_rows() == 1) ? 1 : 2;
			}
		}

		return $checks;
	}

	public function check_for_insert($player_id, $event_id, $type){
    $arr = array('id_player' => $player_id, 'id_event' => $event_id);

    if($type == "linear"){

      $query = $this->db->limit(1)->select('bo_status')->get_where('stf_movement_linear', $arr )->row()->bo_status;
      if($query != '' || $query != null){
        return $query;
      } else {
        return "nf";
      }
    }else if($type == "lateral"){

      $query = $this->db->limit(1)->select('bo_status')->get_where('stf_movement_lateral', $arr )->row()->bo_status;
      if($query != '' || $query != null){
        return $query;
      } else {
        return "nf";
      }

    }else if($type == "zigzag"){

      $query = $this->db->limit(1)->select('bo_status')->get_where('stf_movement_zigzag', $arr )->row()->bo_status;
      if($query != '' || $query != null){
        return $query;
      } else {
        return "nf";
      }
    }
  }

	public function createLinear()
	{
		$id_uniq = uniqid(mt_rand());

		$data = array(
			'id_uniq' => $id_uniq,
			'id_player' => $this->input->post('id_player'),
			'id_event' => $this->input->post('id_event'),
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_time_target_one' => $this->input->post('checkpoints[0]'),
			'nu_time_target_two' => $this->input->post('checkpoints[1]'),
			'nu_time_target_three' => $this->input->post('checkpoints[2]'),
			'nu_time_target_four' => $this->input->post('checkpoints[3]'),
			'nu_time_target_five' => $this->input->post('checkpoints[4]'),
			'nu_time_target_six' => $this->input->post('checkpoints[5]'),
			'nu_time_target_seven' => $this->input->post('checkpoints[6]'),
			'nu_time_target_eight' => $this->input->post('checkpoints[7]'),
			'nu_time_target_nine' => $this->input->post('checkpoints[8]'),
			'nu_time_target_ten' => $this->input->post('checkpoints[9]'),
			'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
		);

		$this->db->insert('stf_movement_linear', $data);
	}

	public function createLateral()
	{
		$id_uniq = uniqid(mt_rand());

		$data = array(
			'id_uniq' => $id_uniq,
			'id_player' => $this->input->post('id_player'),
			'id_event' => $this->input->post('id_event'),
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_time_target_one' => $this->input->post('checkpoints[0]'),
			'nu_time_target_two' => $this->input->post('checkpoints[1]'),
			'nu_time_target_three' => $this->input->post('checkpoints[2]'),
			'nu_time_target_four' => $this->input->post('checkpoints[3]'),
			'nu_time_target_five' => $this->input->post('checkpoints[4]'),
			'nu_time_target_six' => $this->input->post('checkpoints[5]'),
			'nu_time_target_seven' => $this->input->post('checkpoints[6]'),
			'nu_time_target_eight' => $this->input->post('checkpoints[7]'),
			'nu_time_target_nine' => $this->input->post('checkpoints[8]'),
			'nu_time_target_ten' => $this->input->post('checkpoints[9]'),
			'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
		);

		$this->db->insert('stf_movement_lateral', $data);
	}

	public function createZigzag()
	{
		$id_uniq = uniqid(mt_rand());

		$data = array(
			'id_uniq' => $id_uniq,
			'id_player' => $this->input->post('id_player'),
			'id_event' => $this->input->post('id_event'),
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_time_target_one' => $this->input->post('checkpoints[0]'),
			'nu_time_target_two' => $this->input->post('checkpoints[1]'),
			'nu_time_target_three' => $this->input->post('checkpoints[2]'),
			'nu_time_target_four' => $this->input->post('checkpoints[3]'),
			'nu_time_target_five' => $this->input->post('checkpoints[4]'),
			'nu_time_target_six' => $this->input->post('checkpoints[5]'),
			'nu_time_target_seven' => $this->input->post('checkpoints[6]'),
			'nu_time_target_eight' => $this->input->post('checkpoints[7]'),
			'nu_time_target_nine' => $this->input->post('checkpoints[8]'),
			'nu_time_target_ten' => $this->input->post('checkpoints[9]'),
			'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
		);

		$this->db->insert('stf_movement_zigzag', $data);
	}

	public function updateExistingLinear( $player_id, $event_id )
	{

		$data = array(
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_time_target_one' => $this->input->post('checkpoints[0]'),
			'nu_time_target_two' => $this->input->post('checkpoints[1]'),
			'nu_time_target_three' => $this->input->post('checkpoints[2]'),
			'nu_time_target_four' => $this->input->post('checkpoints[3]'),
			'nu_time_target_five' => $this->input->post('checkpoints[4]'),
			'nu_time_target_six' => $this->input->post('checkpoints[5]'),
			'nu_time_target_seven' => $this->input->post('checkpoints[6]'),
			'nu_time_target_eight' => $this->input->post('checkpoints[7]'),
			'nu_time_target_nine' => $this->input->post('checkpoints[8]'),
			'nu_time_target_ten' => $this->input->post('checkpoints[9]'),
			'bo_status'		=> 1
		);

		$array = array('id_player' => $player_id, 'id_event' => $event_id);
		$this->db->limit(1)->where($array)->update('stf_movement_linear', $data);
	}

	public function updateExistingLateral( $player_id, $event_id )
	{

		$data = array(
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_time_target_one' => $this->input->post('checkpoints[0]'),
			'nu_time_target_two' => $this->input->post('checkpoints[1]'),
			'nu_time_target_three' => $this->input->post('checkpoints[2]'),
			'nu_time_target_four' => $this->input->post('checkpoints[3]'),
			'nu_time_target_five' => $this->input->post('checkpoints[4]'),
			'nu_time_target_six' => $this->input->post('checkpoints[5]'),
			'nu_time_target_seven' => $this->input->post('checkpoints[6]'),
			'nu_time_target_eight' => $this->input->post('checkpoints[7]'),
			'nu_time_target_nine' => $this->input->post('checkpoints[8]'),
			'nu_time_target_ten' => $this->input->post('checkpoints[9]'),
			'bo_status'		=> 1
		);

		$array = array('id_player' => $player_id, 'id_event' => $event_id);
		$this->db->limit(1)->where($array)->update('stf_movement_lateral', $data);
	}

	public function updateExistingZigzag( $player_id, $event_id )
	{
		$data = array(
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_time_target_one' => $this->input->post('checkpoints[0]'),
			'nu_time_target_two' => $this->input->post('checkpoints[1]'),
			'nu_time_target_three' => $this->input->post('checkpoints[2]'),
			'nu_time_target_four' => $this->input->post('checkpoints[3]'),
			'nu_time_target_five' => $this->input->post('checkpoints[4]'),
			'nu_time_target_six' => $this->input->post('checkpoints[5]'),
			'nu_time_target_seven' => $this->input->post('checkpoints[6]'),
			'nu_time_target_eight' => $this->input->post('checkpoints[7]'),
			'nu_time_target_nine' => $this->input->post('checkpoints[8]'),
			'nu_time_target_ten' => $this->input->post('checkpoints[9]'),
			'bo_status'		=> 1
		);

		$array = array('id_player' => $player_id, 'id_event' => $event_id);
		$this->db->limit(1)->where($array)->update('stf_movement_zigzag', $data);
	}


	public function getAll($id_event)
	{
		$where = "((stf_movement_linear.bo_status=1 or stf_movement_lateral.bo_status=1 or 'stf_movement_zigzag.id_event'=1 ) or ('stf_movement_linear.id_event'='".$id_event."' or 'stf_movement_lateral.id_event'='".$id_event."' or 'stf_movement_zigzag.id_event'='".$id_event."'))";
		
		$query = $this->db->select('stf_player.id, stf_player.tx_firstname, stf_player.tx_secondname, stf_player.tx_lastname, stf_player.tx_secondlastname')
		                  ->from('stf_player')
		                  ->join('stf_movement_linear', 'stf_movement_linear.id_player = stf_player.id', 'left outer')
		                  ->join('stf_movement_lateral', 'stf_movement_lateral.id_player = stf_player.id', 'left outer')
		                  ->join('stf_movement_zigzag', 'stf_movement_zigzag.id_player = stf_player.id', 'left outer')
		                  ->where($where)
		                  // ->or_where('stf_movement_linear.id_event', $id_event)    
		                  // ->or_where('stf_movement_lateral.id_event', $id_event)
		                  // ->or_where('stf_movement_zigzag.id_event', $id_event)
		                  ->group_by('stf_player.id')
		                  ->order_by('stf_player.tx_firstname', 'asc')
		                  ->get();

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}

	public function delete($type) {
		$this->db->where('id_player', $this->input->input_stream('id_player'));
    	$this->db->update('stf_movement_'.$type, array('bo_status' => '0'));
	}

	public function multidelete($type) {
		foreach($this->input->input_stream('id_player') as $id){
			$this->db->where('id_player', $id);
    	$this->db->update('stf_movement_'.$type, array('bo_status' => '0'));
		}
	}
}