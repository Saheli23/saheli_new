<?php 

/**
* 
*/
class Auth_model extends CI_Model{
	public function getCredentials() {
		$query = $this->db->limit(1)->select('id, id_uniq, tx_email, tx_password, bo_status')
                          ->get_where('stf_user', array('tx_email' => $this->input->post('email')));

        return ($query->num_rows() == 1) ? $query->row() : NULL;
	}
}