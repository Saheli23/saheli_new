<?php

/**
* 
*/
class Forgot_model extends CI_Model
{
  public function check_email_exist()
  {
    $email = $this->input->post('email');
    $where = array('tx_email' => $email, 'bo_status' => 1);

    $query = $this->db->select('id')
      ->from('stf_user')
      ->where($where)
      ->get();

    return $query->num_rows();
  }

  public function new_password( $password )
  {
    $email = $this->input->post('email');
    $this->db->where('tx_email', trim($email));
    $this->db->update('stf_user', array('tx_password' => trim($password)));
  }
}

?>