<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Anthropometric extends REST_Controller
{
	private $userdata;

	public function __construct()
	{
		parent::__construct();

		$token = $this->input->get_request_header('token');

		try {
			
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));

		} catch (Exception $e) {
			
			$this->output->set_status_header(401);
			exit;

		}

		$this->load->model('user_model', 'user');
		$this->load->model('anthropometric_model', 'anthropometric');
	}

	public function _remap($method, $params = array())
	{
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		}

		$this->output->set_status_header(404);
		exit;
	}

	public function index_post()
	{
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_add != 1)
		{
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		}
		else
		{
			$this->load->library('form_validation');

			$this->form_validation->set_rules('id_player', 'Player ID', 'required|integer|trim');
			$this->form_validation->set_rules('id_event', 'Event ID', 'required|integer|trim');
			$this->form_validation->set_rules('measurement_date', 'Measurement Date', 'required|trim');
			$this->form_validation->set_rules('size', 'Size', 'numeric|trim');
			$this->form_validation->set_rules('weight', 'Weight', 'numeric|trim');
			$this->form_validation->set_rules('chest_diameter', 'Chest Diameter', 'numeric|trim');
			$this->form_validation->set_rules('hip_diameter', 'Hip Diameter', 'numeric|trim');
			$this->form_validation->set_rules('left_thigh_diameter', 'Left Thigh Diameter', 'numeric|trim');
			$this->form_validation->set_rules('right_thigh_diameter', 'Right Thigh Diameter', 'numeric|trim');
			$this->form_validation->set_rules('left_ankle_diameter', 'Left Ankle Diameter', 'numeric|trim');
			$this->form_validation->set_rules('right_ankle_diameter', 'Right Ankle Diameter', 'numeric|trim');
			$this->form_validation->set_rules('head_diameter', 'Head Diameter', 'numeric|trim');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE)
			{
				$data['status'] = "error";
				$data['response'] = validation_errors();
			}
			else
			{
				$this->anthropometric->create();

				$data['status'] = "success";
				$data['response'] = "Anthropometric data saved successfully.";
			}
		}

		$this->response($data);
	}

	public function index_get()
	{
		if (!$this->query('id_uniq'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');

		$anthropometric = $this->anthropometric->getOne($id_uniq);

		$data['response'] = ($anthropometric != NULL) ? $anthropometric : "There is no anthropometric data to show.";

		$this->response($data);
	}

	public function index_put()
	{
		if (!$this->query('id_uniq'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');

		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_edit != 1)
		{
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		}
		else
		{
			$this->load->library('form_validation');

			$this->form_validation->set_data($this->input->input_stream());

			$this->form_validation->set_rules('measurement_date', 'Measurement Date', 'required|trim');
			$this->form_validation->set_rules('size', 'Size', 'numeric|trim');
			$this->form_validation->set_rules('weight', 'Weight', 'numeric|trim');
			$this->form_validation->set_rules('chest_diameter', 'Chest Diameter', 'numeric|trim');
			$this->form_validation->set_rules('hip_diameter', 'Hip Diameter', 'numeric|trim');
			$this->form_validation->set_rules('left_thigh_diameter', 'Left Thigh Diameter', 'numeric|trim');
			$this->form_validation->set_rules('right_thigh_diameter', 'Right Thigh Diameter', 'numeric|trim');
			$this->form_validation->set_rules('left_ankle_diameter', 'Left Ankle Diameter', 'numeric|trim');
			$this->form_validation->set_rules('right_ankle_diameter', 'Right Ankle Diameter', 'numeric|trim');
			$this->form_validation->set_rules('head_diameter', 'Head Diameter', 'numeric|trim');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE)
			{
				$data['status'] = "error";
				$data['response'] = validation_errors();
			}
			else
			{
				$this->anthropometric->edit($id_uniq);

				$data['status'] = "success";
				$data['id_uniq_event'] = $this->anthropometric->getEventIdUniq($id_uniq);
				$data['response'] = "Anthropometric data updated successfully.";
			}
		}

		$this->response($data);
	}

	public function check_get()
	{
		if (!$this->query('id_player') || !$this->query('id_event'))
		{
			$this->ouput->set_status_header(400);
			exit;
		}

		$id_player = $this->query('id_player');
		$id_event = $this->query('id_event');

		$data = array();

		$check = $this->anthropometric->check($id_player, $id_event);

		if ($check == TRUE)
		{
			$data['player_exists'] = 1;
			$data['response'] = "This player has already been evaluated.";
		}
		else
		{
			$data['player_exists'] = 0;
			$data['response'] = "";
		}

		$this->response($data);
	}

	public function event_get()
	{
		if (!$this->query('id_uniq_event'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq_event = $this->query('id_uniq_event');

		$this->load->model('event_model', 'event');

		$id_event = $this->event->getID($id_uniq_event);

		if ($id_event == NULL)
		{
			$data['response'] = "There are no details to show.";
		}
		else
		{
			$anthropometrics = $this->anthropometric->getAll($id_event);

			$data['response'] = ($anthropometrics != NULL) ? $anthropometrics : "There are no anthropometric measurements for this event.";
		}

		$this->response($data);
	}

	public function player_weight_get()
	{
		if (!$this->query('id_player') || !$this->query('id_event'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_player = $this->query('id_player');
		$id_event = $this->query('id_event');
		$weight = $this->anthropometric->getPlayerWeight($id_player, $id_event);

		if ($weight == NULL)
		{
			$data['response'] = "This player has not been measured in anthropometrics. Please take the anthropometric data of the player for this event.";
		}
		else
		{
			$data['response'] = $weight;
		}

		$this->response($data);
	}
}