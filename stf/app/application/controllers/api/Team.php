<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Team extends REST_Controller
{
	private $userdata;

	public function __construct()
	{
		parent::__construct();

		$token = $this->input->get_request_header('token');

		try {
			
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));

		} catch (Exception $e) {
			
			$this->output->set_status_header(401);
			exit;

		}

		$this->load->model('user_model', 'user');
		$this->load->model('team_model', 'team');
	}

	public function _remap($method, $params = array())
	{
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		}

		$this->output->set_status_header(404);
		exit;
	}

	public function index_get()
	{
		if (!$this->query('id_uniq'))
		{
			$teams = $this->team->getAll();

			$data['response'] = ($teams != NULL) ? $teams : "There are no teams to show.";
		}
		else
		{
			$id_uniq = $this->query('id_uniq');

			$team = $this->team->getOne($id_uniq);

			$data['response'] = ($team != NULL) ? $team : "There are no details to show.";
		}

		$this->response($data);
	}

	public function index_post()
	{
		$data = array();

		if (!$this->query('id_uniq'))
		{
			$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

			if ($user_permissions->bo_add != 1)
			{
				$data['status'] = "error";
				$data['response'] = "You do not have permission to perform this action";
			}
			else
			{
				$this->load->library('form_validation');

				$this->form_validation->set_rules('name', 'Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('country', 'Country', 'required|integer|trim');
				$this->form_validation->set_rules('state', 'State', 'required|integer|trim');
				$this->form_validation->set_rules('address', 'Address', 'required|min_length[3]|max_length[200]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('zipcode', 'Zipcode', 'max_length[50]|trim');
				$this->form_validation->set_rules('phoneprefix', 'Phone Prefix', 'numeric|max_length[10]|trim');
				$this->form_validation->set_rules('phonenumber', 'Phonenumber', 'numeric|max_length[50]|trim');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[100]|is_unique[stf_team.tx_email]|trim|mb_strtolower');
				$this->form_validation->set_rules('website', 'Website', 'valid_url|max_length[150]|trim|mb_strtolower');
				$this->form_validation->set_rules('facebook', 'Facebook Page', 'valid_url|max_length[150]|trim|mb_strtolower');
				$this->form_validation->set_rules('instagram', 'Instagram Page', 'valid_url|max_length[150]|trim|mb_strtolower');
				$this->form_validation->set_rules('twitter', 'Twitter Page', 'valid_url|max_length[150]|trim|mb_strtolower');
				$this->form_validation->set_rules('categories[]', 'Categories', 'required');

				$this->form_validation->set_error_delimiters('', '<br>');

				if ($this->form_validation->run() == FALSE)
				{
					$data['status'] = "error";
					$data['response'] = validation_errors();
				}
				else
				{
					$image_path = NULL;

					if ($_FILES)
					{
						$upload = $this->upload_logo();

						if ($upload['status'] == "success")
						{
							$image_path = "media/teams/".$upload['response']['file_name'];
						}
						else
						{
							$data['status'] = "error";
							$data['response'] = $upload['response'];

							echo json_encode($data);
							exit;
						}
					}

					$id_uniq = $this->team->create($image_path);

					$data['status'] = "success";
					$data['id_uniq'] = $id_uniq;
					$data['has_image'] = ($image_path != NULL) ? 1 : 0;
					$data['response'] = "Team created successfully";
				}
			}
		}
		else
		{
			$id_uniq = $this->query('id_uniq');
			$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

			if ($user_permissions->bo_edit != 1)
			{
				$data['status'] = "error";
				$data['response'] = "You do not have permission to perform this action";
			}
			else
			{
				$this->load->library('form_validation');

				$this->form_validation->set_rules('name', 'Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('country', 'Country', 'required|integer|trim');
				$this->form_validation->set_rules('state', 'State', 'required|integer|trim');
				$this->form_validation->set_rules('address', 'Address', 'required|min_length[3]|max_length[200]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('zipcode', 'Zipcode', 'max_length[50]|trim');
				$this->form_validation->set_rules('phoneprefix', 'Phone Prefix', 'numeric|max_length[10]|trim');
				$this->form_validation->set_rules('phonenumber', 'Phonenumber', 'numeric|max_length[50]|trim');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[100]|trim|mb_strtolower');
				$this->form_validation->set_rules('website', 'Website', 'valid_url|max_length[150]|trim|mb_strtolower');
				$this->form_validation->set_rules('facebook', 'Facebook Page', 'valid_url|max_length[150]|trim|mb_strtolower');
				$this->form_validation->set_rules('instagram', 'Instagram Page', 'valid_url|max_length[150]|trim|mb_strtolower');
				$this->form_validation->set_rules('twitter', 'Twitter Page', 'valid_url|max_length[150]|trim|mb_strtolower');
				$this->form_validation->set_rules('categories[]', 'Categories', 'required');

				$this->form_validation->set_error_delimiters('', '<br>');

				if ($this->form_validation->run() == FALSE)
				{
					$data['status'] = "error";
					$data['response'] = validation_errors();
				}
				else
				{
					if ($this->team->checkEmailForEdit($id_uniq) == TRUE)
					{
						$data['status'] = "error";
						$data['response'] = "The Email field must contain a unique value.";
					}
					else
					{
						$image_path = NULL;

						if ($_FILES)
						{
							$upload = $this->upload_logo();

							if ($upload['status'] == "success")
							{
								$image_path = "media/teams/".$upload['response']['file_name'];
							}
							else
							{
								$data['status'] = "error";
								$data['response'] = $upload['response'];

								echo json_encode($data);
								exit;
							}
						}

						$this->team->edit($id_uniq, $image_path);

						$data['status'] = "success";
						$data['id_uniq'] = $id_uniq;
						$data['has_image'] = ($image_path != NULL) ? 1 : 0;
						$data['response'] = "Team updated successfully";
					}
				}
			}
		}

		$this->response($data);
	}

	public function upload_logo()
	{
		$config['upload_path'] = "media/teams/";
		$config['allowed_types'] = "jpg|jpeg|png";
		$config['max_size'] = "2048";
		$config['file_ext_tolower'] = TRUE;
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload())
		{
			$upload['status'] = "error";
			$upload['response'] = $this->upload->display_errors('', '<br>');
		}
		else
		{
			$upload['status'] = "success";
			$upload['response'] = $this->upload->data();
		}

		return $upload;
	}

	public function uniqids_get()
	{
		$result = $this->team->getUniqIDs();

		$data['response'] = $result;

		$this->response($data);
	}

	public function categories_get()
	{
		$categories = $this->team->getCategories();

		$data['response'] = ($categories != NULL) ? $categories : "There are no categories to show.";

		$this->response($data);
	}

	public function positions_get()
	{
		$positions = $this->team->getPositions();

		$data['response'] = ($positions != NULL) ? $positions : "There are no positions to show.";

		$this->response($data);
	}

	public function base_image_get()
	{
		if (!$this->query('id_uniq'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');
		$image = $this->team->getTeamImage($id_uniq);

		if ($image != NULL)
		{
			$this->load->helper('file');

			$path = $image->tx_image;
			$type = get_mime_by_extension($path);
			$file = file_get_contents($path);
			$base64 = "data:".$type.";base64,".base64_encode($file);

			$data['is_image'] = 1;
			$data['response'] = $base64;
		}
		else
		{
			$data['is_image'] = 0;
			$data['response'] = "There is no image to show for the requested team.";
		}

		$this->response($data);
	}

	public function delete_put(){
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
		
		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status']   = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				if( $this->team->check_child('event') == 0 ){
					$this->team->delete();
					$data['status']   = "success";
					$data['response'] = "Team deleted successfully.";
				} else {
					$data['status']   = "danger";
					$data['response'] = "Team cannot be deleted.To delete Team, you have to delete all the data related to Team";
				}
			}
		} else {
			/*$this->kicking->updateForce();
			$data['status']   = "success";
			$data['response'] = "Kicking Force data updated successfully.";*/
		}

		$this->response($data);

	}

	public function multidelete_put(){
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
		
		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status']   = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				if( $this->team->check_child_multidelete('event') == 0 ){
					$this->team->multidelete();
					$data['status']   = "success";
					$data['response'] = "Teams deleted successfully.";
				} else {
					$data['status']   = "danger";
					$data['response'] = "Teams cannot be deleted.To delete Team, you have to delete all the data related to Team";
				}
			}
		} else {
			/*$this->kicking->updateForce();
			$data['status']   = "success";
			$data['response'] = "Kicking Force data updated successfully.";*/
		}
		$this->response($data);

	}
}