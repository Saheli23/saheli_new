<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Forgot extends REST_Controller {
 public function __construct() {
    parent::__construct();
    $this->load->model('forgot_model', 'forgot');
  }

  public function _remap($method, $params = array()) {
    $method = $method.'_'.$this->request->method;

    if (method_exists($this, $method)) {
      return call_user_func_array(array($this, $method), $params);
    }

    $this->output->set_status_header(404);
    exit;
  }

  public function passwordforgot_post() {
    $data = array();

    $this->load->library('form_validation');

    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[150]|trim|strtolower');

    $this->form_validation->set_error_delimiters('', '<br>');

    if ($this->form_validation->run() == FALSE) {
      $data['status'] = "error";
      $data['response'] = validation_errors();
    } else {

      $check = $this->forgot->check_email_exist();

      if($check == 1)
      {
         $password = uniqid(mt_rand()); 
         $password_hash = hash_hmac("sha256", $password, $this->config->item("password_key"));

        $this->forgot->new_password($password_hash);

        $email = $this->input->post('email');
        $mail_body = 'Hi, <br><br> Your STF Portal Password is:<br><strong>'.trim($password).'</strong><br><br> Regards, <br> STF Team';
          
          // Always set content-type when sending HTML email
          $headers = 'MIME-Version: 1.0' . "\r\n";
          $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

          // More headers
          $headers = 'From: <admin@stf.com>' . "\r\n";
          $to = $email;
          $subject = 'Reset Password';
          $message = '<html><body>';
          $message .= $mail_body;
          $message .= '</body></html>';

          if(mail($to,$subject,$message,"Content-type: text/html; charset=iso-8859-1")){
            $data['status'] = "success";
            $data['response'] = 'Your new password has been sent iSuccessful!';
          }else{
            $data['status'] = "error";
            $data['response'] = 'Email id does not exist!';
          }
      }else{
        $data['status'] = "error";
        $data['response'] = 'Email id does not exist!';
      }
    }
    $this->response($data);
  }
}