<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class User extends REST_Controller
{
	private $userdata;

	public function __construct()
	{
		parent::__construct();

		$token = $this->input->get_request_header('token');

		try {
			
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));

		} catch (Exception $e) {
			
			$this->output->set_status_header(401);
			exit;

		}

		$this->load->model('user_model', 'user');
	}

	public function _remap($method, $params = array())
	{
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		}

		$this->output->set_status_header(404);
		exit;
	}

	public function index_get() {
		if (!$this->query('id_uniq')) {
			$users = $this->user->getAll();
			$data['response'] = ($users != NULL) ? $users : "There are no details to show.";
		} else {
			$userDetails    = array();
			$id_uniq        = $this->get('id_uniq');
			$user           = $this->user->getOne($id_uniq);
			
			foreach( $user as $key => $value ){
				$userDetails['id_uniq']      = $value->id_uniq;
				$userDetails['tx_firstname'] = $value->tx_firstname;
				$userDetails['tx_lastname']  = $value->tx_lastname;
				$userDetails['tx_email']     = $value->tx_email;
				$userDetails['tx_image']     = $value->tx_image;
				$userDetails['dt_register']     = $value->dt_register;
				$userDetails['bo_admin']     = $value->bo_admin;

				$userDetails['sections'][$key]['id']     = $value->id_section;
				$userDetails['sections'][$key]['name']   = $value->section_name;
				$userDetails['sections'][$key]['view']   = $value->bo_view;
				$userDetails['sections'][$key]['add']    = $value->bo_add;
				$userDetails['sections'][$key]['edit']   = $value->bo_edit;
				$userDetails['sections'][$key]['delete'] = $value->bo_delete;
			}
			$data['response'] = ($userDetails != NULL) ? $userDetails : "There are no details to show.";
		}

		$this->response($data);
	}

	public function index_post() {
		$data = array();

		if (!$this->query('id_uniq')) {
			$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

			if ($user_permissions->bo_admin != 1) {
				$data['status'] = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				$this->load->library('form_validation');

				$this->form_validation->set_rules('firstname', 'First Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('lastname', 'Last Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('email', 'Email', 'required|max_length[150]|valid_email|is_unique[stf_user.tx_email]|trim|mb_strtolower');
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('is_admin', 'User Type', 'required|in_list[0,1]');
				/*$this->form_validation->set_rules('view', 'View Permission', 'required|in_list[0,1]');
				$this->form_validation->set_rules('add', 'Add Permission', 'in_list[0,1]');
				$this->form_validation->set_rules('edit', 'Edit Permission', 'in_list[0,1]');
				$this->form_validation->set_rules('delete', 'Delete Permission', 'in_list[0,1]');*/
				
				$this->form_validation->set_error_delimiters('', '<br>');

				if ($this->form_validation->run() == FALSE) {
					$data['status'] = "error";
					$data['response'] = validation_errors();
				} else {
					$image_path = NULL;

					if ($_FILES) {
						$upload = $this->upload_profile_picture();

						if ($upload['status'] == "success") {
							$image_path = "media/users/".$upload['response']['file_name'];
						} else {
							$data['status'] = "error";
							$data['response'] = $upload['respomse'];

							echo json_decode($data);
							exit;
						}
					}

					$id_uniq = $this->user->create($image_path);

					$data['status'] = "success";
					$data['id_uniq'] = $id_uniq;
					$data['has_image'] = ($image_path != NULL) ? 1 : 0;
					$data['response'] = "User created successfully";
				}
			}
		} else {
			$id_uniq = $this->query('id_uniq');
			$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

			if ($user_permissions->bo_admin != 1) {
				$data['status'] = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				$this->load->library('form_validation');

				$this->form_validation->set_rules('firstname', 'First Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('lastname', 'Last Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('email', 'Email', 'required|max_length[150]|valid_email|trim|mb_strtolower');
				$this->form_validation->set_rules('password', 'Password', '');
				$this->form_validation->set_rules('is_admin', 'User Type', 'required|in_list[0,1]');
				/*$this->form_validation->set_rules('view', 'View Permission', 'required|in_list[0,1]');
				$this->form_validation->set_rules('add', 'Add Permission', 'in_list[0,1]');
				$this->form_validation->set_rules('edit', 'Edit Permission', 'in_list[0,1]');
				$this->form_validation->set_rules('delete', 'Delete Permission', 'in_list[0,1]');*/
				
				$this->form_validation->set_error_delimiters('', '<br>');

				if ($this->form_validation->run() == FALSE) {
					$data['status'] = "error";
					$data['response'] = validation_errors();
				} else {
					if ($this->user->checkEmailForEdit($id_uniq) == TRUE) {
						$data['status'] = "error";
						$data['response'] = "The Email field must contain a unique value.";
					} else {
						$image_path = NULL;

						if ($_FILES) {
							$upload = $this->upload_profile_picture();

							if ($upload['status'] == "success") {
								$image_path = "media/users/".$upload['response']['file_name'];
							} else {
								$data['status'] = "error";
								$data['response'] = $upload['response'];

								echo json_encode($data);
								exit;
							}
						}
						
						$this->user->edit($id_uniq, $image_path);

						$data['status'] = "success";
						$data['id_uniq'] = $id_uniq;
						$data['has_image'] = ($image_path != NULL) ? 1 : 0;
						$data['response'] = "User updated successfully";
					}
				}
			}
		}

		$this->response($data);
	}

	public function upload_profile_picture()
	{
		$config['upload_path'] = "media/users/";
		$config['allowed_types'] = "jpg|jpeg|png|gif";
		$config['max_size'] = "2048";
		$config['file_ext_tolower'] = TRUE;
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload())
		{
			$upload['status'] = "error";
			$upload['response'] = "Extensions (gif, jpg, jpeg, png) files under 2MB"; //$this->upload->display_errors('', '<br>');
		}
		else
		{
			$upload['status'] = "success";
			$upload['response'] = $this->upload->data();
		}

		return $upload;
	}

	public function uniqids_get()
	{
		$result = $this->user->getUniqIDs();

		$data['response'] = $result;

		$this->response($data);
	}	

	public function logged_in_user_get()
	{
		$logged_in_user = $this->user->getLoggedInUser($this->userdata->id_uniq);

		$data['response'] = ($logged_in_user != NULL) ? $logged_in_user : "User";

		$this->response($data);
	}

	public function base_image_get()
	{
		if (!$this->query('id_uniq'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');
		$image = $this->user->getUserImage($id_uniq);

		if ($image != NULL)
		{
			$this->load->helper('file');

			$path = $image->tx_image;
			$type = get_mime_by_extension($path);
			$file = file_get_contents($path);
			$base64 = "data:".$type.";base64,".base64_encode($file);

			$data['is_image'] = 1;
			$data['response'] = $base64;
		}
		else
		{
			$data['is_image'] = 0;
			$data['response'] = "There is no image to show for the requested user.";
		}

		$this->response($data);
	}

	public function delete_put(){

		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status']   = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				$this->user->delete();
				$data['status']   = "success";
				$data['response'] = "User deleted successfully.";
			}
		} else {
			/*$this->kicking->updateForce();
			$data['status']   = "success";
			$data['response'] = "Kicking Force data updated successfully.";*/
		}

		$this->response($data);

	}

	public function multidelete_put(){
	
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status']   = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				$this->user->multiDelete();
				$data['status']   = "success";
				$data['response'] = "Users deleted successfully.";
			}
		} else {
			/*$this->kicking->updateForce();
			$data['status']   = "success";
			$data['response'] = "Kicking Force data updated successfully.";*/
		}
		$this->response($data);
	}
}