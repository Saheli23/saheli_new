<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Kicking extends REST_Controller
{
	private $userdata;

	public function __construct()
	{
		parent::__construct();

		$token = $this->input->get_request_header('token');

		try {
			
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));

		} catch (Exception $e) {
			
			$this->output->set_status_header(401);
			exit;

		}

		$this->load->model('user_model', 'user');
		$this->load->model('kicking_model', 'kicking');
	}

	public function _remap($method, $params = array())
	{
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		}

		$this->output->set_status_header(404);
		exit;
	}

	public function check_post()
	{
		if (!$this->query('id_player') || !$this->query('id_event'))
		{
			$this->ouput->set_status_header(400);
			exit;
		}

		$id_player = $this->query('id_player');
		$id_event = $this->query('id_event');
		$tests = $this->input->post('tests');
		$checks = $this->kicking->check($id_player, $id_event, $tests, 1);
		$data = array();

		if (($checks['aim'] == 0 && $checks['force'] == 0) ||
			($checks['aim'] == 0 && $checks['force'] == 1) || 
			($checks['aim'] == 1 && $checks['force'] == 0) || 
			($checks['aim'] == 1 && $checks['force'] == 1))
		{
			$data['player_exists'] = 1;
			$data['response'] = "This player has already been evaluated.";
		}
		else
		{
			$data['player_exists'] = 0;
			$data['player_not_allowed_to_perform'] = "This player can not perform this test because his age does not meet the requirements.";
			$data['response'] = $checks;
		}

		$this->response($data);
	}

	public function aim_post()
	{
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_add != 1)
		{
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		}
		else
		{
			$this->load->library('form_validation');

			$this->form_validation->set_rules('id_player', 'Player ID', 'required|integer|trim');
			$this->form_validation->set_rules('id_event', 'Event ID', 'required|integer|trim');
			$this->form_validation->set_rules('measurement_date', 'Measurement Date', 'required|trim');

			$this->form_validation->set_rules('right_leg[request_one]', 'Right Leg Request #1', 'required|numeric|trim');
			$this->form_validation->set_rules('right_leg[destination_one]', 'Right Leg Destination #1', 'required|numeric|trim');
			$this->form_validation->set_rules('right_leg[request_two]', 'Right Leg Request #2', 'numeric|trim');
			$this->form_validation->set_rules('right_leg[destination_two]', 'Right Leg Destination #2', 'numeric|trim');
			$this->form_validation->set_rules('right_leg[request_three]', 'Right Leg Request #3', 'numeric|trim');
			$this->form_validation->set_rules('right_leg[destination_three]', 'Right Leg Destination #3', 'numeric|trim');

			$this->form_validation->set_rules('left_leg[request_one]', 'Left Leg Request #1', 'required|numeric|trim');
			$this->form_validation->set_rules('left_leg[destination_one]', 'Left Leg Destination #1', 'required|numeric|trim');
			$this->form_validation->set_rules('left_leg[request_two]', 'Left Leg Request #2', 'numeric|trim');
			$this->form_validation->set_rules('left_leg[destination_two]', 'Left Leg Destination #2', 'numeric|trim');
			$this->form_validation->set_rules('left_leg[request_three]', 'Left Leg Request #3', 'numeric|trim');
			$this->form_validation->set_rules('left_leg[destination_three]', 'Left Leg Destination #3', 'numeric|trim');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE)
			{
				$data['status'] = "error";
				$data['response'] = validation_errors();
			}
			else
			{
				$player_id = $this->input->post('id_player');
				$event_id  = $this->input->post('id_event');
				//$tests 		 = $this->input->post('tests');
				
				// if( $this->kicking->check( $player_id, $event_id, $tests, "all" ) > 0 ){
				// 	$this->kicking->updateExistingAim( $player_id, $event_id );
				// }else{
				// 	$this->kicking->createAim();
				// }
        $checking  = $this->kicking->check_for_insert( $player_id, $event_id, "aim" );
        if($checking == 0){
          $this->kicking->updateExistingAim( $player_id, $event_id );
        }
        if($checking == "nf"){
          $this->kicking->createAim($player_id, $event_id);
        }

				$data['status'] = "success";
				$data['response'] = "Kicking data saved successfully.";
			}
		}

		$this->response($data);
	}

	public function force_post()
	{
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_add != 1)
		{
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		}
		else
		{
			$this->load->library('form_validation');

			$this->form_validation->set_rules('id_player', 'Player ID', 'required|integer|trim');
			$this->form_validation->set_rules('id_event', 'Event ID', 'required|integer|trim');
			$this->form_validation->set_rules('measurement_date', 'Measurement Date', 'required|trim');

			$this->form_validation->set_rules('right_leg[attempt_one]', 'Right Leg Attempt #1', 'required|numeric|trim');
			$this->form_validation->set_rules('right_leg[attempt_two]', 'Right Leg Attempt #2', 'required|numeric|trim');
			$this->form_validation->set_rules('right_leg[attempt_three]', 'Right Leg Attempt #3', 'required|numeric|trim');
			
			$this->form_validation->set_rules('left_leg[attempt_one]', 'Left Leg Attempt #1', 'required|numeric|trim');
			$this->form_validation->set_rules('left_leg[attempt_two]', 'Left Leg Attempt #2', 'required|numeric|trim');
			$this->form_validation->set_rules('left_leg[attempt_three]', 'Left Leg Attempt #3', 'required|numeric|trim');
			
			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE)
			{
				$data['status'] = "error";
				$data['response'] = validation_errors();
			}
			else
			{
				$player_id = $this->input->post('id_player');
				$event_id  = $this->input->post('id_event');
        // $tests     = $this->input->post('tests');

        // if( $this->kicking->check( $player_id, $event_id, $tests, "all" ) > 0 ){
        //   $this->kicking->updateExistingForce( $player_id, $event_id );
        // }else{
        //   $this->kicking->createForce($player_id, $event_id);
        // }
        $checking  = $this->kicking->check_for_insert( $player_id, $event_id, "force" );
				if($checking == 0){
					$this->kicking->updateExistingForce( $player_id, $event_id );
				}
        if($checking == "nf"){
				  $this->kicking->createForce($player_id, $event_id);
				}
				$data['status'] = "success";
				$data['response'] = "Kicking data saved successfully.";
			}
		}

		$this->response($data);
	}

	public function aim_put() {
    $data = array();

    $user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
    
    if( $this->input->input_stream('type') == "delete" ){
      if ($user_permissions->bo_delete != 1) {
        $data['status'] = "error";
        $data['response'] = "You do not have permission to perform this action";
      } else {
        $this->kicking->delete('force');
        $data['status']   = "success";
        $data['response'] = "Kicking data deleted successfully.";
      }
    } else {
    	$player_id = $this->input->input_stream('id_player');
    	$event_id = $this->input->input_stream('id_event');

      $checking = $this->kicking->check_for_insert($player_id, $event_id, "aim");

      if($checking == 1){
        $this->kicking->updateAim();
      }else if($checking == 0){
        $this->kicking->updateExistingAim($player_id, $event_id);
      }
      if($checking == "nf"){
        $this->kicking->createAimtwo();
      }
      $data['status']   = "success";
      $data['response'] = "Kicking Aim data updated successfully.";
    }

    $this->response($data);
  }

	public function force_put() {
    $data = array();

    $user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
    
    if( $this->input->input_stream('type') == "delete" ){
      if ($user_permissions->bo_delete != 1) {
        $data['status'] = "error";
        $data['response'] = "You do not have permission to perform this action";
      } else {
        $this->kicking->delete('force');
        $data['status']   = "success";
        $data['response'] = "Kicking data deleted successfully.";
      }
    } else {
    	$player_id = $this->input->input_stream('id_player');
    	$event_id = $this->input->input_stream('id_event');
      $checking = $this->kicking->check_for_insert($player_id, $event_id, "force");

      if($checking == 1){
        $this->kicking->updateForce();
      }else if($checking == 0){
        $this->kicking->updateExistingForce($player_id, $event_id);
      }
      if($checking == "nf"){
        $this->kicking->createForcetwo();
      }
      $data['status']   = "success";
      $data['response'] = "Kicking Aim data updated successfully.";
    }

    $this->response($data);
  }

	public function event_get()
	{
		if (!$this->query('id_uniq_event'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq_event = $this->query('id_uniq_event');

		$this->load->model('event_model', 'event');

		$id_event = $this->event->getID($id_uniq_event);

		if ($id_event == NULL)
		{
			$data['response'] = "There are no details to show.";
		}
		else
		{
			$kicking = $this->kicking->getAll($id_event);

			$data['response'] = ($kicking != NULL) ? $kicking : "There are no kicking measurements for this event.";
		}

		$this->response($data);
	}

	public function aim_get() {
    if (!$this->query('id_uniq_event')) {
      $this->output->set_status_header(400);
      exit;
    }

    $id_uniq_event = $this->query('id_uniq_event');

    $this->load->model('event_model', 'event');

    $id_event = $this->event->getID($id_uniq_event);


    $id_uniq_player = $this->query('id_uniq_player');

    $this->load->model('player_model', 'player');

    $id_player = $this->player->getID($id_uniq_player);

    if ($id_event == NULL || $id_player == NULL ) {
      $data['response'] = "There are no details to show.";
    } else {
      $kicking = $this->kicking->getDetails($id_event,$id_player);

      $data['response'] = ($kicking != NULL) ? $kicking : "There are no kicking measurements for this event.";
    }

    $this->response($data);
  }
  

	public function delete_put(){
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
		
		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status']   = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				$this->kicking->delete('aim');
				$this->kicking->delete('force');
				$data['status']   = "success";
				$data['response'] = "Kicking data deleted successfully.";
			}
		} else {
			/*$this->kicking->updateForce();
			$data['status']   = "success";
			$data['response'] = "Kicking Force data updated successfully.";*/
		}

		$this->response($data);

	}

	public function multidelete_put(){
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
		
		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status']   = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				$this->kicking->multidelete('aim');
				$this->kicking->multidelete('force');
				$data['status']   = "success";
				$data['response'] = "Kicking data deleted successfully.";
			}
		} else {
			/*$this->kicking->updateForce();
			$data['status']   = "success";
			$data['response'] = "Kicking Force data updated successfully.";*/
		}

		$this->response($data);

	}
}