<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Access extends REST_Controller{
	private $userdata;

	public function __construct() {
		parent::__construct();

		$token = $this->input->get_request_header('token');
		try {
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));
		} catch (Exception $e) {
			$this->output->set_status_header(401);
			exit;
		}
		$this->load->model('access_model', 'access');
	}

	public function _remap($method, $params = array()) {
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method)) {
			return call_user_func_array(array($this, $method), $params);
		}

		$this->output->set_status_header(404);
		exit;
	}

	public function access_get() {
		$user_id    = $this->userdata->id;
		$section_id = $this->_get_args['section_id'];
		$access     = $this->access->getAccess( $user_id, $section_id );
		$data['response'] = $access ? $access : "There is no access to show.";
		$this->response($data);
	}
}

?>