<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Heading extends REST_Controller{
	private $userdata;

	public function __construct(){
		parent::__construct();

		$token = $this->input->get_request_header('token');

		try {
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));
		} catch (Exception $e) {
			
			$this->output->set_status_header(401);
			exit;
		}

		$this->load->model('user_model', 'user');
		$this->load->model('heading_model', 'heading');
	}

	public function _remap($method, $params = array()){
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method)) {
			return call_user_func_array(array($this, $method), $params);
		}

		$this->output->set_status_header(404);
		exit;
	}

	public function check_post() {
		if (!$this->query('id_player') || !$this->query('id_event')) {
			$this->ouput->set_status_header(400);
			exit;
		}

		$id_player = $this->query('id_player');
		$id_event = $this->query('id_event');
		$tests = $this->input->post('tests');
		$checks = $this->heading->check($id_player, $id_event, $tests);
		$data = array();

		if (($checks['aim'] == 0 && $checks['force'] == 0 && $checks['jump'] == 0) ||
			($checks['aim'] == 0 && $checks['force'] == 0 && $checks['jump'] == 1) ||
			($checks['aim'] == 0 && $checks['force'] == 1 && $checks['jump'] == 0) ||
			($checks['aim'] == 0 && $checks['force'] == 1 && $checks['jump'] == 1) ||
			($checks['aim'] == 1 && $checks['force'] == 0 && $checks['jump'] == 0) ||
			($checks['aim'] == 1 && $checks['force'] == 0 && $checks['jump'] == 1) ||
			($checks['aim'] == 1 && $checks['force'] == 1 && $checks['jump'] == 0) ||
			($checks['aim'] == 1 && $checks['force'] == 1 && $checks['jump'] == 1)) {
			$data['player_exists'] = 1;
			$data['response'] = "This player has already been evaluated.";
			$data['checks'] = $checks;
		} else {
			$data['player_exists'] = 0;
			$data['player_not_allowed_to_perform'] = "This player can not perform this test because his age does not meet the requirements.";
			$data['response'] = $checks;
		}

		$this->response($data);
	}

	public function aim_post() {
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_add != 1) {
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		} else {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('id_player', 'Player ID', 'required|integer|trim');
			$this->form_validation->set_rules('id_event', 'Event ID', 'required|integer|trim');
			$this->form_validation->set_rules('measurement_date', 'Measurement Date', 'required|trim');
			$this->form_validation->set_rules('request_one', 'Request #1', 'required|numeric|trim');
			$this->form_validation->set_rules('destination_one', 'Destination #1', 'required|numeric|trim');
			$this->form_validation->set_rules('request_two', 'Request #2', 'numeric|trim');
			$this->form_validation->set_rules('destination_two', 'Destination #2', 'numeric|trim');
			$this->form_validation->set_rules('request_three', 'Request #3', 'numeric|trim');
			$this->form_validation->set_rules('destination_three', 'Destination #3', 'numeric|trim');
			$this->form_validation->set_rules('request_four', 'Request #4', 'numeric|trim');
			$this->form_validation->set_rules('destination_four', 'Destination #4', 'numeric|trim');
			$this->form_validation->set_rules('request_five', 'Request #5', 'numeric|trim');
			$this->form_validation->set_rules('destination_five', 'Destination #5', 'numeric|trim');
			$this->form_validation->set_rules('request_six', 'Request #6', 'numeric|trim');
			$this->form_validation->set_rules('destination_six', 'Destination #6', 'numeric|trim');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE) {
				$data['status'] = "error";
				$data['response'] = validation_errors();
			} else {
				$this->heading->createAim();

				$data['status'] = "success";
				$data['response'] = "Heading data saved successfully.";
			}
		}

		$this->response($data);
	}

	/*public function aim_put() {
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
		
		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status']   = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				$this->heading->delete('aim');
				$data['status']   = "success";
				$data['response'] = "Heading Aim data deleted successfully.";
			}
		} else {
			$this->heading->updateForce();
			$data['status']   = "success";
			$data['response'] = "Heading Force data updated successfully.";
		}

		$this->response($data);
	}*/

	public function force_post() {
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_add != 1) {
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		} else {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('id_player', 'Player ID', 'required|integer|trim');
			$this->form_validation->set_rules('id_event', 'Event ID', 'required|integer|trim');
			$this->form_validation->set_rules('measurement_date', 'Measurement Date', 'required|trim');
			$this->form_validation->set_rules('distance_one', 'Distance #1', 'required|numeric|trim');
			$this->form_validation->set_rules('distance_two', 'Distance #2', 'required|numeric|trim');
			$this->form_validation->set_rules('distance_three', 'Distance #2', 'required|numeric|trim');
			$this->form_validation->set_rules('attempt_one', 'Attempt #1', 'required|numeric|trim');
			$this->form_validation->set_rules('attempt_two', 'Attempt #2', 'required|numeric|trim');
			$this->form_validation->set_rules('attempt_three', 'Attempt #2', 'required|numeric|trim');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE) {
				$data['status'] = "error";
				$data['response'] = validation_errors();
			} else {
				$this->heading->createForce();

				$data['status'] = "success";
				$data['response'] = "Heading data saved successfully.";
			}
		}

		$this->response($data);
	}

	public function force_put() {
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
		
		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status'] = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				$this->heading->delete('force');
				$data['status']   = "success";
				$data['response'] = "Heading Force data deleted successfully.";
			}
		} else {
			$this->heading->updateForce();
			$data['status']   = "success";
			$data['response'] = "Heading Force data updated successfully.";
		}

		$this->response($data);
	}


	public function force_get() {
		if (!$this->query('id_uniq_event')) {
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq_event = $this->query('id_uniq_event');

		$this->load->model('event_model', 'event');

		$id_event = $this->event->getID($id_uniq_event);


		$id_uniq_player = $this->query('id_uniq_player');

		$this->load->model('player_model', 'player');

		$id_player = $this->player->getID($id_uniq_player);

		if ($id_event == NULL || $id_player == NULL ) {
			$data['response'] = "There are no details to show.";
		} else {
			$heading = $this->heading->getDetails($id_event,$id_player);

			$data['response'] = ($heading != NULL) ? $heading : "There are no heading measurements for this event.";
		}

		$this->response($data);
	}

	public function jump_post() {
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_add != 1) {
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		} else {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('id_player', 'Player ID', 'required|integer|trim');
			$this->form_validation->set_rules('id_event', 'Event ID', 'required|integer|trim');
			$this->form_validation->set_rules('measurement_date', 'Measurement Date', 'required|trim');
			$this->form_validation->set_rules('jump_high_one', 'High Jump #1', 'required|numeric|trim');
			$this->form_validation->set_rules('jump_high_two', 'High Jump #2', 'required|numeric|trim');
			$this->form_validation->set_rules('jump_high_three', 'High Jump #3', 'required|numeric|trim');
			
			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE) {
				$data['status'] = "error";
				$data['response'] = validation_errors();
			} else {
				$this->heading->createJump();

				$data['status'] = "success";
				$data['response'] = "Heading data saved successfully.";
			}
		}

		$this->response($data);
	}

	/*public function jump_put() {
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
		
		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status']   = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				$this->heading->delete('jump');
				$data['status']   = "success";
				$data['response'] = "Heading Jump data deleted successfully.";
			}
		} else {
			$this->heading->updateForce();
			$data['status']   = "success";
			$data['response'] = "Heading Force data updated successfully.";
		}

		$this->response($data);
	}*/

	public function delete_put(){
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
		
		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status']   = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				$this->heading->delete('aim');
				$this->heading->delete('force');
				$this->heading->delete('jump');
				$data['status']   = "success";
				$data['response'] = "Heading data deleted successfully.";
			}
		} else {
			/*$this->heading->updateForce();
			$data['status']   = "success";
			$data['response'] = "Heading Force data updated successfully.";*/
		}

		$this->response($data);

	}

	public function event_get() {
		if (!$this->query('id_uniq_event')) {
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq_event = $this->query('id_uniq_event');

		$this->load->model('event_model', 'event');

		$id_event = $this->event->getID($id_uniq_event);

		if ($id_event == NULL) {
			$data['response'] = "There are no details to show.";
		} else {
			$heading = $this->heading->getAll($id_event);

			$data['response'] = ($heading != NULL) ? $heading : "There are no heading measurements for this event.";
		}

		$this->response($data);
	}
}