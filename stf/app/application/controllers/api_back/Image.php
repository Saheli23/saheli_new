<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Image extends REST_Controller
{
	private $userdata;

	public function __construct()
	{
		parent::__construct();

		$token = $this->input->get_request_header('token');

		try {
			
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));

		} catch (Exception $e) {
			
			$this->output->set_status_header(401);
			exit;

		}

		$this->load->model('user_model', 'user');
	}

	public function _remap($method, $params = array())
	{
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		}

		$this->output->set_status_header(404);
		exit;
	}			

	public function set_post()
	{
		$data = array();

		$user_persmissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_persmissions->bo_add != 1)
		{
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		}
		else
		{
			$this->load->library('form_validation');

			$this->form_validation->set_rules('owner', 'Owner', 'required|trim');
			$this->form_validation->set_rules('id_uniq', 'Onwer ID', 'required|trim');
			$this->form_validation->set_rules('xaxis', 'X Axis', 'required|numeric|trim');
			$this->form_validation->set_rules('yaxis', 'y Axis', 'required|numeric|trim');
			$this->form_validation->set_rules('width', 'Width', 'required|numeric|trim');
			$this->form_validation->set_rules('height', 'Height', 'required|numeric|trim');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE)
			{
				$data['status'] = "error";
				$data['response'] = validation_errors();
			}
			else
			{
				$owner = $this->input->post('owner');
				$id_uniq = $this->input->post('id_uniq');
								
				switch ($owner) {
					case 'tea_':
						
						$this->load->model('team_model', 'team');
						$image = $this->team->getTeamImage($id_uniq);

						break;
					case 'use_':
						
						$this->load->model('user_model', 'user');
						$image = $this->user->getUserImage($id_uniq);

						break;
					default:
						break;
				}

				$picture_full_path = $image->tx_image;

				$crop_config['image_library'] = 'gd2';
				$crop_config['source_image'] = $picture_full_path;
				$crop_config['maintain_ratio'] = FALSE;
				$crop_config['width'] = $this->input->post('width');
				$crop_config['height'] = $this->input->post('height');
				$crop_config['x_axis'] = $this->input->post('xaxis');
				$crop_config['y_axis'] = $this->input->post('yaxis');

				$this->load->library('image_lib');

				$this->image_lib->initialize($crop_config);
				$this->image_lib->crop();

				$resize_config['image_library'] = 'gd2';
				$resize_config['source_image'] = $picture_full_path;
				$resize_config['maintain_ratio'] = TRUE;
				$resize_config['width'] = 320;
				$resize_config['height'] = 320;
				
				$this->image_lib->initialize($resize_config);
				$this->image_lib->resize();

				$data['status'] = "success";
			}
		}

		$this->response($data);
	}

	public function player_set_post()
	{
		$this->load->model('player_model', 'player');

		$data = array();

		$user_persmissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_persmissions->bo_add != 1)
		{
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		}
		else
		{
			$this->load->library('form_validation');

			$this->form_validation->set_rules('head[id]', 'Head Image ID', 'required|numeric|trim');
			$this->form_validation->set_rules('head[xaxis]', 'Head Image X Axis', 'required|numeric|trim');
			$this->form_validation->set_rules('head[yaxis]', 'Head Image y Axis', 'required|numeric|trim');
			$this->form_validation->set_rules('head[width]', 'Head Image Width', 'required|numeric|trim');
			$this->form_validation->set_rules('head[height]', 'Head Image Height', 'required|numeric|trim');

			$this->form_validation->set_rules('body[id]', 'Body Image ID', 'required|numeric|trim');
			$this->form_validation->set_rules('body[xaxis]', 'Body Image X Axis', 'required|numeric|trim');
			$this->form_validation->set_rules('body[yaxis]', 'Body Image y Axis', 'required|numeric|trim');
			$this->form_validation->set_rules('body[width]', 'Body Image Width', 'required|numeric|trim');
			$this->form_validation->set_rules('body[height]', 'Body Image Height', 'required|numeric|trim');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE)
			{
				$data['status'] = "error";
				$data['response'] = validation_errors();
			}
			else
			{
				$this->load->library('image_lib');

				if ($this->input->post('head[id]') != 0) { 

					$id = $this->input->post('head[id]');
					$head_image_path = $this->player->getPlayerSpecificImage($id);

					$crop_config['image_library'] = 'gd2';
					$crop_config['source_image'] = $head_image_path;
					$crop_config['maintain_ratio'] = FALSE;
					$crop_config['width'] = $this->input->post('head[width]');
					$crop_config['height'] = $this->input->post('head[height]');
					$crop_config['x_axis'] = $this->input->post('head[xaxis]');
					$crop_config['y_axis'] = $this->input->post('head[yaxis]');

					$this->image_lib->initialize($crop_config);
					$this->image_lib->crop();

					$resize_config['image_library'] = 'gd2';
					$resize_config['source_image'] = $head_image_path;
					$resize_config['maintain_ratio'] = TRUE;
					$resize_config['width'] = 320;
					$resize_config['height'] = 320;
					
					$this->image_lib->initialize($resize_config);
					$this->image_lib->resize();
					$this->image_lib->clear();
				}

				if ($this->input->post('body[id]') != 0) { 

					$id = $this->input->post('body[id]');
					$body_image_path = $this->player->getPlayerSpecificImage($id);

					$crop_config['image_library'] = 'gd2';
					$crop_config['source_image'] = $body_image_path;
					$crop_config['maintain_ratio'] = FALSE;
					$crop_config['width'] = $this->input->post('body[width]');
					$crop_config['height'] = $this->input->post('body[height]');
					$crop_config['x_axis'] = $this->input->post('body[xaxis]');
					$crop_config['y_axis'] = $this->input->post('body[yaxis]');

					$this->image_lib->initialize($crop_config);
					$this->image_lib->crop();

					$resize_config['image_library'] = 'gd2';
					$resize_config['source_image'] = $body_image_path;
					$resize_config['maintain_ratio'] = TRUE;
					$resize_config['width'] = 220;
					$resize_config['height'] = 320;
					
					$this->image_lib->initialize($resize_config);
					$this->image_lib->resize();
					$this->image_lib->clear();
				}

				$data['status'] = "success";
			}
		}

		$this->response($data);
	}
}