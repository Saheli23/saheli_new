<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Section extends REST_Controller{
	private $userdata;

	public function __construct() {
		parent::__construct();
		$token = $this->input->get_request_header('token');
		try {
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));
		} catch (Exception $e) {
			$this->output->set_status_header(401);
			exit;
		}
		$this->load->model('section_model', 'section');
	}

	public function _remap($method, $params = array()) {
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method)) {
			return call_user_func_array(array($this, $method), $params);
		}
		$this->output->set_status_header(404);
		exit;
	}

	public function section_get(){
		$sections         = $this->section->getSection();
		$data['response'] = $sections ? $sections : "There is no Sections to show.";
		$this->response($data);
	}
}