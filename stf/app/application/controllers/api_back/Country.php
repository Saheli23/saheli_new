<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Country extends REST_Controller
{
	private $userdata;

	public function __construct()
	{
		parent::__construct();

		$token = $this->input->get_request_header('token');

		try {
			
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));

		} catch (Exception $e) {
			
			$this->output->set_status_header(401);
			exit;

		}

		$this->load->model('country_model', 'country');
	}

	public function _remap($method, $params = array())
	{
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		}

		$this->output->set_status_header(404);
		exit;
	}

	public function index_get()
	{
		$countries = $this->country->getAll();

		$data['response'] = ($countries != NULL) ? $countries : "There are no couuntries to show.";

		$this->response($data);
	}

	public function states_get()
	{
		if (!$this->query('id_country'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_country = $this->query('id_country');

		$states = $this->country->getCountryStates($id_country);

		$data['response'] = ($states != NULL) ? $states : "There are no states to show";

		$this->response($data);
	}
}