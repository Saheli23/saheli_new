<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Auth extends REST_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('auth_model', 'auth');
	}

	public function _remap($method, $params = array()) {
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method)) {
			return call_user_func_array(array($this, $method), $params);
		}

		$this->output->set_status_header(404);
		exit;
	}

	public function index_post() {
		$data = array();

		$this->load->library('form_validation');

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[150]|trim|strtolower');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');

		$this->form_validation->set_error_delimiters('', '<br>');

		if ($this->form_validation->run() == FALSE) {
			$data['status'] = "error";
			$data['response'] = validation_errors();
		} else {
			$credentials = $this->auth->getCredentials();

			if ($credentials == NULL) {
				$data['status'] = "error";
				$data['response'] = "Incorrect email";
			} else {
				if ($credentials->bo_status != 1) {
					$data['status'] = "error";
					$data['response'] = "Your account isn't activated";
				} else {
					$received_password = hash_hmac("sha256", $this->input->post('password'), $this->config->item("password_key"));

					if (hash_equals($received_password, $credentials->tx_password) == FALSE) {
						$data['status'] = "error";
						$data['response'] = "Incorrect password";
					} else {
						$user_data = array(
							'id'      => $credentials->id,
							'id_uniq' => $credentials->id_uniq,
							'email'   => $credentials->tx_email
						);

						$token = JWT::encode($user_data, $this->config->item("encryption_key"));

						$data['status'] = "success";
						$data['response'] = $token;
					}
				}
			}
		}

		$this->response($data);
	}
}