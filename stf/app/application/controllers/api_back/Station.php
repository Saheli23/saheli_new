<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Station extends REST_Controller
{	
	private $userdata;

	public function __construct()
	{
		parent::__construct();

		$token = $this->input->get_request_header('token');

		try {
			
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));

		} catch (Exception $e) {
			
			$this->output->set_status_header(401);
			exit;

		}

		$this->load->model('user_model', 'user');
		$this->load->model('station_model', 'station');
	}

	public function _remap($method, $params = array())
	{
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		}

		$this->output->set_status_header(404);
		exit;
	}

	public function able_get()
	{
		$stations = $this->station->getStationsAbleToConfigure();

		$data['response'] = ($stations != NULL) ? $stations : "There are no details no show.";

		$this->response($data);
	}

	public function configuration_post()
	{
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_add != 1)
		{
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		}
		else 
		{
			$this->load->library('form_validation');

			$this->form_validation->set_rules('station', 'Station', 'required|integer|trim');
			$this->form_validation->set_rules('test', 'Test', 'required|integer|trim');
			$this->form_validation->set_rules('age_range', 'Age Range', 'required|integer|trim');
			$this->form_validation->set_rules('distance', 'Distance', 'required|integer|trim');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE)
			{
				$data['status'] = "error";
				$data['response'] = validation_errors();
			}
			else
			{
				$config = array(
					'id_station' => $this->input->post('station'),
					'id_test' => $this->input->post('test'),
					'id_age_range' => $this->input->post('age_range')
				);

				if ($this->station->checkConfiguration($config) == TRUE)
				{
					$data['status'] = "error";
					$data['response'] = "Configuration already exists.";
				}
				else
				{
					$this->station->createConfiguration();
					$data['status'] = "success";
				}
			}
		}

		$this->response($data);
	}

	public function configuration_put()
	{
		if (!$this->query('id'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_edit != 1)
		{
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		}
		else 
		{
			$this->load->library('form_validation');

			$this->form_validation->set_data($this->input->input_stream());

			$this->form_validation->set_rules('station', 'Station', 'required|integer|trim');
			$this->form_validation->set_rules('test', 'Test', 'required|integer|trim');
			$this->form_validation->set_rules('age_range', 'Age Range', 'required|integer|trim');
			$this->form_validation->set_rules('distance', 'Distance', 'required|integer|trim');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE)
			{
				$data['status'] = "error";
				$data['response'] = validation_errors();
			}
			else
			{
				$id = $this->query('id');
				$config = array(
					'id_station' => $this->input->input_stream('station'),
					'id_test' => $this->input->input_stream('test'),
					'id_age_range' => $this->input->input_stream('age_range')
				);

				if ($this->station->checkConfigurationForEdit($id, $config) == TRUE)
				{
					$data['status'] = "error";
					$data['response'] = "Configuration already exists.";
				}
				else
				{
					$this->station->editConfiguration($id);
					$data['status'] = "success";
				}
			}
		}

		$this->response($data);
	}

	public function configuration_get()
	{
		if (!$this->query('id'))
		{
			$configurations = $this->station->getConfigurations();

			$data['response'] = ($configurations != NULL) ? $configurations : "There are no configurations to show.";
		}
		else
		{	
			$id = $this->query('id');

			$configuration = $this->station->getConfiguration($id);

			$data['response'] = ($configuration != NULL) ? $configuration : "There are no details to show.";
		}

		$this->response($data);
	}

	public function tests_get()
	{
		if (!$this->query('id_station'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_station = $this->query('id_station');
		$tests = $this->station->getTests($id_station);

		$data['response'] = ($tests != NULL) ? $tests : "There are no tests to show.";

		$this->response($data);
	}
}