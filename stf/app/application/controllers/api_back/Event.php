<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Event extends REST_Controller{
	private $userdata;

	public function __construct() {
		parent::__construct();
		$token = $this->input->get_request_header('token');

		try {
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));
		} catch (Exception $e) {
			$this->output->set_status_header(401);
			exit;
		}

		$this->load->model('user_model', 'user');
		$this->load->model('event_model', 'event');
	}

	public function index_get(){
		if (!$this->query('id_uniq')) {
			$events = $this->event->getAll();

			$data['response'] = ($events != NULL) ? $events : "There are no events to show.";
		} else {
			$id_uniq = $this->query('id_uniq');
			$event = $this->event->getOne($id_uniq);
			$data['response'] = ($event != NULL) ? $event : "There are no details to show.";
		}
		$this->response($data);
	}

	public function index_post() {
		$data = array();
		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_add != 1) {
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		} else {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('name', 'Event Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
			$this->form_validation->set_rules('date', 'Date', 'trim');
			$this->form_validation->set_rules('venue', 'Venue', 'required|integer|trim');
			$this->form_validation->set_rules('team', 'Team', 'required|integer|trim');
			$this->form_validation->set_rules('measurement_system', 'Measurement System', 'required|integer|trim');
			$this->form_validation->set_rules('configurations[]', 'Configurations', 'required');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE) {
				$data['status'] = "error";
				$data['response'] = validation_errors();
			} else {
				$this->event->create();
				$data['status'] = "success";
			}
		}

		$this->response($data);
	}

	public function index_put() {
		if (!$this->query('id_uniq')) {
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_edit != 1) {
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		} else {
			$this->load->library('form_validation');
			$this->form_validation->set_data($this->input->input_stream());

			$this->form_validation->set_rules('name', 'Event Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
			$this->form_validation->set_rules('date', 'Date', 'trim');
			$this->form_validation->set_rules('venue', 'Venue', 'required|integer|trim');
			$this->form_validation->set_rules('team', 'Team', 'required|integer|trim');
			$this->form_validation->set_rules('measurement_system', 'Measurement System', 'required|integer|trim');
			$this->form_validation->set_rules('configurations[]', 'Configurations', 'required');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE) {
				$data['status'] = "error";
				$data['response'] = validation_errors();
			} else {
				$this->event->edit($id_uniq);
				$data['status'] = "success";
			}
		}

		$this->response($data);
	}	

	public function uniqids_get() {
		$result = $this->event->getUniqIDs();
		$data['response'] = $result;
		$this->response($data);
	}

	public function delete_put(){
		$data = array();
		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status']   = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				if( $this->event->check_child() == 0 ){
					$this->event->delete();
					$data['status']   = "success";
					$data['response'] = "Event deleted successfully.";
				} else {
					$data['status']   = "danger";
					$data['response'] = "Event cannot be deleted.To delete Event you have to delete all the data related to Event";
				}
				
			}
		} else {
			/*$this->kicking->updateForce();
			$data['status']   = "success";
			$data['response'] = "Kicking Force data updated successfully.";*/
		}

		$this->response($data);
	}
}