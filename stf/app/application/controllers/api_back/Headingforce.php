<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Headingforce extends REST_Controller{
	private $userdata;

	public function __construct(){
		parent::__construct();

		$token = $this->input->get_request_header('token');

		try {
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));
		} catch (Exception $e) {
			
			$this->output->set_status_header(401);
			exit;
		}

		$this->load->model('user_model', 'user');
		$this->load->model('headingforce_model', 'headingforce');
	}

	public function _remap($method, $params = array()){
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method)) {
			return call_user_func_array(array($this, $method), $params);
		}

		$this->output->set_status_header(404);
		exit;
	}

	public function check_post() {
		if (!$this->query('id_player') || !$this->query('id_event')) {
			$this->ouput->set_status_header(400);
			exit;
		}

		$id_player = $this->query('id_player');
		$id_event = $this->query('id_event');
		$tests = $this->input->post('tests');
		$checks = $this->headingforce->check($id_player, $id_event, $tests);
		$data = array();

		//print_r($checks);

		if ( $checks['force'] == 0 || $checks['force'] == 1  ) {
			$data['player_exists'] = 1;
			$data['response'] = "This player has already been evaluated.";
			$data['checks'] = $checks;
		} else {
			$data['player_exists'] = 0;
			$data['player_not_allowed_to_perform'] = "This player can not perform this test because his age does not meet the requirements.";
			$data['response'] = $checks;
		}

		$this->response($data);
	}

	public function force_post() {
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_add != 1) {
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		} else {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('id_player', 'Player ID', 'required|integer|trim');
			$this->form_validation->set_rules('id_event', 'Event ID', 'required|integer|trim');
			$this->form_validation->set_rules('measurement_date', 'Measurement Date', 'required|trim');
			$this->form_validation->set_rules('distance', 'Distance', 'required|numeric|trim');
			$this->form_validation->set_rules('attempt_one', 'Attempt #1', 'required|numeric|trim');
			$this->form_validation->set_rules('attempt_two', 'Attempt #2', 'required|numeric|trim');
			$this->form_validation->set_rules('attempt_three', 'Attempt #2', 'required|numeric|trim');
			
			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE) {
				$data['status'] = "error";
				$data['response'] = validation_errors();
			} else {
				$this->headingforce->createForce();

				$data['status'] = "success";
				$data['response'] = "Heading data saved successfully.";
			}
		}

		$this->response($data);
	}

	public function force_put() {
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if( $this->input->post('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status'] = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				$this->headingforce->deleteForce();
				$data['status']   = "success";
				$data['response'] = "Heading Force data deleted successfully.";
			}
		} else {
			$this->headingforce->updateForce();
			$data['status']   = "success";
			$data['response'] = "Heading Force data updated successfully.";
		}

		$this->response($data);
	}

	public function event_get() {
		if (!$this->query('id_uniq_event')) {
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq_event = $this->query('id_uniq_event');

		$this->load->model('event_model', 'event');

		$id_event = $this->event->getID($id_uniq_event);

		if ($id_event == NULL) {
			$data['response'] = "There are no details to show.";
		} else {
			$heading = $this->headingforce->getAll($id_event);

			$data['response'] = ($heading != NULL) ? $heading : "There are no heading force measurements for this event.";
		}

		$this->response($data);
	}
}