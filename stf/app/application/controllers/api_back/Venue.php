<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Venue extends REST_Controller{
	private $userdata;

	public function __construct() {
		parent::__construct();
		$token = $this->input->get_request_header('token');
		try {
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));
		} catch (Exception $e) {
			$this->output->set_status_header(401);
			exit;
		}

		$this->load->model('user_model', 'user');
		$this->load->model('venue_model', 'venue');
	}

	public function _remap($method, $params = array()) {
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method)) {
			return call_user_func_array(array($this, $method), $params);
		}

		$this->output->set_status_header(404);
		exit;
	}

	public function index_get() {
		if (!$this->query('id_uniq')) {
			$venues = $this->venue->getAll();

			$data['response'] = ($venues != NULL) ? $venues : "There are no venues to show.";
		} else {
			$id_uniq = $this->query('id_uniq');

			$venue = $this->venue->getOne($id_uniq);

			$data['response'] = ($venue != NULL) ? $venue : "There are no details to show.";
		}

		$this->response($data);
	}

	public function index_post() {
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_add != 1) {
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		} else  {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('name', 'Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
			$this->form_validation->set_rules('country', 'Country', 'required|integer|trim');
			$this->form_validation->set_rules('state', 'State', 'required|integer|trim');
			$this->form_validation->set_rules('address', 'City', 'required|min_length[3]|max_length[200]|trim|mb_strtolower|ucwords');
			$this->form_validation->set_rules('address', 'Address', 'required|min_length[3]|max_length[200]|trim|mb_strtolower|ucwords');
			$this->form_validation->set_rules('zipcode', 'Zipcode', 'max_length[50]|trim');
			$this->form_validation->set_rules('phoneprefix_one', 'Phone Prefix #1', 'required|numeric|max_length[10]|trim');
			$this->form_validation->set_rules('phonenumber_one', 'Phone Number #1', 'required|numeric|max_length[50]|trim');
			$this->form_validation->set_rules('phoneprefix_two', 'Phone Prefix #2', 'numeric|max_length[10]|trim');
			$this->form_validation->set_rules('phonenumber_two', 'Phone Number #2', 'numeric|max_length[50]|trim');
			$this->form_validation->set_rules('email', 'Email', 'required|max_length[150]|valid_email|is_unique[stf_venue.tx_email]|trim|mb_strtolower');
			$this->form_validation->set_rules('website', 'Website', 'max_length[150]|valid_url|trim');
			$this->form_validation->set_rules('facebook', 'Facebook', 'max_length[150]|valid_url|trim');
			$this->form_validation->set_rules('instagram', 'Instagram', 'max_length[150]|valid_url|trim');
			$this->form_validation->set_rules('twitter', 'Twitter', 'max_length[150]|valid_url|trim');
			$this->form_validation->set_rules('contact_person', 'Contact Person', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
			$this->form_validation->set_rules('contact_person_phoneprefix', 'Contact Person Phone Prefix', 'required|numeric|max_length[10]|trim');
			$this->form_validation->set_rules('contact_person_phonenumber', 'Contact Person Phone Number', 'required|numeric|max_length[100]|trim');
			$this->form_validation->set_rules('latitude', 'Latitude', 'required|trim');
			$this->form_validation->set_rules('longitude', 'Longitude', 'required|trim');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE) {
				$data['status'] = "error";
				$data['response'] = validation_errors();
			} else {
				$this->venue->create();
				$data['status'] = "success";
			}
		}

		$this->response($data);
	}

	public function index_put() {
		if (!$this->query('id_uniq')) {
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');
		$data = array();
		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_edit != 1) {
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		} else {
			$this->load->library('form_validation');

			$this->form_validation->set_data($this->input->input_stream());

			$this->form_validation->set_rules('name', 'Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
			$this->form_validation->set_rules('country', 'Country', 'required|integer|trim');
			$this->form_validation->set_rules('state', 'State', 'required|integer|trim');
			$this->form_validation->set_rules('address', 'City', 'required|min_length[3]|max_length[200]|trim|mb_strtolower|ucwords');
			$this->form_validation->set_rules('address', 'Address', 'required|min_length[3]|max_length[200]|trim|mb_strtolower|ucwords');
			$this->form_validation->set_rules('zipcode', 'Zipcode', 'max_length[50]|trim');
			$this->form_validation->set_rules('phoneprefix_one', 'Phone Prefix #1', 'required|numeric|max_length[10]|trim');
			$this->form_validation->set_rules('phonenumber_one', 'Phone Number #1', 'required|numeric|max_length[50]|trim');
			$this->form_validation->set_rules('phoneprefix_two', 'Phone Prefix #2', 'numeric|max_length[10]|trim');
			$this->form_validation->set_rules('phonenumber_two', 'Phone Number #2', 'numeric|max_length[50]|trim');
			$this->form_validation->set_rules('email', 'Email', 'required|max_length[150]|valid_email|trim|mb_strtolower');
			$this->form_validation->set_rules('website', 'Website', 'max_length[150]|valid_url|trim');
			$this->form_validation->set_rules('facebook', 'Facebook', 'max_length[150]|valid_url|trim');
			$this->form_validation->set_rules('instagram', 'Instagram', 'max_length[150]|valid_url|trim');
			$this->form_validation->set_rules('twitter', 'Twitter', 'max_length[150]|valid_url|trim');
			$this->form_validation->set_rules('contact_person', 'Contact Person', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
			$this->form_validation->set_rules('contact_person_phoneprefix', 'Contact Person Phone Prefix', 'required|numeric|max_length[10]|trim');
			$this->form_validation->set_rules('contact_person_phonenumber', 'Contact Person Phone Number', 'required|numeric|max_length[100]|trim');
			$this->form_validation->set_rules('latitude', 'Latitude', 'required|trim');
			$this->form_validation->set_rules('longitude', 'Longitude', 'required|trim');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE) {
				$data['status'] = "error";
				$data['response'] = validation_errors();
			} else {
				if ($this->venue->checkEmailForEdit($id_uniq) == TRUE) {
					$data['status'] = "error";
					$data['response'] = "The Email field must contain a unique value.";
				} else {
					$this->venue->edit($id_uniq);

					$data['status'] = "success";
				}
			}
		}

		$this->response($data);
	}

	public function uniqids_get() {
		$result = $this->venue->getUniqIDs();
		$data['response'] = $result;
		$this->response($data);
	}

	public function delete_put(){
		$data             = array();
		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
		
		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status']   = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {
				if( $this->venue->check_child('event') == 0 ){
					$this->venue->delete();
					$data['status']   = "success";
					$data['response'] = "Venue deleted successfully.";
				} else {
					$data['status']   = "danger";
					$data['response'] = "Venue cannot be deleted.To delete Venue, you have to delete all the data related to Venue";
				}
			}
		} else {
			/*$this->kicking->updateForce();
			$data['status']   = "success";
			$data['response'] = "Kicking Force data updated successfully.";*/
		}
		$this->response($data);
	}	
}