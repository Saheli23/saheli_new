<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/REST_Controller.php';
require_once 'vendor/autoload.php';

use \Firebase\JWT\JWT;

/**
* 
*/
class Player extends REST_Controller
{
	private $userdata;

	public function __construct()
	{
		parent::__construct();

		$token = $this->input->get_request_header('token');

		try {
			
			$this->userdata = JWT::decode($token, $this->config->item('encryption_key'), array('HS256'));

		} catch (Exception $e) {
			
			$this->output->set_status_header(401);
			exit;

		}

		$this->load->model('user_model', 'user');
		$this->load->model('player_model', 'player');
	}

	public function _remap($method, $params = array())
	{
		$method = $method.'_'.$this->request->method;

		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		}

		$this->output->set_status_header(404);
		exit;
	}

	public function index_get()
	{
		if (!$this->query('id_uniq'))
		{
			$players = $this->player->getAll();

			$data['response'] = ($players != NULL) ? $players : "There are no players to show.";
		}
		else
		{
			$id_uniq = $this->query('id_uniq');

			$player = $this->player->getOne($id_uniq);

			$data['response'] = ($player != NULL) ? $player : "There are no details to show.";
		}

		$this->response($data);
	}

	public function index_post()
	{
		$data = array();

		if (!$this->query('id_uniq'))
		{
			$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

			if ($user_permissions->bo_add != 1)
			{
				$data['status'] = "error";
				$data['response'] = "You do not have permission to perform this action";
			}
			else
			{
				$this->load->library('form_validation');

				$this->form_validation->set_rules('firstname', 'First Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('secondname', 'Second Name', 'min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('first_lastname', 'First Last Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('second_lastname', 'Second Last Name', 'min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('email', 'email', 'required|valid_email|max_length[150]|is_unique[stf_player.tx_email]|trim|mb_strtolower');
				$this->form_validation->set_rules('gender', 'Gender', 'required|integer|trim');
				$this->form_validation->set_rules('birthdate', 'Birth Date', 'required|trim');
				$this->form_validation->set_rules('country', 'Country', 'required|integer|trim');
				$this->form_validation->set_rules('state', 'State', 'required|integer|trim');
				$this->form_validation->set_rules('address', 'Address', 'required|min_length[3]|max_length[200]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('zipcode', 'Zipcode', 'max_length[50]|trim');
				$this->form_validation->set_rules('phoneprefix', 'Phone Prefix', 'max_length[10]|numeric|trim');
				$this->form_validation->set_rules('phonenumber', 'Phone Number', 'max_length[50]|numeric|trim');
				
				$this->form_validation->set_error_delimiters('', '<br>');

				if ($this->form_validation->run() == FALSE)
				{
					$data['status'] = "error";
					$data['response'] = validation_errors();
				}
				else
				{
					$images = array();

					$data['has_head_file'] = 0;
					$data['has_body_file'] = 0;

					if ($_FILES)
					{
						$uploads = $this->upload_player_images();

						$data['has_head_file'] = $uploads['has_head_file'];
						$data['has_body_file'] = $uploads['has_body_file'];

						if ($uploads['status'] == "success")
						{
							foreach ($uploads['response'] as $key => $upload)
							{
								array_push($images, array('path' => "media/players/".$upload['file']['file_name'], 'type' => $upload['type']));
							}
						}
						else
						{
							$data['status'] = "error";
							$data['response'] = $upload['response'];

							echo json_encode($data);
							exit;
						}
					}

					$id_uniq = $this->player->create($images);

					$data['status'] = "success";
					$data['id_uniq'] = $id_uniq;
					$data['has_images'] = (!empty($images)) ? 1 : 0;
					$data['response'] = "Player created successfully";
				}
			}
		}
		else
		{
			$id_uniq = $this->query('id_uniq');
			$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

			if ($user_permissions->bo_edit != 1)
			{
				$data['status'] = "error";
				$data['response'] = "You do not have permission to perform this action";
			}
			else
			{
				$this->load->library('form_validation');

				$this->form_validation->set_rules('firstname', 'First Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('secondname', 'Second Name', 'min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('first_lastname', 'First Last Name', 'required|min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('second_lastname', 'Second Last Name', 'min_length[3]|max_length[100]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('email', 'email', 'required|valid_email|max_length[150]|trim|mb_strtolower');
				$this->form_validation->set_rules('gender', 'Gender', 'required|integer|trim');
				$this->form_validation->set_rules('birthdate', 'Birth Date', 'required|trim');
				$this->form_validation->set_rules('country', 'Country', 'required|integer|trim');
				$this->form_validation->set_rules('state', 'State', 'required|integer|trim');
				$this->form_validation->set_rules('address', 'Address', 'required|min_length[3]|max_length[200]|trim|mb_strtolower|ucwords');
				$this->form_validation->set_rules('zipcode', 'Zipcode', 'max_length[50]|trim');
				$this->form_validation->set_rules('phoneprefix', 'Phone Prefix', 'max_length[10]|numeric|trim');
				$this->form_validation->set_rules('phonenumber', 'Phone Number', 'max_length[50]|numeric|trim');
				
				$this->form_validation->set_error_delimiters('', '<br>');

				if ($this->form_validation->run() == FALSE)
				{
					$data['status'] = "error";
					$data['response'] = validation_errors();
				}
				else
				{
					if ($this->player->checkEmailForEdit($id_uniq) == TRUE)
					{
						$data['status'] = "error";
						$data['response'] = "The Email field must contain a unique value.";
					}
					else
					{
						$images = array();

						$data['has_head_file'] = 0;
						$data['has_body_file'] = 0;

						if ($_FILES)
						{
							$uploads = $this->upload_player_images();

							$data['has_head_file'] = $uploads['has_head_file'];
							$data['has_body_file'] = $uploads['has_body_file'];

							if ($uploads['status'] == "success")
							{
								foreach ($uploads['response'] as $key => $upload)
								{
									array_push($images, array('path' => "media/players/".$upload['file']['file_name'], 'type' => $upload['type']));
								}
							}
							else
							{
								$data['status'] = "error";
								$data['response'] = $upload['response'];

								echo json_encode($data);
								exit;
							}
						}

						$this->player->edit($id_uniq, $images);

						$data['status'] = "success";
						$data['id_uniq'] = $id_uniq;
						$data['has_images'] = (!empty($images)) ? 1 : 0;
						$data['response'] = "Player updated successfully";
					}
				}
			}
		}

		$this->response($data);
	}

	public function upload_player_images()
	{
		$files = $_FILES;
		$total_files = count($files);
		$files_uploaded = array();
		$counter = 0;

		$this->load->library('upload');

		$upload['has_head_file'] = 0;
		$upload['has_body_file'] = 0;

		foreach ($files as $key => $file)
		{
			switch ($key) {
				case 'head_file':
					$upload['has_head_file'] = 1; $type = 1;
					break;
				case 'body_file':
					$upload['has_body_file'] = 1; $type = 2;
					break;
			}

			$config['upload_path'] = 'media/players/';
			$config['allowed_types'] = 'png|jpg|jpeg';
			$config['max_size'] = 2048;
			$config['encrypt_name'] = TRUE;
			$config['file_ext_tolower'] = TRUE;

			$this->upload->initialize($config);

			$_FILES['userfile']['name'] = $file['name'];
			$_FILES['userfile']['type'] = $file['type'];
			$_FILES['userfile']['tmp_name'] = $file['tmp_name'];
			$_FILES['userfile']['error'] = $file['error'];
			$_FILES['userfile']['size'] = $file['size'];

			if (!$this->upload->do_upload())
			{
				$upload['status'] = "error";
				$upload['response'] = $this->upload->display_errors('', '<br>');
				break;
			}
			else
			{
				array_push($files_uploaded, array('file' => $this->upload->data(), 'type' => $type));
				$counter++;
			}
		}

		if ($counter == $total_files)
		{
			$upload['status'] = "success";
			$upload['response'] = $files_uploaded;
		}

		return $upload;
	}

	public function uniqids_get()
	{
		$result = $this->player->getUniqIDs();

		$data['response'] = $result;

		$this->response($data);
	}

	public function teams_get()
	{
		if (!$this->query('id_uniq'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');
		$teams = $this->player->getPlayerTeams($id_uniq);

		$data['response'] = ($teams != NULL) ? $teams : "There are no details to show.";

		$this->response($data);
	}

	public function current_team_get()
	{
		if (!$this->query('id_uniq'))
		{
			$this->output->set_status->header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');
		$data['response'] = $this->player->getCurrentTeam($id_uniq);

		$this->response($data);
	}

	public function team_post()
	{
		if (!$this->query('id_uniq'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');

		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_add != 1)
		{
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		}
		else 
		{
			$current_team_dates = FALSE;
		
			$this->load->library('form_validation');

			$this->form_validation->set_rules('team', 'Team', 'required|integer|trim');
			$this->form_validation->set_rules('admission_date', 'Amission Date', 'required|trim');
			$this->form_validation->set_rules('discharge_date', 'Discharge Date', 'trim');
			$this->form_validation->set_rules('number', 'Number', 'required|integer|trim');
			$this->form_validation->set_rules('positions[]', 'Positions', 'required');

			if ($this->input->post('current_team') != NULL)
			{
				$this->form_validation->set_rules('current_team[admission_date]', 'Current Team Admission Date', 'required|trim');
				$this->form_validation->set_rules('current_team[discharge_date]', 'Current Team Discharge Date', 'required|trim');

				$current_team_dates = TRUE;
			}

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE)
			{
				$data['status'] = "error";
				$data['response'] = validation_errors();
			}
			else
			{
				$this->player->createPlayerTeam($id_uniq, $current_team_dates);

				$data['status'] = "success";
			}
		}

		$this->response($data);
	}

	public function team_get()
	{
		if (!$this->query('id_uniq') || !$this->query('id_team_player'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');
		$id_team_player = $this->query('id_team_player');
		$team = $this->player->getPlayerTeam($id_uniq, $id_team_player);

		$data['response'] = ($team != NULL) ? $team : "There are no details to show.";

		$this->response($data);
	}

	public function team_put()
	{
		if (!$this->query('id_uniq') || !$this->query('id_team_player'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');
		$id_team_player = $this->query('id_team_player');

		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);

		if ($user_permissions->bo_edit != 1)
		{
			$data['status'] = "error";
			$data['response'] = "You do not have permission to perform this action";
		}
		else 
		{
			$this->load->library('form_validation');

			$this->form_validation->set_data($this->input->input_stream());

			$this->form_validation->set_rules('team', 'Team', 'required|integer|trim');
			$this->form_validation->set_rules('admission_date', 'Amission Date', 'required|trim');
			$this->form_validation->set_rules('discharge_date', 'Discharge Date', 'trim');
			$this->form_validation->set_rules('number', 'Number', 'required|integer|trim');
			$this->form_validation->set_rules('positions[]', 'Positions', 'required');

			$this->form_validation->set_error_delimiters('', '<br>');

			if ($this->form_validation->run() == FALSE)
			{
				$data['status'] = "error";
				$data['response'] = validation_errors();
			}
			else
			{
				$this->player->editPlayerTeam($id_uniq, $id_team_player);

				$data['status'] = "success";
			}
		}

		$this->response($data);
	}

	public function base_images_get()
	{
		if (!$this->query('id_uniq'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');
		$images = $this->player->getPlayerImages($id_uniq);

		if ($images != NULL)
		{
			$base64 = array();

			$this->load->helper('file');

			foreach ($images as $key => $image)
			{
				$path = $image->route;
				$type = get_mime_by_extension($path);
				$file = file_get_contents($path);
				array_push($base64, array("image" => "data:".$type.";base64,".base64_encode($file), "id_image" => $image->id_image, "type" => $image->id_type_image));
			}

			$data['is_image'] = 1;
			$data['response'] = $base64;
		}
		else
		{
			$data['is_image'] = 0;
			$data['response'] = "There are no images to show for the requested player.";
		}

		$this->response($data);
	}

	public function base_head_image_get()
	{
		if (!$this->query('id_uniq'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');
		$image = $this->player->getPlayerHeadImage($id_uniq);

		if ($image != NULL)
		{
			$this->load->helper('file');

			$path = $image->route;
			$type = get_mime_by_extension($path);
			$file = file_get_contents($path);
			$base64 = array("image" => "data:".$type.";base64,".base64_encode($file), "id_image" => $image->id_image, "type" => $image->id_type_image);
			
			$data['is_image'] = 1;
			$data['response'] = $base64;
		}
		else
		{
			$data['is_image'] = 0;
			$data['response'] = "There are no images to show for the requested player.";
		}

		echo json_encode($data);
	}

	public function base_body_image_get()
	{
		if (!$this->query('id_uniq'))
		{
			$this->output->set_status_header(400);
			exit;
		}

		$id_uniq = $this->query('id_uniq');
		$image = $this->player->getPlayerBodyImage($id_uniq);

		if ($image != NULL)
		{
			$this->load->helper('file');

			$path = $image->route;
			$type = get_mime_by_extension($path);
			$file = file_get_contents($path);
			$base64 = array("image" => "data:".$type.";base64,".base64_encode($file), "id_image" => $image->id_image, "type" => $image->id_type_image);
			
			$data['is_image'] = 1;
			$data['response'] = $base64;
		}
		else
		{
			$data['is_image'] = 0;
			$data['response'] = "There are no images to show for the requested player.";
		}

		echo json_encode($data);
	}

	public function delete_put(){
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
		
		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status']   = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {

				$anthropometric     = $this->player->check_child('anthropometric');

				$dribbling_circular = $this->player->check_child('dribbling_circular');
				$dribbling_zigzag   = $this->player->check_child('dribbling_zigzag');

				$heading_aim        = $this->player->check_child('heading_aim');
				$heading_force      = $this->player->check_child('heading_force');
				$heading_jump       = $this->player->check_child('heading_jump');

				$kicking_aim        = $this->player->check_child('kicking_aim');
				$kicking_force      = $this->player->check_child('kicking_force');

				$movement_lateral   = $this->player->check_child('movement_lateral');
				$movement_linear    = $this->player->check_child('movement_linear');
				$movement_zigzag    = $this->player->check_child('movement_zigzag');

				if( $anthropometric == 0 && $dribbling_circular == 0 && $dribbling_zigzag == 0 &&
					$heading_aim == 0 && $heading_force == 0 && $heading_jump == 0 &&
					$kicking_aim == 0 && $kicking_force == 0 && $movement_lateral == 0 &&
					$movement_linear == 0 && $movement_zigzag == 0 ){
					$this->player->delete();
					$data['status']   = "success";
					$data['response'] = "Player deleted successfully.";
				} else {
					$data['status']   = "danger";
					$data['response'] = "Player cannot be deleted.To delete Player you have to delete all the data related to Player";
				}
			}
		} else {
			/*$this->kicking->updateForce();
			$data['status']   = "success";
			$data['response'] = "Kicking Force data updated successfully.";*/
		}

		$this->response($data);

	}

	public function deleteteam_put(){
		$data = array();

		$user_permissions = $this->user->getUserPermissions($this->userdata->id_uniq);
		
		if( $this->input->input_stream('type') == "delete" ){
			if ($user_permissions->bo_delete != 1) {
				$data['status']   = "error";
				$data['response'] = "You do not have permission to perform this action";
			} else {

				$anthropometric     = $this->player->check_child('anthropometric');

				$dribbling_circular = $this->player->check_child('dribbling_circular');
				$dribbling_zigzag   = $this->player->check_child('dribbling_zigzag');

				$heading_aim        = $this->player->check_child('heading_aim');
				$heading_force      = $this->player->check_child('heading_force');
				$heading_jump       = $this->player->check_child('heading_jump');

				$kicking_aim        = $this->player->check_child('kicking_aim');
				$kicking_force      = $this->player->check_child('kicking_force');

				$movement_lateral   = $this->player->check_child('movement_lateral');
				$movement_linear    = $this->player->check_child('movement_linear');
				$movement_zigzag    = $this->player->check_child('movement_zigzag');

				if( $anthropometric == 0 && $dribbling_circular == 0 && $dribbling_zigzag == 0 &&
					$heading_aim == 0 && $heading_force == 0 && $heading_jump == 0 &&
					$kicking_aim == 0 && $kicking_force == 0 && $movement_lateral == 0 &&
					$movement_linear == 0 && $movement_zigzag == 0 ){
					$this->player->deleteTeam();
					$data['status']   = "success";
					$data['response'] = "Team deleted successfully.";
				} else {
					$data['status']   = "danger";
					$data['response'] = "team cannot be deleted.To delete Player you have to delete all the data related to Player";
				}
			}
		} else {
			/*$this->kicking->updateForce();
			$data['status']   = "success";
			$data['response'] = "Kicking Force data updated successfully.";*/
		}

		$this->response($data);

	}

}