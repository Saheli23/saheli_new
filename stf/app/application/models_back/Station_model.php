<?php

/**
* 
*/
class Station_model extends CI_Model
{
	public function getStationsAbleToConfigure()
	{
		$query = $this->db->select('id, id_uniq, tx_name')->where_not_in('id', 1)->get('stf_station');

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}

	public function checkConfiguration($config)
	{
		$query = $this->db->limit(1)->where($config)->get('stf_station_configuration');

		return ($query->num_rows() == 1) ? TRUE : FALSE;
	}

	public function checkConfigurationForEdit($id, $config)
	{
		$query = $this->db->limit(1)->where_not_in('id', $id)->where($config)->get('stf_station_configuration');

		return ($query->num_rows() == 1) ? TRUE : FALSE;
	}

	public function createConfiguration()
	{
		$data = array(
			'id_station' => $this->input->post('station'),
			'id_test' => $this->input->post('test'),
			'id_age_range' => $this->input->post('age_range'),
			'nu_distance' => $this->input->post('distance')
		);

		$this->db->insert('stf_station_configuration', $data);
	}

	public function editConfiguration($id)
	{
		$data = array(
			'id_station' => $this->input->input_stream('station'),
			'id_test' => $this->input->input_stream('test'),
			'id_age_range' => $this->input->input_stream('age_range'),
			'nu_distance' => $this->input->input_stream('distance')
		);

		$this->db->limit(1)->where('id', $id)->update('stf_station_configuration', $data);
	}

	public function getConfiguration($id)
	{
		$query = $this->db->limit(1)->get_where('stf_station_configuration', array('id' => $id));

		return ($query->num_rows() == 1) ? $query->row() : NULL;
	}

	public function getConfigurations()
	{
		$query = $this->db->select('stf_station_configuration.id, stf_station_configuration.id_station, stf_station_configuration.id_test, stf_station_configuration.id_age_range, stf_station_configuration.nu_distance, stf_station.tx_name AS station_name, stf_test.tx_name AS test_name, stf_age_range.nu_min_age, stf_age_range.nu_max_age, stf_age_range.nu_ball_weight')
		                  ->from('stf_station_configuration')
		                  ->join('stf_station', 'stf_station.id = stf_station_configuration.id_station')
		                  ->join('stf_test', 'stf_test.id = stf_station_configuration.id_test')
		                  ->join('stf_age_range', 'stf_age_range.id = stf_station_configuration.id_age_range')
		                  ->order_by('stf_station.tx_name', 'asc')
		                  ->order_by('stf_test.tx_name', 'asc')
		                  ->order_by('stf_age_range.nu_min_age', 'asc')
		                  ->get();

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}

	public function getTests($id_station)
	{
		$query = $this->db->select('stf_test.id, stf_test.tx_name')
		                  ->from('stf_test')
		                  ->join('stf_station_test', 'stf_station_test.id_test = stf_test.id')
		                  ->where('stf_station_test.id_station', $id_station)
		                  ->get();

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}
}