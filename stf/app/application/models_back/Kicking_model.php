<?php

/**
* 
*/
class Kicking_model extends CI_Model
{
	public function check($id_player, $id_event, $tests)
	{
		/*
		 *  0 => La prueba no existe
		 *  1 => La prueba existe y el jugador fue evaluado
		 *  2 => La prueba existe y el jugador no ha sido evaluado
		*/

		$checks = array(
			'aim' => 0,
			'force' => 0
		);

		foreach ($tests as $key => $test)
		{
			if ($test == "Aim")
			{
				$query = $this->db->limit(1)->select('id')->get_where('stf_kicking_aim', array('id_player' => $id_player, 'id_event' => $id_event));

				$checks['aim'] = ($query->num_rows() == 1) ? 1 : 2;
			}
			else if ($test == "Force")
			{
				$query = $this->db->limit(1)->select('id')->get_where('stf_kicking_force', array('id_player' => $id_player, 'id_event' => $id_event));

				$checks['force'] = ($query->num_rows() == 1) ? 1 : 2;
			}
		}

		return $checks;
	}

	public function createAim()
	{
		$id_uniq = uniqid(mt_rand());

		$data = array(
			'id_uniq' => $id_uniq,
			'id_player' => $this->input->post('id_player'),
			'id_event' => $this->input->post('id_event'),
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_right_leg_request_one' => $this->input->post('right_leg[request_one]'),
			'nu_right_leg_destination_one' => $this->input->post('right_leg[destination_one]'),
			'nu_right_leg_request_two' => $this->input->post('right_leg[request_two]'),
			'nu_right_leg_destination_two' => $this->input->post('right_leg[destination_two]'),
			'nu_right_leg_request_three' => $this->input->post('right_leg[request_three]'),
			'nu_right_leg_destination_three' => $this->input->post('right_leg[destination_three]'),
			'nu_left_leg_request_one' => $this->input->post('left_leg[request_one]'),
			'nu_left_leg_destination_one' => $this->input->post('left_leg[destination_one]'),
			'nu_left_leg_request_two' => $this->input->post('left_leg[request_two]'),
			'nu_left_leg_destination_two' => $this->input->post('left_leg[destination_two]'),
			'nu_left_leg_request_three' => $this->input->post('left_leg[request_three]'),
			'nu_left_leg_destination_three' => $this->input->post('left_leg[destination_three]'),
			'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
		);

		$this->db->insert('stf_kicking_aim', $data);
	}

	public function createForce()
	{
		$id_uniq = uniqid(mt_rand());

		$data = array(
			'id_uniq' => $id_uniq,
			'id_player' => $this->input->post('id_player'),
			'id_event' => $this->input->post('id_event'),
			'dt_measurement' => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_right_leg_attempt_one' => $this->input->post('right_leg[attempt_one]'),
			'nu_right_leg_attempt_two' => $this->input->post('right_leg[attempt_two]'),
			'nu_right_leg_attempt_three' => $this->input->post('right_leg[attempt_three]'),
			'nu_left_leg_attempt_one' => $this->input->post('left_leg[attempt_one]'),
			'nu_left_leg_attempt_two' => $this->input->post('left_leg[attempt_two]'),
			'nu_left_leg_attempt_three' => $this->input->post('left_leg[attempt_three]'),
			'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
		);

		$this->db->insert('stf_kicking_force', $data);
	}

	public function getAll($id_event) {
		/*$query = $this->db->select('stf_player.id, stf_player.tx_firstname, stf_player.tx_secondname, stf_player.tx_lastname, stf_player.tx_secondlastname')
		                  ->from('stf_player')
		                  ->join('stf_kicking_aim', 'stf_kicking_aim.id_player = stf_player.id', 'left outer')
		                  ->join('stf_kicking_force', 'stf_kicking_force.id_player = stf_player.id', 'left outer')
		                  ->where('stf_kicking_aim.id_event', $id_event)
		                  ->or_where('stf_kicking_force.id_event', $id_event)
		                  ->group_by('stf_player.id')
		                  ->order_by('stf_player.tx_firstname', 'asc')
		                  ->get();*/


		$sql = "SELECT stf_player.id_uniq, 
					   stf_player.id, 
					   stf_player.tx_firstname, 
					   stf_player.tx_secondname, 
					   stf_player.tx_lastname, 
					   stf_player.tx_secondlastname 
				  FROM stf_player
	   LEFT OUTER JOIN stf_kicking_aim
					ON stf_kicking_aim.id_player = stf_player.id 
	   LEFT OUTER JOIN stf_kicking_force
	   				ON stf_kicking_force.id_player = stf_player.id
	   			 WHERE stf_kicking_force.bo_status = 1
	   			   AND ( stf_kicking_aim.id_event = ".$id_event."
	   			    OR stf_kicking_force.id_event = ".$id_event." )
	   		  GROUP BY stf_player.id
	   	      ORDER BY stf_player.tx_firstname ASC";
	   	$query = $this->db->query($sql);		    
		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}

	public function delete($type) {
		$this->db->where('id_player', $this->input->input_stream('id_player'));
    	$this->db->update('stf_kicking_'.$type, array('bo_status' => '0'));
	}
}