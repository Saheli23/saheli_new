<?php

/**
* 
*/
class User_model extends CI_Model
{
	public function getUniqIDs()
	{
		$query = $this->db->order_by('tx_firstname', 'asc')->select('id_uniq')->get('stf_user');

		return $query->result();
	}

	public function getID($id_uniq)
	{
		$query = $this->db->limit(1)->select('id')->get_where('stf_user', array('id_uniq' => $id_uniq));

		return ($query->num_rows() == 1) ? $query->row()->id : NULL;
	}

	public function getUserPermissions($id_uniq)
	{
		$id_user = $this->db->limit(1)->select('id')->get_where('stf_user', array('id_uniq' => $id_uniq))->row()->id;

		$query = $this->db->limit(1)->get_where('stf_access', array('id_user' => $id_user));

		return ($query->num_rows() == 1) ? $query->row() : NULL;
	}

	public function create($image_path) {
		$id_uniq = uniqid(mt_rand());
		$security_key = bin2hex($this->encryption->create_key(20));
		$profile_picture = ($image_path != NULL) ? $image_path : 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png';
		
		$sections = json_decode($this->input->post('sections'));

		//die();

		$user_data = array(
			'id_uniq' => $id_uniq,
			'tx_firstname' => $this->input->post('firstname'),
			'tx_lastname' => $this->input->post('lastname'),
			'tx_email' => $this->input->post('email'),
			'tx_password' => hash_hmac("sha256", $this->input->post('password'), $this->config->item('password_key')),
			'tx_image' => $profile_picture,
			'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York")),
			'tx_security_key' => $security_key,
			'bo_status' => 1
		);

		
		$this->db->trans_start();
		$this->db->insert('stf_user', $user_data);

		$id_user = $this->db->insert_id();


		foreach( $sections as $key => $value ){
			$user_access_data = array(
				'id_user' => $id_user,
				'id_section' => $value->id,
				'bo_view' => $value->view,
				'bo_add' => $value->add,
				'bo_edit' => $value->edit,
				'bo_delete' => $value->delete,
				'bo_admin' => $this->input->post('is_admin')
			);

			$this->db->insert('stf_access', $user_access_data);

		}

		
		$this->db->trans_complete();

		return $id_uniq;
	}

	public function getLoggedInUser($id_uniq)
	{
		$query = $this->db->limit(1)
		                  ->select('id_uniq, tx_firstname, tx_lastname, tx_email, dt_register, bo_status')
						  ->get_where('stf_user', array('id_uniq' => $id_uniq));

		return ($query->num_rows() == 1) ? $query->row() : NULL;
	}

	public function getAll() {
		$sql  = "SELECT stf_user.id, 
						stf_user.id_uniq, 
						stf_user.tx_firstname, 
						stf_user.tx_lastname, 
						stf_user.tx_email, 
						stf_access.id_section,
						stf_access.bo_view,
						stf_access.bo_add,
						stf_access.bo_edit,
						stf_access.bo_delete,
						stf_access.bo_admin
				   FROM stf_user
			 INNER JOIN stf_access
		     		 ON stf_user.id = stf_access.id_user
		     	  WHERE stf_user.bo_status = 1
		     	  	AND stf_access.id_section = 5
		     	  ORDER BY stf_user.tx_firstname ASC";
		$query = $this->db->query($sql);
		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}

	public function getOne($id_uniq) {
		$id_user = $this->getID($id_uniq);
		if ($id_user != NULL) {
			$sql  = "SELECT stf_user.id,
						stf_user.id_uniq, 
						stf_user.tx_firstname, 
						stf_user.tx_lastname, 
						stf_user.tx_email,
						stf_user.tx_image,
						stf_user.dt_register,
						stf_access.id_section,
						stf_section.section_name,
						stf_access.bo_view,
						stf_access.bo_add,
						stf_access.bo_edit,
						stf_access.bo_delete,
						stf_access.bo_admin
				   FROM stf_user
		     INNER JOIN stf_access
		     		 ON stf_user.id = stf_access.id_user
		     INNER JOIN stf_section
		     		 ON stf_section.id = stf_access.id_section
		     	  WHERE stf_user.bo_status = 1
		     	  	AND stf_user.id = ".$id_user;
			$user = $this->db->query($sql)->result();
		}

		return $user;
	}

	public function edit($id_uniq, $image_path) {
		$id_user = $this->getID($id_uniq);

		if ($id_user != NULL) {

			$sections = json_decode($this->input->post('sections'));

			$user_data = array(
				'tx_firstname' => $this->input->post('firstname'),
				'tx_lastname'  => $this->input->post('lastname'),
				'tx_email'     => $this->input->post('email')
			);

			if ($this->input->post('password') != "") {
				$user_data['tx_password'] = hash_hmac("sha256", $this->input->post('password'), $this->config->item('password_key'));
			}

			if ($image_path != NULL) {
				$user_data['tx_image'] = $image_path;
			}

			$this->db->trans_start();
			$this->db->limit(1)->where('id', $id_user)->update('stf_user', $user_data);

			$this->db->delete( 'stf_access', array('id_user' => $id_user) );

			foreach( $sections as $key => $value ){
				$user_access_data = array(
					'id_user' => $id_user,
					'id_section' => $value->id,
					'bo_view' => $value->view,
					'bo_add' => $value->add,
					'bo_edit' => $value->edit,
					'bo_delete' => $value->delete,
					'bo_admin' => $this->input->post('is_admin')
				);

				$this->db->insert('stf_access', $user_access_data);
			}

			$this->db->trans_complete();
		}
	}

	public function checkEmailForEdit($id_uniq)
	{
		$query = $this->db->limit(1)
						  ->where_not_in('id_uniq', $id_uniq)
						  ->where('tx_email', $this->input->post('email'))
						  ->select('tx_email')
						  ->get('stf_user');

		return ($query->num_rows() == 1) ? TRUE : FALSE;
	}

	public function getUserImage($id_uniq)
	{
		$query = $this->db->limit(1)->select('tx_image')->get_where('stf_user', array('id_uniq' => $id_uniq));

		return ($query->num_rows() == 1) ? $query->row() : NULL;
	}

	public function delete() {
		$this->db->where('id_uniq', $this->input->input_stream('id_user'));
    	$this->db->update('stf_user', array('bo_status' => '0'));
	}
}