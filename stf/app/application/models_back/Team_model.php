<?php

/**
* 
*/
class Team_model extends CI_Model
{
	public function getUniqIDs()
	{
		$query = $this->db->order_by('tx_name', 'asc')->select('id_uniq')->get('stf_team');

		return $query->result();
	}

	public function getCategories()
	{
		$query = $this->db->get('stf_category');

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}

	public function create($image_path)
	{
		$id_uniq = uniqid(mt_rand());

		$logo = ($image_path != NULL) ? $image_path : 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png';
		$phone_prefix = ($this->input->post('phonenumber') != "") ? $this->input->post('phoneprefix') : "";

		$data = array(
			'id_uniq' => $id_uniq,
			'tx_name' => $this->input->post('name'),
			'tx_image' => $logo,
			'id_country' => $this->input->post('country'),
			'id_state' => $this->input->post('state'),
			'tx_address' => $this->input->post('address'),
			'tx_zipcode' => $this->input->post('zipcode'),
			'tx_phone_prefix' =>  $phone_prefix,
			'tx_phone' =>  $this->input->post('phonenumber'),
			'tx_email' => $this->input->post('email'),
			'tx_website' => $this->input->post('website'),
			'tx_facebook' => $this->input->post('facebook'),
			'tx_twitter' => $this->input->post('twitter'),
			'tx_instagram' => $this->input->post('instagram'),
			'dt_register' => mdate("%Y-%m-%d %H:%i:s", now("America/New_York"))
		);

		$categories_received = json_decode($this->input->post('categories'));
		$categories_to_save = array();

		$this->db->trans_start();
		$this->db->insert('stf_team', $data);
		$id_team = $this->db->insert_id();

		foreach ($categories_received as $key => $category)
		{
			array_push($categories_to_save, array(
				'id_team' => $id_team,
				'id_category' => $category->id_category,
				'id_gender' => $category->id_gender,
				'nu_player' => $category->nu_players
			));
		}

		$this->db->insert_batch('stf_team_category', $categories_to_save);
		$this->db->trans_complete();

		return $id_uniq;
	}

	public function getOne($id_uniq)
	{
		$team = NULL;

		$query = $this->db->limit(1)
			              ->select('stf_team.*, stf_country.tx_name AS country, stf_state.tx_name AS state')
			              ->from('stf_team')
			              ->join('stf_country', 'stf_country.id = stf_team.id_country')
			              ->join('stf_state', 'stf_state.id = stf_team.id_state')
			              ->where('stf_team.id_uniq', $id_uniq)
			              ->get();

		if ($query->num_rows() == 1)
		{
			$team = $query->row();

			$query = $this->db->select('stf_team_category.id_category, stf_team_category.id_gender, stf_team_category.nu_player AS nu_players, stf_category.tx_name AS name, stf_gender.tx_name AS gender')
			                  ->from('stf_team_category')
			                  ->join('stf_category', 'stf_category.id = stf_team_category.id_category')
			                  ->join('stf_gender', 'stf_gender.id = stf_team_category.id_gender')
			                  ->where('stf_team_category.id_team', $team->id)
			                  ->get();

			$team->categories = $query->result();
		}
		
		return $team;
	}

	public function getAll()
	{
		$query = $this->db->order_by('tx_name', 'asc')->select('id, id_uniq, tx_name, tx_email, tx_phone_prefix, tx_phone')->where('bo_status', '1')->get('stf_team');

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}

	public function edit($id_uniq, $image_path)
	{
		$phone_prefix = ($this->input->post('phonenumber') != "") ? $this->input->post('phoneprefix') : "";

		$data = array(
			'tx_name' => $this->input->post('name'),
			'id_country' => $this->input->post('country'),
			'id_state' => $this->input->post('state'),
			'tx_address' => $this->input->post('address'),
			'tx_zipcode' => $this->input->post('zipcode'),
			'tx_phone_prefix' =>  $phone_prefix,
			'tx_phone' =>  $this->input->post('phonenumber'),
			'tx_email' => $this->input->post('email'),
			'tx_website' => $this->input->post('website'),
			'tx_facebook' => $this->input->post('facebook'),
			'tx_twitter' => $this->input->post('twitter'),
			'tx_instagram' => $this->input->post('instagram')
		);

		if ($image_path != NULL)
		{
			$data['tx_image'] = $image_path;
		}

		$categories_received = json_decode($this->input->post('categories'));
		$categories_to_save = array();

		$id_team_query = $this->db->limit(1)->select('id')->get_where('stf_team', array('id_uniq' => $id_uniq));

		if ($id_team_query->num_rows() == 1)
		{
			$this->db->trans_start();
			$this->db->limit(1)->where('id_uniq', $id_uniq)->update('stf_team', $data);
			
			$this->db->delete('stf_team_category', array('id_team' => $id_team_query->row()->id));

			foreach ($categories_received as $key => $category)
			{
				array_push($categories_to_save, array(
					'id_team' => $id_team_query->row()->id,
					'id_category' => $category->id_category,
					'id_gender' => $category->id_gender,
					'nu_player' => $category->nu_players
				));
			}

			$this->db->insert_batch('stf_team_category', $categories_to_save);
			$this->db->trans_complete();
		}
	}

	public function checkEmailForEdit($id_uniq)
	{
		$query = $this->db->limit(1)
						  ->where_not_in('id_uniq', $id_uniq)
						  ->where('tx_email', $this->input->post('email'))
						  ->select('tx_email')
						  ->get('stf_team');

		return ($query->num_rows() == 1) ? TRUE : FALSE;
	}

	public function getPositions()
	{
		$query = $this->db->get('stf_position');

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}

	public function getTeamImage($id_uniq)
	{
		$query = $this->db->limit(1)->select('tx_image')->get_where('stf_team', array('id_uniq' => $id_uniq));

		return ($query->num_rows() == 1) ? $query->row() : NULL;
	}

	public function delete() {
		$this->db->where('id', $this->input->input_stream('id_team'));
    	$this->db->update('stf_team', array('bo_status' => '0'));
	}

	public function check_child( $type ){
		$this->db->where('id_team' , $this->input->input_stream('id_team'));
		return $this->db->get('stf_'.$type)->num_rows();
	}
}