<?php

/**
* 
*/
class Uploader_model extends CI_Model
{
	public function updateProfilePicture($table_name, $id_uniq, $picture_path)
	{
		$data = array(
			'tx_image' => $picture_path
		);

		$this->db->limit(1)->where('id_uniq', $id_uniq)->update($table_name, $data);
	}

	public function updatePlayerPictures($id_uniq, $pictures)
	{
		$query = $this->db->limit(1)->select('id')->get_where('stf_player', array('id_uniq' => $id_uniq));

		if ($query->num_rows() == 1)
		{
			$id_player = $query->row()->id;
			$data = array();

			foreach ($pictures as $key => $picture)
			{
				$this->db->trans_start();
				$this->db->insert('stf_image', array('tx_name' => $picture, 'dt_register' => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))));
				array_push($data, array('id_player' => $id_player, 'id_image' => $this->db->insert_id(), 'id_type_image' => $key + 1));
				$this->db->trans_complete();
			}

			$this->db->insert_batch('stf_player_image', $data);
		}
	}
}