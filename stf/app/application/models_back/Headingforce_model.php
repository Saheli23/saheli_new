<?php

/**
* 
*/
class Headingforce_model extends CI_Model{
	public function check($id_player, $id_event, $tests) {
		/*
		 *  0 => La prueba no existe
		 *  1 => La prueba existe y el jugador fue evaluado
		 *  2 => La prueba existe y el jugador no ha sido evaluado
		*/

		$checks = array( 'force' => 0 );

		/*print_r($tests);

		die();*/

		foreach ($tests as $key => $test) {

			if ($test == "Force") {
				$query = $this->db->limit(1)->select('id')->get_where('stf_heading_force_force', array('id_player' => $id_player, 'id_event' => $id_event));

				$checks['force'] = ($query->num_rows() == 1) ? 1 : 2;
			}
		}

		return $checks;
	}

	public function createForce(){
		$id_uniq = uniqid(mt_rand());
		$data = array(
			'id_uniq'               => $id_uniq,
			'id_player'             => $this->input->post('id_player'),
			'id_event'              => $this->input->post('id_event'),
			'dt_measurement'        => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_distance'           => $this->input->post('distance'),
			'nu_head_attempt_one'   => $this->input->post('attempt_one'),
			'nu_head_attempt_two'   => $this->input->post('attempt_two'),
			'nu_head_attempt_three' => $this->input->post('attempt_three'),
			'dt_register'           => mdate("%Y-%m-%d %H:%i:%s", now("America/New_York"))
		);

		$this->db->insert('stf_heading_force_force', $data);
	}

	public function updateForce() {

		$where = array( $this->input->post('id_player'), 'id_event' => $this->input->post('id_event') ;

		$data = array(
			'dt_measurement'        => mdate("%Y-%m-%d", strtotime($this->input->post('measurement_date'))),
			'nu_distance'           => $this->input->post('distance'),
			'nu_head_attempt_one'   => $this->input->post('attempt_one'),
			'nu_head_attempt_two'   => $this->input->post('attempt_two'),
			'nu_head_attempt_three' => $this->input->post('attempt_three')
		);

		$this->db->where($where);

		$this->db->update('stf_heading_force', $this->input->post('id_player'));
	}

	public function deleteForce() {
		$this->db->where('id_player', $this->input->post('id_player'));
    	$this->db->update('stf_heading_force', array('bo_status' => '0'));
	}

	public function getAll($id_event) {
		$query = $this->db->select('stf_player.id, stf_player.tx_firstname, stf_player.tx_secondname, stf_player.tx_lastname, stf_player.tx_secondlastname')
		                  ->from('stf_player')
		                  ->join('stf_heading_force_force','stf_heading_force_force.id_player = stf_player.id','left outer')
		                  ->where('stf_heading_force_force.id_event', $id_event)
		                  ->group_by('stf_player.id')
		                  ->order_by('stf_player.tx_firstname', 'asc')
		                  ->get();

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}
}