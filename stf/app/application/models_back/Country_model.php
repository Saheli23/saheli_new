<?php 

/**
* 
*/
class Country_model extends CI_Model
{
	public function getAll()
	{
		$query = $this->db->limit(1)->get_where('stf_country', array('id' => 21));
		
		return ($query->num_rows() == 1) ? $query->result() : NULL;
	}

	public function getCountryStates($id_country)
	{
		$query = $this->db->order_by('tx_name', 'asc')->get_where('stf_state', array('id_country' => $id_country));

		return ($query->num_rows() > 0) ? $query->result() : NULL;
	}
}