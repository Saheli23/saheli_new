module.exports = function (grunt) {

	grunt.initConfig({
		uglify: {
			options: {
				mangle: false
			},
			target: {
				files: {
					'dist/js/app.min.js': ['dist/js/app.js']
				}
			}
		},
		concat: {
			dist: {
				src: [
					'src/js/**/*.js',
					'src/js/*.js'
				],
				dest: 'dist/js/app.js'
			}
			/*vendorjs: {
				src: [
					'bower_components/angular-responsive-tables/release/angular-responsive-tables.min.js',
					'bower_components/angular-ui-mask/dist/mask.min.js',
					'bower_components/lodash/dist/lodash.min.js',
					'bower_components/angular-simple-logger/dist/angular-simple-logger.min.js',
					'bower_components/angular-google-maps/dist/angular-google-maps.min.js',
					'bower_components/ng-pickadate/ng-pickadate.js',
					'bower_components/cryptojslib/rollups/aes.js',
					'bower_components/angular-cryptography/mdo-angular-cryptography.js'
				],
				dest: 'dist/js/vendor.min.js'
			},
			vendorcss: {
				src: [
					'bower_components/angular-responsive-tables/release/angular-responsive-tables.min.css'
				],
				dest: 'dist/css/vendor.min.css'
			}*/
		},
		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			target: {
				files: {
					'dist/css/main.min.css': ['src/css/*.css']
				}
			}
		},
		watch: {
			js: {
				files: [
					'Gruntfile.js',
					'src/js/**/*.js',
					'src/js/*.js'
				],
				tasks: ['concat']
			},
			css: {
				files: [
					'src/css/*.css',
					'!src/css/*.min.css'
				],
				tasks: ['cssmin']
			}
		},
		compress: {
			main: {
				options: {
					archive: 'stf.zip'
				},
				files: [
					{ 
						src: [
							'app/**',
							'dist/**',
							'vendor/**',
							'.htaccess',
							'index.php'
						] 
					}
				]
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-compress');
	grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);	

}