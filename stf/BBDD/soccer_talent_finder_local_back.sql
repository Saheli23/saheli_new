-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 23, 2016 at 03:07 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `soccer_talent_finder`
--

-- --------------------------------------------------------

--
-- Table structure for table `stf_access`
--

CREATE TABLE IF NOT EXISTS `stf_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_section` int(11) NOT NULL,
  `bo_view` tinyint(1) NOT NULL DEFAULT '1',
  `bo_add` tinyint(1) NOT NULL DEFAULT '0',
  `bo_edit` tinyint(1) NOT NULL DEFAULT '0',
  `bo_delete` tinyint(1) NOT NULL DEFAULT '0',
  `bo_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=243 ;

--
-- Dumping data for table `stf_access`
--

INSERT INTO `stf_access` (`id`, `id_user`, `id_section`, `bo_view`, `bo_add`, `bo_edit`, `bo_delete`, `bo_admin`) VALUES
(189, 16, 1, 1, 1, 0, 0, 0),
(190, 16, 2, 0, 0, 0, 0, 0),
(191, 16, 3, 1, 0, 1, 0, 0),
(192, 16, 4, 1, 0, 1, 1, 0),
(193, 16, 5, 0, 0, 0, 0, 0),
(194, 2, 1, 1, 0, 0, 0, 0),
(195, 2, 2, 1, 0, 0, 0, 0),
(196, 2, 3, 1, 0, 0, 0, 0),
(197, 2, 4, 1, 0, 0, 0, 0),
(198, 2, 5, 1, 0, 0, 0, 0),
(199, 12, 1, 1, 0, 0, 0, 0),
(200, 12, 2, 1, 0, 0, 0, 0),
(201, 12, 3, 1, 0, 0, 0, 0),
(202, 12, 4, 1, 0, 0, 0, 0),
(203, 12, 5, 1, 1, 1, 1, 0),
(212, 3, 1, 1, 1, 1, 1, 1),
(213, 3, 2, 1, 1, 1, 1, 1),
(214, 3, 3, 1, 1, 1, 1, 1),
(215, 3, 4, 1, 1, 1, 1, 1),
(216, 3, 5, 1, 1, 1, 1, 1),
(217, 4, 1, 1, 1, 1, 1, 1),
(218, 4, 2, 1, 1, 1, 1, 1),
(219, 4, 3, 1, 1, 1, 1, 1),
(220, 4, 4, 1, 1, 1, 1, 1),
(221, 4, 5, 1, 1, 1, 1, 1),
(222, 13, 1, 1, 1, 1, 1, 1),
(223, 13, 2, 1, 1, 1, 1, 1),
(224, 13, 3, 1, 1, 1, 1, 1),
(225, 13, 4, 1, 1, 1, 1, 1),
(226, 13, 5, 1, 1, 1, 1, 1),
(227, 10, 1, 1, 1, 1, 1, 1),
(228, 10, 2, 1, 1, 1, 1, 1),
(229, 10, 3, 1, 1, 1, 1, 1),
(230, 10, 4, 1, 1, 1, 1, 1),
(231, 10, 5, 1, 1, 1, 1, 1),
(232, 11, 1, 1, 1, 1, 1, 1),
(233, 11, 2, 1, 1, 1, 1, 1),
(234, 11, 3, 1, 1, 1, 1, 1),
(235, 11, 4, 1, 1, 1, 1, 1),
(236, 11, 5, 1, 1, 1, 1, 1),
(238, 15, 1, 1, 0, 0, 1, 0),
(239, 15, 2, 1, 0, 0, 1, 0),
(240, 15, 3, 0, 0, 0, 0, 0),
(241, 15, 4, 0, 0, 0, 0, 0),
(242, 15, 5, 1, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `stf_age_range`
--

CREATE TABLE IF NOT EXISTS `stf_age_range` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nu_min_age` int(11) NOT NULL,
  `nu_max_age` int(11) NOT NULL,
  `nu_ball_weight` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `stf_age_range`
--

INSERT INTO `stf_age_range` (`id`, `nu_min_age`, `nu_max_age`, `nu_ball_weight`) VALUES
(1, 5, 8, 326),
(2, 9, 12, 340),
(3, 13, 18, 430);

-- --------------------------------------------------------

--
-- Table structure for table `stf_anthropometric`
--

CREATE TABLE IF NOT EXISTS `stf_anthropometric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_size` float DEFAULT NULL,
  `nu_weight` float DEFAULT NULL,
  `nu_chest_diameter` float DEFAULT NULL,
  `nu_hip_diameter` float DEFAULT NULL,
  `nu_left_thigh_diameter` float DEFAULT NULL,
  `nu_right_thigh_diameter` float DEFAULT NULL,
  `nu_left_ankle_diameter` float DEFAULT NULL,
  `nu_right_ankle_diameter` float DEFAULT NULL,
  `nu_head_diameter` float DEFAULT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_uniq` (`id_uniq`),
  KEY `id_player` (`id_player`),
  KEY `id_event` (`id_event`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `stf_anthropometric`
--

INSERT INTO `stf_anthropometric` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_size`, `nu_weight`, `nu_chest_diameter`, `nu_hip_diameter`, `nu_left_thigh_diameter`, `nu_right_thigh_diameter`, `nu_left_ankle_diameter`, `nu_right_ankle_diameter`, `nu_head_diameter`, `dt_register`, `bo_status`) VALUES
(1, '12282538905735fefae18fb', 17, 1, '2016-05-13', 10, 70, 10, 10, 10, 10, 10, 10, 10, '2016-05-13 12:21:14', 1),
(2, '199402779857567830f018b', 18, 1, '2016-06-07', 6, 39, 14, 9, 8, 8, 8, 8, 11, '2016-06-07 03:30:56', 1),
(3, '11917647685757f9709a91b', 17, 2, '2016-06-08', 5, 2, 5, 4, 5, 5, 4, 5, 4, '2016-06-08 06:54:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_category`
--

CREATE TABLE IF NOT EXISTS `stf_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_name` varchar(100) NOT NULL,
  `tx_abbr` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `stf_category`
--

INSERT INTO `stf_category` (`id`, `tx_name`, `tx_abbr`) VALUES
(1, 'Children A', 'Ch A'),
(2, 'Children B', 'Ch B'),
(3, 'Children C', 'Ch C'),
(4, 'Junior', 'Jr'),
(5, 'Junior 1st Division', 'Jr 1D'),
(6, 'Junior 2nd Division', 'Jr 2D'),
(7, 'Professional', 'Pro');

-- --------------------------------------------------------

--
-- Table structure for table `stf_country`
--

CREATE TABLE IF NOT EXISTS `stf_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_name` varchar(100) NOT NULL,
  `tx_iso_code` varchar(10) NOT NULL,
  `tx_phone_prefix` varchar(5) NOT NULL,
  `tx_zipcode_format` varchar(50) NOT NULL,
  `id_zone` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_stf_country_id_zone` (`id_zone`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=245 ;

--
-- Dumping data for table `stf_country`
--

INSERT INTO `stf_country` (`id`, `tx_name`, `tx_iso_code`, `tx_phone_prefix`, `tx_zipcode_format`, `id_zone`) VALUES
(1, 'Germany', 'DE', '49', 'NNNNN', 1),
(2, 'Austria', 'AT', '43', 'NNNN', 1),
(3, 'Belgium', 'BE', '32', 'NNNN', 1),
(4, 'Canada', 'CA', '1', 'LNL NLN', 2),
(5, 'China', 'CN', '86', 'NNNNNN', 3),
(6, 'Spain', 'ES', '34', 'NNNNN', 1),
(7, 'Finland', 'FI', '358', 'NNNNN', 1),
(8, 'France', 'FR', '33', 'NNNNN', 1),
(9, 'Greece', 'GR', '30', 'NNNNN', 1),
(10, 'Italy', 'IT', '39', 'NNNNN', 1),
(11, 'Japan', 'JP', '81', 'NNN-NNNN', 3),
(12, 'Luxemburg', 'LU', '352', 'NNNN', 1),
(13, 'Netherlands', 'NL', '31', 'NNNN LL', 1),
(14, 'Poland', 'PL', '48', 'NN-NNN', 1),
(15, 'Portugal', 'PT', '351', 'NNNN-NNN', 1),
(16, 'Czech Republic', 'CZ', '420', 'NNN NN', 1),
(17, 'United Kingdom', 'GB', '44', '', 1),
(18, 'Sweden', 'SE', '46', 'NNN NN', 1),
(19, 'Switzerland', 'CH', '41', 'NNNN', 7),
(20, 'Denmark', 'DK', '45', 'NNNN', 1),
(21, 'United States', 'US', '1', 'NNNNN', 2),
(22, 'HongKong', 'HK', '852', '', 3),
(23, 'Norway', 'NO', '47', 'NNNN', 7),
(24, 'Australia', 'AU', '61', 'NNNN', 5),
(25, 'Singapore', 'SG', '65', 'NNNNNN', 3),
(26, 'Ireland', 'IE', '353', '', 1),
(27, 'New Zealand', 'NZ', '64', 'NNNN', 5),
(28, 'South Korea', 'KR', '82', 'NNN-NNN', 3),
(29, 'Israel', 'IL', '972', 'NNNNNNN', 3),
(30, 'South Africa', 'ZA', '27', 'NNNN', 4),
(31, 'Nigeria', 'NG', '234', '', 4),
(32, 'Ivory Coast', 'CI', '225', '', 4),
(33, 'Togo', 'TG', '228', '', 4),
(34, 'Bolivia', 'BO', '591', '', 6),
(35, 'Mauritius', 'MU', '230', '', 4),
(36, 'Romania', 'RO', '40', 'NNNNNN', 1),
(37, 'Slovakia', 'SK', '421', 'NNN NN', 1),
(38, 'Algeria', 'DZ', '213', 'NNNNN', 4),
(39, 'American Samoa', 'AS', '0', '', 2),
(40, 'Andorra', 'AD', '376', 'CNNN', 7),
(41, 'Angola', 'AO', '244', '', 4),
(42, 'Anguilla', 'AI', '0', '', 8),
(43, 'Antigua and Barbuda', 'AG', '0', '', 2),
(44, 'Argentina', 'AR', '54', 'LNNNN', 6),
(45, 'Armenia', 'AM', '374', 'NNNN', 3),
(46, 'Aruba', 'AW', '297', '', 8),
(47, 'Azerbaijan', 'AZ', '994', 'CNNNN', 3),
(48, 'Bahamas', 'BS', '0', '', 2),
(49, 'Bahrain', 'BH', '973', '', 3),
(50, 'Bangladesh', 'BD', '880', 'NNNN', 3),
(51, 'Barbados', 'BB', '0', 'CNNNNN', 2),
(52, 'Belarus', 'BY', '0', 'NNNNNN', 7),
(53, 'Belize', 'BZ', '501', '', 8),
(54, 'Benin', 'BJ', '229', '', 4),
(55, 'Bermuda', 'BM', '0', '', 2),
(56, 'Bhutan', 'BT', '975', '', 3),
(57, 'Botswana', 'BW', '267', '', 4),
(58, 'Brazil', 'BR', '55', 'NNNNN-NNN', 6),
(59, 'Brunei', 'BN', '673', 'LLNNNN', 3),
(60, 'Burkina Faso', 'BF', '226', '', 4),
(61, 'Burma (Myanmar)', 'MM', '95', '', 3),
(62, 'Burundi', 'BI', '257', '', 4),
(63, 'Cambodia', 'KH', '855', 'NNNNN', 3),
(64, 'Cameroon', 'CM', '237', '', 4),
(65, 'Cape Verde', 'CV', '238', 'NNNN', 4),
(66, 'Central African Republic', 'CF', '236', '', 4),
(67, 'Chad', 'TD', '235', '', 4),
(68, 'Chile', 'CL', '56', 'NNN-NNNN', 6),
(69, 'Colombia', 'CO', '57', 'NNNNNN', 6),
(70, 'Comoros', 'KM', '269', '', 4),
(71, 'Congo, Dem. Republic', 'CD', '242', '', 4),
(72, 'Congo, Republic', 'CG', '243', '', 4),
(73, 'Costa Rica', 'CR', '506', 'NNNNN', 8),
(74, 'Croatia', 'HR', '385', 'NNNNN', 7),
(75, 'Cuba', 'CU', '53', '', 8),
(76, 'Cyprus', 'CY', '357', 'NNNN', 1),
(77, 'Djibouti', 'DJ', '253', '', 4),
(78, 'Dominica', 'DM', '0', '', 8),
(79, 'Dominican Republic', 'DO', '0', '', 8),
(80, 'East Timor', 'TL', '670', '', 3),
(81, 'Ecuador', 'EC', '593', 'CNNNNNN', 6),
(82, 'Egypt', 'EG', '20', '', 4),
(83, 'El Salvador', 'SV', '503', '', 8),
(84, 'Equatorial Guinea', 'GQ', '240', '', 4),
(85, 'Eritrea', 'ER', '291', '', 4),
(86, 'Estonia', 'EE', '372', 'NNNNN', 1),
(87, 'Ethiopia', 'ET', '251', '', 4),
(88, 'Falkland Islands', 'FK', '0', 'LLLL NLL', 8),
(89, 'Faroe Islands', 'FO', '298', '', 7),
(90, 'Fiji', 'FJ', '679', '', 5),
(91, 'Gabon', 'GA', '241', '', 4),
(92, 'Gambia', 'GM', '220', '', 4),
(93, 'Georgia', 'GE', '995', 'NNNN', 3),
(94, 'Ghana', 'GH', '233', '', 4),
(95, 'Grenada', 'GD', '0', '', 8),
(96, 'Greenland', 'GL', '299', '', 7),
(97, 'Gibraltar', 'GI', '350', '', 7),
(98, 'Guadeloupe', 'GP', '590', '', 8),
(99, 'Guam', 'GU', '0', '', 5),
(100, 'Guatemala', 'GT', '502', '', 8),
(101, 'Guernsey', 'GG', '0', 'LLN NLL', 7),
(102, 'Guinea', 'GN', '224', '', 4),
(103, 'Guinea-Bissau', 'GW', '245', '', 4),
(104, 'Guyana', 'GY', '592', '', 6),
(105, 'Haiti', 'HT', '509', '', 8),
(106, 'Heard Island and McDonald Islands', 'HM', '0', '', 5),
(107, 'Vatican City State', 'VA', '379', 'NNNNN', 7),
(108, 'Honduras', 'HN', '504', '', 8),
(109, 'Iceland', 'IS', '354', 'NNN', 7),
(110, 'India', 'IN', '91', 'NNN NNN', 3),
(111, 'Indonesia', 'ID', '62', 'NNNNN', 3),
(112, 'Iran', 'IR', '98', 'NNNNN-NNNNN', 3),
(113, 'Iraq', 'IQ', '964', 'NNNNN', 3),
(114, 'Man Island', 'IM', '0', 'CN NLL', 7),
(115, 'Jamaica', 'JM', '0', '', 8),
(116, 'Jersey', 'JE', '0', 'CN NLL', 7),
(117, 'Jordan', 'JO', '962', '', 3),
(118, 'Kazakhstan', 'KZ', '7', 'NNNNNN', 3),
(119, 'Kenya', 'KE', '254', '', 4),
(120, 'Kiribati', 'KI', '686', '', 5),
(121, 'Korea, Dem. Republic of', 'KP', '850', '', 3),
(122, 'Kuwait', 'KW', '965', '', 3),
(123, 'Kyrgyzstan', 'KG', '996', '', 3),
(124, 'Laos', 'LA', '856', '', 3),
(125, 'Latvia', 'LV', '371', 'C-NNNN', 1),
(126, 'Lebanon', 'LB', '961', '', 3),
(127, 'Lesotho', 'LS', '266', '', 4),
(128, 'Liberia', 'LR', '231', '', 4),
(129, 'Libya', 'LY', '218', '', 4),
(130, 'Liechtenstein', 'LI', '423', 'NNNN', 1),
(131, 'Lithuania', 'LT', '370', 'NNNNN', 1),
(132, 'Macau', 'MO', '853', '', 3),
(133, 'Macedonia', 'MK', '389', '', 7),
(134, 'Madagascar', 'MG', '261', '', 4),
(135, 'Malawi', 'MW', '265', '', 4),
(136, 'Malaysia', 'MY', '60', 'NNNNN', 3),
(137, 'Maldives', 'MV', '960', '', 3),
(138, 'Mali', 'ML', '223', '', 4),
(139, 'Malta', 'MT', '356', 'LLL NNNN', 1),
(140, 'Marshall Islands', 'MH', '692', '', 5),
(141, 'Martinique', 'MQ', '596', '', 8),
(142, 'Mauritania', 'MR', '222', '', 4),
(143, 'Hungary', 'HU', '36', 'NNNN', 1),
(144, 'Mayotte', 'YT', '262', '', 4),
(145, 'Mexico', 'MX', '52', 'NNNNN', 2),
(146, 'Micronesia', 'FM', '691', '', 5),
(147, 'Moldova', 'MD', '373', 'C-NNNN', 7),
(148, 'Monaco', 'MC', '377', '980NN', 7),
(149, 'Mongolia', 'MN', '976', '', 3),
(150, 'Montenegro', 'ME', '382', 'NNNNN', 7),
(151, 'Montserrat', 'MS', '0', '', 8),
(152, 'Morocco', 'MA', '212', 'NNNNN', 4),
(153, 'Mozambique', 'MZ', '258', '', 4),
(154, 'Namibia', 'NA', '264', '', 4),
(155, 'Nauru', 'NR', '674', '', 5),
(156, 'Nepal', 'NP', '977', '', 3),
(157, 'Netherlands Antilles', 'AN', '599', '', 8),
(158, 'New Caledonia', 'NC', '687', '', 5),
(159, 'Nicaragua', 'NI', '505', 'NNNNNN', 8),
(160, 'Niger', 'NE', '227', '', 4),
(161, 'Niue', 'NU', '683', '', 5),
(162, 'Norfolk Island', 'NF', '0', '', 5),
(163, 'Northern Mariana Islands', 'MP', '0', '', 5),
(164, 'Oman', 'OM', '968', '', 3),
(165, 'Pakistan', 'PK', '92', '', 3),
(166, 'Palau', 'PW', '680', '', 5),
(167, 'Palestinian Territories', 'PS', '0', '', 3),
(168, 'Panama', 'PA', '507', 'NNNNNN', 8),
(169, 'Papua New Guinea', 'PG', '675', '', 5),
(170, 'Paraguay', 'PY', '595', '', 6),
(171, 'Peru', 'PE', '51', '', 6),
(172, 'Philippines', 'PH', '63', 'NNNN', 3),
(173, 'Pitcairn', 'PN', '0', 'LLLL NLL', 5),
(174, 'Puerto Rico', 'PR', '0', 'NNNNN', 8),
(175, 'Qatar', 'QA', '974', '', 3),
(176, 'Reunion Island', 'RE', '262', '', 4),
(177, 'Russian Federation', 'RU', '7', 'NNNNNN', 7),
(178, 'Rwanda', 'RW', '250', '', 4),
(179, 'Saint Barthelemy', 'BL', '0', '', 8),
(180, 'Saint Kitts and Nevis', 'KN', '0', '', 8),
(181, 'Saint Lucia', 'LC', '0', '', 8),
(182, 'Saint Martin', 'MF', '0', '', 8),
(183, 'Saint Pierre and Miquelon', 'PM', '508', '', 8),
(184, 'Saint Vincent and the Grenadines', 'VC', '0', '', 8),
(185, 'Samoa', 'WS', '685', '', 5),
(186, 'San Marino', 'SM', '378', 'NNNNN', 7),
(187, 'São Tomé and Príncipe', 'ST', '239', '', 4),
(188, 'Saudi Arabia', 'SA', '966', '', 3),
(189, 'Senegal', 'SN', '221', '', 4),
(190, 'Serbia', 'RS', '381', 'NNNNN', 7),
(191, 'Seychelles', 'SC', '248', '', 4),
(192, 'Sierra Leone', 'SL', '232', '', 4),
(193, 'Slovenia', 'SI', '386', 'C-NNNN', 1),
(194, 'Solomon Islands', 'SB', '677', '', 5),
(195, 'Somalia', 'SO', '252', '', 4),
(196, 'South Georgia and the South Sandwich Islands', 'GS', '0', 'LLLL NLL', 8),
(197, 'Sri Lanka', 'LK', '94', 'NNNNN', 3),
(198, 'Sudan', 'SD', '249', '', 4),
(199, 'Suriname', 'SR', '597', '', 8),
(200, 'Svalbard and Jan Mayen', 'SJ', '0', '', 7),
(201, 'Swaziland', 'SZ', '268', '', 4),
(202, 'Syria', 'SY', '963', '', 3),
(203, 'Taiwan', 'TW', '886', 'NNNNN', 3),
(204, 'Tajikistan', 'TJ', '992', '', 3),
(205, 'Tanzania', 'TZ', '255', '', 4),
(206, 'Thailand', 'TH', '66', 'NNNNN', 3),
(207, 'Tokelau', 'TK', '690', '', 5),
(208, 'Tonga', 'TO', '676', '', 5),
(209, 'Trinidad and Tobago', 'TT', '0', '', 6),
(210, 'Tunisia', 'TN', '216', '', 4),
(211, 'Turkey', 'TR', '90', 'NNNNN', 7),
(212, 'Turkmenistan', 'TM', '993', '', 3),
(213, 'Turks and Caicos Islands', 'TC', '0', 'LLLL NLL', 8),
(214, 'Tuvalu', 'TV', '688', '', 5),
(215, 'Uganda', 'UG', '256', '', 4),
(216, 'Ukraine', 'UA', '380', 'NNNNN', 1),
(217, 'United Arab Emirates', 'AE', '971', '', 3),
(218, 'Uruguay', 'UY', '598', '', 6),
(219, 'Uzbekistan', 'UZ', '998', '', 3),
(220, 'Vanuatu', 'VU', '678', '', 5),
(221, 'Venezuela', 'VE', '58', '', 6),
(222, 'Vietnam', 'VN', '84', 'NNNNNN', 3),
(223, 'Virgin Islands (British)', 'VG', '0', 'CNNNN', 2),
(224, 'Virgin Islands (U.S.)', 'VI', '0', '', 2),
(225, 'Wallis and Futuna', 'WF', '681', '', 5),
(226, 'Western Sahara', 'EH', '0', '', 4),
(227, 'Yemen', 'YE', '967', '', 3),
(228, 'Zambia', 'ZM', '260', '', 4),
(229, 'Zimbabwe', 'ZW', '263', '', 4),
(230, 'Albania', 'AL', '355', 'NNNN', 7),
(231, 'Afghanistan', 'AF', '93', '', 3),
(232, 'Antarctica', 'AQ', '0', '', 5),
(233, 'Bosnia and Herzegovina', 'BA', '387', '', 1),
(234, 'Bouvet Island', 'BV', '0', '', 5),
(235, 'British Indian Ocean Territory', 'IO', '0', 'LLLL NLL', 5),
(236, 'Bulgaria', 'BG', '359', 'NNNN', 1),
(237, 'Cayman Islands', 'KY', '0', '', 8),
(238, 'Christmas Island', 'CX', '0', '', 3),
(239, 'Cocos (Keeling) Islands', 'CC', '0', '', 3),
(240, 'Cook Islands', 'CK', '682', '', 5),
(241, 'French Guiana', 'GF', '594', '', 6),
(242, 'French Polynesia', 'PF', '689', '', 5),
(243, 'French Southern Territories', 'TF', '0', '', 5),
(244, 'Åland Islands', 'AX', '0', 'NNNNN', 7);

-- --------------------------------------------------------

--
-- Table structure for table `stf_dribbling_circular`
--

CREATE TABLE IF NOT EXISTS `stf_dribbling_circular` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_test_time` float NOT NULL,
  `nu_obstacles` int(11) NOT NULL,
  `nu_time_between_obstacles` float NOT NULL,
  `nu_overcome_obstacles` int(11) NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_event` (`id_event`),
  UNIQUE KEY `id_uniq` (`id_uniq`),
  KEY `id_player` (`id_player`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stf_dribbling_circular`
--

INSERT INTO `stf_dribbling_circular` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_test_time`, `nu_obstacles`, `nu_time_between_obstacles`, `nu_overcome_obstacles`, `dt_register`, `bo_status`) VALUES
(1, '586c925fa45dcd', 17, 1, '0000-00-00', 0, 0, 0, 0, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_dribbling_zigzag`
--

CREATE TABLE IF NOT EXISTS `stf_dribbling_zigzag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_right_leg_time_target_one` float NOT NULL,
  `nu_right_leg_time_target_two` float DEFAULT NULL,
  `nu_right_leg_time_target_three` float DEFAULT NULL,
  `nu_right_leg_time_target_four` float DEFAULT NULL,
  `nu_right_leg_time_target_five` float DEFAULT NULL,
  `nu_right_leg_time_target_six` float DEFAULT NULL,
  `nu_left_leg_time_target_one` float NOT NULL,
  `nu_left_leg_time_target_two` float DEFAULT NULL,
  `nu_left_leg_time_target_three` float DEFAULT NULL,
  `nu_left_leg_time_target_four` float DEFAULT NULL,
  `nu_left_leg_time_target_five` float DEFAULT NULL,
  `nu_left_leg_time_target_six` float DEFAULT NULL,
  `nu_evaluated_distance` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_uniq` (`id_uniq`),
  KEY `id_player` (`id_player`),
  KEY `id_event` (`id_event`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stf_dribbling_zigzag`
--

INSERT INTO `stf_dribbling_zigzag` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_right_leg_time_target_one`, `nu_right_leg_time_target_two`, `nu_right_leg_time_target_three`, `nu_right_leg_time_target_four`, `nu_right_leg_time_target_five`, `nu_right_leg_time_target_six`, `nu_left_leg_time_target_one`, `nu_left_leg_time_target_two`, `nu_left_leg_time_target_three`, `nu_left_leg_time_target_four`, `nu_left_leg_time_target_five`, `nu_left_leg_time_target_six`, `nu_evaluated_distance`, `dt_register`, `bo_status`) VALUES
(1, 'c6dc79341be1fd3d258', 17, 1, '0000-00-00', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_event`
--

CREATE TABLE IF NOT EXISTS `stf_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `tx_name` varchar(100) NOT NULL,
  `dt_event` date DEFAULT NULL,
  `id_venue` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `id_measurement` int(11) NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_uniq` (`id_uniq`),
  KEY `id_venue` (`id_venue`),
  KEY `id_team` (`id_team`),
  KEY `id_measurement` (`id_measurement`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stf_event`
--

INSERT INTO `stf_event` (`id`, `id_uniq`, `tx_name`, `dt_event`, `id_venue`, `id_team`, `id_measurement`, `dt_register`, `bo_status`) VALUES
(1, '18063858025718dbdccfc0a', 'Massive Event', '2016-11-30', 2, 22, 1, '2016-04-21 09:55:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_event_configuration`
--

CREATE TABLE IF NOT EXISTS `stf_event_configuration` (
  `id_event` int(11) NOT NULL,
  `id_configuration` int(11) NOT NULL,
  PRIMARY KEY (`id_event`,`id_configuration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_event_configuration`
--

INSERT INTO `stf_event_configuration` (`id_event`, `id_configuration`) VALUES
(1, 2),
(1, 4),
(1, 5),
(1, 10),
(1, 11),
(1, 12),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(2, 8),
(2, 13),
(2, 18);

-- --------------------------------------------------------

--
-- Table structure for table `stf_gender`
--

CREATE TABLE IF NOT EXISTS `stf_gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_name` varchar(100) NOT NULL,
  `tx_abbr` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `stf_gender`
--

INSERT INTO `stf_gender` (`id`, `tx_name`, `tx_abbr`) VALUES
(1, 'Male', 'M'),
(2, 'Female', 'F');

-- --------------------------------------------------------

--
-- Table structure for table `stf_heading_aim`
--

CREATE TABLE IF NOT EXISTS `stf_heading_aim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_request_one` float NOT NULL,
  `nu_destination_one` float NOT NULL,
  `nu_request_two` float NOT NULL,
  `nu_destination_two` float NOT NULL,
  `nu_request_three` float NOT NULL,
  `nu_destination_three` float NOT NULL,
  `nu_request_four` float NOT NULL,
  `nu_destination_four` float NOT NULL,
  `nu_request_five` float NOT NULL,
  `nu_destination_five` float NOT NULL,
  `nu_request_six` float NOT NULL,
  `nu_destination_six` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_uniq` (`id_uniq`),
  KEY `id_player` (`id_player`),
  KEY `id_event` (`id_event`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stf_heading_aim`
--

INSERT INTO `stf_heading_aim` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_request_one`, `nu_destination_one`, `nu_request_two`, `nu_destination_two`, `nu_request_three`, `nu_destination_three`, `nu_request_four`, `nu_destination_four`, `nu_request_five`, `nu_destination_five`, `nu_request_six`, `nu_destination_six`, `dt_register`, `bo_status`) VALUES
(1, '1122000973574ec7959242c', 17, 1, '2016-06-01', 11, 3, 4, 3, 21, 2, 14, 1, 11, 2, 15, 5, '2016-06-01 07:31:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_heading_force`
--

CREATE TABLE IF NOT EXISTS `stf_heading_force` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_distance_one` float NOT NULL,
  `nu_distance_two` float DEFAULT NULL,
  `nu_distance_three` float DEFAULT NULL,
  `nu_head_attempt_one` float NOT NULL,
  `nu_head_attempt_two` float NOT NULL,
  `nu_head_attempt_three` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_uniq` (`id_uniq`),
  KEY `id_player` (`id_player`),
  KEY `id_event` (`id_event`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `stf_heading_force`
--

INSERT INTO `stf_heading_force` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_distance_one`, `nu_distance_two`, `nu_distance_three`, `nu_head_attempt_one`, `nu_head_attempt_two`, `nu_head_attempt_three`, `dt_register`, `bo_status`) VALUES
(2, '12806705555755c1a1619cf', 17, 1, '2016-06-14', 6, 6, 7, 11, 6, 10, '2016-06-06 14:32:01', 1),
(3, '64661895157569aba71adb', 18, 1, '2016-06-07', 9, 8, 6, 7, 6, 5, '2016-06-07 05:58:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_heading_jump`
--

CREATE TABLE IF NOT EXISTS `stf_heading_jump` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_extended_arm_height` float NOT NULL,
  `nu_jump_high_one` float NOT NULL,
  `nu_jump_high_two` float DEFAULT NULL,
  `nu_jump_high_three` float DEFAULT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_uniq` (`id_uniq`),
  KEY `id_player` (`id_player`),
  KEY `id_event` (`id_event`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stf_heading_jump`
--

INSERT INTO `stf_heading_jump` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_extended_arm_height`, `nu_jump_high_one`, `nu_jump_high_two`, `nu_jump_high_three`, `dt_register`, `bo_status`) VALUES
(1, '1043119880574ec596d2ea0', 17, 1, '2016-06-01', 9.1, 5.1, 2.1, 1.1, '2016-06-01 07:23:03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_image`
--

CREATE TABLE IF NOT EXISTS `stf_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_name` varchar(150) NOT NULL,
  `dt_register` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `stf_image`
--

INSERT INTO `stf_image` (`id`, `tx_name`, `dt_register`) VALUES
(26, 'media/players/0e2601f2d25db9d6f4f1793d2de6c633.png', '2016-02-15 11:16:11'),
(27, 'media/players/34d5f03eed56291fa2fc91ca12005edf.jpg', '2016-02-15 11:16:11'),
(35, 'media/players/c570a9536fd529edfe84a2938e58ae22.png', '2016-02-15 13:36:50'),
(36, 'media/players/c632b2dc470e75dd64ee35a7f51b393b.jpg', '2016-02-15 13:36:50'),
(37, 'media/players/651894de74b7305788a018fe1b8e0d54.jpg', '2016-02-15 13:46:30'),
(38, 'media/players/3980c0c6e5963d33b3530dbfbac84e10.jpg', '2016-02-19 15:06:38'),
(39, 'media/players/8394f4d923281f7668d7e0a1ae1bdd70.png', '2016-04-11 15:09:02'),
(40, 'media/players/ed8c6b3defe846da95e8140ecfa26164.png', '2016-04-11 15:21:04'),
(41, 'media/players/c3072cea3117763e3c7ade12760e705e.png', '2016-04-11 15:22:20'),
(42, 'media/players/049db7381349dbaf3aa0eecf784956e9.jpg', '2016-04-11 15:28:48'),
(43, 'media/players/aac9520febb64d9250e99410fce13a92.png', '2016-04-27 10:14:26'),
(44, 'media/players/3e3b66f3b517634b3f9786756b6800d1.png', '2016-04-27 10:20:23');

-- --------------------------------------------------------

--
-- Table structure for table `stf_kicking_aim`
--

CREATE TABLE IF NOT EXISTS `stf_kicking_aim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_right_leg_request_one` float NOT NULL,
  `nu_right_leg_destination_one` float NOT NULL,
  `nu_right_leg_request_two` float NOT NULL,
  `nu_right_leg_destination_two` float NOT NULL,
  `nu_right_leg_request_three` float NOT NULL,
  `nu_right_leg_destination_three` float NOT NULL,
  `nu_left_leg_request_one` float NOT NULL,
  `nu_left_leg_destination_one` float NOT NULL,
  `nu_left_leg_request_two` float NOT NULL,
  `nu_left_leg_destination_two` float NOT NULL,
  `nu_left_leg_request_three` float NOT NULL,
  `nu_left_leg_destination_three` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `id_uniq` (`id_uniq`),
  KEY `id_player` (`id_player`),
  KEY `id_event` (`id_event`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stf_kicking_force`
--

CREATE TABLE IF NOT EXISTS `stf_kicking_force` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_right_leg_attempt_one` float NOT NULL,
  `nu_right_leg_attempt_two` float NOT NULL,
  `nu_right_leg_attempt_three` float NOT NULL,
  `nu_left_leg_attempt_one` float NOT NULL,
  `nu_left_leg_attempt_two` float NOT NULL,
  `nu_left_leg_attempt_three` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_uniq` (`id_uniq`),
  KEY `id_player` (`id_player`),
  KEY `id_event` (`id_event`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `stf_kicking_force`
--

INSERT INTO `stf_kicking_force` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_right_leg_attempt_one`, `nu_right_leg_attempt_two`, `nu_right_leg_attempt_three`, `nu_left_leg_attempt_one`, `nu_left_leg_attempt_two`, `nu_left_leg_attempt_three`, `dt_register`, `bo_status`) VALUES
(1, '370058036574e989e3ff94', 17, 1, '2016-06-01', 6, 5, 2, 1, 1, 3, '2016-06-01 04:11:10', 0),
(2, '2126424650576baa2c5eb39', 18, 1, '2016-06-23', 5, 4, 5, 6, 6, 4, '2016-06-23 05:21:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_measurement`
--

CREATE TABLE IF NOT EXISTS `stf_measurement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_name` varchar(100) NOT NULL,
  `tx_abbr` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `stf_measurement`
--

INSERT INTO `stf_measurement` (`id`, `tx_name`, `tx_abbr`) VALUES
(1, 'Meters Kilometers Seconds', 'MKS'),
(2, 'Yards Pounds Seconds', 'YLS');

-- --------------------------------------------------------

--
-- Table structure for table `stf_movement_lateral`
--

CREATE TABLE IF NOT EXISTS `stf_movement_lateral` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_time_target_one` float NOT NULL,
  `nu_time_target_two` float NOT NULL,
  `nu_time_target_three` float NOT NULL,
  `nu_time_target_four` float NOT NULL,
  `nu_time_target_five` float NOT NULL,
  `nu_time_target_six` float NOT NULL,
  `nu_time_target_seven` float NOT NULL,
  `nu_time_target_eight` float NOT NULL,
  `nu_time_target_nine` float NOT NULL,
  `nu_time_target_ten` float NOT NULL,
  `nu_evaluated_distance` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_uniq` (`id_uniq`),
  KEY `id_player` (`id_player`,`id_event`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stf_movement_linear`
--

CREATE TABLE IF NOT EXISTS `stf_movement_linear` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_time_target_one` float NOT NULL,
  `nu_time_target_two` float NOT NULL,
  `nu_time_target_three` float NOT NULL,
  `nu_time_target_four` float NOT NULL,
  `nu_time_target_five` float NOT NULL,
  `nu_time_target_six` float NOT NULL,
  `nu_time_target_seven` float NOT NULL,
  `nu_time_target_eight` float NOT NULL,
  `nu_time_target_nine` float NOT NULL,
  `nu_time_target_ten` float NOT NULL,
  `nu_evaluated_distance` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_uniq` (`id_uniq`),
  KEY `id_player` (`id_player`,`id_event`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stf_movement_zigzag`
--

CREATE TABLE IF NOT EXISTS `stf_movement_zigzag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_time_target_one` float NOT NULL,
  `nu_time_target_two` float NOT NULL,
  `nu_time_target_three` float NOT NULL,
  `nu_time_target_four` float NOT NULL,
  `nu_time_target_five` float NOT NULL,
  `nu_time_target_six` float NOT NULL,
  `nu_time_target_seven` float NOT NULL,
  `nu_time_target_eight` float NOT NULL,
  `nu_time_target_nine` float NOT NULL,
  `nu_time_target_ten` float NOT NULL,
  `nu_evaluated_distance` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_uniq` (`id_uniq`),
  KEY `id_player` (`id_player`,`id_event`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stf_player`
--

CREATE TABLE IF NOT EXISTS `stf_player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `tx_firstname` varchar(100) NOT NULL,
  `tx_secondname` varchar(100) DEFAULT NULL,
  `tx_lastname` varchar(100) NOT NULL,
  `tx_secondlastname` varchar(100) DEFAULT NULL,
  `dt_birthdate` date NOT NULL,
  `id_gender` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `tx_address` varchar(200) DEFAULT NULL,
  `tx_zipcode` varchar(50) DEFAULT NULL,
  `tx_phone_prefix` varchar(10) DEFAULT NULL,
  `tx_phone` varchar(50) DEFAULT NULL,
  `tx_email` varchar(150) NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_stf_player_id_uniq` (`id_uniq`),
  KEY `idx_stf_player_id_gender` (`id_gender`),
  KEY `idx_stf_player_id_country` (`id_country`),
  KEY `idx_stf_player_id_state` (`id_state`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `stf_player`
--

INSERT INTO `stf_player` (`id`, `id_uniq`, `tx_firstname`, `tx_secondname`, `tx_lastname`, `tx_secondlastname`, `dt_birthdate`, `id_gender`, `id_country`, `id_state`, `tx_address`, `tx_zipcode`, `tx_phone_prefix`, `tx_phone`, `tx_email`, `dt_register`, `bo_status`) VALUES
(17, '19623349115720c9420739e', 'Fernando', '', 'Torres', '', '2003-11-26', 1, 21, 2, '418 King Street, Nile', '26999', '1', '8013788399', 'fernandotorres@gmail.com', '2016-04-27 10:14:26', 1),
(18, '16265186455756775cac653', 'Christiano', '', 'Ronaldo', '', '2005-06-08', 1, 21, 1, '1/322 Kapasdanga Colony, Palpara, Po And Dist. Hooghly', '71210', '1', '8013788399', 'crhristiano.ronaldo@gmail.com', '2016-06-07 03:27:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_player_image`
--

CREATE TABLE IF NOT EXISTS `stf_player_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_player` int(11) NOT NULL,
  `id_image` int(11) NOT NULL,
  `id_type_image` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_stf_player_image_id_player` (`id_player`),
  KEY `idx_stf_player_image_id_image` (`id_image`) USING BTREE,
  KEY `idx_stf_player_image_id_type_image` (`id_type_image`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `stf_player_image`
--

INSERT INTO `stf_player_image` (`id`, `id_player`, `id_image`, `id_type_image`) VALUES
(39, 17, 43, 1),
(40, 17, 44, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_position`
--

CREATE TABLE IF NOT EXISTS `stf_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_name` varchar(100) NOT NULL,
  `tx_abbr` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `stf_position`
--

INSERT INTO `stf_position` (`id`, `tx_name`, `tx_abbr`) VALUES
(1, 'Goalkeeper', 'GK'),
(2, 'Centre Back', 'CB'),
(3, 'Sweeper', 'SW'),
(4, 'Left Back', 'LB'),
(5, 'Right Back', 'RB'),
(6, 'Left Wing Back', 'LWB'),
(7, 'Right Wing Back', 'RWB'),
(8, 'Centre Midfield', 'CM'),
(9, 'Defensive Midfield', 'DM'),
(10, 'Attacking Midfield', 'AM'),
(11, 'Left Midfield', 'LM'),
(12, 'Right Midfield', 'RM'),
(13, 'Centre Forward', 'CF'),
(14, 'Second Striker', 'SS'),
(15, 'Left Winger', 'LW'),
(16, 'Right Winger', 'RW');

-- --------------------------------------------------------

--
-- Table structure for table `stf_section`
--

CREATE TABLE IF NOT EXISTS `stf_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_name` varchar(100) DEFAULT NULL,
  `section_link` varchar(20) DEFAULT NULL,
  `section_icon` varchar(10) NOT NULL DEFAULT 'cube',
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `stf_section`
--

INSERT INTO `stf_section` (`id`, `section_name`, `section_link`, `section_icon`, `bo_status`) VALUES
(1, 'Events', 'events', 'diamond', 1),
(2, 'Players', 'players', 'diamond', 1),
(3, 'Teams', 'teams', 'diamond', 1),
(4, 'Venues', 'venues', 'globe', 1),
(5, 'Users', 'users', 'users', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_state`
--

CREATE TABLE IF NOT EXISTS `stf_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_name` varchar(100) NOT NULL,
  `tx_iso_code` varchar(10) NOT NULL,
  `id_country` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_stf_state_id_country` (`id_country`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=313 ;

--
-- Dumping data for table `stf_state`
--

INSERT INTO `stf_state` (`id`, `tx_name`, `tx_iso_code`, `id_country`) VALUES
(1, 'Alabama', 'AL', 21),
(2, 'Alaska', 'AK', 21),
(3, 'Arizona', 'AZ', 21),
(4, 'Arkansas', 'AR', 21),
(5, 'California', 'CA', 21),
(6, 'Colorado', 'CO', 21),
(7, 'Connecticut', 'CT', 21),
(8, 'Delaware', 'DE', 21),
(9, 'Florida', 'FL', 21),
(10, 'Georgia', 'GA', 21),
(11, 'Hawaii', 'HI', 21),
(12, 'Idaho', 'ID', 21),
(13, 'Illinois', 'IL', 21),
(14, 'Indiana', 'IN', 21),
(15, 'Iowa', 'IA', 21),
(16, 'Kansas', 'KS', 21),
(17, 'Kentucky', 'KY', 21),
(18, 'Louisiana', 'LA', 21),
(19, 'Maine', 'ME', 21),
(20, 'Maryland', 'MD', 21),
(21, 'Massachusetts', 'MA', 21),
(22, 'Michigan', 'MI', 21),
(23, 'Minnesota', 'MN', 21),
(24, 'Mississippi', 'MS', 21),
(25, 'Missouri', 'MO', 21),
(26, 'Montana', 'MT', 21),
(27, 'Nebraska', 'NE', 21),
(28, 'Nevada', 'NV', 21),
(29, 'New Hampshire', 'NH', 21),
(30, 'New Jersey', 'NJ', 21),
(31, 'New Mexico', 'NM', 21),
(32, 'New York', 'NY', 21),
(33, 'North Carolina', 'NC', 21),
(34, 'North Dakota', 'ND', 21),
(35, 'Ohio', 'OH', 21),
(36, 'Oklahoma', 'OK', 21),
(37, 'Oregon', 'OR', 21),
(38, 'Pennsylvania', 'PA', 21),
(39, 'Rhode Island', 'RI', 21),
(40, 'South Carolina', 'SC', 21),
(41, 'South Dakota', 'SD', 21),
(42, 'Tennessee', 'TN', 21),
(43, 'Texas', 'TX', 21),
(44, 'Utah', 'UT', 21),
(45, 'Vermont', 'VT', 21),
(46, 'Virginia', 'VA', 21),
(47, 'Washington', 'WA', 21),
(48, 'West Virginia', 'WV', 21),
(49, 'Wisconsin', 'WI', 21),
(50, 'Wyoming', 'WY', 21),
(51, 'Puerto Rico', 'PR', 21),
(52, 'US Virgin Islands', 'VI', 21),
(53, 'District of Columbia', 'DC', 21),
(54, 'Aguascalientes', 'AGS', 145),
(55, 'Baja California', 'BCN', 145),
(56, 'Baja California Sur', 'BCS', 145),
(57, 'Campeche', 'CAM', 145),
(58, 'Chiapas', 'CHP', 145),
(59, 'Chihuahua', 'CHH', 145),
(60, 'Coahuila', 'COA', 145),
(61, 'Colima', 'COL', 145),
(62, 'Distrito Federal', 'DIF', 145),
(63, 'Durango', 'DUR', 145),
(64, 'Guanajuato', 'GUA', 145),
(65, 'Guerrero', 'GRO', 145),
(66, 'Hidalgo', 'HID', 145),
(67, 'Jalisco', 'JAL', 145),
(68, 'Estado de México', 'MEX', 145),
(69, 'Michoacán', 'MIC', 145),
(70, 'Morelos', 'MOR', 145),
(71, 'Nayarit', 'NAY', 145),
(72, 'Nuevo León', 'NLE', 145),
(73, 'Oaxaca', 'OAX', 145),
(74, 'Puebla', 'PUE', 145),
(75, 'Querétaro', 'QUE', 145),
(76, 'Quintana Roo', 'ROO', 145),
(77, 'San Luis Potosí', 'SLP', 145),
(78, 'Sinaloa', 'SIN', 145),
(79, 'Sonora', 'SON', 145),
(80, 'Tabasco', 'TAB', 145),
(81, 'Tamaulipas', 'TAM', 145),
(82, 'Tlaxcala', 'TLA', 145),
(83, 'Veracruz', 'VER', 145),
(84, 'Yucatán', 'YUC', 145),
(85, 'Zacatecas', 'ZAC', 145),
(86, 'Ontario', 'ON', 4),
(87, 'Quebec', 'QC', 4),
(88, 'British Columbia', 'BC', 4),
(89, 'Alberta', 'AB', 4),
(90, 'Manitoba', 'MB', 4),
(91, 'Saskatchewan', 'SK', 4),
(92, 'Nova Scotia', 'NS', 4),
(93, 'New Brunswick', 'NB', 4),
(94, 'Newfoundland and Labrador', 'NL', 4),
(95, 'Prince Edward Island', 'PE', 4),
(96, 'Northwest Territories', 'NT', 4),
(97, 'Yukon', 'YT', 4),
(98, 'Nunavut', 'NU', 4),
(99, 'Buenos Aires', 'B', 44),
(100, 'Catamarca', 'K', 44),
(101, 'Chaco', 'H', 44),
(102, 'Chubut', 'U', 44),
(103, 'Ciudad de Buenos Aires', 'C', 44),
(104, 'Córdoba', 'X', 44),
(105, 'Corrientes', 'W', 44),
(106, 'Entre Ríos', 'E', 44),
(107, 'Formosa', 'P', 44),
(108, 'Jujuy', 'Y', 44),
(109, 'La Pampa', 'L', 44),
(110, 'La Rioja', 'F', 44),
(111, 'Mendoza', 'M', 44),
(112, 'Misiones', 'N', 44),
(113, 'Neuquén', 'Q', 44),
(114, 'Río Negro', 'R', 44),
(115, 'Salta', 'A', 44),
(116, 'San Juan', 'J', 44),
(117, 'San Luis', 'D', 44),
(118, 'Santa Cruz', 'Z', 44),
(119, 'Santa Fe', 'S', 44),
(120, 'Santiago del Estero', 'G', 44),
(121, 'Tierra del Fuego', 'V', 44),
(122, 'Tucumán', 'T', 44),
(123, 'Agrigento', 'AG', 10),
(124, 'Alessandria', 'AL', 10),
(125, 'Ancona', 'AN', 10),
(126, 'Aosta', 'AO', 10),
(127, 'Arezzo', 'AR', 10),
(128, 'Ascoli Piceno', 'AP', 10),
(129, 'Asti', 'AT', 10),
(130, 'Avellino', 'AV', 10),
(131, 'Bari', 'BA', 10),
(132, 'Barletta-Andria-Trani', 'BT', 10),
(133, 'Belluno', 'BL', 10),
(134, 'Benevento', 'BN', 10),
(135, 'Bergamo', 'BG', 10),
(136, 'Biella', 'BI', 10),
(137, 'Bologna', 'BO', 10),
(138, 'Bolzano', 'BZ', 10),
(139, 'Brescia', 'BS', 10),
(140, 'Brindisi', 'BR', 10),
(141, 'Cagliari', 'CA', 10),
(142, 'Caltanissetta', 'CL', 10),
(143, 'Campobasso', 'CB', 10),
(144, 'Carbonia-Iglesias', 'CI', 10),
(145, 'Caserta', 'CE', 10),
(146, 'Catania', 'CT', 10),
(147, 'Catanzaro', 'CZ', 10),
(148, 'Chieti', 'CH', 10),
(149, 'Como', 'CO', 10),
(150, 'Cosenza', 'CS', 10),
(151, 'Cremona', 'CR', 10),
(152, 'Crotone', 'KR', 10),
(153, 'Cuneo', 'CN', 10),
(154, 'Enna', 'EN', 10),
(155, 'Fermo', 'FM', 10),
(156, 'Ferrara', 'FE', 10),
(157, 'Firenze', 'FI', 10),
(158, 'Foggia', 'FG', 10),
(159, 'Forlì-Cesena', 'FC', 10),
(160, 'Frosinone', 'FR', 10),
(161, 'Genova', 'GE', 10),
(162, 'Gorizia', 'GO', 10),
(163, 'Grosseto', 'GR', 10),
(164, 'Imperia', 'IM', 10),
(165, 'Isernia', 'IS', 10),
(166, 'L''Aquila', 'AQ', 10),
(167, 'La Spezia', 'SP', 10),
(168, 'Latina', 'LT', 10),
(169, 'Lecce', 'LE', 10),
(170, 'Lecco', 'LC', 10),
(171, 'Livorno', 'LI', 10),
(172, 'Lodi', 'LO', 10),
(173, 'Lucca', 'LU', 10),
(174, 'Macerata', 'MC', 10),
(175, 'Mantova', 'MN', 10),
(176, 'Massa', 'MS', 10),
(177, 'Matera', 'MT', 10),
(178, 'Medio Campidano', 'VS', 10),
(179, 'Messina', 'ME', 10),
(180, 'Milano', 'MI', 10),
(181, 'Modena', 'MO', 10),
(182, 'Monza e della Brianza', 'MB', 10),
(183, 'Napoli', 'NA', 10),
(184, 'Novara', 'NO', 10),
(185, 'Nuoro', 'NU', 10),
(186, 'Ogliastra', 'OG', 10),
(187, 'Olbia-Tempio', 'OT', 10),
(188, 'Oristano', 'OR', 10),
(189, 'Padova', 'PD', 10),
(190, 'Palermo', 'PA', 10),
(191, 'Parma', 'PR', 10),
(192, 'Pavia', 'PV', 10),
(193, 'Perugia', 'PG', 10),
(194, 'Pesaro-Urbino', 'PU', 10),
(195, 'Pescara', 'PE', 10),
(196, 'Piacenza', 'PC', 10),
(197, 'Pisa', 'PI', 10),
(198, 'Pistoia', 'PT', 10),
(199, 'Pordenone', 'PN', 10),
(200, 'Potenza', 'PZ', 10),
(201, 'Prato', 'PO', 10),
(202, 'Ragusa', 'RG', 10),
(203, 'Ravenna', 'RA', 10),
(204, 'Reggio Calabria', 'RC', 10),
(205, 'Reggio Emilia', 'RE', 10),
(206, 'Rieti', 'RI', 10),
(207, 'Rimini', 'RN', 10),
(208, 'Roma', 'RM', 10),
(209, 'Rovigo', 'RO', 10),
(210, 'Salerno', 'SA', 10),
(211, 'Sassari', 'SS', 10),
(212, 'Savona', 'SV', 10),
(213, 'Siena', 'SI', 10),
(214, 'Siracusa', 'SR', 10),
(215, 'Sondrio', 'SO', 10),
(216, 'Taranto', 'TA', 10),
(217, 'Teramo', 'TE', 10),
(218, 'Terni', 'TR', 10),
(219, 'Torino', 'TO', 10),
(220, 'Trapani', 'TP', 10),
(221, 'Trento', 'TN', 10),
(222, 'Treviso', 'TV', 10),
(223, 'Trieste', 'TS', 10),
(224, 'Udine', 'UD', 10),
(225, 'Varese', 'VA', 10),
(226, 'Venezia', 'VE', 10),
(227, 'Verbano-Cusio-Ossola', 'VB', 10),
(228, 'Vercelli', 'VC', 10),
(229, 'Verona', 'VR', 10),
(230, 'Vibo Valentia', 'VV', 10),
(231, 'Vicenza', 'VI', 10),
(232, 'Viterbo', 'VT', 10),
(233, 'Aceh', 'AC', 111),
(234, 'Bali', 'BA', 111),
(235, 'Bangka', 'BB', 111),
(236, 'Banten', 'BT', 111),
(237, 'Bengkulu', 'BE', 111),
(238, 'Central Java', 'JT', 111),
(239, 'Central Kalimantan', 'KT', 111),
(240, 'Central Sulawesi', 'ST', 111),
(241, 'Coat of arms of East Java', 'JI', 111),
(242, 'East kalimantan', 'KI', 111),
(243, 'East Nusa Tenggara', 'NT', 111),
(244, 'Lambang propinsi', 'GO', 111),
(245, 'Jakarta', 'JK', 111),
(246, 'Jambi', 'JA', 111),
(247, 'Lampung', 'LA', 111),
(248, 'Maluku', 'MA', 111),
(249, 'North Maluku', 'MU', 111),
(250, 'North Sulawesi', 'SA', 111),
(251, 'North Sumatra', 'SU', 111),
(252, 'Papua', 'PA', 111),
(253, 'Riau', 'RI', 111),
(254, 'Lambang Riau', 'KR', 111),
(255, 'Southeast Sulawesi', 'SG', 111),
(256, 'South Kalimantan', 'KS', 111),
(257, 'South Sulawesi', 'SN', 111),
(258, 'South Sumatra', 'SS', 111),
(259, 'West Java', 'JB', 111),
(260, 'West Kalimantan', 'KB', 111),
(261, 'West Nusa Tenggara', 'NB', 111),
(262, 'Lambang Provinsi Papua Barat', 'PB', 111),
(263, 'West Sulawesi', 'SR', 111),
(264, 'West Sumatra', 'SB', 111),
(265, 'Yogyakarta', 'YO', 111),
(266, 'Aichi', '23', 11),
(267, 'Akita', '05', 11),
(268, 'Aomori', '02', 11),
(269, 'Chiba', '12', 11),
(270, 'Ehime', '38', 11),
(271, 'Fukui', '18', 11),
(272, 'Fukuoka', '40', 11),
(273, 'Fukushima', '07', 11),
(274, 'Gifu', '21', 11),
(275, 'Gunma', '10', 11),
(276, 'Hiroshima', '34', 11),
(277, 'Hokkaido', '01', 11),
(278, 'Hyogo', '28', 11),
(279, 'Ibaraki', '08', 11),
(280, 'Ishikawa', '17', 11),
(281, 'Iwate', '03', 11),
(282, 'Kagawa', '37', 11),
(283, 'Kagoshima', '46', 11),
(284, 'Kanagawa', '14', 11),
(285, 'Kochi', '39', 11),
(286, 'Kumamoto', '43', 11),
(287, 'Kyoto', '26', 11),
(288, 'Mie', '24', 11),
(289, 'Miyagi', '04', 11),
(290, 'Miyazaki', '45', 11),
(291, 'Nagano', '20', 11),
(292, 'Nagasaki', '42', 11),
(293, 'Nara', '29', 11),
(294, 'Niigata', '15', 11),
(295, 'Oita', '44', 11),
(296, 'Okayama', '33', 11),
(297, 'Okinawa', '47', 11),
(298, 'Osaka', '27', 11),
(299, 'Saga', '41', 11),
(300, 'Saitama', '11', 11),
(301, 'Shiga', '25', 11),
(302, 'Shimane', '32', 11),
(303, 'Shizuoka', '22', 11),
(304, 'Tochigi', '09', 11),
(305, 'Tokushima', '36', 11),
(306, 'Tokyo', '13', 11),
(307, 'Tottori', '31', 11),
(308, 'Toyama', '16', 11),
(309, 'Wakayama', '30', 11),
(310, 'Yamagata', '06', 11),
(311, 'Yamaguchi', '35', 11),
(312, 'Yamanashi', '19', 11);

-- --------------------------------------------------------

--
-- Table structure for table `stf_station`
--

CREATE TABLE IF NOT EXISTS `stf_station` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `tx_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_uniq` (`id_uniq`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `stf_station`
--

INSERT INTO `stf_station` (`id`, `id_uniq`, `tx_name`) VALUES
(1, '141944456056671492caaa9', 'Anthropometrics'),
(2, '18727680295667144015046', 'Dribbling'),
(3, '1465975612566714511e954', 'Kicking'),
(4, '16223341295667145aefa87', 'Movement'),
(5, '19400868405667146cbf54e', 'Heading');

-- --------------------------------------------------------

--
-- Table structure for table `stf_station_configuration`
--

CREATE TABLE IF NOT EXISTS `stf_station_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_station` int(11) NOT NULL,
  `id_test` int(11) NOT NULL,
  `id_age_range` int(11) NOT NULL,
  `nu_distance` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `stf_station_configuration`
--

INSERT INTO `stf_station_configuration` (`id`, `id_station`, `id_test`, `id_age_range`, `nu_distance`) VALUES
(2, 3, 1, 3, 15),
(3, 4, 5, 3, 35),
(5, 5, 7, 2, 5),
(6, 5, 3, 3, 25),
(7, 4, 4, 1, 10),
(8, 5, 1, 3, 20),
(9, 3, 8, 3, 30),
(10, 3, 1, 2, 10),
(11, 3, 8, 2, 20),
(12, 3, 8, 1, 10),
(13, 5, 7, 3, 10),
(14, 5, 1, 2, 13),
(15, 4, 4, 2, 10),
(16, 4, 5, 2, 10),
(17, 4, 6, 2, 10),
(18, 5, 3, 2, 20);

-- --------------------------------------------------------

--
-- Table structure for table `stf_station_test`
--

CREATE TABLE IF NOT EXISTS `stf_station_test` (
  `id_station` int(11) NOT NULL,
  `id_test` int(11) NOT NULL,
  PRIMARY KEY (`id_station`,`id_test`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_station_test`
--

INSERT INTO `stf_station_test` (`id_station`, `id_test`) VALUES
(3, 1),
(3, 8),
(4, 4),
(4, 5),
(4, 6),
(5, 1),
(5, 3),
(5, 7);

-- --------------------------------------------------------

--
-- Table structure for table `stf_team`
--

CREATE TABLE IF NOT EXISTS `stf_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `tx_name` varchar(100) NOT NULL,
  `tx_image` varchar(100) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `tx_address` varchar(200) DEFAULT NULL,
  `tx_zipcode` varchar(50) DEFAULT NULL,
  `tx_phone_prefix` varchar(10) DEFAULT NULL,
  `tx_phone` varchar(50) DEFAULT NULL,
  `tx_email` varchar(100) NOT NULL,
  `tx_website` varchar(150) DEFAULT NULL,
  `tx_facebook` varchar(150) DEFAULT NULL,
  `tx_twitter` varchar(150) DEFAULT NULL,
  `tx_instagram` varchar(150) DEFAULT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_stf_team_id_uniq` (`id_uniq`),
  KEY `idx_stf_team_id_state` (`id_state`) USING BTREE,
  KEY `idx_stf_team_id_country` (`id_country`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `stf_team`
--

INSERT INTO `stf_team` (`id`, `id_uniq`, `tx_name`, `tx_image`, `id_country`, `id_state`, `tx_address`, `tx_zipcode`, `tx_phone_prefix`, `tx_phone`, `tx_email`, `tx_website`, `tx_facebook`, `tx_twitter`, `tx_instagram`, `dt_register`, `bo_status`) VALUES
(19, '31568110256b22b52e6034', 'Real Madrid Fc', 'media/teams/9d581ad23bd4af980872f317e0187786.png', 21, 1, '607 Prescott Place, Sunwest', '80333', '', '', 'realmadrid@gmail.com', '', '', '', '', '2016-02-03 11:31:00', 1),
(20, '23170036456b25c1562431', 'Manchester United', 'media/teams/28e235196bb73b8b4fe44b880098be2e.png', 21, 4, '', '', '', '', 'manchester@gmail.com', '', '', '', '', '2016-02-03 14:59:00', 1),
(21, '147868628256b354a11714c', 'Barcelona Fc', 'media/teams/81696c10a7ce1d4e1b8bd7257114e111.png', 21, 16, 'Usa', '75678', '', '', 'barcelona@gmail.com', '', '', '', '', '2016-02-04 08:39:00', 1),
(22, '207350296756b3554e07244', 'Atlético De Madrid', 'media/teams/c44a8a651ffd7a7f439d6635674dd61d.jpg', 21, 5, '1/322 Kapasdanga Colony, Palpara, Po And Dist. Hooghly', '71210', '1', '8013788399', 'atletico1@gmail.com', 'google.com', '', '', '', '2016-02-04 08:42:00', 1),
(23, '708305226570beeb2e9b2b', 'Bayern Munich', 'media/teams/56d827e721e47a3bc0cad6e7c2b61baa.png', 21, 27, '887 Middleton Street, Rehrersburg', '50344', '', '', 'bayern@gmail.com', 'https://www.fcbayern.de/es/', '', '', '', '2016-04-11 14:36:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_team_category`
--

CREATE TABLE IF NOT EXISTS `stf_team_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_team` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_gender` int(11) NOT NULL,
  `nu_player` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_stf_team_category_id_category` (`id_category`),
  KEY `idx_stf_team_category_id_gender` (`id_gender`),
  KEY `idx_stf_team_category_id_team` (`id_team`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `stf_team_category`
--

INSERT INTO `stf_team_category` (`id`, `id_team`, `id_category`, `id_gender`, `nu_player`) VALUES
(1, 21, 4, 2, 11),
(2, 21, 5, 1, 11),
(3, 21, 7, 1, 11),
(8, 22, 1, 1, 10),
(9, 22, 2, 1, 10),
(10, 22, 2, 2, 10),
(11, 22, 1, 2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `stf_team_player`
--

CREATE TABLE IF NOT EXISTS `stf_team_player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_team` int(11) NOT NULL,
  `id_player` int(11) NOT NULL,
  `nu_dorsal` int(11) DEFAULT NULL,
  `dt_admission` date NOT NULL,
  `dt_discharge` date DEFAULT NULL,
  `dt_register` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_stf_team_player_id_team` (`id_team`),
  KEY `idx_stf_team_player_id_player` (`id_player`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `stf_team_player`
--

INSERT INTO `stf_team_player` (`id`, `id_team`, `id_player`, `nu_dorsal`, `dt_admission`, `dt_discharge`, `dt_register`) VALUES
(5, 22, 17, 10, '2012-07-30', NULL, '2016-04-27 10:22:38'),
(6, 22, 18, 3, '2016-06-01', '2016-06-07', '2016-06-07 03:29:46');

-- --------------------------------------------------------

--
-- Table structure for table `stf_team_player_position`
--

CREATE TABLE IF NOT EXISTS `stf_team_player_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_team_player` int(11) NOT NULL,
  `id_position` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_stf_team_player_position_id_team_player` (`id_team_player`),
  KEY `idx_stf_team_player_position_id_position` (`id_position`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `stf_team_player_position`
--

INSERT INTO `stf_team_player_position` (`id`, `id_team_player`, `id_position`) VALUES
(59, 5, 7),
(60, 6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `stf_test`
--

CREATE TABLE IF NOT EXISTS `stf_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `stf_test`
--

INSERT INTO `stf_test` (`id`, `tx_name`) VALUES
(1, 'Aim'),
(2, 'Circular'),
(3, 'Force'),
(4, 'Lateral'),
(5, 'Zigzag'),
(6, 'Lineal'),
(7, 'Jump'),
(8, 'Power');

-- --------------------------------------------------------

--
-- Table structure for table `stf_type_image`
--

CREATE TABLE IF NOT EXISTS `stf_type_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `stf_type_image`
--

INSERT INTO `stf_type_image` (`id`, `tx_name`) VALUES
(1, 'Head Picture'),
(2, 'Body Picture');

-- --------------------------------------------------------

--
-- Table structure for table `stf_user`
--

CREATE TABLE IF NOT EXISTS `stf_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `tx_firstname` varchar(100) NOT NULL,
  `tx_lastname` varchar(100) NOT NULL,
  `tx_email` varchar(150) NOT NULL,
  `tx_password` varchar(64) NOT NULL,
  `tx_image` varchar(100) NOT NULL,
  `dt_register` datetime NOT NULL,
  `tx_security_key` varchar(40) NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_stf_user_id_uniq` (`id_uniq`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `stf_user`
--

INSERT INTO `stf_user` (`id`, `id_uniq`, `tx_firstname`, `tx_lastname`, `tx_email`, `tx_password`, `tx_image`, `dt_register`, `tx_security_key`, `bo_status`) VALUES
(2, '19467925645632498738fda', 'Arturo', 'Guillén', 'arturo.guillen@360creativestudio.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2015-10-29 12:29:59', 'c2d6421ea04d3e8a6a5eca3947ed82e49de18570', 1),
(3, '1892336874563249e03ef47', 'Jorge', 'Paredes', 'jorge.paredes@360creativestudio.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2015-10-29 12:31:28', 'f241c4286241ab8da75c4fd9c42699f51a575752', 1),
(4, '1987186358563255dc897db', 'Manuel', 'Alvis', 'manuel.alvis@360creativestudio.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2015-10-29 13:22:36', '6dc0aa7aaf46dceb99abf896406252f4d8e5f555', 1),
(10, '121380482565c76d44ee30', 'Omar', 'Milano', 'omarmilano@test.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2015-11-30 11:18:28', '74081e9c147ff90630629ff2e42be8113109a24f', 1),
(11, '523134342570bca198c9d6', 'Lewis', 'Mooney', 'lewismooney@musanpoly.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/users/ba04aa66af8240b34bf35b08f6c13e4c.jpg', '2016-04-11 12:00:25', '986dc480bfc24cb3994af6104bf59aa8a29034ad', 1),
(12, '59298983570bcded7c23a', 'Leah', 'Decker', 'leahdecker@slumberia.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/users/a49366e435954b2f5cb39d8c6fc1201f.jpg', '2016-04-11 12:16:45', 'd4c18362d8cdf51653aa9d965bb9349bfef54c37', 1),
(13, '12973706565748bc462a8a0', 'Test', 'User', 'testuser@360creativestudio.com', 'e70844b23a08357f83b8c247b8d2f99eb20de5980df2159d790d446c044d7979', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-05-27 17:29:42', '55c6dde21fdcbef75016cf7c18a90d9163f86449', 1),
(15, '1674824192575a256409ffd', 'Sdffsad', 'Saffsasafaf', 'bireshwar.goswami@gmail.com', '947e070b0884726d7a8723b07536d7ae5d84004951ad45f9110b8184441cfe42', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-06-09 22:26:44', 'a1ae97a2c7dc0cdb0ce6ab452927f333af440b93', 1),
(16, '412457104575a577adee0a', 'Bireshwar', 'Goswami', 'bireshwar@capitalnumbers.com', 'e70844b23a08357f83b8c247b8d2f99eb20de5980df2159d790d446c044d7979', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-06-10 02:00:26', '55c6dde21fdcbef75016cf7c18a90d9163f86449', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_venue`
--

CREATE TABLE IF NOT EXISTS `stf_venue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_uniq` varchar(100) NOT NULL,
  `tx_name` varchar(100) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `id_city` varchar(50) DEFAULT NULL,
  `tx_address` varchar(200) DEFAULT NULL,
  `tx_zipcode` varchar(50) DEFAULT NULL,
  `tx_phone_prefix` varchar(10) NOT NULL,
  `tx_phone` varchar(50) NOT NULL,
  `tx_phone_prefix_2` varchar(10) DEFAULT NULL,
  `tx_phone_2` varchar(50) DEFAULT NULL,
  `tx_email` varchar(150) NOT NULL,
  `tx_website` varchar(150) DEFAULT NULL,
  `tx_facebook` varchar(150) DEFAULT NULL,
  `tx_twitter` varchar(150) DEFAULT NULL,
  `tx_instagram` varchar(150) DEFAULT NULL,
  `tx_contact_name` varchar(100) NOT NULL,
  `tx_contact_phone_prefix` varchar(10) NOT NULL,
  `tx_contact_phone` varchar(100) NOT NULL,
  `tx_latitude` varchar(50) NOT NULL,
  `tx_longitude` varchar(50) NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_stf_venue_id_country` (`id_country`),
  KEY `idx_stf_venue_id_state` (`id_state`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `stf_venue`
--

INSERT INTO `stf_venue` (`id`, `id_uniq`, `tx_name`, `id_country`, `id_state`, `id_city`, `tx_address`, `tx_zipcode`, `tx_phone_prefix`, `tx_phone`, `tx_phone_prefix_2`, `tx_phone_2`, `tx_email`, `tx_website`, `tx_facebook`, `tx_twitter`, `tx_instagram`, `tx_contact_name`, `tx_contact_phone_prefix`, `tx_contact_phone`, `tx_latitude`, `tx_longitude`, `dt_register`, `bo_status`) VALUES
(1, '193022567756c5d68d116bf', 'At&t Stadium', 21, 43, NULL, '1 At&t Way, Arlington', '76011', '1', '8178924000', '', '', 'stadium@stadium.dallascowboys.com', 'http://stadium.dallascowboys.com/', '', '', '', 'Kline Young', '1', '9695342074', '32.7472844', '-97.09449389999997', '2016-02-18 09:34:53', 1),
(2, '1084983436570d104037e86', 'Michigan Stadium', 21, 22, NULL, '1201 South Main Street Ann Arbor', '48104', '1', '7346472583', '', '', 'stadium@mgoblue.com', 'http://www.mgoblue.com/facilities/michigan-stadium.html', '', '', '', 'Tiffany Vasquez', '1', '9685363822', '42.2658365', '-83.74869560000002', '2016-04-12 11:12:00', 1),
(3, '2088812811576b9c4ee1d32', 'Abc Stadium', 21, 1, 'XYZA', 'Xyz', '98765', '1', '1234567777', '1', '1222222228', 'abc@gmail.com', 'http://google.com', 'http://facebook.com', 'http://twitter.com', 'http://instagram.com', 'Bireshwar Goswami', '1', '8013788399', '40.148237', '-101.7625', '2016-06-23 04:22:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_zone`
--

CREATE TABLE IF NOT EXISTS `stf_zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tx_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `stf_zone`
--

INSERT INTO `stf_zone` (`id`, `tx_name`) VALUES
(1, 'Europe'),
(2, 'North America'),
(3, 'Asia'),
(4, 'Africa'),
(5, 'Oceania'),
(6, 'South America'),
(7, 'Europe (non-EU)'),
(8, 'Central America/Antilla');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
