-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1build0.15.04.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 11, 2016 at 10:58 AM
-- Server version: 5.6.28-0ubuntu0.15.04.1
-- PHP Version: 5.6.4-4ubuntu6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `soccer_talent_finder_local`
--

-- --------------------------------------------------------

--
-- Table structure for table `stf_access`
--

CREATE TABLE IF NOT EXISTS `stf_access` (
`id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_section` int(11) NOT NULL,
  `bo_view` tinyint(1) NOT NULL DEFAULT '1',
  `bo_add` tinyint(1) NOT NULL DEFAULT '0',
  `bo_edit` tinyint(1) NOT NULL DEFAULT '0',
  `bo_delete` tinyint(1) NOT NULL DEFAULT '0',
  `bo_admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=428 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_access`
--

INSERT INTO `stf_access` (`id`, `id_user`, `id_section`, `bo_view`, `bo_add`, `bo_edit`, `bo_delete`, `bo_admin`) VALUES
(189, 16, 1, 1, 1, 0, 0, 0),
(190, 16, 2, 0, 0, 0, 0, 0),
(191, 16, 3, 1, 0, 1, 0, 0),
(192, 16, 4, 1, 0, 1, 1, 0),
(193, 16, 5, 0, 0, 0, 0, 0),
(194, 2, 1, 1, 0, 0, 0, 0),
(195, 2, 2, 1, 0, 0, 0, 0),
(196, 2, 3, 1, 0, 0, 0, 0),
(197, 2, 4, 1, 0, 0, 0, 0),
(198, 2, 5, 1, 0, 0, 0, 0),
(199, 12, 1, 1, 0, 0, 0, 0),
(200, 12, 2, 1, 0, 0, 0, 0),
(201, 12, 3, 1, 0, 0, 0, 0),
(202, 12, 4, 1, 0, 0, 0, 0),
(203, 12, 5, 1, 1, 1, 1, 0),
(212, 3, 1, 1, 1, 1, 1, 1),
(213, 3, 2, 1, 1, 1, 1, 1),
(214, 3, 3, 1, 1, 1, 1, 1),
(215, 3, 4, 1, 1, 1, 1, 1),
(216, 3, 5, 1, 1, 1, 1, 1),
(217, 4, 1, 1, 1, 1, 1, 1),
(218, 4, 2, 1, 1, 1, 1, 1),
(219, 4, 3, 1, 1, 1, 1, 1),
(220, 4, 4, 1, 1, 1, 1, 1),
(221, 4, 5, 1, 1, 1, 1, 1),
(222, 13, 1, 1, 1, 1, 1, 1),
(223, 13, 2, 1, 1, 1, 1, 1),
(224, 13, 3, 1, 1, 1, 1, 1),
(225, 13, 4, 1, 1, 1, 1, 1),
(226, 13, 5, 1, 1, 1, 1, 1),
(227, 10, 1, 1, 1, 1, 1, 1),
(228, 10, 2, 1, 1, 1, 1, 1),
(229, 10, 3, 1, 1, 1, 1, 1),
(230, 10, 4, 1, 1, 1, 1, 1),
(231, 10, 5, 1, 1, 1, 1, 1),
(232, 11, 1, 1, 1, 1, 1, 1),
(233, 11, 2, 1, 1, 1, 1, 1),
(234, 11, 3, 1, 1, 1, 1, 1),
(235, 11, 4, 1, 1, 1, 1, 1),
(236, 11, 5, 1, 1, 1, 1, 1),
(238, 15, 1, 1, 0, 0, 1, 0),
(239, 15, 2, 1, 0, 0, 1, 0),
(240, 15, 3, 0, 0, 0, 0, 0),
(241, 15, 4, 0, 0, 0, 0, 0),
(242, 15, 5, 1, 1, 1, 0, 0),
(243, 17, 1, 1, 0, 0, 0, 0),
(244, 17, 2, 1, 0, 0, 0, 0),
(245, 17, 3, 1, 0, 0, 0, 0),
(246, 17, 4, 1, 0, 0, 0, 0),
(247, 17, 5, 1, 0, 0, 0, 0),
(248, 18, 1, 1, 0, 0, 0, 0),
(249, 18, 2, 1, 0, 0, 0, 0),
(250, 18, 3, 1, 0, 0, 0, 0),
(251, 18, 4, 1, 0, 0, 0, 0),
(252, 18, 5, 1, 0, 0, 0, 0),
(253, 19, 1, 1, 0, 0, 0, 0),
(254, 19, 2, 1, 0, 0, 0, 0),
(255, 19, 3, 1, 0, 0, 0, 0),
(256, 19, 4, 1, 0, 0, 0, 0),
(257, 19, 5, 1, 0, 0, 0, 0),
(258, 20, 1, 1, 0, 0, 0, 0),
(259, 20, 2, 1, 0, 0, 0, 0),
(260, 20, 3, 1, 0, 0, 0, 0),
(261, 20, 4, 1, 0, 0, 0, 0),
(262, 20, 5, 1, 0, 0, 0, 0),
(263, 21, 1, 1, 0, 0, 0, 0),
(264, 21, 2, 1, 0, 0, 0, 0),
(265, 21, 3, 1, 0, 0, 0, 0),
(266, 21, 4, 1, 0, 0, 0, 0),
(267, 21, 5, 1, 0, 0, 0, 0),
(268, 22, 1, 1, 0, 0, 0, 0),
(269, 22, 2, 1, 0, 0, 0, 0),
(270, 22, 3, 1, 0, 0, 0, 0),
(271, 22, 4, 1, 0, 0, 0, 0),
(272, 22, 5, 1, 0, 0, 0, 0),
(273, 23, 1, 1, 0, 0, 0, 0),
(274, 23, 2, 1, 0, 0, 0, 0),
(275, 23, 3, 1, 0, 0, 0, 0),
(276, 23, 4, 1, 0, 0, 0, 0),
(277, 23, 5, 1, 0, 0, 0, 0),
(278, 24, 1, 1, 0, 0, 0, 0),
(279, 24, 2, 1, 0, 0, 0, 0),
(280, 24, 3, 1, 0, 0, 0, 0),
(281, 24, 4, 1, 0, 0, 0, 0),
(282, 24, 5, 1, 0, 0, 0, 0),
(283, 25, 1, 1, 0, 0, 0, 0),
(284, 25, 2, 1, 0, 0, 0, 0),
(285, 25, 3, 1, 0, 0, 0, 0),
(286, 25, 4, 1, 0, 0, 0, 0),
(287, 25, 5, 1, 0, 0, 0, 0),
(288, 26, 1, 1, 0, 0, 0, 0),
(289, 26, 2, 1, 0, 0, 0, 0),
(290, 26, 3, 1, 0, 0, 0, 0),
(291, 26, 4, 1, 0, 0, 0, 0),
(292, 26, 5, 1, 0, 0, 0, 0),
(293, 27, 1, 1, 0, 0, 0, 0),
(294, 27, 2, 1, 0, 0, 0, 0),
(295, 27, 3, 1, 0, 0, 0, 0),
(296, 27, 4, 1, 0, 0, 0, 0),
(297, 27, 5, 1, 0, 0, 0, 0),
(298, 28, 1, 1, 0, 0, 0, 0),
(299, 28, 2, 1, 0, 0, 0, 0),
(300, 28, 3, 1, 0, 0, 0, 0),
(301, 28, 4, 1, 0, 0, 0, 0),
(302, 28, 5, 1, 0, 0, 0, 0),
(313, 29, 1, 1, 0, 0, 0, 0),
(314, 29, 2, 1, 0, 0, 0, 0),
(315, 29, 3, 1, 0, 0, 0, 0),
(316, 29, 4, 1, 0, 0, 0, 0),
(317, 29, 5, 1, 0, 0, 0, 0),
(323, 30, 1, 1, 0, 0, 0, 0),
(324, 30, 2, 1, 0, 0, 0, 0),
(325, 30, 3, 1, 0, 0, 0, 0),
(326, 30, 4, 1, 0, 0, 0, 0),
(327, 30, 5, 1, 0, 0, 0, 0),
(328, 31, 1, 1, 0, 0, 0, 0),
(329, 31, 2, 1, 0, 0, 0, 0),
(330, 31, 3, 1, 0, 0, 0, 0),
(331, 31, 4, 1, 0, 0, 0, 0),
(332, 31, 5, 1, 0, 0, 0, 0),
(333, 32, 1, 1, 0, 0, 0, 0),
(334, 32, 2, 1, 0, 0, 0, 0),
(335, 32, 3, 1, 0, 0, 0, 0),
(336, 32, 4, 1, 0, 0, 0, 0),
(337, 32, 5, 1, 0, 0, 0, 0),
(338, 33, 1, 1, 0, 0, 0, 0),
(339, 33, 2, 1, 0, 0, 0, 0),
(340, 33, 3, 1, 0, 0, 0, 0),
(341, 33, 4, 1, 0, 0, 0, 0),
(342, 33, 5, 1, 0, 0, 0, 0),
(343, 34, 1, 1, 0, 0, 0, 0),
(344, 34, 2, 1, 0, 0, 0, 0),
(345, 34, 3, 1, 0, 0, 0, 0),
(346, 34, 4, 1, 0, 0, 0, 0),
(347, 34, 5, 1, 0, 0, 0, 0),
(348, 35, 1, 1, 0, 0, 0, 0),
(349, 35, 2, 1, 0, 0, 0, 0),
(350, 35, 3, 1, 0, 0, 0, 0),
(351, 35, 4, 1, 0, 0, 0, 0),
(352, 35, 5, 1, 0, 0, 0, 0),
(353, 36, 1, 1, 0, 0, 0, 0),
(354, 36, 2, 1, 0, 0, 0, 0),
(355, 36, 3, 1, 0, 0, 0, 0),
(356, 36, 4, 1, 0, 0, 0, 0),
(357, 36, 5, 1, 0, 0, 0, 0),
(358, 37, 1, 1, 0, 0, 0, 0),
(359, 37, 2, 1, 0, 0, 0, 0),
(360, 37, 3, 1, 0, 0, 0, 0),
(361, 37, 4, 1, 0, 0, 0, 0),
(362, 37, 5, 1, 0, 0, 0, 0),
(363, 38, 1, 1, 0, 0, 0, 0),
(364, 38, 2, 1, 0, 0, 0, 0),
(365, 38, 3, 1, 0, 0, 0, 0),
(366, 38, 4, 1, 0, 0, 0, 0),
(367, 38, 5, 1, 0, 0, 0, 0),
(368, 39, 1, 1, 0, 0, 0, 0),
(369, 39, 2, 1, 0, 0, 0, 0),
(370, 39, 3, 1, 0, 0, 0, 0),
(371, 39, 4, 1, 0, 0, 0, 0),
(372, 39, 5, 1, 0, 0, 0, 0),
(373, 40, 1, 1, 0, 0, 0, 0),
(374, 40, 2, 1, 0, 0, 0, 0),
(375, 40, 3, 1, 0, 0, 0, 0),
(376, 40, 4, 1, 0, 0, 0, 0),
(377, 40, 5, 1, 0, 0, 0, 0),
(378, 41, 1, 1, 0, 0, 0, 0),
(379, 41, 2, 1, 0, 0, 0, 0),
(380, 41, 3, 1, 0, 0, 0, 0),
(381, 41, 4, 1, 0, 0, 0, 0),
(382, 41, 5, 1, 0, 0, 0, 0),
(383, 42, 1, 1, 0, 0, 0, 0),
(384, 42, 2, 1, 0, 0, 0, 0),
(385, 42, 3, 1, 0, 0, 0, 0),
(386, 42, 4, 1, 0, 0, 0, 0),
(387, 42, 5, 1, 0, 0, 0, 0),
(388, 43, 1, 1, 0, 0, 0, 0),
(389, 43, 2, 1, 0, 0, 0, 0),
(390, 43, 3, 1, 0, 0, 0, 0),
(391, 43, 4, 1, 0, 0, 0, 0),
(392, 43, 5, 1, 0, 0, 0, 0),
(393, 44, 1, 1, 0, 0, 0, 0),
(394, 44, 2, 1, 0, 0, 0, 0),
(395, 44, 3, 1, 0, 0, 0, 0),
(396, 44, 4, 1, 0, 0, 0, 0),
(397, 44, 5, 1, 0, 0, 0, 0),
(398, 45, 1, 1, 0, 0, 0, 0),
(399, 45, 2, 1, 0, 0, 0, 0),
(400, 45, 3, 1, 0, 0, 0, 0),
(401, 45, 4, 1, 0, 0, 0, 0),
(402, 45, 5, 1, 0, 0, 0, 0),
(403, 46, 1, 1, 1, 1, 1, 1),
(404, 46, 2, 1, 1, 1, 1, 1),
(405, 46, 3, 1, 1, 1, 1, 1),
(406, 46, 4, 1, 1, 1, 1, 1),
(407, 46, 5, 1, 1, 1, 1, 1),
(418, 48, 1, 1, 1, 1, 1, 1),
(419, 48, 2, 1, 1, 1, 1, 1),
(420, 48, 3, 1, 1, 1, 1, 1),
(421, 48, 4, 1, 1, 1, 1, 1),
(422, 48, 5, 1, 1, 1, 1, 1),
(423, 47, 1, 1, 1, 1, 1, 1),
(424, 47, 2, 1, 1, 1, 1, 1),
(425, 47, 3, 1, 1, 1, 1, 1),
(426, 47, 4, 1, 1, 1, 1, 1),
(427, 47, 5, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_age_range`
--

CREATE TABLE IF NOT EXISTS `stf_age_range` (
`id` int(11) NOT NULL,
  `nu_min_age` int(11) NOT NULL,
  `nu_max_age` int(11) NOT NULL,
  `nu_ball_weight` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_age_range`
--

INSERT INTO `stf_age_range` (`id`, `nu_min_age`, `nu_max_age`, `nu_ball_weight`) VALUES
(1, 5, 8, 326),
(2, 9, 12, 340),
(3, 13, 18, 430);

-- --------------------------------------------------------

--
-- Table structure for table `stf_anthropometric`
--

CREATE TABLE IF NOT EXISTS `stf_anthropometric` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_size` float DEFAULT NULL,
  `nu_weight` float DEFAULT NULL,
  `nu_chest_diameter` float DEFAULT NULL,
  `nu_hip_diameter` float DEFAULT NULL,
  `nu_left_thigh_diameter` float DEFAULT NULL,
  `nu_right_thigh_diameter` float DEFAULT NULL,
  `nu_left_ankle_diameter` float DEFAULT NULL,
  `nu_right_ankle_diameter` float DEFAULT NULL,
  `nu_head_diameter` float DEFAULT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_anthropometric`
--

INSERT INTO `stf_anthropometric` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_size`, `nu_weight`, `nu_chest_diameter`, `nu_hip_diameter`, `nu_left_thigh_diameter`, `nu_right_thigh_diameter`, `nu_left_ankle_diameter`, `nu_right_ankle_diameter`, `nu_head_diameter`, `dt_register`, `bo_status`) VALUES
(1, '12282538905735fefae18fb', 17, 1, '2016-07-19', 3, 4, 5, 4, 2, 3, 4, 5, 3, '2016-05-13 12:21:14', 1),
(2, '199402779857567830f018b', 18, 1, '2016-06-07', 6, 39, 14, 9, 8, 8, 8, 8, 11, '2016-06-07 03:30:56', 0),
(3, '11917647685757f9709a91b', 17, 2, '2016-06-08', 5, 2, 5, 4, 5, 5, 4, 5, 4, '2016-06-08 06:54:40', 1),
(4, '14154468815784e848a1d71', 22, 1, '2016-07-12', 2, 332.33, 3, 3, 0, 3, 3, 3, 23, '2016-07-12 08:53:28', 0),
(5, '13600461865785dd94194fb', 22, 2, '2016-07-04', 3, 2, 32, 3, 33, 22, 21, 21, 19, '2016-07-13 02:20:04', 1),
(6, '1033567050578efe6354bd0', 23, 1, '2016-07-20', 3, 1, 4, 3, 3, 3, 3, 3, 3, '2016-07-20 00:30:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_category`
--

CREATE TABLE IF NOT EXISTS `stf_category` (
`id` int(11) NOT NULL,
  `tx_name` varchar(100) NOT NULL,
  `tx_abbr` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_category`
--

INSERT INTO `stf_category` (`id`, `tx_name`, `tx_abbr`) VALUES
(1, 'Children A', 'Ch A'),
(2, 'Children B', 'Ch B'),
(3, 'Children C', 'Ch C'),
(4, 'Junior', 'Jr'),
(5, 'Junior 1st Division', 'Jr 1D'),
(6, 'Junior 2nd Division', 'Jr 2D'),
(7, 'Professional', 'Pro');

-- --------------------------------------------------------

--
-- Table structure for table `stf_country`
--

CREATE TABLE IF NOT EXISTS `stf_country` (
`id` int(11) NOT NULL,
  `tx_name` varchar(100) NOT NULL,
  `tx_iso_code` varchar(10) NOT NULL,
  `tx_phone_prefix` varchar(5) NOT NULL,
  `tx_zipcode_format` varchar(50) NOT NULL,
  `id_zone` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_country`
--

INSERT INTO `stf_country` (`id`, `tx_name`, `tx_iso_code`, `tx_phone_prefix`, `tx_zipcode_format`, `id_zone`) VALUES
(1, 'Germany', 'DE', '49', 'NNNNN', 1),
(2, 'Austria', 'AT', '43', 'NNNN', 1),
(3, 'Belgium', 'BE', '32', 'NNNN', 1),
(4, 'Canada', 'CA', '1', 'LNL NLN', 2),
(5, 'China', 'CN', '86', 'NNNNNN', 3),
(6, 'Spain', 'ES', '34', 'NNNNN', 1),
(7, 'Finland', 'FI', '358', 'NNNNN', 1),
(8, 'France', 'FR', '33', 'NNNNN', 1),
(9, 'Greece', 'GR', '30', 'NNNNN', 1),
(10, 'Italy', 'IT', '39', 'NNNNN', 1),
(11, 'Japan', 'JP', '81', 'NNN-NNNN', 3),
(12, 'Luxemburg', 'LU', '352', 'NNNN', 1),
(13, 'Netherlands', 'NL', '31', 'NNNN LL', 1),
(14, 'Poland', 'PL', '48', 'NN-NNN', 1),
(15, 'Portugal', 'PT', '351', 'NNNN-NNN', 1),
(16, 'Czech Republic', 'CZ', '420', 'NNN NN', 1),
(17, 'United Kingdom', 'GB', '44', '', 1),
(18, 'Sweden', 'SE', '46', 'NNN NN', 1),
(19, 'Switzerland', 'CH', '41', 'NNNN', 7),
(20, 'Denmark', 'DK', '45', 'NNNN', 1),
(21, 'United States', 'US', '1', 'NNNNN', 2),
(22, 'HongKong', 'HK', '852', '', 3),
(23, 'Norway', 'NO', '47', 'NNNN', 7),
(24, 'Australia', 'AU', '61', 'NNNN', 5),
(25, 'Singapore', 'SG', '65', 'NNNNNN', 3),
(26, 'Ireland', 'IE', '353', '', 1),
(27, 'New Zealand', 'NZ', '64', 'NNNN', 5),
(28, 'South Korea', 'KR', '82', 'NNN-NNN', 3),
(29, 'Israel', 'IL', '972', 'NNNNNNN', 3),
(30, 'South Africa', 'ZA', '27', 'NNNN', 4),
(31, 'Nigeria', 'NG', '234', '', 4),
(32, 'Ivory Coast', 'CI', '225', '', 4),
(33, 'Togo', 'TG', '228', '', 4),
(34, 'Bolivia', 'BO', '591', '', 6),
(35, 'Mauritius', 'MU', '230', '', 4),
(36, 'Romania', 'RO', '40', 'NNNNNN', 1),
(37, 'Slovakia', 'SK', '421', 'NNN NN', 1),
(38, 'Algeria', 'DZ', '213', 'NNNNN', 4),
(39, 'American Samoa', 'AS', '0', '', 2),
(40, 'Andorra', 'AD', '376', 'CNNN', 7),
(41, 'Angola', 'AO', '244', '', 4),
(42, 'Anguilla', 'AI', '0', '', 8),
(43, 'Antigua and Barbuda', 'AG', '0', '', 2),
(44, 'Argentina', 'AR', '54', 'LNNNN', 6),
(45, 'Armenia', 'AM', '374', 'NNNN', 3),
(46, 'Aruba', 'AW', '297', '', 8),
(47, 'Azerbaijan', 'AZ', '994', 'CNNNN', 3),
(48, 'Bahamas', 'BS', '0', '', 2),
(49, 'Bahrain', 'BH', '973', '', 3),
(50, 'Bangladesh', 'BD', '880', 'NNNN', 3),
(51, 'Barbados', 'BB', '0', 'CNNNNN', 2),
(52, 'Belarus', 'BY', '0', 'NNNNNN', 7),
(53, 'Belize', 'BZ', '501', '', 8),
(54, 'Benin', 'BJ', '229', '', 4),
(55, 'Bermuda', 'BM', '0', '', 2),
(56, 'Bhutan', 'BT', '975', '', 3),
(57, 'Botswana', 'BW', '267', '', 4),
(58, 'Brazil', 'BR', '55', 'NNNNN-NNN', 6),
(59, 'Brunei', 'BN', '673', 'LLNNNN', 3),
(60, 'Burkina Faso', 'BF', '226', '', 4),
(61, 'Burma (Myanmar)', 'MM', '95', '', 3),
(62, 'Burundi', 'BI', '257', '', 4),
(63, 'Cambodia', 'KH', '855', 'NNNNN', 3),
(64, 'Cameroon', 'CM', '237', '', 4),
(65, 'Cape Verde', 'CV', '238', 'NNNN', 4),
(66, 'Central African Republic', 'CF', '236', '', 4),
(67, 'Chad', 'TD', '235', '', 4),
(68, 'Chile', 'CL', '56', 'NNN-NNNN', 6),
(69, 'Colombia', 'CO', '57', 'NNNNNN', 6),
(70, 'Comoros', 'KM', '269', '', 4),
(71, 'Congo, Dem. Republic', 'CD', '242', '', 4),
(72, 'Congo, Republic', 'CG', '243', '', 4),
(73, 'Costa Rica', 'CR', '506', 'NNNNN', 8),
(74, 'Croatia', 'HR', '385', 'NNNNN', 7),
(75, 'Cuba', 'CU', '53', '', 8),
(76, 'Cyprus', 'CY', '357', 'NNNN', 1),
(77, 'Djibouti', 'DJ', '253', '', 4),
(78, 'Dominica', 'DM', '0', '', 8),
(79, 'Dominican Republic', 'DO', '0', '', 8),
(80, 'East Timor', 'TL', '670', '', 3),
(81, 'Ecuador', 'EC', '593', 'CNNNNNN', 6),
(82, 'Egypt', 'EG', '20', '', 4),
(83, 'El Salvador', 'SV', '503', '', 8),
(84, 'Equatorial Guinea', 'GQ', '240', '', 4),
(85, 'Eritrea', 'ER', '291', '', 4),
(86, 'Estonia', 'EE', '372', 'NNNNN', 1),
(87, 'Ethiopia', 'ET', '251', '', 4),
(88, 'Falkland Islands', 'FK', '0', 'LLLL NLL', 8),
(89, 'Faroe Islands', 'FO', '298', '', 7),
(90, 'Fiji', 'FJ', '679', '', 5),
(91, 'Gabon', 'GA', '241', '', 4),
(92, 'Gambia', 'GM', '220', '', 4),
(93, 'Georgia', 'GE', '995', 'NNNN', 3),
(94, 'Ghana', 'GH', '233', '', 4),
(95, 'Grenada', 'GD', '0', '', 8),
(96, 'Greenland', 'GL', '299', '', 7),
(97, 'Gibraltar', 'GI', '350', '', 7),
(98, 'Guadeloupe', 'GP', '590', '', 8),
(99, 'Guam', 'GU', '0', '', 5),
(100, 'Guatemala', 'GT', '502', '', 8),
(101, 'Guernsey', 'GG', '0', 'LLN NLL', 7),
(102, 'Guinea', 'GN', '224', '', 4),
(103, 'Guinea-Bissau', 'GW', '245', '', 4),
(104, 'Guyana', 'GY', '592', '', 6),
(105, 'Haiti', 'HT', '509', '', 8),
(106, 'Heard Island and McDonald Islands', 'HM', '0', '', 5),
(107, 'Vatican City State', 'VA', '379', 'NNNNN', 7),
(108, 'Honduras', 'HN', '504', '', 8),
(109, 'Iceland', 'IS', '354', 'NNN', 7),
(110, 'India', 'IN', '91', 'NNN NNN', 3),
(111, 'Indonesia', 'ID', '62', 'NNNNN', 3),
(112, 'Iran', 'IR', '98', 'NNNNN-NNNNN', 3),
(113, 'Iraq', 'IQ', '964', 'NNNNN', 3),
(114, 'Man Island', 'IM', '0', 'CN NLL', 7),
(115, 'Jamaica', 'JM', '0', '', 8),
(116, 'Jersey', 'JE', '0', 'CN NLL', 7),
(117, 'Jordan', 'JO', '962', '', 3),
(118, 'Kazakhstan', 'KZ', '7', 'NNNNNN', 3),
(119, 'Kenya', 'KE', '254', '', 4),
(120, 'Kiribati', 'KI', '686', '', 5),
(121, 'Korea, Dem. Republic of', 'KP', '850', '', 3),
(122, 'Kuwait', 'KW', '965', '', 3),
(123, 'Kyrgyzstan', 'KG', '996', '', 3),
(124, 'Laos', 'LA', '856', '', 3),
(125, 'Latvia', 'LV', '371', 'C-NNNN', 1),
(126, 'Lebanon', 'LB', '961', '', 3),
(127, 'Lesotho', 'LS', '266', '', 4),
(128, 'Liberia', 'LR', '231', '', 4),
(129, 'Libya', 'LY', '218', '', 4),
(130, 'Liechtenstein', 'LI', '423', 'NNNN', 1),
(131, 'Lithuania', 'LT', '370', 'NNNNN', 1),
(132, 'Macau', 'MO', '853', '', 3),
(133, 'Macedonia', 'MK', '389', '', 7),
(134, 'Madagascar', 'MG', '261', '', 4),
(135, 'Malawi', 'MW', '265', '', 4),
(136, 'Malaysia', 'MY', '60', 'NNNNN', 3),
(137, 'Maldives', 'MV', '960', '', 3),
(138, 'Mali', 'ML', '223', '', 4),
(139, 'Malta', 'MT', '356', 'LLL NNNN', 1),
(140, 'Marshall Islands', 'MH', '692', '', 5),
(141, 'Martinique', 'MQ', '596', '', 8),
(142, 'Mauritania', 'MR', '222', '', 4),
(143, 'Hungary', 'HU', '36', 'NNNN', 1),
(144, 'Mayotte', 'YT', '262', '', 4),
(145, 'Mexico', 'MX', '52', 'NNNNN', 2),
(146, 'Micronesia', 'FM', '691', '', 5),
(147, 'Moldova', 'MD', '373', 'C-NNNN', 7),
(148, 'Monaco', 'MC', '377', '980NN', 7),
(149, 'Mongolia', 'MN', '976', '', 3),
(150, 'Montenegro', 'ME', '382', 'NNNNN', 7),
(151, 'Montserrat', 'MS', '0', '', 8),
(152, 'Morocco', 'MA', '212', 'NNNNN', 4),
(153, 'Mozambique', 'MZ', '258', '', 4),
(154, 'Namibia', 'NA', '264', '', 4),
(155, 'Nauru', 'NR', '674', '', 5),
(156, 'Nepal', 'NP', '977', '', 3),
(157, 'Netherlands Antilles', 'AN', '599', '', 8),
(158, 'New Caledonia', 'NC', '687', '', 5),
(159, 'Nicaragua', 'NI', '505', 'NNNNNN', 8),
(160, 'Niger', 'NE', '227', '', 4),
(161, 'Niue', 'NU', '683', '', 5),
(162, 'Norfolk Island', 'NF', '0', '', 5),
(163, 'Northern Mariana Islands', 'MP', '0', '', 5),
(164, 'Oman', 'OM', '968', '', 3),
(165, 'Pakistan', 'PK', '92', '', 3),
(166, 'Palau', 'PW', '680', '', 5),
(167, 'Palestinian Territories', 'PS', '0', '', 3),
(168, 'Panama', 'PA', '507', 'NNNNNN', 8),
(169, 'Papua New Guinea', 'PG', '675', '', 5),
(170, 'Paraguay', 'PY', '595', '', 6),
(171, 'Peru', 'PE', '51', '', 6),
(172, 'Philippines', 'PH', '63', 'NNNN', 3),
(173, 'Pitcairn', 'PN', '0', 'LLLL NLL', 5),
(174, 'Puerto Rico', 'PR', '0', 'NNNNN', 8),
(175, 'Qatar', 'QA', '974', '', 3),
(176, 'Reunion Island', 'RE', '262', '', 4),
(177, 'Russian Federation', 'RU', '7', 'NNNNNN', 7),
(178, 'Rwanda', 'RW', '250', '', 4),
(179, 'Saint Barthelemy', 'BL', '0', '', 8),
(180, 'Saint Kitts and Nevis', 'KN', '0', '', 8),
(181, 'Saint Lucia', 'LC', '0', '', 8),
(182, 'Saint Martin', 'MF', '0', '', 8),
(183, 'Saint Pierre and Miquelon', 'PM', '508', '', 8),
(184, 'Saint Vincent and the Grenadines', 'VC', '0', '', 8),
(185, 'Samoa', 'WS', '685', '', 5),
(186, 'San Marino', 'SM', '378', 'NNNNN', 7),
(187, 'São Tomé and Príncipe', 'ST', '239', '', 4),
(188, 'Saudi Arabia', 'SA', '966', '', 3),
(189, 'Senegal', 'SN', '221', '', 4),
(190, 'Serbia', 'RS', '381', 'NNNNN', 7),
(191, 'Seychelles', 'SC', '248', '', 4),
(192, 'Sierra Leone', 'SL', '232', '', 4),
(193, 'Slovenia', 'SI', '386', 'C-NNNN', 1),
(194, 'Solomon Islands', 'SB', '677', '', 5),
(195, 'Somalia', 'SO', '252', '', 4),
(196, 'South Georgia and the South Sandwich Islands', 'GS', '0', 'LLLL NLL', 8),
(197, 'Sri Lanka', 'LK', '94', 'NNNNN', 3),
(198, 'Sudan', 'SD', '249', '', 4),
(199, 'Suriname', 'SR', '597', '', 8),
(200, 'Svalbard and Jan Mayen', 'SJ', '0', '', 7),
(201, 'Swaziland', 'SZ', '268', '', 4),
(202, 'Syria', 'SY', '963', '', 3),
(203, 'Taiwan', 'TW', '886', 'NNNNN', 3),
(204, 'Tajikistan', 'TJ', '992', '', 3),
(205, 'Tanzania', 'TZ', '255', '', 4),
(206, 'Thailand', 'TH', '66', 'NNNNN', 3),
(207, 'Tokelau', 'TK', '690', '', 5),
(208, 'Tonga', 'TO', '676', '', 5),
(209, 'Trinidad and Tobago', 'TT', '0', '', 6),
(210, 'Tunisia', 'TN', '216', '', 4),
(211, 'Turkey', 'TR', '90', 'NNNNN', 7),
(212, 'Turkmenistan', 'TM', '993', '', 3),
(213, 'Turks and Caicos Islands', 'TC', '0', 'LLLL NLL', 8),
(214, 'Tuvalu', 'TV', '688', '', 5),
(215, 'Uganda', 'UG', '256', '', 4),
(216, 'Ukraine', 'UA', '380', 'NNNNN', 1),
(217, 'United Arab Emirates', 'AE', '971', '', 3),
(218, 'Uruguay', 'UY', '598', '', 6),
(219, 'Uzbekistan', 'UZ', '998', '', 3),
(220, 'Vanuatu', 'VU', '678', '', 5),
(221, 'Venezuela', 'VE', '58', '', 6),
(222, 'Vietnam', 'VN', '84', 'NNNNNN', 3),
(223, 'Virgin Islands (British)', 'VG', '0', 'CNNNN', 2),
(224, 'Virgin Islands (U.S.)', 'VI', '0', '', 2),
(225, 'Wallis and Futuna', 'WF', '681', '', 5),
(226, 'Western Sahara', 'EH', '0', '', 4),
(227, 'Yemen', 'YE', '967', '', 3),
(228, 'Zambia', 'ZM', '260', '', 4),
(229, 'Zimbabwe', 'ZW', '263', '', 4),
(230, 'Albania', 'AL', '355', 'NNNN', 7),
(231, 'Afghanistan', 'AF', '93', '', 3),
(232, 'Antarctica', 'AQ', '0', '', 5),
(233, 'Bosnia and Herzegovina', 'BA', '387', '', 1),
(234, 'Bouvet Island', 'BV', '0', '', 5),
(235, 'British Indian Ocean Territory', 'IO', '0', 'LLLL NLL', 5),
(236, 'Bulgaria', 'BG', '359', 'NNNN', 1),
(237, 'Cayman Islands', 'KY', '0', '', 8),
(238, 'Christmas Island', 'CX', '0', '', 3),
(239, 'Cocos (Keeling) Islands', 'CC', '0', '', 3),
(240, 'Cook Islands', 'CK', '682', '', 5),
(241, 'French Guiana', 'GF', '594', '', 6),
(242, 'French Polynesia', 'PF', '689', '', 5),
(243, 'French Southern Territories', 'TF', '0', '', 5),
(244, 'Åland Islands', 'AX', '0', 'NNNNN', 7);

-- --------------------------------------------------------

--
-- Table structure for table `stf_dribbling_circular`
--

CREATE TABLE IF NOT EXISTS `stf_dribbling_circular` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_test_time` float NOT NULL,
  `nu_obstacles` int(11) NOT NULL,
  `nu_time_between_obstacles` float NOT NULL,
  `nu_overcome_obstacles` int(11) NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_dribbling_circular`
--

INSERT INTO `stf_dribbling_circular` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_test_time`, `nu_obstacles`, `nu_time_between_obstacles`, `nu_overcome_obstacles`, `dt_register`, `bo_status`) VALUES
(1, '586c925fa45dcd', 17, 1, '0000-00-00', 0, 0, 0, 0, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_dribbling_zigzag`
--

CREATE TABLE IF NOT EXISTS `stf_dribbling_zigzag` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_right_leg_time_target_one` float NOT NULL,
  `nu_right_leg_time_target_two` float DEFAULT NULL,
  `nu_right_leg_time_target_three` float DEFAULT NULL,
  `nu_right_leg_time_target_four` float DEFAULT NULL,
  `nu_right_leg_time_target_five` float DEFAULT NULL,
  `nu_right_leg_time_target_six` float DEFAULT NULL,
  `nu_left_leg_time_target_one` float NOT NULL,
  `nu_left_leg_time_target_two` float DEFAULT NULL,
  `nu_left_leg_time_target_three` float DEFAULT NULL,
  `nu_left_leg_time_target_four` float DEFAULT NULL,
  `nu_left_leg_time_target_five` float DEFAULT NULL,
  `nu_left_leg_time_target_six` float DEFAULT NULL,
  `nu_evaluated_distance` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_dribbling_zigzag`
--

INSERT INTO `stf_dribbling_zigzag` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_right_leg_time_target_one`, `nu_right_leg_time_target_two`, `nu_right_leg_time_target_three`, `nu_right_leg_time_target_four`, `nu_right_leg_time_target_five`, `nu_right_leg_time_target_six`, `nu_left_leg_time_target_one`, `nu_left_leg_time_target_two`, `nu_left_leg_time_target_three`, `nu_left_leg_time_target_four`, `nu_left_leg_time_target_five`, `nu_left_leg_time_target_six`, `nu_evaluated_distance`, `dt_register`, `bo_status`) VALUES
(1, 'c6dc79341be1fd3d258', 17, 1, '0000-00-00', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_event`
--

CREATE TABLE IF NOT EXISTS `stf_event` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `tx_name` varchar(100) NOT NULL,
  `dt_event` date DEFAULT NULL,
  `id_venue` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `id_measurement` int(11) NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_event`
--

INSERT INTO `stf_event` (`id`, `id_uniq`, `tx_name`, `dt_event`, `id_venue`, `id_team`, `id_measurement`, `dt_register`, `bo_status`) VALUES
(1, '18063858025718dbdccfc0a', 'Massive Event', '2016-11-30', 2, 22, 1, '2016-04-21 09:55:40', 1),
(2, '17640716355784d8f724e39', 'Testevent', '2016-07-12', 3, 22, 1, '2016-07-12 07:48:07', 1),
(3, '15529943655784e73916f00', 'Fhjjj', '2016-07-13', 3, 22, 1, '2016-07-12 08:48:57', 1),
(4, '479729385787785370914', 'Mera', '2016-07-18', 1, 22, 1, '2016-07-14 07:32:35', 0),
(5, '204513180557877adee23eb', 'Meravenue', '2016-07-19', 3, 21, 1, '2016-07-14 07:43:26', 0),
(6, '24965616957877c3568d89', 'Ssss', '2016-07-18', 1, 21, 1, '2016-07-14 07:49:09', 0),
(7, '91977259957877c6e342b9', 'Ssss', '2016-07-31', 3, 22, 1, '2016-07-14 07:50:06', 0),
(8, '963717186578877efc8a45', 'Sedd', '2016-07-18', 3, 22, 1, '2016-07-15 01:43:11', 1),
(9, '14217778057887b4b43620', 'Power Event', '2016-07-22', 3, 22, 1, '2016-07-15 01:57:31', 1),
(10, '1021263093579eee3cef8d8', 'Saaaa', '2016-08-09', 3, 22, 1, '2016-08-01 02:37:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_event_configuration`
--

CREATE TABLE IF NOT EXISTS `stf_event_configuration` (
  `id_event` int(11) NOT NULL,
  `id_configuration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_event_configuration`
--

INSERT INTO `stf_event_configuration` (`id_event`, `id_configuration`) VALUES
(1, 5),
(1, 10),
(1, 11),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(2, 10),
(2, 15),
(3, 10),
(3, 17),
(8, 10),
(9, 11),
(10, 19);

-- --------------------------------------------------------

--
-- Table structure for table `stf_gender`
--

CREATE TABLE IF NOT EXISTS `stf_gender` (
`id` int(11) NOT NULL,
  `tx_name` varchar(100) NOT NULL,
  `tx_abbr` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_gender`
--

INSERT INTO `stf_gender` (`id`, `tx_name`, `tx_abbr`) VALUES
(1, 'Male', 'M'),
(2, 'Female', 'F');

-- --------------------------------------------------------

--
-- Table structure for table `stf_heading_aim`
--

CREATE TABLE IF NOT EXISTS `stf_heading_aim` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_request_one` float NOT NULL,
  `nu_destination_one` float NOT NULL,
  `nu_request_two` float NOT NULL,
  `nu_destination_two` float NOT NULL,
  `nu_request_three` float NOT NULL,
  `nu_destination_three` float NOT NULL,
  `nu_request_four` float NOT NULL,
  `nu_destination_four` float NOT NULL,
  `nu_request_five` float NOT NULL,
  `nu_destination_five` float NOT NULL,
  `nu_request_six` float NOT NULL,
  `nu_destination_six` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_heading_aim`
--

INSERT INTO `stf_heading_aim` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_request_one`, `nu_destination_one`, `nu_request_two`, `nu_destination_two`, `nu_request_three`, `nu_destination_three`, `nu_request_four`, `nu_destination_four`, `nu_request_five`, `nu_destination_five`, `nu_request_six`, `nu_destination_six`, `dt_register`, `bo_status`) VALUES
(2, '1549682408578dc3b90fc8a', 18, 1, '2016-07-19', 24, 2, 10, 4, 14, 2, 22, 6, 19, 4, 7, 6, '2016-07-19 02:07:53', 1),
(3, '432438581578e0e91cb36a', 22, 1, '2016-07-19', 23, 5, 23, 5, 4, 5, 12, 5, 21, 1, 14, 8, '2016-07-19 07:27:13', 1),
(5, '755706216578f42413a9ec', 22, 1, '2016-07-19', 23, 5, 23, 5, 4, 5, 12, 5, 21, 1, 14, 8, '2016-07-20 05:20:01', 1),
(7, '1211975477579071924b736', 23, 1, '2016-07-21', 9, 3, 8, 3, 19, 2, 10, 2, 22, 4, 12, 2, '2016-07-21 02:54:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stf_heading_force`
--

CREATE TABLE IF NOT EXISTS `stf_heading_force` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_distance_one` float NOT NULL,
  `nu_distance_two` float DEFAULT NULL,
  `nu_distance_three` float DEFAULT NULL,
  `nu_head_attempt_one` float NOT NULL,
  `nu_head_attempt_two` float NOT NULL,
  `nu_head_attempt_three` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_heading_force`
--

INSERT INTO `stf_heading_force` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_distance_one`, `nu_distance_two`, `nu_distance_three`, `nu_head_attempt_one`, `nu_head_attempt_two`, `nu_head_attempt_three`, `dt_register`, `bo_status`) VALUES
(3, '64661895157569aba71adb', 18, 1, '2016-06-07', 9, 9, 7, 5, 2, 4, '2016-06-07 05:58:18', 1),
(4, '204732116457862e78509c5', 22, 1, '2016-07-13', 5, 5, 8, 5, 2, 2, '2016-07-13 08:05:12', 1),
(7, '302445536579071a55437e', 23, 1, '2016-07-21', 3, 3, 3, 3, 3, 3, '2016-07-21 02:54:29', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stf_heading_jump`
--

CREATE TABLE IF NOT EXISTS `stf_heading_jump` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_extended_arm_height` float NOT NULL,
  `nu_jump_high_one` float NOT NULL,
  `nu_jump_high_two` float DEFAULT NULL,
  `nu_jump_high_three` float DEFAULT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_heading_jump`
--

INSERT INTO `stf_heading_jump` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_extended_arm_height`, `nu_jump_high_one`, `nu_jump_high_two`, `nu_jump_high_three`, `dt_register`, `bo_status`) VALUES
(2, '1764338981578dc4ca18ffe', 18, 1, '2016-07-26', 2, 0.13, 2, 5, '2016-07-19 02:12:26', 1),
(3, '1776266083578e0e68940b3', 22, 1, '2016-07-19', 1, 5, 8, 8, '2016-07-19 07:26:32', 1),
(5, '78190851957906ee17c3c1', 17, 1, '2016-07-21', 2, 2, 3, 1, '2016-07-21 02:42:41', 1),
(7, '8901890875790717d327f0', 23, 1, '2016-07-21', 2, 3, 1, 4, '2016-07-21 02:53:49', 0),
(8, '8416178215791a19403bc4', 23, 1, '2016-07-22', 2, 2, 1, 4, '2016-07-22 00:31:16', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stf_image`
--

CREATE TABLE IF NOT EXISTS `stf_image` (
`id` int(11) NOT NULL,
  `tx_name` varchar(150) NOT NULL,
  `dt_register` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_image`
--

INSERT INTO `stf_image` (`id`, `tx_name`, `dt_register`) VALUES
(26, 'media/players/0e2601f2d25db9d6f4f1793d2de6c633.png', '2016-02-15 11:16:11'),
(27, 'media/players/34d5f03eed56291fa2fc91ca12005edf.jpg', '2016-02-15 11:16:11'),
(35, 'media/players/c570a9536fd529edfe84a2938e58ae22.png', '2016-02-15 13:36:50'),
(36, 'media/players/c632b2dc470e75dd64ee35a7f51b393b.jpg', '2016-02-15 13:36:50'),
(37, 'media/players/651894de74b7305788a018fe1b8e0d54.jpg', '2016-02-15 13:46:30'),
(38, 'media/players/3980c0c6e5963d33b3530dbfbac84e10.jpg', '2016-02-19 15:06:38'),
(39, 'media/players/8394f4d923281f7668d7e0a1ae1bdd70.png', '2016-04-11 15:09:02'),
(40, 'media/players/ed8c6b3defe846da95e8140ecfa26164.png', '2016-04-11 15:21:04'),
(41, 'media/players/c3072cea3117763e3c7ade12760e705e.png', '2016-04-11 15:22:20'),
(42, 'media/players/049db7381349dbaf3aa0eecf784956e9.jpg', '2016-04-11 15:28:48'),
(43, 'media/players/aac9520febb64d9250e99410fce13a92.png', '2016-04-27 10:14:26'),
(44, 'media/players/3e3b66f3b517634b3f9786756b6800d1.png', '2016-04-27 10:20:23'),
(45, 'media/players/1d4eac6dced9a7d086a7ae4547d52890.jpg', '2016-08-01 00:26:59');

-- --------------------------------------------------------

--
-- Table structure for table `stf_kicking_aim`
--

CREATE TABLE IF NOT EXISTS `stf_kicking_aim` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_right_leg_request_one` float NOT NULL,
  `nu_right_leg_destination_one` float NOT NULL,
  `nu_right_leg_request_two` float NOT NULL,
  `nu_right_leg_destination_two` float NOT NULL,
  `nu_right_leg_request_three` float NOT NULL,
  `nu_right_leg_destination_three` float NOT NULL,
  `nu_left_leg_request_one` float NOT NULL,
  `nu_left_leg_destination_one` float NOT NULL,
  `nu_left_leg_request_two` float NOT NULL,
  `nu_left_leg_destination_two` float NOT NULL,
  `nu_left_leg_request_three` float NOT NULL,
  `nu_left_leg_destination_three` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_kicking_aim`
--

INSERT INTO `stf_kicking_aim` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_right_leg_request_one`, `nu_right_leg_destination_one`, `nu_right_leg_request_two`, `nu_right_leg_destination_two`, `nu_right_leg_request_three`, `nu_right_leg_destination_three`, `nu_left_leg_request_one`, `nu_left_leg_destination_one`, `nu_left_leg_request_two`, `nu_left_leg_destination_two`, `nu_left_leg_request_three`, `nu_left_leg_destination_three`, `dt_register`, `bo_status`) VALUES
(1, '1850390133578719ad533fe', 22, 1, '2016-08-08', 13, 2, 8, 2, 5, 2, 15, 2, 1, 2, 1, 2, '2016-08-08 04:51:46', 1),
(3, '855757437578dd965153b0', 18, 1, '2016-07-26', 24, 6, 22, 6, 9, 6, 5, 9, 20, 8, 23, 10, '2016-07-19 03:40:21', 1),
(4, '1824095149578de0658870d', 18, 2, '2016-07-19', 24, 2, 3, 8, 4, 5, 21, 4, 24, 5, 14, 8, '2016-07-19 04:10:13', 1),
(6, '1472655957578ef98198523', 17, 1, '2016-08-05', 13, 2, 9, 2, 19, 2, 8, 2, 9, 2, 12, 2, '2016-08-05 06:20:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_kicking_force`
--

CREATE TABLE IF NOT EXISTS `stf_kicking_force` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_right_leg_attempt_one` float NOT NULL,
  `nu_right_leg_attempt_two` float NOT NULL,
  `nu_right_leg_attempt_three` float NOT NULL,
  `nu_left_leg_attempt_one` float NOT NULL,
  `nu_left_leg_attempt_two` float NOT NULL,
  `nu_left_leg_attempt_three` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_kicking_force`
--

INSERT INTO `stf_kicking_force` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_right_leg_attempt_one`, `nu_right_leg_attempt_two`, `nu_right_leg_attempt_three`, `nu_left_leg_attempt_one`, `nu_left_leg_attempt_two`, `nu_left_leg_attempt_three`, `dt_register`, `bo_status`) VALUES
(1, '370058036574e989e3ff94', 17, 1, '2016-08-05', 3, 3, 3, 3, 3, 3, '2016-06-01 04:11:10', 1),
(2, '2126424650576baa2c5eb39', 18, 1, '2016-06-23', 5, 4, 5, 6, 6, 4, '2016-06-23 05:21:48', 1),
(3, '154791732657aae0b999bf7', 23, 1, '2016-08-10', 3, 2, 2, 2, 2, 2, '2016-08-10 04:07:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_measurement`
--

CREATE TABLE IF NOT EXISTS `stf_measurement` (
`id` int(11) NOT NULL,
  `tx_name` varchar(100) NOT NULL,
  `tx_abbr` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_measurement`
--

INSERT INTO `stf_measurement` (`id`, `tx_name`, `tx_abbr`) VALUES
(1, 'Meters Kilometers Seconds', 'MKS'),
(2, 'Yards Pounds Seconds', 'YLS');

-- --------------------------------------------------------

--
-- Table structure for table `stf_movement_lateral`
--

CREATE TABLE IF NOT EXISTS `stf_movement_lateral` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_time_target_one` float NOT NULL,
  `nu_time_target_two` float NOT NULL,
  `nu_time_target_three` float NOT NULL,
  `nu_time_target_four` float NOT NULL,
  `nu_time_target_five` float NOT NULL,
  `nu_time_target_six` float NOT NULL,
  `nu_time_target_seven` float NOT NULL,
  `nu_time_target_eight` float NOT NULL,
  `nu_time_target_nine` float NOT NULL,
  `nu_time_target_ten` float NOT NULL,
  `nu_evaluated_distance` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_movement_lateral`
--

INSERT INTO `stf_movement_lateral` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_time_target_one`, `nu_time_target_two`, `nu_time_target_three`, `nu_time_target_four`, `nu_time_target_five`, `nu_time_target_six`, `nu_time_target_seven`, `nu_time_target_eight`, `nu_time_target_nine`, `nu_time_target_ten`, `nu_evaluated_distance`, `dt_register`, `bo_status`) VALUES
(1, '18063858025718dbdccfc0m', 17, 1, '2016-07-20', 2, 5, 6, 4, 2, 5, 1, 6, 8, 2, 5, '2016-07-20 00:00:00', 1),
(2, '18063858021718dbdccfc0m', 18, 1, '2016-07-20', 2, 5, 6, 6, 2, 5, 9, 12, 8, 2, 8, '2016-07-20 00:00:00', 1),
(3, '803247174579748aee5847', 23, 1, '2016-07-26', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-07-26 07:25:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_movement_linear`
--

CREATE TABLE IF NOT EXISTS `stf_movement_linear` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_time_target_one` float NOT NULL,
  `nu_time_target_two` float NOT NULL,
  `nu_time_target_three` float NOT NULL,
  `nu_time_target_four` float NOT NULL,
  `nu_time_target_five` float NOT NULL,
  `nu_time_target_six` float NOT NULL,
  `nu_time_target_seven` float NOT NULL,
  `nu_time_target_eight` float NOT NULL,
  `nu_time_target_nine` float NOT NULL,
  `nu_time_target_ten` float NOT NULL,
  `nu_evaluated_distance` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_movement_linear`
--

INSERT INTO `stf_movement_linear` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_time_target_one`, `nu_time_target_two`, `nu_time_target_three`, `nu_time_target_four`, `nu_time_target_five`, `nu_time_target_six`, `nu_time_target_seven`, `nu_time_target_eight`, `nu_time_target_nine`, `nu_time_target_ten`, `nu_evaluated_distance`, `dt_register`, `bo_status`) VALUES
(1, '18063858025718dbdccfc0h', 17, 1, '2016-07-20', 2, 2, 5, 6, 8, 8, 9, 12, 16, 7, 8, '2016-07-20 00:00:00', 1),
(2, '180638541025718dbdccfc0m', 18, 1, '2016-07-20', 2, 5, 6, 6, 2, 8, 9, 12, 16, 7, 8, '2016-07-20 00:00:00', 1),
(3, '1915707009579749b06e178', 23, 1, '2016-07-26', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2016-07-26 07:29:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_movement_zigzag`
--

CREATE TABLE IF NOT EXISTS `stf_movement_zigzag` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `dt_measurement` date NOT NULL,
  `nu_time_target_one` float NOT NULL,
  `nu_time_target_two` float NOT NULL,
  `nu_time_target_three` float NOT NULL,
  `nu_time_target_four` float NOT NULL,
  `nu_time_target_five` float NOT NULL,
  `nu_time_target_six` float NOT NULL,
  `nu_time_target_seven` float NOT NULL,
  `nu_time_target_eight` float NOT NULL,
  `nu_time_target_nine` float NOT NULL,
  `nu_time_target_ten` float NOT NULL,
  `nu_evaluated_distance` float NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_movement_zigzag`
--

INSERT INTO `stf_movement_zigzag` (`id`, `id_uniq`, `id_player`, `id_event`, `dt_measurement`, `nu_time_target_one`, `nu_time_target_two`, `nu_time_target_three`, `nu_time_target_four`, `nu_time_target_five`, `nu_time_target_six`, `nu_time_target_seven`, `nu_time_target_eight`, `nu_time_target_nine`, `nu_time_target_ten`, `nu_evaluated_distance`, `dt_register`, `bo_status`) VALUES
(1, '18063858025718dbdccfc0f', 17, 1, '2016-07-20', 2, 5, 6, 6, 2, 5, 9, 6, 16, 2, 8, '2016-07-20 00:00:00', 1),
(2, '18063128025718dbdccfc0m', 18, 1, '2016-07-20', 2, 5, 6, 4, 8, 8, 9, 12, 16, 7, 8, '2016-07-20 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_player`
--

CREATE TABLE IF NOT EXISTS `stf_player` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `tx_firstname` varchar(100) NOT NULL,
  `tx_secondname` varchar(100) DEFAULT NULL,
  `tx_lastname` varchar(100) NOT NULL,
  `tx_secondlastname` varchar(100) DEFAULT NULL,
  `dt_birthdate` date NOT NULL,
  `id_gender` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `tx_address` varchar(200) DEFAULT NULL,
  `tx_zipcode` varchar(50) DEFAULT NULL,
  `tx_phone_prefix` varchar(10) DEFAULT NULL,
  `tx_phone` varchar(50) DEFAULT NULL,
  `tx_email` varchar(150) NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_player`
--

INSERT INTO `stf_player` (`id`, `id_uniq`, `tx_firstname`, `tx_secondname`, `tx_lastname`, `tx_secondlastname`, `dt_birthdate`, `id_gender`, `id_country`, `id_state`, `tx_address`, `tx_zipcode`, `tx_phone_prefix`, `tx_phone`, `tx_email`, `dt_register`, `bo_status`) VALUES
(17, '19623349115720c9420739e', 'Fernando', '', 'Torres', '', '2003-11-26', 1, 21, 2, '418 King Street, Nile', '26999', '1', '8013788399', 'fernandotorres@gmail.com', '2016-04-27 10:14:26', 1),
(18, '16265186455756775cac653', 'Christiano', '', 'Ronaldo', '', '2005-06-08', 1, 21, 1, '1/322 Kapasdanga Colony, Palpara, Po And Dist. Hooghly', '71210', '1', '8013788399567', 'crhristiano.ronaldo@gmail.com', '2016-06-07 03:27:24', 1),
(19, '17516715545784992425652', 'Saheli', 'Ghosh', 'Ghosh', 'Ghosh', '2011-07-06', 2, 21, 16, 'Sddfrrr', '11111', '1', '3233232323232', 'ghosh@ggg.com', '2016-07-12 03:15:48', 0),
(20, '1802789516578499dc08876', 'Saheli', '', 'Ssssss', '', '2010-07-21', 1, 21, 2, 'Krklfjkjkjekj', '11111', '1', '11111111111111', 'ssss@gggg.com', '2016-07-12 03:18:52', 0),
(21, '57849483657849c5119191', 'Aaaa', 'Aaa', 'Aaaaaaa', '', '2011-07-07', 2, 21, 2, 'Ssssss', '22222', '1', '22222222222', 'sssss@gm.com', '2016-07-12 03:29:21', 0),
(22, '123092921157849c80b5185', 'Ddddd', 'Dddddd', 'Ddddd', 'Dddd', '2004-07-20', 2, 21, 4, 'Dsdddddsdsds', '22222', '1', '22222222222222224', 'ddddd@gmail.vom', '2016-07-12 03:30:08', 1),
(23, '11436984685785beffdc171', 'Movementplayer', '', 'Movementplayer', '', '2004-07-15', 1, 21, 16, 'Ssssss', '11111', '1', '11111111111111112', 'saaaa@gmail.com', '2016-07-13 00:09:35', 1),
(24, '1743811256579ee9648ccb3', 'Jin', '', 'Lus', '', '1999-08-05', 2, 21, 17, 'Sssssss', '33333', '1', '33333333333', 'lus@gmail.com', '2016-08-01 02:17:08', 0),
(25, '1436538319579f3f11eac85', 'Sxsxsxsxsxs', '', 'Xsxsxsxsxs', '', '2011-07-31', 2, 21, 2, 'Xxsxxsxxsxsxs', '33333', '1', '33333333333', 'xsxsxx@dcdcdc.com', '2016-08-01 08:22:41', 0),
(26, '948957658579f3ff49b00f', 'Dadcadad', 'Dadadada', 'Dadadad', '', '2011-07-31', 2, 21, 17, 'Addadad', '23456', '1', '234567890987654', 'adad@dd.com', '2016-08-01 08:26:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_player_image`
--

CREATE TABLE IF NOT EXISTS `stf_player_image` (
`id` int(11) NOT NULL,
  `id_player` int(11) NOT NULL,
  `id_image` int(11) NOT NULL,
  `id_type_image` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_player_image`
--

INSERT INTO `stf_player_image` (`id`, `id_player`, `id_image`, `id_type_image`) VALUES
(39, 17, 43, 1),
(40, 17, 44, 1),
(41, 18, 45, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_position`
--

CREATE TABLE IF NOT EXISTS `stf_position` (
`id` int(11) NOT NULL,
  `tx_name` varchar(100) NOT NULL,
  `tx_abbr` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_position`
--

INSERT INTO `stf_position` (`id`, `tx_name`, `tx_abbr`) VALUES
(1, 'Goalkeeper', 'GK'),
(2, 'Centre Back', 'CB'),
(3, 'Sweeper', 'SW'),
(4, 'Left Back', 'LB'),
(5, 'Right Back', 'RB'),
(6, 'Left Wing Back', 'LWB'),
(7, 'Right Wing Back', 'RWB'),
(8, 'Centre Midfield', 'CM'),
(9, 'Defensive Midfield', 'DM'),
(10, 'Attacking Midfield', 'AM'),
(11, 'Left Midfield', 'LM'),
(12, 'Right Midfield', 'RM'),
(13, 'Centre Forward', 'CF'),
(14, 'Second Striker', 'SS'),
(15, 'Left Winger', 'LW'),
(16, 'Right Winger', 'RW');

-- --------------------------------------------------------

--
-- Table structure for table `stf_section`
--

CREATE TABLE IF NOT EXISTS `stf_section` (
`id` int(11) NOT NULL,
  `section_name` varchar(100) DEFAULT NULL,
  `section_link` varchar(20) DEFAULT NULL,
  `section_icon` varchar(10) NOT NULL DEFAULT 'cube',
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stf_section`
--

INSERT INTO `stf_section` (`id`, `section_name`, `section_link`, `section_icon`, `bo_status`) VALUES
(1, 'Events', 'events', 'diamond', 1),
(2, 'Players', 'players', 'diamond', 1),
(3, 'Teams', 'teams', 'diamond', 1),
(4, 'Venues', 'venues', 'globe', 1),
(5, 'Users', 'users', 'users', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_state`
--

CREATE TABLE IF NOT EXISTS `stf_state` (
`id` int(11) NOT NULL,
  `tx_name` varchar(100) NOT NULL,
  `tx_iso_code` varchar(10) NOT NULL,
  `id_country` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=313 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_state`
--

INSERT INTO `stf_state` (`id`, `tx_name`, `tx_iso_code`, `id_country`) VALUES
(1, 'Alabama', 'AL', 21),
(2, 'Alaska', 'AK', 21),
(3, 'Arizona', 'AZ', 21),
(4, 'Arkansas', 'AR', 21),
(5, 'California', 'CA', 21),
(6, 'Colorado', 'CO', 21),
(7, 'Connecticut', 'CT', 21),
(8, 'Delaware', 'DE', 21),
(9, 'Florida', 'FL', 21),
(10, 'Georgia', 'GA', 21),
(11, 'Hawaii', 'HI', 21),
(12, 'Idaho', 'ID', 21),
(13, 'Illinois', 'IL', 21),
(14, 'Indiana', 'IN', 21),
(15, 'Iowa', 'IA', 21),
(16, 'Kansas', 'KS', 21),
(17, 'Kentucky', 'KY', 21),
(18, 'Louisiana', 'LA', 21),
(19, 'Maine', 'ME', 21),
(20, 'Maryland', 'MD', 21),
(21, 'Massachusetts', 'MA', 21),
(22, 'Michigan', 'MI', 21),
(23, 'Minnesota', 'MN', 21),
(24, 'Mississippi', 'MS', 21),
(25, 'Missouri', 'MO', 21),
(26, 'Montana', 'MT', 21),
(27, 'Nebraska', 'NE', 21),
(28, 'Nevada', 'NV', 21),
(29, 'New Hampshire', 'NH', 21),
(30, 'New Jersey', 'NJ', 21),
(31, 'New Mexico', 'NM', 21),
(32, 'New York', 'NY', 21),
(33, 'North Carolina', 'NC', 21),
(34, 'North Dakota', 'ND', 21),
(35, 'Ohio', 'OH', 21),
(36, 'Oklahoma', 'OK', 21),
(37, 'Oregon', 'OR', 21),
(38, 'Pennsylvania', 'PA', 21),
(39, 'Rhode Island', 'RI', 21),
(40, 'South Carolina', 'SC', 21),
(41, 'South Dakota', 'SD', 21),
(42, 'Tennessee', 'TN', 21),
(43, 'Texas', 'TX', 21),
(44, 'Utah', 'UT', 21),
(45, 'Vermont', 'VT', 21),
(46, 'Virginia', 'VA', 21),
(47, 'Washington', 'WA', 21),
(48, 'West Virginia', 'WV', 21),
(49, 'Wisconsin', 'WI', 21),
(50, 'Wyoming', 'WY', 21),
(51, 'Puerto Rico', 'PR', 21),
(52, 'US Virgin Islands', 'VI', 21),
(53, 'District of Columbia', 'DC', 21),
(54, 'Aguascalientes', 'AGS', 145),
(55, 'Baja California', 'BCN', 145),
(56, 'Baja California Sur', 'BCS', 145),
(57, 'Campeche', 'CAM', 145),
(58, 'Chiapas', 'CHP', 145),
(59, 'Chihuahua', 'CHH', 145),
(60, 'Coahuila', 'COA', 145),
(61, 'Colima', 'COL', 145),
(62, 'Distrito Federal', 'DIF', 145),
(63, 'Durango', 'DUR', 145),
(64, 'Guanajuato', 'GUA', 145),
(65, 'Guerrero', 'GRO', 145),
(66, 'Hidalgo', 'HID', 145),
(67, 'Jalisco', 'JAL', 145),
(68, 'Estado de México', 'MEX', 145),
(69, 'Michoacán', 'MIC', 145),
(70, 'Morelos', 'MOR', 145),
(71, 'Nayarit', 'NAY', 145),
(72, 'Nuevo León', 'NLE', 145),
(73, 'Oaxaca', 'OAX', 145),
(74, 'Puebla', 'PUE', 145),
(75, 'Querétaro', 'QUE', 145),
(76, 'Quintana Roo', 'ROO', 145),
(77, 'San Luis Potosí', 'SLP', 145),
(78, 'Sinaloa', 'SIN', 145),
(79, 'Sonora', 'SON', 145),
(80, 'Tabasco', 'TAB', 145),
(81, 'Tamaulipas', 'TAM', 145),
(82, 'Tlaxcala', 'TLA', 145),
(83, 'Veracruz', 'VER', 145),
(84, 'Yucatán', 'YUC', 145),
(85, 'Zacatecas', 'ZAC', 145),
(86, 'Ontario', 'ON', 4),
(87, 'Quebec', 'QC', 4),
(88, 'British Columbia', 'BC', 4),
(89, 'Alberta', 'AB', 4),
(90, 'Manitoba', 'MB', 4),
(91, 'Saskatchewan', 'SK', 4),
(92, 'Nova Scotia', 'NS', 4),
(93, 'New Brunswick', 'NB', 4),
(94, 'Newfoundland and Labrador', 'NL', 4),
(95, 'Prince Edward Island', 'PE', 4),
(96, 'Northwest Territories', 'NT', 4),
(97, 'Yukon', 'YT', 4),
(98, 'Nunavut', 'NU', 4),
(99, 'Buenos Aires', 'B', 44),
(100, 'Catamarca', 'K', 44),
(101, 'Chaco', 'H', 44),
(102, 'Chubut', 'U', 44),
(103, 'Ciudad de Buenos Aires', 'C', 44),
(104, 'Córdoba', 'X', 44),
(105, 'Corrientes', 'W', 44),
(106, 'Entre Ríos', 'E', 44),
(107, 'Formosa', 'P', 44),
(108, 'Jujuy', 'Y', 44),
(109, 'La Pampa', 'L', 44),
(110, 'La Rioja', 'F', 44),
(111, 'Mendoza', 'M', 44),
(112, 'Misiones', 'N', 44),
(113, 'Neuquén', 'Q', 44),
(114, 'Río Negro', 'R', 44),
(115, 'Salta', 'A', 44),
(116, 'San Juan', 'J', 44),
(117, 'San Luis', 'D', 44),
(118, 'Santa Cruz', 'Z', 44),
(119, 'Santa Fe', 'S', 44),
(120, 'Santiago del Estero', 'G', 44),
(121, 'Tierra del Fuego', 'V', 44),
(122, 'Tucumán', 'T', 44),
(123, 'Agrigento', 'AG', 10),
(124, 'Alessandria', 'AL', 10),
(125, 'Ancona', 'AN', 10),
(126, 'Aosta', 'AO', 10),
(127, 'Arezzo', 'AR', 10),
(128, 'Ascoli Piceno', 'AP', 10),
(129, 'Asti', 'AT', 10),
(130, 'Avellino', 'AV', 10),
(131, 'Bari', 'BA', 10),
(132, 'Barletta-Andria-Trani', 'BT', 10),
(133, 'Belluno', 'BL', 10),
(134, 'Benevento', 'BN', 10),
(135, 'Bergamo', 'BG', 10),
(136, 'Biella', 'BI', 10),
(137, 'Bologna', 'BO', 10),
(138, 'Bolzano', 'BZ', 10),
(139, 'Brescia', 'BS', 10),
(140, 'Brindisi', 'BR', 10),
(141, 'Cagliari', 'CA', 10),
(142, 'Caltanissetta', 'CL', 10),
(143, 'Campobasso', 'CB', 10),
(144, 'Carbonia-Iglesias', 'CI', 10),
(145, 'Caserta', 'CE', 10),
(146, 'Catania', 'CT', 10),
(147, 'Catanzaro', 'CZ', 10),
(148, 'Chieti', 'CH', 10),
(149, 'Como', 'CO', 10),
(150, 'Cosenza', 'CS', 10),
(151, 'Cremona', 'CR', 10),
(152, 'Crotone', 'KR', 10),
(153, 'Cuneo', 'CN', 10),
(154, 'Enna', 'EN', 10),
(155, 'Fermo', 'FM', 10),
(156, 'Ferrara', 'FE', 10),
(157, 'Firenze', 'FI', 10),
(158, 'Foggia', 'FG', 10),
(159, 'Forlì-Cesena', 'FC', 10),
(160, 'Frosinone', 'FR', 10),
(161, 'Genova', 'GE', 10),
(162, 'Gorizia', 'GO', 10),
(163, 'Grosseto', 'GR', 10),
(164, 'Imperia', 'IM', 10),
(165, 'Isernia', 'IS', 10),
(166, 'L''Aquila', 'AQ', 10),
(167, 'La Spezia', 'SP', 10),
(168, 'Latina', 'LT', 10),
(169, 'Lecce', 'LE', 10),
(170, 'Lecco', 'LC', 10),
(171, 'Livorno', 'LI', 10),
(172, 'Lodi', 'LO', 10),
(173, 'Lucca', 'LU', 10),
(174, 'Macerata', 'MC', 10),
(175, 'Mantova', 'MN', 10),
(176, 'Massa', 'MS', 10),
(177, 'Matera', 'MT', 10),
(178, 'Medio Campidano', 'VS', 10),
(179, 'Messina', 'ME', 10),
(180, 'Milano', 'MI', 10),
(181, 'Modena', 'MO', 10),
(182, 'Monza e della Brianza', 'MB', 10),
(183, 'Napoli', 'NA', 10),
(184, 'Novara', 'NO', 10),
(185, 'Nuoro', 'NU', 10),
(186, 'Ogliastra', 'OG', 10),
(187, 'Olbia-Tempio', 'OT', 10),
(188, 'Oristano', 'OR', 10),
(189, 'Padova', 'PD', 10),
(190, 'Palermo', 'PA', 10),
(191, 'Parma', 'PR', 10),
(192, 'Pavia', 'PV', 10),
(193, 'Perugia', 'PG', 10),
(194, 'Pesaro-Urbino', 'PU', 10),
(195, 'Pescara', 'PE', 10),
(196, 'Piacenza', 'PC', 10),
(197, 'Pisa', 'PI', 10),
(198, 'Pistoia', 'PT', 10),
(199, 'Pordenone', 'PN', 10),
(200, 'Potenza', 'PZ', 10),
(201, 'Prato', 'PO', 10),
(202, 'Ragusa', 'RG', 10),
(203, 'Ravenna', 'RA', 10),
(204, 'Reggio Calabria', 'RC', 10),
(205, 'Reggio Emilia', 'RE', 10),
(206, 'Rieti', 'RI', 10),
(207, 'Rimini', 'RN', 10),
(208, 'Roma', 'RM', 10),
(209, 'Rovigo', 'RO', 10),
(210, 'Salerno', 'SA', 10),
(211, 'Sassari', 'SS', 10),
(212, 'Savona', 'SV', 10),
(213, 'Siena', 'SI', 10),
(214, 'Siracusa', 'SR', 10),
(215, 'Sondrio', 'SO', 10),
(216, 'Taranto', 'TA', 10),
(217, 'Teramo', 'TE', 10),
(218, 'Terni', 'TR', 10),
(219, 'Torino', 'TO', 10),
(220, 'Trapani', 'TP', 10),
(221, 'Trento', 'TN', 10),
(222, 'Treviso', 'TV', 10),
(223, 'Trieste', 'TS', 10),
(224, 'Udine', 'UD', 10),
(225, 'Varese', 'VA', 10),
(226, 'Venezia', 'VE', 10),
(227, 'Verbano-Cusio-Ossola', 'VB', 10),
(228, 'Vercelli', 'VC', 10),
(229, 'Verona', 'VR', 10),
(230, 'Vibo Valentia', 'VV', 10),
(231, 'Vicenza', 'VI', 10),
(232, 'Viterbo', 'VT', 10),
(233, 'Aceh', 'AC', 111),
(234, 'Bali', 'BA', 111),
(235, 'Bangka', 'BB', 111),
(236, 'Banten', 'BT', 111),
(237, 'Bengkulu', 'BE', 111),
(238, 'Central Java', 'JT', 111),
(239, 'Central Kalimantan', 'KT', 111),
(240, 'Central Sulawesi', 'ST', 111),
(241, 'Coat of arms of East Java', 'JI', 111),
(242, 'East kalimantan', 'KI', 111),
(243, 'East Nusa Tenggara', 'NT', 111),
(244, 'Lambang propinsi', 'GO', 111),
(245, 'Jakarta', 'JK', 111),
(246, 'Jambi', 'JA', 111),
(247, 'Lampung', 'LA', 111),
(248, 'Maluku', 'MA', 111),
(249, 'North Maluku', 'MU', 111),
(250, 'North Sulawesi', 'SA', 111),
(251, 'North Sumatra', 'SU', 111),
(252, 'Papua', 'PA', 111),
(253, 'Riau', 'RI', 111),
(254, 'Lambang Riau', 'KR', 111),
(255, 'Southeast Sulawesi', 'SG', 111),
(256, 'South Kalimantan', 'KS', 111),
(257, 'South Sulawesi', 'SN', 111),
(258, 'South Sumatra', 'SS', 111),
(259, 'West Java', 'JB', 111),
(260, 'West Kalimantan', 'KB', 111),
(261, 'West Nusa Tenggara', 'NB', 111),
(262, 'Lambang Provinsi Papua Barat', 'PB', 111),
(263, 'West Sulawesi', 'SR', 111),
(264, 'West Sumatra', 'SB', 111),
(265, 'Yogyakarta', 'YO', 111),
(266, 'Aichi', '23', 11),
(267, 'Akita', '05', 11),
(268, 'Aomori', '02', 11),
(269, 'Chiba', '12', 11),
(270, 'Ehime', '38', 11),
(271, 'Fukui', '18', 11),
(272, 'Fukuoka', '40', 11),
(273, 'Fukushima', '07', 11),
(274, 'Gifu', '21', 11),
(275, 'Gunma', '10', 11),
(276, 'Hiroshima', '34', 11),
(277, 'Hokkaido', '01', 11),
(278, 'Hyogo', '28', 11),
(279, 'Ibaraki', '08', 11),
(280, 'Ishikawa', '17', 11),
(281, 'Iwate', '03', 11),
(282, 'Kagawa', '37', 11),
(283, 'Kagoshima', '46', 11),
(284, 'Kanagawa', '14', 11),
(285, 'Kochi', '39', 11),
(286, 'Kumamoto', '43', 11),
(287, 'Kyoto', '26', 11),
(288, 'Mie', '24', 11),
(289, 'Miyagi', '04', 11),
(290, 'Miyazaki', '45', 11),
(291, 'Nagano', '20', 11),
(292, 'Nagasaki', '42', 11),
(293, 'Nara', '29', 11),
(294, 'Niigata', '15', 11),
(295, 'Oita', '44', 11),
(296, 'Okayama', '33', 11),
(297, 'Okinawa', '47', 11),
(298, 'Osaka', '27', 11),
(299, 'Saga', '41', 11),
(300, 'Saitama', '11', 11),
(301, 'Shiga', '25', 11),
(302, 'Shimane', '32', 11),
(303, 'Shizuoka', '22', 11),
(304, 'Tochigi', '09', 11),
(305, 'Tokushima', '36', 11),
(306, 'Tokyo', '13', 11),
(307, 'Tottori', '31', 11),
(308, 'Toyama', '16', 11),
(309, 'Wakayama', '30', 11),
(310, 'Yamagata', '06', 11),
(311, 'Yamaguchi', '35', 11),
(312, 'Yamanashi', '19', 11);

-- --------------------------------------------------------

--
-- Table structure for table `stf_station`
--

CREATE TABLE IF NOT EXISTS `stf_station` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `tx_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_station`
--

INSERT INTO `stf_station` (`id`, `id_uniq`, `tx_name`) VALUES
(1, '141944456056671492caaa9', 'Anthropometrics'),
(2, '18727680295667144015046', 'Dribbling'),
(3, '1465975612566714511e954', 'Kicking'),
(4, '16223341295667145aefa87', 'Movement'),
(5, '19400868405667146cbf54e', 'Heading');

-- --------------------------------------------------------

--
-- Table structure for table `stf_station_configuration`
--

CREATE TABLE IF NOT EXISTS `stf_station_configuration` (
`id` int(11) NOT NULL,
  `id_station` int(11) NOT NULL,
  `id_test` int(11) NOT NULL,
  `id_age_range` int(11) NOT NULL,
  `nu_distance` float NOT NULL,
  `bo_status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_station_configuration`
--

INSERT INTO `stf_station_configuration` (`id`, `id_station`, `id_test`, `id_age_range`, `nu_distance`, `bo_status`) VALUES
(2, 3, 1, 3, 15, 1),
(3, 4, 5, 3, 35, 0),
(5, 5, 7, 2, 5, 1),
(6, 5, 3, 3, 25, 0),
(7, 4, 4, 1, 10, 1),
(8, 5, 1, 3, 20, 1),
(9, 3, 8, 3, 30, 1),
(10, 3, 1, 2, 10, 1),
(11, 3, 8, 2, 20, 1),
(12, 3, 8, 1, 10, 1),
(13, 5, 7, 3, 10, 1),
(14, 5, 1, 2, 13, 1),
(15, 4, 4, 2, 10, 1),
(16, 4, 5, 2, 10, 1),
(17, 4, 9, 2, 10, 1),
(18, 5, 3, 2, 20, 0),
(19, 2, 2, 2, 2, 1),
(20, 2, 5, 2, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_station_test`
--

CREATE TABLE IF NOT EXISTS `stf_station_test` (
  `id_station` int(11) NOT NULL,
  `id_test` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_station_test`
--

INSERT INTO `stf_station_test` (`id_station`, `id_test`) VALUES
(2, 2),
(2, 5),
(3, 1),
(3, 8),
(4, 4),
(4, 5),
(4, 6),
(5, 1),
(5, 7),
(5, 8);

-- --------------------------------------------------------

--
-- Table structure for table `stf_team`
--

CREATE TABLE IF NOT EXISTS `stf_team` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `tx_name` varchar(100) NOT NULL,
  `tx_image` varchar(100) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `tx_address` varchar(200) DEFAULT NULL,
  `tx_zipcode` varchar(50) DEFAULT NULL,
  `tx_phone_prefix` varchar(10) DEFAULT NULL,
  `tx_phone` varchar(50) DEFAULT NULL,
  `tx_email` varchar(100) NOT NULL,
  `tx_website` varchar(150) DEFAULT NULL,
  `tx_facebook` varchar(150) DEFAULT NULL,
  `tx_twitter` varchar(150) DEFAULT NULL,
  `tx_instagram` varchar(150) DEFAULT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_team`
--

INSERT INTO `stf_team` (`id`, `id_uniq`, `tx_name`, `tx_image`, `id_country`, `id_state`, `tx_address`, `tx_zipcode`, `tx_phone_prefix`, `tx_phone`, `tx_email`, `tx_website`, `tx_facebook`, `tx_twitter`, `tx_instagram`, `dt_register`, `bo_status`) VALUES
(19, '31568110256b22b52e6034', 'Real Madrid Fc', 'media/teams/9d581ad23bd4af980872f317e0187786.png', 21, 1, '607 Prescott Place, Sunwest', '80333', '', '', 'realmadrid@gmail.com', '', '', '', '', '2016-02-03 11:31:00', 0),
(20, '23170036456b25c1562431', 'Manchester United', 'media/teams/28e235196bb73b8b4fe44b880098be2e.png', 21, 4, '', '', '', '', 'manchester@gmail.com', '', '', '', '', '2016-02-03 14:59:00', 0),
(21, '147868628256b354a11714c', 'Barcelona Fc', 'media/teams/81696c10a7ce1d4e1b8bd7257114e111.png', 21, 16, 'Usa', '75678', '', '', 'barcelona@gmail.com', '', '', '', '', '2016-02-04 08:39:00', 1),
(22, '207350296756b3554e07244', 'Atlético De Madrid', 'media/teams/d8fda6f4904c753dd4b5671c3d00811d.jpg', 21, 5, '1/322 Kapasdanga Colony, Palpara, Po And Dist. Hooghly', '71210', '1', '8013788399', 'atletico1@gmail.com', 'google.com', '', '', '', '2016-02-04 08:42:00', 1),
(23, '708305226570beeb2e9b2b', 'Bayern Munich', 'media/teams/56d827e721e47a3bc0cad6e7c2b61baa.png', 21, 27, '887 Middleton Street, Rehrersburg', '50344', '', '', 'bayern@gmail.com', 'https://www.fcbayern.de/es/', '', '', '', '2016-04-11 14:36:00', 1),
(24, '1684352375784859cef2fc', 'Saheli', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', 21, 16, 'Sdff', '22222', '1', '2222222222', 'sssssss@dddd.vom', 'sssssss', '', '', '', '2016-07-12 01:52:00', 0),
(25, '1058557695578488c6f1fb9', 'Saheliteam', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', 21, 1, 'Xcdddd', '22222', '1', '222222222222', 'sssssss@ddddsd.com', 'sss.com', '', '', '', '2016-07-12 02:05:00', 0),
(26, '198879428057848a75ae4e6', 'Newteam', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', 21, 2, 'Sddddokm', '12222', '1', '222222222222', 'sssssss@dddddd.bno', 'sss.com', '', '', '', '2016-07-12 02:13:00', 0),
(27, '207002989657848aa3d17a2', 'Ssssss', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', 21, 16, 'Sssss', '22222', '1', '22222222222', 'gbgbd@ggvbb.bnn', '', '', '', '', '2016-07-12 02:13:00', 0),
(28, '1222336435787559e96692', 'Dddddd', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', 21, 18, 'Dddd', '22222', '1', '33333333333', 'sssss@gm.com', 'wwwww.gmail.com', '', '', '', '2016-07-14 05:04:00', 0),
(29, '1988062935788826dbc83a', 'Runn', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', 21, 2, 'Dsddsd', '12121', '1', '22222222222', 'sssss@gmail.com', 'google.com', '', '', '', '2016-07-15 02:27:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stf_team_category`
--

CREATE TABLE IF NOT EXISTS `stf_team_category` (
`id` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_gender` int(11) NOT NULL,
  `nu_player` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_team_category`
--

INSERT INTO `stf_team_category` (`id`, `id_team`, `id_category`, `id_gender`, `nu_player`) VALUES
(1, 21, 4, 2, 11),
(2, 21, 5, 1, 11),
(3, 21, 7, 1, 11),
(12, 24, 1, 2, 677),
(13, 25, 1, 2, 34),
(14, 26, 1, 2, 34),
(15, 27, 2, 2, 33),
(16, 28, 1, 2, 3),
(17, 29, 1, 2, 5),
(18, 22, 1, 1, 10),
(19, 22, 2, 1, 10),
(20, 22, 2, 2, 10),
(21, 22, 1, 2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `stf_team_player`
--

CREATE TABLE IF NOT EXISTS `stf_team_player` (
`id` int(11) NOT NULL,
  `id_team` int(11) NOT NULL,
  `id_player` int(11) NOT NULL,
  `nu_dorsal` int(11) DEFAULT NULL,
  `dt_admission` date NOT NULL,
  `dt_discharge` date DEFAULT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_team_player`
--

INSERT INTO `stf_team_player` (`id`, `id_team`, `id_player`, `nu_dorsal`, `dt_admission`, `dt_discharge`, `dt_register`, `bo_status`) VALUES
(5, 22, 17, 10, '2012-07-30', NULL, '2016-04-27 10:22:38', 1),
(6, 22, 18, 3, '2016-06-01', '2016-06-07', '2016-06-07 03:29:46', 0),
(7, 22, 22, 219, '2016-07-11', '2016-07-13', '2016-07-12 04:32:27', 0),
(8, 22, 21, 3333, '2016-07-11', '2016-07-12', '2016-07-12 06:35:49', 1),
(9, 22, 22, 3, '2016-07-11', '2016-07-12', '2016-07-15 04:39:07', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stf_team_player_position`
--

CREATE TABLE IF NOT EXISTS `stf_team_player_position` (
`id` int(11) NOT NULL,
  `id_team_player` int(11) NOT NULL,
  `id_position` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_team_player_position`
--

INSERT INTO `stf_team_player_position` (`id`, `id_team_player`, `id_position`) VALUES
(59, 5, 7),
(60, 6, 4),
(62, 7, 15),
(63, 8, 15),
(64, 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stf_test`
--

CREATE TABLE IF NOT EXISTS `stf_test` (
`id` int(11) NOT NULL,
  `tx_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_test`
--

INSERT INTO `stf_test` (`id`, `tx_name`) VALUES
(1, 'Aim'),
(2, 'Circular'),
(3, 'Force'),
(4, 'Lateral'),
(5, 'Zigzag'),
(6, 'Lineal'),
(7, 'Jump'),
(8, 'Power'),
(9, 'Linear');

-- --------------------------------------------------------

--
-- Table structure for table `stf_type_image`
--

CREATE TABLE IF NOT EXISTS `stf_type_image` (
`id` int(11) NOT NULL,
  `tx_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_type_image`
--

INSERT INTO `stf_type_image` (`id`, `tx_name`) VALUES
(1, 'Head Picture'),
(2, 'Body Picture');

-- --------------------------------------------------------

--
-- Table structure for table `stf_user`
--

CREATE TABLE IF NOT EXISTS `stf_user` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `tx_firstname` varchar(100) NOT NULL,
  `tx_lastname` varchar(100) NOT NULL,
  `tx_email` varchar(150) NOT NULL,
  `tx_password` varchar(64) NOT NULL,
  `tx_image` varchar(100) NOT NULL,
  `dt_register` datetime NOT NULL,
  `tx_security_key` varchar(40) NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_user`
--

INSERT INTO `stf_user` (`id`, `id_uniq`, `tx_firstname`, `tx_lastname`, `tx_email`, `tx_password`, `tx_image`, `dt_register`, `tx_security_key`, `bo_status`) VALUES
(2, '19467925645632498738fda', 'Arturo', 'Guillén', 'arturo.guillen@360creativestudio.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2015-10-29 12:29:59', 'c2d6421ea04d3e8a6a5eca3947ed82e49de18570', 0),
(3, '1892336874563249e03ef47', 'Jorge', 'Paredes', 'jorge.paredes@360creativestudio.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2015-10-29 12:31:28', 'f241c4286241ab8da75c4fd9c42699f51a575752', 1),
(4, '1987186358563255dc897db', 'Manuel', 'Alvis', 'manuel.alvis@360creativestudio.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2015-10-29 13:22:36', '6dc0aa7aaf46dceb99abf896406252f4d8e5f555', 0),
(10, '121380482565c76d44ee30', 'Omar', 'Milano', 'omarmilano@test.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2015-11-30 11:18:28', '74081e9c147ff90630629ff2e42be8113109a24f', 0),
(11, '523134342570bca198c9d6', 'Lewis', 'Mooney', 'lewismooney@musanpoly.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/users/ba04aa66af8240b34bf35b08f6c13e4c.jpg', '2016-04-11 12:00:25', '986dc480bfc24cb3994af6104bf59aa8a29034ad', 0),
(12, '59298983570bcded7c23a', 'Leah', 'Decker', 'leahdecker@slumberia.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/users/a49366e435954b2f5cb39d8c6fc1201f.jpg', '2016-04-11 12:16:45', 'd4c18362d8cdf51653aa9d965bb9349bfef54c37', 1),
(13, '12973706565748bc462a8a0', 'Test', 'User', 'testuser@360creativestudio.com', 'e70844b23a08357f83b8c247b8d2f99eb20de5980df2159d790d446c044d7979', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-05-27 17:29:42', '55c6dde21fdcbef75016cf7c18a90d9163f86449', 1),
(15, '1674824192575a256409ffd', 'Sdffsad', 'Saffsasafaf', 'bireshwar.goswami@gmail.com', '947e070b0884726d7a8723b07536d7ae5d84004951ad45f9110b8184441cfe42', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-06-09 22:26:44', 'a1ae97a2c7dc0cdb0ce6ab452927f333af440b93', 0),
(16, '412457104575a577adee0a', 'Bireshwar', 'Goswami', 'bireshwar@capitalnumbers.com', 'e70844b23a08357f83b8c247b8d2f99eb20de5980df2159d790d446c044d7979', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-06-10 02:00:26', '55c6dde21fdcbef75016cf7c18a90d9163f86449', 1),
(17, '1963169463578338c50c8f1', 'Onlyrefresh', 'Refre', 'serfg@gmail.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-11 02:12:21', 'be17c382e7edd6deb7368c97ed44f0ba40dcb631', 0),
(18, '706748137578346d94feea', 'Seeee', 'Ssss', 'ssss@gmail.com', 'e265bf3265d9bef8acfe59a940f7ad3f566b3ac5f6cc01b8c0ee2bd3495af5e7', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-11 03:12:25', 'ea5f35c96517da777d3c0b1e342a9045e35de897', 0),
(19, '1654642206578347a318c2a', 'Ssssa', 'Aaaaa', 'aaaaaa@ddddd.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-11 03:15:47', '36fe6c5654d3b649a0397dfada7a01689273913b', 0),
(20, '1054819130578349e9afba0', 'Saaaadd', 'Sdddff', 'ssdddd@dddd.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-11 03:25:29', 'b8a31a010bcb7558c4f0bdd46ce2c034d5211cd1', 0),
(21, '110691099257834b76238f0', 'Assas', 'Asss', 'asas@gmail.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-11 03:32:06', 'bb098ad93835548cc2e52ed779d065de7a7ccc7d', 0),
(22, '46392170457834d2d7eb61', 'Ssssssssdss', 'Sdsddss', 'ssdsdsd@gmail.com', 'e265bf3265d9bef8acfe59a940f7ad3f566b3ac5f6cc01b8c0ee2bd3495af5e7', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-11 03:39:25', '8b10678fd9c78e222be634714b542ece6b7c070f', 0),
(23, '923943256578352eb396d2', 'Reeee', 'Eeeee', 'eeeee@fffff.com', '4b8fbd6d7dae8f457495b2ccf3b77ee0be55a8e729aeb7c9244604bf8fa38b35', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-11 04:03:55', 'bf2b7b8a1bdad214029a233e7633b6e0f652e22a', 0),
(24, '19811262195783541330a08', 'Sddsdsd', 'Sdsdd', 'sdsdsd@edd.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-11 04:08:51', '408888a4711f9d65f1d3db29d7d7ddf54b8b35de', 0),
(25, '1953382164578354d54501c', 'Dffdfd', 'Dfdffdf', 'dfdfdf@fbg.coj', '574f4db658c7204d722c91731918cb0a496627e3dfd8d2ec835a609bf794fc4a', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-11 04:12:05', '369f22262b757762a35c654d486eb76ed9b01824', 0),
(26, '11345822825783565cb0db5', 'Ssssss', 'Ssssss', 'sssssss@sssss.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-11 04:18:36', '01666f92c009887b8685c1c1e29dde6c889cc6fc', 0),
(27, '14059880885783574cb054b', 'Saheli', 'Ghosh', 'sahelighosh@gmail.com', 'e70844b23a08357f83b8c247b8d2f99eb20de5980df2159d790d446c044d7979', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-11 04:22:36', '8c805306cf6152164ef6583a5de00b11cbaf4588', 0),
(28, '153687544557836e9a6ec00', 'Saasasasa', 'Sasasasa', 'asasasa@dwddd.com', 'cdf4e4755b2674ca6fe9f7d6d71a9594200f24077fe5f8a25688716f2060da97', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-11 06:02:02', 'e15ff8ccc80a7418d5344a3e5ff991641860e140', 0),
(29, '19765946575783734dd503a', 'Ssssss', 'Sssssss', 'sssssssss@gamil.com', 'e70844b23a08357f83b8c247b8d2f99eb20de5980df2159d790d446c044d7979', 'media/users/2db765f2647276e699578b3cc2407c4c.gif', '2016-07-11 06:22:05', 'a8bcfef353512e50c8295bfaf81c68f4abb57547', 0),
(30, '63997728357837f64a7052', 'Saffggghsj', 'Ssssss', 'ghdd@gmail.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/users/e2dff79146963965c648122fc8b38260.jpg', '2016-07-11 07:13:40', '4c258268d903b0a97909ccd725e74e23e08a3f68', 0),
(31, '1054064671578390b862fba', 'Sassa', 'Asassa', 'asasassaa@gmail.com', 'bb75f2663a2ca3a0e595877489fb744bd8c96e23a120f446a03374ad34f45431', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-11 08:27:36', '9be61c7880f32ba1bc1399adc22ddd21a6470e94', 0),
(32, '109148049957847a087d2aa', 'Fgb', 'Cncncnc', 'ccccbb@ggg.com', '1887835f53c0bf7d4d8e83c97c0d3cdcb2cef7847812f7e2ed1c5eea673368ad', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-12 01:03:04', '1b04cc474d8cb1798b5815da1d7fd10bca99be4a', 0),
(33, '23583796057847aaa53fe9', 'Saheli', 'Dsdsdds', 'dsdsdsdsd@fffff.com', 'a51efd07854a50413019214786c3b40e37a029f410a4bb417d9e9da258936e95', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-12 01:05:46', '5a14481b6d50cd1ee7337211f022d53961010afd', 0),
(34, '124194824457847bcd4e0f4', 'Cdsfff', 'Ffffff', 'ffffff@gnnn.bnm', 'e70844b23a08357f83b8c247b8d2f99eb20de5980df2159d790d446c044d7979', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-12 01:10:37', 'f59b46418b0a4312524bf8ebe270e507a57e1c6e', 0),
(35, '24577183657847c17ad92a', 'Saheli', 'Bbcjkbj', 'bjdbjbj@ffff.com', 'e70844b23a08357f83b8c247b8d2f99eb20de5980df2159d790d446c044d7979', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-12 01:11:51', 'ed8282748dcf8a14a62ae8557110797991dd9788', 0),
(36, '16457262657847c8413fa3', 'Saheli', 'Ghosh', 'shhhhs2ffff@hmmm.com', '2475462bdb82f601ed3f2e2d268d93bd446c6af38d206d4456b938c8f19f686e', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-12 01:13:40', '951a5b6443b7f5d1becee1b5ab896fc039839cc7', 0),
(37, '206340949457847d1966925', 'Saheli', 'Ghosh', 'ghdhddh2hkk@gmail.com', 'd73901b77163b33f4e16d3a0ec772f2760b2b15e5413460b6942bdc1e42df5c3', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-12 01:16:09', '52272c114e744c9a83742d2ce44b0001156b5055', 0),
(38, '974958722578480ff10ca8', 'Saheli', 'Dfggff', 'ssssaa@gmail.com', 'e265bf3265d9bef8acfe59a940f7ad3f566b3ac5f6cc01b8c0ee2bd3495af5e7', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-12 01:32:47', 'b9cc9c8e50b069089c5b9e58e8faa7ed82dfd0af', 0),
(39, '95941791257848bb2386f2', 'Paglauser', 'Paglauser', 'paglauser@gmail.vm', 'd73901b77163b33f4e16d3a0ec772f2760b2b15e5413460b6942bdc1e42df5c3', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-12 02:18:26', '3844c83a485956309f6cc8ba5cb012647b05f2a4', 0),
(40, '32104885257848d230a0f6', 'Paghhh', 'Shhjhj', 'shsjsjj@gmail.com', '35818eba202fe56d9060e912d823f028b115666dbcd729527cc3bf71af17d2b4', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-12 02:24:35', 'fcc1674dc19470f4bbac662b23f995eb3256ad87', 0),
(41, '172684041757848d56b39bd', 'Ssssss', 'Sssss', 'sssss@gggg.vbom', '3ebee154472aaeac86b56070c2d7fb1c6e276705f05de4ddd00ddb6baa635851', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-12 02:25:26', 'aa78365d8603652b3c75b2b9eca181cd0446169f', 0),
(42, '62451640257848db9bc7e7', 'Addadadad', 'Aadadada', 'adadadda@ddddd.vbb', '9e662c1de9072bec281b02c10aa47a7fd78f9856daa87b5f06ff81316425c6f4', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-12 02:27:05', 'faea52741dff87218c5ac894c12757dc58733425', 0),
(43, '12734416257848f52eb44e', 'Pagl', 'Bkjekjfwrfr', 'rfrfrfw@gmail.com', '80fd9e1fa40b3f5cad74d5aa039bb8dc057fcec664e9ab09e551822332c79483', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-12 02:33:54', '78fa5b38df4eca37212ee0c69c69a9258ff458e8', 0),
(44, '8560930885784906b3fa8d', 'Ssssss', 'Sssssss', 'sssssss@gmmm.com', '6a471b405ffb7247b304e69be7afe0f593849afc557bc5e570b06ec93835abc8', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-12 02:38:35', 'b48650275f869c912ea9b79f479d8ef9fe7582ec', 0),
(45, '153742810957887eea3c646', 'Ghyh', 'Bjkjbn', 'saaaa@hffjjkf.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-15 02:12:58', '08faa6a2f4e9c737c7b57d0239384a084593633d', 0),
(46, '74891811757887f892f050', 'Testinguser', 'Ghj', 'testinguser@gmail.com', '012cb9e300d24e14a7dc9030b48fc9cb724f814f774e9fe8d27d3f23cf2c0c89', 'media/672d798cbd9c20dc4d3b7defbc6de8d2.png', '2016-07-15 02:15:37', 'ce0ec5811c72da520b4638bcace606c749e06af0', 0),
(47, '131168530357972be7646cb', 'Saheli', 'Ghosh', 'saheli@capitalnumbers.com', 'e70844b23a08357f83b8c247b8d2f99eb20de5980df2159d790d446c044d7979', 'media/users/66737ee100086c428f26573dd096778f.jpg', '2016-07-26 05:22:47', 'c88fa7399ec65ac71afdaef41d3cdf3915836f4b', 1),
(48, '964357848579ed5f86a61f', 'Sdfcgvhbjn', 'Asdfghjk', 'asdfghj@xcvbn.vom', '3d7b6f7415609591cf8b0dd328b9489f683dd26dab9e02f935ee61e037e3fa08', 'media/users/1318015ea15e788de8de53b4033173a7.gif', '2016-08-01 00:54:16', '58b460bc02c6842b63ec8ed8f06a6a36fc4930ee', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stf_venue`
--

CREATE TABLE IF NOT EXISTS `stf_venue` (
`id` int(11) NOT NULL,
  `id_uniq` varchar(100) NOT NULL,
  `tx_name` varchar(100) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `id_city` varchar(50) DEFAULT NULL,
  `tx_address` varchar(200) DEFAULT NULL,
  `tx_zipcode` varchar(50) DEFAULT NULL,
  `tx_phone_prefix` varchar(10) NOT NULL,
  `tx_phone` varchar(50) NOT NULL,
  `tx_phone_prefix_2` varchar(10) DEFAULT NULL,
  `tx_phone_2` varchar(50) DEFAULT NULL,
  `tx_email` varchar(150) NOT NULL,
  `tx_website` varchar(150) DEFAULT NULL,
  `tx_facebook` varchar(150) DEFAULT NULL,
  `tx_twitter` varchar(150) DEFAULT NULL,
  `tx_instagram` varchar(150) DEFAULT NULL,
  `tx_contact_name` varchar(100) NOT NULL,
  `tx_contact_phone_prefix` varchar(10) NOT NULL,
  `tx_contact_phone` varchar(100) NOT NULL,
  `tx_latitude` varchar(50) NOT NULL,
  `tx_longitude` varchar(50) NOT NULL,
  `dt_register` datetime NOT NULL,
  `bo_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_venue`
--

INSERT INTO `stf_venue` (`id`, `id_uniq`, `tx_name`, `id_country`, `id_state`, `id_city`, `tx_address`, `tx_zipcode`, `tx_phone_prefix`, `tx_phone`, `tx_phone_prefix_2`, `tx_phone_2`, `tx_email`, `tx_website`, `tx_facebook`, `tx_twitter`, `tx_instagram`, `tx_contact_name`, `tx_contact_phone_prefix`, `tx_contact_phone`, `tx_latitude`, `tx_longitude`, `dt_register`, `bo_status`) VALUES
(1, '193022567756c5d68d116bf', 'At&t Stadium', 21, 43, NULL, '1 At&t Way, Arlington', '76011', '1', '8178924000', '', '', 'stadium@stadium.dallascowboys.com', 'http://stadium.dallascowboys.com/', '', '', '', 'Kline Young', '1', '9695342074', '32.7472844', '-97.09449389999997', '2016-02-18 09:34:53', 1),
(2, '1084983436570d104037e86', 'Michigan Stadium', 21, 22, NULL, '1201 South Main Street Ann Arbor', '48104', '1', '7346472583', '', '', 'stadium@mgoblue.com', 'http://www.mgoblue.com/facilities/michigan-stadium.html', '', '', '', 'Tiffany Vasquez', '1', '9685363822', '42.2658365', '-83.74869560000002', '2016-04-12 11:12:00', 1),
(3, '2088812811576b9c4ee1d32', 'Abc Stadium', 21, 1, 'XYZA', 'Xyz', '98765', '1', '1234567777', '1', '1222222228', 'abc@gmail.com', 'http://google.com', 'http://facebook.com', 'http://twitter.com', 'http://instagram.com', 'Bireshwar Goswami', '1', '8013788398', '40.148237', '-101.7625', '2016-06-23 04:22:39', 1),
(4, '156996039357846e1e8cdc2', 'Saheli', 21, 15, 'dfr', 'Sddddokm', '12222', '1', '111111111111', '1', '1111111111111111', 'saheli@gmail.com', 'http://hjjjjjjj.com', '', '', '', 'Dsssss', '1', '111111111111111111', '22.572646', '88.36389499999996', '2016-07-12 00:12:14', 0),
(5, '116405208757847491653c3', 'Saaaa', 21, 17, 'aaaaa', 'Aaaaaa', '12345', '1', '3333333333', '1', '3333333333333333', 'dfff@dm.com', '', '', '', '', 'Dfffff', '1', '334444444439', '22.572646', '88.36389499999996', '2016-07-12 00:39:45', 0),
(6, '199493933257847861eeaa2', 'Ssaa', 21, 16, 'aasas', 'Assasas', '21212', '1', '12121212121', '1', '12121212121', 'sdddd@gmail.com', 'sdsdsdd.vom', '', '', '', 'Scssc', '1', '2233333333331', '52.557716', '1.7108369999999695', '2016-07-12 00:56:01', 0),
(7, '69790056357847a7cc025b', 'Fhjhjhhd', 21, 3, 'ddddd', 'Dddd', '22222', '1', '222222222222222200', '1', '22222222222222224', 'gdhghgh@hjhjh.com', 'www.gmailcom', '', '', '', 'Gdgdfffdc', '1', '33333333329', '40.148237', '-101.7625', '2016-07-12 01:05:00', 0),
(8, '164765313157847ee04aab3', 'Sssssss', 21, 2, 'ssssssss', 'Sssss', '11111', '1', '11111111111', '1', '111111111111', 'sssssss@dddd.vom', '', '', '', '', 'Ssssss', '1', '222222222222', '22.572646', '88.36389499999996', '2016-07-12 01:23:44', 0),
(9, '76708270578481c5694e2', 'Ssfsfgssg', 21, 15, 'xddddddd', 'Ddddddd', '12334', '1', '33455333333333332', '1', '7777777777', 'dgddgddhg@fgfff.com', 'wwwwhhw.com', '', '', '', 'Ddddd', '1', '333333333333332', '22.572646', '88.36389499999996', '2016-07-12 01:36:05', 0),
(10, '794124171578483c2304ff', 'Ssssss', 21, 2, 'ssss', 'Sssss', '11111', '1', '1111111111111', '1', '44444444444', 'sssssss@ddhhh.com', 'wwwwhhw.com', '', '', '', 'Ssssss', '1', '22222222225', '50.937531', '6.960278600000038', '2016-07-12 01:44:34', 0),
(11, '2046174131578492fd808f0', 'Mera', 21, 2, 'deeeeee', 'Eeeeeeeee', '22222', '1', '222222222222', '1', '22222222222', 'shaheheheh@gmail.com', 'wwwww.gmail.com', '', '', '', 'Saaaaa', '1', '222222222220', '22.572646', '88.36389499999996', '2016-07-12 02:49:33', 0),
(12, '103975607157849401c1f84', 'Meravenue', 21, 17, 'ssssss', 'Sssss', '11111', '1', '222222222222222', '1', '222222222222222200', 'sddd@gggg.vom', 'eeee', '', '', '', 'Dddd', '1', '22173333327', '40.148237', '-101.7625', '2016-07-12 02:53:53', 0),
(13, '17911884015788800e71a6d', 'Tests', 21, 1, 'cddd', 'Cccccc', '11111', '1', '644442112121', '1', '31546545454543', 'dffdf@gm.vom', 'google.com', '', '', '', 'Sddfff', '1', '564644454451', '22.572646', '88.36389499999996', '2016-07-15 02:17:50', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stf_zone`
--

CREATE TABLE IF NOT EXISTS `stf_zone` (
`id` int(11) NOT NULL,
  `tx_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stf_zone`
--

INSERT INTO `stf_zone` (`id`, `tx_name`) VALUES
(1, 'Europe'),
(2, 'North America'),
(3, 'Asia'),
(4, 'Africa'),
(5, 'Oceania'),
(6, 'South America'),
(7, 'Europe (non-EU)'),
(8, 'Central America/Antilla');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `stf_access`
--
ALTER TABLE `stf_access`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stf_age_range`
--
ALTER TABLE `stf_age_range`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stf_anthropometric`
--
ALTER TABLE `stf_anthropometric`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_uniq` (`id_uniq`), ADD KEY `id_player` (`id_player`), ADD KEY `id_event` (`id_event`);

--
-- Indexes for table `stf_category`
--
ALTER TABLE `stf_category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stf_country`
--
ALTER TABLE `stf_country`
 ADD PRIMARY KEY (`id`), ADD KEY `idx_stf_country_id_zone` (`id_zone`);

--
-- Indexes for table `stf_dribbling_circular`
--
ALTER TABLE `stf_dribbling_circular`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_event` (`id_event`), ADD UNIQUE KEY `id_uniq` (`id_uniq`), ADD KEY `id_player` (`id_player`);

--
-- Indexes for table `stf_dribbling_zigzag`
--
ALTER TABLE `stf_dribbling_zigzag`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_uniq` (`id_uniq`), ADD KEY `id_player` (`id_player`), ADD KEY `id_event` (`id_event`) USING BTREE;

--
-- Indexes for table `stf_event`
--
ALTER TABLE `stf_event`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_uniq` (`id_uniq`), ADD KEY `id_venue` (`id_venue`), ADD KEY `id_team` (`id_team`), ADD KEY `id_measurement` (`id_measurement`);

--
-- Indexes for table `stf_event_configuration`
--
ALTER TABLE `stf_event_configuration`
 ADD PRIMARY KEY (`id_event`,`id_configuration`);

--
-- Indexes for table `stf_gender`
--
ALTER TABLE `stf_gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stf_heading_aim`
--
ALTER TABLE `stf_heading_aim`
 ADD PRIMARY KEY (`id`) USING BTREE, ADD UNIQUE KEY `id_uniq` (`id_uniq`), ADD KEY `id_player` (`id_player`), ADD KEY `id_event` (`id_event`);

--
-- Indexes for table `stf_heading_force`
--
ALTER TABLE `stf_heading_force`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_uniq` (`id_uniq`), ADD KEY `id_player` (`id_player`), ADD KEY `id_event` (`id_event`);

--
-- Indexes for table `stf_heading_jump`
--
ALTER TABLE `stf_heading_jump`
 ADD PRIMARY KEY (`id`) USING BTREE, ADD UNIQUE KEY `id_uniq` (`id_uniq`), ADD KEY `id_player` (`id_player`), ADD KEY `id_event` (`id_event`);

--
-- Indexes for table `stf_image`
--
ALTER TABLE `stf_image`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stf_kicking_aim`
--
ALTER TABLE `stf_kicking_aim`
 ADD PRIMARY KEY (`id`) USING BTREE, ADD UNIQUE KEY `id_uniq` (`id_uniq`), ADD KEY `id_player` (`id_player`), ADD KEY `id_event` (`id_event`);

--
-- Indexes for table `stf_kicking_force`
--
ALTER TABLE `stf_kicking_force`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_uniq` (`id_uniq`), ADD KEY `id_player` (`id_player`), ADD KEY `id_event` (`id_event`);

--
-- Indexes for table `stf_measurement`
--
ALTER TABLE `stf_measurement`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stf_movement_lateral`
--
ALTER TABLE `stf_movement_lateral`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_uniq` (`id_uniq`), ADD KEY `id_player` (`id_player`,`id_event`);

--
-- Indexes for table `stf_movement_linear`
--
ALTER TABLE `stf_movement_linear`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_uniq` (`id_uniq`), ADD KEY `id_player` (`id_player`,`id_event`);

--
-- Indexes for table `stf_movement_zigzag`
--
ALTER TABLE `stf_movement_zigzag`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_uniq` (`id_uniq`), ADD KEY `id_player` (`id_player`,`id_event`);

--
-- Indexes for table `stf_player`
--
ALTER TABLE `stf_player`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `idx_stf_player_id_uniq` (`id_uniq`), ADD KEY `idx_stf_player_id_gender` (`id_gender`), ADD KEY `idx_stf_player_id_country` (`id_country`), ADD KEY `idx_stf_player_id_state` (`id_state`) USING BTREE;

--
-- Indexes for table `stf_player_image`
--
ALTER TABLE `stf_player_image`
 ADD PRIMARY KEY (`id`), ADD KEY `idx_stf_player_image_id_player` (`id_player`), ADD KEY `idx_stf_player_image_id_image` (`id_image`) USING BTREE, ADD KEY `idx_stf_player_image_id_type_image` (`id_type_image`);

--
-- Indexes for table `stf_position`
--
ALTER TABLE `stf_position`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stf_section`
--
ALTER TABLE `stf_section`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stf_state`
--
ALTER TABLE `stf_state`
 ADD PRIMARY KEY (`id`), ADD KEY `idx_stf_state_id_country` (`id_country`);

--
-- Indexes for table `stf_station`
--
ALTER TABLE `stf_station`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_uniq` (`id_uniq`);

--
-- Indexes for table `stf_station_configuration`
--
ALTER TABLE `stf_station_configuration`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stf_station_test`
--
ALTER TABLE `stf_station_test`
 ADD PRIMARY KEY (`id_station`,`id_test`);

--
-- Indexes for table `stf_team`
--
ALTER TABLE `stf_team`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `idx_stf_team_id_uniq` (`id_uniq`), ADD KEY `idx_stf_team_id_state` (`id_state`) USING BTREE, ADD KEY `idx_stf_team_id_country` (`id_country`) USING BTREE;

--
-- Indexes for table `stf_team_category`
--
ALTER TABLE `stf_team_category`
 ADD PRIMARY KEY (`id`), ADD KEY `idx_stf_team_category_id_category` (`id_category`), ADD KEY `idx_stf_team_category_id_gender` (`id_gender`), ADD KEY `idx_stf_team_category_id_team` (`id_team`) USING BTREE;

--
-- Indexes for table `stf_team_player`
--
ALTER TABLE `stf_team_player`
 ADD PRIMARY KEY (`id`), ADD KEY `idx_stf_team_player_id_team` (`id_team`), ADD KEY `idx_stf_team_player_id_player` (`id_player`);

--
-- Indexes for table `stf_team_player_position`
--
ALTER TABLE `stf_team_player_position`
 ADD PRIMARY KEY (`id`), ADD KEY `idx_stf_team_player_position_id_team_player` (`id_team_player`), ADD KEY `idx_stf_team_player_position_id_position` (`id_position`) USING BTREE;

--
-- Indexes for table `stf_test`
--
ALTER TABLE `stf_test`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stf_type_image`
--
ALTER TABLE `stf_type_image`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stf_user`
--
ALTER TABLE `stf_user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `idx_stf_user_id_uniq` (`id_uniq`);

--
-- Indexes for table `stf_venue`
--
ALTER TABLE `stf_venue`
 ADD PRIMARY KEY (`id`), ADD KEY `idx_stf_venue_id_country` (`id_country`), ADD KEY `idx_stf_venue_id_state` (`id_state`);

--
-- Indexes for table `stf_zone`
--
ALTER TABLE `stf_zone`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `stf_access`
--
ALTER TABLE `stf_access`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=428;
--
-- AUTO_INCREMENT for table `stf_age_range`
--
ALTER TABLE `stf_age_range`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `stf_anthropometric`
--
ALTER TABLE `stf_anthropometric`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `stf_category`
--
ALTER TABLE `stf_category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `stf_country`
--
ALTER TABLE `stf_country`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=245;
--
-- AUTO_INCREMENT for table `stf_dribbling_circular`
--
ALTER TABLE `stf_dribbling_circular`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `stf_dribbling_zigzag`
--
ALTER TABLE `stf_dribbling_zigzag`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `stf_event`
--
ALTER TABLE `stf_event`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `stf_gender`
--
ALTER TABLE `stf_gender`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stf_heading_aim`
--
ALTER TABLE `stf_heading_aim`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `stf_heading_force`
--
ALTER TABLE `stf_heading_force`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `stf_heading_jump`
--
ALTER TABLE `stf_heading_jump`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `stf_image`
--
ALTER TABLE `stf_image`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `stf_kicking_aim`
--
ALTER TABLE `stf_kicking_aim`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `stf_kicking_force`
--
ALTER TABLE `stf_kicking_force`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `stf_measurement`
--
ALTER TABLE `stf_measurement`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stf_movement_lateral`
--
ALTER TABLE `stf_movement_lateral`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `stf_movement_linear`
--
ALTER TABLE `stf_movement_linear`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `stf_movement_zigzag`
--
ALTER TABLE `stf_movement_zigzag`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stf_player`
--
ALTER TABLE `stf_player`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `stf_player_image`
--
ALTER TABLE `stf_player_image`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `stf_position`
--
ALTER TABLE `stf_position`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `stf_section`
--
ALTER TABLE `stf_section`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `stf_state`
--
ALTER TABLE `stf_state`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=313;
--
-- AUTO_INCREMENT for table `stf_station`
--
ALTER TABLE `stf_station`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `stf_station_configuration`
--
ALTER TABLE `stf_station_configuration`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `stf_team`
--
ALTER TABLE `stf_team`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `stf_team_category`
--
ALTER TABLE `stf_team_category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `stf_team_player`
--
ALTER TABLE `stf_team_player`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `stf_team_player_position`
--
ALTER TABLE `stf_team_player_position`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `stf_test`
--
ALTER TABLE `stf_test`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `stf_type_image`
--
ALTER TABLE `stf_type_image`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stf_user`
--
ALTER TABLE `stf_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `stf_venue`
--
ALTER TABLE `stf_venue`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `stf_zone`
--
ALTER TABLE `stf_zone`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
