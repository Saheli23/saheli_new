var stf = angular.module('TeamControllers', []);


stf.controller('TeamListController', ['$scope', 'Team', 'Access', 'STFcache', 'Auth', function ($scope, Team, Access, STFcache, Auth) {
	
   $scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};


	$scope.getTeams = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('teams');
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.teams = null;
		$scope.loadingTeams = true;
		$scope.loadingTeamsAlert = false;

		Team.getTeams().then(function (res) {
			$scope.loadingTeams = false;
			if (angular.isObject(res.data.response)) {
				$scope.teams = res.data.response;
				
			} else {
				$scope.loadingTeamsAlert = {
					type: 'success',
					message: res.data.response
				};
			}
			$scope.getAccess(3);
		}, function (err) {

			$scope.loadingTeams = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTeams();

	$scope.deleteTeam = function( team_id, ind ){
		if(confirm("Are you sure to delete this Team?")){
			var data = $.param({id_team:team_id,type:'delete'});
			Team.delete(data).then(function (res) {
				$scope.loadingTeams = false;
				
				$scope.getTeams(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingTeamsAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {

				$scope.loadingTeams = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingTeamsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};

	$scope.bulkDeleteTeam = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Team?")){
			var data = $.param({id_team:values,type:'delete',action:action_id});
			Team.multidelete(data).then(function (res) {
				$scope.loadingTeams = false;
				
				$scope.getTeams(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingTeamsAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {

				$scope.loadingTeams = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingTeamsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one user to delete.");
	 }
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
			changeAttr( $scope.teams, 'teams', $scope.access.bo_view );
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

	$scope.restrictView = function(){
		$('#alertMessage').show();
		setTimeout(function(){ 
			$('#alertMessage').hide();
		}, 2000);
	}

	function compile(element){
		var el = angular.element(element);    
		$scope = el.scope();
		$injector = el.injector();
		$injector.invoke(function($compile){
			$compile(el)($scope);
		})     
	}

	function changeAttr( obj, slug, viewAccess ){
        for( var i=0; i < obj.length; i++ ){
            var el = document.getElementById((slug.substring(0, slug.length-1))+"_id_"+i);
            if( viewAccess ){
                el.removeAttribute("ng-click");
                el.setAttribute("ng-href",slug+"/"+obj[i].id_uniq);
                el.setAttribute("href",slug+"/"+obj[i].id_uniq);
            } else {
                el.removeAttribute("ng-href");
                el.removeAttribute("href");
                el.setAttribute("ng-click", "restrictView()");
                compile(el);
            }
        }
    }


	$scope.orderOptions = [
		{ value: 'tx_name', text: 'Name' },
		{ value: 'tx_email', text: 'Email' },
		{ value: 'tx_phone', text: 'Phone number' }
	];

}]);

stf.controller('TeamCreateController', ['$scope', '$timeout', '$location', '$filter', '$rootScope', '$window', 'Country', 'Auth', 'Gender', 'Team', 'STFcache', function ($scope, $timeout, $location, $filter, $rootScope, $window, Country, Auth, Gender, Team, STFcache) {
	
	 angular.element('input[type=number]').on("keypress",function (evt) {
    	    if (String.fromCharCode(evt.which) == "e"){
        	return false;
            }
            if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length >1){ 
              //alert($(this).val().split(".")[1].length);
             // $(this).val()
            // alert("gy");
                //if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat($(this).val().slice(0, -1));
            }  
         }    

			});


	$scope.formdata = {
		name: '',
		country: '',
		state: '',
		city:'',
		address: '',
		phoneprefix: '-',
		phonenumber: '',
		zipcode: '',
		email: '',
		website: '',
		facebook: '',
		instagram: '',
		twitter: '',
		userfile: '',
		categories: []
	}

	$scope.categoriesdata = {
		id_category: '',
		category: '',
		id_gender: '',
		gender: '',
		nu_players: ''
	};

	$scope.zipcodeMask = "?";

	$scope.getCountries = function () {

		$scope.loadingCountries = true;
		$scope.loadingCountriesAlert = false;

		Country.getCountries().then(function (res) {

			$scope.loadingCountries = false;

			if (angular.isObject(res.data.response)) {

				$scope.countries = res.data.response;

			} else {

				$scope.loadingCountriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCountries = false;

			if (err.status == 401) { 

				Auth.deleteToken();

			} else {

				$scope.loadingCountriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountryStates = function (id_country) {

		$scope.loadingStates = true;
		$scope.loadingStatesAlert = false;

		Country.getCountryStates(id_country).then(function (res) {

			$scope.loadingStates = false;

			if (angular.isObject(res.data.response)) {

				$scope.states = res.data.response;

			} else {

				$scope.loadingStatesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStates = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStatesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCategories = function () {

		$scope.loadingCategories = true;
		$scope.loadingCategoriesAlert = false;

		Team.getCategories().then(function (res) {

			$scope.loadingCategories = false;

			if (angular.isObject(res.data.response)) {

				$scope.categories = res.data.response;

			} else {

				$scope.loadingCategoriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCategories = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingCategoriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getGenders = function () {

		$scope.loadingGenders = true;
		$scope.loadingGendersAlert = false;

		Gender.getGenders().then(function (res) {

			$scope.loadingGenders = false;

			if (angular.isObject(res.data.response)) {

				$scope.genders = res.data.response;

			} else {

				$scope.loadingGendersAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingGenders = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingGendersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};	

	$scope.getCountries();
	$scope.getCategories();
	$scope.getGenders();

	$scope.$watch('formdata.country', function () {

		var countryIndex = _.findIndex($scope.countries, { id: $scope.formdata.country });

		if (countryIndex != -1) {

			$scope.zipcodeMask = $filter('zipcodeMask')($scope.countries[countryIndex].tx_zipcode_format);
			$scope.zipcodeRequired = ($scope.zipcodeMask.length > 0) ? true : false;

			$scope.formdata.phoneprefix = $scope.countries[countryIndex].tx_phone_prefix;

			var id_country = $scope.formdata.country;
			$scope.getCountryStates(id_country);

		};

		$scope.states = null;
		$scope.formdata.state = '';
		$scope.CreateTeamForm.state.$setPristine();

	});

	$scope.addCategory = function () {

		$scope.showTooltip = false;

		var categoryData = {
			id_category: $scope.categoriesdata.id_category,
			category: $scope.categories[$scope.categoriesdata.id_category - 1].tx_name,
			id_gender: $scope.categoriesdata.id_gender,
			gender: $scope.genders[_.findIndex($scope.genders, { id: $scope.categoriesdata.id_gender })].tx_name,
			nu_players: $scope.categoriesdata.nu_players
		};

		var dataIndexToLookFor = {
			id_category: $scope.categoriesdata.id_category,
			id_gender: $scope.categoriesdata.id_gender
		};

		if (_.findIndex($scope.formdata.categories, dataIndexToLookFor) == -1) {

			$scope.formdata.categories.push(categoryData);

			$scope.categoriesdata.id_category = '',
			$scope.categoriesdata.category = '',
			$scope.categoriesdata.id_gender = '',
			$scope.categoriesdata.gender = '',
			$scope.categoriesdata.nu_players = ''

			$scope.TeamCategoriesForm.$setPristine();
			
		} else {

			$scope.showTooltip = true;

			$timeout(function () {

				$scope.showTooltip = false;				

			}, 2000);

		}

	};

	$scope.deleteCategory = function (index) {

		$scope.formdata.categories.splice(index, 1);

	};

	$scope.createTeam = function () {

		$scope.creatingTeam = true;
		$scope.creatingTeamAlert = false;

		var data = new FormData();

		data.append('name', $scope.formdata.name);
		data.append('country', $scope.formdata.country);
		data.append('state', $scope.formdata.state);
		data.append('city', $scope.formdata.city);
		data.append('address', $scope.formdata.address);
		data.append('phoneprefix', $scope.formdata.phoneprefix);
		data.append('phonenumber', $scope.formdata.phonenumber);
		data.append('zipcode', $scope.formdata.zipcode);
		data.append('email', $scope.formdata.email);
		data.append('website', $scope.formdata.website);
		data.append('facebook', $scope.formdata.facebook);
		data.append('instagram', $scope.formdata.instagram);
		data.append('twitter', $scope.formdata.twitter);
		data.append('userfile', $scope.formdata.userfile);
		data.append('categories', angular.toJson($scope.formdata.categories));

		Team.createTeam(data).then(function (res) {

			$scope.creatingTeam = false;

			if (res.data.status == "success") {

				STFcache.delete('teams');
				STFcache.delete('teamUniqIDs');

				if (res.data.has_image == 1) {

					$window.sessionStorage.setItem('response', res.data.response);

					$location.path('/set_image/tea_/' + res.data.id_uniq);

				} else {

					$rootScope.tooltipMessageAlert = {
						type: 'success',
						message: res.data.response
					}

					$location.path('/teams');

				}

			} else {

				$scope.creatingTeamAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingTeam = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('TeamDetailsController', ['$scope', '$routeParams', '$location', 'Team', 'Access', 'STFcache', 'Auth', function ($scope, $routeParams, $location, Team, Access, STFcache, Auth) {
	
	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getUniqIDs = function () {

		$scope.loadingUniqIDs = true;
		$scope.loadingUniqIDsAlert = false;

		Team.getUniqIDs().then(function (res) {

			$scope.loadingUniqIDs = false;

			$scope.result = res.data.response;
			$scope.totalItems = parseInt($scope.result.length);
			$scope.currentItem = parseInt(_.findIndex($scope.result, { id_uniq: $scope.id_uniq }));
			$scope.nextItem = $scope.currentItem + 1;	
			$scope.prevItem = $scope.currentItem - 1;

			$scope.nextID = ($scope.nextItem > -1 && $scope.nextItem < $scope.totalItems) ? $scope.result[$scope.nextItem].id_uniq : false;
			$scope.prevID = ($scope.prevItem >= 0) ? $scope.result[$scope.prevItem].id_uniq : false;

		}, function (err) {

			$scope.loadingUniqIDs = false;

			if (err.status == 401) {

				Auth.deleteToken();

			};

		});

	};

	$scope.prevTeam = function () { $location.path('/teams/' + $scope.prevID); };
	$scope.nextTeam = function () { $location.path('/teams/' + $scope.nextID); };

	$scope.getTeam = function (id_uniq) {

		$scope.loadingTeam = true;
		$scope.loadingTeamAlert = false;

		Team.getTeam(id_uniq).then(function (res) {

			$scope.loadingTeam = false;

			if (angular.isObject(res.data.response)) {

				$scope.team = res.data.response;
				$scope.getUniqIDs();
				$scope.getAccess(3);

			} else {

				$scope.loadingTeamAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTeam = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTeam($scope.id_uniq);

	$scope.getTeams = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('teams');

		$scope.teams = null;
		$scope.loadingTeams = true;
		$scope.loadingTeamsAlert = false;

		Team.getTeams().then(function (res) {
			$scope.loadingTeams = false;
			if (angular.isObject(res.data.response)) {
				$scope.teams = res.data.response;
				$scope.getAccess(3);
			} else {
				$scope.loadingTeamsAlert = {
					type: 'success',
					message: res.data.response
				};
			}
		}, function (err) {

			$scope.loadingTeams = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.deleteTeam = function( team_id, ind ){
		if(confirm("Are you sure to delete this Team?")){
			var data = $.param({id_team:team_id,type:'delete'});
			Team.delete(data).then(function (res) {
				$scope.loadingTeams = false;
				
				$scope.getTeams(true);

				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingTeamsAlert = {type: res.data.status,message: res.data.response};
				   $location.path('/teams');
				}

			}, function (err) {

				$scope.loadingTeams = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingTeamsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

}]);

stf.controller('TeamEditController', ['$scope', '$timeout', '$location', '$filter', '$routeParams', '$rootScope', '$window', 'Country', 'Auth', 'Gender', 'Team', 'STFcache', function ($scope, $timeout, $location, $filter, $routeParams, $rootScope, $window, Country, Auth, Gender, Team, STFcache) {
	
	 angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
         //   if (String.fromCharCode(evt.which) == "."){
        	// return false;
         //    }
           var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;

			});

	$scope.formdata = {
		name: '',
		country: '',
		state: '',
	    city:'',
		address: '',
		phoneprefix: '-',
		phonenumber: '',
		zipcode: '',
		email: '',
		website: '',
		facebook: '',
		instagram: '',
		twitter: '',
		userfile: '',
		categories: []
	}

	$scope.categoriesdata = {
		id_category: '',
		category: '',
		id_gender: '',
		gender: '',
		nu_players: ''
	};

	$scope.zipcodeMask = "?";

	var id_uniq = $routeParams.id_uniq;

	$scope.getTeam = function (id_uniq) {

		$scope.loadingTeam = true;
		$scope.loadingTeamAlert = false;

		Team.getTeam(id_uniq).then(function (res) {

			$scope.loadingTeam = false;

			if (angular.isObject(res.data.response)) {

				$scope.team = res.data.response;

				$scope.formdata.name = $scope.team.tx_name;
				$scope.formdata.country = $scope.team.id_country;
				$scope.formdata.state = $scope.team.id_state;
				$scope.formdata.city = $scope.team.tx_city;
				$scope.formdata.address = $scope.team.tx_address;
				$scope.formdata.prefix = $scope.team.tx_phone_prefix;
				$scope.formdata.phonenumber = parseFloat($scope.team.tx_phone);
				$scope.formdata.email = $scope.team.tx_email;
				$scope.formdata.website = $scope.team.tx_website;
				$scope.formdata.facebook = $scope.team.tx_facebook;
				$scope.formdata.instagram = $scope.team.tx_instagram;
				$scope.formdata.twitter = $scope.team.tx_twitter;
				
				angular.forEach($scope.team.categories, function (value, key) {

					$scope.formdata.categories.push({
						id_category: value.id_category,
						category: value.name,
						id_gender: value.id_gender,
						gender: value.gender,
						nu_players: value.nu_players
					});

				});

			} else {

				$scope.loadingTeamAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTeam = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountries = function () {

		$scope.loadingCountries = true;
		$scope.loadingCountriesAlert = false;

		Country.getCountries().then(function (res) {

			$scope.loadingCountries = false;

			if (angular.isObject(res.data.response)) {

				$scope.countries = res.data.response;
				$scope.$emit('countriesLoaded');

			} else {

				$scope.loadingCountriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCountries = false;

			if (err.status == 401) { 

				Auth.deleteToken();

			} else {

				$scope.loadingCountriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountryStates = function (id_country) {

		$scope.loadingStates = true;
		$scope.loadingStatesAlert = false;

		Country.getCountryStates(id_country).then(function (res) {

			$scope.loadingStates = false;

			if (angular.isObject(res.data.response)) {

				$scope.states = res.data.response;

			} else {

				$scope.loadingStatesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStates = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStatesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCategories = function () {

		$scope.loadingCategories = true;
		$scope.loadingCategoriesAlert = false;

		Team.getCategories().then(function (res) {

			$scope.loadingCategories = false;

			if (angular.isObject(res.data.response)) {

				$scope.categories = res.data.response;

			} else {

				$scope.loadingCategoriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCategories = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingCategoriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getGenders = function () {

		$scope.loadingGenders = true;
		$scope.loadingGendersAlert = false;

		Gender.getGenders().then(function (res) {

			$scope.loadingGenders = false;

			if (angular.isObject(res.data.response)) {

				$scope.genders = res.data.response;

			} else {

				$scope.loadingGendersAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingGenders = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingGendersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};	

	$scope.getTeam(id_uniq);
	$scope.getCountries();
	$scope.getCategories();
	$scope.getGenders();

	$scope.$on('countriesLoaded', function () {

		$scope.$watch('formdata.country', function () {

			var countryIndex = _.findIndex($scope.countries, { id: $scope.formdata.country });

			if (countryIndex != -1) {

				$scope.zipcodeMask = $filter('zipcodeMask')($scope.countries[countryIndex].tx_zipcode_format);
				$scope.zipcodeRequired = ($scope.zipcodeMask.length > 0) ? true : false;

				$timeout(function () { $scope.formdata.zipcode = $scope.team.tx_zipcode; }, 1000);

				$scope.formdata.phoneprefix = $scope.countries[countryIndex].tx_phone_prefix;

				var id_country = $scope.formdata.country;
				$scope.getCountryStates(id_country);

			} else {

				$scope.states = null;
				$scope.formdata.state = '';
				$scope.EditTeamForm.state.$setPristine();

			}

		});

	})

	$scope.addCategory = function () {

		$scope.showTooltip = false;

		var categoryData = {
			id_category: $scope.categoriesdata.id_category,
			category: $scope.categories[$scope.categoriesdata.id_category - 1].tx_name,
			id_gender: $scope.categoriesdata.id_gender,
			gender: $scope.genders[_.findIndex($scope.genders, { id: $scope.categoriesdata.id_gender })].tx_name,
			nu_players: $scope.categoriesdata.nu_players
		};

		var dataIndexToLookFor = {
			id_category: $scope.categoriesdata.id_category,
			id_gender: $scope.categoriesdata.id_gender
		};

		if (_.findIndex($scope.formdata.categories, dataIndexToLookFor) == -1) {

			$scope.formdata.categories.push(categoryData);

			$scope.categoriesdata.id_category = '',
			$scope.categoriesdata.category = '',
			$scope.categoriesdata.id_gender = '',
			$scope.categoriesdata.gender = '',
			$scope.categoriesdata.nu_players = ''

			$scope.TeamCategoriesForm.$setPristine();
			
		} else {

			$scope.showTooltip = true;

			$timeout(function () {

				$scope.showTooltip = false;				

			}, 2000);

		}

	};

	$scope.deleteCategory = function (index) {

		$scope.formdata.categories.splice(index, 1);

	};

	$scope.editTeam = function () {

		$scope.editingTeam = true;
		$scope.editingTeamAlert = false;

		var data = new FormData();

		data.append('name', $scope.formdata.name);
		data.append('country', $scope.formdata.country);
		data.append('state', $scope.formdata.state);
		data.append('address', $scope.formdata.address);
		data.append('phoneprefix', $scope.formdata.phoneprefix);
		data.append('phonenumber', $scope.formdata.phonenumber);
		data.append('zipcode', $scope.formdata.zipcode);
		data.append('email', $scope.formdata.email);
		data.append('website', $scope.formdata.website);
		data.append('facebook', $scope.formdata.facebook);
		data.append('instagram', $scope.formdata.instagram);
		data.append('twitter', $scope.formdata.twitter);
		data.append('userfile', $scope.formdata.userfile);
		data.append('categories', angular.toJson($scope.formdata.categories));

		Team.editTeam(id_uniq, data).then(function (res) {

			$scope.editingTeam = false;
			
			if (res.data.status == "success") {

				STFcache.delete('teams');
				STFcache.delete('teams' + id_uniq);

				if (res.data.has_image == 1) {

					$window.sessionStorage.setItem('response', res.data.response);

					$location.path('/set_image/tea_/' + res.data.id_uniq);

				} else {

					$rootScope.tooltipMessageAlert = {
						type: 'success',
						message: res.data.response
					}

					$location.path('/teams');

				}

			} else {

				$scope.editingTeamAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.editingTeam = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingTeamAlert = {
					type: 'danger',
					message: err.status + ' ' +err.statusText
				}

			}

		});

	};
}]);