var stf = angular.module('DribblingControllers', []);

stf.controller('DribblingListController', ['$scope', '$routeParams', '$location', 'Event', 'Dribbling', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, $location, Event, Dribbling, Access, Auth, STFcache) {
	
	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;

				$scope.getAccess(1);

				if (_.findIndex($scope.event.configurations, { station: 'Dribbling' }) >= 0) {

 					$scope.$emit('eventLoaded');

				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

	$scope.getEvent($scope.id_uniq);
	
}]);

stf.controller('DribblingCreateController', ['$scope', '$routeParams', '$location', '$rootScope', 'Event', 'Player', 'Dribbling', 'Auth', 'STFcache', function ($scope, $routeParams, $location, $rootScope, Event, Player, Dribbling, Auth, STFcache) {
	
	$scope.id_uniq = $routeParams.id_uniq;
	$scope.player = null;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	$scope.date = null;
	$scope.maxDate = new Date();
	$scope.pickDateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: true,
		selectMonths: true,
	};

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;

				if (_.findIndex($scope.event.configurations, { station: 'Dribbling' }) >= 0) {

 					/*$scope.aimdata.id_event = $scope.event.id;
 					$scope.forcedata.id_event = $scope.event.id;*/

				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getEvent($scope.id_uniq);

}]);