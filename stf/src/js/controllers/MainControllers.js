var stf = angular.module('MainControllers', []);

stf.controller('LoginController', ['$scope', '$location', 'Auth', function ($scope, $location, Auth) {
	
	$scope.formdata = {
		email: '',
		password: ''
	};

	$scope.login = function () {

		$scope.loginAlert = false;
		$scope.loginUserIn = true;

		var data = $.param($scope.formdata);

		Auth.login(data).then(function (res) {

			$scope.loginUserIn = false;
			
			if (res.data.status == "success") {

				Auth.createToken(res.data.response);
				$location.path('dashboard');

			} else {

				$scope.loginAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loginUserIn = false;
			$scope.loginAlert = {
				type: 'danger',
				message: err.status + ' ' + err.statusText
			};

		});

	};

}]);

stf.controller('PasswordForgotController', ['$scope','Forgot', function ($scope,Forgot) {
	$scope.formdata = {
		email: ''
		
	};

	$scope.passwordforgot = function () {

		$scope.ForgotPassAlert = false;
		$scope.ForgotPass = true;

		var data = $.param($scope.formdata);

		Forgot.passwordforgot(data).then(function (res) {

			$scope.ForgotPass = false;
			
			if (res.data.status == "success") {

				$scope.ForgotPassAlert = {
					type: 'success',
					message: res.data.response
				};
				//$location.path('dashboard');

			} else {

				$scope.ForgotPassAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.ForgotPass = false;
			$scope.ForgotPassAlert = {
				type: 'danger',
				message: err.status + ' ' + err.statusText
			};

		});

	};
}]);

stf.controller('PasswordRecoverController', ['$scope', function ($scope) {
	
}]);

stf.controller('tooltipMessageAlertController', ['$scope', '$rootScope', '$timeout', function ($scope, $rootScope, $timeout) {
	
	$timeout(function () {

		$rootScope.tooltipMessageAlert = false;

	}, 4000);

}]);

stf.controller('HistoryController', ['$scope', '$window', function ($scope, $window) {
	
	$scope.goBack = function () {

		$window.history.back();

	};

}]);

stf.controller('topbarMenuController', ['$scope', 'Auth', 'User', 'Section','STFcache', function ($scope, Auth, User, Section, STFcache ) {

	$scope.sections = function(refresh){
		if (refresh !== undefined && refresh == true) STFcache.delete('sections');
		var arr = [];
		Section.getSection().then(function(res){
			angular.forEach(res.data.response, function(value, key) {
				arr.push({
					id: value.id,
					name: value.section_name,
					link: value.section_link,
					icon: value.section_icon,
				});
			});
		});
		return arr;
	}(true);

	$scope.logout = function () { Auth.deleteToken(); }

	$scope.getLoggedInUser = function () {

		$scope.loadingLoggedInUser = true;

		User.getLoggedInUser().then(function (res) {

			$scope.loadingLoggedInUser = false;
			$scope.loggedInUser = res.data.response;
			
		}, function (err) {

			$scope.loadingLoggedInUser = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loggedInUser = err.status + ' ' + err.statusText;

			}

		});

	};

	$scope.getLoggedInUser();

	$scope.sideMenuOpen = false;

	$scope.toggleMenu = function () { $scope.sideMenuOpen = !$scope.sideMenuOpen };

	angular.element('#side-menu').perfectScrollbar({
		suppressScrollX: true
	});

}]);

stf.controller('setImageController', ['$scope',  '$location', '$rootScope', '$window', '$routeParams', '$timeout', 'Team', 'User', 'Player', 'Image', 'Auth', function ($scope, $location, $rootScope, $window, $routeParams, $timeout, Team, User, Player, Image, Auth) {
	
	var owners = ['tea_', 'pla_', 'use_'];
	var owner = $routeParams.owner;
	var id_uniq = $routeParams.id_uniq;
	var redirectURL;

	$scope.formdata = {
		owner: owner,
		id_uniq: id_uniq,
		xaxis: '',
		yaxis: '',
		width: '',
		height: ''
	};

	$scope.playerdata = {
		head: {
			id: 0,
			xaxis: 0,
			yaxis: 0,
			width: 0,
			height: 0
		},
		body: {
			id: 0,
			xaxis: 0,
			yaxis: 0,
			width: 0,
			height: 0
		}
	};

	$scope.getTeamImage = function (id_uniq) {

		$scope.loadingImage = true;
		$scope.loadingImageAlert = false;

		Team.getBaseImage(id_uniq).then(function (res) {

			$scope.loadingImage = false;
			
			if (res.data.is_image == 1) {

				$scope.image = res.data.response;

				$timeout(function () {

					$scope.$emit('imgReady');

				}, 500);

			} else {

				$scope.loadingImageAlert = {
					type: 'success',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.loadingImage = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingImageAlert = {
					type: 'danger',
					message: err.statusText
				}

			}

		});

	};

	$scope.getUserImage = function (id_uniq) {

		$scope.loadingImage = true;
		$scope.loadingImageAlert = false;

		User.getBaseImage(id_uniq).then(function (res) {

			$scope.loadingImage = false;
			
			if (res.data.is_image == 1) {

				$scope.image = res.data.response;

				$timeout(function () {

					$scope.$emit('imgReady');

				}, 500);

			} else {

				$scope.loadingImageAlert = {
					type: 'success',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.loadingImage = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingImageAlert = {
					type: 'danger',
					message: err.statusText
				}

			}

		});

	};

	var playerOptions = {

		getImages: function (id_uniq) {

			$scope.loadingImage = true;
			$scope.loadingImageAlert = false;

			Player.getBaseImages(id_uniq).then(function (res) {

				$scope.loadingImage = false;

				if (res.data.is_image == 1) {

					$scope.playerHasImages = true;
					$scope.playerImages = res.data.response;

					var headImageIndex = _.findIndex($scope.playerImages,  { type: "1" });
					var bodyImageIndex = _.findIndex($scope.playerImages,  { type: "2" });

					$scope.headImage = $scope.playerImages[headImageIndex].image;
					$scope.bodyImage = $scope.playerImages[bodyImageIndex].image;

					$scope.playerdata.head.id = $scope.playerImages[headImageIndex].id_image;
					$scope.playerdata.body.id = $scope.playerImages[bodyImageIndex].id_image;

					$timeout(function () {

						$scope.$emit('headImgReady');
						$scope.$emit('bodyImgReady');

					}, 500);

				} else {

					$scope.loadingImageAlert = {
						type: 'success',
						message: res.data.response
					}

				}

			}, function (err) {

				$scope.loadingImage = false;

				if (err.status == 401) {

					Auth.deleteToken();

				} else {

					$scope.loadingImageAlert = {
						type: 'danger',
						message: err.statusText
					}

				}

			});

		},
		getHeadImage: function (id_uniq) {

			$scope.loadingImage = true;
			$scope.loadingImageAlert = false;

			Player.getBaseHeadImage(id_uniq).then(function (res) {

				$scope.loadingImage = false;

				if (res.data.is_image == 1) {

					$scope.playerHasImages = true;
					$scope.playerHeadImage = res.data.response.image;
					$scope.playerdata.head.id = res.data.response.id_image;

					$timeout(function () {

						$scope.$emit('headImgReady');
					
					}, 500);

				} else {

					$scope.loadingImageAlert = {
						type: 'success',
						message: res.data.response
					}

				}

			}, function (err) {

				$scope.loadingImage = false;

				if (err.status == 401) {

					Auth.deleteToken();

				} else {

					$scope.loadingImageAlert = {
						type: 'danger',
						message: err.statusText
					}

				}

			});

		},
		getBodyImage: function (id_uniq) {

			$scope.loadingImage = true;
			$scope.loadingImageAlert = false;

			Player.getBaseBodyImage(id_uniq).then(function (res) {

				$scope.loadingImage = false;

				if (res.data.is_image == 1) {

					$scope.playerHasImages = true;
					$scope.playerBodyImage = res.data.response.image;
					$scope.playerdata.body.id = res.data.response.id_image;

					$timeout(function () {

						$scope.$emit('bodyImgReady');
					
					}, 500);

				} else {

					$scope.loadingImageAlert = {
						type: 'success',
						message: res.data.response
					}

				}

			}, function (err) {

				$scope.loadingImage = false;

				if (err.status == 401) {

					Auth.deleteToken();

				} else {

					$scope.loadingImageAlert = {
						type: 'danger',
						message: err.statusText
					}

				}

			});

		}

	}

	if (_.indexOf(owners, owner) == -1) { $location.path('/dashboard'); }
	else {

		switch (owner) {

			case 'tea_':
				$scope.getTeamImage(id_uniq);
				redirectURL = '/teams';
				break;
			case 'use_':
				$scope.getUserImage(id_uniq);
				redirectURL = '/users';
				break;
			case 'pla_':

				if ($window.sessionStorage.playerHeadFile == undefined || $window.sessionStorage.playerBodyFile == undefined) { $location.path('/dashboard'); }
				else {

					var headFile = $window.sessionStorage.playerHeadFile;
					var bodyFile = $window.sessionStorage.playerBodyFile;

					if (headFile == 1 && bodyFile == 1) {

						playerOptions.getImages(id_uniq);

					} else if (headFile == 1 && bodyFile == 0) {

						playerOptions.getHeadImage(id_uniq);

					} else if (headFile == 0 && bodyFile == 1) {

						playerOptions.getBodyImage(id_uniq);

					}

				}

				redirectURL = '/players';
				break;

		}

	}

	$scope.setImage = function () {

		$scope.settingImage = true;
		$scope.settingImageAlert = false;

		var data = $.param($scope.formdata);

		Image.set(data).then(function (res) {

			$scope.settingImage = false;

			if (res.data.status == "success") {

				var response = $window.sessionStorage.getItem('response') || "Action completed successfully";

				$window.sessionStorage.removeItem('response');

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: response
				};

				$location.path(redirectURL);

			} else {

				$scope.settingImageAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.settingImage = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.settingImageAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.setPlayerImages = function () {

		$scope.settingPlayersImages = true;
		$scope.settingPlayersImagesAlert = false;

		var data = $.param($scope.playerdata);

		Image.playerSet(data).then(function (res) {

			$scope.settingPlayersImages = false;

			if (res.data.status == "success") {

				var response = $window.sessionStorage.getItem('response') || "Action completed successfully";

				$window.sessionStorage.removeItem('response');
				$window.sessionStorage.removeItem('playerHeadFile');
				$window.sessionStorage.removeItem('playerBodyFile');

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: response
				};

				$location.path(redirectURL);

			} else {

				$scope.settingPlayersImagesAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.settingPlayersImages = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.settingPlayersImagesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

}]);