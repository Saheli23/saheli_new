var stf = angular.module('HeadingControllers', []);

stf.controller('HeadingListController', ['$scope', '$routeParams', '$location', 'Event', 'Heading', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, $location, Event, Heading, Access, Auth, STFcache) {Access, 
	
	$scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};


	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;
				if (_.findIndex($scope.event.configurations, { station: 'Heading' }) >= 0) {

 					$scope.$emit('eventLoaded');
				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getHeadings = function (id_uniq, refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('headingEvent' + $scope.id_uniq);
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.measurements = null;
		$scope.loadingHeadings = true;
		$scope.loadingHeadingsAlert = false;

		Heading.getAll($scope.id_uniq).then(function (res) {
			$scope.loadingHeadings = false;
			if (angular.isObject(res.data.response)) {
				$scope.measurements = res.data.response;
			} else {
				$scope.loadingHeadingsAlert = {
					type: 'success',
					message: res.data.response
				};
			}
			$scope.getAccess(1);
		}, function (err) {

			$scope.loadingHeadings = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingHeadingsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.deletePlayerEvent = function( player_id, ind ){

		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:player_id,type:'delete'});
			Heading.delete(data).then(function (res) {
				$scope.loadingHeadings = false;
				$scope.getHeadings($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingHeadingsAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingHeadings = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingHeadingsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};
	$scope.bulkDeletePlayerEvent = function(){

		action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:values,type:'delete',action:action_id});
			Heading.multidelete(data).then(function (res) {
				$scope.loadingHeadings = false;
				$scope.getHeadings($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingHeadingsAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingHeadings = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingHeadingsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one user to delete.");
	 }
	};

	$scope.updatePlayerEvent = function( player_id ){
		var data = $.param({id_player:player_id,type:'update'});
		Heading.updateForce(data).then(function (res) {

			$scope.loadingHeadings = false;
			if (angular.isObject(res.data.response)) {

				//$scope.measurements = res.data.response;

			} else {
				$scope.loadingHeadingsAlert = {type: 'success',message: res.data.response};
			}

		}, function (err) {

			$scope.loadingHeadings = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingHeadingsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };

			}

		});
	}

	$scope.getEvent($scope.id_uniq);
	$scope.getHeadings($scope.id_uniq);

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}
	
	/*$scope.orderOptions = [
		{ value: 'name', text: 'Name' },
		{ value: '-date', text: 'Date' }
	];*/
	
}]);

stf.controller('HeadingCreateController', ['$scope', '$routeParams', '$location', '$rootScope', '$filter', 'Event', 'Player', 'Heading', 'Anthropometric', 'Auth', 'STFcache', function ($scope, $routeParams, $location, $rootScope, $filter, Event, Player, Heading, Anthropometric, Auth, STFcache) {
	

    

	$scope.id_uniq = $routeParams.id_uniq;
	$scope.player = null;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	var measurement = {
		"MKS" : {
			"speed" : "m/s",
			"distance" : "mts"
		},
		"YLS" : {
			"speed" : "y/s",
			"distance" : "fts"
		},
		"default" : {
			"speed" : "m/s",
			"distance" : "mts"
		}
	};
	
	/* AIM TEST CODE */

	$scope.aimTest = false;
	$scope.HeadAimFieldOneValidated = false;
	$scope.HeadAimFieldTwoValidated = false;
	$scope.HeadAimFieldThreeValidated = false;
	$scope.HeadAimFieldFourValidated = false;
	$scope.HeadAimFieldFiveValidated = false;
	$scope.HeadAimFieldSixValidated = false;

	$scope.aimdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		request_one: 0,
		destination_one: '',
		request_two: 0,
		destination_two: '',
		request_three: 0,
		destination_three: '',
		request_four: 0,
		destination_four: '',
		request_five: 0,
		destination_five: '',
		request_six: 0,
		destination_six: ''
	};

  $scope.getHeadingMeasurement = function( type, dim ){
        if(type=== undefined){
        	type="default";
        	
        	return measurement[type][dim];
        }
		return measurement[type][dim];
	};

	$scope.startAimTest = function () { 
         
       

		$scope.aimTest = true; 
		$scope.aimdata.request_one = Math.floor((Math.random() * 24) + 1);
         //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
           if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length >1){ 
	             
	                this.value = parseFloat($(this).val().slice(0, -1));
	            }  
	         	}    
            	var charCode = (evt.which) ? evt.which : event.keyCode;
				if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 	return true;


			});

          angular.element('.onlynumbertext').on("keypress",function (evt) {
    	    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;

			});
          
	
	};

	$scope.validateHeadAimField = function (field) {

		switch (field) {

			case 1: 
				$scope.AimFieldOneValidated = true;
				$scope.aimdata.request_two = Math.floor((Math.random() * 24) + 1);	
				break;
			case 2:
				$scope.AimFieldTwoValidated = true;
				$scope.aimdata.request_three = Math.floor((Math.random() * 24) + 1);
				break;
			case 3:
				$scope.AimFieldThreeValidated = true;
				$scope.aimdata.request_four = Math.floor((Math.random() * 24) + 1);
				break;
			case 4:
				$scope.AimFieldFourValidated = true;
				$scope.aimdata.request_five = Math.floor((Math.random() * 24) + 1);
				break;
			case 5:
				$scope.AimFieldFiveValidated = true;
				$scope.aimdata.request_six = Math.floor((Math.random() * 24) + 1);
				break;
			case 6:
				$scope.AimFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	/* END AIM TEST CODE */

	/* JUMP TEST CODE */

	$scope.jumpTest = false;
	$scope.HeadJumpFieldOneValidated = false;
	$scope.HeadJumpFieldTwoValidated = false;
	$scope.HeadJumpFieldThreeValidated = false;
	


	console.log($filter('date')(new Date(), 'MM/dd/yyyy'));
	$scope.jumpdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		extended_arm_height: '',
		jump_high_one: '',
		jump_result_one: '',
		jump_high_two: '',
		jump_result_two: '',
		jump_high_three: '',
		jump_result_three: ''
	};

	$scope.startJumpTest = function () {
        

		$scope.jumpTest = true;
        
      //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
           if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length >1){ 
	             
	                this.value = parseFloat($(this).val().slice(0, -1));
	            }  
	         	}    
            	var charCode = (evt.which) ? evt.which : event.keyCode;
				if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 	return true;


			});
         
         angular.element('.onlynumbertext').on("keypress",function (evt) {
    	    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;

			});

	};

	$scope.setHeightWithExtendedArm = function () {
        
		$scope.extendedArmHeightSet = true;

	};

	$scope.validateHeadJumpField = function (field) {

		switch (field) {

			case 1:
				$scope.HeadJumpFieldOneValidated = true;
				var weight = parseFloat($scope.weight);
				var heightWithExtendedArm = parseFloat($scope.jumpdata.extended_arm_height);
				var height = parseFloat($scope.jumpdata.jump_high_one);
				var difference = height - heightWithExtendedArm;
				var result = weight * Math.pow((4.9 * difference), 2);
				$scope.jumpdata.jump_result_one = result.toFixed(2);
				break;
			case 2:
				$scope.HeadJumpFieldTwoValidated = true;
				var weight = parseFloat($scope.weight);
				var heightWithExtendedArm = parseFloat($scope.jumpdata.extended_arm_height);
				var height = parseFloat($scope.jumpdata.jump_high_two);
				var difference = height - heightWithExtendedArm;
				var result = weight * Math.pow((4.9 * difference), 2);
				$scope.jumpdata.jump_result_two = result.toFixed(2);
				break;
			case 3:
				$scope.HeadJumpFieldThreeValidated = true;
				$scope.HeadJumpFieldTwoValidated = true;
				var weight = parseFloat($scope.weight);
				var heightWithExtendedArm = parseFloat($scope.jumpdata.extended_arm_height);
				var height = parseFloat($scope.jumpdata.jump_high_three);
				var difference = height - heightWithExtendedArm;
				var result = weight * Math.pow((4.9 * difference), 2);
				$scope.jumpdata.jump_result_three = result.toFixed(2);
				$scope.HeadingJumpFormCompleted = true;
				break;
			default:
				break;

		}

	};
	/* END JUMP TEST CODE */


	/* FORCE TEST CODE */
	$scope.forcedata = {
		id_player: '',
		id_event: '',
		measurement_date : $filter('date')(new Date(), 'MM/dd/yyyy'),
		distance: [ '', '', '' ],
		attempt : [ '', '', '' ],
		result  : [ '', '', '' ]
	};

	$scope.headForcedata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		distance_one: '',
		distance_two: '',
		distance_three: '',
		attempt_one : '',
		attempt_two : '',
		attempt_three : '',
		result_one : '',
		result_two : '',
		result_three : ''
	};


	$scope.forceFieldValidated = [ false, false, false ];

	$scope.startForceTest = function () { 

		

		 //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
           if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length >1){ 
	             
	                this.value = parseFloat($(this).val().slice(0, -1));
	            }  
	         	}    
            	var charCode = (evt.which) ? evt.which : event.keyCode;
				if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 	return true;


			});

        angular.element('.onlynumbertext').on("keypress",function (evt) {
    	    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;

			});

		return $scope.forceTest = true; 
	};

	$scope.validateForceField = function (field) {
		$scope.forceFieldValidated[field] = true;

		var velocityInitial = 0;
		var velocityFinal   = parseFloat( $scope.forcedata.attempt[field] );
		var distance        = $scope.forcedata.distance[field];
		var mass            = $scope.ball_weight / 1000;
		var force           = mass * ( ( Math.pow( velocityFinal, 2 ) - velocityInitial ) / ( 2 * distance ) );

		$scope.forcedata.result[field] = force.toFixed(2);

		if( field === 2 ) $scope.CreateHeadingForceFormCompleted = true;
	};

	/* END FORCE TEST CODE */

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {
     
			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;
                
            

               if (_.findIndex($scope.event.configurations, { station: 'Heading' }) >= 0) {

 					$scope.aimdata.id_event = $scope.event.id;
 					$scope.forcedata.id_event = $scope.event.id;
 					$scope.jumpdata.id_event = $scope.event.id;

 					$scope.eventHasHeadingJump = (_.findIndex($scope.event.configurations, { station: 'Heading', test: 'Jump' }) >= 0) ? true : false;
 					$scope.eventHasHeadingForce = (_.findIndex($scope.event.configurations, { station: 'Heading', test: 'Force' }) >= 0) ? true : false;
 					$scope.eventHasHeadingAim = (_.findIndex($scope.event.configurations, { station: 'Heading', test: 'Aim' }) >= 0) ? true : false;

 				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {

		console.log(id_uniq);

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {
			console.log(res);
             
			if (angular.isObject(res.data.response)) {

				$scope.aimdata.id_player = res.data.response.id;
				$scope.forcedata.id_player = res.data.response.id;
				$scope.jumpdata.id_player = res.data.response.id;

				return res.data.response;
               
			} else {

				$scope.loadingPlayer = false;

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		}).then(function (args) {

			if (args != undefined) {

				var player = args;
				var data = [];
				var forceAgeRangeData = [];
				var aimAgeRangeData = [];
				var jumpAgeRangeData = [];

				Anthropometric.getWeight(player.id, $scope.event.id).then(function (res) {

					if (angular.isObject(res.data.response)) {

						return res.data.response;

					} else {

						$scope.loadingPlayer = false;

						$scope.loadingPlayerAlert = {
							type: 'success',
							message: res.data.response
						};

					}

				}, function (err) {

					$scope.loadingPlayer = false;

					$scope.loadingPlayerAlert = {
						type: 'danger',
						message: err.status + ' ' + err.statusText
					};

				}).then(function (args) {

					if (args !== undefined) {

						$scope.weight = args.nu_weight;

						console.log($scope.weight);


						for (var i = 0; i < $scope.event.configurations.length; i++) {
					
							if ($scope.event.configurations[i].station == "Heading") {

								if (_.findIndex(data, { value: $scope.event.configurations[i].test }) == -1) {

									data.push({
										name: 'tests[]',
										value: $scope.event.configurations[i].test
									});

								};

								if ($scope.event.configurations[i].test == "Force") {

									forceAgeRangeData.push({
										test: $scope.event.configurations[i].test,
										min: $scope.event.configurations[i].nu_min_age,
										max: $scope.event.configurations[i].nu_max_age,
										ball_weight: $scope.event.configurations[i].nu_ball_weight
									});	

								};

								if ($scope.event.configurations[i].test == "Aim") {

									aimAgeRangeData.push({
										test: $scope.event.configurations[i].test,
										min: $scope.event.configurations[i].nu_min_age,
										max: $scope.event.configurations[i].nu_max_age,
										ball_weight: $scope.event.configurations[i].nu_ball_weight
									});	

								};

								if ($scope.event.configurations[i].test == "Jump") {

									jumpAgeRangeData.push({
										test: $scope.event.configurations[i].test,
										min: $scope.event.configurations[i].nu_min_age,
										max: $scope.event.configurations[i].nu_max_age,
										ball_weight: $scope.event.configurations[i].nu_ball_weight
									});	

								};

							};

						};

						data = $.param(data);

						Heading.check(player.id, $scope.event.id, data).then(function (res) {

							$scope.loadingPlayer = false;

							if (res.data.player_exists == 1) {

								$scope.loadingPlayerAlert = {
									type: 'success',
									message: res.data.response
								};

							} else {

								var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));

								$scope.playerCanPerformAimTest = false;
								$scope.playerCanPerformForceTest = false;
								$scope.playerCanPerformJumpTest = false;

								if (aimAgeRangeData.length > 0) {

									for (var i = 0; i < aimAgeRangeData.length; i++) {

										var min = parseInt(aimAgeRangeData[i].min);
										var max = parseInt(aimAgeRangeData[i].max);

										if (playerAge >= min && playerAge <= max) {

											$scope.playerCanPerformAimTest = true;
											break;

										};

									};

								};

								if (forceAgeRangeData.length > 0) {

									for (var i = 0; i < forceAgeRangeData.length; i++) {

										var min = parseInt(forceAgeRangeData[i].min);
										var max = parseInt(forceAgeRangeData[i].max);

										if (playerAge >= min && playerAge <= max) {

											$scope.playerCanPerformForceTest = true;
											$scope.ball_weight = parseInt(forceAgeRangeData[i].ball_weight);
											break;

										};

									};

								};

								if (jumpAgeRangeData.length > 0) {

									for (var i = 0; i < jumpAgeRangeData.length; i++) {

										var min = parseInt(jumpAgeRangeData[i].min);
										var max = parseInt(jumpAgeRangeData[i].max);

										if (playerAge >= min && playerAge <= max) {

											$scope.playerCanPerformJumpTest = true;
											$scope.ball_weight = parseInt(jumpAgeRangeData[i].ball_weight);
											break;

										};

									};

								};

								$scope.player = player;

								$scope.AimTestAlert = ($scope.playerCanPerformAimTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
								$scope.ForceTestAlert = ($scope.playerCanPerformForceTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
								$scope.JumpTestAlert = ($scope.playerCanPerformJumpTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;

								$scope.showAim = (res.data.response.aim == 2) ? true : false;
								$scope.showForce = (res.data.response.force == 2) ? true : false;
								$scope.showJump = (res.data.response.jump == 2) ? true : false;

							}

						}, function (err) {

							$scope.loadingPlayer = false;

							$scope.loadingPlayerAlert = {
								type: 'danger',
								message: res.data.response
							};

						});	

					};	

				});

			};

		});

	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$scope.aimdata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			request_one: '',
			destination_one: '',
			request_two: '',
			destination_two: '',
			request_three: '',
			destination_three: '',
			request_four: '',
			destination_four: '',
			request_five: '',
			destination_five: '',
			request_six: '',
			destination_six: ''
		};

		$scope.forcedata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			distance_one: '',
			distance_two: '',
			distance_three: ''
		};

		$scope.jumpdata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			jump_high_one: '',
			jump_high_two: '',
			jump_high_three: ''
		};
				
		$scope.CreateHeadingAimForm.$setPristine();
		$scope.CreateHeadingForceForm.$setPristine();
		$scope.CreateHeadingJumpForm.$setPristine();

	};

	$scope.getEvent($scope.id_uniq);

	$scope.createHeadingAim = function () {

		$scope.creatingHeadingAim = true;
		$scope.creatingHeadingAimAlert = false;

		var data = $.param($scope.aimdata);

		Heading.createAim(data).then(function (res) {

			$scope.creatingHeadingAim = false;

			if (res.data.status == "success") {
                
				STFcache.delete('headingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/heading');

			} else {

				$scope.creatingHeadingAimAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingHeadingAim = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingHeadingAimAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.setForceData = function( source ){
		$scope.headForcedata.id_player        = source.id_player;
		$scope.headForcedata.id_event         = source.id_event;
		$scope.headForcedata.measurement_date = source.measurement_date; 
		$scope.headForcedata.distance_one     = source.distance[0]; 
		$scope.headForcedata.distance_two     = source.distance[1]; 
		$scope.headForcedata.distance_three   = source.distance[2]; 
		$scope.headForcedata.attempt_one      = source.attempt[0]; 
		$scope.headForcedata.attempt_two      = source.attempt[1]; 
		$scope.headForcedata.attempt_three    = source.attempt[2];
	}

	$scope.createHeadingForce = function () {

		$scope.creatingHeadingForce = true;
		$scope.creatingHeadingForceAlert = false;

		$scope.setForceData( $scope.forcedata );

		var data = $.param($scope.headForcedata);

		Heading.createForce(data).then(function (res) {

			$scope.creatingHeadingForce = false;
            
			if (res.data.status == "success") {
                
				STFcache.delete('headingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/heading');

			} else {

				$scope.creatingHeadingForceAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingHeadingForce = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingHeadingForceAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.createHeadingJump = function () {

		$scope.creatingHeadingJump = true;
		$scope.creatingHeadingJumpAlert = false;

		var data = $.param($scope.jumpdata);

		Heading.createJump(data).then(function (res) {

			$scope.creatingHeadingJump = false;
               
			if (res.data.status == "success") {

				STFcache.delete('headingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/heading');

			} else {

				$scope.creatingHeadingJumpAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingHeadingJump = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingHeadingJumpAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};
	
}]);

stf.controller('HeadingEditController', ['$scope', '$routeParams', '$location', '$rootScope', '$filter', 'Event', 'Player', 'Heading', 'Anthropometric', 'Auth', 'STFcache', function ($scope, $routeParams, $location, $rootScope, $filter, Event, Player, Heading, Anthropometric, Auth, STFcache) {
	//console.log($routeParams);
	
	$scope.id_uniq = $routeParams.id;
	$scope.player_id_uniq = $routeParams.playerid;
	$scope.player = null;
  
 
  
	var measurement = {
		"MKS" : {
			"speed" : "m/s",
			"distance" : "mts"
		},
		"YLS" : {
			"speed" : "y/s",
			"distance" : "fts"
		},
		"default" : {
			"speed" : "m/s",
			"distance" : "mts"
		}
	};

	console.log($scope.id_uniq,$scope.player_id_uniq);

	/* AIM TEST CODE */

	$scope.aimTest = false;
	$scope.HeadAimFieldOneValidated = false;
	$scope.HeadAimFieldTwoValidated = false;
	$scope.HeadAimFieldThreeValidated = false;
	$scope.HeadAimFieldFourValidated = false;
	$scope.HeadAimFieldFiveValidated = false;
	$scope.HeadAimFieldSixValidated = false;

	$scope.aimdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		request_one: 0,
		destination_one: '',
		request_two: 0,
		destination_two: '',
		request_three: 0,
		destination_three: '',
		request_four: 0,
		destination_four: '',
		request_five: 0,
		destination_five: '',
		request_six: 0,
		destination_six: ''
	};

	$scope.getHeadingMeasurement = function( type, dim ){
		//console.log(type);return false;
        if(type=== undefined){
        	type="default";
        	
        	return measurement[type][dim];
        }
		return measurement[type][dim];
	};

	$scope.startAimTest = function () { 

		$scope.aimTest = true; 
		$scope.aimdata.request_one = Math.floor((Math.random() * 24) + 1);
        
           //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
            	if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length >1){ 
	             
	                this.value = parseFloat($(this).val().slice(0, -1));
	            }  
	         	}    
            	var charCode = (evt.which) ? evt.which : event.keyCode;
				if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 	return true;


			});


	};

	$scope.validateHeadAimField = function (field) {

		switch (field) {

			case 1: 
				$scope.AimFieldOneValidated = true;
				$scope.aimdata.request_two = Math.floor((Math.random() * 24) + 1);	
				break;
			case 2:
				$scope.AimFieldTwoValidated = true;
				$scope.aimdata.request_three = Math.floor((Math.random() * 24) + 1);
				break;
			case 3:
				$scope.AimFieldThreeValidated = true;
				$scope.aimdata.request_four = Math.floor((Math.random() * 24) + 1);
				break;
			case 4:
				$scope.AimFieldFourValidated = true;
				$scope.aimdata.request_five = Math.floor((Math.random() * 24) + 1);
				break;
			case 5:
				$scope.AimFieldFiveValidated = true;
				$scope.aimdata.request_six = Math.floor((Math.random() * 24) + 1);
				break;
			case 6:
				$scope.AimFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	/* END AIM TEST CODE */

	/* JUMP TEST CODE */

	$scope.jumpTest = false;
	$scope.HeadJumpFieldOneValidated = false;
	$scope.HeadJumpFieldTwoValidated = false;
	$scope.HeadJumpFieldThreeValidated = false;
	
	$scope.jumpdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		extended_arm_height: '',
		jump_high_one: '',
		jump_result_one: '',
		jump_high_two: '',
		jump_result_two: '',
		jump_high_three: '',
		jump_result_three: ''
	};

	$scope.startJumpTest = function () {

		$scope.jumpTest = true;
        
        //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
            	if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length >1){ 
	             
	                this.value = parseFloat($(this).val().slice(0, -1));
	            }  
	         	}    
            	var charCode = (evt.which) ? evt.which : event.keyCode;
				if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 	return true;


			});

	};

	$scope.setHeightWithExtendedArm = function () {

		$scope.extendedArmHeightSet = true;

	};

	$scope.validateHeadJumpField = function (field) {

		switch (field) {

			case 1:
				$scope.HeadJumpFieldOneValidated = true;
				var weight = parseFloat($scope.weight);
				var heightWithExtendedArm = parseFloat($scope.jumpdata.extended_arm_height);
				var height = parseFloat($scope.jumpdata.jump_high_one);
				var difference = height - heightWithExtendedArm;
				var result = weight * Math.pow((4.9 * difference), 2);
				$scope.jumpdata.jump_result_one = result.toFixed(2);
				break;
			case 2:
				$scope.HeadJumpFieldTwoValidated = true;
				var weight = parseFloat($scope.weight);
				var heightWithExtendedArm = parseFloat($scope.jumpdata.extended_arm_height);
				var height = parseFloat($scope.jumpdata.jump_high_two);
				var difference = height - heightWithExtendedArm;
				var result = weight * Math.pow((4.9 * difference), 2);
				$scope.jumpdata.jump_result_two = result.toFixed(2);
				break;
			case 3:
				$scope.HeadJumpFieldThreeValidated = true;
				$scope.HeadJumpFieldTwoValidated = true;
				var weight = parseFloat($scope.weight);
				var heightWithExtendedArm = parseFloat($scope.jumpdata.extended_arm_height);
				var height = parseFloat($scope.jumpdata.jump_high_three);
				var difference = height - heightWithExtendedArm;
				var result = weight * Math.pow((4.9 * difference), 2);
				$scope.jumpdata.jump_result_three = result.toFixed(2);
				$scope.HeadingJumpFormCompleted = true;
				break;
			default:
				break;

		}

	};
	/* END JUMP TEST CODE */


	/* FORCE TEST CODE */
	$scope.forcedata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		distance: [ '', '', '' ],
		attempt : [ '', '', '' ],
		result  : [ '', '', '' ]
	};

	$scope.headForcedata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		distance_one: '',
		distance_two: '',
		distance_three: '',
		attempt_one : '',
		attempt_two : '',
		attempt_three : '',
		result_one : '',
		result_two : '',
		result_three : ''
	};


	$scope.forceFieldValidated = [ false, false, false ];

	$scope.startForceTest = function () { 
		
       
       //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
            	if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length >1){ 
	             
	                this.value = parseFloat($(this).val().slice(0, -1));
	            }  
	         	}    
            	var charCode = (evt.which) ? evt.which : event.keyCode;
				if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 	return true;


			});

              angular.element('.onlynumbertext').on("keypress",function (evt) {
    	    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;

			});

             return $scope.forceTest = true; 
	};

	$scope.validateForceField = function (field) {
		$scope.forceFieldValidated[field] = true;

		var velocityInitial = 0;
		var velocityFinal   = parseFloat( $scope.forcedata.attempt[field] );
		var distance        = $scope.forcedata.distance[field];
		var mass            = $scope.ball_weight / 1000;
		var force           = mass * ( ( Math.pow( velocityFinal, 2 ) - velocityInitial ) / ( 2 * distance ) );

		$scope.forcedata.result[field] = force.toFixed(2);

		if( field === 2 ) $scope.CreateHeadingForceFormCompleted = true;
	};

	/* END FORCE TEST CODE */

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;


				console.log("Hello>>>>",$scope.event);
             //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
           if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length >1){ 
	             
	                this.value = parseFloat($(this).val().slice(0, -1));
	            }  
	         	}    
            	var charCode = (evt.which) ? evt.which : event.keyCode;
				if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 	return true;


			});
         
          	angular.element('.onlynumbertext').on("keypress",function (evt) {
    	    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;

			});

				if (_.findIndex($scope.event.configurations, { station: 'Heading' }) >= 0) {

 					$scope.aimdata.id_event = $scope.event.id;
 					$scope.forcedata.id_event = $scope.event.id;
 					$scope.jumpdata.id_event = $scope.event.id;

 					$scope.eventHasHeadingJump = (_.findIndex($scope.event.configurations, { station: 'Heading', test: 'Jump' }) >= 0) ? true : false;
 					$scope.eventHasHeadingForce = (_.findIndex($scope.event.configurations, { station: 'Heading', test: 'Force' }) >= 0) ? true : false;
 					$scope.eventHasHeadingAim = (_.findIndex($scope.event.configurations, { station: 'Heading', test: 'Aim' }) >= 0) ? true : false;
 					$scope.getPlayer($scope.player_id_uniq);
 				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {

		console.log(">>>>>Hiii",$scope.event);
		//console.log(id_uniq);

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			if (angular.isObject(res.data.response)) {

				$scope.aimdata.id_player = res.data.response.id;
				$scope.forcedata.id_player = res.data.response.id;
				$scope.jumpdata.id_player = res.data.response.id;

				return res.data.response;

			} else {

				$scope.loadingPlayer = false;

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		}).then(function (args) {

			if (args != undefined) {

				var player = args;
				var data = [];
				var forceAgeRangeData = [];
				var aimAgeRangeData = [];
				var jumpAgeRangeData = [];

				Anthropometric.getWeight(player.id, $scope.event.id).then(function (res) {

					if (angular.isObject(res.data.response)) {

						return res.data.response;

					} else {

						$scope.loadingPlayer = false;

						$scope.loadingPlayerAlert = {
							type: 'success',
							message: res.data.response
						};

					}

				}, function (err) {

					$scope.loadingPlayer = false;

					$scope.loadingPlayerAlert = {
						type: 'danger',
						message: err.status + ' ' + err.statusText
					};

				}).then(function (args) {
					console.log(args);
					if (args !== undefined) {

						$scope.weight = args.nu_weight;

						for (var i = 0; i < $scope.event.configurations.length; i++) {
					
							if ($scope.event.configurations[i].station == "Heading") {

								if (_.findIndex(data, { value: $scope.event.configurations[i].test }) == -1) {

									data.push({
										name: 'tests[]',
										value: $scope.event.configurations[i].test
									});

								};

								if ($scope.event.configurations[i].test == "Force") {

									forceAgeRangeData.push({
										test: $scope.event.configurations[i].test,
										min: $scope.event.configurations[i].nu_min_age,
										max: $scope.event.configurations[i].nu_max_age,
										ball_weight: $scope.event.configurations[i].nu_ball_weight
									});	

								};

								if ($scope.event.configurations[i].test == "Aim") {

									aimAgeRangeData.push({
										test: $scope.event.configurations[i].test,
										min: $scope.event.configurations[i].nu_min_age,
										max: $scope.event.configurations[i].nu_max_age,
										ball_weight: $scope.event.configurations[i].nu_ball_weight
									});	

								};

								if ($scope.event.configurations[i].test == "Jump") {

									jumpAgeRangeData.push({
										test: $scope.event.configurations[i].test,
										min: $scope.event.configurations[i].nu_min_age,
										max: $scope.event.configurations[i].nu_max_age,
										ball_weight: $scope.event.configurations[i].nu_ball_weight
									});	

								};

							};

						};

						data = $.param(data);

						var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));

						$scope.playerCanPerformAimTest = false;
						$scope.playerCanPerformForceTest = false;
						$scope.playerCanPerformJumpTest = false;

						if (aimAgeRangeData.length > 0) {

							for (var i = 0; i < aimAgeRangeData.length; i++) {

								var min = parseInt(aimAgeRangeData[i].min);
								var max = parseInt(aimAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformAimTest = true;
									break;

								};

							};

						};

						if (forceAgeRangeData.length > 0) {

							for (var i = 0; i < forceAgeRangeData.length; i++) {

								var min = parseInt(forceAgeRangeData[i].min);
								var max = parseInt(forceAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformForceTest = true;
									$scope.ball_weight = parseInt(forceAgeRangeData[i].ball_weight);
									break;

								};

							};

						};

						if (jumpAgeRangeData.length > 0) {

							for (var i = 0; i < jumpAgeRangeData.length; i++) {

								var min = parseInt(jumpAgeRangeData[i].min);
								var max = parseInt(jumpAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformJumpTest = true;
									$scope.ball_weight = parseInt(jumpAgeRangeData[i].ball_weight);
									break;

								};

							};

						};

						$scope.player = player;
						//console.log(">>>Hey",player);
						/*$scope.AimTestAlert = ($scope.playerCanPerformAimTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
						$scope.ForceTestAlert = ($scope.playerCanPerformForceTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
						$scope.JumpTestAlert = ($scope.playerCanPerformJumpTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;*/

						/*$scope.showAim = (res.data.response.aim == 2) ? true : false;
						$scope.showForce = (res.data.response.force == 2) ? true : false;
						$scope.showJump = (res.data.response.jump == 2) ? true : false;*/

						Heading.getDetails($scope.id_uniq,$scope.player_id_uniq).then(function (res) {

							$scope.loadingEvent = false;

							if (angular.isObject(res.data.response)) {
                                console.log(res.data.response);
								//if( $scope.event ){
                                if(res.data.response.forceStat==1){
								$scope.forcedata.id_player = res.data.response.forcePlayerID;
								$scope.forcedata.id_event = res.data.response.forceEvtID;
								$scope.forcedata.measurement_date = res.data.response.forceMeasurementDt;
								$scope.forcedata.distance[0] = parseFloat(res.data.response.forceDisOne);
								$scope.forcedata.distance[1] = parseFloat(res.data.response.forceDisTwo);
								$scope.forcedata.distance[2] = parseFloat(res.data.response.forceDisThree);

								$scope.forcedata.attempt[0] = parseFloat(res.data.response.forceAttemptOne);
								$scope.forcedata.attempt[1] = parseFloat(res.data.response.forceAttemptTwo);
								$scope.forcedata.attempt[2] = parseFloat(res.data.response.forceAttemptThree);

								$scope.forcedata.result[0] = $scope.calculateForce(0);
								$scope.forcedata.result[1] = $scope.calculateForce(1);
								$scope.forcedata.result[2] = $scope.calculateForce(2);
                                }
                                else{
                                	$scope.forcedata.measurement_date=$filter('date')(new Date(), 'MM/dd/yyyy');
                                   
                                }
                                //added by sg//
                                if(res.data.response.aimStat==1){
                                $scope.aimdata.id_player = res.data.response.aimPlayerID;
								$scope.aimdata.id_event = res.data.response.aimEvtID;
								$scope.aimdata.measurement_date = res.data.response.aimMeasurementDt;
								$scope.aimdata.request_one=parseFloat(res.data.response.aimReqOne);
								$scope.aimdata.request_two=parseFloat(res.data.response.aimReqTwo);
								$scope.aimdata.request_three=parseFloat(res.data.response.aimReqThree);
								$scope.aimdata.request_four=parseFloat(res.data.response.aimReqFour);
								$scope.aimdata.request_five=parseFloat(res.data.response.aimReqFive);
								$scope.aimdata.request_six=parseFloat(res.data.response.aimReqSix);
								$scope.aimdata.destination_one=parseFloat(res.data.response.aimDesOne);
                                $scope.aimdata.destination_two=parseFloat(res.data.response.aimDesTwo);
                                $scope.aimdata.destination_three=parseFloat(res.data.response.aimDesThree);
                                $scope.aimdata.destination_four=parseFloat(res.data.response.aimDesFour);
                                $scope.aimdata.destination_five=parseFloat(res.data.response.aimDesFive);
                                $scope.aimdata.destination_six=parseFloat(res.data.response.aimDesSix);
								}
								else{
									$scope.aimdata.measurement_date=$filter('date')(new Date(), 'MM/dd/yyyy');
								}
                                
                                if(res.data.response.jumpStat==1){
								$scope.jumpdata.id_player = res.data.response.jumpPlayerID;
								$scope.jumpdata.id_event = res.data.response.jumpEvtID;
								$scope.jumpdata.measurement_date = res.data.response.jumpMeasurementDt;
								$scope.jumpdata.extended_arm_height=parseFloat(res.data.response.jumpArmHeight);
								$scope.jumpdata.jump_high_one=parseFloat(res.data.response.jumpHighOne);
								$scope.jumpdata.jump_high_two=parseFloat(res.data.response.jumpHighTwo);
								$scope.jumpdata.jump_high_three=parseFloat(res.data.response.jumpHighThree);
								}
								else{
									$scope.jumpdata.measurement_date=$filter('date')(new Date(), 'MM/dd/yyyy');
								}
								//added by sg end//
								

								//$scope.forcedata.result[2] = res.data.response.forceAttemptOne;

								/*$scope.headForcedata = {
									id_player: res.data.response.forcePlayerID,
									id_event: res.data.response.forceEvtID,
									measurement_date: res.data.response.forceMeasurementDt,
									distance_one: parseFloat(res.data.response.forceDisOne),
									distance_two: parseFloat(res.data.response.forceDisTwo),
									distance_three: parseFloat(res.data.response.forceDisThree),
									attempt_one : parseFloat(res.data.response.forceAttemptOne),
									attempt_two : parseFloat(res.data.response.forceAttemptTwo),
									attempt_three : parseFloat(res.data.response.forceAttemptThree),
									result_one : parseFloat($scope.calculateForce(0)),
									result_two : parseFloat($scope.calculateForce(1)),
									result_three : parseFloat($scope.calculateForce(2))
								};*/
							} else {
								$scope.loadingEventAlert = {
									type: 'success',
									message: res.data.response
								};
							}

						}, function (err) {
							$scope.loadingEvent = false;

							if (err.status == 401) {

								Auth.deleteToken();

							} else {

								$scope.loadingEventAlert = {
									type: 'danger',
									message: err.status + ' ' + err.statusText
								};

							}

						});

					};	

				});

			};

		});

	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$scope.aimdata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			request_one: '',
			destination_one: '',
			request_two: '',
			destination_two: '',
			request_three: '',
			destination_three: '',
			request_four: '',
			destination_four: '',
			request_five: '',
			destination_five: '',
			request_six: '',
			destination_six: ''
		};

		$scope.forcedata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			distance_one: '',
			distance_two: '',
			distance_three: ''
		};

		$scope.jumpdata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			jump_high_one: '',
			jump_high_two: '',
			jump_high_three: ''
		};
				
		$scope.CreateHeadingAimForm.$setPristine();
		$scope.CreateHeadingForceForm.$setPristine();
		$scope.CreateHeadingJumpForm.$setPristine();

	};

	
	$scope.getEvent($scope.id_uniq);
	//if( $scope.event ) $scope.getPlayer($scope.player_id_uniq);

	

	$scope.setForceData = function( source ){
		$scope.headForcedata.id_player        = source.id_player;
		$scope.headForcedata.id_event         = source.id_event;
		$scope.headForcedata.measurement_date = source.measurement_date; 
		$scope.headForcedata.distance_one     = source.distance[0]; 
		$scope.headForcedata.distance_two     = source.distance[1]; 
		$scope.headForcedata.distance_three   = source.distance[2]; 
		$scope.headForcedata.attempt_one      = source.attempt[0]; 
		$scope.headForcedata.attempt_two      = source.attempt[1]; 
		$scope.headForcedata.attempt_three    = source.attempt[2];
	}

	$scope.calculateForce = function (field ) {
		//$scope.forceFieldValidated[field] = true;

		var velocityInitial = 0;
		var velocityFinal   = $scope.forcedata.attempt[field];
		var distance        = $scope.forcedata.distance[field];
		var mass            = $scope.ball_weight / 1000;


		console.log(velocityFinal,distance,$scope.ball_weight);
		var force           = mass * ( ( Math.pow( velocityFinal, 2 ) - velocityInitial ) / ( 2 * distance ) );

		return force.toFixed(2);

		//if( field === 2 ) $scope.CreateHeadingForceFormCompleted = true;
	};	

	$scope.EditHeadingForce = function(){
		$scope.creatingHeadingForce = true;
		$scope.creatingHeadingForceAlert = false;

		$scope.setForceData( $scope.forcedata );

		var data = $.param($scope.headForcedata);

		Heading.updateForce(data).then(function (res) {

			$scope.creatingHeadingForce = false;

			if (res.data.status == "success") {

				STFcache.delete('headingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/heading');

			} else {

				$scope.creatingHeadingForceAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingHeadingForce = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingHeadingForceAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	}

	$scope.EditHeadingAim = function () {

		$scope.creatingHeadingAim = true;
		$scope.creatingHeadingAimAlert = false;

		var data = $.param($scope.aimdata);

		Heading.updateAim(data).then(function (res) {

			$scope.creatingHeadingAim = false;

			if (res.data.status == "success") {
                
				STFcache.delete('headingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/heading');

			} else {

				$scope.creatingHeadingAimAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingHeadingAim = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingHeadingAimAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.EditHeadingJump = function () {

		$scope.creatingHeadingJump = true;
		$scope.creatingHeadingJumpAlert = false;

		var data = $.param($scope.jumpdata);

		Heading.updateJump(data).then(function (res) {

			$scope.creatingHeadingJump = false;
               
			if (res.data.status == "success") {

				STFcache.delete('headingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/heading');

			} else {

				$scope.creatingHeadingJumpAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingHeadingJump = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingHeadingJumpAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};


}]);

stf.controller('HeadingResultsController', ['$scope', function ($scope) {
	
}]);

