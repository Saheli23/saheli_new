var stf = angular.module('AnthropometricControllers', []);

stf.controller('AnthropometricListController', ['$scope', '$routeParams', 'Event', 'Anthropometric', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, Event, Anthropometric, Access, Auth, STFcache) {
	
	 $scope.bulk=false;
   	var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     };

	$scope.togglechild=function(e){
		//console.log("hh");
			 values=[];
			$(".childcheckbox").each(function(){
		        if($(this).is(":checked"))
		            values.push($(this).val());
		    });
		    //alert(values);
		   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
		   	angular.element("#mastercheckbox").prop("checked",true);
		   } 
		   else{
		
		    angular.element("#mastercheckbox").prop("checked",false);
		      }
	};


	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAnthropometrics = function (id_uniq, refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('anthropometricEvent' + $scope.id_uniq);
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.measurements = null;
		$scope.loadingAnthropometrics = true;
		$scope.loadingAnthropometricsAlert = false;

		Anthropometric.getAll(id_uniq).then(function (res) {

			$scope.loadingAnthropometrics = false;

			if (angular.isObject(res.data.response)) {

				$scope.measurements = res.data.response;
				
			} else {

				$scope.loadingAnthropometricsAlert = {
					type: 'success',
					message: res.data.response
				};

			}
			$scope.getAccess(1);

		}, function (err) {

			$scope.loadingAnthropometrics = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingAnthropometricsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getEvent($scope.id_uniq);
	$scope.getAnthropometrics($scope.id_uniq);	
    

    $scope.deleteAnthropometrics = function(id_uniq){

			if(confirm("Are you sure to delete this Anthropometrics of this player?")){
				var data = $.param({id_uniq:id_uniq,type:'delete'});
				Anthropometric.delete(data).then(function (res) {
					$scope.loadingAnthropometrics = false;
					
					$scope.getAnthropometrics($scope.id_uniq,true);
					if (angular.isObject(res.data.response)) {
						//$scope.measurements = res.data.response;

					} else {
						$scope.loadingAnthropometricsAlert = {type: 'success',message: res.data.response};
					}

				}, function (err) {

					$scope.loadingAnthropometrics = false;

					if (err.status == 401) {
						Auth.deleteToken();
					} else {
						$scope.loadingAnthropometricsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
					}
				});
			}
		};
	
	$scope.bulkDeleteAnthropometrics = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Anthropometrics of this player?")){
			var data = $.param({id_uniq:values,type:'delete',action:action_id});
			Anthropometric.multidelete(data).then(function (res) {
					$scope.loadingAnthropometrics = false;
					
					$scope.getAnthropometrics($scope.id_uniq,true);
					if (angular.isObject(res.data.response)) {
						//$scope.measurements = res.data.response;

					} else {
						$scope.loadingAnthropometricsAlert = {type: 'success',message: res.data.response};
					}

				}, function (err) {

					$scope.loadingAnthropometrics = false;

					if (err.status == 401) {
						Auth.deleteToken();
					} else {
						$scope.loadingAnthropometricsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
					}
				});
			}
	 }
	 else{
	 	alert("Please select atleast one to delete.");
	 }
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

	$scope.orderOptions = [
		{ value: 'tx_firstname', text: 'Name' },
		{ value: '-dt_measurement', text: 'Date' }
	];
	
}]);

stf.controller('AnthropometricCreateController', ['$scope', '$routeParams', '$location', '$rootScope', 'Event', 'Player', 'Auth', 'Anthropometric', 'STFcache','$filter', function ($scope, $routeParams, $location, $rootScope, Event, Player, Auth, Anthropometric, STFcache,$filter) {
	
	$scope.id_uniq = $routeParams.id_uniq;
	$scope.player = null;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);
    
    var measurement = {
		"MKS" : {
			"size" : "cm",
			"weight" :"kg",
			"diameter":"cm"
			
		},
		"YLS" : {
			"size" : "inch",
			"weight" :"pound",
			"diameter":"cm"
		}
	};
$scope.getHeadingMeasurement = function( type, dim ){

		return measurement[type][dim];
	};
	$scope.formdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		size: '',
		weight: '',
		chest_diameter: '',
		hip_diameter: '',
		left_thigh_diameter: '',
		right_thigh_diameter: '',
		left_ankle_diameter: '',
		right_ankle_diameter: '',
		head_diameter: ''
	};

	//$scope.date = null;
	//$scope.maxDate = new Date();
	// $scope.pickDateOptions = {
	// 	format: 'mm/dd/yyyy',
	// 	selectYears: true,
	// 	selectMonths: true,
	// };

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;
				$scope.formdata.id_event = $scope.event.id;
                
                  //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }

	            if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length >1){ 
	             
	                this.value = parseFloat($(this).val().slice(0, -1));
	            }  
	         	}    
            	var charCode = (evt.which) ? evt.which : event.keyCode;
				if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 	return true;


				});

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			if (angular.isObject(res.data.response)) {

				var player = res.data.response;
				
				$scope.formdata.id_player = player.id;

				Anthropometric.check($scope.formdata.id_player, $scope.formdata.id_event).then(function (res) {

					$scope.loadingPlayer = false;

					if (res.data.player_exists == 1) {
                       
						$scope.loadingPlayerAlert = {
							type: 'success',
							message: res.data.response
						};
						
					} else {

						$scope.player = player;

					}

				}, function (err) {

					$scope.loadingPlayer = false;

					$scope.loadingPlayerAlert = {
						type: 'danger',
						message: res.data.response
					};

				});

			} else {

				$scope.loadingPlayer = false;

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$scope.formdata.id_player = '';
		$scope.formdata.measurement_date = '';
		$scope.formdata.size = '';
		$scope.formdata.weight = '';
		$scope.formdata.chest_diameter = '';
		$scope.formdata.hip_diameter = '';
		$scope.formdata.left_thigh_diameter = '';
		$scope.formdata.right_thigh_diameter = '';
		$scope.formdata.left_ankle_diameter = '';
		$scope.formdata.right_ankle_diameter = '';
		$scope.formdata.head_diameter = '';

		$scope.CreateAnthropometricForm.$setPristine();

	};

	$scope.getEvent($scope.id_uniq);

	$scope.createAnthropometric = function () {

		$scope.creatingAnthropometric = true;
		$scope.creatingAnthropometricAlert = false;

		var data = $.param($scope.formdata);

		Anthropometric.create(data).then(function (res) {

			$scope.creatingAnthropometric = false;

			if (res.data.status == "success") {

				STFcache.delete('anthropometricEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/anthropometrics');

			} else {

				$scope.creatingAnthropometricAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingAnthropometric = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingAnthropometricAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('AnthropometricDetailController', ['$scope', 'Access', function ($scope, Access ) {
	
}]);

stf.controller('AnthropometricEditController', ['$scope', '$routeParams', '$location', '$rootScope', '$filter', 'Anthropometric', 'Auth', 'STFcache','Event', function ($scope, $routeParams, $location, $rootScope, $filter, Anthropometric, Auth, STFcache,Event) {
	
	var id_uniq = $routeParams.id_uniq;

	$scope.randomNum = Math.floor((Math.random() * 10) + 1);
    
     var measurement = {
		"MKS" : {
			"size" : "cm",
			"weight" :"kg",
			"diameter":"cm"
			
		},
		"YLS" : {
			"size" : "inch",
			"weight" :"pound",
			"diameter":"cm"
		}
	};
$scope.getHeadingMeasurement = function( type, dim ){
     // if(type="undefined")return true;
		return measurement[type][dim];
	};

	$scope.formdata = {
		measurement_date: '',
		size: '',
		weight: '',
		chest_diameter: '',
		hip_diameter: '',
		left_thigh_diameter: '',
		right_thigh_diameter: '',
		left_ankle_diameter: '',
		right_ankle_diameter: '',
		head_diameter: ''
	};

	$scope.date = null;
	$scope.maxDate = new Date();
	$scope.pickDateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: true,
		selectMonths: true,
	};

	
   
	$scope.getAnthropometric = function (id_uniq) {

		$scope.loadingAnthropometric = true;
		$scope.loadingAnthropometricAlert = false;

		Anthropometric.getOne(id_uniq).then(function (res) {

			$scope.loadingAnthropometric = false;

			if (angular.isObject(res.data.response)) {

				$scope.anthropometric = res.data.response;
                $scope.eventid=parseFloat($scope.anthropometric.stf_measurement_abbr);
				$scope.formdata.measurement_date = $filter('date')($scope.anthropometric.dt_measurement, 'MM/dd/yyyy');
				$scope.formdata.size = parseFloat($scope.anthropometric.nu_size);
				$scope.formdata.weight = parseFloat($scope.anthropometric.nu_weight);
				$scope.formdata.chest_diameter = parseFloat($scope.anthropometric.nu_chest_diameter);
				$scope.formdata.hip_diameter = parseFloat($scope.anthropometric.nu_hip_diameter);
				$scope.formdata.left_thigh_diameter = parseFloat($scope.anthropometric.nu_left_thigh_diameter);
				$scope.formdata.right_thigh_diameter = parseFloat($scope.anthropometric.nu_right_thigh_diameter);
				$scope.formdata.left_ankle_diameter = parseFloat($scope.anthropometric.nu_left_ankle_diameter);
				$scope.formdata.right_ankle_diameter = parseFloat($scope.anthropometric.nu_right_ankle_diameter);
				$scope.formdata.head_diameter = parseFloat($scope.anthropometric.nu_head_diameter);
              
                    //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
           if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length >1){ 
	             
	                this.value = parseFloat($(this).val().slice(0, -1));
	            }  
	         	}    
            	var charCode = (evt.which) ? evt.which : event.keyCode;
				if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 	return true;


			});


				

			} else {

				$scope.loadingAnthropometricAlert = {
					type: 'success',
					message: res.data.response

				};
            
			}

		}, function (err) {

			$scope.loadingAnthropometric = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingAnthropometricAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAnthropometric(id_uniq);

   
	$scope.editAnthropometric = function () {

		$scope.editingAnthropometric = true;
		$scope.editingAnthropometricAlert = false;

		var data = $.param($scope.formdata);

		Anthropometric.edit(data, id_uniq).then(function (res) {

			$scope.editingAnthropometric = false;

			if (res.data.status == 'success') {

				STFcache.delete('anthropometric' + id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + res.data.id_uniq_event + '/anthropometrics');

			} else {

				$scope.editingAnthropometricAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.editingAnthropometric = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingAnthropometricAlert = {
					type: 'danger',
					message: err.status + ' '  + err.statusText
				}

			}

		});

	};


}]);