var stf = angular.module('UserControllers', []);

stf.controller('UserListController', ['$scope', 'User', 'Section', 'Access', 'Auth', 'STFcache', function ($scope, User, Section, Access, Auth, STFcache) {
     
  
   $scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());

    });
    
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};


	$scope.getUsers = function (refresh) {
        
		if (refresh !== undefined && refresh == true) STFcache.delete('users');
         
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.users = null;
		$scope.loadingUsers = true;
		$scope.loadingUsersAlert = false;
        
		User.getAll().then(function (res) {

			$scope.loadingUsers = false;
           
			if (angular.isObject(res.data.response)) {
				$scope.users = res.data.response;
                
				$scope.getAccess(5);
				
			} else {
				$scope.loadingUsersAlert = {
					type: 'success',
					message: res.data.response
				};
			}
			$scope.getAccess(5);
		}, function (err) {

			$scope.loadingUsers = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingUsersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getUsers();
    
	$scope.deleteUser = function( user_id, ind ){

		if(confirm("Are you sure to delete this User?")){
			var data = $.param({id_user:user_id,type:'delete'});
			User.delete(data).then(function (res) {
				
				var loggedInUser=$scope.loggedInUser.id_uniq;
				
				//if delete its own account session deleted
				if(user_id==loggedInUser){
					Auth.deleteToken();
				}
				
				$scope.loadingUsers = false;
				
				$scope.getUsers(true);
				if (angular.isObject(res.data.response)) {
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingUsersAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingUsers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingUsersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};

	$scope.bulkDeleteUser = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);

       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this User?")){
			var data = $.param({id_user:values,type:'delete',action:action_id});
			User.multiDelete(data).then(function (res) {
                
                //if delete its own account session deleted
				var loggedInUser=$scope.loggedInUser.id_uniq;
				console.log(values.indexOf(loggedInUser));
				if(values.indexOf(loggedInUser) !== -1) {
				     Auth.deleteToken();
				  }
				
				
				$scope.loadingUsers = false;
				
				$scope.getUsers(true);
				if (angular.isObject(res.data.response)) {
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingUsersAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingUsers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingUsersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one user to delete.");
	 }
	};
   

	$scope.getAccess = function(id){
		//console.log($scope.users);
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
			changeAttr( $scope.users, 'users', $scope.access.bo_view );
		
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}
    
   
	$scope.restrictView = function(){
		$('#alertMessage').show();
		setTimeout(function(){ 
			$('#alertMessage').hide();
		}, 2000);
	}

	function compile(element){
		var el = angular.element(element);    
		$scope = el.scope();
		$injector = el.injector();
		$injector.invoke(function($compile){
			$compile(el)($scope);
		})     
	}

	function changeAttr( obj, slug, viewAccess ){
        for( var i=0; i < obj.length; i++ ){
            var el = document.getElementById((slug.substring(0, slug.length-1))+"_id_"+i);
            if( viewAccess ){
                el.removeAttribute("ng-click");
                el.setAttribute("ng-href",slug+"/"+obj[i].id_uniq);
                el.setAttribute("href",slug+"/"+obj[i].id_uniq);
            } else {
                el.removeAttribute("ng-href");
                el.removeAttribute("href");
                el.setAttribute("ng-click", "restrictView()");
                compile(el);
            }
        }
    }


	$scope.orderOptions = [
		{ value: 'tx_name', text: 'Full Name' },
		{ value: 'tx_email', text: 'Email' },
		{ value: 'bo_admin', text: 'Role' }
	];
		 $scope.arrange=false;
			$scope.$watch('o', function() {
		    //alert('changed');
		    $scope.arrange=false;
		    console.log($scope.o);
		    if($scope.o=="bo_admin"){
		    	$scope.arrange=true;
		    }
		});

}]);

stf.controller('UserCreateController', ['$scope', '$rootScope', '$location', '$window', 'User', 'Section', 'Auth', 'STFcache', function ($scope, $rootScope, $location, $window, User, Section, Auth, STFcache) {

	$scope.formdata = {
		firstname: '',
		lastname: '',
		email: '',
		password: '',
		userfile: '',
		is_admin: '',
		sections: function(){
			var arr = [];
			Section.getSection().then(function(res){
				angular.forEach(res.data.response, function(value, key) {
					arr.push({
						id: value.id,
						name: value.section_name,
						view: '0',
						add: '0',
						edit: '0',
						'delete': '0'
					});
				});
			});
			return arr;
		}()
	};

	$scope.adminOptions = [
		{ value: 0, text: 'Collaborator' },
		{ value: 1, text: 'Administrator' }
	];
	
	$scope.$watch('formdata.is_admin', function () {
		$scope.formdata.has_admin = parseInt( $scope.formdata.is_admin ) ? true : false;

		angular.forEach( $scope.formdata.sections, function( value, key ){
			$scope.formdata.sections[key].view      = ($scope.formdata.is_admin == 1) ? '1' : '1';
			$scope.formdata.sections[key].add       = ($scope.formdata.is_admin == 1) ? '1' : '0';
			$scope.formdata.sections[key].edit      = ($scope.formdata.is_admin == 1) ? '1' : '0';
			$scope.formdata.sections[key]['delete'] = ($scope.formdata.is_admin == 1) ? '1' : '0';

		});
	});

	$scope.createUser = function () {

		$scope.creatingUserAlert = false;
		$scope.creatingUser = true;

		var data = new FormData();

		data.append('firstname', $scope.formdata.firstname);
		data.append('lastname', $scope.formdata.lastname);
		data.append('email', $scope.formdata.email);
		data.append('password', $scope.formdata.password);
		data.append('delete', $scope.formdata.delete);
		data.append('is_admin', $scope.formdata.is_admin);
		data.append('userfile', $scope.formdata.userfile);
		data.append('sections', JSON.stringify($scope.formdata.sections));

		/*angular.forEach( $scope.formdata.sections, function( value, key ){
			
		});*/

		User.createUser(data).then(function (res) {

			$scope.creatingUser = false;
			if (res.data.status == "success") {

				STFcache.delete('users');
				STFcache.delete('userUniqIDs');
				
				if (res.data.has_image == 1) {

					$window.sessionStorage.setItem('response', res.data.response);

					$location.path('/set_image/use_/' + res.data.id_uniq);

				} else {

					$rootScope.tooltipMessageAlert = {
						type: 'success',
						message: res.data.response
					}

					$location.path('/users');

				}
				
			} else {

				$scope.creatingUserAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.creatingUser = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingUserAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.setCheckBox = function(idx){
		if( idx === 0 ){
			if( parseInt( $scope.formdata.sections[idx].add ) || 
				parseInt( $scope.formdata.sections[idx].edit ) ||
				parseInt( $scope.formdata.sections[idx].delete ) )
				$scope.formdata.sections[idx].view = "1";
		}
	};
}]);

stf.controller('UserDetailsController', ['$scope', '$routeParams', '$location', 'User', 'Access', 'Auth','STFcache', function ($scope, $routeParams, $location, User, Access, Auth,STFcache) {
	
	$scope.id_uniq = $routeParams.id_uniq;
    


	$scope.getUniqIDs = function () {

		$scope.loadingUniqIDs = true;
		$scope.loadingUniqIDsAlert = false;

		User.getUniqIDs().then(function (res) {

			$scope.loadingUniqIDs = false;

			$scope.result = res.data.response;
			$scope.totalItems = parseInt($scope.result.length);
			$scope.currentItem = parseInt(_.findIndex($scope.result, { id_uniq: $scope.id_uniq }));
			$scope.nextItem = $scope.currentItem + 1;	
			$scope.prevItem = $scope.currentItem - 1;

			$scope.nextID = ($scope.nextItem > -1 && $scope.nextItem < $scope.totalItems) ? $scope.result[$scope.nextItem].id_uniq : false;
			$scope.prevID = ($scope.prevItem >= 0) ? $scope.result[$scope.prevItem].id_uniq : false;

		}, function (err) {

			$scope.loadingUniqIDs = false;

			if (err.status == 401) {

				Auth.deleteToken();

			};

		});

	};

	$scope.prevUser = function () { $location.path('/users/' + $scope.prevID); };
	$scope.nextUser = function () { $location.path('/users/' + $scope.nextID); };

	$scope.getUser = function (id_uniq) {

		$scope.loadingUser = true;
		$scope.loadingUserAlert = false;

		User.getOne(id_uniq).then(function (res) {

			$scope.loadingUser = false;

			if (angular.isObject(res.data.response)) {

				$scope.user = res.data.response;
				$scope.getUniqIDs();
				$scope.getAccess(5);

			} else {

				$scope.loadingUserAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingUser = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingUserAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getUser($scope.id_uniq);

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

	$scope.deleteUser = function( user_id, ind ){
       // console.log(user_id);
		if(confirm("Are you sure to delete this User?")){
			var data = $.param({id_user:user_id,type:'delete'});
			User.delete(data).then(function (res) {

				var loggedInUser=$scope.loggedInUser.id_uniq;
				
				//if delete its own account session deleted
				if(user_id==loggedInUser){
					Auth.deleteToken();
				}
				
				$scope.loadingUsers = false;
				
				$scope.getUsers(true);
				if (angular.isObject(res.data.response)) {
					//$scope.measurements = res.data.response;
                   
				} else {
					$scope.loadingUsersAlert = {type: 'success',message: res.data.response};
				    $location.path('/users');
				}

			}, function (err) {

				$scope.loadingUsers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingUsersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};
   

	$scope.getUsers = function (refresh) {
        
		if (refresh !== undefined && refresh == true) STFcache.delete('users');
        
		$scope.users = null;
		$scope.loadingUsers = true;
		$scope.loadingUsersAlert = false;
        
		User.getAll().then(function (res) {

			$scope.loadingUsers = false;
           
			if (angular.isObject(res.data.response)) {
				$scope.users = res.data.response;
                
				$scope.getAccess(5);
				
			} else {
				$scope.loadingUsersAlert = {
					type: 'success',
					message: res.data.response
				};
			}
			$scope.getAccess(5);
		}, function (err) {

			$scope.loadingUsers = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingUsersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('UserEditController', ['$scope', '$rootScope', '$location', '$routeParams', '$window', 'User', 'Section', 'Auth', 'STFcache', function ($scope, $rootScope, $location, $routeParams, $window, User, Section, Auth, STFcache) {



	$scope.formdata = {
		firstname: '',
		lastname: '',
		email: '',
		password: '',
		userfile: '',
		is_admin: '',
		sections: function(){
			var arr = [];
			Section.getSection().then(function(res){
				angular.forEach(res.data.response, function(value, key) {
					arr.push({
						id: value.id,
						name: value.section_name,
						view: '0',
						add: '0',
						edit: '0',
						'delete': '0'
					});
				});
			});
			return arr;
		}()
	};

	$scope.adminOptions = [
		{ value: '0', text: 'Collaborator' },
		{ value: '1', text: 'Administrator' }
	];

	
	var id_uniq = $routeParams.id_uniq;

	$scope.getUser = function (id_uniq) {

		$scope.loadingUser = true;
		$scope.loadingUserAlert = false;

		User.getOne(id_uniq).then(function (res) {

			$scope.loadingUser = false;

			if (angular.isObject(res.data.response)) {
				$scope.user = res.data.response;
				$scope.formdata.firstname = $scope.user.tx_firstname;
				$scope.formdata.lastname  = $scope.user.tx_lastname;
				$scope.formdata.email     = $scope.user.tx_email;
				$scope.formdata.is_admin  = $scope.user.bo_admin;
				$scope.formdata.sections  = $scope.user.sections;
				$scope.formdata.user_type  = $scope.user.bo_admin;
				$scope.$emit('userLoaded');
			} else {
				$scope.loadingUserAlert = {
					type: 'success',
					message: res.data.response
				};
			}
		}, function (err) {
			$scope.loadingUser = false;
			if (err.status == 401) {
				Auth.deleteToken();
			} else {
				$scope.loadingUserAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};
			}
		});
	};

	$scope.getUser(id_uniq);

	$scope.$on('userLoaded', function () {
		$scope.changeUserType(0);
	});

	$scope.editUser = function () {

		$scope.editingUserAlert = false;
		$scope.editingUser = true;

		var data = new FormData();

		data.append('firstname', $scope.formdata.firstname);
		data.append('lastname', $scope.formdata.lastname);
		data.append('email', $scope.formdata.email);
		data.append('password', $scope.formdata.password);
		data.append('is_admin', $scope.formdata.is_admin);
		data.append('userfile', $scope.formdata.userfile);
		data.append('sections', JSON.stringify($scope.formdata.sections));
		
		
		User.editUser(id_uniq, data).then(function (res) {

			$scope.editingUser = false;
			
			if (res.data.status == "success") {

				STFcache.delete('users');
				STFcache.delete('users' + id_uniq);

				if (res.data.has_image == 1) {

					$window.sessionStorage.setItem('response', res.data.response);

					$location.path('/set_image/use_/' + res.data.id_uniq);

				} else {

					$rootScope.tooltipMessageAlert = {
						type: 'success',
						message: res.data.response
					}

					$location.path('/users');

				}

			} else {

				$scope.editingUserAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.editingUser = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingUserAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.setCheckBox = function(idx){
		if( idx === 0 ){
			if( parseInt( $scope.formdata.sections[idx].add ) || 
				parseInt( $scope.formdata.sections[idx].edit ) ||
				parseInt( $scope.formdata.sections[idx].delete ) )
				$scope.formdata.sections[idx].view = "1";
		}
	};
    

	$scope.changeUserType = function(type){
		$scope.formdata.has_admin = parseInt( $scope.formdata.is_admin ) ? true : false;
		angular.forEach( $scope.formdata.sections, function( value, key ){
			$scope.formdata.sections[key].view      = type === 0 ? value.view : "1";
			$scope.formdata.sections[key].add       = type === 0 ? value.add : $scope.formdata.is_admin;
			$scope.formdata.sections[key].edit      = type === 0 ? value.edit : $scope.formdata.is_admin;
			$scope.formdata.sections[key]['delete'] = type === 0 ? value['delete'] : $scope.formdata.is_admin;
		});
	};

}]);