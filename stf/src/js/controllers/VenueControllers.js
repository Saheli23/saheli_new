var stf = angular.module('VenueControllers', []);

stf.controller('VenueListController', ['$scope', 'Venue', 'Access', 'Auth', 'STFcache', function ($scope, Venue, Access, Auth, STFcache) {
	
	$scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};

	$scope.getVenues = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('venues');

		$scope.venues = null;
		$scope.loadingVenues = true;
		$scope.loadingVenuesAlert = false;

		Venue.getVenues().then(function (res) {

			$scope.loadingVenues = false;

			if (angular.isObject(res.data.response)) {

				$scope.venues = res.data.response;
				$scope.getAccess(4);

			} else {

				$scope.loadingVenuesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingVenues = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingVenuesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getVenues();

	$scope.deleteVenue = function( venue_id, ind ){

		if(confirm("Are you sure to delete this Venue?")){
			var data = $.param({id_venue:venue_id,type:'delete'});
			Venue.delete(data).then(function (res) {
				$scope.loadingVenues = false;
				
				$scope.getVenues(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingVenuesAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {

				$scope.loadingVenues = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingVenuesAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};

	$scope.bulkDeleteVenue = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Venue?")){
			var data = $.param({id_venue:values,type:'delete',action:action_id});
			Venue.multidelete(data).then(function (res) {
				$scope.loadingVenues = false;
				
				$scope.getVenues(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingVenuesAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {

				$scope.loadingVenues = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingVenuesAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one user to delete.");
	 }
	};


	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
			changeAttr( $scope.venues, 'venues', $scope.access.bo_view );
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

	$scope.restrictView = function(){
		$('#alertMessage').show();
		setTimeout(function(){ 
			$('#alertMessage').hide();
		}, 2000);
	}

	function compile(element){
		var el = angular.element(element);    
		$scope = el.scope();
		$injector = el.injector();
		$injector.invoke(function($compile){
			$compile(el)($scope);
		})     
	}

	function changeAttr( obj, slug, viewAccess ){
        for( var i=0; i < obj.length; i++ ){
            var el = document.getElementById((slug.substring(0, slug.length-1))+"_id_"+i);
            if( viewAccess ){
                el.removeAttribute("ng-click");
                el.setAttribute("ng-href",slug+"/"+obj[i].id_uniq);
                el.setAttribute("href",slug+"/"+obj[i].id_uniq);
            } else {
                el.removeAttribute("ng-href");
                el.removeAttribute("href");
                el.setAttribute("ng-click", "restrictView()");
                compile(el);
            }
        }
    }


	$scope.orderOptions = [
		{ value: 'tx_name', text: 'Name' },
		{ value: 'tx_email', text: 'Email' },
		{ value: 'tx_phone', text: 'Phone number' },
		{ value: 'tx_contact_name', text: 'Contact Person' }
	];

}]);

stf.controller('VenueCreateController', ['$scope', '$filter', '$location', '$rootScope', '$timeout', 'Country', 'Auth', 'Venue', 'STFcache', function ($scope, $filter, $location, $rootScope, $timeout, Country, Auth, Venue, STFcache) {
    
     angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
         //    if (String.fromCharCode(evt.which) == "."){
        	// return false;
         //    }
            var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;

			});

	$scope.formdata = {
		name: '',
		country: '',
		state: '',
		city: '',
		address: '',
		zipcode: '',
		phoneprefix_one: '-',
		phonenumber_one: '',
		phoneprefix_two: '-',
		phonenumber_two: '',
		email: '',
		website: '',
		facebook: '',
		instagram: '',
		twitter: '',
		contact_person: '',
		contact_person_phoneprefix: '-',
		contact_person_phonenumber: '',
		latitude: 40.148237,
		longitude: -101.762500
	};

	$timeout(function () {

		$scope.map = {
			center: {
				latitude: $scope.formdata.latitude,
				longitude: $scope.formdata.longitude
			},
			zoom: 4
		};

		$scope.marker = {
			id: 0,
			options: {
				draggable: true
			},
			coords: {
				latitude: $scope.map.center.latitude,
				longitude: $scope.map.center.longitude
			},
			events: {
				dragend: function (marker, eventName, args) {

					$scope.formdata.latitude = marker.getPosition().lat();
					$scope.formdata.longitude = marker.getPosition().lng();

				}
			}
		};

		$scope.searchBox = {
			template: 'dist/views/templates/google.maps.searchbox.html',
			events: {
				places_changed: function (searchbox) {

					var place = searchbox.getPlaces();

					var lat = place[0].geometry.location.lat();
					var lng = place[0].geometry.location.lng();

					$scope.formdata.latitude = lat;
					$scope.formdata.longitude = lng;

					$scope.map.center.latitude = lat;
					$scope.map.center.longitude	= lng;

					$scope.marker.coords.latitude = lat;
					$scope.marker.coords.longitude	= lng;

					$scope.map.zoom = 17;

				}
			}
		};

		$scope.renderMap = true;

	}, 500);

	$scope.getCountries = function () {

		$scope.loadingCountries = true;
		$scope.loadingCountriesAlert = false;

		Country.getCountries().then(function (res) {

			$scope.loadingCountries = false;

			if (angular.isObject(res.data.response)) {

				$scope.countries = res.data.response;

			} else {

				$scope.loadingCountriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCountries = false;

			if (err.status == 401) { 

				Auth.deleteToken();

			} else {

				$scope.loadingCountriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountryStates = function (id_country) {

		$scope.loadingStates = true;
		$scope.loadingStatesAlert = false;

		Country.getCountryStates(id_country).then(function (res) {

			$scope.loadingStates = false;

			if (angular.isObject(res.data.response)) {

				$scope.states = res.data.response;

			} else {

				$scope.loadingStatesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStates = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStatesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountries();

	$scope.$watch('formdata.country', function () {

		var countryIndex = _.findIndex($scope.countries, { id: $scope.formdata.country });

		if (countryIndex != -1) {

			$scope.zipcodeMask = $filter('zipcodeMask')($scope.countries[countryIndex].tx_zipcode_format);
			$scope.zipcodeRequired = ($scope.zipcodeMask.length > 0) ? true : false;

			$scope.formdata.phoneprefix_one = $scope.countries[countryIndex].tx_phone_prefix;
			$scope.formdata.phoneprefix_two = $scope.countries[countryIndex].tx_phone_prefix;
			$scope.formdata.contact_person_phoneprefix = $scope.countries[countryIndex].tx_phone_prefix;

			var id_country = $scope.formdata.country;
			$scope.getCountryStates(id_country);

		};

		$scope.states = null;
		$scope.formdata.state = '';
		$scope.CreateVenueForm.state.$setPristine();

	});

	$scope.createVenue = function () {

		$scope.creatingVenue = true;
		$scope.creatingVenueAlert = false;

		var data = $.param($scope.formdata);

		Venue.createVenue(data).then(function (res) {

			$scope.creatingVenue = false;

			if (res.data.status == "success") {

				STFcache.delete('venues');
				STFcache.delete('venueUniqIDs');

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Venue created successfully'
				};

				$location.path('venues');

			} else {

				$scope.creatingVenueAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingVenue = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingVenueAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('VenueDetailsController', ['$scope', '$routeParams', '$timeout', '$location', 'Venue', 'Access', 'Auth','STFcache',function ($scope, $routeParams, $timeout, $location, Venue, Access, Auth,STFcache) {
	
	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getUniqIDs = function () {

		$scope.loadingUniqIDs = true;
		$scope.loadingUniqIDsAlert = false;

		Venue.getUniqIDs().then(function (res) {

			$scope.loadingUniqIDs = false;

			$scope.result = res.data.response;
			$scope.totalItems = parseInt($scope.result.length);
			$scope.currentItem = parseInt(_.findIndex($scope.result, { id_uniq: $scope.id_uniq }));
			$scope.nextItem = $scope.currentItem + 1;	
			$scope.prevItem = $scope.currentItem - 1;

			$scope.nextID = ($scope.nextItem > -1 && $scope.nextItem < $scope.totalItems) ? $scope.result[$scope.nextItem].id_uniq : false;
			$scope.prevID = ($scope.prevItem >= 0) ? $scope.result[$scope.prevItem].id_uniq : false;

		}, function (err) {

			$scope.loadingUniqIDs = false;

			if (err.status == 401) {

				Auth.deleteToken();

			};

		});

	};

	$scope.prevVenue = function () { $location.path('/venues/' + $scope.prevID); };
	$scope.nextVenue = function () { $location.path('/venues/' + $scope.nextID); };

	$scope.getVenue = function (id_uniq) {

		$scope.loadingVenue = true;
		$scope.loadingVenueAlert = false;

		Venue.getVenue(id_uniq).then(function (res) {

			$scope.loadingVenue = false;

			if (angular.isObject(res.data.response)) {

				$scope.venue = res.data.response;
				$scope.getUniqIDs();

				$scope.$emit('venueLoaded');
				$scope.getAccess(4);
												
			} else {

				$scope.loadingVenueAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingVenue = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingVenueAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};
	$scope.getVenues = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('venues');

		$scope.venues = null;
		$scope.loadingVenues = true;
		$scope.loadingVenuesAlert = false;

		Venue.getVenues().then(function (res) {

			$scope.loadingVenues = false;

			if (angular.isObject(res.data.response)) {

				$scope.venues = res.data.response;
				$scope.getAccess(4);

			} else {

				$scope.loadingVenuesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingVenues = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingVenuesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};


	$scope.$on('venueLoaded', function () {

		$timeout(function () {

			$scope.map = {
				center: {
					latitude: $scope.venue.tx_latitude,
					longitude: $scope.venue.tx_longitude
				},
				zoom: 17
			};

			$scope.marker = {
				id: 0,
				coords: {
					latitude: $scope.map.center.latitude,
					longitude: $scope.map.center.longitude
				},
				events: {
					dragend: function (marker, eventName, args) {

						$scope.formdata.latitude = marker.getPosition().lat();
						$scope.formdata.longitude = marker.getPosition().lng();

					}
				}
			};

			$scope.searchBox = {
				template: 'dist/views/templates/google.maps.searchbox.html',
				events: {
					places_changed: function (searchbox) {

						var place = searchbox.getPlaces();

						var lat = place[0].geometry.location.lat();
						var lng = place[0].geometry.location.lng();

						$scope.formdata.latitude = lat;
						$scope.formdata.longitude = lng;

						$scope.map.center.latitude = lat;
						$scope.map.center.longitude	= lng;

						$scope.marker.coords.latitude = lat;
						$scope.marker.coords.longitude	= lng;

					}
				}
			};

			$scope.renderMap = true;

		}, 500);

	});

	$scope.getVenue($scope.id_uniq);

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}
   
   $scope.deleteVenue = function( venue_id, ind ){

		if(confirm("Are you sure to delete this Venue?")){
			var data = $.param({id_venue:venue_id,type:'delete'});
			Venue.delete(data).then(function (res) {
				$scope.loadingVenues = false;
				
				$scope.getVenues(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingVenuesAlert = {type: res.data.status,message: res.data.response};
					    $location.path('/venues');
				}

			}, function (err) {

				$scope.loadingVenues = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingVenuesAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};
}]);

stf.controller('VenueEditController', ['$scope', '$routeParams', '$rootScope', '$filter', '$timeout', '$location', 'Venue', 'Auth', 'Country', 'STFcache', 'uiGmapGoogleMapApi', function ($scope, $routeParams, $rootScope, $filter, $timeout, $location, Venue, Auth, Country, STFcache, uiGmapGoogleMapApi) {
    
     angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
         //    if (String.fromCharCode(evt.which) == "."){
        	// return false;
         //    }
         	var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;

			});

	var id_uniq = $routeParams.id_uniq;
	
	$scope.formdata = {
		name: '',
		country: '',
		state: '',
		city: '',
		address: '',
		zipcode: '',
		phoneprefix_one: '-',
		phonenumber_one: '',
		phoneprefix_two: '-',
		phonenumber_two: '',
		email: '',
		website: '',
		facebook: '',
		instagram: '',
		twitter: '',
		contact_person: '',
		contact_person_phoneprefix: '-',
		contact_person_phonenumber: '',
		latitude: '',
		longitude: ''
	};

	$scope.getVenue = function (id_uniq) {

		$scope.loadingVenue = true;
		$scope.loadingVenueAlert = false;

		Venue.getVenue(id_uniq).then(function (res) {

			$scope.loadingVenue = false;

			if (angular.isObject(res.data.response)) {

				$scope.venue = res.data.response;

				$scope.formdata.name = $scope.venue.venue_name;
				$scope.formdata.country = $scope.venue.id_country;
				$scope.formdata.state = $scope.venue.id_state;
				$scope.formdata.city = $scope.venue.id_city;
				$scope.formdata.address = $scope.venue.tx_address;
				$scope.formdata.phoneprefix_one = $scope.venue.tx_phone_prefix;
				$scope.formdata.phonenumber_one = parseFloat($scope.venue.tx_phone);
				$scope.formdata.phoneprefix_two = $scope.venue.tx_phone_prefix_2;
				$scope.formdata.phonenumber_two = parseFloat($scope.venue.tx_phone_2);
				$scope.formdata.email = $scope.venue.tx_email;
				$scope.formdata.website = $scope.venue.tx_website;
				$scope.formdata.facebook = $scope.venue.tx_facebook;
				$scope.formdata.instagram = $scope.venue.tx_instagram;
				$scope.formdata.twitter = $scope.venue.tx_twitter;
				$scope.formdata.contact_person = $scope.venue.tx_contact_name;
				$scope.formdata.contact_person_phoneprefix = $scope.venue.tx_contact_phone_prefix;
				$scope.formdata.contact_person_phonenumber = parseFloat($scope.venue.tx_contact_phone);
				$scope.formdata.latitude = $scope.venue.tx_latitude;
				$scope.formdata.longitude = $scope.venue.tx_longitude;

				$scope.$emit('venueLoaded');

			} else {

				$scope.loadingVenueAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingVenue = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingVenueAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountries = function () {

		$scope.loadingCountries = true;
		$scope.loadingCountriesAlert = false;

		Country.getCountries().then(function (res) {

			$scope.loadingCountries = false;

			if (angular.isObject(res.data.response)) {

				$scope.countries = res.data.response;
				$scope.$emit('countriesLoaded');

			} else {

				$scope.loadingCountriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCountries = false;

			if (err.status == 401) { 

				Auth.deleteToken();

			} else {

				$scope.loadingCountriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountryStates = function (id_country) {

		$scope.loadingStates = true;
		$scope.loadingStatesAlert = false;

		Country.getCountryStates(id_country).then(function (res) {

			$scope.loadingStates = false;

			if (angular.isObject(res.data.response)) {

				$scope.states = res.data.response;

			} else {

				$scope.loadingStatesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStates = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStatesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getVenue(id_uniq);
	$scope.getCountries();

	$scope.$on('countriesLoaded', function () {

		$scope.$watch('formdata.country', function () {

			var countryIndex = _.findIndex($scope.countries, { id: $scope.formdata.country });

			if (countryIndex != -1) {

				$scope.zipcodeMask = $filter('zipcodeMask')($scope.countries[countryIndex].tx_zipcode_format);
				$scope.zipcodeRequired = ($scope.zipcodeMask.length > 0) ? true : false;
				
				$timeout(function () { $scope.formdata.zipcode = $scope.venue.tx_zipcode; }, 1000);

				$scope.formdata.phoneprefix_one = $scope.countries[countryIndex].tx_phone_prefix;
				$scope.formdata.phoneprefix_two = $scope.countries[countryIndex].tx_phone_prefix;
				$scope.formdata.contact_person_phoneprefix = $scope.countries[countryIndex].tx_phone_prefix;

				var id_country = $scope.formdata.country;
				$scope.getCountryStates(id_country);

			} else {

				$scope.states = null;
				$scope.formdata.state = '';
				$scope.EditVenueForm.state.$setPristine();

			}

		});

	});

	$scope.$on('venueLoaded', function () {

		$timeout(function () {

			$scope.map = {
				center: {
					latitude: $scope.venue.tx_latitude,
					longitude: $scope.venue.tx_longitude
				},
				zoom: 17
			};

			$scope.marker = {
				id: 0,
				options: {
					draggable: true
				},
				coords: {
					latitude: $scope.map.center.latitude,
					longitude: $scope.map.center.longitude
				},
				events: {
					dragend: function (marker, eventName, args) {

						$scope.formdata.latitude = marker.getPosition().lat();
						$scope.formdata.longitude = marker.getPosition().lng();

					}
				}
			};

			$scope.searchBox = {
				template: 'dist/views/templates/google.maps.searchbox.html',
				events: {
					places_changed: function (searchbox) {

						var place = searchbox.getPlaces();

						var lat = place[0].geometry.location.lat();
						var lng = place[0].geometry.location.lng();

						$scope.formdata.latitude = lat;
						$scope.formdata.longitude = lng;

						$scope.map.center.latitude = lat;
						$scope.map.center.longitude	= lng;

						$scope.marker.coords.latitude = lat;
						$scope.marker.coords.longitude	= lng;

					}
				}
			};

			$scope.renderMap = true;

		}, 500);

	});

	$scope.editVenue = function () {

		$scope.editingVenue = true;
		$scope.editingVenueAlert = false;

		var data = $.param($scope.formdata);

		Venue.editVenue(id_uniq, data).then(function (res) {

			$scope.editingVenue = false;

			if (res.data.status == "success") {

				STFcache.delete('venues' + id_uniq);
				STFcache.delete('venues');

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Venue updated successfully'
				};

				$location.path('venues');

			} else {

				$scope.editingVenueAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.editingVenue = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingVenueAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};
	
}]);