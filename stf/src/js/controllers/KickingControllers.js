var stf = angular.module('KickingControllers', []);

stf.controller('KickingListController', ['$scope', '$routeParams', '$location', 'Event', 'Kicking', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, $location, Event, Kicking, Access, Auth, STFcache) {
	
   $scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};
	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;

				if (_.findIndex($scope.event.configurations, { station: 'Kicking' }) >= 0) {

 					$scope.$emit('eventLoaded');

				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getKickings = function (id_uniq, refresh) {
       
		if (refresh !== undefined && refresh == true) STFcache.delete('kickingEvent' + $scope.id_uniq);
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.measurements = null;
		$scope.loadingKickings = true;
		$scope.loadingKickingsAlert = false;

		Kicking.getAll(id_uniq).then(function (res) {

			$scope.loadingKickings = false;

			if (angular.isObject(res.data.response)) {
				$scope.measurements = res.data.response;
				console.log($scope.measurements);
			} else {
				$scope.loadingKickingsAlert = {
					type: 'success',
					message: res.data.response
				};
			}
			$scope.getAccess(1);
		}, function (err) {

			$scope.loadingKickings = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingKickingsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.deletePlayerEvent = function( player_id, ind ){
		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:player_id,type:'delete'});
			Kicking.delete(data).then(function (res) {
				$scope.loadingKickings = false;
				$scope.getKickings($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingKickingsAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingKickings = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingKickingsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};
	$scope.bulkDeletePlayerEvent = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:values,type:'delete',action:action_id});
			Kicking.multidelete(data).then(function (res) {
				$scope.loadingKickings = false;
				$scope.getKickings($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingKickingsAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingKickings = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingKickingsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one to delete.");
	 }
	};

	$scope.getEvent($scope.id_uniq);
	$scope.getKickings($scope.id_uniq);

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

	/*$scope.orderOptions = [
		{ value: 'name', text: 'Name' },
		{ value: '-date', text: 'Date' }
	];*/
	
}]);

stf.controller('KickingCreateController', ['$scope', '$routeParams', '$location', '$rootScope', '$filter', '$timeout', 'Event', 'Player', 'Kicking', 'Auth', 'STFcache', function ($scope, $routeParams, $location, $rootScope, $filter, $timeout, Event, Player, Kicking, Auth, STFcache) {
	
	$scope.id_uniq = $routeParams.id_uniq;
	$scope.player = null;
	$scope.ball_weight = 0;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	$scope.legs = [
		{ value: 1, name: 'Right' },
		{ value: 2, name: 'Left' }
	];

	/* AIM TEST CODE */
    var measurement = {
		"MKS" : {
			"speed" : "m/s",
			"distance" : "mts"
		},
		"YLS" : {
			"speed" : "y/s",
			"distance" : "ft"
		},
		"default" : {
			"speed" : "m/s",
			"distance" : "mts"
		}
	};
  
  $scope.getHeadingMeasurement = function( type, dim ){
        if(type=== undefined){
        	type="default";
        	
        	return measurement[type][dim];
        }
		return measurement[type][dim];
	};

	$scope.aimTest = false;

	$scope.firstAimLeg = 'First';
	$scope.secondAimLeg = 'Second';

	$scope.firstGroupFieldOneValidated = false;
	$scope.firstGroupFieldTwoValidated = false;
	$scope.firstGroupFieldThreeValidated = false;

	$scope.secondGroupFieldOneValidated = false;
	$scope.secondGroupFieldTwoValidated = false;
	$scope.secondGroupFieldThreeValidated = false;

	$scope.aimdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		initial_leg: '',
		right_leg: {},
		left_leg: {}
	
	};

	$scope.firstGroupData = {
		request_one: 0,
		destination_one: '',
		request_two: 0,
		destination_two: '',
		request_three: 0,
		destination_three: ''
	};

	$scope.secondGroupData = {
		request_one: 0,
		destination_one: '',
		request_two: 0,
		destination_two: '',
		request_three: 0,
		destination_three: ''
	};

	$scope.startAimTest = function () { 
           //only take number
           
         
          	angular.element('.onlynumbertext').on("keypress",function (evt) {
    	    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;

			});
		return $scope.aimTest = true; };
	    $scope.$watch('aimdata.initial_leg', function (newVal, oldVal) {

		switch (newVal) {

			case 1:
				$scope.firstGroupData.request_one = Math.floor((Math.random() * 24) + 1);
				$scope.firstAimLeg = 'Right';
				$scope.secondAimLeg = 'Left';
				break;
			case 2:
				$scope.firstGroupData.request_one = Math.floor((Math.random() * 24) + 1);
				$scope.firstAimLeg = 'Left';
				$scope.secondAimLeg = 'Right';
				break;
			default:
				$scope.firstGroupData.request_one = 0;
				$scope.firstAimLeg = 'First';
				$scope.secondAimLeg = 'Second';
				break;

		}

	});

	$scope.validateFirstGroupField = function (field) {

		switch (field) {

			case 1: 
				$scope.firstGroupFieldOneValidated = true;
				$scope.firstGroupData.request_two = Math.floor((Math.random() * 24) + 1);	
				break;
			case 2:
				$scope.firstGroupFieldTwoValidated = true;
				$scope.firstGroupData.request_three = Math.floor((Math.random() * 24) + 1);
				break;
			case 3:
				$scope.firstGroupFieldThreeValidated = true;
				$scope.firstGroupFormCompleted = true;
				$scope.secondGroupData.request_one = Math.floor((Math.random() * 24) + 1);
				break;
			default: 
				break;	

		}

	};

	$scope.validateSecondGroupField = function (field) {

		switch (field) {

			case 1: 
				$scope.secondGroupFieldOneValidated = true;
				$scope.secondGroupData.request_two = Math.floor((Math.random() * 24) + 1);	
				break;
			case 2:
				$scope.secondGroupFieldTwoValidated = true;
				$scope.secondGroupData.request_three = Math.floor((Math.random() * 24) + 1);
				break;
			case 3:
				$scope.secondGroupFieldThreeValidated = true;
				$scope.secondGroupFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	/* END AIM TEST CODE */



	/* FORCE TEST CODE */

	$scope.forceTest = false;

	$scope.firstForceLeg = 'First';
	$scope.secondForceLeg = 'Second';

	$scope.forcedata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		initial_leg: '',
		right_leg: {},
		left_leg: {}
	};

	$scope.firstGroupForceData = {
		attempt_one: '',
		attempt_two: '',
		attempt_three: '',
		result_one: '',
		result_two: '',
		result_three: ''
	};

	$scope.secondGroupForceData = {
		attempt_one: '',
		attempt_two: '',
		attempt_three: '',
		result_one: '',
		result_two: '',
		result_three: ''
	};

	$scope.startForceTest = function () { 
             //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
           if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length >1){ 
	             
	                this.value = parseFloat($(this).val().slice(0, -1));
	            }  
	         	}    
            	var charCode = (evt.which) ? evt.which : event.keyCode;
				if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 	return true;

         });  
		return $scope.forceTest = true; };
	$scope.$watch('forcedata.initial_leg', function (newVal, oldVal) {

		switch (newVal) {

			case 1:
				$scope.firstForceLeg = 'Right';
				$scope.secondForceLeg = 'Left';
				break;
			case 2:
				$scope.firstForceLeg = 'Left';
				$scope.secondForceLeg = 'Right';
				break;
			default:
				$scope.firstForceLeg = 'First';
				$scope.secondForceLeg = 'Second';
				break;

		}

	});

	$scope.validateFirstGroupForceField = function (field) {

		switch (field) {

			case 1: 
				$scope.firstGroupForceFieldOneValidated = true;
				$scope.firstGroupForceData.result_one = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_one)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_one)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 2:
				$scope.firstGroupForceFieldTwoValidated = true;
				$scope.firstGroupForceData.result_two = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_two)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_two)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 3:
				$scope.firstGroupForceFieldThreeValidated = true;
				$scope.firstGroupForceData.result_three = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_three)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_three)) / 0.05) / 0.22481).toFixed(2);
				$scope.firstGroupForceFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	$scope.validateSecondGroupForceField = function (field) {

		switch (field) {

			case 1: 
				$scope.secondGroupForceFieldOneValidated = true;
				$scope.secondGroupForceData.result_one = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_one)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_one)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 2:
				$scope.secondGroupForceFieldTwoValidated = true;
				$scope.secondGroupForceData.result_two = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_two)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_two)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 3:
				$scope.secondGroupForceFieldThreeValidated = true;
				$scope.secondGroupForceData.result_three = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_three)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_three)) / 0.05) / 0.22481).toFixed(2);
				$scope.secondGroupForceFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	/* END FORCE TEST CODE */

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;
				$scope.measurement_system = ($scope.event.stf_measurement_abbr != "MKS") ? "YLS" : "MKS";
                
                   //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
           if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length >1){ 
	             
	                this.value = parseFloat($(this).val().slice(0, -1));
	            }  
	         	}    
            	var charCode = (evt.which) ? evt.which : event.keyCode;
				if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 	return true;

         });  

				if (_.findIndex($scope.event.configurations, { station: 'Kicking' }) >= 0) {

 					$scope.aimdata.id_event = $scope.event.id;
 					$scope.forcedata.id_event = $scope.event.id;

 					$scope.eventHasKickingAim = (_.findIndex($scope.event.configurations, { station: 'Kicking', test: 'Aim' }) >= 0) ? true : false;
 					$scope.eventHasKickingForce = (_.findIndex($scope.event.configurations, { station: 'Kicking', test: 'Power' }) >= 0) ? true : false;
                   
                    // $scope.getPlayer($scope.player_id_uniq);
 				} else {	

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {
       
		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			if (angular.isObject(res.data.response)) {

				$scope.aimdata.id_player = res.data.response.id;
				$scope.forcedata.id_player = res.data.response.id;

				return res.data.response;
                

			} else {

				$scope.loadingPlayer = false;

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		}).then(function (args) {

			if (args !== undefined) {

				var player = args;
				var data = [];
				var forceAgeRangeData = [];
				var aimAgeRangeData = [];

				for (var i = 0; i < $scope.event.configurations.length; i++) {
					
					if ($scope.event.configurations[i].station == "Kicking") {

						if (_.findIndex(data, { value: $scope.event.configurations[i].test }) == -1) {

							data.push({
								name: 'tests[]',
								value: $scope.event.configurations[i].test
							});

						};

						if ($scope.event.configurations[i].test == "Power") {

							forceAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	

						};

						if ($scope.event.configurations[i].test == "Aim") {

							aimAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	

						};

					};

				};

				data = $.param(data);
								
				Kicking.check(player.id, $scope.event.id, data).then(function (res) {

					$scope.loadingPlayer = false;
                   

					if (res.data.player_exists == 1) {

						$scope.loadingPlayerAlert = {
							type: 'success',
							message: res.data.response
						};

					} else {

						var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));

						$scope.playerCanPerformAimTest = false;
						$scope.playerCanPerformForceTest = false;

						if (aimAgeRangeData.length > 0) {

							for (var i = 0; i < aimAgeRangeData.length; i++) {

								var min = parseInt(aimAgeRangeData[i].min);
								var max = parseInt(aimAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformAimTest = true;
									break;

								};

							};

						};
                       
						if (forceAgeRangeData.length > 0) {

							for (var i = 0; i < forceAgeRangeData.length; i++) {

								var min = parseInt(forceAgeRangeData[i].min);
								var max = parseInt(forceAgeRangeData[i].max);
                               
								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformForceTest = true;
									$scope.ball_weight = parseInt(forceAgeRangeData[i].ball_weight);
									break;

								};

							};

						};

						$scope.player = player;

						$scope.AimTestAlert = ($scope.playerCanPerformAimTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
						$scope.ForceTestAlert = ($scope.playerCanPerformForceTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
                       // console.log(res.data.response);
						$scope.showAim = (res.data.response.aim == 2) ? true : false;
						$scope.showForce = (res.data.response.force == 2) ? true : false;

					}

				}, function (err) {

					$scope.loadingPlayer = false;

					$scope.loadingPlayerAlert = {
						type: 'danger',
						message: res.data.response
					};

				});

			};

		});

	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$scope.aimdata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			right_leg: {},
			left_left: {}
		};

		$scope.rightLegAimData = {
			request_one: 0,
			destination_one: '',
			request_two: 0,
			destination_two: '',
			request_three: 0,
			destination_three: ''
		};

		$scope.leftLegAimData = {
			request_one: 0,
			destination_one: '',
			request_two: 0,
			destination_two: '',
			request_three: 0,
			destination_three: ''
		};

		$scope.forcedata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			initial_leg: '',
			right_leg: {},
			left_leg: {}
		};

		$scope.firstGroupForceData = {
			attempt_one: '',
			attempt_two: '',
			attempt_three: '',
			result_one: '',
			result_two: '',
			result_three: ''
		};

		$scope.secondGroupForceData = {
			attempt_one: '',
			attempt_two: '',
			attempt_three: '',
			result_one: '',
			result_two: '',
			result_three: ''
		};
		
		$scope.CreateKickingAimForm.$setPristine();
		$scope.rightLegAimForm.$setPristine();
		$scope.leftLegAimForm.$setPristine();

		$scope.CreateKickingForceForm.$setPristine();
		$scope.firstGroupForceForm.$setPristine();
		$scope.secondGroupForceForm$setPristine();
	};

	$scope.getEvent($scope.id_uniq);

	$scope.createKickingAim = function () {

		switch ($scope.aimdata.initial_leg) {

			case 1:
				$scope.aimdata.right_leg = $scope.firstGroupData;
				$scope.aimdata.left_leg = $scope.secondGroupData;
				break;
			case 2:
				$scope.aimdata.right_leg = $scope.secondGroupData;
				$scope.aimdata.left_leg = $scope.firstGroupData;
				break;
			default:
				return;

		}

		$scope.creatingKickingAim = true;
		$scope.creatingKickingAimAlert = false;

		var data = $.param($scope.aimdata);

		Kicking.createAim(data).then(function (res) {

			$scope.creatingKickingAim = false;

			if (res.data.status == "success") {

				STFcache.delete('kickingEvent' + $scope.id_uniq);
                

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/kicking');

			} else {

				$scope.creatingKickingAimAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.creatingKickingAim = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingKickingAimAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.createKickingForce= function () {

		switch ($scope.forcedata.initial_leg) {

			case 1:
				$scope.forcedata.right_leg = $scope.firstGroupForceData;
				$scope.forcedata.left_leg = $scope.secondGroupForceData;
				break;
			case 2:
				$scope.forcedata.right_leg = $scope.secondGroupForceData;
				$scope.forcedata.left_leg = $scope.firstGroupForceData;
				break;
			default:
				return;

		}

		$scope.creatingKickingForce = true;
		$scope.creatingKickingForceAlert = false;

		var data = $.param($scope.forcedata);

		Kicking.createForce(data).then(function (res) {

			$scope.creatingKickingForce = false;

			if (res.data.status == "success") {

				STFcache.delete('kickingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/kicking');

			} else {

				$scope.creatingKickingForceAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.creatingKickingForce = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingKickingForceAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};
	
}]);

stf.controller('KickingEditController',  ['$scope', '$routeParams', '$location', '$rootScope', '$filter', '$timeout', 'Event', 'Player', 'Kicking', 'Auth', 'STFcache', function ($scope, $routeParams, $location, $rootScope, $filter, $timeout, Event, Player, Kicking, Auth, STFcache) {

	// $scope.id_uniq = $routeParams.playerid;
	// $scope.eventid=$routeParams.id;
	$scope.id_uniq = $routeParams.id;
	$scope.player_id_uniq = $routeParams.playerid;
	$scope.player = null;
	$scope.ball_weight = 0;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	$scope.legs = [
		{ value: 1, name: 'Right' },
		{ value: 2, name: 'Left' }
	];

	/* AIM TEST CODE */
    
     var measurement = {
		"MKS" : {
			"speed" : "m/s",
			"distance" : "mts"
		},
		"YLS" : {
			"speed" : "y/s",
			"distance" : "ft"
		},
		"default" : {
			"speed" : "m/s",
			"distance" : "mts"
		}
	};
  
  $scope.getHeadingMeasurement = function( type, dim ){
        if(type=== undefined){
        	type="default";
        	
        	return measurement[type][dim];
        }
		return measurement[type][dim];
	};
    

	$scope.aimTest = false;

	$scope.firstAimLeg = 'First';
	$scope.secondAimLeg = 'Second';

	$scope.firstGroupFieldOneValidated = false;
	$scope.firstGroupFieldTwoValidated = false;
	$scope.firstGroupFieldThreeValidated = false;

	$scope.secondGroupFieldOneValidated = false;
	$scope.secondGroupFieldTwoValidated = false;
	$scope.secondGroupFieldThreeValidated = false;

	$scope.aimdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		initial_leg: '',
		right_leg: {},
		left_leg: {}
	
	};

	$scope.firstGroupData = {
		request_one: 0,
		destination_one: '',
		request_two: 0,
		destination_two: '',
		request_three: 0,
		destination_three: ''
	};

	$scope.secondGroupData = {
		request_one: 0,
		destination_one: '',
		request_two: 0,
		destination_two: '',
		request_three: 0,
		destination_three: ''
	};

	$scope.startAimTest = function () { 
        //only take number

        angular.element('.onlynumbertext').on("keypress",function (evt) {
    	    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;

		});
		return $scope.aimTest = true; 

	};

	$scope.$watch('aimdata.initial_leg', function (newVal, oldVal) {
		switch (newVal) {
			case 1:
				$scope.firstGroupData.request_one = Math.floor((Math.random() * 24) + 1);
				$scope.firstAimLeg = 'Right';
				$scope.secondAimLeg = 'Left';
				break;
			case 2:
				$scope.firstGroupData.request_one = Math.floor((Math.random() * 24) + 1);
				$scope.firstAimLeg = 'Left';
				$scope.secondAimLeg = 'Right';
				break;
			default:
				//$scope.firstGroupData.request_one = 0;
				$scope.firstAimLeg = 'First';
				$scope.secondAimLeg = 'Second';
				break;
		}
	});

	$scope.validateFirstGroupField = function (field) {
		switch (field) {
			case 1: 
				$scope.firstGroupFieldOneValidated = true;
				$scope.firstGroupData.request_two = Math.floor((Math.random() * 24) + 1);	
				break;
			case 2:
				$scope.firstGroupFieldTwoValidated = true;
				$scope.firstGroupData.request_three = Math.floor((Math.random() * 24) + 1);
				break;
			case 3:
				$scope.firstGroupFieldThreeValidated = true;
				$scope.firstGroupFormCompleted = true;
				$scope.secondGroupData.request_one = Math.floor((Math.random() * 24) + 1);
				break;
			default: 
				break;	
		}

	};

	$scope.validateSecondGroupField = function (field) {

		switch (field) {

			case 1: 
				$scope.secondGroupFieldOneValidated = true;
				$scope.secondGroupData.request_two = Math.floor((Math.random() * 24) + 1);	
				break;
			case 2:
				$scope.secondGroupFieldTwoValidated = true;
				$scope.secondGroupData.request_three = Math.floor((Math.random() * 24) + 1);
				break;
			case 3:
				$scope.secondGroupFieldThreeValidated = true;
				$scope.secondGroupFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	/* END AIM TEST CODE */

	/* FORCE TEST CODE */

	$scope.forceTest = false;

	$scope.firstForceLeg = 'First';
	$scope.secondForceLeg = 'Second';

	$scope.forcedata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		initial_leg: '',
		right_leg: {},
		left_leg: {}
	};

	$scope.firstGroupForceData = {
		attempt_one: '',
		attempt_two: '',
		attempt_three: '',
		result_one: '',
		result_two: '',
		result_three: ''
	};

	$scope.secondGroupForceData = {
		attempt_one: '',
		attempt_two: '',
		attempt_three: '',
		result_one: '',
		result_two: '',
		result_three: ''
	};

	$scope.startForceTest = function () {
   angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
           if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length >1){ 
	             
	                this.value = parseFloat($(this).val().slice(0, -1));
	            }  
	         	}    
            	var charCode = (evt.which) ? evt.which : event.keyCode;
				if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 	return true;
         }); 
	 return $scope.forceTest = true; };
	$scope.$watch('forcedata.initial_leg', function (newVal, oldVal) {

		switch (newVal) {

			case 1:
				$scope.firstForceLeg = 'Right';
				$scope.secondForceLeg = 'Left';
				break;
			case 2:
				$scope.firstForceLeg = 'Left';
				$scope.secondForceLeg = 'Right';
				break;
			default:
				$scope.firstForceLeg = 'First';
				$scope.secondForceLeg = 'Second';
				break;

		}

	});

	$scope.validateFirstGroupForceField = function (field) {

		switch (field) {

			case 1: 
				$scope.firstGroupForceFieldOneValidated = true;
				$scope.firstGroupForceData.result_one = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_one)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_one)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 2:
				$scope.firstGroupForceFieldTwoValidated = true;
				$scope.firstGroupForceData.result_two = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_two)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_two)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 3:
				$scope.firstGroupForceFieldThreeValidated = true;
				$scope.firstGroupForceData.result_three = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_three)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_three)) / 0.05) / 0.22481).toFixed(2);
				$scope.firstGroupForceFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	$scope.validateSecondGroupForceField = function (field) {

		switch (field) {

			case 1: 
				$scope.secondGroupForceFieldOneValidated = true;
				$scope.secondGroupForceData.result_one = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_one)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_one)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 2:
				$scope.secondGroupForceFieldTwoValidated = true;
				$scope.secondGroupForceData.result_two = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_two)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_two)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 3:
				$scope.secondGroupForceFieldThreeValidated = true;
				$scope.secondGroupForceData.result_three = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_three)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_three)) / 0.05) / 0.22481).toFixed(2);
				$scope.secondGroupForceFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	/* END FORCE TEST CODE */

	$scope.getEvent = function (id_uniq) {
       
		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;
				$scope.measurement_system = ($scope.event.stf_measurement_abbr != "MKS") ? "YLS" : "MKS";
                
                   //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
           if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length >1){ 
	             
	                this.value = parseFloat($(this).val().slice(0, -1));
	            }  
	         	}    
            	var charCode = (evt.which) ? evt.which : event.keyCode;
				if(charCode==46)return true;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 	return true;
         });  

				if (_.findIndex($scope.event.configurations, { station: 'Kicking' }) >= 0) {

 					$scope.aimdata.id_event = $scope.event.id;
 					$scope.forcedata.id_event = $scope.event.id;

 					$scope.eventHasKickingAim = (_.findIndex($scope.event.configurations, { station: 'Kicking', test: 'Aim' }) >= 0) ? true : false;
 					$scope.eventHasKickingForce = (_.findIndex($scope.event.configurations, { station: 'Kicking', test: 'Power' }) >= 0) ? true : false;
                    console.log("eventHasKickingForce",$scope.eventHasKickingForce);
                    $scope.getPlayer($scope.player_id_uniq);
 				} else {	

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {
		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {
			if (angular.isObject(res.data.response)) {
				$scope.aimdata.id_player = res.data.response.id;
				$scope.forcedata.id_player = res.data.response.id;
				return res.data.response;
			} else {
				$scope.loadingPlayer = false;
				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};
			}
		}, function (err) {
			$scope.loadingPlayer = false;
			if (err.status == 401) {
				Auth.deleteToken();
			} else {
				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};
			}
		}).then(function (args) {

			if (args !== undefined) {
				var player = args;
				var data = [];
				var forceAgeRangeData = [];
				var aimAgeRangeData = [];

				for (var i = 0; i < $scope.event.configurations.length; i++) {
					if ($scope.event.configurations[i].station == "Kicking") {
						if (_.findIndex(data, { value: $scope.event.configurations[i].test }) == -1) {

							data.push({
								name: 'tests[]',
								value: $scope.event.configurations[i].test
							});
						};

						if ($scope.event.configurations[i].test == "Power") {
							forceAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	
						};

						if ($scope.event.configurations[i].test == "Aim") {
							aimAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	
						};
					};
				};

				data = $.param(data);
				var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));

				$scope.playerCanPerformAimTest = false;
				$scope.playerCanPerformForceTest = false;

				if (aimAgeRangeData.length > 0) {

					for (var i = 0; i < aimAgeRangeData.length; i++) {

						var min = parseInt(aimAgeRangeData[i].min);
						var max = parseInt(aimAgeRangeData[i].max);

						if (playerAge >= min && playerAge <= max) {

							$scope.playerCanPerformAimTest = true;
							break;

						};

					};

				};
         //  console.log(forceAgeRangeData);
				if (forceAgeRangeData.length > 0) {

					for (var i = 0; i < forceAgeRangeData.length; i++) {

						var min = parseInt(forceAgeRangeData[i].min);
						var max = parseInt(forceAgeRangeData[i].max);

						if (playerAge >= min && playerAge <= max) {

							$scope.playerCanPerformForceTest = true;
							$scope.ball_weight = parseInt(forceAgeRangeData[i].ball_weight);
							break;

						};

					};

				};


				$scope.player = player;
               
               	Kicking.getDetails($scope.id_uniq,$scope.player_id_uniq).then(function (res) {
					$scope.loadingEvent = false;
					if (angular.isObject(res.data.response)) {
                        console.log(res.data.response);
						//if( $scope.event ){
                        //added by sg//
                        if(res.data.response.aimStat==1){
                        	//document.getElementsByName('initial_leg').disabled = false;

                       // $scope.aimdata.initial_leg="number:1";
                        $scope.aimdata.id_player = res.data.response.aimPlayerID;
						$scope.aimdata.id_event = res.data.response.aimEvtID;
						$scope.aimdata.measurement_date = res.data.response.aimMeasurementDt;
						$scope.firstGroupData.request_one=parseFloat(res.data.response.aimRightReqOne);
						$scope.firstGroupData.request_two=parseFloat(res.data.response.aimRightReqTwo);
						$scope.firstGroupData.request_three=parseFloat(res.data.response.aimRightReqThree);
						$scope.firstGroupData.destination_one=parseFloat(res.data.response.aimRightDestOne);
                        $scope.firstGroupData.destination_two=parseFloat(res.data.response.aimRightDestTwo);
                        $scope.firstGroupData.destination_three=parseFloat(res.data.response.aimRightDestThree);
                        
                        $scope.secondGroupData.request_one=parseFloat(res.data.response.aimLeftReqOne);
						$scope.secondGroupData.request_two=parseFloat(res.data.response.aimLeftReqTwo);
						$scope.secondGroupData.request_three=parseFloat(res.data.response.aimLeftReqThree);
						$scope.secondGroupData.destination_one=parseFloat(res.data.response.aimLeftDestOne);
                        $scope.secondGroupData.destination_two=parseFloat(res.data.response.aimLeftDestTwo);
                        $scope.secondGroupData.destination_three=parseFloat(res.data.response.aimLeftDestThree);
						console.log($scope.firstGroupData.request_one);
						}
						else{
							$scope.aimdata.measurement_date=$filter('date')(new Date(), 'MM/dd/yyyy');
						}
                        
      //                   if(res.data.response.jumpStat==1){
						// $scope.jumpdata.id_player = res.data.response.jumpPlayerID;
						// $scope.jumpdata.id_event = res.data.response.jumpEvtID;
						// $scope.jumpdata.measurement_date = res.data.response.jumpMeasurementDt;
						// $scope.jumpdata.extended_arm_height=parseFloat(res.data.response.jumpArmHeight);
						// $scope.jumpdata.jump_high_one=parseFloat(res.data.response.jumpHighOne);
						// $scope.jumpdata.jump_high_two=parseFloat(res.data.response.jumpHighTwo);
						// $scope.jumpdata.jump_high_three=parseFloat(res.data.response.jumpHighThree);
						// }
						// else{
						// 	$scope.jumpdata.measurement_date=$filter('date')(new Date(), 'MM/dd/yyyy');
						// }
						//added by sg end//
						//added by sg
						if(res.data.response.forceStat==1){
                  	$scope.showForce=true;


                  	  $scope.forcedata.id_player = res.data.response.forcePlayerID;
						$scope.forcedata.id_event = res.data.response.forceEvtID;
						$scope.forcedata.measurement_date = res.data.response.forceMeasurementDt;
						//$scope.forcedata.request_one=parseFloat(res.data.response.aimRightReqOne);
						$scope.firstGroupForceData.attempt_one=parseFloat(res.data.response.forceRightAttemptOne);
						$scope.firstGroupForceData.attempt_two=parseFloat(res.data.response.forceRightAttemptTwo);
						$scope.firstGroupForceData.attempt_three=parseFloat(res.data.response.forceRightAttemptThree);
                        //$scope.firstGroupData.destination_two=parseFloat(res.data.response.aimRightDestTwo);
                        //$scope.firstGroupData.destination_three=parseFloat(res.data.response.aimRightDestThree);
                        
                        $scope.secondGroupForceData.attempt_one=parseFloat(res.data.response.forceLeftAttemptOne);
						$scope.secondGroupForceData.attempt_two=parseFloat(res.data.response.forceLeftAttemptTwo);
						$scope.secondGroupForceData.attempt_three=parseFloat(res.data.response.forceLeftAttemptThree);
						// $scope.secondGroupData.destination_one=parseFloat(res.data.response.aimLeftDestOne);
      //                   $scope.secondGroupData.destination_two=parseFloat(res.data.response.aimLeftDestTwo);
      //                   $scope.secondGroupData.destination_three=parseFloat(res.data.response.aimLeftDestThree);
                  }
                  else{
                  	$scope.showForce=true;
							$scope.forcedata.measurement_date=$filter('date')(new Date(), 'MM/dd/yyyy');
						 }
						//$scope.forcedata.result[2] = res.data.response.forceAttemptOne;

						// $scope.headForcedata = {
						// 	id_player: res.data.response.forcePlayerID,
						// 	id_event: res.data.response.forceEvtID,
						// 	measurement_date: res.data.response.forceMeasurementDt,
						// 	distance_one: parseFloat(res.data.response.forceDisOne),
						// 	distance_two: parseFloat(res.data.response.forceDisTwo),
						// 	distance_three: parseFloat(res.data.response.forceDisThree),
						// 	attempt_one : parseFloat(res.data.response.forceAttemptOne),
						// 	attempt_two : parseFloat(res.data.response.forceAttemptTwo),
						// 	attempt_three : parseFloat(res.data.response.forceAttemptThree),
						// 	result_one : parseFloat($scope.calculateForce(0)),
						// 	result_two : parseFloat($scope.calculateForce(1)),
						// 	result_three : parseFloat($scope.calculateForce(2))
						// };

                  
						$scope.player = player;
				$scope.AimTestAlert = ($scope.playerCanPerformAimTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
				$scope.ForceTestAlert = ($scope.playerCanPerformForceTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
              // console.log("hh",res.data.response);
				$scope.showAim = (res.data.response.aim == 2) ? true : false;
				//$scope.showForce = (res.data.response.force == 2) ? true : false;
			console.log("showforce",$scope.showForce);
					} else {
						$scope.loadingEventAlert = {
							type: 'success',
							message: res.data.response
						};
					}

				}, function (err) {
					$scope.loadingEvent = false;

					if (err.status == 401) {

						Auth.deleteToken();

					} else {

						$scope.loadingEventAlert = {
							type: 'danger',
							message: err.status + ' ' + err.statusText
						};

					}

				});

                $scope.player = player;
				$scope.AimTestAlert = ($scope.playerCanPerformAimTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
				$scope.ForceTestAlert = ($scope.playerCanPerformForceTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
              // console.log("hh",res.data.response);
				$scope.showAim = (res.data.response.aim == 2) ? true : false;
				$scope.showForce = (res.data.response.force == 2) ? true : false;
			console.log("showforce",$scope.showForce);
			};
		});
	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$scope.aimdata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			right_leg: {},
			left_left: {}
		};

		$scope.rightLegAimData = {
			request_one: 0,
			destination_one: '',
			request_two: 0,
			destination_two: '',
			request_three: 0,
			destination_three: ''
		};

		$scope.leftLegAimData = {
			request_one: 0,
			destination_one: '',
			request_two: 0,
			destination_two: '',
			request_three: 0,
			destination_three: ''
		};

		$scope.forcedata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			initial_leg: '',
			right_leg: {},
			left_leg: {}
		};

		$scope.firstGroupForceData = {
			attempt_one: '',
			attempt_two: '',
			attempt_three: '',
			result_one: '',
			result_two: '',
			result_three: ''
		};

		$scope.secondGroupForceData = {
			attempt_one: '',
			attempt_two: '',
			attempt_three: '',
			result_one: '',
			result_two: '',
			result_three: ''
		};
		
		$scope.CreateKickingAimForm.$setPristine();
		$scope.rightLegAimForm.$setPristine();
		$scope.leftLegAimForm.$setPristine();

		$scope.CreateKickingForceForm.$setPristine();
		$scope.firstGroupForceForm.$setPristine();
		$scope.secondGroupForceForm$setPristine();
	};

	$scope.getEvent($scope.id_uniq);
    
	$scope.editKickingAim = function () {

		switch ($scope.aimdata.initial_leg) {

			case 1:
				$scope.aimdata.right_leg = $scope.firstGroupData;
				$scope.aimdata.left_leg = $scope.secondGroupData;
				break;
			case 2:
				$scope.aimdata.right_leg = $scope.secondGroupData;
				$scope.aimdata.left_leg = $scope.firstGroupData;
				break;
			default:
				return;

		}

		$scope.creatingKickingAim = true;
		$scope.creatingKickingAimAlert = false;

		var data = $.param($scope.aimdata);

		Kicking.updateAim(data).then(function (res) {

			$scope.creatingKickingAim = false;

			if (res.data.status == "success") {

				STFcache.delete('kickingEvent' + $scope.id_uniq);
                

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/kicking');

			} else {

				$scope.creatingKickingAimAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.creatingKickingAim = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingKickingAimAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.editKickingForce= function () {

		switch ($scope.forcedata.initial_leg) {

			case 1:
				$scope.forcedata.right_leg = $scope.firstGroupForceData;
				$scope.forcedata.left_leg = $scope.secondGroupForceData;
				break;
			case 2:
				$scope.forcedata.right_leg = $scope.secondGroupForceData;
				$scope.forcedata.left_leg = $scope.firstGroupForceData;
				break;
			default:
				return;

		}

		$scope.creatingKickingForce = true;
		$scope.creatingKickingForceAlert = false;

		var data = $.param($scope.forcedata);

		Kicking.updateForce(data).then(function (res) {

			$scope.creatingKickingForce = false;

			if (res.data.status == "success") {

				STFcache.delete('kickingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/kicking');

			} else {

				$scope.creatingKickingForceAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.creatingKickingForce = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingKickingForceAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};
			
}]);
