var stf = angular.module('MainFilters', []);

stf.filter('zipcodeMask', function () {

	return function (input) {

		var count = input.length;
		var result = '';

		for (var i = 0; i < count; i++) {
			
			if (input[i] == 'N') result += '9'
			else result += '';

		};

		return result;

	}

});

stf.filter('dateToAngular', function () {

	return function (input) {

		input = input || '';

		return input.replace(' ', 'T');

	}

});

stf.filter('currentAge', ['$filter', function ($filter) {

	return function (input) {

		var ageDifMs = Date.now() - new Date($filter('date')(input, 'yyyy/MM/dd'));
		var ageDate = new Date(ageDifMs);
		
		return Math.abs(ageDate.getUTCFullYear() - 1970);	

	}

}]);