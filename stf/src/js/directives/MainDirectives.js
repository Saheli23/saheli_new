var stf = angular.module('MainDirectives', []);

/*stf.directive('readImage', ['$timeout', function ($timeout) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			$(iElement).on('change', function () {

				scope.formdata.userfile = '';

				var preview = $('#img-preview');
				var file = $(iElement)[0].files[0];
				var reader = new FileReader();
				var types = ['image/png', 'image/jpeg'];

				preview.cropper('destroy');

				if (file) {

					if (_.indexOf(types, file.type) != -1) {

						scope.invalidFile = false;

						reader.onloadend = function () {

							preview.attr('src', reader.result);

						}
						reader.readAsDataURL(file);

						$timeout(function () {

							scope.$emit('imgReady');

						}, 500);

					} else {

						scope.invalidFile = true;
						preview.attr('src', '');

					}

				} else {

					preview.attr('src', '');

				}

				scope.$apply();

			});

		}
	};
}]);*/

stf.directive('removeChar', function() {
  return {
    require: 'ngModel',
    link: function (scope, element, attr, ngModelCtrl) {
      function fromUser(text) {
        var transformedInput = text.replace(/[^0-9]/g, '');
        if(transformedInput !== text) {
            ngModelCtrl.$setViewValue(transformedInput);
            ngModelCtrl.$render();
        }
        return transformedInput;  // or return Number(transformedInput)
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  }; 
});

stf.directive('cropper', [function () {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			scope.$on('imgReady', function () {

				$(iElement).cropper({
					aspectRatio: 1,
					dragCrop: true,
					cropBoxMovable: true,
					cropBoxResizable: true,
					minCropBoxWidth: 320,
					minCropBoxHeight: 320
				}).on('built.cropper', function () {

					var croppedData = $(iElement).cropper('getData');

					scope.formdata.xaxis = parseInt(croppedData.x);
					scope.formdata.yaxis = parseInt(croppedData.y);
					scope.formdata.width = parseInt(croppedData.width);
					scope.formdata.height = parseInt(croppedData.height);

				}).on('cropend.cropper', function (e) {

					var croppedData = $(iElement).cropper('getData');
					
					scope.formdata.xaxis = parseInt(croppedData.x);
					scope.formdata.yaxis = parseInt(croppedData.y);
					scope.formdata.width = parseInt(croppedData.width);
					scope.formdata.height = parseInt(croppedData.height);

				}).on('zoom.cropper', function (e) {

					var croppedData = $(iElement).cropper('getData');
					
					scope.formdata.xaxis = parseInt(croppedData.x);
					scope.formdata.yaxis = parseInt(croppedData.y);
					scope.formdata.width = parseInt(croppedData.width);
					scope.formdata.height = parseInt(croppedData.height);

				});

			});

		}
	};
}]);

stf.directive('fileModel', ['$parse', function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			var model = $parse(iAttrs.fileModel);
			var modelSetter = model.assign;

			iElement.bind('change', function () {

				scope.$apply(function () {

					modelSetter(scope, iElement[0].files[0]);

				});

			});

		}
	};
}]);

stf.directive('topbarMenu', [function () {
	return {
		templateUrl: 'dist/views/templates/topbar.menu.html',
		restrict: 'E',
		controller: 'topbarMenuController'
	};
}])

stf.directive('stfFooter', [function () {
	return {
		templateUrl: 'dist/views/templates/footer.html',
		replace: true,
		restrict: 'E'
	};
}])