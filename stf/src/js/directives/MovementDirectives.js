var stf = angular.module('MovementDirectives', []);

stf.directive('movementLinear', ['$interval', '$timeout', '$document', function ($interval, $timeout, $document) {
	return {
		templateUrl: 'dist/views/templates/movement.linear.html',
		restrict: 'E',
		scope: {
			activetab: '=',
			playerid: '=',
			eventid: '='
		},
		controller: function ($scope, $interval, $filter, $routeParams, $rootScope, $location, Movement, Auth, STFcache) {

			var id_uniq = $routeParams.id_uniq;

			$scope.init = function () {

				console.log("Init Started!!");
                
				$scope.interval = false;
				$scope.stopwatch = new Date(Date.UTC(99, 11, 1, 0, 0, 0));
				$scope.totaltime = 0;
				$scope.attempts = 0;

				$scope.timerCompleted = false;
				$scope.lineardata = {
					id_player: $scope.playerid,
					id_event: $scope.eventid,
					measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
					checkpoints: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				};

			};

			$scope.resetTimer = function () {
              	console.log("Hello");
				$interval.cancel($scope.interval);
				$scope.init();

			};

			$scope.$on('$destroy', function () { $interval.cancel($scope.interval); });

			$rootScope.$on('movementTestPlayerChanged', function () {

				$scope.resetTimer();

			});

			$scope.init();

			$scope.createMovementLinear = function () {

				$scope.creatingMovementLinear = true;
				$scope.creatingMovementLinearAlert = false;

				var data = $.param($scope.lineardata);

				Movement.createLinear(data).then(function (res) {

					$scope.creatingMovementLinear = false;

					if (res.data.status == "success") {

						STFcache.delete('movementEvent' + id_uniq);

						$rootScope.tooltipMessageAlert = {
							type: 'success',
							message: res.data.response
						};

						$location.path('/events/' + id_uniq + '/movement');

					} else {

						$scope.creatingMovementLinearAlert = {
							type: 'danger',
							message: res.data.response
						};

					}

				}, function (err) {

					$scope.creatingMovementLinear = false;

					if (err.status == 401) {

						Auth.deleteToken();

					} else {

						$scope.creatingMovementLinearAlert = {
							type: 'danger',
							message: err.status + ' ' + err.statusText
						};

					}
				});

			};

		},
		link: function (scope, iElement, iAttrs) {

			$document.on('keypress', function (e) {

				if (e.charCode == 115 && scope.activetab === "Linear") {

					if (scope.interval == false) {

						scope.d = new Date();

						scope.interval = $interval(function () {

							scope.now = new Date();
							scope.ellapsed = scope.now.getTime() - scope.d.getTime();
							scope.stopwatch = scope.ellapsed;

						}, 33);	

					} else {

						if (scope.attempts < 10) {

							if (scope.lineardata.checkpoints.length == 0) {

								scope.lineardata.checkpoints[scope.attempts] = scope.stopwatch;
								
							} else {

								var ellapsedTime = 0;

								angular.forEach(scope.lineardata.checkpoints, function (value, key) {

									ellapsedTime += value;

								});

								scope.lineardata.checkpoints[scope.attempts] = scope.stopwatch - ellapsedTime;
								
							}

							scope.attempts++;

						};

						if (scope.attempts == 10) {

							$interval.cancel(scope.interval);
							scope.totaltime += scope.stopwatch;
							scope.timerCompleted = true;

						};

					}

				};

			});

		}
	};
}]);

stf.directive('movementLateral', ['$interval', '$timeout', '$document', function ($interval, $timeout, $document) {
	return {
		templateUrl: 'dist/views/templates/movement.lateral.html',
		restrict: 'E',
		scope: {
			activetab: '=',
			playerid: '=',
			eventid: '='
		},
		controller: function ($scope, $interval, $filter, $routeParams, $rootScope, $location, Movement, Auth, STFcache) {

			var id_uniq = $routeParams.id_uniq;

			$scope.init = function () {
              
				$scope.interval = false;
				$scope.stopwatch = new Date(Date.UTC(99, 11, 1, 0, 0, 0));
				$scope.totaltime = 0;
				$scope.attempts = 0;

				$scope.timerCompleted = false;
				$scope.lateraldata = {
					id_player: $scope.playerid,
					id_event: $scope.eventid,
					measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
					checkpoints: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				};

			};

			$scope.resetTimer = function () {

				$interval.cancel($scope.interval);
				$scope.init();

			};

			$scope.$on('$destroy', function () { $interval.cancel($scope.interval); });

			$rootScope.$on('movementTestPlayerChanged', function () {

				$scope.resetTimer();

			})

			$scope.init();

			$scope.createMovementLateral = function () {

				$scope.creatingMovementLateral = true;
				$scope.creatingMovementLateralAlert = false;

				var data = $.param($scope.lateraldata);

				Movement.createLateral(data).then(function (res) {

					$scope.creatingMovementLateral = false;

					if (res.data.status == "success") {

						STFcache.delete('movementEvent' + id_uniq);

						$rootScope.tooltipMessageAlert = {
							type: 'success',
							message: res.data.response
						};

						$location.path('/events/' + id_uniq + '/movement');

					} else {

						$scope.creatingMovementLateralAlert = {
							type: 'danger',
							message: res.data.response
						};

					}

				}, function (err) {

					$scope.creatingMovementLateral = false;

					if (err.status == 401) {

						Auth.deleteToken();

					} else {

						$scope.creatingMovementLateralAlert = {
							type: 'danger',
							message: err.status + ' ' + err.statusText
						};

					}
				});

			};

		},
		link: function (scope, iElement, iAttrs) {

			$document.on('keypress', function (e) {

				if (e.charCode == 115 && scope.activetab === "Lateral") {

					if (scope.interval == false) {

						scope.d = new Date();

						scope.interval = $interval(function () {

							scope.now = new Date();
							scope.ellapsed = scope.now.getTime() - scope.d.getTime();
							scope.stopwatch = scope.ellapsed;

						}, 33);	

					} else {

						if (scope.attempts < 10) {

							if (scope.lateraldata.checkpoints.length == 0) {

								scope.lateraldata.checkpoints[scope.attempts] = scope.stopwatch;
								
							} else {

								var ellapsedTime = 0;

								angular.forEach(scope.lateraldata.checkpoints, function (value, key) {

									ellapsedTime += value;

								});

								scope.lateraldata.checkpoints[scope.attempts] = scope.stopwatch - ellapsedTime;
								
							}

							scope.attempts++;

						};

						if (scope.attempts == 10) {

							$interval.cancel(scope.interval);
							scope.totaltime += scope.stopwatch;
							scope.timerCompleted = true;

						};

					}

				};

			});

		}
	};
}]);

stf.directive('movementZigzag', ['$interval', '$timeout', '$document', function ($interval, $timeout, $document) {
	return {
		templateUrl: 'dist/views/templates/movement.zigzag.html',
		restrict: 'E',
		scope: {
			activetab: '=',
			playerid: '=',
			eventid: '='
		},
		controller: function ($scope, $interval, $filter, $routeParams, $rootScope, $location, Movement, Auth, STFcache) {

			var id_uniq = $routeParams.id_uniq;

			$scope.init = function () {

				$scope.interval = false;
				$scope.stopwatch = new Date(Date.UTC(99, 11, 1, 0, 0, 0));
				$scope.totaltime = 0;
				$scope.attempts = 0;

				$scope.timerCompleted = false;
				$scope.zigzagdata = {
					id_player: $scope.playerid,
					id_event: $scope.eventid,
					measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
					checkpoints: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				};

			};

			$scope.resetTimer = function () {

				$interval.cancel($scope.interval);
				$scope.init();

			};

			$scope.$on('$destroy', function () { $interval.cancel($scope.interval); });

			$rootScope.$on('movementTestPlayerChanged', function () {

				$scope.resetTimer();

			})

			$scope.init();

			$scope.createMovementZigzag = function () {

				$scope.creatingMovementZigzag = true;
				$scope.creatingMovementZigzagAlert = false;

				var data = $.param($scope.zigzagdata);

				Movement.createZigzag(data).then(function (res) {

					$scope.creatingMovementZigzag = false;

					if (res.data.status == "success") {

						STFcache.delete('movementEvent' + id_uniq);

						$rootScope.tooltipMessageAlert = {
							type: 'success',
							message: res.data.response
						};

						$location.path('/events/' + id_uniq + '/movement');

					} else {

						$scope.creatingMovementZigzagAlert = {
							type: 'danger',
							message: res.data.response
						};

					}

				}, function (err) {

					$scope.creatingMovementZigzag = false;

					if (err.status == 401) {

						Auth.deleteToken();

					} else {

						$scope.creatingMovementZigzagAlert = {
							type: 'danger',
							message: err.status + ' ' + err.statusText
						};

					}
				});

			};

		},
		link: function (scope, iElement, iAttrs) {

			$document.on('keypress', function (e) {

				if (e.charCode == 115 && scope.activetab === "Zigzag") {

					if (scope.interval == false) {

						scope.d = new Date();

						scope.interval = $interval(function () {

							scope.now = new Date();
							scope.ellapsed = scope.now.getTime() - scope.d.getTime();
							scope.stopwatch = scope.ellapsed;

						}, 33);	

					} else {

						if (scope.attempts < 10) {

							if (scope.zigzagdata.checkpoints.length == 0) {

								scope.zigzagdata.checkpoints[scope.attempts] = scope.stopwatch;
								
							} else {

								var ellapsedTime = 0;

								angular.forEach(scope.zigzagdata.checkpoints, function (value, key) {

									ellapsedTime += value;

								});

								scope.zigzagdata.checkpoints[scope.attempts] = scope.stopwatch - ellapsedTime;
								
							}

							scope.attempts++;

						};

						if (scope.attempts == 10) {

							$interval.cancel(scope.interval);
							scope.totaltime += scope.stopwatch;
							scope.timerCompleted = true;

						};

					}

				};

			});

		}
	};
}]);