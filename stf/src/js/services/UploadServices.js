var stf = angular.module('UploadServices', []);

stf.factory('Upload', ['$http', '$rootScope', '$window', function ($http, $rootScope, $window) {
	

	return {
		uploadProfilePicture: function (data) {

			return $http.post($rootScope.apiURL + 'upload', data, {

				params: {
					token: $window.localStorage.token
				},
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined
				}

			});

		},
		uploadPlayerPictures: function (data) {

			return $http.post($rootScope.apiURL + 'upload/player', data, {

				params: {
					token: $window.localStorage.token
				},
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined
				}

			});

		}
	};
}]);