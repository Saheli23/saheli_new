var stf = angular.module('AccessServices', []);

stf.factory('Access', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	return {
		getAccess: function ( section_id ) {
			return $http.get('api/access/access', {
				params: {
					section_id : section_id
				},
				headers: {
					token: $window.localStorage.token
				}
			}).then(function (response) {
				return response.data.response;
			});
		},

		formatData: function( data ){
			data.bo_view   = data.bo_view === "1";
			data.bo_add    = data.bo_add === "1";
			data.bo_edit   = data.bo_edit === "1";
			data.bo_delete = data.bo_delete === "1";
			data.bo_admin  = data.bo_admin === "1";
			return data;
		}
	};
}])