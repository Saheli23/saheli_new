var stf = angular.module('AuthServices', []);

stf.factory('Auth', ['$http', '$window', '$location', function ($http, $window, $location) {
	

	return {
		login: function (data) {

			return $http.post('api/auth', data);

		},
		createToken: function (data) {

			$window.localStorage.setItem('token', data);

		},
		checkToken: function () {

			if ($window.localStorage.token === undefined) { $location.path('/'); };

		},
		goToDashboardIfToken: function () {

			if ($window.localStorage.token !== undefined) { $location.path('/dashboard'); };

		},
		deleteToken: function () {

			$window.localStorage.removeItem('token');
			$location.path('/');

		}
	};
}]);