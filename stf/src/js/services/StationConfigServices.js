var stf = angular.module('StationConfigServices', []);

stf.factory('Station', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		getStationsAbleToConfigure: function () {

			return $http.get('api/station/able', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('stationsAbleToConfigure')
			});

		},
		createConfiguration: function (data) {

			return $http.post('api/station/configuration', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		editConfiguration: function (id, data) {

			return $http.put('api/station/configuration', data, {
				params: {
					id: id
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getConfiguration: function (id) {

			return $http.get('api/station/configuration', {
				params: {
					id: id
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('stationConfig' + id)
			});

		},
		getConfigurations: function () {

			return $http.get('api/station/configuration', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('stationsConfigAll')
			});

		},
		getTests: function (id_station) {

			return $http.get('api/station/tests', {
				params: {
					id_station: id_station
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('tests' + id_station)
			});

		},

		delete: function (data) {
			return $http.put('api/station/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		multiDelete: function (data) {
			return $http.put('api/station/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}]);