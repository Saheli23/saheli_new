var nv = angular.module('AnthropometricServices', []);

nv.factory('Anthropometric', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		check: function (id_player, id_event) {

			return $http.get('api/anthropometric/check', {
				params: {
					id_player: id_player,
					id_event: id_event
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		create: function (data) {

			return $http.post('api/anthropometric', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getAll: function (id_uniq_event) {

			return $http.get('api/anthropometric/event', {
				params: {
					id_uniq_event: id_uniq_event
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('anthropometricEvent' + id_uniq_event)
			});

		},
		getOne: function (id_uniq) {

			return $http.get('api/anthropometric', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('anthropometric' + id_uniq)
			});

		},
		edit: function (data, id_uniq) {

			return $http.put('api/anthropometric', data, {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getWeight: function (id_player, id_event) {

			return $http.get('api/anthropometric/player_weight', {
				params: {
					id_player: id_player,
					id_event: id_event
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		
		delete: function (data) {
			return $http.put('api/anthropometric/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		multiDelete: function (data) {
			return $http.put('api/anthropometric/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}])