var stf = angular.module('MeasurementServices', []);

stf.factory('Measurement', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		getMeasurements: function () {

			return $http.get('api/measurement', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.delete('measurements')
			});

		}
	};
}])