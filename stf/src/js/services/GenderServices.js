var stf = angular.module('GenderServices', []);

stf.factory('Gender', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		getGenders: function () {

			return $http.get('api/gender', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('genders')
			});

		}
	};
}]);