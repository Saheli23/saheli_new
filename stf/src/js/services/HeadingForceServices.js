var stf = angular.module('HeadingForceServices', []);

stf.factory('HeadingForce', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {

	return {
		check: function (id_player, id_event, data) {
			return $http.post('api/headingforce/check', data, {
				params: {
					id_player: id_player,
					id_event: id_event
				},
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		createForce: function (data) {
			return $http.post('api/headingforce/force', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		updateForce: function (data) {
			$http.put('api/headingforce/force', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		deleteForce: function (data) {
			$http.put('api/headingforce/force', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		getAll: function (id_uniq_event) {

			return $http.get('api/headingforce/event', {
				params: {
					id_uniq_event: id_uniq_event
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('headingEvent' + id_uniq_event)
			});

		}
	};
}])