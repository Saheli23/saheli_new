var stf = angular.module('ImageServices', []);

stf.factory('Image', ['$http', '$window', function ($http, $window) {
	

	return {
		set: function (data) {

			return $http.post('api/image/set', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		playerSet: function (data) {

			return $http.post('api/image/player_set', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		}
	};
}])