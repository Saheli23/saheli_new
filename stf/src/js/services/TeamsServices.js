var stf = angular.module('TeamServices', []);

stf.factory('Team', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		getCategories: function () {

			return $http.get('api/team/categories', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('categories')
			});

		},
		createTeam: function (data) {

			return $http.post('api/team', data, {
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'token': $window.localStorage.token
				}
			})

		},
		getTeam: function (id_uniq) {

			return $http.get('api/team', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('teams' + id_uniq)
			});

		},
		getTeams: function () {

			return $http.get('api/team', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('teams')
			});

		},
		editTeam: function (id_uniq, data) {

			return $http.post('api/team', data, {
				params: {
					id_uniq: id_uniq
				},
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'token': $window.localStorage.token
				}
			});

		},
		getPositions: function () {

			return $http.get('api/team/positions', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('positions')
			});

		},
		getUniqIDs: function () {

			return $http.get('api/team/uniqids', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('teamUniqIDs')
			});

		},
		getBaseImage: function (id_uniq) {

			return $http.get('api/team/base_image', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},

		delete: function (data) {
			return $http.put('api/team/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		
		multidelete: function (data) {
			return $http.put('api/team/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}]);