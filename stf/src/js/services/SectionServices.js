var stf = angular.module('SectionServices', []);
stf.factory('Section', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	return {
		getSection: function () {
			return $http.get('api/section/section', {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}])