var stf = angular.module('CountryServices', []);

stf.factory('Country', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		getCountries: function () {

			return $http.get('api/country', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('countries')
			});

		},
		getCountryStates: function (id_country) {

			return $http.get('api/country/states', {
				params: {
					id_country: id_country
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('states')
			});

		}
	};
}]);