var stf = angular.module('CacheServices', []);

stf.factory('STFcache', ['$cacheFactory', function ($cacheFactory) {
	
	console.log($cacheFactory.info());

	return {
		set: function (name) {

			if ($cacheFactory.get(name) !== undefined) return;

			return $cacheFactory(name);

		},
		delete: function (name) {

			if ($cacheFactory.get(name) === undefined) return;

			return $cacheFactory.get(name).destroy();

		},
		save: function (name) {

			this.set(name);

			return $cacheFactory.get(name);

		}
	};
}]);