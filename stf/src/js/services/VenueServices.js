var stf = angular.module('VenueServices', []);

stf.factory('Venue', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		createVenue: function (data) {

			return $http.post('api/venue', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getVenues: function () {

			return $http.get('api/venue', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('venues')
			});

		},
		getVenue: function (id_uniq) {

			return $http.get('api/venue', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('venues' + id_uniq)
			});

		},
		editVenue: function (id_uniq, data) {

			return $http.put('api/venue', data, {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getUniqIDs: function () {

			return $http.get('api/venue/uniqids', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('venueUniqIDs')
			});

		},

		delete: function (data) {
			return $http.put('api/venue/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		multidelete: function (data) {
			return $http.put('api/venue/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
	};
}]);