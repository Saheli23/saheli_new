var stf = angular.module('UserServices', []);

stf.factory('User', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		createUser: function (data) {

			return $http.post('api/user', data, {
				/*params: {
					token: $window.localStorage.token
				},*/
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'token': $window.localStorage.token
				}
			});

		},
		getLoggedInUser: function () {

			return $http.get('api/user/logged_in_user', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('logged_in_user')
			});

		},
		getAll: function () {

			return $http.get('api/user', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('users')
			});

		},
		getOne: function (id_uniq) {

			return $http.get('api/user', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('users' + id_uniq)
			});

		},
		editUser: function (id_uniq, data) {

			return $http.post('api/user', data, {
				params: {
					id_uniq: id_uniq
				},
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'token': $window.localStorage.token
				}
			});

		},
		getUniqIDs: function () {

			return $http.get('api/user/uniqids', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('userUniqIDs')
			});

		},
		getBaseImage: function (id_uniq) {

			return $http.get('api/user/base_image', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},

		delete: function (data) {
			return $http.put('api/user/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		multiDelete: function (data) {
			return $http.put('api/user/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}]);