var stf = angular.module('ForgotPasswordServices', []);

stf.factory('Forgot', ['$http', '$window', '$location', function ($http, $window, $location) {
	

	return {
		passwordforgot: function (data) {

			return $http.post('api/forgot/passwordforgot', data);

		}
	};
}]);