var stf = angular.module('stf', [
	'uiGmapgoogle-maps',
	'ui.bootstrap',
	'ui.mask',
	'wt.responsive',
	'pickadate',
	'mdo-angular-cryptography',
	'ngRoute',
	'ngMessages',
	'ngSanitize',
	'MainControllers',
	'MainDirectives',
	'MainFilters',
	'AuthServices',
	'CacheServices',
	'UserControllers',
	'UserServices',
	'EventControllers',
	'EventServices',
	'StationConfigControllers',
	'StationConfigServices',
	'AnthropometricControllers',
	'AnthropometricServices',
	'DribblingControllers',
	'DribblingServices',
	'KickingControllers',
	'KickingServices',
	'MovementControllers',
	'MovementDirectives',
	'MovementServices',
	'HeadingControllers',
	'HeadingServices',
	'PlayerControllers',
	'PlayerServices',
	'PlayerDirectives',
	'TeamControllers',
	'TeamServices',
	'VenueControllers',
	'VenueServices',
	'UploadControllers',
	'UploadServices',
	'CountryServices',
	'GenderServices',
	'MeasurementServices',
	'ImageServices',
	'AgeServices',
	'SectionServices',
	'AccessServices',
	'ForgotPasswordServices'
]);

stf.config(['$routeProvider', '$locationProvider', 'uiGmapGoogleMapApiProvider', '$cryptoProvider', function ($routeProvider, $locationProvider, uiGmapGoogleMapApiProvider, $cryptoProvider) {
	
	uiGmapGoogleMapApiProvider.configure({
		v: '3.20',
		libraries: 'places'
	});

	$cryptoProvider.setCryptographyKey('`I<a@4z5Od(7`W_');

	$routeProvider

		.when('/', {
			templateUrl: 'dist/views/login.html',
			controller: 'LoginController',
			resolve: ['Auth', function (Auth) {
				Auth.goToDashboardIfToken();
			}]
		})

		.when('/home', {
			redirectTo: '/'
		})

		.when('/forgot-password', {
			templateUrl: 'dist/views/password.forgot.html',
			controller: 'PasswordForgotController',
			resolve: ['Auth', function (Auth) {
				Auth.goToDashboardIfToken();
			}]
		})

		.when('/password-recover', {
			templateUrl: 'dist/views/password.recover.html',
			controller: 'PasswordRecoverController',
			resolve: ['Auth', function (Auth) {
				Auth.goToDashboardIfToken();
			}]
		})

		.when('/dashboard', {
			templateUrl: 'dist/views/dashboard.html',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Dashboard'
			}
		})

		.when('/upload/:owner/:uniqid', {
			templateUrl: 'dist/views/upload.html',
			controller: 'ProfilePictureUploadController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}]
		})

		.when('/set_image/:owner/:id_uniq', {
			templateUrl: 'dist/views/set.image.html',
			controller: 'setImageController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}]
		})

		.when('/users', {
			templateUrl: 'dist/views/user.list.html',
			controller: 'UserListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Users'
			}
		})

		.when('/users/create', {
			templateUrl: 'dist/views/user.create.html',
			controller: 'UserCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Users'
			}
		})

		.when('/users/:id_uniq', {
			templateUrl: 'dist/views/user.details.html',
			controller: 'UserDetailsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Users'
			}
		})

		.when('/users/:id_uniq/edit', {
			templateUrl: 'dist/views/user.edit.html',
			controller: 'UserEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Users'
			}
		})

		.when('/events', {
			templateUrl: 'dist/views/event.list.html',
			controller: 'EventListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/create', {
			templateUrl: 'dist/views/event.create.html',
			controller: 'EventCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/stations/config', {
			templateUrl: 'dist/views/station.config.list.html',
			controller: 'StationConfigListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/stations/config/create', {
			templateUrl: 'dist/views/station.config.create.html',
			controller: 'StationConfigCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/stations/config/:id/edit', {
			templateUrl: 'dist/views/station.config.edit.html',
			controller: 'StationConfigEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id_uniq', {
			templateUrl: 'dist/views/event.details.html',
			controller: 'EventDetailsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id_uniq/edit', {
			templateUrl: 'dist/views/event.edit.html',
			controller: 'EventEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id_uniq/anthropometrics', {
			templateUrl: 'dist/views/anthropometrics.list.html',
			controller: 'AnthropometricListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id_uniq/anthropometrics/create', {
			templateUrl: 'dist/views/anthropometrics.create.html',
			controller: 'AnthropometricCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id_uniq/anthropometrics/:id_uniq_player', {
			templateUrl: 'dist/views/anthropometrics.results.html',
			controller: 'AnthropometricDetailController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/anthropometrics/:id_uniq/edit', {
			templateUrl: 'dist/views/anthropometrics.edit.html',
			controller: 'AnthropometricEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id_uniq/dribbling', {
			templateUrl: 'dist/views/dribbling.list.html',
			controller: 'DribblingListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id_uniq/dribbling/create', {
			templateUrl: 'dist/views/dribbling.create.html',
			controller: 'DribblingCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id_uniq/kicking', {
			templateUrl: 'dist/views/kicking.list.html',
			controller: 'KickingListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id_uniq/kicking/create', {
			templateUrl: 'dist/views/kicking.create.html',
			controller: 'KickingCreateController',
			resolve: ['Auth', function (Auth){
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id/kicking/:playerid/edit', {
			templateUrl: 'dist/views/kicking.edit.html',
			controller: 'KickingEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id_uniq/movement', {
			templateUrl: 'dist/views/movement.list.html',
			controller: 'MovementListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id_uniq/movement/create', {
			templateUrl: 'dist/views/movement.create.html',
			controller: 'MovementCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id_uniq/heading', {
			templateUrl: 'dist/views/heading.list.html',
			controller: 'HeadingListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id_uniq/heading/create', {
			templateUrl: 'dist/views/heading.create.html',
			controller: 'HeadingCreateController',
			resolve: ['Auth', function (Auth){
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id/heading/:playerid', {
			templateUrl: 'dist/views/heading.results.html',
			controller: 'HeadingResultsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/events/:id/heading/:playerid/edit', {
			templateUrl: 'dist/views/heading.edit.html',
			controller: 'HeadingEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Events'
			}
		})

		.when('/players', {
			templateUrl: 'dist/views/player.list.html',
			controller: 'PlayerListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})

		.when('/players/create', {
			templateUrl: 'dist/views/player.create.html',
			controller: 'PlayerCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})

		.when('/players/:id_uniq', {
			templateUrl: 'dist/views/player.details.html',
			controller: 'PlayerDetailsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})

		.when('/players/:id_uniq/edit', {
			templateUrl: 'dist/views/player.edit.html',
			controller: 'PlayerEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})

		.when('/players/:id_uniq/teams', {
			templateUrl: 'dist/views/player.teams.html',
			controller: 'PlayerTeamsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})

		.when('/players/:id_uniq/teams/create', {
			templateUrl: 'dist/views/player.team.create.html',
			controller: 'PlayerTeamCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})

		.when('/players/:id_uniq/teams/:id_team_player/edit', {
			templateUrl: 'dist/views/player.team.edit.html',
			controller: 'PlayerTeamEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Players'
			}
		})

		.when('/teams', {
			templateUrl: 'dist/views/team.list.html',
			controller: 'TeamListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Teams'
			}
		})

		.when('/teams/create', {
			templateUrl: 'dist/views/team.create.html',
			controller: 'TeamCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Teams'
			}
		})

		.when('/teams/:id_uniq', {
			templateUrl: 'dist/views/team.details.html',
			controller: 'TeamDetailsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Teams'
			}
		})

		.when('/teams/:id_uniq/edit', {
			templateUrl: 'dist/views/team.edit.html',
			controller: 'TeamEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Teams'
			}
		})

		.when('/venues', {
			templateUrl: 'dist/views/venue.list.html',
			controller: 'VenueListController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Venues'
			}
		})

		.when('/venues/create', {
			templateUrl: 'dist/views/venue.create.html',
			controller: 'VenueCreateController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Venues'
			}
		})

		.when('/venues/:id_uniq', {
			templateUrl: 'dist/views/venue.details.html',
			controller: 'VenueDetailsController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Venues'
			}
		})

		.when('/venues/:id_uniq/edit', {
			templateUrl: 'dist/views/venue.edit.html',
			controller: 'VenueEditController',
			resolve: ['Auth', function (Auth) {
				Auth.checkToken();
			}],
			data: {
				title: 'Venues'
			}
		})

		.otherwise({ redirectTo: '/' });

	$locationProvider.html5Mode(true);

}]);

stf.run(['$rootScope', '$http', '$window', '$crypto', function ($rootScope, $http, $window, $crypto) {

	$rootScope.$on('$routeChangeSuccess', function (e, current, previous) {

		if (angular.isDefined(current.data) && angular.isDefined(current.data.title)) { $rootScope.currentlocation = current.data.title; }
		else { $rootScope.currentlocation = 'STF' }

	});
	
	$rootScope.tooltipMessageAlert = false;
	$rootScope.currentYear = new Date().getFullYear();
	
	$http.defaults.headers.post = { 'Content-Type': 'application/x-www-form-urlencoded' };
	$http.defaults.headers.put = { 'Content-Type': 'application/x-www-form-urlencoded' };

}]);
var stf = angular.module('AnthropometricControllers', []);

stf.controller('AnthropometricListController', ['$scope', '$routeParams', 'Event', 'Anthropometric', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, Event, Anthropometric, Access, Auth, STFcache) {
	
	 $scope.bulk=false;
   	var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
			 values=[];
			$(".childcheckbox").each(function(){
		        if($(this).is(":checked"))
		            values.push($(this).val());
		    });
		    //alert(values);
		   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
		   	angular.element("#mastercheckbox").prop("checked",true);
		   } 
		   else{
		
		    angular.element("#mastercheckbox").prop("checked",false);
		      }
	};


	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAnthropometrics = function (id_uniq, refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('anthropometricEvent' + $scope.id_uniq);
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.measurements = null;
		$scope.loadingAnthropometrics = true;
		$scope.loadingAnthropometricsAlert = false;

		Anthropometric.getAll(id_uniq).then(function (res) {

			$scope.loadingAnthropometrics = false;

			if (angular.isObject(res.data.response)) {

				$scope.measurements = res.data.response;
				
			} else {

				$scope.loadingAnthropometricsAlert = {
					type: 'success',
					message: res.data.response
				};

			}
			$scope.getAccess(1);

		}, function (err) {

			$scope.loadingAnthropometrics = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingAnthropometricsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getEvent($scope.id_uniq);
	$scope.getAnthropometrics($scope.id_uniq);

	$scope.deleteAnthropometrics = function(id_uniq){

			if(confirm("Are you sure to delete this Anthropometrics of this player?")){
				var data = $.param({id_uniq:id_uniq,type:'delete'});
				Anthropometric.delete(data).then(function (res) {
					$scope.loadingAnthropometrics = false;
					
					$scope.getAnthropometrics($scope.id_uniq,true);
					if (angular.isObject(res.data.response)) {
						//$scope.measurements = res.data.response;

					} else {
						$scope.loadingAnthropometricsAlert = {type: 'success',message: res.data.response};
					}

				}, function (err) {

					$scope.loadingAnthropometrics = false;

					if (err.status == 401) {
						Auth.deleteToken();
					} else {
						$scope.loadingAnthropometricsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
					}
				});
			}
		};
	
	$scope.bulkDeleteAnthropometrics = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Anthropometrics of this player?")){
			var data = $.param({id_uniq:values,type:'delete',action:action_id});
			Anthropometric.multidelete(data).then(function (res) {
					$scope.loadingAnthropometrics = false;
					
					$scope.getAnthropometrics($scope.id_uniq,true);
					if (angular.isObject(res.data.response)) {
						//$scope.measurements = res.data.response;

					} else {
						$scope.loadingAnthropometricsAlert = {type: 'success',message: res.data.response};
					}

				}, function (err) {

					$scope.loadingAnthropometrics = false;

					if (err.status == 401) {
						Auth.deleteToken();
					} else {
						$scope.loadingAnthropometricsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
					}
				});
			}
	 }
	 else{
	 	alert("Please select atleast one to delete.");
	 }
	};	

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

	$scope.orderOptions = [
		{ value: 'tx_firstname', text: 'Name' },
		{ value: '-dt_measurement', text: 'Date' }
	];
	
}]);

stf.controller('AnthropometricCreateController', ['$scope', '$routeParams', '$location', '$rootScope', 'Event', 'Player', 'Auth', 'Anthropometric', 'STFcache','$filter', function ($scope, $routeParams, $location, $rootScope, Event, Player, Auth, Anthropometric, STFcache,$filter) {
	
	$scope.id_uniq = $routeParams.id_uniq;
	$scope.player = null;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	$scope.formdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		size: '',
		weight: '',
		chest_diameter: '',
		hip_diameter: '',
		left_thigh_diameter: '',
		right_thigh_diameter: '',
		left_ankle_diameter: '',
		right_ankle_diameter: '',
		head_diameter: ''
	};

	//$scope.date = null;
	//$scope.maxDate = new Date();
	// $scope.pickDateOptions = {
	// 	format: 'mm/dd/yyyy',
	// 	selectYears: true,
	// 	selectMonths: true,
	// };

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;
				$scope.formdata.id_event = $scope.event.id;
                
                  //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    if (String.fromCharCode(evt.which) == "e"){
        	return false;
            }
            if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length >1){ 
             
                this.value = parseFloat($(this).val().slice(0, -1));
            }  
         }    

			});

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			if (angular.isObject(res.data.response)) {

				var player = res.data.response;
				
				$scope.formdata.id_player = player.id;

				Anthropometric.check($scope.formdata.id_player, $scope.formdata.id_event).then(function (res) {

					$scope.loadingPlayer = false;

					if (res.data.player_exists == 1) {
                       
						$scope.loadingPlayerAlert = {
							type: 'success',
							message: res.data.response
						};
						
					} else {

						$scope.player = player;

					}

				}, function (err) {

					$scope.loadingPlayer = false;

					$scope.loadingPlayerAlert = {
						type: 'danger',
						message: res.data.response
					};

				});

			} else {

				$scope.loadingPlayer = false;

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$scope.formdata.id_player = '';
		$scope.formdata.measurement_date = '';
		$scope.formdata.size = '';
		$scope.formdata.weight = '';
		$scope.formdata.chest_diameter = '';
		$scope.formdata.hip_diameter = '';
		$scope.formdata.left_thigh_diameter = '';
		$scope.formdata.right_thigh_diameter = '';
		$scope.formdata.left_ankle_diameter = '';
		$scope.formdata.right_ankle_diameter = '';
		$scope.formdata.head_diameter = '';

		$scope.CreateAnthropometricForm.$setPristine();

	};

	$scope.getEvent($scope.id_uniq);

	$scope.createAnthropometric = function () {

		$scope.creatingAnthropometric = true;
		$scope.creatingAnthropometricAlert = false;

		var data = $.param($scope.formdata);

		Anthropometric.create(data).then(function (res) {

			$scope.creatingAnthropometric = false;

			if (res.data.status == "success") {

				STFcache.delete('anthropometricEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/anthropometrics');

			} else {

				$scope.creatingAnthropometricAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingAnthropometric = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingAnthropometricAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('AnthropometricDetailController', ['$scope', 'Access', function ($scope, Access ) {
	
}]);

stf.controller('AnthropometricEditController', ['$scope', '$routeParams', '$location', '$rootScope', '$filter', 'Anthropometric', 'Auth', 'STFcache', function ($scope, $routeParams, $location, $rootScope, $filter, Anthropometric, Auth, STFcache) {
	
	var id_uniq = $routeParams.id_uniq;

	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	$scope.formdata = {
		measurement_date: '',
		size: '',
		weight: '',
		chest_diameter: '',
		hip_diameter: '',
		left_thigh_diameter: '',
		right_thigh_diameter: '',
		left_ankle_diameter: '',
		right_ankle_diameter: '',
		head_diameter: ''
	};

	$scope.date = null;
	$scope.maxDate = new Date();
	$scope.pickDateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: true,
		selectMonths: true,
	};

	$scope.getAnthropometric = function (id_uniq) {

		$scope.loadingAnthropometric = true;
		$scope.loadingAnthropometricAlert = false;

		Anthropometric.getOne(id_uniq).then(function (res) {

			$scope.loadingAnthropometric = false;

			if (angular.isObject(res.data.response)) {

				$scope.anthropometric = res.data.response;

				$scope.formdata.measurement_date = $filter('date')($scope.anthropometric.dt_measurement, 'MM/dd/yyyy');
				$scope.formdata.size = parseFloat($scope.anthropometric.nu_size);
				$scope.formdata.weight = parseFloat($scope.anthropometric.nu_weight);
				$scope.formdata.chest_diameter = parseFloat($scope.anthropometric.nu_chest_diameter);
				$scope.formdata.hip_diameter = parseFloat($scope.anthropometric.nu_hip_diameter);
				$scope.formdata.left_thigh_diameter = parseFloat($scope.anthropometric.nu_left_thigh_diameter);
				$scope.formdata.right_thigh_diameter = parseFloat($scope.anthropometric.nu_right_thigh_diameter);
				$scope.formdata.left_ankle_diameter = parseFloat($scope.anthropometric.nu_left_ankle_diameter);
				$scope.formdata.right_ankle_diameter = parseFloat($scope.anthropometric.nu_right_ankle_diameter);
				$scope.formdata.head_diameter = parseFloat($scope.anthropometric.nu_head_diameter);
                
                    //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    if (String.fromCharCode(evt.which) == "e"){
        	return false;
            }
            if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length >1){ 
              //alert($(this).val().split(".")[1].length);
             // $(this).val()
            // alert("gy");
                //if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat($(this).val().slice(0, -1));
            }  
         }    

			});


				

			} else {

				$scope.loadingAnthropometricAlert = {
					type: 'success',
					message: res.data.response

				};
            
			}

		}, function (err) {

			$scope.loadingAnthropometric = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingAnthropometricAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAnthropometric(id_uniq);

	$scope.editAnthropometric = function () {

		$scope.editingAnthropometric = true;
		$scope.editingAnthropometricAlert = false;

		var data = $.param($scope.formdata);

		Anthropometric.edit(data, id_uniq).then(function (res) {

			$scope.editingAnthropometric = false;

			if (res.data.status == 'success') {

				STFcache.delete('anthropometric' + id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + res.data.id_uniq_event + '/anthropometrics');

			} else {

				$scope.editingAnthropometricAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.editingAnthropometric = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingAnthropometricAlert = {
					type: 'danger',
					message: err.status + ' '  + err.statusText
				}

			}

		});

	};

}]);
var stf = angular.module('DribblingControllers', []);

stf.controller('DribblingListController', ['$scope', '$routeParams', '$location', 'Event', 'Dribbling', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, $location, Event, Dribbling, Access, Auth, STFcache) {
	
	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;

				$scope.getAccess(1);

				if (_.findIndex($scope.event.configurations, { station: 'Dribbling' }) >= 0) {

 					$scope.$emit('eventLoaded');

				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

	$scope.getEvent($scope.id_uniq);
	
}]);

stf.controller('DribblingCreateController', ['$scope', '$routeParams', '$location', '$rootScope', 'Event', 'Player', 'Dribbling', 'Auth', 'STFcache', function ($scope, $routeParams, $location, $rootScope, Event, Player, Dribbling, Auth, STFcache) {
	
	$scope.id_uniq = $routeParams.id_uniq;
	$scope.player = null;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	$scope.date = null;
	$scope.maxDate = new Date();
	$scope.pickDateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: true,
		selectMonths: true,
	};

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;

				if (_.findIndex($scope.event.configurations, { station: 'Dribbling' }) >= 0) {

 					/*$scope.aimdata.id_event = $scope.event.id;
 					$scope.forcedata.id_event = $scope.event.id;*/

				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getEvent($scope.id_uniq);

}]);
var stf = angular.module('EventControllers', []);

stf.controller('EventListController', ['$scope', 'Event', 'Auth', 'User', 'Access', 'STFcache', function ($scope, Event, Auth, User, Access, STFcache) {
	
	$scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};



	$scope.getEvents = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('events');
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.events = null;
		$scope.loadingEvents = true;
		$scope.loadingEventsAlert = false;

		Event.getAll().then(function (res) {
			$scope.loadingEvents = false;
			if (angular.isObject(res.data.response)) {
				$scope.events = res.data.response;
				
			} else {
				$scope.loadingEventsAlert = { type: 'success', message: res.data.response };
			}
			$scope.getAccess(1);
		}, function (err) {
			$scope.loadingEvents = false;
			err.status == 401 ? Auth.deleteToken() : $scope.loadingEventsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
		});

	};

	$scope.getEvents();

	$scope.deleteEvent = function( event_id, ind ){

		if(confirm("Are you sure to delete this Event?")){
			var data = $.param({id_event:event_id,type:'delete'});
			Event.delete(data).then(function (res) {
				$scope.loadingEvents = false;
				$scope.getEvents(true);
				if (angular.isObject(res.data.response)) {
					//console.log(res.data.response);
				} else {
					$scope.loadingEventsAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {
				$scope.loadingEvents = false;
				err.status == 401 ? Auth.deleteToken() : $scope.loadingEventsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
			});
		}
	};

	 $scope.bulkDeleteEvent = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Event?")){
			var data = $.param({id_event:values,type:'delete',action:action_id});
			Event.multidelete(data).then(function (res) {
				$scope.loadingEvents = false;
				$scope.getEvents(true);
				if (angular.isObject(res.data.response)) {
					//console.log(res.data.response);
				} else {
					$scope.loadingEventsAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {
				$scope.loadingEvents = false;
				err.status == 401 ? Auth.deleteToken() : $scope.loadingEventsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
			});
		}
	 }
	 else{
	 	alert("Please select atleast one Event to delete.");
	 }
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
			changeAttr( $scope.events, 'events', $scope.access.bo_view );
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		})
	}

	$scope.restrictView = function(){
		$('#alertMessage').show();
		setTimeout(function(){ 
			$('#alertMessage').hide();
		}, 2000);
	}

	function compile(element){
		var el = angular.element(element);    
		$scope = el.scope();
		$injector = el.injector();
		$injector.invoke(function($compile){
			$compile(el)($scope);
		})     
	}

	function changeAttr( obj, slug, viewAccess ){
        for( var i=0; i < obj.length; i++ ){
            var el = document.getElementById((slug.substring(0, slug.length-1))+"_id_"+i);
            if( viewAccess ){
                el.removeAttribute("ng-click");
                el.setAttribute("ng-href",slug+"/"+obj[i].id_uniq);
                el.setAttribute("href",slug+"/"+obj[i].id_uniq);
            } else {
                el.removeAttribute("ng-href");
                el.removeAttribute("href");
                el.setAttribute("ng-click", "restrictView()");
                compile(el);
            }
        }
    }


	$scope.orderOptions = [
		{ value: 'tx_name', text: 'Name' },
		{ value: '-dt_event', text: 'Date' }
	];

}]);

stf.controller('EventCreateController', ['$scope', '$timeout', '$rootScope', '$location', 'Auth', 'Venue', 'Team', 'Measurement', 'Station', 'Event', 'STFcache', function ($scope, $timeout, $rootScope, $location, Auth, Venue, Team, Measurement, Station, Event, STFcache) {
	var minDate = new Date();

	$scope.formdata = {
		name: '',
		date: '',
		venue: '',
		team: '',
		measurement_system: '',
		configurations: []
	};

	$scope.configurationdata = {
		station: '',
		test: '',
		age_range: '',
		distance: '0'
	};

	$scope.date = null;

	//$scope.minDate = new Date();
	$scope.minDate = new Date(minDate.setFullYear(minDate.getFullYear()));
	$scope.dateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: 100,
		selectMonths: true
	};
   
	$scope.getVenues = function () {

		$scope.loadingVenues = true;
		$scope.loadingVenuesAlert = false;

		Venue.getVenues().then(function (res) {
			$scope.loadingVenues = false;

			if (angular.isObject(res.data.response)) {

				$scope.venues = res.data.response;

			} else {

				$scope.loadingVenuesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingVenues = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingVenuesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTeams = function () {

		$scope.loadingEvents = true;
		$scope.loadingEventsAlert = false;

		Team.getTeams().then(function (res) {

			$scope.loadingEvents = false;

			if (angular.isObject(res.data.response)) {

				$scope.teams = res.data.response;
				
			} else {

				$scope.loadingEventsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvents = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getMeasurements = function () {

		$scope.loadingMeasurements = true;
		$scope.loadingMeasurementsAlert = false;

		Measurement.getMeasurements().then(function (res) {

			$scope.loadingMeasurements = false;

			if (angular.isObject(res.data.response)) {

				$scope.measurements = res.data.response;
				
			} else {

				$scope.loadingMeasurementsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingMeasurements = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingMeasurementsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getStationsAbleToConfigure = function () {

		$scope.loadingStationsAbleToConfigure = true;
		$scope.loadingStationsAbleToConfigureAlert = false;

		Station.getStationsAbleToConfigure().then(function (res) {

			$scope.loadingStationsAbleToConfigure = false;

			if (angular.isObject(res.data.response)) {

				$scope.stations = res.data.response;

			} else {

				$scope.loadingStationsAbleToConfigureAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStationsAbleToConfigure = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStationsAbleToConfigureAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAllStationConfigurations = function () {

		$scope.loadingConfigurations = true;
		$scope.loadingConfigurationsAlert = false;

		Station.getConfigurations().then(function (res) {

			$scope.loadingConfigurations = false;

			if (angular.isObject(res.data.response)) {

				$scope.configurations = res.data.response;
				$scope.$emit('configurationsLoaded');

			} else {

				$scope.loadingConfigurationsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingConfigurations = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingConfigurationsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getVenues();
	$scope.getTeams();
	$scope.getMeasurements();
	$scope.getStationsAbleToConfigure();
	$scope.getAllStationConfigurations();

	$scope.$on('configurationsLoaded', function () {

		$scope.$watch('configurationdata.station', function () {

			$scope.tests = [];

			$scope.configurationdata.test = '';
			$scope.configurationdata.age_range = '';
			$scope.configurationdata.distance = '0';

			$scope.ConfigurationForm.test.$setPristine();
			$scope.ConfigurationForm.age_range.$setPristine();
			
			angular.forEach($scope.configurations, function (value, key) {

				if (value.id_station == $scope.configurationdata.station) {

					if (_.findIndex($scope.tests, { id: value.id_test, name: value.test_name }) == -1) {

						$scope.tests.push({
							id: value.id_test,
							name: value.test_name
						});	

					};

				};

			});

		});

		$scope.$watch('configurationdata.test', function () {

			$scope.ages = [];

			$scope.configurationdata.age_range = '';
			$scope.configurationdata.distance = '0';

			$scope.ConfigurationForm.age_range.$setPristine();
			
			angular.forEach($scope.configurations, function (value, key) {

				if (value.id_station == $scope.configurationdata.station && value.id_test == $scope.configurationdata.test) {

					if (_.indexOf($scope.ages, value.nu_age) == -1) {

						if (_.findIndex($scope.ages, { id: value.id_age_range, name: value.nu_min_age + ' - ' + value.nu_max_age }) == -1) {

							$scope.ages.push({
								id: value.id_age_range,
								name: value.nu_min_age + ' - ' + value.nu_max_age
							});	

						};


					};

				};

			});

		});

		$scope.$watch('configurationdata.age_range', function () {

			angular.forEach($scope.configurations, function (value, key) {

				if (value.id_station == $scope.configurationdata.station && value.id_test == $scope.configurationdata.test && value.id_age_range == $scope.configurationdata.age_range) {

					$scope.configurationdata.distance = value.nu_distance;

				};

			});

		});

	});

	$scope.addConfiguration = function () {

		$scope.showTooltip = false;

		var indexToLookFor = {
			id_station: $scope.configurationdata.station,
			id_test: $scope.configurationdata.test,
			id_age_range: $scope.configurationdata.age_range,
			nu_distance: $scope.configurationdata.distance
		};

		var index = _.findIndex($scope.configurations, indexToLookFor);

		var dataToPush = {
			id: $scope.configurations[index].id,
			station: $scope.configurations[index].station_name,
			test: $scope.configurations[index].test_name,
			age_range: $scope.configurations[index].nu_min_age + ' - ' + $scope.configurations[index].nu_max_age,
			distance: $scope.configurations[index].nu_distance
		};

		if (_.findIndex($scope.formdata.configurations, dataToPush) == -1) {

			$scope.formdata.configurations.push(dataToPush);

			$scope.configurationdata = {
				station: '',
				test: '',
				age_range: '',
				distance: '0'
			};

			$scope.ConfigurationForm.$setPristine();

		} else {

			$scope.showTooltip = true;

			$timeout(function () {

				$scope.showTooltip = false;				

			}, 2000);

		}

	};	

	$scope.deleteConfiguration = function (index) {

		$scope.formdata.configurations.splice(index, 1);

	};

	$scope.createEvent = function () {

		$scope.creatingEvent = true;
		$scope.creatingEventAlert = false;

		var data = $.param($scope.formdata);

		Event.create(data).then(function (res) {

			$scope.creatingEvent = false;

			if (res.data.status == "success") {

				STFcache.delete('events');
				STFcache.delete('eventUniqIDs');

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Event created successfully'
				};

				$location.path('/events');

			} else {

				$scope.creatingEventAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('EventDetailsController', ['$scope', '$routeParams', '$location', 'Event', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, $location, Event, Access, Auth, STFcache ) {
	$scope.id_uniq = $routeParams.id_uniq;
	$scope.getUniqIDs = function () {

		$scope.loadingUniqIDs = true;
		$scope.loadingUniqIDsAlert = false;

		Event.getUniqIDs().then(function (res) {

			$scope.loadingUniqIDs = false;

			$scope.result = res.data.response;
			$scope.totalItems = parseInt($scope.result.length);
			$scope.currentItem = parseInt(_.findIndex($scope.result, { id_uniq: $scope.id_uniq }));
			$scope.nextItem = $scope.currentItem + 1;	
			$scope.prevItem = $scope.currentItem - 1;

			$scope.nextID = ($scope.nextItem > -1 && $scope.nextItem < $scope.totalItems) ? $scope.result[$scope.nextItem].id_uniq : false;
			$scope.prevID = ($scope.prevItem >= 0) ? $scope.result[$scope.prevItem].id_uniq : false;

		}, function (err) {

			$scope.loadingUniqIDs = false;

			if (err.status == 401) {

				Auth.deleteToken();

			};

		});

	};

	$scope.prevEvent = function () { $location.path('/events/' + $scope.prevID); };
	$scope.nextEvent = function () { $location.path('/events/' + $scope.nextID); };

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {
				$scope.event = res.data.response;
				$scope.getUniqIDs();
				$scope.$emit('eventLoaded');
				

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}
			$scope.getAccess(1);
		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getEvents = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('events');

		$scope.events = null;
		$scope.loadingEvents = true;
		$scope.loadingEventsAlert = false;

		Event.getAll().then(function (res) {
			$scope.loadingEvents = false;
			if (angular.isObject(res.data.response)) {
				$scope.events = res.data.response;
				$scope.getAccess(1);
			} else {
				$scope.loadingEventsAlert = { type: 'success', message: res.data.response };
			}

		}, function (err) {
			$scope.loadingEvents = false;
			err.status == 401 ? Auth.deleteToken() : $scope.loadingEventsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
		});

	};

	$scope.deleteEvent = function( event_id, ind ){

		if(confirm("Are you sure to delete this Event?")){
			var data = $.param({id_event:event_id,type:'delete'});
			Event.delete(data).then(function (res) {
				$scope.loadingEvents = false;
				$scope.getEvents(true);
				if (angular.isObject(res.data.response)) {
					//console.log(res.data.response);
					// $location.path('/events');
				} else {
					$scope.loadingEventsAlert = {type: res.data.status,message: res.data.response};
				   $location.path('/events');
				}

			}, function (err) {
				$scope.loadingEvents = false;
				err.status == 401 ? Auth.deleteToken() : $scope.loadingEventsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
			});
		}
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		})
	}

	$scope.$on('eventLoaded', function () {
		$scope.checkStation = function (station) {
			var configurations = $scope.event.configurations.length
			for (var i = 0; i < configurations; i++) {	
				if ($scope.event.configurations[i].station == station) { return true; }
			};

		};

	});

	$scope.getEvent($scope.id_uniq);

}]);

stf.controller('EventEditController', ['$scope', '$timeout', '$rootScope', '$location', '$routeParams', '$filter', 'Auth', 'Venue', 'Team', 'Measurement', 'Station', 'Event', 'STFcache', function ($scope, $timeout, $rootScope, $location, $routeParams, $filter, Auth, Venue, Team, Measurement, Station, Event, STFcache) {
	var minDate = new Date();
	var id_uniq = $routeParams.id_uniq;

	$scope.formdata = {
		name: '',
		date: '',
		venue: '',
		team: '',
		measurement_system: '',
		configurations: []
	};

	$scope.configurationdata = {
		station: '',
		test: '',
		age_range: '',
		distance: '0'
	};

	$scope.date = null;
	$scope.minDate = new Date(minDate.setFullYear(minDate.getFullYear()));
	$scope.dateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: 100,
		selectMonths: true
	};

	$scope.getVenues = function () {

		$scope.loadingVenues = true;
		$scope.loadingVenuesAlert = false;

		Venue.getVenues().then(function (res) {

			$scope.loadingVenues = false;

			if (angular.isObject(res.data.response)) {

				$scope.venues = res.data.response;

			} else {

				$scope.loadingVenuesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingVenues = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingVenuesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTeams = function () {

		$scope.loadingEvents = true;
		$scope.loadingEventsAlert = false;

		Team.getTeams().then(function (res) {

			$scope.loadingEvents = false;

			if (angular.isObject(res.data.response)) {

				$scope.teams = res.data.response;
				
			} else {

				$scope.loadingEventsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvents = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getMeasurements = function () {

		$scope.loadingMeasurements = true;
		$scope.loadingMeasurementsAlert = false;

		Measurement.getMeasurements().then(function (res) {

			$scope.loadingMeasurements = false;

			if (angular.isObject(res.data.response)) {

				$scope.measurements = res.data.response;
				
			} else {

				$scope.loadingMeasurementsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingMeasurements = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingMeasurementsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getStationsAbleToConfigure = function () {

		$scope.loadingStationsAbleToConfigure = true;
		$scope.loadingStationsAbleToConfigureAlert = false;

		Station.getStationsAbleToConfigure().then(function (res) {

			$scope.loadingStationsAbleToConfigure = false;

			if (angular.isObject(res.data.response)) {

				$scope.stations = res.data.response;
				
			} else {

				$scope.loadingStationsAbleToConfigureAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStationsAbleToConfigure = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStationsAbleToConfigureAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAllStationConfigurations = function () {

		$scope.loadingConfigurations = true;
		$scope.loadingConfigurationsAlert = false;

		Station.getConfigurations().then(function (res) {

			$scope.loadingConfigurations = false;

			if (angular.isObject(res.data.response)) {

				$scope.configurations = res.data.response;
				$scope.$emit('configurationsLoaded');

			} else {

				$scope.loadingConfigurationsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingConfigurations = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingConfigurationsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;

				$scope.formdata.name = $scope.event.tx_name;
				$scope.formdata.date = $filter('date')($scope.event.dt_event, 'MM/dd/yyyy');
				$scope.formdata.venue = $scope.event.id_venue;
				$scope.formdata.team = $scope.event.id_team;
				$scope.formdata.measurement_system = $scope.event.id_measurement;
		
				angular.forEach($scope.event.configurations, function (value, key) {

					$scope.formdata.configurations.push({
						id: value.id_configuration,
						station: value.station,
						test: value.test,
						age_range: value.nu_min_age + ' - ' + value.nu_max_age,
						distance: value.nu_distance
					});

				});

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getVenues();
	$scope.getTeams();
	$scope.getMeasurements();
	$scope.getStationsAbleToConfigure();
	$scope.getAllStationConfigurations();
	$scope.getEvent(id_uniq);

	$scope.$on('configurationsLoaded', function () {

		$scope.$watch('configurationdata.station', function () {

			$scope.tests = [];

			$scope.configurationdata.test = '';
			$scope.configurationdata.age_range = '';
			$scope.configurationdata.distance = '0';

			$scope.ConfigurationForm.test.$setPristine();
			$scope.ConfigurationForm.age_range.$setPristine();
			
			angular.forEach($scope.configurations, function (value, key) {

				if (value.id_station == $scope.configurationdata.station) {

					if (_.findIndex($scope.tests, { id: value.id_test, name: value.test_name }) == -1) {

						$scope.tests.push({
							id: value.id_test,
							name: value.test_name
						});	

					};

				};

			});

		});

		$scope.$watch('configurationdata.test', function () {

			$scope.ages = [];

			$scope.configurationdata.age_range = '';
			$scope.configurationdata.distance = '0';

			$scope.ConfigurationForm.age_range.$setPristine();
			
			angular.forEach($scope.configurations, function (value, key) {

				if (value.id_station == $scope.configurationdata.station && value.id_test == $scope.configurationdata.test) {

					if (_.indexOf($scope.ages, value.nu_age) == -1) {

						if (_.findIndex($scope.ages, { id: value.id_age_range, name: value.nu_min_age + ' - ' + value.nu_max_age }) == -1) {

							$scope.ages.push({
								id: value.id_age_range,
								name: value.nu_min_age + ' - ' + value.nu_max_age
							});	

						};


					};

				};

			});

		});

		$scope.$watch('configurationdata.age_range', function () {

			angular.forEach($scope.configurations, function (value, key) {

				if (value.id_station == $scope.configurationdata.station && value.id_test == $scope.configurationdata.test && value.id_age_range == $scope.configurationdata.age_range) {

					$scope.configurationdata.distance = value.nu_distance;

				};

			});

		});

	});

	$scope.addConfiguration = function () {

		$scope.showTooltip = false;

		var indexToLookFor = {
			id_station: $scope.configurationdata.station,
			id_test: $scope.configurationdata.test,
			id_age_range: $scope.configurationdata.age_range,
			nu_distance: $scope.configurationdata.distance
		};

		var index = _.findIndex($scope.configurations, indexToLookFor);

		var dataToPush = {
			id: $scope.configurations[index].id,
			station: $scope.configurations[index].station_name,
			test: $scope.configurations[index].test_name,
			age_range: $scope.configurations[index].nu_min_age + ' - ' + $scope.configurations[index].nu_max_age,
			distance: $scope.configurations[index].nu_distance
		};

		if (_.findIndex($scope.formdata.configurations, dataToPush) == -1) {

			$scope.formdata.configurations.push(dataToPush);

			$scope.configurationdata = {
				station: '',
				test: '',
				age_range: '',
				distance: '0'
			};

			$scope.ConfigurationForm.$setPristine();

		} else {

			$scope.showTooltip = true;

			$timeout(function () {

				$scope.showTooltip = false;				

			}, 2000);

		}

	};

	$scope.deleteConfiguration = function (index) {

		$scope.formdata.configurations.splice(index, 1);

	};

	$scope.editEvent = function () {

		$scope.editingEvent = true;
		$scope.editingEventAlert = false;

		var data = $.param($scope.formdata);

		Event.edit(id_uniq, data).then(function (res) {

			$scope.editingEvent = false;

			if (res.data.status == "success") {

				STFcache.delete('events');
				STFcache.delete('events' + id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Event updated correctly'
				};

				$location.path('events');

			} else {

				$scope.editingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.editingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);
var stf = angular.module('HeadingControllers', []);

stf.controller('HeadingListController', ['$scope', '$routeParams', '$location', 'Event', 'Heading', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, $location, Event, Heading, Access, Auth, STFcache) {Access, 
	
	$scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};


	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;
				if (_.findIndex($scope.event.configurations, { station: 'Heading' }) >= 0) {

 					$scope.$emit('eventLoaded');
				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getHeadings = function (id_uniq, refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('headingEvent' + $scope.id_uniq);
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.measurements = null;
		$scope.loadingHeadings = true;
		$scope.loadingHeadingsAlert = false;

		Heading.getAll($scope.id_uniq).then(function (res) {
			$scope.loadingHeadings = false;
			if (angular.isObject(res.data.response)) {
				$scope.measurements = res.data.response;
			} else {
				$scope.loadingHeadingsAlert = {
					type: 'success',
					message: res.data.response
				};
			}
			$scope.getAccess(1);
		}, function (err) {

			$scope.loadingHeadings = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingHeadingsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.deletePlayerEvent = function( player_id, ind ){

		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:player_id,type:'delete'});
			Heading.delete(data).then(function (res) {
				$scope.loadingHeadings = false;
				$scope.getHeadings($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingHeadingsAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingHeadings = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingHeadingsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};

	$scope.bulkDeletePlayerEvent = function(){
         console.log(values);
		action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:values,type:'delete',action:action_id});
			Heading.multidelete(data).then(function (res) {
				$scope.loadingHeadings = false;
				$scope.getHeadings($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingHeadingsAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingHeadings = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingHeadingsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one user to delete.");
	 }
	};

	$scope.updatePlayerEvent = function( player_id ){
		var data = $.param({id_player:player_id,type:'update'});
		Heading.updateForce(data).then(function (res) {

			$scope.loadingHeadings = false;
			if (angular.isObject(res.data.response)) {

				//$scope.measurements = res.data.response;

			} else {
				$scope.loadingHeadingsAlert = {type: 'success',message: res.data.response};
			}

		}, function (err) {

			$scope.loadingHeadings = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingHeadingsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };

			}

		});
	}

	$scope.getEvent($scope.id_uniq);
	$scope.getHeadings($scope.id_uniq);

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}
	
	/*$scope.orderOptions = [
		{ value: 'name', text: 'Name' },
		{ value: '-date', text: 'Date' }
	];*/
	
}]);

stf.controller('HeadingCreateController', ['$scope', '$routeParams', '$location', '$rootScope', '$filter', 'Event', 'Player', 'Heading', 'Anthropometric', 'Auth', 'STFcache', function ($scope, $routeParams, $location, $rootScope, $filter, Event, Player, Heading, Anthropometric, Auth, STFcache) {
	

    

	$scope.id_uniq = $routeParams.id_uniq;
	$scope.player = null;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	var measurement = {
		"MKS" : {
			"speed" : "m/s",
			"distance" : "mts"
		},
		"YLS" : {
			"speed" : "y/s",
			"distance" : "ft"
		}
	};
	
	/* AIM TEST CODE */

	$scope.aimTest = false;
	$scope.HeadAimFieldOneValidated = false;
	$scope.HeadAimFieldTwoValidated = false;
	$scope.HeadAimFieldThreeValidated = false;
	$scope.HeadAimFieldFourValidated = false;
	$scope.HeadAimFieldFiveValidated = false;
	$scope.HeadAimFieldSixValidated = false;

	$scope.aimdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		request_one: 0,
		destination_one: '',
		request_two: 0,
		destination_two: '',
		request_three: 0,
		destination_three: '',
		request_four: 0,
		destination_four: '',
		request_five: 0,
		destination_five: '',
		request_six: 0,
		destination_six: ''
	};

  $scope.getHeadingMeasurement = function( type, dim ){

		return measurement[type][dim];
	};
	$scope.startAimTest = function () { 
         
       

		$scope.aimTest = true; 
		$scope.aimdata.request_one = Math.floor((Math.random() * 24) + 1);
         //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    if (String.fromCharCode(evt.which) == "e"){
        	return false;
            }
            if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length >1){ 
            
                this.value = parseFloat($(this).val().slice(0, -1));
            }  
         }    

			});


          
	
	};

	$scope.validateHeadAimField = function (field) {

		switch (field) {

			case 1: 
				$scope.AimFieldOneValidated = true;
				$scope.aimdata.request_two = Math.floor((Math.random() * 24) + 1);	
				break;
			case 2:
				$scope.AimFieldTwoValidated = true;
				$scope.aimdata.request_three = Math.floor((Math.random() * 24) + 1);
				break;
			case 3:
				$scope.AimFieldThreeValidated = true;
				$scope.aimdata.request_four = Math.floor((Math.random() * 24) + 1);
				break;
			case 4:
				$scope.AimFieldFourValidated = true;
				$scope.aimdata.request_five = Math.floor((Math.random() * 24) + 1);
				break;
			case 5:
				$scope.AimFieldFiveValidated = true;
				$scope.aimdata.request_six = Math.floor((Math.random() * 24) + 1);
				break;
			case 6:
				$scope.AimFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	/* END AIM TEST CODE */

	/* JUMP TEST CODE */

	$scope.jumpTest = false;
	$scope.HeadJumpFieldOneValidated = false;
	$scope.HeadJumpFieldTwoValidated = false;
	$scope.HeadJumpFieldThreeValidated = false;
	


	console.log($filter('date')(new Date(), 'MM/dd/yyyy'));
	$scope.jumpdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		extended_arm_height: '',
		jump_high_one: '',
		jump_result_one: '',
		jump_high_two: '',
		jump_result_two: '',
		jump_high_three: '',
		jump_result_three: ''
	};

	$scope.startJumpTest = function () {
        

		$scope.jumpTest = true;

	};

	$scope.setHeightWithExtendedArm = function () {
        
		$scope.extendedArmHeightSet = true;

	};

	$scope.validateHeadJumpField = function (field) {

		switch (field) {

			case 1:
				$scope.HeadJumpFieldOneValidated = true;
				var weight = parseFloat($scope.weight);
				var heightWithExtendedArm = parseFloat($scope.jumpdata.extended_arm_height);
				var height = parseFloat($scope.jumpdata.jump_high_one);
				var difference = height - heightWithExtendedArm;
				var result = weight * Math.pow((4.9 * difference), 2);
				$scope.jumpdata.jump_result_one = result.toFixed(2);
				break;
			case 2:
				$scope.HeadJumpFieldTwoValidated = true;
				var weight = parseFloat($scope.weight);
				var heightWithExtendedArm = parseFloat($scope.jumpdata.extended_arm_height);
				var height = parseFloat($scope.jumpdata.jump_high_two);
				var difference = height - heightWithExtendedArm;
				var result = weight * Math.pow((4.9 * difference), 2);
				$scope.jumpdata.jump_result_two = result.toFixed(2);
				break;
			case 3:
				$scope.HeadJumpFieldThreeValidated = true;
				$scope.HeadJumpFieldTwoValidated = true;
				var weight = parseFloat($scope.weight);
				var heightWithExtendedArm = parseFloat($scope.jumpdata.extended_arm_height);
				var height = parseFloat($scope.jumpdata.jump_high_three);
				var difference = height - heightWithExtendedArm;
				var result = weight * Math.pow((4.9 * difference), 2);
				$scope.jumpdata.jump_result_three = result.toFixed(2);
				$scope.HeadingJumpFormCompleted = true;
				break;
			default:
				break;

		}

	};
	/* END JUMP TEST CODE */


	/* FORCE TEST CODE */
	$scope.forcedata = {
		id_player: '',
		id_event: '',
		measurement_date : $filter('date')(new Date(), 'MM/dd/yyyy'),
		distance: [ '', '', '' ],
		attempt : [ '', '', '' ],
		result  : [ '', '', '' ]
	};

	$scope.headForcedata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		distance_one: '',
		distance_two: '',
		distance_three: '',
		attempt_one : '',
		attempt_two : '',
		attempt_three : '',
		result_one : '',
		result_two : '',
		result_three : ''
	};


	$scope.forceFieldValidated = [ false, false, false ];

	$scope.startForceTest = function () { 

		return $scope.forceTest = true; 
	};

	$scope.validateForceField = function (field) {
		$scope.forceFieldValidated[field] = true;

		var velocityInitial = 0;
		var velocityFinal   = parseFloat( $scope.forcedata.attempt[field] );
		var distance        = $scope.forcedata.distance[field];
		var mass            = $scope.ball_weight / 1000;
		var force           = mass * ( ( Math.pow( velocityFinal, 2 ) - velocityInitial ) / ( 2 * distance ) );

		$scope.forcedata.result[field] = force.toFixed(2);

		if( field === 2 ) $scope.CreateHeadingForceFormCompleted = true;
	};

	/* END FORCE TEST CODE */

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {
     
			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;
                
           

               if (_.findIndex($scope.event.configurations, { station: 'Heading' }) >= 0) {

 					$scope.aimdata.id_event = $scope.event.id;
 					$scope.forcedata.id_event = $scope.event.id;
 					$scope.jumpdata.id_event = $scope.event.id;

 					$scope.eventHasHeadingJump = (_.findIndex($scope.event.configurations, { station: 'Heading', test: 'Jump' }) >= 0) ? true : false;
 					$scope.eventHasHeadingForce = (_.findIndex($scope.event.configurations, { station: 'Heading', test: 'Force' }) >= 0) ? true : false;
 					$scope.eventHasHeadingAim = (_.findIndex($scope.event.configurations, { station: 'Heading', test: 'Aim' }) >= 0) ? true : false;

 				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {

		console.log(id_uniq);

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {
			console.log(res);
             
			if (angular.isObject(res.data.response)) {

				$scope.aimdata.id_player = res.data.response.id;
				$scope.forcedata.id_player = res.data.response.id;
				$scope.jumpdata.id_player = res.data.response.id;

				return res.data.response;
               
			} else {

				$scope.loadingPlayer = false;

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		}).then(function (args) {

			if (args != undefined) {

				var player = args;
				var data = [];
				var forceAgeRangeData = [];
				var aimAgeRangeData = [];
				var jumpAgeRangeData = [];

				Anthropometric.getWeight(player.id, $scope.event.id).then(function (res) {

					if (angular.isObject(res.data.response)) {

						return res.data.response;

					} else {

						$scope.loadingPlayer = false;

						$scope.loadingPlayerAlert = {
							type: 'success',
							message: res.data.response
						};

					}

				}, function (err) {

					$scope.loadingPlayer = false;

					$scope.loadingPlayerAlert = {
						type: 'danger',
						message: err.status + ' ' + err.statusText
					};

				}).then(function (args) {

					if (args !== undefined) {

						$scope.weight = args.nu_weight;

						console.log($scope.weight);


						for (var i = 0; i < $scope.event.configurations.length; i++) {
					
							if ($scope.event.configurations[i].station == "Heading") {

								if (_.findIndex(data, { value: $scope.event.configurations[i].test }) == -1) {

									data.push({
										name: 'tests[]',
										value: $scope.event.configurations[i].test
									});

								};

								if ($scope.event.configurations[i].test == "Force") {

									forceAgeRangeData.push({
										test: $scope.event.configurations[i].test,
										min: $scope.event.configurations[i].nu_min_age,
										max: $scope.event.configurations[i].nu_max_age,
										ball_weight: $scope.event.configurations[i].nu_ball_weight
									});	

								};

								if ($scope.event.configurations[i].test == "Aim") {

									aimAgeRangeData.push({
										test: $scope.event.configurations[i].test,
										min: $scope.event.configurations[i].nu_min_age,
										max: $scope.event.configurations[i].nu_max_age,
										ball_weight: $scope.event.configurations[i].nu_ball_weight
									});	

								};

								if ($scope.event.configurations[i].test == "Jump") {

									jumpAgeRangeData.push({
										test: $scope.event.configurations[i].test,
										min: $scope.event.configurations[i].nu_min_age,
										max: $scope.event.configurations[i].nu_max_age,
										ball_weight: $scope.event.configurations[i].nu_ball_weight
									});	

								};

							};

						};

						data = $.param(data);

						Heading.check(player.id, $scope.event.id, data).then(function (res) {

							$scope.loadingPlayer = false;

							if (res.data.player_exists == 1) {

								$scope.loadingPlayerAlert = {
									type: 'success',
									message: res.data.response
								};

							} else {

								var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));

								$scope.playerCanPerformAimTest = false;
								$scope.playerCanPerformForceTest = false;
								$scope.playerCanPerformJumpTest = false;

								if (aimAgeRangeData.length > 0) {

									for (var i = 0; i < aimAgeRangeData.length; i++) {

										var min = parseInt(aimAgeRangeData[i].min);
										var max = parseInt(aimAgeRangeData[i].max);

										if (playerAge >= min && playerAge <= max) {

											$scope.playerCanPerformAimTest = true;
											break;

										};

									};

								};

								if (forceAgeRangeData.length > 0) {

									for (var i = 0; i < forceAgeRangeData.length; i++) {

										var min = parseInt(forceAgeRangeData[i].min);
										var max = parseInt(forceAgeRangeData[i].max);

										if (playerAge >= min && playerAge <= max) {

											$scope.playerCanPerformForceTest = true;
											$scope.ball_weight = parseInt(forceAgeRangeData[i].ball_weight);
											break;

										};

									};

								};

								if (jumpAgeRangeData.length > 0) {

									for (var i = 0; i < jumpAgeRangeData.length; i++) {

										var min = parseInt(jumpAgeRangeData[i].min);
										var max = parseInt(jumpAgeRangeData[i].max);

										if (playerAge >= min && playerAge <= max) {

											$scope.playerCanPerformJumpTest = true;
											$scope.ball_weight = parseInt(jumpAgeRangeData[i].ball_weight);
											break;

										};

									};

								};

								$scope.player = player;

								$scope.AimTestAlert = ($scope.playerCanPerformAimTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
								$scope.ForceTestAlert = ($scope.playerCanPerformForceTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
								$scope.JumpTestAlert = ($scope.playerCanPerformJumpTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;

								$scope.showAim = (res.data.response.aim == 2) ? true : false;
								$scope.showForce = (res.data.response.force == 2) ? true : false;
								$scope.showJump = (res.data.response.jump == 2) ? true : false;

							}

						}, function (err) {

							$scope.loadingPlayer = false;

							$scope.loadingPlayerAlert = {
								type: 'danger',
								message: res.data.response
							};

						});	

					};	

				});

			};

		});

	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$scope.aimdata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			request_one: '',
			destination_one: '',
			request_two: '',
			destination_two: '',
			request_three: '',
			destination_three: '',
			request_four: '',
			destination_four: '',
			request_five: '',
			destination_five: '',
			request_six: '',
			destination_six: ''
		};

		$scope.forcedata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			distance_one: '',
			distance_two: '',
			distance_three: ''
		};

		$scope.jumpdata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			jump_high_one: '',
			jump_high_two: '',
			jump_high_three: ''
		};
				
		$scope.CreateHeadingAimForm.$setPristine();
		$scope.CreateHeadingForceForm.$setPristine();
		$scope.CreateHeadingJumpForm.$setPristine();

	};

	$scope.getEvent($scope.id_uniq);

	$scope.createHeadingAim = function () {

		$scope.creatingHeadingAim = true;
		$scope.creatingHeadingAimAlert = false;

		var data = $.param($scope.aimdata);

		Heading.createAim(data).then(function (res) {

			$scope.creatingHeadingAim = false;

			if (res.data.status == "success") {
                
				STFcache.delete('headingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/heading');

			} else {

				$scope.creatingHeadingAimAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingHeadingAim = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingHeadingAimAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.setForceData = function( source ){
		$scope.headForcedata.id_player        = source.id_player;
		$scope.headForcedata.id_event         = source.id_event;
		$scope.headForcedata.measurement_date = source.measurement_date; 
		$scope.headForcedata.distance_one     = source.distance[0]; 
		$scope.headForcedata.distance_two     = source.distance[1]; 
		$scope.headForcedata.distance_three   = source.distance[2]; 
		$scope.headForcedata.attempt_one      = source.attempt[0]; 
		$scope.headForcedata.attempt_two      = source.attempt[1]; 
		$scope.headForcedata.attempt_three    = source.attempt[2];
	}

	$scope.createHeadingForce = function () {

		$scope.creatingHeadingForce = true;
		$scope.creatingHeadingForceAlert = false;

		$scope.setForceData( $scope.forcedata );

		var data = $.param($scope.headForcedata);

		Heading.createForce(data).then(function (res) {

			$scope.creatingHeadingForce = false;
            
			if (res.data.status == "success") {
                
				STFcache.delete('headingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/heading');

			} else {

				$scope.creatingHeadingForceAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingHeadingForce = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingHeadingForceAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.createHeadingJump = function () {

		$scope.creatingHeadingJump = true;
		$scope.creatingHeadingJumpAlert = false;

		var data = $.param($scope.jumpdata);

		Heading.createJump(data).then(function (res) {

			$scope.creatingHeadingJump = false;
               
			if (res.data.status == "success") {

				STFcache.delete('headingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/heading');

			} else {

				$scope.creatingHeadingJumpAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingHeadingJump = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingHeadingJumpAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};
	
}]);

stf.controller('HeadingEditController', ['$scope', '$routeParams', '$location', '$rootScope', '$filter', 'Event', 'Player', 'Heading', 'Anthropometric', 'Auth', 'STFcache', function ($scope, $routeParams, $location, $rootScope, $filter, Event, Player, Heading, Anthropometric, Auth, STFcache) {
	//console.log($routeParams);
	
	$scope.id_uniq = $routeParams.id;
	$scope.player_id_uniq = $routeParams.playerid;
	$scope.player = null;
  
 
  
	var measurement = {
		"MKS" : {
			"speed" : "m/s",
			"distance" : "mts"
		},
		"YLS" : {
			"speed" : "y/s",
			"distance" : "ft"
		}
	};

	console.log($scope.id_uniq,$scope.player_id_uniq);

	/* AIM TEST CODE */

	$scope.aimTest = false;
	$scope.HeadAimFieldOneValidated = false;
	$scope.HeadAimFieldTwoValidated = false;
	$scope.HeadAimFieldThreeValidated = false;
	$scope.HeadAimFieldFourValidated = false;
	$scope.HeadAimFieldFiveValidated = false;
	$scope.HeadAimFieldSixValidated = false;

	$scope.aimdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		request_one: 0,
		destination_one: '',
		request_two: 0,
		destination_two: '',
		request_three: 0,
		destination_three: '',
		request_four: 0,
		destination_four: '',
		request_five: 0,
		destination_five: '',
		request_six: 0,
		destination_six: ''
	};

	$scope.getHeadingMeasurement = function( type, dim ){

		return measurement[type][dim];
	};

	$scope.startAimTest = function () { 

		$scope.aimTest = true; 
		$scope.aimdata.request_one = Math.floor((Math.random() * 24) + 1);
        
           //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    if (String.fromCharCode(evt.which) == "e"){
        	return false;
            }
            if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length >1){ 
              //alert($(this).val().split(".")[1].length);
             // $(this).val()
            // alert("gy");
                //if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat($(this).val().slice(0, -1));
            }  
         }    

			});
	};

	$scope.validateHeadAimField = function (field) {

		switch (field) {

			case 1: 
				$scope.AimFieldOneValidated = true;
				$scope.aimdata.request_two = Math.floor((Math.random() * 24) + 1);	
				break;
			case 2:
				$scope.AimFieldTwoValidated = true;
				$scope.aimdata.request_three = Math.floor((Math.random() * 24) + 1);
				break;
			case 3:
				$scope.AimFieldThreeValidated = true;
				$scope.aimdata.request_four = Math.floor((Math.random() * 24) + 1);
				break;
			case 4:
				$scope.AimFieldFourValidated = true;
				$scope.aimdata.request_five = Math.floor((Math.random() * 24) + 1);
				break;
			case 5:
				$scope.AimFieldFiveValidated = true;
				$scope.aimdata.request_six = Math.floor((Math.random() * 24) + 1);
				break;
			case 6:
				$scope.AimFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	/* END AIM TEST CODE */

	/* JUMP TEST CODE */

	$scope.jumpTest = false;
	$scope.HeadJumpFieldOneValidated = false;
	$scope.HeadJumpFieldTwoValidated = false;
	$scope.HeadJumpFieldThreeValidated = false;
	
	$scope.jumpdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		extended_arm_height: '',
		jump_high_one: '',
		jump_result_one: '',
		jump_high_two: '',
		jump_result_two: '',
		jump_high_three: '',
		jump_result_three: ''
	};

	$scope.startJumpTest = function () {

		$scope.jumpTest = true;

	};

	$scope.setHeightWithExtendedArm = function () {

		$scope.extendedArmHeightSet = true;

	};

	$scope.validateHeadJumpField = function (field) {

		switch (field) {

			case 1:
				$scope.HeadJumpFieldOneValidated = true;
				var weight = parseFloat($scope.weight);
				var heightWithExtendedArm = parseFloat($scope.jumpdata.extended_arm_height);
				var height = parseFloat($scope.jumpdata.jump_high_one);
				var difference = height - heightWithExtendedArm;
				var result = weight * Math.pow((4.9 * difference), 2);
				$scope.jumpdata.jump_result_one = result.toFixed(2);
				break;
			case 2:
				$scope.HeadJumpFieldTwoValidated = true;
				var weight = parseFloat($scope.weight);
				var heightWithExtendedArm = parseFloat($scope.jumpdata.extended_arm_height);
				var height = parseFloat($scope.jumpdata.jump_high_two);
				var difference = height - heightWithExtendedArm;
				var result = weight * Math.pow((4.9 * difference), 2);
				$scope.jumpdata.jump_result_two = result.toFixed(2);
				break;
			case 3:
				$scope.HeadJumpFieldThreeValidated = true;
				$scope.HeadJumpFieldTwoValidated = true;
				var weight = parseFloat($scope.weight);
				var heightWithExtendedArm = parseFloat($scope.jumpdata.extended_arm_height);
				var height = parseFloat($scope.jumpdata.jump_high_three);
				var difference = height - heightWithExtendedArm;
				var result = weight * Math.pow((4.9 * difference), 2);
				$scope.jumpdata.jump_result_three = result.toFixed(2);
				$scope.HeadingJumpFormCompleted = true;
				break;
			default:
				break;

		}

	};
	/* END JUMP TEST CODE */


	/* FORCE TEST CODE */
	$scope.forcedata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		distance: [ '', '', '' ],
		attempt : [ '', '', '' ],
		result  : [ '', '', '' ]
	};

	$scope.headForcedata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		distance_one: '',
		distance_two: '',
		distance_three: '',
		attempt_one : '',
		attempt_two : '',
		attempt_three : '',
		result_one : '',
		result_two : '',
		result_three : ''
	};


	$scope.forceFieldValidated = [ false, false, false ];

	$scope.startForceTest = function () { 
		return $scope.forceTest = true; 

	};

	$scope.validateForceField = function (field) {
		$scope.forceFieldValidated[field] = true;

		var velocityInitial = 0;
		var velocityFinal   = parseFloat( $scope.forcedata.attempt[field] );
		var distance        = $scope.forcedata.distance[field];
		var mass            = $scope.ball_weight / 1000;
		var force           = mass * ( ( Math.pow( velocityFinal, 2 ) - velocityInitial ) / ( 2 * distance ) );

		$scope.forcedata.result[field] = force.toFixed(2);

		if( field === 2 ) $scope.CreateHeadingForceFormCompleted = true;
	};

	/* END FORCE TEST CODE */

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;


				console.log("Hello>>>>",$scope.event);
             //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    if (String.fromCharCode(evt.which) == "e"){
        	return false;
            }
            if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length >1){ 
              //alert($(this).val().split(".")[1].length);
             // $(this).val()
            // alert("gy");
                //if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat($(this).val().slice(0, -1));
            }  
         }    

			});
         
          	angular.element('.onlynumbertext').on("keypress",function (evt) {
    	    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;

			});

				if (_.findIndex($scope.event.configurations, { station: 'Heading' }) >= 0) {

 					$scope.aimdata.id_event = $scope.event.id;
 					$scope.forcedata.id_event = $scope.event.id;
 					$scope.jumpdata.id_event = $scope.event.id;

 					$scope.eventHasHeadingJump = (_.findIndex($scope.event.configurations, { station: 'Heading', test: 'Jump' }) >= 0) ? true : false;
 					$scope.eventHasHeadingForce = (_.findIndex($scope.event.configurations, { station: 'Heading', test: 'Force' }) >= 0) ? true : false;
 					$scope.eventHasHeadingAim = (_.findIndex($scope.event.configurations, { station: 'Heading', test: 'Aim' }) >= 0) ? true : false;
 					$scope.getPlayer($scope.player_id_uniq);
 				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {

		console.log(">>>>>Hiii",$scope.event);
		//console.log(id_uniq);

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			if (angular.isObject(res.data.response)) {

				$scope.aimdata.id_player = res.data.response.id;
				$scope.forcedata.id_player = res.data.response.id;
				$scope.jumpdata.id_player = res.data.response.id;

				return res.data.response;

			} else {

				$scope.loadingPlayer = false;

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		}).then(function (args) {

			if (args != undefined) {

				var player = args;
				var data = [];
				var forceAgeRangeData = [];
				var aimAgeRangeData = [];
				var jumpAgeRangeData = [];

				Anthropometric.getWeight(player.id, $scope.event.id).then(function (res) {

					if (angular.isObject(res.data.response)) {

						return res.data.response;

					} else {

						$scope.loadingPlayer = false;

						$scope.loadingPlayerAlert = {
							type: 'success',
							message: res.data.response
						};

					}

				}, function (err) {

					$scope.loadingPlayer = false;

					$scope.loadingPlayerAlert = {
						type: 'danger',
						message: err.status + ' ' + err.statusText
					};

				}).then(function (args) {
					console.log(args);
					if (args !== undefined) {

						$scope.weight = args.nu_weight;

						for (var i = 0; i < $scope.event.configurations.length; i++) {
					
							if ($scope.event.configurations[i].station == "Heading") {

								if (_.findIndex(data, { value: $scope.event.configurations[i].test }) == -1) {

									data.push({
										name: 'tests[]',
										value: $scope.event.configurations[i].test
									});

								};

								if ($scope.event.configurations[i].test == "Force") {

									forceAgeRangeData.push({
										test: $scope.event.configurations[i].test,
										min: $scope.event.configurations[i].nu_min_age,
										max: $scope.event.configurations[i].nu_max_age,
										ball_weight: $scope.event.configurations[i].nu_ball_weight
									});	

								};

								if ($scope.event.configurations[i].test == "Aim") {

									aimAgeRangeData.push({
										test: $scope.event.configurations[i].test,
										min: $scope.event.configurations[i].nu_min_age,
										max: $scope.event.configurations[i].nu_max_age,
										ball_weight: $scope.event.configurations[i].nu_ball_weight
									});	

								};

								if ($scope.event.configurations[i].test == "Jump") {

									jumpAgeRangeData.push({
										test: $scope.event.configurations[i].test,
										min: $scope.event.configurations[i].nu_min_age,
										max: $scope.event.configurations[i].nu_max_age,
										ball_weight: $scope.event.configurations[i].nu_ball_weight
									});	

								};

							};

						};

						data = $.param(data);

						var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));

						$scope.playerCanPerformAimTest = false;
						$scope.playerCanPerformForceTest = false;
						$scope.playerCanPerformJumpTest = false;

						if (aimAgeRangeData.length > 0) {

							for (var i = 0; i < aimAgeRangeData.length; i++) {

								var min = parseInt(aimAgeRangeData[i].min);
								var max = parseInt(aimAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformAimTest = true;
									break;

								};

							};

						};

						if (forceAgeRangeData.length > 0) {

							for (var i = 0; i < forceAgeRangeData.length; i++) {

								var min = parseInt(forceAgeRangeData[i].min);
								var max = parseInt(forceAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformForceTest = true;
									$scope.ball_weight = parseInt(forceAgeRangeData[i].ball_weight);
									break;

								};

							};

						};

						if (jumpAgeRangeData.length > 0) {

							for (var i = 0; i < jumpAgeRangeData.length; i++) {

								var min = parseInt(jumpAgeRangeData[i].min);
								var max = parseInt(jumpAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformJumpTest = true;
									$scope.ball_weight = parseInt(jumpAgeRangeData[i].ball_weight);
									break;

								};

							};

						};

						$scope.player = player;
						//console.log(">>>Hey",player);
						/*$scope.AimTestAlert = ($scope.playerCanPerformAimTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
						$scope.ForceTestAlert = ($scope.playerCanPerformForceTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
						$scope.JumpTestAlert = ($scope.playerCanPerformJumpTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;*/

						/*$scope.showAim = (res.data.response.aim == 2) ? true : false;
						$scope.showForce = (res.data.response.force == 2) ? true : false;
						$scope.showJump = (res.data.response.jump == 2) ? true : false;*/

						Heading.getDetails($scope.id_uniq,$scope.player_id_uniq).then(function (res) {

							$scope.loadingEvent = false;

							if (angular.isObject(res.data.response)) {
                                console.log(res.data.response);
								//if( $scope.event ){
                                if(res.data.response.forceStat==1){
								$scope.forcedata.id_player = res.data.response.forcePlayerID;
								$scope.forcedata.id_event = res.data.response.forceEvtID;
								$scope.forcedata.measurement_date = res.data.response.forceMeasurementDt;
								$scope.forcedata.distance[0] = parseFloat(res.data.response.forceDisOne);
								$scope.forcedata.distance[1] = parseFloat(res.data.response.forceDisTwo);
								$scope.forcedata.distance[2] = parseFloat(res.data.response.forceDisThree);

								$scope.forcedata.attempt[0] = parseFloat(res.data.response.forceAttemptOne);
								$scope.forcedata.attempt[1] = parseFloat(res.data.response.forceAttemptTwo);
								$scope.forcedata.attempt[2] = parseFloat(res.data.response.forceAttemptThree);

								$scope.forcedata.result[0] = $scope.calculateForce(0);
								$scope.forcedata.result[1] = $scope.calculateForce(1);
								$scope.forcedata.result[2] = $scope.calculateForce(2);
                                }
                                else{
                                	$scope.forcedata.measurement_date=$filter('date')(new Date(), 'MM/dd/yyyy');
                                   
                                }
                                //added by sg//
                                if(res.data.response.aimStat==1){
                                $scope.aimdata.id_player = res.data.response.aimPlayerID;
								$scope.aimdata.id_event = res.data.response.aimEvtID;
								$scope.aimdata.measurement_date = res.data.response.aimMeasurementDt;
								$scope.aimdata.request_one=parseFloat(res.data.response.aimReqOne);
								$scope.aimdata.request_two=parseFloat(res.data.response.aimReqTwo);
								$scope.aimdata.request_three=parseFloat(res.data.response.aimReqThree);
								$scope.aimdata.request_four=parseFloat(res.data.response.aimReqFour);
								$scope.aimdata.request_five=parseFloat(res.data.response.aimReqFive);
								$scope.aimdata.request_six=parseFloat(res.data.response.aimReqSix);
								$scope.aimdata.destination_one=parseFloat(res.data.response.aimDesOne);
                                $scope.aimdata.destination_two=parseFloat(res.data.response.aimDesTwo);
                                $scope.aimdata.destination_three=parseFloat(res.data.response.aimDesThree);
                                $scope.aimdata.destination_four=parseFloat(res.data.response.aimDesFour);
                                $scope.aimdata.destination_five=parseFloat(res.data.response.aimDesFive);
                                $scope.aimdata.destination_six=parseFloat(res.data.response.aimDesSix);
								}
								else{
									$scope.aimdata.measurement_date=$filter('date')(new Date(), 'MM/dd/yyyy');
								}
                                
                                if(res.data.response.jumpStat==1){
								$scope.jumpdata.id_player = res.data.response.jumpPlayerID;
								$scope.jumpdata.id_event = res.data.response.jumpEvtID;
								$scope.jumpdata.measurement_date = res.data.response.jumpMeasurementDt;
								$scope.jumpdata.extended_arm_height=parseFloat(res.data.response.jumpArmHeight);
								$scope.jumpdata.jump_high_one=parseFloat(res.data.response.jumpHighOne);
								$scope.jumpdata.jump_high_two=parseFloat(res.data.response.jumpHighTwo);
								$scope.jumpdata.jump_high_three=parseFloat(res.data.response.jumpHighThree);
								}
								else{
									$scope.jumpdata.measurement_date=$filter('date')(new Date(), 'MM/dd/yyyy');
								}
								//added by sg end//
								

								//$scope.forcedata.result[2] = res.data.response.forceAttemptOne;

								/*$scope.headForcedata = {
									id_player: res.data.response.forcePlayerID,
									id_event: res.data.response.forceEvtID,
									measurement_date: res.data.response.forceMeasurementDt,
									distance_one: parseFloat(res.data.response.forceDisOne),
									distance_two: parseFloat(res.data.response.forceDisTwo),
									distance_three: parseFloat(res.data.response.forceDisThree),
									attempt_one : parseFloat(res.data.response.forceAttemptOne),
									attempt_two : parseFloat(res.data.response.forceAttemptTwo),
									attempt_three : parseFloat(res.data.response.forceAttemptThree),
									result_one : parseFloat($scope.calculateForce(0)),
									result_two : parseFloat($scope.calculateForce(1)),
									result_three : parseFloat($scope.calculateForce(2))
								};*/
							} else {
								$scope.loadingEventAlert = {
									type: 'success',
									message: res.data.response
								};
							}

						}, function (err) {
							$scope.loadingEvent = false;

							if (err.status == 401) {

								Auth.deleteToken();

							} else {

								$scope.loadingEventAlert = {
									type: 'danger',
									message: err.status + ' ' + err.statusText
								};

							}

						});

					};	

				});

			};

		});

	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$scope.aimdata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			request_one: '',
			destination_one: '',
			request_two: '',
			destination_two: '',
			request_three: '',
			destination_three: '',
			request_four: '',
			destination_four: '',
			request_five: '',
			destination_five: '',
			request_six: '',
			destination_six: ''
		};

		$scope.forcedata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			distance_one: '',
			distance_two: '',
			distance_three: ''
		};

		$scope.jumpdata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			jump_high_one: '',
			jump_high_two: '',
			jump_high_three: ''
		};
				
		$scope.CreateHeadingAimForm.$setPristine();
		$scope.CreateHeadingForceForm.$setPristine();
		$scope.CreateHeadingJumpForm.$setPristine();

	};

	
	$scope.getEvent($scope.id_uniq);
	//if( $scope.event ) $scope.getPlayer($scope.player_id_uniq);

	

	$scope.setForceData = function( source ){
		$scope.headForcedata.id_player        = source.id_player;
		$scope.headForcedata.id_event         = source.id_event;
		$scope.headForcedata.measurement_date = source.measurement_date; 
		$scope.headForcedata.distance_one     = source.distance[0]; 
		$scope.headForcedata.distance_two     = source.distance[1]; 
		$scope.headForcedata.distance_three   = source.distance[2]; 
		$scope.headForcedata.attempt_one      = source.attempt[0]; 
		$scope.headForcedata.attempt_two      = source.attempt[1]; 
		$scope.headForcedata.attempt_three    = source.attempt[2];
	}

	$scope.calculateForce = function (field ) {
		//$scope.forceFieldValidated[field] = true;

		var velocityInitial = 0;
		var velocityFinal   = $scope.forcedata.attempt[field];
		var distance        = $scope.forcedata.distance[field];
		var mass            = $scope.ball_weight / 1000;


		console.log(velocityFinal,distance,$scope.ball_weight);
		var force           = mass * ( ( Math.pow( velocityFinal, 2 ) - velocityInitial ) / ( 2 * distance ) );

		return force.toFixed(2);

		//if( field === 2 ) $scope.CreateHeadingForceFormCompleted = true;
	};	

	$scope.EditHeadingForce = function(){
		$scope.creatingHeadingForce = true;
		$scope.creatingHeadingForceAlert = false;

		$scope.setForceData( $scope.forcedata );

		var data = $.param($scope.headForcedata);

		Heading.updateForce(data).then(function (res) {

			$scope.creatingHeadingForce = false;

			if (res.data.status == "success") {

				STFcache.delete('headingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/heading');

			} else {

				$scope.creatingHeadingForceAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingHeadingForce = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingHeadingForceAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	}

	$scope.EditHeadingAim = function () {

		$scope.creatingHeadingAim = true;
		$scope.creatingHeadingAimAlert = false;

		var data = $.param($scope.aimdata);

		Heading.updateAim(data).then(function (res) {

			$scope.creatingHeadingAim = false;

			if (res.data.status == "success") {
                
				STFcache.delete('headingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/heading');

			} else {

				$scope.creatingHeadingAimAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingHeadingAim = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingHeadingAimAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.EditHeadingJump = function () {

		$scope.creatingHeadingJump = true;
		$scope.creatingHeadingJumpAlert = false;

		var data = $.param($scope.jumpdata);

		Heading.updateJump(data).then(function (res) {

			$scope.creatingHeadingJump = false;
               
			if (res.data.status == "success") {

				STFcache.delete('headingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/heading');

			} else {

				$scope.creatingHeadingJumpAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingHeadingJump = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingHeadingJumpAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};


}]);

stf.controller('HeadingResultsController', ['$scope', function ($scope) {
	
}]);

var stf = angular.module('KickingControllers', []);

stf.controller('KickingListController', ['$scope', '$routeParams', '$location', 'Event', 'Kicking', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, $location, Event, Kicking, Access, Auth, STFcache) {
	
   $scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};
	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;

				if (_.findIndex($scope.event.configurations, { station: 'Kicking' }) >= 0) {

 					$scope.$emit('eventLoaded');

				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getKickings = function (id_uniq, refresh) {
       
		if (refresh !== undefined && refresh == true) STFcache.delete('kickingEvent' + $scope.id_uniq);
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.measurements = null;
		$scope.loadingKickings = true;
		$scope.loadingKickingsAlert = false;

		Kicking.getAll(id_uniq).then(function (res) {

			$scope.loadingKickings = false;

			if (angular.isObject(res.data.response)) {
				$scope.measurements = res.data.response;
				
			} else {
				$scope.loadingKickingsAlert = {
					type: 'success',
					message: res.data.response

				};

			}
			$scope.getAccess(1);
		}, function (err) {

			$scope.loadingKickings = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingKickingsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.deletePlayerEvent = function( player_id, ind ){
		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:player_id,type:'delete'});
			Kicking.delete(data).then(function (res) {
				$scope.loadingKickings = false;
				$scope.getKickings($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingKickingsAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingKickings = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingKickingsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};

	$scope.bulkDeletePlayerEvent = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:values,type:'delete',action:action_id});
			Kicking.multidelete(data).then(function (res) {
				$scope.loadingKickings = false;
				$scope.getKickings($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingKickingsAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingKickings = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingKickingsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one to delete.");
	 }
	};


	$scope.getEvent($scope.id_uniq);
	$scope.getKickings($scope.id_uniq);

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

	/*$scope.orderOptions = [
		{ value: 'name', text: 'Name' },
		{ value: '-date', text: 'Date' }
	];*/
	
}]);

stf.controller('KickingCreateController', ['$scope', '$routeParams', '$location', '$rootScope', '$filter', '$timeout', 'Event', 'Player', 'Kicking', 'Auth', 'STFcache', function ($scope, $routeParams, $location, $rootScope, $filter, $timeout, Event, Player, Kicking, Auth, STFcache) {
	
	$scope.id_uniq = $routeParams.id_uniq;
	$scope.player = null;
	$scope.ball_weight = 0;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	$scope.legs = [
		{ value: 1, name: 'Right' },
		{ value: 2, name: 'Left' }
	];

	/* AIM TEST CODE */

	$scope.aimTest = false;

	$scope.firstAimLeg = 'First';
	$scope.secondAimLeg = 'Second';

	$scope.firstGroupFieldOneValidated = false;
	$scope.firstGroupFieldTwoValidated = false;
	$scope.firstGroupFieldThreeValidated = false;

	$scope.secondGroupFieldOneValidated = false;
	$scope.secondGroupFieldTwoValidated = false;
	$scope.secondGroupFieldThreeValidated = false;

	$scope.aimdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		initial_leg: '',
		right_leg: {},
		left_leg: {}
	
	};

	$scope.firstGroupData = {
		request_one: 0,
		destination_one: '',
		request_two: 0,
		destination_two: '',
		request_three: 0,
		destination_three: ''
	};

	$scope.secondGroupData = {
		request_one: 0,
		destination_one: '',
		request_two: 0,
		destination_two: '',
		request_three: 0,
		destination_three: ''
	};

	$scope.startAimTest = function () { 
           //only take number
           
         
          	angular.element('.onlynumbertext').on("keypress",function (evt) {
    	    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;

			});
		return $scope.aimTest = true; };
	    $scope.$watch('aimdata.initial_leg', function (newVal, oldVal) {

		switch (newVal) {

			case 1:
				$scope.firstGroupData.request_one = Math.floor((Math.random() * 24) + 1);
				$scope.firstAimLeg = 'Right';
				$scope.secondAimLeg = 'Left';
				break;
			case 2:
				$scope.firstGroupData.request_one = Math.floor((Math.random() * 24) + 1);
				$scope.firstAimLeg = 'Left';
				$scope.secondAimLeg = 'Right';
				break;
			default:
				$scope.firstGroupData.request_one = 0;
				$scope.firstAimLeg = 'First';
				$scope.secondAimLeg = 'Second';
				break;

		}

	});

	$scope.validateFirstGroupField = function (field) {

		switch (field) {

			case 1: 
				$scope.firstGroupFieldOneValidated = true;
				$scope.firstGroupData.request_two = Math.floor((Math.random() * 24) + 1);	
				break;
			case 2:
				$scope.firstGroupFieldTwoValidated = true;
				$scope.firstGroupData.request_three = Math.floor((Math.random() * 24) + 1);
				break;
			case 3:
				$scope.firstGroupFieldThreeValidated = true;
				$scope.firstGroupFormCompleted = true;
				$scope.secondGroupData.request_one = Math.floor((Math.random() * 24) + 1);
				break;
			default: 
				break;	

		}

	};

	$scope.validateSecondGroupField = function (field) {

		switch (field) {

			case 1: 
				$scope.secondGroupFieldOneValidated = true;
				$scope.secondGroupData.request_two = Math.floor((Math.random() * 24) + 1);	
				break;
			case 2:
				$scope.secondGroupFieldTwoValidated = true;
				$scope.secondGroupData.request_three = Math.floor((Math.random() * 24) + 1);
				break;
			case 3:
				$scope.secondGroupFieldThreeValidated = true;
				$scope.secondGroupFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	/* END AIM TEST CODE */



	/* FORCE TEST CODE */

	$scope.forceTest = false;

	$scope.firstForceLeg = 'First';
	$scope.secondForceLeg = 'Second';

	$scope.forcedata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		initial_leg: '',
		right_leg: {},
		left_leg: {}
	};

	$scope.firstGroupForceData = {
		attempt_one: '',
		attempt_two: '',
		attempt_three: '',
		result_one: '',
		result_two: '',
		result_three: ''
	};

	$scope.secondGroupForceData = {
		attempt_one: '',
		attempt_two: '',
		attempt_three: '',
		result_one: '',
		result_two: '',
		result_three: ''
	};

	$scope.startForceTest = function () { return $scope.forceTest = true; };
	$scope.$watch('forcedata.initial_leg', function (newVal, oldVal) {

		switch (newVal) {

			case 1:
				$scope.firstForceLeg = 'Right';
				$scope.secondForceLeg = 'Left';
				break;
			case 2:
				$scope.firstForceLeg = 'Left';
				$scope.secondForceLeg = 'Right';
				break;
			default:
				$scope.firstForceLeg = 'First';
				$scope.secondForceLeg = 'Second';
				break;

		}

	});

	$scope.validateFirstGroupForceField = function (field) {

		switch (field) {

			case 1: 
				$scope.firstGroupForceFieldOneValidated = true;
				$scope.firstGroupForceData.result_one = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_one)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_one)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 2:
				$scope.firstGroupForceFieldTwoValidated = true;
				$scope.firstGroupForceData.result_two = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_two)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_two)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 3:
				$scope.firstGroupForceFieldThreeValidated = true;
				$scope.firstGroupForceData.result_three = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_three)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_three)) / 0.05) / 0.22481).toFixed(2);
				$scope.firstGroupForceFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	$scope.validateSecondGroupForceField = function (field) {

		switch (field) {

			case 1: 
				$scope.secondGroupForceFieldOneValidated = true;
				$scope.secondGroupForceData.result_one = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_one)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_one)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 2:
				$scope.secondGroupForceFieldTwoValidated = true;
				$scope.secondGroupForceData.result_two = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_two)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_two)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 3:
				$scope.secondGroupForceFieldThreeValidated = true;
				$scope.secondGroupForceData.result_three = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_three)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_three)) / 0.05) / 0.22481).toFixed(2);
				$scope.secondGroupForceFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	/* END FORCE TEST CODE */

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;
				$scope.measurement_system = ($scope.event.stf_measurement_abbr != "MKS") ? "YLS" : "MKS";
                
                   //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    if (String.fromCharCode(evt.which) == "e"){
        	return false;
            }
            if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length >1){ 
             
                this.value = parseFloat($(this).val().slice(0, -1));
            }  
         }  
         });  

				if (_.findIndex($scope.event.configurations, { station: 'Kicking' }) >= 0) {

 					$scope.aimdata.id_event = $scope.event.id;
 					$scope.forcedata.id_event = $scope.event.id;

 					$scope.eventHasKickingAim = (_.findIndex($scope.event.configurations, { station: 'Kicking', test: 'Aim' }) >= 0) ? true : false;
 					$scope.eventHasKickingForce = (_.findIndex($scope.event.configurations, { station: 'Kicking', test: 'Force' }) >= 0) ? true : false;

 				} else {	

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			if (angular.isObject(res.data.response)) {

				$scope.aimdata.id_player = res.data.response.id;
				$scope.forcedata.id_player = res.data.response.id;

				return res.data.response;
                

			} else {

				$scope.loadingPlayer = false;

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		}).then(function (args) {

			if (args !== undefined) {

				var player = args;
				var data = [];
				var forceAgeRangeData = [];
				var aimAgeRangeData = [];

				for (var i = 0; i < $scope.event.configurations.length; i++) {
					
					if ($scope.event.configurations[i].station == "Kicking") {

						if (_.findIndex(data, { value: $scope.event.configurations[i].test }) == -1) {

							data.push({
								name: 'tests[]',
								value: $scope.event.configurations[i].test
							});

						};

						if ($scope.event.configurations[i].test == "Force") {

							forceAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	

						};

						if ($scope.event.configurations[i].test == "Aim") {

							aimAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	

						};

					};

				};

				data = $.param(data);
								
				Kicking.check(player.id, $scope.event.id, data).then(function (res) {

					$scope.loadingPlayer = false;

					if (res.data.player_exists == 1) {

						$scope.loadingPlayerAlert = {
							type: 'success',
							message: res.data.response
						};

					} else {

						var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));

						$scope.playerCanPerformAimTest = false;
						$scope.playerCanPerformForceTest = false;

						if (aimAgeRangeData.length > 0) {

							for (var i = 0; i < aimAgeRangeData.length; i++) {

								var min = parseInt(aimAgeRangeData[i].min);
								var max = parseInt(aimAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformAimTest = true;
									break;

								};

							};

						};

						if (forceAgeRangeData.length > 0) {

							for (var i = 0; i < forceAgeRangeData.length; i++) {

								var min = parseInt(forceAgeRangeData[i].min);
								var max = parseInt(forceAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformForceTest = true;
									$scope.ball_weight = parseInt(forceAgeRangeData[i].ball_weight);
									break;

								};

							};

						};

						$scope.player = player;

						$scope.AimTestAlert = ($scope.playerCanPerformAimTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
						$scope.ForceTestAlert = ($scope.playerCanPerformForceTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;

						$scope.showAim = (res.data.response.aim == 2) ? true : false;
						$scope.showForce = (res.data.response.force == 2) ? true : false;

					}

				}, function (err) {

					$scope.loadingPlayer = false;

					$scope.loadingPlayerAlert = {
						type: 'danger',
						message: res.data.response
					};

				});

			};

		});

	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$scope.aimdata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			right_leg: {},
			left_left: {}
		};

		$scope.rightLegAimData = {
			request_one: 0,
			destination_one: '',
			request_two: 0,
			destination_two: '',
			request_three: 0,
			destination_three: ''
		};

		$scope.leftLegAimData = {
			request_one: 0,
			destination_one: '',
			request_two: 0,
			destination_two: '',
			request_three: 0,
			destination_three: ''
		};

		$scope.forcedata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			initial_leg: '',
			right_leg: {},
			left_leg: {}
		};

		$scope.firstGroupForceData = {
			attempt_one: '',
			attempt_two: '',
			attempt_three: '',
			result_one: '',
			result_two: '',
			result_three: ''
		};

		$scope.secondGroupForceData = {
			attempt_one: '',
			attempt_two: '',
			attempt_three: '',
			result_one: '',
			result_two: '',
			result_three: ''
		};
		
		$scope.CreateKickingAimForm.$setPristine();
		$scope.rightLegAimForm.$setPristine();
		$scope.leftLegAimForm.$setPristine();

		$scope.CreateKickingForceForm.$setPristine();
		$scope.firstGroupForceForm.$setPristine();
		$scope.secondGroupForceForm$setPristine();
	};

	$scope.getEvent($scope.id_uniq);

	$scope.createKickingAim = function () {

		switch ($scope.aimdata.initial_leg) {

			case 1:
				$scope.aimdata.right_leg = $scope.firstGroupData;
				$scope.aimdata.left_leg = $scope.secondGroupData;
				break;
			case 2:
				$scope.aimdata.right_leg = $scope.secondGroupData;
				$scope.aimdata.left_leg = $scope.firstGroupData;
				break;
			default:
				return;

		}

		$scope.creatingKickingAim = true;
		$scope.creatingKickingAimAlert = false;

		var data = $.param($scope.aimdata);

		Kicking.createAim(data).then(function (res) {

			$scope.creatingKickingAim = false;

			if (res.data.status == "success") {

				STFcache.delete('kickingEvent' + $scope.id_uniq);
                

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/kicking');

			} else {

				$scope.creatingKickingAimAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.creatingKickingAim = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingKickingAimAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.createKickingForce= function () {

		switch ($scope.forcedata.initial_leg) {

			case 1:
				$scope.forcedata.right_leg = $scope.firstGroupForceData;
				$scope.forcedata.left_leg = $scope.secondGroupForceData;
				break;
			case 2:
				$scope.forcedata.right_leg = $scope.secondGroupForceData;
				$scope.forcedata.left_leg = $scope.firstGroupForceData;
				break;
			default:
				return;

		}

		$scope.creatingKickingForce = true;
		$scope.creatingKickingForceAlert = false;

		var data = $.param($scope.forcedata);

		Kicking.createForce(data).then(function (res) {

			$scope.creatingKickingForce = false;

			if (res.data.status == "success") {

				STFcache.delete('kickingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/kicking');

			} else {

				$scope.creatingKickingForceAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.creatingKickingForce = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingKickingForceAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};
	
}]);

stf.controller('KickingEditController',  ['$scope', '$routeParams', '$location', '$rootScope', '$filter', '$timeout', 'Event', 'Player', 'Kicking', 'Auth', 'STFcache', function ($scope, $routeParams, $location, $rootScope, $filter, $timeout, Event, Player, Kicking, Auth, STFcache) {

	// $scope.id_uniq = $routeParams.playerid;
	// $scope.eventid=$routeParams.id;
	$scope.id_uniq = $routeParams.id;
	$scope.player_id_uniq = $routeParams.playerid;
	$scope.player = null;
	$scope.ball_weight = 0;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	$scope.legs = [
		{ value: 1, name: 'Right' },
		{ value: 2, name: 'Left' }
	];

	/* AIM TEST CODE */

	$scope.aimTest = false;

	$scope.firstAimLeg = 'First';
	$scope.secondAimLeg = 'Second';

	$scope.firstGroupFieldOneValidated = false;
	$scope.firstGroupFieldTwoValidated = false;
	$scope.firstGroupFieldThreeValidated = false;

	$scope.secondGroupFieldOneValidated = false;
	$scope.secondGroupFieldTwoValidated = false;
	$scope.secondGroupFieldThreeValidated = false;

	$scope.aimdata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		initial_leg: '',
		right_leg: {},
		left_leg: {}
	
	};

	$scope.firstGroupData = {
		request_one: 0,
		destination_one: '',
		request_two: 0,
		destination_two: '',
		request_three: 0,
		destination_three: ''
	};

	$scope.secondGroupData = {
		request_one: 0,
		destination_one: '',
		request_two: 0,
		destination_two: '',
		request_three: 0,
		destination_three: ''
	};

	$scope.startAimTest = function () { 
        //only take number

        angular.element('.onlynumbertext').on("keypress",function (evt) {
    	    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;

		});
		return $scope.aimTest = true; 

	};

	$scope.$watch('aimdata.initial_leg', function (newVal, oldVal) {
		switch (newVal) {
			case 1:
				$scope.firstGroupData.request_one = Math.floor((Math.random() * 24) + 1);
				$scope.firstAimLeg = 'Right';
				$scope.secondAimLeg = 'Left';
				break;
			case 2:
				$scope.firstGroupData.request_one = Math.floor((Math.random() * 24) + 1);
				$scope.firstAimLeg = 'Left';
				$scope.secondAimLeg = 'Right';
				break;
			default:
				//$scope.firstGroupData.request_one = 0;
				$scope.firstAimLeg = 'First';
				$scope.secondAimLeg = 'Second';
				break;
		}
	});

	$scope.validateFirstGroupField = function (field) {
		switch (field) {
			case 1: 
				$scope.firstGroupFieldOneValidated = true;
				$scope.firstGroupData.request_two = Math.floor((Math.random() * 24) + 1);	
				break;
			case 2:
				$scope.firstGroupFieldTwoValidated = true;
				$scope.firstGroupData.request_three = Math.floor((Math.random() * 24) + 1);
				break;
			case 3:
				$scope.firstGroupFieldThreeValidated = true;
				$scope.firstGroupFormCompleted = true;
				$scope.secondGroupData.request_one = Math.floor((Math.random() * 24) + 1);
				break;
			default: 
				break;	
		}

	};

	$scope.validateSecondGroupField = function (field) {

		switch (field) {

			case 1: 
				$scope.secondGroupFieldOneValidated = true;
				$scope.secondGroupData.request_two = Math.floor((Math.random() * 24) + 1);	
				break;
			case 2:
				$scope.secondGroupFieldTwoValidated = true;
				$scope.secondGroupData.request_three = Math.floor((Math.random() * 24) + 1);
				break;
			case 3:
				$scope.secondGroupFieldThreeValidated = true;
				$scope.secondGroupFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	/* END AIM TEST CODE */

	/* FORCE TEST CODE */

	$scope.forceTest = false;

	$scope.firstForceLeg = 'First';
	$scope.secondForceLeg = 'Second';

	$scope.forcedata = {
		id_player: '',
		id_event: '',
		measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
		initial_leg: '',
		right_leg: {},
		left_leg: {}
	};

	$scope.firstGroupForceData = {
		attempt_one: '',
		attempt_two: '',
		attempt_three: '',
		result_one: '',
		result_two: '',
		result_three: ''
	};

	$scope.secondGroupForceData = {
		attempt_one: '',
		attempt_two: '',
		attempt_three: '',
		result_one: '',
		result_two: '',
		result_three: ''
	};

	$scope.startForceTest = function () { return $scope.forceTest = true; };
	$scope.$watch('forcedata.initial_leg', function (newVal, oldVal) {

		switch (newVal) {

			case 1:
				$scope.firstForceLeg = 'Right';
				$scope.secondForceLeg = 'Left';
				break;
			case 2:
				$scope.firstForceLeg = 'Left';
				$scope.secondForceLeg = 'Right';
				break;
			default:
				$scope.firstForceLeg = 'First';
				$scope.secondForceLeg = 'Second';
				break;

		}

	});

	$scope.validateFirstGroupForceField = function (field) {

		switch (field) {

			case 1: 
				$scope.firstGroupForceFieldOneValidated = true;
				$scope.firstGroupForceData.result_one = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_one)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_one)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 2:
				$scope.firstGroupForceFieldTwoValidated = true;
				$scope.firstGroupForceData.result_two = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_two)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_two)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 3:
				$scope.firstGroupForceFieldThreeValidated = true;
				$scope.firstGroupForceData.result_three = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_three)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.firstGroupForceData.attempt_three)) / 0.05) / 0.22481).toFixed(2);
				$scope.firstGroupForceFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	$scope.validateSecondGroupForceField = function (field) {

		switch (field) {

			case 1: 
				$scope.secondGroupForceFieldOneValidated = true;
				$scope.secondGroupForceData.result_one = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_one)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_one)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 2:
				$scope.secondGroupForceFieldTwoValidated = true;
				$scope.secondGroupForceData.result_two = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_two)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_two)) / 0.05) / 0.22481).toFixed(2);
				break;
			case 3:
				$scope.secondGroupForceFieldThreeValidated = true;
				$scope.secondGroupForceData.result_three = ($scope.measurement_system == "MKS") ? ((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_three)) / 0.05).toFixed(2) : (((($scope.ball_weight / 1000) * parseFloat($scope.secondGroupForceData.attempt_three)) / 0.05) / 0.22481).toFixed(2);
				$scope.secondGroupForceFormCompleted = true;
				break;
			default: 
				break;	

		}

	};

	/* END FORCE TEST CODE */

	$scope.getEvent = function (id_uniq) {
       
		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;
				$scope.measurement_system = ($scope.event.stf_measurement_abbr != "MKS") ? "YLS" : "MKS";
                
                   //only take number
             angular.element('input[type=number]').on("keypress",function (evt) {
    	    if (String.fromCharCode(evt.which) == "e"){
        	return false;
            }
            if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length >1){ 
             
                this.value = parseFloat($(this).val().slice(0, -1));
            }  
         }  
         });  

				if (_.findIndex($scope.event.configurations, { station: 'Kicking' }) >= 0) {

 					$scope.aimdata.id_event = $scope.event.id;
 					$scope.forcedata.id_event = $scope.event.id;

 					$scope.eventHasKickingAim = (_.findIndex($scope.event.configurations, { station: 'Kicking', test: 'Aim' }) >= 0) ? true : false;
 					$scope.eventHasKickingForce = (_.findIndex($scope.event.configurations, { station: 'Kicking', test: 'Force' }) >= 0) ? true : false;
                    $scope.getPlayer($scope.player_id_uniq);
 				} else {	

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {
		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {
			if (angular.isObject(res.data.response)) {
				$scope.aimdata.id_player = res.data.response.id;
				$scope.forcedata.id_player = res.data.response.id;
				return res.data.response;
			} else {
				$scope.loadingPlayer = false;
				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};
			}
		}, function (err) {
			$scope.loadingPlayer = false;
			if (err.status == 401) {
				Auth.deleteToken();
			} else {
				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};
			}
		}).then(function (args) {

			if (args !== undefined) {
				var player = args;
				var data = [];
				var forceAgeRangeData = [];
				var aimAgeRangeData = [];
				for (var i = 0; i < $scope.event.configurations.length; i++) {
					if ($scope.event.configurations[i].station == "Kicking") {
						if (_.findIndex(data, { value: $scope.event.configurations[i].test }) == -1) {

							data.push({
								name: 'tests[]',
								value: $scope.event.configurations[i].test
							});
						};

						if ($scope.event.configurations[i].test == "Force") {
							forceAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	
						};

						if ($scope.event.configurations[i].test == "Aim") {
							aimAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	
						};
					};
				};

				data = $.param(data);
				var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));

				$scope.playerCanPerformAimTest = false;
				$scope.playerCanPerformForceTest = false;

				if (aimAgeRangeData.length > 0) {

					for (var i = 0; i < aimAgeRangeData.length; i++) {

						var min = parseInt(aimAgeRangeData[i].min);
						var max = parseInt(aimAgeRangeData[i].max);

						if (playerAge >= min && playerAge <= max) {

							$scope.playerCanPerformAimTest = true;
							break;

						};

					};

				};

				if (forceAgeRangeData.length > 0) {

					for (var i = 0; i < forceAgeRangeData.length; i++) {

						var min = parseInt(forceAgeRangeData[i].min);
						var max = parseInt(forceAgeRangeData[i].max);

						if (playerAge >= min && playerAge <= max) {

							$scope.playerCanPerformForceTest = true;
							$scope.ball_weight = parseInt(forceAgeRangeData[i].ball_weight);
							break;

						};

					};

				};

				$scope.player = player;
               
               	Kicking.getDetails($scope.id_uniq,$scope.player_id_uniq).then(function (res) {
					$scope.loadingEvent = false;
					if (angular.isObject(res.data.response)) {
                        console.log(res.data.response);
						//if( $scope.event ){
                        //added by sg//
                        //if(res.data.response.aimStat==1){
                        	//document.getElementsByName('initial_leg').disabled = false;
                        $scope.aimdata.initial_leg="number:1";
                        $scope.aimdata.id_player = res.data.response.aimPlayerID;
						$scope.aimdata.id_event = res.data.response.aimEvtID;
						$scope.aimdata.measurement_date = res.data.response.aimMeasurementDt;
						$scope.firstGroupData.request_one=parseFloat(res.data.response.aimRightReqOne);
						$scope.firstGroupData.request_two=parseFloat(res.data.response.aimRightReqTwo);
						$scope.firstGroupData.request_three=parseFloat(res.data.response.aimRightReqThree);
						$scope.firstGroupData.destination_one=parseFloat(res.data.response.aimRightDestOne);
                        $scope.firstGroupData.destination_two=parseFloat(res.data.response.aimRightDestTwo);
                        $scope.firstGroupData.destination_three=parseFloat(res.data.response.aimRightDestThree);
                        
                        $scope.secondGroupData.request_one=parseFloat(res.data.response.aimLeftReqOne);
						$scope.secondGroupData.request_two=parseFloat(res.data.response.aimLeftReqTwo);
						$scope.secondGroupData.request_three=parseFloat(res.data.response.aimLeftReqThree);
						$scope.secondGroupData.destination_one=parseFloat(res.data.response.aimLeftDestOne);
                        $scope.secondGroupData.destination_two=parseFloat(res.data.response.aimLeftDestTwo);
                        $scope.secondGroupData.destination_three=parseFloat(res.data.response.aimLeftDestThree);
						console.log($scope.firstGroupData.request_one);
						//}
						//else{
							//$scope.aimdata.measurement_date=$filter('date')(new Date(), 'MM/dd/yyyy');
						//}
                        
      //                   if(res.data.response.jumpStat==1){
						// $scope.jumpdata.id_player = res.data.response.jumpPlayerID;
						// $scope.jumpdata.id_event = res.data.response.jumpEvtID;
						// $scope.jumpdata.measurement_date = res.data.response.jumpMeasurementDt;
						// $scope.jumpdata.extended_arm_height=parseFloat(res.data.response.jumpArmHeight);
						// $scope.jumpdata.jump_high_one=parseFloat(res.data.response.jumpHighOne);
						// $scope.jumpdata.jump_high_two=parseFloat(res.data.response.jumpHighTwo);
						// $scope.jumpdata.jump_high_three=parseFloat(res.data.response.jumpHighThree);
						// }
						// else{
						// 	$scope.jumpdata.measurement_date=$filter('date')(new Date(), 'MM/dd/yyyy');
						// }
						//added by sg end//
						

						//$scope.forcedata.result[2] = res.data.response.forceAttemptOne;

						/*$scope.headForcedata = {
							id_player: res.data.response.forcePlayerID,
							id_event: res.data.response.forceEvtID,
							measurement_date: res.data.response.forceMeasurementDt,
							distance_one: parseFloat(res.data.response.forceDisOne),
							distance_two: parseFloat(res.data.response.forceDisTwo),
							distance_three: parseFloat(res.data.response.forceDisThree),
							attempt_one : parseFloat(res.data.response.forceAttemptOne),
							attempt_two : parseFloat(res.data.response.forceAttemptTwo),
							attempt_three : parseFloat(res.data.response.forceAttemptThree),
							result_one : parseFloat($scope.calculateForce(0)),
							result_two : parseFloat($scope.calculateForce(1)),
							result_three : parseFloat($scope.calculateForce(2))
						};*/
					} else {
						$scope.loadingEventAlert = {
							type: 'success',
							message: res.data.response
						};
					}

				}, function (err) {
					$scope.loadingEvent = false;

					if (err.status == 401) {

						Auth.deleteToken();

					} else {

						$scope.loadingEventAlert = {
							type: 'danger',
							message: err.status + ' ' + err.statusText
						};

					}

				});
				// $scope.AimTestAlert = ($scope.playerCanPerformAimTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
				// $scope.ForceTestAlert = ($scope.playerCanPerformForceTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;

				// $scope.showAim = (res.data.response.aim == 2) ? true : false;
				// $scope.showForce = (res.data.response.force == 2) ? true : false;
			};
		});
	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$scope.aimdata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			right_leg: {},
			left_left: {}
		};

		$scope.rightLegAimData = {
			request_one: 0,
			destination_one: '',
			request_two: 0,
			destination_two: '',
			request_three: 0,
			destination_three: ''
		};

		$scope.leftLegAimData = {
			request_one: 0,
			destination_one: '',
			request_two: 0,
			destination_two: '',
			request_three: 0,
			destination_three: ''
		};

		$scope.forcedata = {
			id_player: '',
			id_event: '',
			measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
			initial_leg: '',
			right_leg: {},
			left_leg: {}
		};

		$scope.firstGroupForceData = {
			attempt_one: '',
			attempt_two: '',
			attempt_three: '',
			result_one: '',
			result_two: '',
			result_three: ''
		};

		$scope.secondGroupForceData = {
			attempt_one: '',
			attempt_two: '',
			attempt_three: '',
			result_one: '',
			result_two: '',
			result_three: ''
		};
		
		$scope.CreateKickingAimForm.$setPristine();
		$scope.rightLegAimForm.$setPristine();
		$scope.leftLegAimForm.$setPristine();

		$scope.CreateKickingForceForm.$setPristine();
		$scope.firstGroupForceForm.$setPristine();
		$scope.secondGroupForceForm$setPristine();
	};

	$scope.getEvent($scope.id_uniq);
    
	$scope.editKickingAim = function () {

		switch ($scope.aimdata.initial_leg) {

			case 1:
				$scope.aimdata.right_leg = $scope.firstGroupData;
				$scope.aimdata.left_leg = $scope.secondGroupData;
				break;
			case 2:
				$scope.aimdata.right_leg = $scope.secondGroupData;
				$scope.aimdata.left_leg = $scope.firstGroupData;
				break;
			default:
				return;

		}

		$scope.creatingKickingAim = true;
		$scope.creatingKickingAimAlert = false;

		var data = $.param($scope.aimdata);

		Kicking.updateAim(data).then(function (res) {

			$scope.creatingKickingAim = false;

			if (res.data.status == "success") {

				STFcache.delete('kickingEvent' + $scope.id_uniq);
                

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/kicking');

			} else {

				$scope.creatingKickingAimAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.creatingKickingAim = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingKickingAimAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.editKickingForce= function () {

		switch ($scope.forcedata.initial_leg) {

			case 1:
				$scope.forcedata.right_leg = $scope.firstGroupForceData;
				$scope.forcedata.left_leg = $scope.secondGroupForceData;
				break;
			case 2:
				$scope.forcedata.right_leg = $scope.secondGroupForceData;
				$scope.forcedata.left_leg = $scope.firstGroupForceData;
				break;
			default:
				return;

		}

		$scope.creatingKickingForce = true;
		$scope.creatingKickingForceAlert = false;

		var data = $.param($scope.forcedata);

		Kicking.createForce(data).then(function (res) {

			$scope.creatingKickingForce = false;

			if (res.data.status == "success") {

				STFcache.delete('kickingEvent' + $scope.id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: res.data.response
				};

				$location.path('/events/' + $scope.id_uniq + '/kicking');

			} else {

				$scope.creatingKickingForceAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.creatingKickingForce = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingKickingForceAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};
			
}]);

var stf = angular.module('MainControllers', []);

stf.controller('LoginController', ['$scope','$location', 'Auth', function ($scope, $location, Auth) {
	
	$scope.formdata = {
		email: '',
		password: ''
	};

	

	$scope.login = function () {

		$scope.loginAlert = false;
		$scope.loginUserIn = true;

		var data = $.param($scope.formdata);

		Auth.login(data).then(function (res) {

			$scope.loginUserIn = false;
			
			if (res.data.status == "success") {

				Auth.createToken(res.data.response);
				$location.path('dashboard');

			} else {

				$scope.loginAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loginUserIn = false;
			$scope.loginAlert = {
				type: 'danger',
				message: err.status + ' ' + err.statusText
			};

		});

	};

}]);

stf.controller('PasswordForgotController', ['$scope','Forgot', function ($scope,Forgot) {
	$scope.formdata = {
		email: ''
		
	};

	$scope.passwordforgot = function () {

		$scope.ForgotPassAlert = false;
		$scope.ForgotPass = true;

		var data = $.param($scope.formdata);

		Forgot.passwordforgot(data).then(function (res) {

			$scope.ForgotPass = false;
			
			if (res.data.status == "success") {

				$scope.ForgotPassAlert = {
					type: 'success',
					message: res.data.response
				};
				//$location.path('dashboard');

			} else {

				$scope.ForgotPassAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.ForgotPass = false;
			$scope.ForgotPassAlert = {
				type: 'danger',
				message: err.status + ' ' + err.statusText
			};

		});

	};
}]);

stf.controller('PasswordRecoverController', ['$scope', function ($scope) {
	
}]);

stf.controller('tooltipMessageAlertController', ['$scope', '$rootScope', '$timeout', function ($scope, $rootScope, $timeout) {
	
	$timeout(function () {

		$rootScope.tooltipMessageAlert = false;

	}, 4000);

}]);

stf.controller('HistoryController', ['$scope', '$window', function ($scope, $window) {
	
	$scope.goBack = function () {

		$window.history.back();

	};

}]);

stf.controller('topbarMenuController', ['$scope', 'Auth', 'User', 'Section','STFcache', function ($scope, Auth, User, Section, STFcache ) {

	$scope.sections = function(refresh){
		if (refresh !== undefined && refresh == true) STFcache.delete('sections');
		var arr = [];
		Section.getSection().then(function(res){
			angular.forEach(res.data.response, function(value, key) {
				arr.push({
					id: value.id,
					name: value.section_name,
					link: value.section_link,
					icon: value.section_icon,
				});
			});
		});
		return arr;
	}(true);

	$scope.logout = function () { Auth.deleteToken(); }

	$scope.getLoggedInUser = function () {

		$scope.loadingLoggedInUser = true;

		User.getLoggedInUser().then(function (res) {

			$scope.loadingLoggedInUser = false;
			$scope.loggedInUser = res.data.response;
			
		}, function (err) {

			$scope.loadingLoggedInUser = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loggedInUser = err.status + ' ' + err.statusText;

			}

		});

	};

	$scope.getLoggedInUser();

	$scope.sideMenuOpen = false;

	$scope.toggleMenu = function () { $scope.sideMenuOpen = !$scope.sideMenuOpen };

	angular.element('#side-menu').perfectScrollbar({
		suppressScrollX: true
	});

}]);

stf.controller('setImageController', ['$scope',  '$location', '$rootScope', '$window', '$routeParams', '$timeout', 'Team', 'User', 'Player', 'Image', 'Auth', function ($scope, $location, $rootScope, $window, $routeParams, $timeout, Team, User, Player, Image, Auth) {
	
	var owners = ['tea_', 'pla_', 'use_'];
	var owner = $routeParams.owner;
	var id_uniq = $routeParams.id_uniq;
	var redirectURL;

	$scope.formdata = {
		owner: owner,
		id_uniq: id_uniq,
		xaxis: '',
		yaxis: '',
		width: '',
		height: ''
	};

	$scope.playerdata = {
		head: {
			id: 0,
			xaxis: 0,
			yaxis: 0,
			width: 0,
			height: 0
		},
		body: {
			id: 0,
			xaxis: 0,
			yaxis: 0,
			width: 0,
			height: 0
		}
	};

	$scope.getTeamImage = function (id_uniq) {

		$scope.loadingImage = true;
		$scope.loadingImageAlert = false;

		Team.getBaseImage(id_uniq).then(function (res) {

			$scope.loadingImage = false;
			
			if (res.data.is_image == 1) {

				$scope.image = res.data.response;

				$timeout(function () {

					$scope.$emit('imgReady');

				}, 500);

			} else {

				$scope.loadingImageAlert = {
					type: 'success',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.loadingImage = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingImageAlert = {
					type: 'danger',
					message: err.statusText
				}

			}

		});

	};

	$scope.getUserImage = function (id_uniq) {

		$scope.loadingImage = true;
		$scope.loadingImageAlert = false;

		User.getBaseImage(id_uniq).then(function (res) {

			$scope.loadingImage = false;
			
			if (res.data.is_image == 1) {

				$scope.image = res.data.response;

				$timeout(function () {

					$scope.$emit('imgReady');

				}, 500);

			} else {

				$scope.loadingImageAlert = {
					type: 'success',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.loadingImage = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingImageAlert = {
					type: 'danger',
					message: err.statusText
				}

			}

		});

	};

	var playerOptions = {

		getImages: function (id_uniq) {

			$scope.loadingImage = true;
			$scope.loadingImageAlert = false;

			Player.getBaseImages(id_uniq).then(function (res) {

				$scope.loadingImage = false;

				if (res.data.is_image == 1) {

					$scope.playerHasImages = true;
					$scope.playerImages = res.data.response;

					var headImageIndex = _.findIndex($scope.playerImages,  { type: "1" });
					var bodyImageIndex = _.findIndex($scope.playerImages,  { type: "2" });

					$scope.headImage = $scope.playerImages[headImageIndex].image;
					$scope.bodyImage = $scope.playerImages[bodyImageIndex].image;

					$scope.playerdata.head.id = $scope.playerImages[headImageIndex].id_image;
					$scope.playerdata.body.id = $scope.playerImages[bodyImageIndex].id_image;

					$timeout(function () {

						$scope.$emit('headImgReady');
						$scope.$emit('bodyImgReady');

					}, 500);

				} else {

					$scope.loadingImageAlert = {
						type: 'success',
						message: res.data.response
					}

				}

			}, function (err) {

				$scope.loadingImage = false;

				if (err.status == 401) {

					Auth.deleteToken();

				} else {

					$scope.loadingImageAlert = {
						type: 'danger',
						message: err.statusText
					}

				}

			});

		},
		getHeadImage: function (id_uniq) {

			$scope.loadingImage = true;
			$scope.loadingImageAlert = false;

			Player.getBaseHeadImage(id_uniq).then(function (res) {

				$scope.loadingImage = false;

				if (res.data.is_image == 1) {

					$scope.playerHasImages = true;
					$scope.playerHeadImage = res.data.response.image;
					$scope.playerdata.head.id = res.data.response.id_image;

					$timeout(function () {

						$scope.$emit('headImgReady');
					
					}, 500);

				} else {

					$scope.loadingImageAlert = {
						type: 'success',
						message: res.data.response
					}

				}

			}, function (err) {

				$scope.loadingImage = false;

				if (err.status == 401) {

					Auth.deleteToken();

				} else {

					$scope.loadingImageAlert = {
						type: 'danger',
						message: err.statusText
					}

				}

			});

		},
		getBodyImage: function (id_uniq) {

			$scope.loadingImage = true;
			$scope.loadingImageAlert = false;

			Player.getBaseBodyImage(id_uniq).then(function (res) {

				$scope.loadingImage = false;

				if (res.data.is_image == 1) {

					$scope.playerHasImages = true;
					$scope.playerBodyImage = res.data.response.image;
					$scope.playerdata.body.id = res.data.response.id_image;

					$timeout(function () {

						$scope.$emit('bodyImgReady');
					
					}, 500);

				} else {

					$scope.loadingImageAlert = {
						type: 'success',
						message: res.data.response
					}

				}

			}, function (err) {

				$scope.loadingImage = false;

				if (err.status == 401) {

					Auth.deleteToken();

				} else {

					$scope.loadingImageAlert = {
						type: 'danger',
						message: err.statusText
					}

				}

			});

		}

	}

	if (_.indexOf(owners, owner) == -1) { $location.path('/dashboard'); }
	else {

		switch (owner) {

			case 'tea_':
				$scope.getTeamImage(id_uniq);
				redirectURL = '/teams';
				break;
			case 'use_':
				$scope.getUserImage(id_uniq);
				redirectURL = '/users';
				break;
			case 'pla_':

				if ($window.sessionStorage.playerHeadFile == undefined || $window.sessionStorage.playerBodyFile == undefined) { $location.path('/dashboard'); }
				else {

					var headFile = $window.sessionStorage.playerHeadFile;
					var bodyFile = $window.sessionStorage.playerBodyFile;

					if (headFile == 1 && bodyFile == 1) {

						playerOptions.getImages(id_uniq);

					} else if (headFile == 1 && bodyFile == 0) {

						playerOptions.getHeadImage(id_uniq);

					} else if (headFile == 0 && bodyFile == 1) {

						playerOptions.getBodyImage(id_uniq);

					}

				}

				redirectURL = '/players';
				break;

		}

	}

	$scope.setImage = function () {

		$scope.settingImage = true;
		$scope.settingImageAlert = false;

		var data = $.param($scope.formdata);

		Image.set(data).then(function (res) {

			$scope.settingImage = false;

			if (res.data.status == "success") {

				var response = $window.sessionStorage.getItem('response') || "Action completed successfully";

				$window.sessionStorage.removeItem('response');

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: response
				};

				$location.path(redirectURL);

			} else {

				$scope.settingImageAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.settingImage = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.settingImageAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.setPlayerImages = function () {

		$scope.settingPlayersImages = true;
		$scope.settingPlayersImagesAlert = false;

		var data = $.param($scope.playerdata);

		Image.playerSet(data).then(function (res) {

			$scope.settingPlayersImages = false;

			if (res.data.status == "success") {

				var response = $window.sessionStorage.getItem('response') || "Action completed successfully";

				$window.sessionStorage.removeItem('response');
				$window.sessionStorage.removeItem('playerHeadFile');
				$window.sessionStorage.removeItem('playerBodyFile');

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: response
				};

				$location.path(redirectURL);

			} else {

				$scope.settingPlayersImagesAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.settingPlayersImages = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.settingPlayersImagesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

}]);
var stf = angular.module('MovementControllers', []);

stf.controller('MovementListController', ['$scope', '$routeParams', '$location', 'Event', 'Movement', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, $location, Event, Movement, Access, Auth, STFcache) {
	
	 $scope.bulk=false;
   	var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
			 values=[];
			$(".childcheckbox").each(function(){
		        if($(this).is(":checked"))
		            values.push($(this).val());
		    });
		    //alert(values);
		   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
		   	angular.element("#mastercheckbox").prop("checked",true);
		   } 
		   else{
		
		    angular.element("#mastercheckbox").prop("checked",false);
		      }
	};
    

	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;

				if (_.findIndex($scope.event.configurations, { station: 'Movement' }) >= 0) {

 					$scope.$emit('eventLoaded');

				} else {

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getMovements = function (id_uniq, refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('movementEvent' + $scope.id_uniq);
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.measurements = null;
		$scope.loadingMovements = true;
		$scope.loadingMovementsAlert = false;

		Movement.getAll($scope.id_uniq).then(function (res) {

			$scope.loadingMovements = false;

			if (angular.isObject(res.data.response)) {

				$scope.measurements = res.data.response;
				
            
			} else {
               
				$scope.loadingMovementsAlert = {
					type: 'success',
					message: res.data.response
				};

			}
			$scope.getAccess(1);
		}, function (err) {

			$scope.loadingMovements = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingMovementsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getEvent($scope.id_uniq);
	$scope.getMovements($scope.id_uniq);
    
   
	$scope.deleteMovement = function(id_player){

			if(confirm("Are you sure to delete this Player?")){
				var data = $.param({id_player:id_player,type:'delete'});
				Movement.delete(data).then(function (res) {
					$scope.loadingMovements = false;
					
					$scope.getMovements($scope.id_uniq,true);
					if (angular.isObject(res.data.response)) {
						//$scope.measurements = res.data.response;

					} else {
						$scope.loadingMovementsAlert = {type: 'success',message: res.data.response};
					}

				}, function (err) {

					$scope.loadingMovements = false;

					if (err.status == 401) {
						Auth.deleteToken();
					} else {
						$scope.loadingMovementsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
					}
				});
			}
		};

	$scope.bulkDeleteMovement = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:values,type:'delete',action:action_id});
			Movement.multidelete(data).then(function (res) {
				$scope.loadingMovements = false;
				
				$scope.getMovements($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingMovementsAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingMovements = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingMovementsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one to delete.");
	 }
	};


	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

}]);

stf.controller('MovementCreateController', ['$scope',  '$routeParams', '$location', '$filter', '$rootScope', 'Event', 'Player', 'Movement', 'Auth', function ($scope, $routeParams, $location, $filter, $rootScope, Event, Player, Movement, Auth) {
	
   

	$scope.id_uniq = $routeParams.id_uniq;
	$scope.player = null;
	$scope.ball_weight = 0;
	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	$scope.getEvent = function (id_uniq) {

		$scope.loadingEvent = true;
		$scope.loadingEventAlert = false;

		Event.getOne(id_uniq).then(function (res) {

			$scope.loadingEvent = false;

			if (angular.isObject(res.data.response)) {

				$scope.event = res.data.response;
				$scope.measurement_system = ($scope.event.stf_measurement_abbr != "MKS") ? "YLS" : "MKS";

				if (_.findIndex($scope.event.configurations, { station: 'Movement' }) >= 0) {

 					/*$scope.aimdata.id_event = $scope.event.id;
 					$scope.forcedata.id_event = $scope.event.id;*/

 					$scope.eventHasMovementLinear = (_.findIndex($scope.event.configurations, { station: 'Movement', test: 'Linear' }) >= 0) ? true : false;
 					$scope.eventHasMovementLateral = (_.findIndex($scope.event.configurations, { station: 'Movement', test: 'Lateral' }) >= 0) ? true : false;
 					$scope.eventHasMovementZigzag = (_.findIndex($scope.event.configurations, { station: 'Movement', test: 'Zigzag' }) >= 0) ? true : false;

 				} else {	

					$location.path('/events/' + $scope.id_uniq);

				}

			} else {

				$scope.loadingEventAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingEvent = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingEventAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			if (angular.isObject(res.data.response)) {

				return res.data.response;	

			} else {

				$scope.loadingPlayer = false;

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		}).then(function (args) {

			if (args !== undefined) {

				var player = args;
				var data = [];
				var linearAgeRangeData = [];
				var lateralAgeRangeData = [];
				var zigzagAgeRangeData = [];

				for (var i = 0; i < $scope.event.configurations.length; i++) {
					
					if ($scope.event.configurations[i].station == "Movement") {

						if (_.findIndex(data, { value: $scope.event.configurations[i].test }) == -1) {

							data.push({
								name: 'tests[]',
								value: $scope.event.configurations[i].test
							});

						};

						if ($scope.event.configurations[i].test == "Linear") {

							linearAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	

						};

						if ($scope.event.configurations[i].test == "Lateral") {

							lateralAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	

						};

						if ($scope.event.configurations[i].test == "Zigzag") {

							zigzagAgeRangeData.push({
								test: $scope.event.configurations[i].test,
								min: $scope.event.configurations[i].nu_min_age,
								max: $scope.event.configurations[i].nu_max_age,
								ball_weight: $scope.event.configurations[i].nu_ball_weight
							});	

						};

					};

				};

				data = $.param(data);

				Movement.check(player.id, $scope.event.id, data).then(function (res) {

					$scope.loadingPlayer = false;

					if (res.data.player_exists == 1) {

						$scope.loadingPlayerAlert = {
							type: 'success',
							message: res.data.response
						};

					} else {

						var playerAge = parseInt($filter('currentAge')(player.dt_birthdate));

						$scope.playerCanPerformLinearTest = false;
						$scope.playerCanPerformLateralTest = false;
						$scope.playerCanPerformZigzagTest = false;

						if (linearAgeRangeData.length > 0) {

							for (var i = 0; i < linearAgeRangeData.length; i++) {

								var min = parseInt(linearAgeRangeData[i].min);
								var max = parseInt(linearAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformLinearTest = true;
									break;

								};

							};

						};

						if (lateralAgeRangeData.length > 0) {

							for (var i = 0; i < lateralAgeRangeData.length; i++) {

								var min = parseInt(lateralAgeRangeData[i].min);
								var max = parseInt(lateralAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformLateralTest = true;
									break;

								};

							};

						};

						if (zigzagAgeRangeData.length > 0) {

							for (var i = 0; i < zigzagAgeRangeData.length; i++) {

								var min = parseInt(zigzagAgeRangeData[i].min);
								var max = parseInt(zigzagAgeRangeData[i].max);

								if (playerAge >= min && playerAge <= max) {

									$scope.playerCanPerformZigzagTest = true;
									break;

								};

							};

						};

						$scope.player = player;

						$scope.LinearTestAlert = ($scope.playerCanPerformLinearTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
						$scope.LateralTestAlert = ($scope.playerCanPerformLateralTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;
						$scope.ZigzagTestAlert = ($scope.playerCanPerformZigzagTest == false) ? { type: 'info', message: res.data.player_not_allowed_to_perform } : null;

						$scope.showLinear = (res.data.response.linear == 2) ? true : false;
						$scope.showLateral = (res.data.response.lateral == 2) ? true : false;
						$scope.showZigzag = (res.data.response.zigzag == 2) ? true : false;

					}

				}, function (err) {

					$scope.loadingPlayer = false;

					$scope.loadingPlayerAlert = {
						type: 'danger',
						message: res.data.response
					};

				});

			};

		});

	};

	$scope.changePlayer = function () {

		$scope.playerIDuniq = null;
		$scope.player = null;

		$rootScope.$emit('movementTestPlayerChanged');

	};

	$scope.activateTab = function (tab) {

		switch (tab) {

			case 0:
				$scope.activetab = "Linear";
				break;
			case 1:
				$scope.activetab = "Lateral";
				break;
			case 2:
				$scope.activetab = "Zigzag";
				break;
			default: 
				$scope.activetab = null;
				break;

		}

	};

	$scope.getEvent($scope.id_uniq);


}]);
var stf = angular.module('PlayerControllers', []);

stf.controller('PlayerListController', ['$rootScope','$scope', 'Player', 'Access', 'Auth', 'STFcache',  function ($rootScope,$scope, Player, Access, Auth, STFcache) {
	
	$scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};



	$scope.getPlayers = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('players');
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.players = null;
		$scope.loadingPlayers = true;
		$scope.loadingPlayersAlert = false;

		Player.getAll().then(function (res) {

			$scope.loadingPlayers = false;

			if (angular.isObject(res.data.response)) {
				$scope.players = res.data.response;
				//to change phonenumber string to number
				 res.data.response.forEach(function(players){
	            players.tx_phone= parseFloat(players.tx_phone);
	            players.tx_phone_prefix=parseFloat(players.tx_phone_prefix);
	           });
				// console.log($scope.players);
				
			} else {

				$scope.loadingPlayersAlert = {
					type: 'success',
					message: res.data.response
				};

			}
			$scope.getAccess(2);
		}, function (err) {

			$scope.loadingPlayers = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayers();

	$scope.deletePlayerEvent = function( player_id, ind ){

		console.log(player_id);

		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:player_id,type:'delete'});
			Player.delete(data).then(function (res) {
				$scope.loadingPlayers = false;
				$scope.getPlayers(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingPlayersAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {

				$scope.loadingPlayers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingPlayersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	}
    
   $scope.bulkDeletePlayerEvent = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:values,type:'delete',action:action_id});
			Player.multidelete(data).then(function (res) {
				$scope.loadingPlayers = false;
				$scope.getPlayers(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingPlayersAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {

				$scope.loadingPlayers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingPlayersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one Player to delete.");
	 }
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);

			changeAttr( $scope.players, 'players', $scope.access.bo_view );

		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		})
	}

	$scope.restrictView = function(){
		$('#alertMessage').show();
		setTimeout(function(){ 
			$('#alertMessage').hide();
		}, 2000);
	}

	function compile(element){
		var el = angular.element(element);    
		$scope = el.scope();
		$injector = el.injector();
		$injector.invoke(function($compile){
			$compile(el)($scope);
		})     
	}

	function changeAttr( obj, slug, viewAccess ){
        for( var i=0; i < obj.length; i++ ){
            var el = document.getElementById((slug.substring(0, slug.length-1))+"_id_"+i);
            if( viewAccess ){
                el.removeAttribute("ng-click");
                el.setAttribute("ng-href",slug+"/"+obj[i].id_uniq);
                el.setAttribute("href",slug+"/"+obj[i].id_uniq);
            } else {
                el.removeAttribute("ng-href");
                el.removeAttribute("href");
                el.setAttribute("ng-click", "restrictView()");
                compile(el);
            }
        }
    }


	$scope.orderOptions = [
		{ value: 'tx_firstname', text: 'Name' },
		{ value: 'tx_email', text: 'Email' },
		{ value: 'tx_phone', text: 'Phone number' }
	];

	

}]);

stf.controller('PlayerCreateController', ['$scope', '$filter', '$location', '$window', '$rootScope', 'Player', 'Gender', 'Country', 'Auth', 'STFcache', function ($scope, $filter, $location, $window, $rootScope, Player, Gender, Country, Auth, STFcache) {
	
	 angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
          	var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;


			});

	var minDate = new Date();
	var maxDate = new Date();

	$scope.formdata = {
		firstname: '',
		secondname: '',
		first_lastname: '',
		second_lastname: '',
		gender: '',
		birthdate: '',
		country: '',
		state: '',
		city:'',
		address: '',
		zipcode: '',
		phoneprefix: '-',
		phonenumber: '',
		email: '',
		head_file: '',
		body_file: ''
	};

	$scope.zipcodeMask = "?";
	$scope.date = null;
	$scope.minDate = new Date(minDate.setFullYear(minDate.getFullYear() - 18));
	$scope.maxDate = new Date(maxDate.setFullYear(maxDate.getFullYear() - 5));
	$scope.dateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: 100,
		selectMonths: true,
	};

	$scope.getGenders = function () {

		$scope.loadingGenders = true;
		$scope.loadingGendersAlert = false;

		Gender.getGenders().then(function (res) {

			$scope.loadingGenders = false;

			if (angular.isObject(res.data.response)) {

				$scope.genders = res.data.response;

			} else {

				$scope.loadingGendersAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingGenders = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingGendersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountries = function () {

		$scope.loadingCountries = true;
		$scope.loadingCountriesAlert = false;

		Country.getCountries().then(function (res) {

			$scope.loadingCountries = false;

			if (angular.isObject(res.data.response)) {

				$scope.countries = res.data.response;

			} else {

				$scope.loadingCountriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCountries = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingCountriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountryStates = function (id_country) {

		$scope.loadingStates = true;
		$scope.loadingStatesAlert = false;

		Country.getCountryStates(id_country).then(function (res) {

			$scope.loadingStates = false;

			if (angular.isObject(res.data.response)) {

				$scope.states = res.data.response;

			} else {

				$scope.loadingStatesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStates = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStatesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getGenders();
	$scope.getCountries();

	$scope.$watch('formdata.country', function () {

		var countryIndex = _.findIndex($scope.countries, { id: $scope.formdata.country });

		if (countryIndex != -1) {

			$scope.zipcodeMask = $filter('zipcodeMask')($scope.countries[countryIndex].tx_zipcode_format);
			$scope.zipcodeRequired = ($scope.zipcodeMask.length > 0) ? true : false;			

			$scope.formdata.phoneprefix = $scope.countries[countryIndex].tx_phone_prefix;

			var id_country = $scope.formdata.country;
			$scope.getCountryStates(id_country);

		};

		$scope.states = null;
		$scope.formdata.state = '';
		$scope.CreatePlayerForm.state.$setPristine();

	});

	$scope.createPlayer = function () {

		$scope.creatingPlayer = true;
		$scope.creatingPlayerAlert = false;

		var data = new FormData();

		data.append('firstname', $scope.formdata.firstname);
		data.append('secondname', $scope.formdata.secondname);
		data.append('first_lastname', $scope.formdata.first_lastname);
		data.append('second_lastname', $scope.formdata.second_lastname);
		data.append('gender', $scope.formdata.gender);
		data.append('birthdate', $scope.formdata.birthdate);
		data.append('country', $scope.formdata.country);
		data.append('state', $scope.formdata.state);
		data.append('city', $scope.formdata.city);
		data.append('address', $scope.formdata.address);
		data.append('zipcode', $scope.formdata.zipcode);
		data.append('phoneprefix', $scope.formdata.phoneprefix);
		data.append('phonenumber', $scope.formdata.phonenumber);
		data.append('email', $scope.formdata.email);
		data.append('head_file', $scope.formdata.head_file);
		data.append('body_file', $scope.formdata.body_file);

		Player.create(data).then(function (res) {

			$scope.creatingPlayer = false;

			if (res.data.status == "success") {

				STFcache.delete('players');
				STFcache.delete('playerUniqIDs');

				if (res.data.has_images == 1) {

					$window.sessionStorage.playerHeadFile = (res.data.has_head_file == 1) ? 1 : 0;
					$window.sessionStorage.playerBodyFile = (res.data.has_body_file == 1) ? 1 : 0;

					$window.sessionStorage.setItem('response', res.data.response);

					$location.path('/set_image/pla_/' + res.data.id_uniq);

				} else {

					$rootScope.tooltipMessageAlert = {
						type: 'success',
						message: res.data.response
					}

					$location.path('/players');

				}

			} else {

				$scope.creatingPlayerAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('PlayerDetailsController', ['$scope', '$routeParams', '$location', '$filter', 'Player', 'Access', 'Auth','STFcache', function ($scope, $routeParams, $location, $filter, Player, Access, Auth,STFcache) {
	
	$scope.id_uniq = $routeParams.id_uniq;

	$scope.randomNum = Math.floor((Math.random() * 10) + 1);

	$scope.getUniqIDs = function () {

		$scope.loadingUniqIDs = true;
		$scope.loadingUniqIDsAlert = false;

		Player.getUniqIDs().then(function (res) {

			$scope.loadingUniqIDs = false;

			$scope.result = res.data.response;
			$scope.totalItems = parseInt($scope.result.length);
			$scope.currentItem = parseInt(_.findIndex($scope.result, { id_uniq: $scope.id_uniq }));
			$scope.nextItem = $scope.currentItem + 1;	
			$scope.prevItem = $scope.currentItem - 1;

			$scope.nextID = ($scope.nextItem > -1 && $scope.nextItem < $scope.totalItems) ? $scope.result[$scope.nextItem].id_uniq : false;
			$scope.prevID = ($scope.prevItem >= 0) ? $scope.result[$scope.prevItem].id_uniq : false;

		}, function (err) {

			$scope.loadingUniqIDs = false;

			if (err.status == 401) {

				Auth.deleteToken();

			};

		});

	};

	$scope.prevPlayer = function () { $location.path('/players/' + $scope.prevID); };
	$scope.nextPlayer = function () { $location.path('/players/' + $scope.nextID); };

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			$scope.loadingPlayer = false;

			if (angular.isObject(res.data.response)) {

				$scope.player = res.data.response;
				$scope.getUniqIDs();
				$scope.getAccess(2);

			} else {

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer($scope.id_uniq);

	$scope.getPlayers = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('players');

		$scope.players = null;
		$scope.loadingPlayers = true;
		$scope.loadingPlayersAlert = false;

		Player.getAll().then(function (res) {

			$scope.loadingPlayers = false;

			if (angular.isObject(res.data.response)) {
				$scope.players = res.data.response;
				
			} else {

				$scope.loadingPlayersAlert = {
					type: 'success',
					message: res.data.response
				};

			}
			$scope.getAccess(2);
		}, function (err) {

			$scope.loadingPlayers = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	} 

	$scope.deletePlayerEvent = function( player_id, ind ){

		console.log(player_id);

		if(confirm("Are you sure to delete this Player?")){
			var data = $.param({id_player:player_id,type:'delete'});
			Player.delete(data).then(function (res) {
				$scope.loadingPlayers = false;
				$scope.getPlayers(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;
                     
				} else {
					$scope.loadingPlayerAlert = {type: res.data.status,message: res.data.response};
					if(res.data.status=="success"){
					 $location.path('/players');
					}
				}

			}, function (err) {

				$scope.loadingPlayers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingPlayersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	}

}]);

stf.controller('PlayerEditController', ['$scope', '$filter', '$location', '$routeParams', '$timeout', '$rootScope', '$window', 'Player', 'Gender', 'Country', 'Auth', 'STFcache', function ($scope, $filter, $location, $routeParams, $timeout, $rootScope, $window, Player, Gender, Country, Auth, STFcache) {
	
	 angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
         	var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;

            
			});

	var minDate = new Date();
	var maxDate = new Date();

	$scope.formdata = {
		firstname: '',
		secondname: '',
		first_lastname: '',
		second_lastname: '',
		gender: '',
		birthdate: '',
		country: '',
		state: '',
		city: '',
		address: '',
		zipcode: '',
		phoneprefix: '-',
		phonenumber: '',
		email: '',
		head_file: '',
		body_file: ''
	};

	$scope.zipcodeMask = "?";
	$scope.date = null;
	$scope.minDate = new Date(minDate.setFullYear(minDate.getFullYear() - 18));
	$scope.maxDate = new Date(maxDate.setFullYear(maxDate.getFullYear() - 5));
	$scope.dateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: 100,
		selectMonths: true
	};

	var id_uniq = $routeParams.id_uniq;

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			$scope.loadingPlayer = false;

			if (angular.isObject(res.data.response)) {

				$scope.player = res.data.response;
                 
				$scope.formdata.firstname = $scope.player.tx_firstname;
				$scope.formdata.secondname = $scope.player.tx_secondname;
				$scope.formdata.first_lastname = $scope.player.tx_lastname;
				$scope.formdata.second_lastname = $scope.player.tx_secondlastname;
				$scope.formdata.gender = $scope.player.id_gender;
				$scope.formdata.birthdate = $filter('date')($scope.player.dt_birthdate, 'MM/dd/yyyy');
				$scope.formdata.country = $scope.player.id_country;
				$scope.formdata.state = $scope.player.id_state;
				$scope.formdata.city = $scope.player.tx_city;
				$scope.formdata.address = $scope.player.tx_address;
				$scope.formdata.phonenumber = parseFloat($scope.player.tx_phone);
				$scope.formdata.email = $scope.player.tx_email;

			} else {

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getGenders = function () {

		$scope.loadingGenders = true;
		$scope.loadingGendersAlert = false;

		Gender.getGenders().then(function (res) {

			$scope.loadingGenders = false;

			if (angular.isObject(res.data.response)) {

				$scope.genders = res.data.response;

			} else {

				$scope.loadingGendersAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingGenders = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingGendersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountries = function () {

		$scope.loadingCountries = true;
		$scope.loadingCountriesAlert = false;

		Country.getCountries().then(function (res) {

			$scope.loadingCountries = false;

			if (angular.isObject(res.data.response)) {

				$scope.countries = res.data.response;
				$scope.$emit('countriesLoaded');

			} else {

				$scope.loadingCountriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCountries = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingCountriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountryStates = function (id_country) {

		$scope.loadingStates = true;
		$scope.loadingStatesAlert = false;

		Country.getCountryStates(id_country).then(function (res) {

			$scope.loadingStates = false;

			if (angular.isObject(res.data.response)) {

				$scope.states = res.data.response;

			} else {

				$scope.loadingStatesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStates = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStatesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer(id_uniq);
	$scope.getGenders();
	$scope.getCountries();

	$scope.$on('countriesLoaded', function () {

		$scope.$watch('formdata.country', function () {

			var countryIndex = _.findIndex($scope.countries, { id: $scope.formdata.country });

			if (countryIndex != -1) {

				$scope.zipcodeMask = $filter('zipcodeMask')($scope.countries[countryIndex].tx_zipcode_format);
				$scope.zipcodeRequired = ($scope.zipcodeMask.length > 0) ? true : false;

				$timeout(function () { $scope.formdata.zipcode = $scope.player.tx_zipcode; }, 1000);

				$scope.formdata.phoneprefix = $scope.countries[countryIndex].tx_phone_prefix;

				var id_country = $scope.formdata.country;
				$scope.getCountryStates(id_country);

			} else {

				$scope.states = null;
				$scope.formdata.state = '';
				$scope.EditPlayerForm.state.$setPristine();

			}

		});

	});

	$scope.editPlayer = function () {

		$scope.editingPlayer = true;
		$scope.editingPlayerAlert = false;

		var data = new FormData();

		data.append('firstname', $scope.formdata.firstname);
		data.append('secondname', $scope.formdata.secondname);
		data.append('first_lastname', $scope.formdata.first_lastname);
		data.append('second_lastname', $scope.formdata.second_lastname);
		data.append('gender', $scope.formdata.gender);
		data.append('birthdate', $scope.formdata.birthdate);
		data.append('country', $scope.formdata.country);
		data.append('state', $scope.formdata.state);
		data.append('city', $scope.formdata.city);
		data.append('address', $scope.formdata.address);
		data.append('zipcode', $scope.formdata.zipcode);
		data.append('phoneprefix', $scope.formdata.phoneprefix);
		data.append('phonenumber', $scope.formdata.phonenumber);
		data.append('email', $scope.formdata.email);
		data.append('head_file', $scope.formdata.head_file);
		data.append('body_file', $scope.formdata.body_file);

		Player.edit(id_uniq, data).then(function (res) {

			$scope.editingPlayer = false;

			if (res.data.status == "success") {

				STFcache.delete('players');
				STFcache.delete('players' + id_uniq);
				
				if (res.data.has_images == 1) {

					$window.sessionStorage.playerHeadFile = (res.data.has_head_file == 1) ? 1 : 0;
					$window.sessionStorage.playerBodyFile = (res.data.has_body_file == 1) ? 1 : 0;

					$window.sessionStorage.setItem('response', res.data.response);

					$location.path('/set_image/pla_/' + res.data.id_uniq);

				} else {

					$rootScope.tooltipMessageAlert = {
						type: 'success',
						message: res.data.response
					}

					$location.path('/players');

				}

			} else {

				$scope.editingPlayerAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.editingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('PlayerTeamsController', ['$scope', '$routeParams', 'Player', 'Access', 'Auth', 'STFcache', function ($scope, $routeParams, Player, Access, Auth, STFcache) {
	



	$scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};
    

	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			$scope.loadingPlayer = false;

			if (angular.isObject(res.data.response)) {

				$scope.player = res.data.response;
				$scope.getAccess(2);
			} else {

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.getPlayerTeams = function (id_uniq, refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('players' + id_uniq + 'teams');
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.teams = null;
		$scope.loadingTeams = true;
		$scope.loadingTeamsAlert = false;

		Player.getTeams(id_uniq).then(function (res) {

			$scope.loadingTeams = false;

			if (angular.isObject(res.data.response)) {

				$scope.teams = res.data.response;

			} else {

				$scope.loadingTeamsAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTeams = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer($scope.id_uniq);
	$scope.getPlayerTeams($scope.id_uniq);

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		})
	}

	$scope.orderOptions = [
		{ value: 'team', text: 'Name' },
		{ value: '-dt_admission', text: 'Admission Date' },
		{ value: '-dt_discharge', text: 'Discharge Date' }
	];

$scope.deletePlayerTeam = function( player_id, team_id ){
	//console.log(player_id);
		if(confirm("Are you sure to delete this Team of this Player?")){
			var data = $.param({
								id_player:player_id,
								id_team:team_id,
								type:'delete'
							});
			Player.deleteTeam(data).then(function( res ){
				$scope.loadingTeams = false;
				$scope.getPlayerTeams($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);

				} else {
					$scope.loadingTeamsAlert = {type: res.data.status,message: res.data.response};
				}
			}, function (err) {
				$scope.loadingTeams = false;

				if (err.status == 401) {

					Auth.deleteToken();

				} else {

					$scope.loadingTeamsAlert = {
						type: 'danger',
						message: err.status + ' ' + err.statusText
					};

				}
			});
		}
	};

	 $scope.bulkDeletePlayerTeam = function(){

       var id_team=angular.element('#teamid').val();
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Team of this Player?")){
			var data = $.param({
								id_player:values,
								id_team:id_team,
								action:action_id,
								type:'delete'
							});
			
			Player.multideleteTeam(data).then(function( res ){
				$scope.loadingTeams = false;
				$scope.getPlayerTeams($scope.id_uniq,true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);

				} else {
					$scope.loadingTeamsAlert = {type: res.data.status,message: res.data.response};
				}
			}, function (err) {
				$scope.loadingTeams = false;

				if (err.status == 401) {

					Auth.deleteToken();

				} else {

					$scope.loadingTeamsAlert = {
						type: 'danger',
						message: err.status + ' ' + err.statusText
					};

				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one Team to delete.");
	 }
	};


}]);

stf.controller('PlayerTeamCreateController', ['$scope', '$routeParams', '$timeout', '$filter', '$rootScope', '$location', 'Player', 'Team', 'Auth', 'STFcache', function ($scope, $routeParams, $timeout, $filter, $rootScope, $location, Player, Team, Auth, STFcache) {
	
	angular.element('input[type=number]').on("keypress",function (evt) {
    	    var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;

            

			});
    

	var id_uniq = $routeParams.id_uniq;

	$scope.date = null;
	$scope.admissionDate = null;
	$scope.dischargeDate = null;
	$scope.maxDate = new Date();
	$scope.dateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: 100,
		selectMonths: true
	};

	$scope.formdata = {
		current_team: {},
		team: '',
		admission_date: '',
		discharge_date: '',
		number: '',
		positions: []
	};

	$scope.positionsdata = {
		id_position: '',
		position: ''
	};

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			$scope.loadingPlayer = false;

			if (angular.isObject(res.data.response)) {

				$scope.player = res.data.response;

			} else {

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.getPlayerCurrentTeam = function (id_uniq) {

		$scope.loadingCurrentTeam = true;
		$scope.loadingCurrentTeamAlert = false;

		Player.getCurrentTeam(id_uniq).then(function (res) {

			$scope.loadingCurrentTeam = false;

			if (angular.isObject(res.data.response)) {

				$scope.currentTeam = res.data.response;

				$scope.formdata.current_team.id_team_player = $scope.currentTeam.id_team_player;
				$scope.formdata.current_team.admission_date = $filter('date')($scope.currentTeam.dt_admission, 'MM/dd/yyyy');
				$scope.formdata.current_team.discharge_date = $filter('date')($scope.currentTeam.dt_discharge, 'MM/dd/yyyy');

				$scope.date = $scope.formdata.current_team.discharge_date;

			};

		}, function (err) {

			$scope.loadingCurrentTeam = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingCurrentTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTeams = function () {

		$scope.loadingTeams = true;
		$scope.loadingTeamsAlert = false;

		Team.getTeams().then(function (res) {

			$scope.loadingTeams = false;

			if (angular.isObject(res.data.response)) {

				$scope.teams = res.data.response;

			} else {

				$scope.loadingTeamsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTeams = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPositions = function () {

		$scope.loadingPositions = true;
		$scope.loadingPositionsAlert = false;

		Team.getPositions().then(function (res) {

			$scope.loadingPositions = false;

			if (angular.isObject(res.data.response)) {

				$scope.positions = res.data.response

			} else {

				$scope.loadingPositionsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPositions = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPositionsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer(id_uniq);
	$scope.getPlayerCurrentTeam(id_uniq);
	$scope.getTeams();
	$scope.getPositions();

	$scope.$watch('formdata.current_team.discharge_date', function () {

		if ($scope.formdata.current_team.discharge_date != null) { $scope.checkCurrentTeamDates(); };

	});

	$scope.checkCurrentTeamDates = function () {

		$scope.wrongCurrentTeamDischargeDate = false;

		var admission = new Date($scope.formdata.current_team.admission_date).getTime();
		var discharge = new Date($scope.formdata.current_team.discharge_date).getTime();

		if (discharge <= admission) { $scope.wrongCurrentTeamDischargeDate = true };

	};

	$scope.$watch('formdata.admission_date', function () {

		$scope.checkDates();

	});

	$scope.$watch('formdata.discharge_date', function () {

		$scope.checkDates();

	});

	$scope.checkDates = function () {

		$scope.wrongDischargeDate = false;
        $scope.wrongAdmissionDate= false;

		var admission = new Date($scope.formdata.admission_date).getTime();
		var discharge = new Date($scope.formdata.discharge_date).getTime();
   
		if (discharge <= admission) $scope.wrongDischargeDate = true;

		var currentTeamDischarge = new Date($scope.formdata.current_team.discharge_date).getTime();
        if (admission <= currentTeamDischarge) $scope.wrongAdmissionDate = true;
	    if (discharge <= currentTeamDischarge) $scope.wrongDischargeDate = true;
	};

	$scope.addPosition = function () {

		$scope.showTooltip = false;

		var positionsData = {
			id_position: $scope.positionsdata.id_position,
			position: $scope.positions[$scope.positionsdata.id_position - 1].tx_name
		};

		var dataIndexToLookFor = {
			id_position: $scope.positionsdata.id_position
		};

		if (_.findIndex($scope.formdata.positions, dataIndexToLookFor) == -1) {

			$scope.formdata.positions.push(positionsData);
			$scope.positionsdata.id_position = '';

		} else {

			$scope.showTooltip = true;

			$timeout(function () {

				$scope.showTooltip = false;				

			}, 2000);

		}

	};

	$scope.deletePosition = function (index) {

		$scope.formdata.positions.splice(index, 1);

	};

	$scope.createPlayerTeam = function () {

		$scope.creatingTeam = true;
		$scope.creatingTeamAlert = false;

		var data = $.param($scope.formdata);

		Player.createTeam(id_uniq, data).then(function (res) {

			$scope.creatingTeam = false;

			if (res.data.status == "success") {

				STFcache.delete('players' + id_uniq + 'current_team');
				STFcache.delete('players' + id_uniq + 'teams');
				STFcache.delete('players' + id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Team created correctly'
				};

				$location.path('players/' + id_uniq + '/teams');

			} else {

				$scope.creatingTeamAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingTeam = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};
	
}]);

stf.controller('PlayerTeamEditController', ['$scope', '$routeParams', '$timeout', '$filter', '$rootScope', '$location', 'Player', 'Team', 'Auth', 'STFcache', function ($scope, $routeParams, $timeout, $filter, $rootScope, $location, Player, Team, Auth, STFcache) {
	
	 angular.element('input[type=number]').on("keypress",function (evt) {
    	    var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;

            

			});

	var id_uniq = $routeParams.id_uniq;
	var id_team_player = $routeParams.id_team_player;

	$scope.admissionDate = null;
	$scope.dischargeDate = null;
	$scope.maxDate = new Date();
	$scope.dateOptions = {
		format: 'mm/dd/yyyy',
		selectYears: 100,
		selectMonths: true
	};

	$scope.formdata = {
		team: '',
		admission_date: '',
		discharge_date: '',
		number: '',
		positions: []
	};

	$scope.positionsdata = {
		id_position: '',
		position: ''
	};

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingPlayer = true;
		$scope.loadingPlayerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			$scope.loadingPlayer = false;

			if (angular.isObject(res.data.response)) {

				$scope.player = res.data.response;

			} else {

				$scope.loadingPlayerAlert = {
					type: 'success',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.loadingPlayer = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.getPlayerTeam = function (id_uniq, id_team_player) {

		$scope.loadingPlayerTeam = true;
		$scope.loadingPlayerTeamAlert = false;

		Player.getTeam(id_uniq, id_team_player).then(function (res) {

			$scope.loadingPlayerTeam = false;

			if (angular.isObject(res.data.response)) {

				$scope.teamExist = true;
				$scope.playerTeam = res.data.response;
				
				$scope.formdata.team = $scope.playerTeam.id_team;
				$scope.formdata.admission_date = $filter('date')($scope.playerTeam.dt_admission, 'MM/dd/yyyy');
				$scope.formdata.discharge_date = $filter('date')($scope.playerTeam.dt_discharge, 'MM/dd/yyyy');
				$scope.formdata.number = parseInt($scope.playerTeam.nu_dorsal);

				angular.forEach($scope.playerTeam.positions, function (value, key) {

					$scope.formdata.positions.push({
						id_position: value.id_position,
						position: value.tx_name
					});

				})

			} else {

				$scope.loadingPlayerTeamAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPlayerTeam = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPlayerTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTeams = function () {

		$scope.loadingTeams = true;
		$scope.loadingTeamsAlert = false;

		Team.getTeams().then(function (res) {

			$scope.loadingTeams = false;

			if (angular.isObject(res.data.response)) {

				$scope.teams = res.data.response;

			} else {

				$scope.loadingTeamsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTeams = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPositions = function () {

		$scope.loadingPositions = true;
		$scope.loadingPositionsAlert = false;

		Team.getPositions().then(function (res) {

			$scope.loadingPositions = false;

			if (angular.isObject(res.data.response)) {

				$scope.positions = res.data.response

			} else {

				$scope.loadingPositionsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingPositions = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingPositionsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer(id_uniq);
	$scope.getPlayerTeam(id_uniq, id_team_player);
	$scope.getTeams();
	$scope.getPositions();

	$scope.$watch('formdata.admission_date', function () {

		if ($scope.formdata.discharge_date != null) { $scope.checkDates(); };

	});

	$scope.$watch('formdata.discharge_date', function () {

		if ($scope.formdata.discharge_date != null) { $scope.checkDates(); };

	});

	$scope.checkDates = function () {

		$scope.wrongDischargeDate = false;

		var admission = new Date($scope.formdata.admission_date).getTime();
		var discharge = new Date($scope.formdata.discharge_date).getTime();

		if (discharge <= admission) $scope.wrongDischargeDate = true;

	};

	$scope.addPosition = function () {

		$scope.showTooltip = false;

		var positionsData = {
			id_position: $scope.positionsdata.id_position,
			position: $scope.positions[$scope.positionsdata.id_position - 1].tx_name
		};

		var dataIndexToLookFor = {
			id_position: $scope.positionsdata.id_position
		};

		if (_.findIndex($scope.formdata.positions, dataIndexToLookFor) == -1) {

			$scope.formdata.positions.push(positionsData);
			$scope.positionsdata.id_position = '';

		} else {

			$scope.showTooltip = true;

			$timeout(function () {

				$scope.showTooltip = false;				

			}, 2000);

		}

	};

	$scope.deletePosition = function (index) {

		$scope.formdata.positions.splice(index, 1);

	};

	$scope.editPlayerTeam = function () {

		$scope.editingTeam = true;
		$scope.editingTeamAlert = false;

		var data = $.param($scope.formdata);

		Player.editTeam(id_uniq, id_team_player, data).then(function (res) {

			$scope.editingTeam = false;

			if (res.data.status == "success") {

				STFcache.delete('players' + id_uniq + 'teams' + id_team_player);
				STFcache.delete('players' + id_uniq + 'teams');
				STFcache.delete('players' + id_uniq);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Team updated correctly'
				};

				$location.path('players/' + id_uniq + '/teams');

			} else {

				$scope.editingTeamAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.editingTeam = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};
	
}]);
var stf = angular.module('StationConfigControllers', []);

stf.controller('StationConfigListController', ['$scope', 'Station', 'Access', 'Auth', 'STFcache', function ($scope, Station, Access, Auth, STFcache) {
	 
   $scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};


	$scope.getAllConfigurations = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('stationsConfigAll');
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.configurations = null;
		$scope.loadingConfigurations = true;
		$scope.loadingConfigurationsAlert = false;

		Station.getConfigurations().then(function (res) {

			$scope.loadingConfigurations = false;

			if (angular.isObject(res.data.response)) {

				$scope.configurations = res.data.response;
				//converting age string into number
	               res.data.response.forEach(function(configurations){
	            configurations.nu_min_age= parseFloat(configurations.nu_min_age);
	           });
				console.log($scope.configurations);
				
			} else {

				$scope.loadingConfigurationsAlert = {
					type: 'success',
					message: res.data.response
				};

			}
			$scope.getAccess(1);
		}, function (err) {

			$scope.loadingConfigurations = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingConfigurationsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAllConfigurations();

	$scope.orderOptions = [
		{ value: 'station_name', text: 'Station' },
		{ value: 'test_name', text: 'Test' },
		{ value: '-nu_min_age', text: 'Age Range' },
		{ value: 'nu_distance', text: 'Distance' }
	];
    
    $scope.arrange="";
			$scope.$watch('o', function() {
		    //alert('changed');
		    $scope.arrange="";
		    console.log($scope.o);
		    if($scope.o=="-nu_min_age"){
		    	$scope.arrange="'-nu_min_age'";
		    	console.log($scope.arrange);
		    }
		});
	$scope.deleteStationConfiguration = function(id_config){

			if(confirm("Are you sure to delete this Configuration?")){
				var data = $.param({id_config:id_config,type:'delete'});
				Station.delete(data).then(function (res) {
					$scope.loadingConfigurations = false;
					
					$scope.getAllConfigurations(true);
					if (angular.isObject(res.data.response)) {
						//$scope.measurements = res.data.response;

					} else {
						$scope.loadingConfigurationsAlert = {type: 'success',message: res.data.response};
					}

				}, function (err) {

					$scope.loadingConfigurations = false;

					if (err.status == 401) {
						Auth.deleteToken();
					} else {
						$scope.loadingConfigurationsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
					}
				});
			}
		};

	$scope.bulkDeleteStationConfiguration = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Configuration?")){
			var data = $.param({id_config:values,type:'delete',action:action_id});
			Station.multidelete(data).then(function (res) {
				$scope.loadingConfigurations = false;
				
				$scope.getAllConfigurations(true);
				if (angular.isObject(res.data.response)) {
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingConfigurationsAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingConfigurations = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingConfigurationsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one to delete.");
	 }
	};


	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

}]);

stf.controller('StationConfigCreateController', ['$scope', '$location', '$rootScope', 'Station', 'Age', 'Auth', 'STFcache',  function ($scope, $location, $rootScope, Station, Age, Auth, STFcache) {
	
	$scope.formdata = {
		station: '',
		test: '',
		age_range: '',
		distance: ''
	};

	$scope.getStationsAbleToConfigure = function () {

		$scope.loadingStations = true;
		$scope.loadingStationsALert = false;

		Station.getStationsAbleToConfigure().then(function (res) {

			$scope.loadingStations = false;

			if (angular.isObject(res.data.response)) {

				$scope.stations = res.data.response;
				$scope.$emit('stationsLoaded');
				//only take numbers
					angular.element('input[type=number]').on("keypress",function (evt) {
	    	    // if (String.fromCharCode(evt.which) == "e"){
	        	// return false;
	         //    }
	         //    if (String.fromCharCode(evt.which) == "."){
	        	// return false;
	         //    }
	            var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;

				});

			} else {

				$scope.loadingStationsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStations = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStationsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAgeRange = function () {

		$scope.loadingRange = true;
		$scope.loadingRangeAlert = false;

		Age.getRange().then(function (res) {

			$scope.loadingRange = false;

			if (angular.isObject(res.data.response)) {

				$scope.range = res.data.response;

			} else {

				$scope.loadingRangeAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingRange = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingRangeAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTests = function (id_station) {

		$scope.loadingTests = true;
		$scope.loadingTestsAlert = false;

		Station.getTests(id_station).then(function (res) {

			$scope.loadingTests = false;

			if (angular.isObject(res.data.response)) {

				$scope.tests = res.data.response;
                

			} else {

				$scope.loadingTestsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTests = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTestsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getStationsAbleToConfigure();
	$scope.getAgeRange();

	$scope.$on('stationsLoaded', function () {

		$scope.$watch('formdata.station', function () {

			var stationIndex = _.findIndex($scope.stations, { id: $scope.formdata.station });

			if (stationIndex != -1) {

				var id_station = $scope.formdata.station;
				$scope.getTests(id_station);

			}

			$scope.tests = null;
			$scope.formdata.test = '';
			$scope.CreateStationConfigurationForm.test.$setPristine();

		});

	});

	$scope.createStationConfiguration = function () {

		$scope.creatingConfiguration = true;
		$scope.creatingConfigurationAlert = false;

		var data = $.param($scope.formdata);
        
		Station.createConfiguration(data).then(function (res) {

			$scope.creatingConfiguration = false;

			if (res.data.status == "success") {

				STFcache.delete('stationsConfigAll');

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Configuration created successfully'
				};

				$location.path('/events/stations/config');

			} else {

				$scope.creatingConfigurationAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingConfiguration = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingConfigurationAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('StationConfigEditController', ['$scope', '$routeParams', '$location', '$rootScope', 'Station', 'Age', 'Auth', 'STFcache',  function ($scope, $routeParams, $location, $rootScope, Station, Age, Auth, STFcache) {
	
	var id = $routeParams.id;

	$scope.formdata = {
		station: '',
		test: '',
		age_range: '',
		distance: ''
	};

	$scope.getConfiguration = function (id) {

		$scope.loadingConfiguration = true;
		$scope.loadingConfigurationAlert = false;

		Station.getConfiguration(id).then(function (res) {

			$scope.loadingConfiguration = false;

			if (angular.isObject(res.data.response)) {

				$scope.configuration = res.data.response;
				$scope.formdata.station = $scope.configuration.id_station;
				$scope.formdata.test = $scope.configuration.id_test;
				$scope.formdata.age_range = $scope.configuration.id_age_range;
				$scope.formdata.distance = parseFloat($scope.configuration.nu_distance);
				
				angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
         //    if (String.fromCharCode(evt.which) == "."){
	        // 	return false;
	        //     }
	        var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;
            

			});

			} else {

				$scope.loadingConfigurationAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingConfiguration = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingConfigurationAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getStationsAbleToConfigure = function () {

		$scope.loadingStations = true;
		$scope.loadingStationsALert = false;

		Station.getStationsAbleToConfigure().then(function (res) {

			$scope.loadingStations = false;

			if (angular.isObject(res.data.response)) {

				$scope.stations = res.data.response;
				$scope.$emit('stationsLoaded');

			} else {

				$scope.loadingStationsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStations = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStationsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getAgeRange = function () {

		$scope.loadingRange = true;
		$scope.loadingRangeAlert = false;

		Age.getRange().then(function (res) {

			$scope.loadingRange = false;

			if (angular.isObject(res.data.response)) {

				$scope.range = res.data.response;

			} else {

				$scope.loadingRangeAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingRange = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingRangeAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTests = function (id_station) {

		$scope.loadingTests = true;
		$scope.loadingTestsAlert = false;

		Station.getTests(id_station).then(function (res) {

			$scope.loadingTests = false;

			if (angular.isObject(res.data.response)) {

				$scope.tests = res.data.response;

			} else {

				$scope.loadingTestsAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTests = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTestsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getConfiguration(id);
	$scope.getStationsAbleToConfigure();
	$scope.getAgeRange();

	$scope.$on('stationsLoaded', function () {

		$scope.$watch('formdata.station', function () {

			var stationIndex = _.findIndex($scope.stations, { id: $scope.formdata.station });

			if (stationIndex != -1) {

				var id_station = $scope.formdata.station;
				$scope.getTests(id_station);

			} else {

				$scope.tests = null;
				$scope.formdata.test = '';
				$scope.EditStationConfigurationForm.test.$setPristine();

			}

		});

	});

	$scope.editStationConfiguration = function () {

		$scope.editingConfiguration = true;
		$scope.editingConfigurationAlert = false;

		var data = $.param($scope.formdata);

		Station.editConfiguration(id, data).then(function (res) {

			$scope.editingConfiguration = false;

			if (res.data.status == "success") {

				STFcache.delete('stationsConfigAll');
				STFcache.delete('stationConfig' + id);

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Configuration updated successfully'
				};

				$location.path('/events/stations/config');

			} else {

				$scope.editingConfigurationAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.editingConfiguration = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingConfigurationAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);
var stf = angular.module('TeamControllers', []);


stf.controller('TeamListController', ['$scope', 'Team', 'Access', 'STFcache', 'Auth', function ($scope, Team, Access, STFcache, Auth) {
	
   $scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};


	$scope.getTeams = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('teams');
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.teams = null;
		$scope.loadingTeams = true;
		$scope.loadingTeamsAlert = false;

		Team.getTeams().then(function (res) {
			$scope.loadingTeams = false;
			if (angular.isObject(res.data.response)) {
				$scope.teams = res.data.response;
				
			} else {
				$scope.loadingTeamsAlert = {
					type: 'success',
					message: res.data.response
				};
			}
			$scope.getAccess(3);
		}, function (err) {

			$scope.loadingTeams = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTeams();

	$scope.deleteTeam = function( team_id, ind ){
		if(confirm("Are you sure to delete this Team?")){
			var data = $.param({id_team:team_id,type:'delete'});
			Team.delete(data).then(function (res) {
				$scope.loadingTeams = false;
				
				$scope.getTeams(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingTeamsAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {

				$scope.loadingTeams = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingTeamsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};

	$scope.bulkDeleteTeam = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Team?")){
			var data = $.param({id_team:values,type:'delete',action:action_id});
			Team.multidelete(data).then(function (res) {
				$scope.loadingTeams = false;
				
				$scope.getTeams(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingTeamsAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {

				$scope.loadingTeams = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingTeamsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one Team to delete.");
	 }
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
			changeAttr( $scope.teams, 'teams', $scope.access.bo_view );
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

	$scope.restrictView = function(){
		$('#alertMessage').show();
		setTimeout(function(){ 
			$('#alertMessage').hide();
		}, 2000);
	}

	function compile(element){
		var el = angular.element(element);    
		$scope = el.scope();
		$injector = el.injector();
		$injector.invoke(function($compile){
			$compile(el)($scope);
		})     
	}

	function changeAttr( obj, slug, viewAccess ){
        for( var i=0; i < obj.length; i++ ){
            var el = document.getElementById((slug.substring(0, slug.length-1))+"_id_"+i);
            if( viewAccess ){
                el.removeAttribute("ng-click");
                el.setAttribute("ng-href",slug+"/"+obj[i].id_uniq);
                el.setAttribute("href",slug+"/"+obj[i].id_uniq);
            } else {
                el.removeAttribute("ng-href");
                el.removeAttribute("href");
                el.setAttribute("ng-click", "restrictView()");
                compile(el);
            }
        }
    }


	$scope.orderOptions = [
		{ value: 'tx_name', text: 'Name' },
		{ value: 'tx_email', text: 'Email' },
		{ value: 'tx_phone', text: 'Phone number' }
	];

}]);

stf.controller('TeamCreateController', ['$scope', '$timeout', '$location', '$filter', '$rootScope', '$window', 'Country', 'Auth', 'Gender', 'Team', 'STFcache', function ($scope, $timeout, $location, $filter, $rootScope, $window, Country, Auth, Gender, Team, STFcache) {
	
	 angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
         //    if($(this).val().indexOf('.')!=-1){         
         //    if($(this).val().split(".")[1].length >1){ 
         //      //alert($(this).val().split(".")[1].length);
         //     // $(this).val()
         //    // alert("gy");
         //        //if( isNaN( parseFloat( this.value ) ) ) return;
         //        this.value = parseFloat($(this).val().slice(0, -1));
         //    }  
         // }    
           var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;
			});


	$scope.formdata = {
		name: '',
		country: '',
		state: '',
		city:'',
		address: '',
		phoneprefix: '-',
		phonenumber: '',
		zipcode: '',
		email: '',
		website: '',
		facebook: '',
		instagram: '',
		twitter: '',
		userfile: '',
		categories: []
	}

	$scope.categoriesdata = {
		id_category: '',
		category: '',
		id_gender: '',
		gender: '',
		nu_players: ''
	};

	$scope.zipcodeMask = "?";

	$scope.getCountries = function () {

		$scope.loadingCountries = true;
		$scope.loadingCountriesAlert = false;

		Country.getCountries().then(function (res) {

			$scope.loadingCountries = false;

			if (angular.isObject(res.data.response)) {

				$scope.countries = res.data.response;

			} else {

				$scope.loadingCountriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCountries = false;

			if (err.status == 401) { 

				Auth.deleteToken();

			} else {

				$scope.loadingCountriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountryStates = function (id_country) {

		$scope.loadingStates = true;
		$scope.loadingStatesAlert = false;

		Country.getCountryStates(id_country).then(function (res) {

			$scope.loadingStates = false;

			if (angular.isObject(res.data.response)) {

				$scope.states = res.data.response;

			} else {

				$scope.loadingStatesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStates = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStatesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCategories = function () {

		$scope.loadingCategories = true;
		$scope.loadingCategoriesAlert = false;

		Team.getCategories().then(function (res) {

			$scope.loadingCategories = false;

			if (angular.isObject(res.data.response)) {

				$scope.categories = res.data.response;

			} else {

				$scope.loadingCategoriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCategories = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingCategoriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getGenders = function () {

		$scope.loadingGenders = true;
		$scope.loadingGendersAlert = false;

		Gender.getGenders().then(function (res) {

			$scope.loadingGenders = false;

			if (angular.isObject(res.data.response)) {

				$scope.genders = res.data.response;

			} else {

				$scope.loadingGendersAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingGenders = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingGendersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};	

	$scope.getCountries();
	$scope.getCategories();
	$scope.getGenders();

	$scope.$watch('formdata.country', function () {

		var countryIndex = _.findIndex($scope.countries, { id: $scope.formdata.country });

		if (countryIndex != -1) {

			$scope.zipcodeMask = $filter('zipcodeMask')($scope.countries[countryIndex].tx_zipcode_format);
			$scope.zipcodeRequired = ($scope.zipcodeMask.length > 0) ? true : false;

			$scope.formdata.phoneprefix = $scope.countries[countryIndex].tx_phone_prefix;

			var id_country = $scope.formdata.country;
			$scope.getCountryStates(id_country);

		};

		$scope.states = null;
		$scope.formdata.state = '';
		$scope.CreateTeamForm.state.$setPristine();

	});

	$scope.addCategory = function () {

		$scope.showTooltip = false;

		var categoryData = {
			id_category: $scope.categoriesdata.id_category,
			category: $scope.categories[$scope.categoriesdata.id_category - 1].tx_name,
			id_gender: $scope.categoriesdata.id_gender,
			gender: $scope.genders[_.findIndex($scope.genders, { id: $scope.categoriesdata.id_gender })].tx_name,
			nu_players: $scope.categoriesdata.nu_players
		};

		var dataIndexToLookFor = {
			id_category: $scope.categoriesdata.id_category,
			id_gender: $scope.categoriesdata.id_gender
		};

		if (_.findIndex($scope.formdata.categories, dataIndexToLookFor) == -1) {

			$scope.formdata.categories.push(categoryData);

			$scope.categoriesdata.id_category = '',
			$scope.categoriesdata.category = '',
			$scope.categoriesdata.id_gender = '',
			$scope.categoriesdata.gender = '',
			$scope.categoriesdata.nu_players = ''

			$scope.TeamCategoriesForm.$setPristine();
			
		} else {

			$scope.showTooltip = true;

			$timeout(function () {

				$scope.showTooltip = false;				

			}, 2000);

		}

	};

	$scope.deleteCategory = function (index) {

		$scope.formdata.categories.splice(index, 1);

	};

	$scope.createTeam = function () {

		$scope.creatingTeam = true;
		$scope.creatingTeamAlert = false;

		var data = new FormData();

		data.append('name', $scope.formdata.name);
		data.append('country', $scope.formdata.country);
		data.append('state', $scope.formdata.state);
		data.append('city', $scope.formdata.city);
		data.append('address', $scope.formdata.address);
		data.append('phoneprefix', $scope.formdata.phoneprefix);
		data.append('phonenumber', $scope.formdata.phonenumber);
		data.append('zipcode', $scope.formdata.zipcode);
		data.append('email', $scope.formdata.email);
		data.append('website', $scope.formdata.website);
		data.append('facebook', $scope.formdata.facebook);
		data.append('instagram', $scope.formdata.instagram);
		data.append('twitter', $scope.formdata.twitter);
		data.append('userfile', $scope.formdata.userfile);
		data.append('categories', angular.toJson($scope.formdata.categories));

		Team.createTeam(data).then(function (res) {

			$scope.creatingTeam = false;

			if (res.data.status == "success") {

				STFcache.delete('teams');
				STFcache.delete('teamUniqIDs');

				if (res.data.has_image == 1) {

					$window.sessionStorage.setItem('response', res.data.response);

					$location.path('/set_image/tea_/' + res.data.id_uniq);

				} else {

					$rootScope.tooltipMessageAlert = {
						type: 'success',
						message: res.data.response
					}

					$location.path('/teams');

				}

			} else {

				$scope.creatingTeamAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingTeam = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('TeamDetailsController', ['$scope', '$routeParams', '$location', 'Team', 'Access', 'STFcache', 'Auth', function ($scope, $routeParams, $location, Team, Access, STFcache, Auth) {
	
	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getUniqIDs = function () {

		$scope.loadingUniqIDs = true;
		$scope.loadingUniqIDsAlert = false;

		Team.getUniqIDs().then(function (res) {

			$scope.loadingUniqIDs = false;

			$scope.result = res.data.response;
			$scope.totalItems = parseInt($scope.result.length);
			$scope.currentItem = parseInt(_.findIndex($scope.result, { id_uniq: $scope.id_uniq }));
			$scope.nextItem = $scope.currentItem + 1;	
			$scope.prevItem = $scope.currentItem - 1;

			$scope.nextID = ($scope.nextItem > -1 && $scope.nextItem < $scope.totalItems) ? $scope.result[$scope.nextItem].id_uniq : false;
			$scope.prevID = ($scope.prevItem >= 0) ? $scope.result[$scope.prevItem].id_uniq : false;

		}, function (err) {

			$scope.loadingUniqIDs = false;

			if (err.status == 401) {

				Auth.deleteToken();

			};

		});

	};

	$scope.prevTeam = function () { $location.path('/teams/' + $scope.prevID); };
	$scope.nextTeam = function () { $location.path('/teams/' + $scope.nextID); };

	$scope.getTeam = function (id_uniq) {

		$scope.loadingTeam = true;
		$scope.loadingTeamAlert = false;

		Team.getTeam(id_uniq).then(function (res) {

			$scope.loadingTeam = false;

			if (angular.isObject(res.data.response)) {

				$scope.team = res.data.response;
				$scope.getUniqIDs();
				$scope.getAccess(3);

			} else {

				$scope.loadingTeamAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTeam = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getTeam($scope.id_uniq);

	$scope.getTeams = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('teams');

		$scope.teams = null;
		$scope.loadingTeams = true;
		$scope.loadingTeamsAlert = false;

		Team.getTeams().then(function (res) {
			$scope.loadingTeams = false;
			if (angular.isObject(res.data.response)) {
				$scope.teams = res.data.response;
				$scope.getAccess(3);
			} else {
				$scope.loadingTeamsAlert = {
					type: 'success',
					message: res.data.response
				};
			}
		}, function (err) {

			$scope.loadingTeams = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamsAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.deleteTeam = function( team_id, ind ){
		if(confirm("Are you sure to delete this Team?")){
			var data = $.param({id_team:team_id,type:'delete'});
			Team.delete(data).then(function (res) {
				$scope.loadingTeams = false;
				
				$scope.getTeams(true);

				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingTeamsAlert = {type: res.data.status,message: res.data.response};
				   $location.path('/teams');
				}

			}, function (err) {

				$scope.loadingTeams = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingTeamsAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

}]);

stf.controller('TeamEditController', ['$scope', '$timeout', '$location', '$filter', '$routeParams', '$rootScope', '$window', 'Country', 'Auth', 'Gender', 'Team', 'STFcache', function ($scope, $timeout, $location, $filter, $routeParams, $rootScope, $window, Country, Auth, Gender, Team, STFcache) {
	
	 angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
         //     if (String.fromCharCode(evt.which) == "."){
        	// return false;
         //    }
         	var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;

			});

	$scope.formdata = {
		name: '',
		country: '',
		state: '',
		city:'',
		address: '',
		phoneprefix: '-',
		phonenumber: '',
		zipcode: '',
		email: '',
		website: '',
		facebook: '',
		instagram: '',
		twitter: '',
		userfile: '',
		categories: []
	}

	$scope.categoriesdata = {
		id_category: '',
		category: '',
		id_gender: '',
		gender: '',
		nu_players: ''
	};

	$scope.zipcodeMask = "?";

	var id_uniq = $routeParams.id_uniq;

	$scope.getTeam = function (id_uniq) {

		$scope.loadingTeam = true;
		$scope.loadingTeamAlert = false;

		Team.getTeam(id_uniq).then(function (res) {

			$scope.loadingTeam = false;

			if (angular.isObject(res.data.response)) {

				$scope.team = res.data.response;

				$scope.formdata.name = $scope.team.tx_name;
				$scope.formdata.country = $scope.team.id_country;
				$scope.formdata.state = $scope.team.id_state;
				$scope.formdata.city = $scope.team.tx_city;
				$scope.formdata.address = $scope.team.tx_address;
				$scope.formdata.prefix = $scope.team.tx_phone_prefix;
				$scope.formdata.phonenumber = parseFloat($scope.team.tx_phone);
				$scope.formdata.email = $scope.team.tx_email;
				$scope.formdata.website = $scope.team.tx_website;
				$scope.formdata.facebook = $scope.team.tx_facebook;
				$scope.formdata.instagram = $scope.team.tx_instagram;
				$scope.formdata.twitter = $scope.team.tx_twitter;
				
				angular.forEach($scope.team.categories, function (value, key) {

					$scope.formdata.categories.push({
						id_category: value.id_category,
						category: value.name,
						id_gender: value.id_gender,
						gender: value.gender,
						nu_players: value.nu_players
					});

				});

			} else {

				$scope.loadingTeamAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingTeam = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingTeamAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountries = function () {

		$scope.loadingCountries = true;
		$scope.loadingCountriesAlert = false;

		Country.getCountries().then(function (res) {

			$scope.loadingCountries = false;

			if (angular.isObject(res.data.response)) {

				$scope.countries = res.data.response;
				$scope.$emit('countriesLoaded');

			} else {

				$scope.loadingCountriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCountries = false;

			if (err.status == 401) { 

				Auth.deleteToken();

			} else {

				$scope.loadingCountriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountryStates = function (id_country) {

		$scope.loadingStates = true;
		$scope.loadingStatesAlert = false;

		Country.getCountryStates(id_country).then(function (res) {

			$scope.loadingStates = false;

			if (angular.isObject(res.data.response)) {

				$scope.states = res.data.response;

			} else {

				$scope.loadingStatesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStates = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStatesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCategories = function () {

		$scope.loadingCategories = true;
		$scope.loadingCategoriesAlert = false;

		Team.getCategories().then(function (res) {

			$scope.loadingCategories = false;

			if (angular.isObject(res.data.response)) {

				$scope.categories = res.data.response;

			} else {

				$scope.loadingCategoriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCategories = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingCategoriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getGenders = function () {

		$scope.loadingGenders = true;
		$scope.loadingGendersAlert = false;

		Gender.getGenders().then(function (res) {

			$scope.loadingGenders = false;

			if (angular.isObject(res.data.response)) {

				$scope.genders = res.data.response;

			} else {

				$scope.loadingGendersAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingGenders = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingGendersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};	

	$scope.getTeam(id_uniq);
	$scope.getCountries();
	$scope.getCategories();
	$scope.getGenders();

	$scope.$on('countriesLoaded', function () {

		$scope.$watch('formdata.country', function () {

			var countryIndex = _.findIndex($scope.countries, { id: $scope.formdata.country });

			if (countryIndex != -1) {

				$scope.zipcodeMask = $filter('zipcodeMask')($scope.countries[countryIndex].tx_zipcode_format);
				$scope.zipcodeRequired = ($scope.zipcodeMask.length > 0) ? true : false;

				$timeout(function () { $scope.formdata.zipcode = $scope.team.tx_zipcode; }, 1000);

				$scope.formdata.phoneprefix = $scope.countries[countryIndex].tx_phone_prefix;

				var id_country = $scope.formdata.country;
				$scope.getCountryStates(id_country);

			} else {

				$scope.states = null;
				$scope.formdata.state = '';
				$scope.EditTeamForm.state.$setPristine();

			}

		});

	})

	$scope.addCategory = function () {

		$scope.showTooltip = false;

		var categoryData = {
			id_category: $scope.categoriesdata.id_category,
			category: $scope.categories[$scope.categoriesdata.id_category - 1].tx_name,
			id_gender: $scope.categoriesdata.id_gender,
			gender: $scope.genders[_.findIndex($scope.genders, { id: $scope.categoriesdata.id_gender })].tx_name,
			nu_players: $scope.categoriesdata.nu_players
		};

		var dataIndexToLookFor = {
			id_category: $scope.categoriesdata.id_category,
			id_gender: $scope.categoriesdata.id_gender
		};

		if (_.findIndex($scope.formdata.categories, dataIndexToLookFor) == -1) {

			$scope.formdata.categories.push(categoryData);

			$scope.categoriesdata.id_category = '',
			$scope.categoriesdata.category = '',
			$scope.categoriesdata.id_gender = '',
			$scope.categoriesdata.gender = '',
			$scope.categoriesdata.nu_players = ''

			$scope.TeamCategoriesForm.$setPristine();
			
		} else {

			$scope.showTooltip = true;

			$timeout(function () {

				$scope.showTooltip = false;				

			}, 2000);

		}

	};

	$scope.deleteCategory = function (index) {

		$scope.formdata.categories.splice(index, 1);

	};

	$scope.editTeam = function () {

		$scope.editingTeam = true;
		$scope.editingTeamAlert = false;

		var data = new FormData();

		data.append('name', $scope.formdata.name);
		data.append('country', $scope.formdata.country);
		data.append('state', $scope.formdata.state);
		data.append('city', $scope.formdata.city);
		data.append('address', $scope.formdata.address);
		data.append('phoneprefix', $scope.formdata.phoneprefix);
		data.append('phonenumber', $scope.formdata.phonenumber);
		data.append('zipcode', $scope.formdata.zipcode);
		data.append('email', $scope.formdata.email);
		data.append('website', $scope.formdata.website);
		data.append('facebook', $scope.formdata.facebook);
		data.append('instagram', $scope.formdata.instagram);
		data.append('twitter', $scope.formdata.twitter);
		data.append('userfile', $scope.formdata.userfile);
		data.append('categories', angular.toJson($scope.formdata.categories));

		Team.editTeam(id_uniq, data).then(function (res) {

			$scope.editingTeam = false;
			
			if (res.data.status == "success") {

				STFcache.delete('teams');
				STFcache.delete('teams' + id_uniq);

				if (res.data.has_image == 1) {

					$window.sessionStorage.setItem('response', res.data.response);

					$location.path('/set_image/tea_/' + res.data.id_uniq);

				} else {

					$rootScope.tooltipMessageAlert = {
						type: 'success',
						message: res.data.response
					}

					$location.path('/teams');

				}

			} else {

				$scope.editingTeamAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.editingTeam = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingTeamAlert = {
					type: 'danger',
					message: err.status + ' ' +err.statusText
				}

			}

		});

	};
}]);
var stf = angular.module('UploadControllers', []);

stf.controller('ProfilePictureUploadController', ['$scope', '$location', '$routeParams', '$rootScope', 'Team', 'Player', 'User', 'Upload', 'STFcache', 'Auth', function ($scope, $location, $routeParams, $rootScope, Team, Player, User, Upload, STFcache, Auth) {
	
	var owners = ['tea_', 'pla_', 'use_'];
	var owner = $routeParams.owner;
	var id_uniq = $routeParams.uniqid;
	var recentlyCreated = $routeParams.recently_created || undefined;
	var redirectURL;
	var cacheToDelete;

	$scope.canUpload = false;
	$scope.formdata = {
		owner: owner,
		id_uniq: id_uniq,
		userfile: '',
		xaxis: '',
		yaxis: '',
		width: '',
		height: ''
	};

	$scope.playerdata = {
		id_uniq: id_uniq,
		head: {
			file: '',
			xaxis: '',
			yaxis: '',
			width: '',
			height: ''
		},
		body: {
			file: '',
			xaxis: '',
			yaxis: '',
			width: '',
			height: ''
		}
	};

	$scope.getTeam = function (id_uniq) {

		$scope.loadingOwner = true;
		$scope.loadingOwnerAlert = false;

		Team.getTeam(id_uniq).then(function (res) {

			$scope.loadingOwner = false;

			if (angular.isObject(res.data.response)) {

				$scope.canUpload = true;
				$scope.isTeam = true;
				$scope.team = res.data.response;

			} else {

				$scope.loadingOwnerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingOwner = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingOwnerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getPlayer = function (id_uniq) {

		$scope.loadingOwner = true;
		$scope.loadingOwnerAlert = false;

		Player.getOne(id_uniq).then(function (res) {

			$scope.loadingOwner = false;

			if (angular.isObject(res.data.response)) {

				$scope.isPlayer = true;
				$scope.player = res.data.response;

			} else {

				$scope.loadingOwnerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingOwner = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingOwnerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	}

	$scope.getUser = function (id_uniq) {

		$scope.loadingOwner = true;
		$scope.loadingOwnerAlert = false;

		User.getOne(id_uniq).then(function (res) {

			$scope.loadingOwner = false;

			if (angular.isObject(res.data.response)) {

				$scope.canUpload = true;
				$scope.isUser = true;
				$scope.user = res.data.response;

			} else {

				$scope.loadingOwnerAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingOwner = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingOwnerAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	}

	if (_.indexOf(owners, owner) == -1) { $location.path('dashboard'); } else {

		switch (owner) {

			case 'tea_':
				$scope.getTeam(id_uniq);
				redirectURL = (recentlyCreated !== undefined && recentlyCreated == 1) ? 'teams' : 'teams/' + id_uniq;
				$rootScope.tooltipMessageAlert = (recentlyCreated !== undefined && recentlyCreated == 1) ? { type: 'success', message: 'Team created successfully' } : false;
				cacheToDelete = {
					single: 'teams' + id_uniq,
					all: 'teams'
				};
				break;
			case 'pla_':
				$scope.getPlayer(id_uniq);
				redirectURL = (recentlyCreated !== undefined && recentlyCreated == 1) ? 'players' : 'players/' + id_uniq;
				$rootScope.tooltipMessageAlert = (recentlyCreated !== undefined && recentlyCreated == 1) ? { type: 'success', message: 'Player created successfully' } : false;
				cacheToDelete = {
					single: 'players' + id_uniq,
					all: 'players'
				};
				break;
			case 'use_': 
				$scope.getUser(id_uniq);
				redirectURL = (recentlyCreated !== undefined && recentlyCreated == 1) ? 'users' : 'users/' + id_uniq;
				$rootScope.tooltipMessageAlert = (recentlyCreated !== undefined && recentlyCreated == 1) ? { type: 'success', message: 'User created successfully' } : false;
				cacheToDelete = {
					single: 'users' + id_uniq,
					all: 'users'
				};
				break;
			default:
				break;

		}

	}

	$scope.cancelUpload = function () {

		STFcache.delete(cacheToDelete.single);
		STFcache.delete(cacheToDelete.all);

		$location.url(redirectURL);

	};

	$scope.uploadProfilePicture = function () {

		$scope.uploadingPicture = true;
		$scope.uploadingPictureAlert = false;

		var data = new FormData();

		data.append('owner', $scope.formdata.owner);
		data.append('id_uniq', $scope.formdata.id_uniq);
		data.append('userfile', $scope.formdata.userfile);
		data.append('xaxis', $scope.formdata.xaxis);
		data.append('yaxis', $scope.formdata.yaxis);
		data.append('width', $scope.formdata.width);
		data.append('height', $scope.formdata.height);

		Upload.uploadProfilePicture(data).then(function (res) {

			$scope.uploadingPicture = false;

			if (res.data.status == 'success') {

				STFcache.delete(cacheToDelete.single);
				STFcache.delete(cacheToDelete.all);

				$location.url(redirectURL);

			} else {

				$scope.uploadingPictureAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err){

			$scope.uploadingPicture = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.uploadingPictureAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});	

	};

	$scope.uploadPlayerPictures = function () {

		$scope.uploadingPlayerPictures = true;
		$scope.uploadingPlayerPicturesAlert = false;

		var data = new FormData();

		data.append('id_uniq', $scope.playerdata.id_uniq);
		data.append('head[file]', $scope.playerdata.head.file);
		data.append('head[xaxis]', $scope.playerdata.head.xaxis);
		data.append('head[yaxis]', $scope.playerdata.head.yaxis);
		data.append('head[width]', $scope.playerdata.head.width);
		data.append('head[height]', $scope.playerdata.head.height);
		data.append('body[file]', $scope.playerdata.body.file);
		data.append('body[xaxis]', $scope.playerdata.body.xaxis);
		data.append('body[yaxis]', $scope.playerdata.body.yaxis);
		data.append('body[width]', $scope.playerdata.body.width);
		data.append('body[height]', $scope.playerdata.body.height);

		Upload.uploadPlayerPictures(data).then(function (res) {

			$scope.uploadingPlayerPictures = false;

			if (res.data.status == "success") {

				STFcache.delete(cacheToDelete.single);
				STFcache.delete(cacheToDelete.all);

				$location.url(redirectURL);

			} else {

				$scope.uploadingPlayerPicturesAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.uploadingPlayerPictures = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.uploadingPlayerPicturesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}])
var stf = angular.module('UserControllers', []);

stf.controller('UserListController', ['$scope', 'User', 'Section', 'Access', 'Auth', 'STFcache', function ($scope, User, Section, Access, Auth, STFcache) {
     
  
   $scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());

    });
    
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};


	$scope.getUsers = function (refresh) {
        
		if (refresh !== undefined && refresh == true) STFcache.delete('users');
         
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.users = null;
		$scope.loadingUsers = true;
		$scope.loadingUsersAlert = false;
        
		User.getAll().then(function (res) {

			$scope.loadingUsers = false;
           
			if (angular.isObject(res.data.response)) {
				$scope.users = res.data.response;
                
				$scope.getAccess(5);
				
			} else {
				$scope.loadingUsersAlert = {
					type: 'success',
					message: res.data.response
				};
			}
			$scope.getAccess(5);
		}, function (err) {

			$scope.loadingUsers = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingUsersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getUsers();
    
	$scope.deleteUser = function( user_id, ind ){

		if(confirm("Are you sure to delete this User?")){
			var data = $.param({id_user:user_id,type:'delete'});
			User.delete(data).then(function (res) {
				
				var loggedInUser=$scope.loggedInUser.id_uniq;
				
				//if delete its own account session deleted
				if(user_id==loggedInUser){
					Auth.deleteToken();
				}
				
				$scope.loadingUsers = false;
				
				$scope.getUsers(true);
				if (angular.isObject(res.data.response)) {
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingUsersAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingUsers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingUsersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};

	$scope.bulkDeleteUser = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);

       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this User?")){
			var data = $.param({id_user:values,type:'delete',action:action_id});
			User.multiDelete(data).then(function (res) {
                
                //if delete its own account session deleted
				var loggedInUser=$scope.loggedInUser.id_uniq;
				console.log(values.indexOf(loggedInUser));
				if(values.indexOf(loggedInUser) !== -1) {
				     Auth.deleteToken();
				  }
				
				
				$scope.loadingUsers = false;
				
				$scope.getUsers(true);
				if (angular.isObject(res.data.response)) {
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingUsersAlert = {type: 'success',message: res.data.response};
				}

			}, function (err) {

				$scope.loadingUsers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingUsersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one user to delete.");
	 }
	};
   

	$scope.getAccess = function(id){
		//console.log($scope.users);
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
			changeAttr( $scope.users, 'users', $scope.access.bo_view );
		
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}
    
   
	$scope.restrictView = function(){
		$('#alertMessage').show();
		setTimeout(function(){ 
			$('#alertMessage').hide();
		}, 2000);
	}

	function compile(element){
		var el = angular.element(element);    
		$scope = el.scope();
		$injector = el.injector();
		$injector.invoke(function($compile){
			$compile(el)($scope);
		})     
	}

	function changeAttr( obj, slug, viewAccess ){
        for( var i=0; i < obj.length; i++ ){
            var el = document.getElementById((slug.substring(0, slug.length-1))+"_id_"+i);
            if( viewAccess ){
                el.removeAttribute("ng-click");
                el.setAttribute("ng-href",slug+"/"+obj[i].id_uniq);
                el.setAttribute("href",slug+"/"+obj[i].id_uniq);
            } else {
                el.removeAttribute("ng-href");
                el.removeAttribute("href");
                el.setAttribute("ng-click", "restrictView()");
                compile(el);
            }
        }
    }


	$scope.orderOptions = [
		{ value: 'tx_name', text: 'Full Name' },
		{ value: 'tx_email', text: 'Email' },
		{ value: 'bo_admin', text: 'Role' }
	];
		 $scope.arrange=false;
			$scope.$watch('o', function() {
		    //alert('changed');
		    $scope.arrange=false;
		    console.log($scope.o);
		    if($scope.o=="bo_admin"){
		    	$scope.arrange=true;
		    }
		});

}]);

stf.controller('UserCreateController', ['$scope', '$rootScope', '$location', '$window', 'User', 'Section', 'Auth', 'STFcache', function ($scope, $rootScope, $location, $window, User, Section, Auth, STFcache) {

	$scope.formdata = {
		firstname: '',
		lastname: '',
		email: '',
		password: '',
		userfile: '',
		is_admin: '',
		sections: function(){
			var arr = [];
			Section.getSection().then(function(res){
				angular.forEach(res.data.response, function(value, key) {
					arr.push({
						id: value.id,
						name: value.section_name,
						view: '0',
						add: '0',
						edit: '0',
						'delete': '0'
					});
				});
			});
			return arr;
		}()
	};

	$scope.adminOptions = [
		{ value: 0, text: 'Collaborator' },
		{ value: 1, text: 'Administrator' }
	];
	
	$scope.$watch('formdata.is_admin', function () {
		$scope.formdata.has_admin = parseInt( $scope.formdata.is_admin ) ? true : false;

		angular.forEach( $scope.formdata.sections, function( value, key ){
			$scope.formdata.sections[key].view      = ($scope.formdata.is_admin == 1) ? '1' : '1';
			$scope.formdata.sections[key].add       = ($scope.formdata.is_admin == 1) ? '1' : '0';
			$scope.formdata.sections[key].edit      = ($scope.formdata.is_admin == 1) ? '1' : '0';
			$scope.formdata.sections[key]['delete'] = ($scope.formdata.is_admin == 1) ? '1' : '0';

		});
	});

	$scope.createUser = function () {

		$scope.creatingUserAlert = false;
		$scope.creatingUser = true;

		var data = new FormData();

		data.append('firstname', $scope.formdata.firstname);
		data.append('lastname', $scope.formdata.lastname);
		data.append('email', $scope.formdata.email);
		data.append('password', $scope.formdata.password);
		data.append('delete', $scope.formdata.delete);
		data.append('is_admin', $scope.formdata.is_admin);
		data.append('userfile', $scope.formdata.userfile);
		data.append('sections', JSON.stringify($scope.formdata.sections));

		/*angular.forEach( $scope.formdata.sections, function( value, key ){
			
		});*/

		User.createUser(data).then(function (res) {

			$scope.creatingUser = false;
			if (res.data.status == "success") {

				STFcache.delete('users');
				STFcache.delete('userUniqIDs');
				
				if (res.data.has_image == 1) {

					$window.sessionStorage.setItem('response', res.data.response);

					$location.path('/set_image/use_/' + res.data.id_uniq);

				} else {

					$rootScope.tooltipMessageAlert = {
						type: 'success',
						message: res.data.response
					}

					$location.path('/users');

				}
				
			} else {

				$scope.creatingUserAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.creatingUser = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingUserAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.setCheckBox = function(idx){
		if( idx === 0 ){
			if( parseInt( $scope.formdata.sections[idx].add ) || 
				parseInt( $scope.formdata.sections[idx].edit ) ||
				parseInt( $scope.formdata.sections[idx].delete ) )
				$scope.formdata.sections[idx].view = "1";
		}
	};
}]);

stf.controller('UserDetailsController', ['$scope', '$routeParams', '$location', 'User', 'Access', 'Auth','STFcache', function ($scope, $routeParams, $location, User, Access, Auth,STFcache) {
	
	$scope.id_uniq = $routeParams.id_uniq;
    


	$scope.getUniqIDs = function () {

		$scope.loadingUniqIDs = true;
		$scope.loadingUniqIDsAlert = false;

		User.getUniqIDs().then(function (res) {

			$scope.loadingUniqIDs = false;

			$scope.result = res.data.response;
			$scope.totalItems = parseInt($scope.result.length);
			$scope.currentItem = parseInt(_.findIndex($scope.result, { id_uniq: $scope.id_uniq }));
			$scope.nextItem = $scope.currentItem + 1;	
			$scope.prevItem = $scope.currentItem - 1;

			$scope.nextID = ($scope.nextItem > -1 && $scope.nextItem < $scope.totalItems) ? $scope.result[$scope.nextItem].id_uniq : false;
			$scope.prevID = ($scope.prevItem >= 0) ? $scope.result[$scope.prevItem].id_uniq : false;

		}, function (err) {

			$scope.loadingUniqIDs = false;

			if (err.status == 401) {

				Auth.deleteToken();

			};

		});

	};

	$scope.prevUser = function () { $location.path('/users/' + $scope.prevID); };
	$scope.nextUser = function () { $location.path('/users/' + $scope.nextID); };

	$scope.getUser = function (id_uniq) {

		$scope.loadingUser = true;
		$scope.loadingUserAlert = false;

		User.getOne(id_uniq).then(function (res) {

			$scope.loadingUser = false;

			if (angular.isObject(res.data.response)) {

				$scope.user = res.data.response;
				$scope.getUniqIDs();
				$scope.getAccess(5);

			} else {

				$scope.loadingUserAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingUser = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingUserAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getUser($scope.id_uniq);

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

	$scope.deleteUser = function( user_id, ind ){
       // console.log(user_id);
		if(confirm("Are you sure to delete this User?")){
			var data = $.param({id_user:user_id,type:'delete'});
			User.delete(data).then(function (res) {
				$scope.loadingUsers = false;
				
				$scope.getUsers(true);
				if (angular.isObject(res.data.response)) {
					//$scope.measurements = res.data.response;
                   
				} else {
					$scope.loadingUsersAlert = {type: 'success',message: res.data.response};
				    $location.path('/users');
				}

			}, function (err) {

				$scope.loadingUsers = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingUsersAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};
   

	$scope.getUsers = function (refresh) {
        
		if (refresh !== undefined && refresh == true) STFcache.delete('users');
        
		$scope.users = null;
		$scope.loadingUsers = true;
		$scope.loadingUsersAlert = false;
        
		User.getAll().then(function (res) {

			$scope.loadingUsers = false;
           
			if (angular.isObject(res.data.response)) {
				$scope.users = res.data.response;
                
				$scope.getAccess(5);
				
			} else {
				$scope.loadingUsersAlert = {
					type: 'success',
					message: res.data.response
				};
			}
			$scope.getAccess(5);
		}, function (err) {

			$scope.loadingUsers = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingUsersAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('UserEditController', ['$scope', '$rootScope', '$location', '$routeParams', '$window', 'User', 'Section', 'Auth', 'STFcache', function ($scope, $rootScope, $location, $routeParams, $window, User, Section, Auth, STFcache) {



	$scope.formdata = {
		firstname: '',
		lastname: '',
		email: '',
		password: '',
		userfile: '',
		is_admin: '',
		sections: function(){
			var arr = [];
			Section.getSection().then(function(res){
				angular.forEach(res.data.response, function(value, key) {
					arr.push({
						id: value.id,
						name: value.section_name,
						view: '0',
						add: '0',
						edit: '0',
						'delete': '0'
					});
				});
			});
			return arr;
		}()
	};

	$scope.adminOptions = [
		{ value: '0', text: 'Collaborator' },
		{ value: '1', text: 'Administrator' }
	];

	
	var id_uniq = $routeParams.id_uniq;

	$scope.getUser = function (id_uniq) {

		$scope.loadingUser = true;
		$scope.loadingUserAlert = false;

		User.getOne(id_uniq).then(function (res) {

			$scope.loadingUser = false;

			if (angular.isObject(res.data.response)) {
				$scope.user = res.data.response;
				$scope.formdata.firstname = $scope.user.tx_firstname;
				$scope.formdata.lastname  = $scope.user.tx_lastname;
				$scope.formdata.email     = $scope.user.tx_email;
				$scope.formdata.is_admin  = $scope.user.bo_admin;
				$scope.formdata.sections  = $scope.user.sections;
				$scope.formdata.user_type  = $scope.user.bo_admin;
				$scope.$emit('userLoaded');
			} else {
				$scope.loadingUserAlert = {
					type: 'success',
					message: res.data.response
				};
			}
		}, function (err) {
			$scope.loadingUser = false;
			if (err.status == 401) {
				Auth.deleteToken();
			} else {
				$scope.loadingUserAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};
			}
		});
	};

	$scope.getUser(id_uniq);

	$scope.$on('userLoaded', function () {
		$scope.changeUserType(0);
	});

	$scope.editUser = function () {

		$scope.editingUserAlert = false;
		$scope.editingUser = true;

		var data = new FormData();

		data.append('firstname', $scope.formdata.firstname);
		data.append('lastname', $scope.formdata.lastname);
		data.append('email', $scope.formdata.email);
		data.append('password', $scope.formdata.password);
		data.append('is_admin', $scope.formdata.is_admin);
		data.append('userfile', $scope.formdata.userfile);
		data.append('sections', JSON.stringify($scope.formdata.sections));
		
		
		User.editUser(id_uniq, data).then(function (res) {

			$scope.editingUser = false;
			
			if (res.data.status == "success") {

				STFcache.delete('users');
				STFcache.delete('users' + id_uniq);

				if (res.data.has_image == 1) {

					$window.sessionStorage.setItem('response', res.data.response);

					$location.path('/set_image/use_/' + res.data.id_uniq);

				} else {

					$rootScope.tooltipMessageAlert = {
						type: 'success',
						message: res.data.response
					}

					$location.path('/users');

				}

			} else {

				$scope.editingUserAlert = {
					type: 'danger',
					message: res.data.response
				}

			}

		}, function (err) {

			$scope.editingUser = false;
			
			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingUserAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				}

			}

		});

	};

	$scope.setCheckBox = function(idx){
		if( idx === 0 ){
			if( parseInt( $scope.formdata.sections[idx].add ) || 
				parseInt( $scope.formdata.sections[idx].edit ) ||
				parseInt( $scope.formdata.sections[idx].delete ) )
				$scope.formdata.sections[idx].view = "1";
		}
	};
    

	$scope.changeUserType = function(type){
		$scope.formdata.has_admin = parseInt( $scope.formdata.is_admin ) ? true : false;
		angular.forEach( $scope.formdata.sections, function( value, key ){
			$scope.formdata.sections[key].view      = type === 0 ? value.view : "1";
			$scope.formdata.sections[key].add       = type === 0 ? value.add : $scope.formdata.is_admin;
			$scope.formdata.sections[key].edit      = type === 0 ? value.edit : $scope.formdata.is_admin;
			$scope.formdata.sections[key]['delete'] = type === 0 ? value['delete'] : $scope.formdata.is_admin;
		});
	};

}]);
var stf = angular.module('VenueControllers', []);

stf.controller('VenueListController', ['$scope', 'Venue', 'Access', 'Auth', 'STFcache', function ($scope, Venue, Access, Auth, STFcache) {
	
	$scope.bulk=false;
   var	values=[];
     $scope.toggle=function(){
      if(angular.element("#mastercheckbox").is(":checked")){
      	angular.element(".childcheckbox").prop("checked",true);
      	values=[];
    $(".childcheckbox").each(function(){
        if($(this).is(":checked"))
            values.push($(this).val());
    });
    //alert(values);
      }
      else{
      	values=[];
    // alert(values);
      	angular.element(".childcheckbox").prop("checked",false);
      }
      
     }

	$scope.togglechild=function(e){
		//console.log("hh");
		 values=[];
		$(".childcheckbox").each(function(){
	        if($(this).is(":checked"))
	            values.push($(this).val());
	    });
	    //alert(values);
	   if(angular.element(".childcheckbox").length== angular.element(".childcheckbox:checked").length){
	   	angular.element("#mastercheckbox").prop("checked",true);
	   } 
	   else{
	
	    angular.element("#mastercheckbox").prop("checked",false);
	      }
	};

	$scope.getVenues = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('venues');
        
        angular.element("#mastercheckbox").prop("checked",false); 
		$scope.venues = null;
		$scope.loadingVenues = true;
		$scope.loadingVenuesAlert = false;

		Venue.getVenues().then(function (res) {

			$scope.loadingVenues = false;

			if (angular.isObject(res.data.response)) {

				$scope.venues = res.data.response;
				$scope.getAccess(4);

			} else {

				$scope.loadingVenuesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingVenues = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingVenuesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getVenues();

	$scope.deleteVenue = function( venue_id, ind ){

		if(confirm("Are you sure to delete this Venue?")){
			var data = $.param({id_venue:venue_id,type:'delete'});
			Venue.delete(data).then(function (res) {
				$scope.loadingVenues = false;
				
				$scope.getVenues(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingVenuesAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {

				$scope.loadingVenues = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingVenuesAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};

	$scope.bulkDeleteVenue = function( ){

       //console.log(angular.element('#bulkaction').val());
       console.log(values);
       action_id=angular.element('#bulkaction').val();
       if(action_id==1 && values.length !=0){
		if(confirm("Are you sure to delete this Venue?")){
			var data = $.param({id_venue:values,type:'delete',action:action_id});
			Venue.multidelete(data).then(function (res) {
				$scope.loadingVenues = false;
				
				$scope.getVenues(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingVenuesAlert = {type: res.data.status,message: res.data.response};
				}

			}, function (err) {

				$scope.loadingVenues = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingVenuesAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	 }
	 else{
	 	alert("Please select atleast one Venue to delete.");
	 }
	};

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
			changeAttr( $scope.venues, 'venues', $scope.access.bo_view );
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}

	$scope.restrictView = function(){
		$('#alertMessage').show();
		setTimeout(function(){ 
			$('#alertMessage').hide();
		}, 2000);
	}

	function compile(element){
		var el = angular.element(element);    
		$scope = el.scope();
		$injector = el.injector();
		$injector.invoke(function($compile){
			$compile(el)($scope);
		})     
	}

	function changeAttr( obj, slug, viewAccess ){
        for( var i=0; i < obj.length; i++ ){
            var el = document.getElementById((slug.substring(0, slug.length-1))+"_id_"+i);
            if( viewAccess ){
                el.removeAttribute("ng-click");
                el.setAttribute("ng-href",slug+"/"+obj[i].id_uniq);
                el.setAttribute("href",slug+"/"+obj[i].id_uniq);
            } else {
                el.removeAttribute("ng-href");
                el.removeAttribute("href");
                el.setAttribute("ng-click", "restrictView()");
                compile(el);
            }
        }
    }


	$scope.orderOptions = [
		{ value: 'tx_name', text: 'Name' },
		{ value: 'tx_email', text: 'Email' },
		{ value: 'tx_phone', text: 'Phone number' },
		{ value: 'tx_contact_name', text: 'Contact Person' }
	];

}]);

stf.controller('VenueCreateController', ['$scope', '$filter', '$location', '$rootScope', '$timeout', 'Country', 'Auth', 'Venue', 'STFcache', function ($scope, $filter, $location, $rootScope, $timeout, Country, Auth, Venue, STFcache) {
    
     angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
         //    if (String.fromCharCode(evt.which) == "."){
        	// return false;
         //    }
         	var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;

			});

	$scope.formdata = {
		name: '',
		country: '',
		state: '',
		city: '',
		address: '',
		zipcode: '',
		phoneprefix_one: '-',
		phonenumber_one: '',
		phoneprefix_two: '-',
		phonenumber_two: '',
		email: '',
		website: '',
		facebook: '',
		instagram: '',
		twitter: '',
		contact_person: '',
		contact_person_phoneprefix: '-',
		contact_person_phonenumber: '',
		latitude: 40.148237,
		longitude: -101.762500
	};

	$timeout(function () {

		$scope.map = {
			center: {
				latitude: $scope.formdata.latitude,
				longitude: $scope.formdata.longitude
			},
			zoom: 4
		};

		$scope.marker = {
			id: 0,
			options: {
				draggable: true
			},
			coords: {
				latitude: $scope.map.center.latitude,
				longitude: $scope.map.center.longitude
			},
			events: {
				dragend: function (marker, eventName, args) {

					$scope.formdata.latitude = marker.getPosition().lat();
					$scope.formdata.longitude = marker.getPosition().lng();

				}
			}
		};

		$scope.searchBox = {
			template: 'dist/views/templates/google.maps.searchbox.html',
			events: {
				places_changed: function (searchbox) {

					var place = searchbox.getPlaces();

					var lat = place[0].geometry.location.lat();
					var lng = place[0].geometry.location.lng();

					$scope.formdata.latitude = lat;
					$scope.formdata.longitude = lng;

					$scope.map.center.latitude = lat;
					$scope.map.center.longitude	= lng;

					$scope.marker.coords.latitude = lat;
					$scope.marker.coords.longitude	= lng;

					$scope.map.zoom = 17;

				}
			}
		};

		$scope.renderMap = true;

	}, 500);

	$scope.getCountries = function () {

		$scope.loadingCountries = true;
		$scope.loadingCountriesAlert = false;

		Country.getCountries().then(function (res) {

			$scope.loadingCountries = false;

			if (angular.isObject(res.data.response)) {

				$scope.countries = res.data.response;

			} else {

				$scope.loadingCountriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCountries = false;

			if (err.status == 401) { 

				Auth.deleteToken();

			} else {

				$scope.loadingCountriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountryStates = function (id_country) {

		$scope.loadingStates = true;
		$scope.loadingStatesAlert = false;

		Country.getCountryStates(id_country).then(function (res) {

			$scope.loadingStates = false;

			if (angular.isObject(res.data.response)) {

				$scope.states = res.data.response;

			} else {

				$scope.loadingStatesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStates = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStatesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountries();

	$scope.$watch('formdata.country', function () {

		var countryIndex = _.findIndex($scope.countries, { id: $scope.formdata.country });

		if (countryIndex != -1) {

			$scope.zipcodeMask = $filter('zipcodeMask')($scope.countries[countryIndex].tx_zipcode_format);
			$scope.zipcodeRequired = ($scope.zipcodeMask.length > 0) ? true : false;

			$scope.formdata.phoneprefix_one = $scope.countries[countryIndex].tx_phone_prefix;
			$scope.formdata.phoneprefix_two = $scope.countries[countryIndex].tx_phone_prefix;
			$scope.formdata.contact_person_phoneprefix = $scope.countries[countryIndex].tx_phone_prefix;

			var id_country = $scope.formdata.country;
			$scope.getCountryStates(id_country);

		};

		$scope.states = null;
		$scope.formdata.state = '';
		$scope.CreateVenueForm.state.$setPristine();

	});

	$scope.createVenue = function () {

		$scope.creatingVenue = true;
		$scope.creatingVenueAlert = false;

		var data = $.param($scope.formdata);

		Venue.createVenue(data).then(function (res) {

			$scope.creatingVenue = false;

			if (res.data.status == "success") {

				STFcache.delete('venues');
				STFcache.delete('venueUniqIDs');

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Venue created successfully'
				};

				$location.path('venues');

			} else {

				$scope.creatingVenueAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.creatingVenue = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.creatingVenueAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

}]);

stf.controller('VenueDetailsController', ['$scope', '$routeParams', '$timeout', '$location', 'Venue', 'Access', 'Auth','STFcache',function ($scope, $routeParams, $timeout, $location, Venue, Access, Auth,STFcache) {
	
	$scope.id_uniq = $routeParams.id_uniq;

	$scope.getUniqIDs = function () {

		$scope.loadingUniqIDs = true;
		$scope.loadingUniqIDsAlert = false;

		Venue.getUniqIDs().then(function (res) {

			$scope.loadingUniqIDs = false;

			$scope.result = res.data.response;
			$scope.totalItems = parseInt($scope.result.length);
			$scope.currentItem = parseInt(_.findIndex($scope.result, { id_uniq: $scope.id_uniq }));
			$scope.nextItem = $scope.currentItem + 1;	
			$scope.prevItem = $scope.currentItem - 1;

			$scope.nextID = ($scope.nextItem > -1 && $scope.nextItem < $scope.totalItems) ? $scope.result[$scope.nextItem].id_uniq : false;
			$scope.prevID = ($scope.prevItem >= 0) ? $scope.result[$scope.prevItem].id_uniq : false;

		}, function (err) {

			$scope.loadingUniqIDs = false;

			if (err.status == 401) {

				Auth.deleteToken();

			};

		});

	};

	$scope.prevVenue = function () { $location.path('/venues/' + $scope.prevID); };
	$scope.nextVenue = function () { $location.path('/venues/' + $scope.nextID); };

	$scope.getVenue = function (id_uniq) {

		$scope.loadingVenue = true;
		$scope.loadingVenueAlert = false;

		Venue.getVenue(id_uniq).then(function (res) {

			$scope.loadingVenue = false;

			if (angular.isObject(res.data.response)) {

				$scope.venue = res.data.response;
				$scope.getUniqIDs();

				$scope.$emit('venueLoaded');
				$scope.getAccess(4);
												
			} else {

				$scope.loadingVenueAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingVenue = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingVenueAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};
	$scope.getVenues = function (refresh) {

		if (refresh !== undefined && refresh == true) STFcache.delete('venues');

		$scope.venues = null;
		$scope.loadingVenues = true;
		$scope.loadingVenuesAlert = false;

		Venue.getVenues().then(function (res) {

			$scope.loadingVenues = false;

			if (angular.isObject(res.data.response)) {

				$scope.venues = res.data.response;
				$scope.getAccess(4);

			} else {

				$scope.loadingVenuesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingVenues = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingVenuesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};


	$scope.$on('venueLoaded', function () {

		$timeout(function () {

			$scope.map = {
				center: {
					latitude: $scope.venue.tx_latitude,
					longitude: $scope.venue.tx_longitude
				},
				zoom: 17
			};

			$scope.marker = {
				id: 0,
				coords: {
					latitude: $scope.map.center.latitude,
					longitude: $scope.map.center.longitude
				},
				events: {
					dragend: function (marker, eventName, args) {

						$scope.formdata.latitude = marker.getPosition().lat();
						$scope.formdata.longitude = marker.getPosition().lng();

					}
				}
			};

			$scope.searchBox = {
				template: 'dist/views/templates/google.maps.searchbox.html',
				events: {
					places_changed: function (searchbox) {

						var place = searchbox.getPlaces();

						var lat = place[0].geometry.location.lat();
						var lng = place[0].geometry.location.lng();

						$scope.formdata.latitude = lat;
						$scope.formdata.longitude = lng;

						$scope.map.center.latitude = lat;
						$scope.map.center.longitude	= lng;

						$scope.marker.coords.latitude = lat;
						$scope.marker.coords.longitude	= lng;

					}
				}
			};

			$scope.renderMap = true;

		}, 500);

	});

	$scope.getVenue($scope.id_uniq);

	$scope.getAccess = function(id){
		Access.getAccess(id).then(function (response) {
			$scope.access = Access.formatData(response);
		}, function (err) {
			err.status == 401 ? Auth.deleteToken() : $scope.loggedInUser = err.status + ' ' + err.statusText;
		});
	}
   
   $scope.deleteVenue = function( venue_id, ind ){

		if(confirm("Are you sure to delete this Venue?")){
			var data = $.param({id_venue:venue_id,type:'delete'});
			Venue.delete(data).then(function (res) {
				$scope.loadingVenues = false;
				
				$scope.getVenues(true);
				if (angular.isObject(res.data.response)) {
					console.log(res.data.response);
					//$scope.measurements = res.data.response;

				} else {
					$scope.loadingVenuesAlert = {type: res.data.status,message: res.data.response};
					    $location.path('/venues');
				}

			}, function (err) {

				$scope.loadingVenues = false;

				if (err.status == 401) {
					Auth.deleteToken();
				} else {
					$scope.loadingVenuesAlert = { type: 'danger', message: err.status + ' ' + err.statusText };
				}
			});
		}
	};
}]);

stf.controller('VenueEditController', ['$scope', '$routeParams', '$rootScope', '$filter', '$timeout', '$location', 'Venue', 'Auth', 'Country', 'STFcache', 'uiGmapGoogleMapApi', function ($scope, $routeParams, $rootScope, $filter, $timeout, $location, Venue, Auth, Country, STFcache, uiGmapGoogleMapApi) {
    
     angular.element('input[type=number]').on("keypress",function (evt) {
    	    // if (String.fromCharCode(evt.which) == "e"){
        	// return false;
         //    }
         //     if (String.fromCharCode(evt.which) == "."){
        	// return false;
         //    }
			var charCode = (evt.which) ? evt.which : event.keyCode;
			//if(charCode==46)return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
			 return true;
			});
	var id_uniq = $routeParams.id_uniq;
	
	$scope.formdata = {
		name: '',
		country: '',
		state: '',
		city: '',
		address: '',
		zipcode: '',
		phoneprefix_one: '-',
		phonenumber_one: '',
		phoneprefix_two: '-',
		phonenumber_two: '',
		email: '',
		website: '',
		facebook: '',
		instagram: '',
		twitter: '',
		contact_person: '',
		contact_person_phoneprefix: '-',
		contact_person_phonenumber: '',
		latitude: '',
		longitude: ''
	};

	$scope.getVenue = function (id_uniq) {

		$scope.loadingVenue = true;
		$scope.loadingVenueAlert = false;

		Venue.getVenue(id_uniq).then(function (res) {

			$scope.loadingVenue = false;

			if (angular.isObject(res.data.response)) {

				$scope.venue = res.data.response;
                 
				$scope.formdata.name = $scope.venue.venue_name;
				$scope.formdata.country = $scope.venue.id_country;
				$scope.formdata.state = $scope.venue.id_state;
				$scope.formdata.city = $scope.venue.id_city;
				$scope.formdata.address = $scope.venue.tx_address;
				$scope.formdata.phoneprefix_one = $scope.venue.tx_phone_prefix;
				$scope.formdata.phonenumber_one = parseFloat($scope.venue.tx_phone);
				$scope.formdata.phoneprefix_two = $scope.venue.tx_phone_prefix_2;
				$scope.formdata.phonenumber_two = parseFloat($scope.venue.tx_phone_2);
				$scope.formdata.email = $scope.venue.tx_email;
				$scope.formdata.website = $scope.venue.tx_website;
				$scope.formdata.facebook = $scope.venue.tx_facebook;
				$scope.formdata.instagram = $scope.venue.tx_instagram;
				$scope.formdata.twitter = $scope.venue.tx_twitter;
				$scope.formdata.contact_person = $scope.venue.tx_contact_name;
				$scope.formdata.contact_person_phoneprefix = $scope.venue.tx_contact_phone_prefix;
				$scope.formdata.contact_person_phonenumber = parseFloat($scope.venue.tx_contact_phone);
				$scope.formdata.latitude = $scope.venue.tx_latitude;
				$scope.formdata.longitude = $scope.venue.tx_longitude;

				$scope.$emit('venueLoaded');

			} else {

				$scope.loadingVenueAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingVenue = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingVenueAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountries = function () {

		$scope.loadingCountries = true;
		$scope.loadingCountriesAlert = false;

		Country.getCountries().then(function (res) {

			$scope.loadingCountries = false;

			if (angular.isObject(res.data.response)) {

				$scope.countries = res.data.response;
				$scope.$emit('countriesLoaded');

			} else {

				$scope.loadingCountriesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingCountries = false;

			if (err.status == 401) { 

				Auth.deleteToken();

			} else {

				$scope.loadingCountriesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getCountryStates = function (id_country) {

		$scope.loadingStates = true;
		$scope.loadingStatesAlert = false;

		Country.getCountryStates(id_country).then(function (res) {

			$scope.loadingStates = false;

			if (angular.isObject(res.data.response)) {

				$scope.states = res.data.response;

			} else {

				$scope.loadingStatesAlert = {
					type: 'success',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.loadingStates = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.loadingStatesAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};

	$scope.getVenue(id_uniq);
	$scope.getCountries();

	$scope.$on('countriesLoaded', function () {

		$scope.$watch('formdata.country', function () {

			var countryIndex = _.findIndex($scope.countries, { id: $scope.formdata.country });

			if (countryIndex != -1) {

				$scope.zipcodeMask = $filter('zipcodeMask')($scope.countries[countryIndex].tx_zipcode_format);
				$scope.zipcodeRequired = ($scope.zipcodeMask.length > 0) ? true : false;
				
				$timeout(function () { $scope.formdata.zipcode = $scope.venue.tx_zipcode; }, 1000);

				$scope.formdata.phoneprefix_one = $scope.countries[countryIndex].tx_phone_prefix;
				$scope.formdata.phoneprefix_two = $scope.countries[countryIndex].tx_phone_prefix;
				$scope.formdata.contact_person_phoneprefix = $scope.countries[countryIndex].tx_phone_prefix;

				var id_country = $scope.formdata.country;
				$scope.getCountryStates(id_country);

			} else {

				$scope.states = null;
				$scope.formdata.state = '';
				$scope.EditVenueForm.state.$setPristine();

			}

		});

	});

	$scope.$on('venueLoaded', function () {

		$timeout(function () {

			$scope.map = {
				center: {
					latitude: $scope.venue.tx_latitude,
					longitude: $scope.venue.tx_longitude
				},
				zoom: 17
			};

			$scope.marker = {
				id: 0,
				options: {
					draggable: true
				},
				coords: {
					latitude: $scope.map.center.latitude,
					longitude: $scope.map.center.longitude
				},
				events: {
					dragend: function (marker, eventName, args) {

						$scope.formdata.latitude = marker.getPosition().lat();
						$scope.formdata.longitude = marker.getPosition().lng();

					}
				}
			};

			$scope.searchBox = {
				template: 'dist/views/templates/google.maps.searchbox.html',
				events: {
					places_changed: function (searchbox) {

						var place = searchbox.getPlaces();

						var lat = place[0].geometry.location.lat();
						var lng = place[0].geometry.location.lng();

						$scope.formdata.latitude = lat;
						$scope.formdata.longitude = lng;

						$scope.map.center.latitude = lat;
						$scope.map.center.longitude	= lng;

						$scope.marker.coords.latitude = lat;
						$scope.marker.coords.longitude	= lng;

					}
				}
			};

			$scope.renderMap = true;

		}, 500);

	});

	$scope.editVenue = function () {

		$scope.editingVenue = true;
		$scope.editingVenueAlert = false;

		var data = $.param($scope.formdata);

		Venue.editVenue(id_uniq, data).then(function (res) {

			$scope.editingVenue = false;

			if (res.data.status == "success") {

				STFcache.delete('venues' + id_uniq);
				STFcache.delete('venues');

				$rootScope.tooltipMessageAlert = {
					type: 'success',
					message: 'Venue updated successfully'
				};

				$location.path('venues');

			} else {

				$scope.editingVenueAlert = {
					type: 'danger',
					message: res.data.response
				};

			}

		}, function (err) {

			$scope.editingVenue = false;

			if (err.status == 401) {

				Auth.deleteToken();

			} else {

				$scope.editingVenueAlert = {
					type: 'danger',
					message: err.status + ' ' + err.statusText
				};

			}

		});

	};
	
}]);
var stf = angular.module('MainDirectives', []);

/*stf.directive('readImage', ['$timeout', function ($timeout) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			$(iElement).on('change', function () {

				scope.formdata.userfile = '';

				var preview = $('#img-preview');
				var file = $(iElement)[0].files[0];
				var reader = new FileReader();
				var types = ['image/png', 'image/jpeg'];

				preview.cropper('destroy');

				if (file) {

					if (_.indexOf(types, file.type) != -1) {

						scope.invalidFile = false;

						reader.onloadend = function () {

							preview.attr('src', reader.result);

						}
						reader.readAsDataURL(file);

						$timeout(function () {

							scope.$emit('imgReady');

						}, 500);

					} else {

						scope.invalidFile = true;
						preview.attr('src', '');

					}

				} else {

					preview.attr('src', '');

				}

				scope.$apply();

			});

		}
	};
}]);*/

stf.directive('removeChar', function() {
  return {
    require: 'ngModel',
    link: function (scope, element, attr, ngModelCtrl) {
      function fromUser(text) {
        var transformedInput = text.replace(/[^0-9]/g, '');
        if(transformedInput !== text) {
            ngModelCtrl.$setViewValue(transformedInput);
            ngModelCtrl.$render();
        }
        return transformedInput;  // or return Number(transformedInput)
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  }; 
});

stf.directive('cropper', [function () {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			scope.$on('imgReady', function () {

				$(iElement).cropper({
					aspectRatio: 1,
					dragCrop: true,
					cropBoxMovable: true,
					cropBoxResizable: true,
					minCropBoxWidth: 320,
					minCropBoxHeight: 320
				}).on('built.cropper', function () {

					var croppedData = $(iElement).cropper('getData');

					scope.formdata.xaxis = parseInt(croppedData.x);
					scope.formdata.yaxis = parseInt(croppedData.y);
					scope.formdata.width = parseInt(croppedData.width);
					scope.formdata.height = parseInt(croppedData.height);

				}).on('cropend.cropper', function (e) {

					var croppedData = $(iElement).cropper('getData');
					
					scope.formdata.xaxis = parseInt(croppedData.x);
					scope.formdata.yaxis = parseInt(croppedData.y);
					scope.formdata.width = parseInt(croppedData.width);
					scope.formdata.height = parseInt(croppedData.height);

				}).on('zoom.cropper', function (e) {

					var croppedData = $(iElement).cropper('getData');
					
					scope.formdata.xaxis = parseInt(croppedData.x);
					scope.formdata.yaxis = parseInt(croppedData.y);
					scope.formdata.width = parseInt(croppedData.width);
					scope.formdata.height = parseInt(croppedData.height);

				});

			});

		}
	};
}]);

stf.directive('fileModel', ['$parse', function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			var model = $parse(iAttrs.fileModel);
			var modelSetter = model.assign;

			iElement.bind('change', function () {

				scope.$apply(function () {

					modelSetter(scope, iElement[0].files[0]);

				});

			});

		}
	};
}]);

stf.directive('topbarMenu', [function () {
	return {
		templateUrl: 'dist/views/templates/topbar.menu.html',
		restrict: 'E',
		controller: 'topbarMenuController'
	};
}])

stf.directive('stfFooter', [function () {
	return {
		templateUrl: 'dist/views/templates/footer.html',
		replace: true,
		restrict: 'E'
	};
}])
var stf = angular.module('MovementDirectives', []);

stf.directive('movementLinear', ['$interval', '$timeout', '$document', function ($interval, $timeout, $document) {
	return {
		templateUrl: 'dist/views/templates/movement.linear.html',
		restrict: 'E',
		scope: {
			activetab: '=',
			playerid: '=',
			eventid: '='
		},
		controller: function ($scope, $interval, $filter, $routeParams, $rootScope, $location, Movement, Auth, STFcache) {

			var id_uniq = $routeParams.id_uniq;
			console.log($scope.playerid);
			$scope.init = function () {
				console.log("Init Started!!");
				console.log($scope.playerid);
				$scope.interval = false;
				$scope.stopwatch = new Date(Date.UTC(99, 11, 1, 0, 0, 0));
				$scope.totaltime = 0;
				$scope.attempts = 0;

				$scope.timerCompleted = false;
				$scope.lineardata = {
					id_player: $scope.playerid,
					id_event: $scope.eventid,
					measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
					checkpoints: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				};

			};

			$scope.resetTimer = function () {
				console.log("Hello");
				$interval.cancel($scope.interval);
				$scope.init();

			};

			$scope.$on('$destroy', function () { $interval.cancel($scope.interval); });

			$rootScope.$on('movementTestPlayerChanged', function () {

				$scope.resetTimer();

			});

			$scope.init();

			$scope.createMovementLinear = function () {

				$scope.creatingMovementLinear = true;
				$scope.creatingMovementLinearAlert = false;

				var data = $.param($scope.lineardata);

				Movement.createLinear(data).then(function (res) {

					$scope.creatingMovementLinear = false;

					if (res.data.status == "success") {

						STFcache.delete('movementEvent' + id_uniq);

						$rootScope.tooltipMessageAlert = {
							type: 'success',
							message: res.data.response
						};

						$location.path('/events/' + id_uniq + '/movement');

					} else {

						$scope.creatingMovementLinearAlert = {
							type: 'danger',
							message: res.data.response
						};

					}

				}, function (err) {

					$scope.creatingMovementLinear = false;

					if (err.status == 401) {

						Auth.deleteToken();

					} else {

						$scope.creatingMovementLinearAlert = {
							type: 'danger',
							message: err.status + ' ' + err.statusText
						};

					}
				});

			};

		},
		link: function (scope, iElement, iAttrs) {

			$document.on('keypress', function (e) {

				if (e.charCode == 115 && scope.activetab === "Linear") {

					if (scope.interval == false) {

						scope.d = new Date();

						scope.interval = $interval(function () {

							scope.now = new Date();
							scope.ellapsed = scope.now.getTime() - scope.d.getTime();
							scope.stopwatch = scope.ellapsed;

						}, 33);	

					} else {

						if (scope.attempts < 10) {

							if (scope.lineardata.checkpoints.length == 0) {

								scope.lineardata.checkpoints[scope.attempts] = scope.stopwatch;
								
							} else {

								var ellapsedTime = 0;

								angular.forEach(scope.lineardata.checkpoints, function (value, key) {

									ellapsedTime += value;

								});

								scope.lineardata.checkpoints[scope.attempts] = scope.stopwatch - ellapsedTime;
								
							}

							scope.attempts++;

						};

						if (scope.attempts == 10) {

							$interval.cancel(scope.interval);
							scope.totaltime += scope.stopwatch;
							scope.timerCompleted = true;

						};

					}

				};

			});

		}
	};
}]);

stf.directive('movementLateral', ['$interval', '$timeout', '$document', function ($interval, $timeout, $document) {
	return {
		templateUrl: 'dist/views/templates/movement.lateral.html',
		restrict: 'E',
		scope: {
			activetab: '=',
			playerid: '=',
			eventid: '='
		},
		controller: function ($scope, $interval, $filter, $routeParams, $rootScope, $location, Movement, Auth, STFcache) {

			var id_uniq = $routeParams.id_uniq;

			$scope.init = function () {

				$scope.interval = false;
				$scope.stopwatch = new Date(Date.UTC(99, 11, 1, 0, 0, 0));
				$scope.totaltime = 0;
				$scope.attempts = 0;

				$scope.timerCompleted = false;
				$scope.lateraldata = {
					id_player: $scope.playerid,
					id_event: $scope.eventid,
					measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
					checkpoints: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				};

			};

			$scope.resetTimer = function () {

				$interval.cancel($scope.interval);
				$scope.init();

			};

			$scope.$on('$destroy', function () { $interval.cancel($scope.interval); });

			$rootScope.$on('movementTestPlayerChanged', function () {

				$scope.resetTimer();

			})

			$scope.init();

			$scope.createMovementLateral = function () {

				$scope.creatingMovementLateral = true;
				$scope.creatingMovementLateralAlert = false;

				var data = $.param($scope.lateraldata);

				Movement.createLateral(data).then(function (res) {

					$scope.creatingMovementLateral = false;

					if (res.data.status == "success") {

						STFcache.delete('movementEvent' + id_uniq);

						$rootScope.tooltipMessageAlert = {
							type: 'success',
							message: res.data.response
						};

						$location.path('/events/' + id_uniq + '/movement');

					} else {

						$scope.creatingMovementLateralAlert = {
							type: 'danger',
							message: res.data.response
						};

					}

				}, function (err) {

					$scope.creatingMovementLateral = false;

					if (err.status == 401) {

						Auth.deleteToken();

					} else {

						$scope.creatingMovementLateralAlert = {
							type: 'danger',
							message: err.status + ' ' + err.statusText
						};

					}
				});

			};

		},
		link: function (scope, iElement, iAttrs) {

			$document.on('keypress', function (e) {

				if (e.charCode == 115 && scope.activetab === "Lateral") {

					if (scope.interval == false) {

						scope.d = new Date();

						scope.interval = $interval(function () {

							scope.now = new Date();
							scope.ellapsed = scope.now.getTime() - scope.d.getTime();
							scope.stopwatch = scope.ellapsed;

						}, 33);	

					} else {

						if (scope.attempts < 10) {

							if (scope.lateraldata.checkpoints.length == 0) {

								scope.lateraldata.checkpoints[scope.attempts] = scope.stopwatch;
								
							} else {

								var ellapsedTime = 0;

								angular.forEach(scope.lateraldata.checkpoints, function (value, key) {

									ellapsedTime += value;

								});

								scope.lateraldata.checkpoints[scope.attempts] = scope.stopwatch - ellapsedTime;
								
							}

							scope.attempts++;

						};

						if (scope.attempts == 10) {

							$interval.cancel(scope.interval);
							scope.totaltime += scope.stopwatch;
							scope.timerCompleted = true;

						};

					}

				};

			});

		}
	};
}]);

stf.directive('movementZigzag', ['$interval', '$timeout', '$document', function ($interval, $timeout, $document) {
	return {
		templateUrl: 'dist/views/templates/movement.zigzag.html',
		restrict: 'E',
		scope: {
			activetab: '=',
			playerid: '=',
			eventid: '='
		},
		controller: function ($scope, $interval, $filter, $routeParams, $rootScope, $location, Movement, Auth, STFcache) {

			var id_uniq = $routeParams.id_uniq;

			$scope.init = function () {

				$scope.interval = false;
				$scope.stopwatch = new Date(Date.UTC(99, 11, 1, 0, 0, 0));
				$scope.totaltime = 0;
				$scope.attempts = 0;

				$scope.timerCompleted = false;
				$scope.zigzagdata = {
					id_player: $scope.playerid,
					id_event: $scope.eventid,
					measurement_date: $filter('date')(new Date(), 'MM/dd/yyyy'),
					checkpoints: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				};

			};

			$scope.resetTimer = function () {

				$interval.cancel($scope.interval);
				$scope.init();

			};

			$scope.$on('$destroy', function () { $interval.cancel($scope.interval); });

			$rootScope.$on('movementTestPlayerChanged', function () {

				$scope.resetTimer();

			})

			$scope.init();

			$scope.createMovementZigzag = function () {

				$scope.creatingMovementZigzag = true;
				$scope.creatingMovementZigzagAlert = false;

				var data = $.param($scope.zigzagdata);

				Movement.createZigzag(data).then(function (res) {

					$scope.creatingMovementZigzag = false;

					if (res.data.status == "success") {

						STFcache.delete('movementEvent' + id_uniq);

						$rootScope.tooltipMessageAlert = {
							type: 'success',
							message: res.data.response
						};

						$location.path('/events/' + id_uniq + '/movement');

					} else {

						$scope.creatingMovementZigzagAlert = {
							type: 'danger',
							message: res.data.response
						};

					}

				}, function (err) {

					$scope.creatingMovementZigzag = false;

					if (err.status == 401) {

						Auth.deleteToken();

					} else {

						$scope.creatingMovementZigzagAlert = {
							type: 'danger',
							message: err.status + ' ' + err.statusText
						};

					}
				});

			};

		},
		link: function (scope, iElement, iAttrs) {

			$document.on('keypress', function (e) {

				if (e.charCode == 115 && scope.activetab === "Zigzag") {

					if (scope.interval == false) {

						scope.d = new Date();

						scope.interval = $interval(function () {

							scope.now = new Date();
							scope.ellapsed = scope.now.getTime() - scope.d.getTime();
							scope.stopwatch = scope.ellapsed;

						}, 33);	

					} else {

						if (scope.attempts < 10) {

							if (scope.zigzagdata.checkpoints.length == 0) {

								scope.zigzagdata.checkpoints[scope.attempts] = scope.stopwatch;
								
							} else {

								var ellapsedTime = 0;

								angular.forEach(scope.zigzagdata.checkpoints, function (value, key) {

									ellapsedTime += value;

								});

								scope.zigzagdata.checkpoints[scope.attempts] = scope.stopwatch - ellapsedTime;
								
							}

							scope.attempts++;

						};

						if (scope.attempts == 10) {

							$interval.cancel(scope.interval);
							scope.totaltime += scope.stopwatch;
							scope.timerCompleted = true;

						};

					}

				};

			});

		}
	};
}]);
var stf = angular.module('PlayerDirectives', []);

stf.directive('readHeadImage', ['$timeout', function ($timeout) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			$(iElement).on('change', function () {

				scope.playerdata.head.file = '';

				var preview = $('#headpicture-preview');
				var file = $(iElement)[0].files[0];
				var reader = new FileReader();
				var types = ['image/png', 'image/jpeg'];

				preview.cropper('destroy');

				if (file) {

					if (_.indexOf(types, file.type) != -1) {

						scope.invalidHeadPictureFile = false;

						reader.onloadend = function () {

							preview.attr('src', reader.result);

						}
						reader.readAsDataURL(file);

						$timeout(function () {

							scope.$emit('headImgReady');

						}, 500);

					} else {

						scope.invalidHeadPictureFile = true;
						preview.attr('src', '');

					}

				} else {

					preview.attr('src', '');

				}

				scope.$apply();

			});

		}
	};
}]);

stf.directive('readBodyImage', ['$timeout', function ($timeout) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			$(iElement).on('change', function () {

				scope.playerdata.body.file = '';

				var preview = $('#bodypicture-preview');
				var file = $(iElement)[0].files[0];
				var reader = new FileReader();
				var types = ['image/png', 'image/jpeg'];

				preview.cropper('destroy');

				if (file) {

					if (_.indexOf(types, file.type) != -1) {

						scope.invalidBodyPictureFile = false;

						reader.onloadend = function () {

							preview.attr('src', reader.result);

						}
						reader.readAsDataURL(file);

						$timeout(function () {

							scope.$emit('bodyImgReady');

						}, 500);

					} else {

						scope.invalidBodyPictureFile = true;
						preview.attr('src', '');

					}

				} else {

					preview.attr('src', '');

				}

				scope.$apply();

			});

		}
	};
}]);

stf.directive('headpictureCropper', [function () {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			scope.$on('headImgReady', function () {

				$(iElement).cropper({
					aspectRatio: 1,
					dragCrop: true,
					cropBoxMovable: true,
					cropBoxResizable: true,
					minCropBoxWidth: 320,
					minCropBoxHeight: 320
				}).on('built.cropper', function () {

					var croppedData = $(iElement).cropper('getData');
					
					scope.playerdata.head.xaxis = parseInt(croppedData.x);
					scope.playerdata.head.yaxis = parseInt(croppedData.y);
					scope.playerdata.head.width = parseInt(croppedData.width);
					scope.playerdata.head.height = parseInt(croppedData.height);

				}).on('cropend.cropper', function (e) {

					var croppedData = $(iElement).cropper('getData');
					
					scope.playerdata.head.xaxis = parseInt(croppedData.x);
					scope.playerdata.head.yaxis = parseInt(croppedData.y);
					scope.playerdata.head.width = parseInt(croppedData.width);
					scope.playerdata.head.height = parseInt(croppedData.height);

				}).on('zoom.cropper', function (e) {

					var croppedData = $(iElement).cropper('getData');
					
					scope.playerdata.head.xaxis = parseInt(croppedData.x);
					scope.playerdata.head.yaxis = parseInt(croppedData.y);
					scope.playerdata.head.width = parseInt(croppedData.width);
					scope.playerdata.head.height = parseInt(croppedData.height);

				});

			});

		}
	};
}]);

stf.directive('bodypictureCropper', [function () {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			
			scope.$on('bodyImgReady', function () {

				$(iElement).cropper({
					aspectRatio: 2 / 3,
					dragCrop: true,
					cropBoxMovable: true,
					cropBoxResizable: true,
					minCropBoxWidth: 220,
					minCropBoxHeight: 320
				}).on('built.cropper', function () {

					var croppedData = $(iElement).cropper('getData');
					
					scope.playerdata.body.xaxis = parseInt(croppedData.x);
					scope.playerdata.body.yaxis = parseInt(croppedData.y);
					scope.playerdata.body.width = parseInt(croppedData.width);
					scope.playerdata.body.height = parseInt(croppedData.height);

				}).on('cropend.cropper', function (e) {

					var croppedData = $(iElement).cropper('getData');
					
					scope.playerdata.body.xaxis = parseInt(croppedData.x);
					scope.playerdata.body.yaxis = parseInt(croppedData.y);
					scope.playerdata.body.width = parseInt(croppedData.width);
					scope.playerdata.body.height = parseInt(croppedData.height);

				}).on('zoom.cropper', function (e) {

					var croppedData = $(iElement).cropper('getData');
					
					scope.playerdata.body.xaxis = parseInt(croppedData.x);
					scope.playerdata.body.yaxis = parseInt(croppedData.y);
					scope.playerdata.body.width = parseInt(croppedData.width);
					scope.playerdata.body.height = parseInt(croppedData.height);

				});

			});

		}
	};
}]);
var stf = angular.module('MainFilters', []);

stf.filter('zipcodeMask', function () {

	return function (input) {

		var count = input.length;
		var result = '';

		for (var i = 0; i < count; i++) {
			
			if (input[i] == 'N') result += '9'
			else result += '';

		};

		return result;

	}

});

stf.filter('dateToAngular', function () {

	return function (input) {

		input = input || '';

		return input.replace(' ', 'T');

	}

});

stf.filter('currentAge', ['$filter', function ($filter) {

	return function (input) {

		var ageDifMs = Date.now() - new Date($filter('date')(input, 'yyyy/MM/dd'));
		var ageDate = new Date(ageDifMs);
		
		return Math.abs(ageDate.getUTCFullYear() - 1970);	

	}

}]);

var stf = angular.module('ForgotPasswordServices', []);

stf.factory('Forgot', ['$http', '$window', '$location', function ($http, $window, $location) {
	

	return {
		passwordforgot: function (data) {

			return $http.post('api/forgot/passwordforgot', data);

		}
	};
}]);

var stf = angular.module('AccessServices', []);

stf.factory('Access', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	return {
		getAccess: function ( section_id ) {
			return $http.get('api/access/access', {
				params: {
					section_id : section_id
				},
				headers: {
					token: $window.localStorage.token
				}
			}).then(function (response) {
				return response.data.response;
			});
		},

		formatData: function( data ){
			data.bo_view   = data.bo_view === "1";
			data.bo_add    = data.bo_add === "1";
			data.bo_edit   = data.bo_edit === "1";
			data.bo_delete = data.bo_delete === "1";
			data.bo_admin  = data.bo_admin === "1";
			return data;
		}
	};
}])
var stf = angular.module('AgeServices', []);

stf.factory('Age', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		getRange: function () {

			return $http.get('api/age/range', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('agerange')
			});

		}
	};
}])
var nv = angular.module('AnthropometricServices', []);

nv.factory('Anthropometric', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		check: function (id_player, id_event) {

			return $http.get('api/anthropometric/check', {
				params: {
					id_player: id_player,
					id_event: id_event
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		create: function (data) {

			return $http.post('api/anthropometric', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getAll: function (id_uniq_event) {

			return $http.get('api/anthropometric/event', {
				params: {
					id_uniq_event: id_uniq_event
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('anthropometricEvent' + id_uniq_event)
			});

		},
		getOne: function (id_uniq) {

			return $http.get('api/anthropometric', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('anthropometric' + id_uniq)
			});

		},
		edit: function (data, id_uniq) {

			return $http.put('api/anthropometric', data, {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getWeight: function (id_player, id_event) {

			return $http.get('api/anthropometric/player_weight', {
				params: {
					id_player: id_player,
					id_event: id_event
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		delete: function (data) {
			return $http.put('api/anthropometric/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put('api/anthropometric/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
	};
}])
var stf = angular.module('AuthServices', []);

stf.factory('Auth', ['$http', '$window', '$location', function ($http, $window, $location) {
	

	return {
		login: function (data) {

			return $http.post('api/auth', data);

		},
		createToken: function (data) {

			$window.localStorage.setItem('token', data);

		},
		checkToken: function () {

			if ($window.localStorage.token === undefined) { $location.path('/'); };

		},
		goToDashboardIfToken: function () {

			if ($window.localStorage.token !== undefined) { $location.path('/dashboard'); };

		},
		deleteToken: function () {

			$window.localStorage.removeItem('token');
			$location.path('/');

		}
	};
}]);
var stf = angular.module('CacheServices', []);

stf.factory('STFcache', ['$cacheFactory', function ($cacheFactory) {

	return {
		set: function (name) {

			if ($cacheFactory.get(name) !== undefined) return;

			return $cacheFactory(name);

		},
		delete: function (name) {

			if ($cacheFactory.get(name) === undefined) return;

			return $cacheFactory.get(name).destroy();

		},
		save: function (name) {

			this.set(name);

			return $cacheFactory.get(name);

		}
	};
}]);
var stf = angular.module('CountryServices', []);

stf.factory('Country', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		getCountries: function () {

			return $http.get('api/country', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('countries')
			});

		},
		getCountryStates: function (id_country) {

			return $http.get('api/country/states', {
				params: {
					id_country: id_country
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('states')
			});

		}
	};
}]);
var stf = angular.module('DribblingServices', []);

stf.factory('Dribbling', ['$http', '$window', function ($http, $window) {
	

	return {

	};
}])
var stf = angular.module('EventServices', []);
stf.factory('Event', ['$http', '$window', '$rootScope', 'STFcache', function ($http, $window, $rootScope, STFcache) {
	return {
		create: function (data) {
			return $http.post('api/event', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		getAll: function () {
			return $http.get('api/event', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('events')
			});
		},
		getOne: function (id_uniq) {
			return $http.get('api/event', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('events' + id_uniq)
			});
		},
		edit: function (id_uniq, data) {

			return $http.put('api/event', data, {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getUniqIDs: function () {

			return $http.get('api/event/uniqids', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('eventUniqIDs')
			});

		},
		delete: function (data) {
			return $http.put('api/event/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put('api/event/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

	};
}])
var stf = angular.module('GenderServices', []);

stf.factory('Gender', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		getGenders: function () {

			return $http.get('api/gender', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('genders')
			});

		}
	};
}]);
var stf = angular.module('HeadingServices', []);

stf.factory('Heading', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		check: function (id_player, id_event, data) {
			return $http.post('api/heading/check', data, {
				params: {
					id_player: id_player,
					id_event: id_event
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		createForce: function (data) {

			return $http.post('api/heading/force', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},

		updateForce: function (data) {
			return $http.put('api/heading/force', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		updateAim: function (data) {
			return $http.put('api/heading/aim', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		updateJump: function (data) {
			return $http.put('api/heading/jump', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		createAim: function (data) {
			return $http.post('api/heading/aim', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		createJump: function (data) {
			return $http.post('api/heading/jump', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		delete: function (data) {
			return $http.put('api/heading/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		multidelete: function (data) {
			return $http.put('api/heading/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},

		getAll: function (id_uniq_event) {

			return $http.get('api/heading/event', {
				params: {
					id_uniq_event: id_uniq_event
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('headingEvent' + id_uniq_event)
			});

		}, 

		getDetails: function (id_uniq_event, id_uniq_player ) {

			return $http.get('api/heading/force', {
				params: {
					id_uniq_event: id_uniq_event,
					id_uniq_player: id_uniq_player
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('headingEvent' + id_uniq_event)
			});

		}
	};
}])
var stf = angular.module('ImageServices', []);

stf.factory('Image', ['$http', '$window', function ($http, $window) {
	

	return {
		set: function (data) {

			return $http.post('api/image/set', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		playerSet: function (data) {

			return $http.post('api/image/player_set', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		}
	};
}])
var stf = angular.module('KickingServices', []);

stf.factory('Kicking', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		check: function (id_player, id_event, data) {

			return $http.post('api/kicking/check', data, {
				params: {
					id_player: id_player,
					id_event: id_event
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		createAim: function (data) {

			return $http.post('api/kicking/aim', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		updateAim: function (data) {

			return $http.put('api/kicking/aim', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		createForce: function (data) {

			return $http.post('api/kicking/force', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getAll: function (id_uniq_event) {

			return $http.get('api/kicking/event', {
				params: {
					id_uniq_event: id_uniq_event
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('kickingEvent' + id_uniq_event)
			});

		},

		delete: function (data) {
			return $http.put('api/kicking/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put('api/kicking/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		getDetails: function (id_uniq_event, id_uniq_player ) {

			return $http.get('api/kicking/aim', {
				params: {
					id_uniq_event: id_uniq_event,
					id_uniq_player: id_uniq_player
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('kickingEvent' + id_uniq_event)
			});

		}
	};
}])
var stf = angular.module('MeasurementServices', []);

stf.factory('Measurement', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		getMeasurements: function () {

			return $http.get('api/measurement', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.delete('measurements')
			});

		}
	};
}])
var stf = angular.module('MovementServices', []);

stf.factory('Movement', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		check: function (id_player, id_event, data) {

			return $http.post('api/movement/check', data, {
				params: {
					id_player: id_player,
					id_event: id_event
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		createLinear: function (data) {

			return $http.post('api/movement/linear', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		createLateral: function (data) {

			return $http.post('api/movement/lateral', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		createZigzag: function (data) {

			return $http.post('api/movement/zigzag', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getAll: function (id_uniq_event) {

			return $http.get('api/movement/event', {
				params: {
					id_uniq_event: id_uniq_event
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('movementEvent' + id_uniq_event)
			});

		},
		delete: function (data) {
			return $http.put('api/movement/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put('api/movement/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
	};
}])
var stf = angular.module('PlayerServices', []);

stf.factory('Player', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		create: function (data) {

			return $http.post('api/player', data, {
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'token': $window.localStorage.token
				}
			});

		},
		getOne: function (id_uniq) {

			return $http.get('api/player', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('players' + id_uniq)
			});

		},
		getAll: function () {

			return $http.get('api/player', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('players')
			});

		},
		edit: function (id_uniq, data) {

			return $http.post('api/player', data, {
				params: {
					id_uniq: id_uniq
				},
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'token': $window.localStorage.token
				}
			});

		},
		getCurrentTeam: function (id_uniq) {

			return $http.get('api/player/current_team', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('players' + id_uniq + 'current_team')
			});

		},
		createTeam: function (id_uniq, data) {

			return $http.post('api/player/team', data, {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getTeams: function (id_uniq) {

			return $http.get('api/player/teams', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('players' + id_uniq + 'teams')
			});

		},
		getTeam: function (id_uniq, id_team_player) {

			return $http.get('api/player/team', {
				params:{
					id_uniq: id_uniq,
					id_team_player: id_team_player
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('players' + id_uniq + 'teams' + id_team_player)
			});

		},
		editTeam: function (id_uniq, id_team_player, data) {

			return $http.put('api/player/team', data, {
				params:{
					id_uniq: id_uniq,
					id_team_player: id_team_player
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getUniqIDs: function () {

			return $http.get('api/player/uniqids', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('playerUniqIDs')
			});

		},
		getBaseImages: function (id_uniq) {

			return $http.get('api/player/base_images', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getBaseHeadImage: function (id_uniq) {

			return $http.get('api/player/base_head_image', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getBaseBodyImage: function (id_uniq) {

			return $http.get('api/player/base_body_image', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},

		delete: function (data) {
			return $http.put('api/player/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		deleteTeam : function (data) {
			return $http.put('api/player/deleteteam', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put('api/player/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multideleteTeam: function (data) {
			return $http.put('api/player/multideleteteam', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		}

	};
}]);
var stf = angular.module('SectionServices', []);
stf.factory('Section', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	return {
		getSection: function () {
			return $http.get('api/section/section', {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}])
var stf = angular.module('StationConfigServices', []);

stf.factory('Station', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		getStationsAbleToConfigure: function () {

			return $http.get('api/station/able', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('stationsAbleToConfigure')
			});

		},
		createConfiguration: function (data) {

			return $http.post('api/station/configuration', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		editConfiguration: function (id, data) {

			return $http.put('api/station/configuration', data, {
				params: {
					id: id
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getConfiguration: function (id) {

			return $http.get('api/station/configuration', {
				params: {
					id: id
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('stationConfig' + id)
			});

		},
		getConfigurations: function () {

			return $http.get('api/station/configuration', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('stationsConfigAll')
			});

		},
		getTests: function (id_station) {

			return $http.get('api/station/tests', {
				params: {
					id_station: id_station
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('tests' + id_station)
			});

		},
		delete: function (data) {
			return $http.put('api/station/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put('api/station/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
	};
}]);
var stf = angular.module('TeamServices', []);

stf.factory('Team', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		getCategories: function () {

			return $http.get('api/team/categories', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('categories')
			});

		},
		createTeam: function (data) {

			return $http.post('api/team', data, {
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'token': $window.localStorage.token
				}
			})

		},
		getTeam: function (id_uniq) {

			return $http.get('api/team', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('teams' + id_uniq)
			});

		},
		getTeams: function () {

			return $http.get('api/team', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('teams')
			});

		},
		editTeam: function (id_uniq, data) {

			return $http.post('api/team', data, {
				params: {
					id_uniq: id_uniq
				},
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'token': $window.localStorage.token
				}
			});

		},
		getPositions: function () {

			return $http.get('api/team/positions', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('positions')
			});

		},
		getUniqIDs: function () {

			return $http.get('api/team/uniqids', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('teamUniqIDs')
			});

		},
		getBaseImage: function (id_uniq) {

			return $http.get('api/team/base_image', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},

		delete: function (data) {
			return $http.put('api/team/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put('api/team/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}]);
var stf = angular.module('UploadServices', []);

stf.factory('Upload', ['$http', '$rootScope', '$window', function ($http, $rootScope, $window) {
	

	return {
		uploadProfilePicture: function (data) {

			return $http.post($rootScope.apiURL + 'upload', data, {

				params: {
					token: $window.localStorage.token
				},
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined
				}

			});

		},
		uploadPlayerPictures: function (data) {

			return $http.post($rootScope.apiURL + 'upload/player', data, {

				params: {
					token: $window.localStorage.token
				},
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined
				}

			});

		}
	};
}]);
var stf = angular.module('UserServices', []);

stf.factory('User', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		createUser: function (data) {

			return $http.post('api/user', data, {
				/*params: {
					token: $window.localStorage.token
				},*/
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'token': $window.localStorage.token
				}
			});

		},
		getLoggedInUser: function () {

			return $http.get('api/user/logged_in_user', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('logged_in_user')
			});

		},
		getAll: function () {

			return $http.get('api/user', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('users')
			});

		},
		getOne: function (id_uniq) {

			return $http.get('api/user', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('users' + id_uniq)
			});

		},
		editUser: function (id_uniq, data) {

			return $http.post('api/user', data, {
				params: {
					id_uniq: id_uniq
				},
				transformRequest: angular.identity,
				headers: {
					'Content-Type': undefined,
					'token': $window.localStorage.token
				}
			});

		},
		getUniqIDs: function () {

			return $http.get('api/user/uniqids', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('userUniqIDs')
			});

		},
		getBaseImage: function (id_uniq) {

			return $http.get('api/user/base_image', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},

		delete: function (data) {
			return $http.put('api/user/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multiDelete: function (data) {
			return $http.put('api/user/multiDelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}]);
var stf = angular.module('VenueServices', []);

stf.factory('Venue', ['$http', '$window', 'STFcache', function ($http, $window, STFcache) {
	

	return {
		createVenue: function (data) {

			return $http.post('api/venue', data, {
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getVenues: function () {

			return $http.get('api/venue', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('venues')
			});

		},
		getVenue: function (id_uniq) {

			return $http.get('api/venue', {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('venues' + id_uniq)
			});

		},
		editVenue: function (id_uniq, data) {

			return $http.put('api/venue', data, {
				params: {
					id_uniq: id_uniq
				},
				headers: {
					token: $window.localStorage.token
				}
			});

		},
		getUniqIDs: function () {

			return $http.get('api/venue/uniqids', {
				headers: {
					token: $window.localStorage.token
				},
				cache: STFcache.save('venueUniqIDs')
			});

		},

		delete: function (data) {
			return $http.put('api/venue/delete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		},
		multidelete: function (data) {
			return $http.put('api/venue/multidelete', data, {
				headers: {
					token: $window.localStorage.token
				}
			});
		}
	};
}]);

