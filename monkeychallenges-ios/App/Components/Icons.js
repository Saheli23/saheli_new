// @flow

import React, { PropTypes,} from 'react'
import {
  TouchableOpacity,Text,View
 } from 'react-native'
import styles from './Styles/IconStyle'
import {ActionConst, Actions as NavigationActions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Colors, Metrics } from '../Themes'





const openSearch = () => {
}




export default {


  searchButton (newStyle) {
    return (

      <View>
        <Icon name='search'
          size={Metrics.icons.xl}
          color={Colors.background}
          style={[styles.searchButton,newStyle]}
        />
      </View>

    )
  },
   chevronUpButton (newStyle) {
    return (

      <TouchableOpacity onPress={this.props.closeModal}>
        <Icon name='chevron-up'
          size={Metrics.icons.small}
          color={Colors.background}
          style={[styles.searchButton,newStyle]}
        />
      </TouchableOpacity>

    )
  },

  FundChallengesButton () {
    pressHandlerOpen = () => {

    NavigationActions.OpenChallengesView();

   }

    return (

      <TouchableOpacity style={{width:((Metrics.screenWidth/3)-15),justifyContent: 'center',flexDirection:'column',alignItems: 'center',}}  onPress={() => this.pressHandlerOpen()}>
        <Icon name='usd'
          size={Metrics.icons.small}
          color={Colors.background}
           style={styles.searchButton,{alignSelf:'center',margin:5}}
        />
        <View style={{justifyContent: 'center',alignSelf:'center',padding:5,paddingTop:0}}>
        <Text style={{color:Colors.background,alignSelf:'center',fontWeight:'bold',fontSize:12}}>Fund</Text>
        <Text style={{color:Colors.background,alignSelf:'center',fontWeight:'bold',fontSize:12}}>Challenges</Text></View>
      </TouchableOpacity>

    )
  },
  PendingChallengesButton () {
   pressHandlerPending = () => {
    NavigationActions.MyPendingChallengesView();
   }
    return (

      <TouchableOpacity style={{width:((Metrics.screenWidth/3)-15), flexDirection:'column',justifyContent: 'center',alignItems: 'center',}} onPress={() => this.pressHandlerPending()}>
        <Icon name='clock-o'
          size={Metrics.icons.small}
          color={Colors.background}
          style={styles.searchButton,{alignSelf:'center',margin:5}}
        />
        <View style={{justifyContent: 'center',alignSelf:'center',padding:5,paddingTop:0}}>
        <Text style={{color:Colors.background,alignSelf:'center',fontWeight:'bold',fontSize:12}}>Pending</Text>
        <Text style={{color:Colors.background,alignSelf:'center',fontWeight:'bold',fontSize:12}}>Challenges</Text></View>
      </TouchableOpacity>

    )
  },
  JudgeChallengesButton () {

    pressHandlerJudge = () => {
     NavigationActions.JudgingChallengesView();
    }
    return (

      <TouchableOpacity style={{width:((Metrics.screenWidth/3)-15), flexDirection:'column',justifyContent: 'center',alignItems: 'center',}}
      onPress={() => this.pressHandlerJudge()}>
        <Icon name='gavel'
          size={Metrics.icons.small}
          color={Colors.background}
          style={styles.searchButton,{alignSelf:'center',margin:5}}
        />
        <View style={{justifyContent: 'center',alignSelf:'center',padding:5,paddingTop:0}}>
        <Text style={{color:Colors.background,alignSelf:'center',fontWeight:'bold',fontSize:12}}>Judge</Text>
        <Text style={{color:Colors.background,alignSelf:'center',fontWeight:'bold',fontSize:12}}>Challenges</Text></View>
      </TouchableOpacity>

    )
  },
  WinnersButton () {
    pressHandlerWin = () => {
     NavigationActions.winChallengesView();
    }
    return (

      <TouchableOpacity style={{width:((Metrics.screenWidth/3)-15), flexDirection:'column',justifyContent: 'center',alignItems: 'center',}}
      onPress={() => {
      //  this.pressHandlerWin()
        NavigationActions.winChallengesView()
      }}>
        <Icon name='star'
          size={Metrics.icons.small}
          color={Colors.background}
          style={styles.searchButton,{alignSelf:'center',margin:5}}
        />
        <View style={{justifyContent: 'center',alignSelf:'center',padding:5,paddingTop:0}}>
        <Text style={{color:Colors.background,alignSelf:'center',fontWeight:'bold',fontSize:12}}>Winners</Text></View>

      </TouchableOpacity>

    )
  },
   FailedButton () {
    return (

      <TouchableOpacity style={{width:((Metrics.screenWidth/3)-15), flexDirection:'column',justifyContent: 'center',alignItems: 'center',}}
      onPress={() => {
      //  this.pressHandlerWin()
        NavigationActions.failChallengesView()
      }}>
        <Icon name='thumbs-down'
          size={Metrics.icons.small}
          color={Colors.background}
          style={styles.searchButton,{alignSelf:'center',margin:5}}
        />
        <View style={{justifyContent: 'center',alignSelf:'center',padding:5,paddingTop:0}}>
        <Text style={{color:Colors.background,alignSelf:'center',fontWeight:'bold',fontSize:12}}>Failed</Text></View>

      </TouchableOpacity>

    )
  },
   NearMeButton () {
    return (
      <TouchableOpacity style={{width:((Metrics.screenWidth/3)-15), flexDirection:'column',justifyContent: 'center',alignItems: 'center',}}>
        <Icon name='map-marker'
          size={Metrics.icons.small}
          color={Colors.background}
          style={styles.searchButton,{alignSelf:'center',margin:5}}
        />
        <View style={{justifyContent: 'center',alignSelf:'center',padding:5,paddingTop:0}}>
        <Text style={{color:Colors.background,alignSelf:'center',fontWeight:'bold',fontSize:12}}>Near Me</Text></View>

      </TouchableOpacity>

    )
  },


}
