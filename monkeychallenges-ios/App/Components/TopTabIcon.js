// @flow


import React, { PropTypes,} from 'react'
import { View, Text } from 'react-native'
import styles from './Styles/TabIconStyle'
const propTypes = {
  selected: PropTypes.bool,
  title: PropTypes.string,
};

const TopTabIcon = (props) => (
  <Text style={{ color: props.selected ? 'black' : 'black' }}>{props.title}</Text>
)

TopTabIcon.propTypes = propTypes

export default TopTabIcon

// // Prop type warnings
// TopTabIcon.propTypes = {
//   someProperty: React.PropTypes.object,
//   someSetting: React.PropTypes.bool.isRequired
// }
//
// // Defaults for props
// TopTabIcon.defaultProps = {
//   someSetting: false
// }





