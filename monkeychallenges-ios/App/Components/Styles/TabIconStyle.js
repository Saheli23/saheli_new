// @flow

import { StyleSheet } from 'react-native'
import { Colors, Metrics, ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Metrics.titlePadding
  },
  iconStyle:{
  	textAlign:'center'
  },
  iconTextStyle:{
  	fontSize: 9,
    fontWeight: 'bold', textAlign:'center'
  }
})
