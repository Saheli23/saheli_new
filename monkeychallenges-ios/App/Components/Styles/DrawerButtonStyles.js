// @flow

import { Metrics, Colors, Fonts } from '../../Themes'

export default {
  text: {
    ...Fonts.style.medium,
    color: Colors.panther,
    marginVertical: 5
  }
}
