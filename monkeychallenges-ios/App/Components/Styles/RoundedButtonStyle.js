// @flow

import {StyleSheet} from 'react-native'
import {Fonts, Colors, Metrics} from '../../Themes/'

export default StyleSheet.create({
  button: {
    height: 35,
        marginHorizontal: Metrics.section,
    marginVertical: Metrics.baseMargin,
    backgroundColor: Colors.matchingColor,
    justifyContent: 'center'
  },
  buttonText: {
    color: Colors.background,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: Fonts.size.medium,
    marginVertical: Metrics.baseMargin
  }
})
