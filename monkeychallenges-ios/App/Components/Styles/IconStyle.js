// @flow

import {StyleSheet} from 'react-native'
import { Metrics, Colors } from '../../Themes/'

const navButton = {
  color: Colors.steel,
}

export default StyleSheet.create({
  backButton: {
    marginTop: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin
  },
  searchButton: {
    ...navButton,
    marginRight: Metrics.smallMargin,  
  },
  navButtonLeft:{
     ...navButton,
    marginLeft: Metrics.smallMargin,

  }
})
