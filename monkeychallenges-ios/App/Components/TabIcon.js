// @flow

import React, { PropTypes,} from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import styles from './Styles/TabIconStyle'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Colors, Metrics } from '../Themes'

const propTypes = {
  selected: PropTypes.bool,
  title: PropTypes.string,
};
const TabIcon = (props) => {

	const selectedStyle = {
		 color: props.selected ? Colors.matchingColor : Colors.steel
	}
	if(props.title == "HOME"){
      return <View>
      	<Icon name='home'
          size={Metrics.icons.small}
          style={[styles.iconStyle,selectedStyle]}
        />
      <Text style={[selectedStyle,styles.iconTextStyle]}>{props.title}</Text></View>
	}else if (props.title == "FAVORITES"){
		return <View>
		<Icon name='heart'
          size={Metrics.icons.small}
           style={[styles.iconStyle,selectedStyle]}
        />
		<Text style={[selectedStyle,styles.iconTextStyle]}>WATCH LIST</Text></View>
	}else if (props.title == "CREATE"){
		return <View>
		<Icon name='plus-circle'
          size={Metrics.icons.small}
           style={[styles.iconStyle,selectedStyle]}
        />
		<Text style={[selectedStyle,styles.iconTextStyle]}>{props.title}</Text></View>
	}else if (props.title == "MY CHALLENGES"){
		return <View>
		<Icon name='trophy'
          size={Metrics.icons.small}
           style={[styles.iconStyle,selectedStyle]}
        />


        <View>
		<Text style={[selectedStyle,styles.iconTextStyle]}>MY</Text>
    <Text style={[selectedStyle,styles.iconTextStyle]}>CHALLENGES</Text>
    	</View>


    </View>
	}else if (props.title == "MY PROFILE"){
		return <View>
			<Icon name='user'
          size={Metrics.icons.small}
           style={[styles.iconStyle,selectedStyle]}
        />
		 <View>
		<Text style={[selectedStyle,styles.iconTextStyle]}>MY</Text>
    <Text style={[selectedStyle,styles.iconTextStyle]}>PROFILE</Text>
    	</View>
    	</View>
	}



}



TabIcon.propTypes = propTypes

export default TabIcon

// // Prop type warnings
// TabIcon.propTypes = {
//   someProperty: React.PropTypes.object,
//   someSetting: React.PropTypes.bool.isRequired
// }
//
// // Defaults for props
// TabIcon.defaultProps = {
//   someSetting: false
// }
