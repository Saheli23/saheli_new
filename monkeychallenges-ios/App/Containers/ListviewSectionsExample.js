// @flow

import React from 'react'
import { View, ListView, Text,Dimensions } from 'react-native'
import { connect } from 'react-redux'

// For empty lists
import AlertMessage from '../Components/AlertMessage'
   const { width, height } = Dimensions.get('window')
// Styles
//import styles from './Styles/ListviewExampleStyle'
import styles from './Styles/ListviewGridExampleStyle'
import styles1 from './Styles/FavoritesChallengesStyle';
class ListviewSectionsExample extends React.Component {

  state: {
    dataSource: Object
  }

  constructor (props) {
    super(props)

    /* ***********************************************************
    * STEP 1
    * This is an array of objects with the properties you desire
    * Usually this should come from Redux mapStateToProps
    *************************************************************/
    const dataObjects = {
      first: [
        {title: 'First Title', description: 'First Description',status:"first"},
        {title: 'Second Title', description: 'Second Description',status:"first"},
        {title: 'Third Title', description: 'Third Description',status:"first"},
        {title: 'Fourth Title', description: 'Fourth Description',status:"first"},
        {title: 'Fifth Title', description: 'Fifth Description',status:"first"},
        {title: 'Sixth Title', description: 'Sixth Description',status:"first"},
        {title: 'Seventh Title', description: 'Seventh Description',status:"first"},
        {title: 'Eighth Title', description: 'Eighth Description',status:"first"},
        {title: 'Ninth Title', description: 'Ninth Description',status:"first"},
        {title: 'Tenth Title', description: 'Tenth Description',status:"first"}
      ],
      second: [
        {title: 'Eleventh Title', description: 'Eleventh Description',status:"second"},
        {title: '12th Title', description: '12th Description',status:"second"},
        {title: '13th Title', description: '13th Description',status:"second"},
        {title: '14th Title', description: '14th Description',status:"second"},
        {title: '15th Title', description: '15th Description',status:"second"},
        {title: '16th Title', description: '16th Description',status:"second"},
        {title: '17th Title', description: '17th Description',status:"second"},
        {title: '18th Title', description: '18th Description',status:"second"},
        {title: '19th Title', description: '19th Description',status:"second"},
        {title: '20th Title', description: '20th Description',status:"second"},
        {title: 'BLACKJACK!', description: 'BLACKJACK! Description',status:"second"}
      ]
    }
    /* ***********************************************************
    * STEP 2
    * Teach datasource how to detect if rows are different
    * Make this function fast!  Perhaps something like:
    *   (r1, r2) => r1.id !== r2.id}
    *   The same goes for sectionHeaderHasChanged
    *************************************************************/
    const rowHasChanged = (r1, r2) => r1 !== r2
    const sectionHeaderHasChanged = (s1, s2) => s1 !== s2

    // DataSource configured
    const ds = new ListView.DataSource({rowHasChanged, sectionHeaderHasChanged})

    // Datasource is always in state
    this.state = {
      dataSource: ds.cloneWithRowsAndSections(dataObjects)
    }
  }

  /* ***********************************************************
  * STEP 3
  * `renderRow` function -How each cell/row should be rendered
  * It's our best practice to place a single component here:
  *
  * e.g.
    return <MyCustomCell title={rowData.title} description={rowData.description} />
  *************************************************************/
  renderRow (rowData, sectionID) {
    // You can condition on sectionID (key as string), for different cells
    // in different sections
    return (
      <View style={styles1.challenge}>
        <Text style={styles.boldLabel}>Section {sectionID} - {rowData.title}</Text>
        <Text style={styles.label}>{rowData.description}   -  {rowData.status}</Text>
      </View>
    )
  }

  /* ***********************************************************
  * STEP 4
  * If your datasource is driven by Redux, you'll need to
  * reset it when new data arrives.
  * DO NOT! place `cloneWithRowsAndSections` inside of render, since render
  * is called very often, and should remain fast!  Just replace
  * state's datasource on newProps.
  *
  * e.g.
    componentWillReceiveProps (newProps) {
      if (newProps.someData) {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRowsAndSections(newProps.someData)
        })
      }
    }
  *************************************************************/

  // Used for friendly AlertMessage
  // returns true if the dataSource is empty
  noRowData () {
    return this.state.dataSource.getRowCount() === 0
  }

  renderHeader (data, sectionID) {
    console.log(data);
    console.log(sectionID);
    switch (sectionID) {
      case 'first':
        return <View style={{height:20,width:width,margin:10}}><Text style={styles.boldLabel}>First Section</Text></View>
      default:
        return <View style={{height:20,width:width,margin:10}}><Text style={styles.boldLabel}>Second Section</Text></View>
    }
  }

  render () {
    return (
      <View style={styles.mainDoubleNavContainer}>
        <AlertMessage title='Nothing to See Here, Move Along' show={this.noRowData()} />
        <ListView
          renderSectionHeader={this.renderHeader}
          contentContainerStyle={styles.listContent}
          dataSource={this.state.dataSource}
          renderRow={this.renderRow}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    // ...redux state to props here
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListviewSectionsExample)
