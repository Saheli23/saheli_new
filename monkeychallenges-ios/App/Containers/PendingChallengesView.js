// @flow

import React from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Image, ListView, View, TouchableOpacity,Dimensions} from 'react-native'
const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)

//import TrendingActions from '../Redux/TrendingRedux'

import PendingChallengesActions from '../Redux/PendingChallengesRedux'

// alert message
import AlertMessage from '../Components/AlertMessage'


// import {Images} from '../Themes'
import {Metrics, Images} from '../Themes'

// import YourActions from '../Redux/YourRedux'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions as NavigationActions } from 'react-native-router-flux'
import Animatable from 'react-native-animatable'

// Styles
import styles from './Styles/JudgingChallengesViewStyle'
//import styles from './Styles/PostLoginStyle'

// button
import RoundedButton from '../Components/RoundedButton'
// I18n
import I18n from 'react-native-i18n'

// progressBar
import * as Progress from 'react-native-progress'

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'


type ChallengeListProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,
    getPendingChallenges: () => void
}


class PendingChallengeViewItem extends React.Component {

   handleChallengesView(itemData) {
    //alert(itemData);
    NavigationActions.ChallengeDetailView({item:itemData})
    //this.props.levelSelected(levelNumber);
  }


  render () {
    let itemData = this.props.itemData
    return <View style={styles.challenge}>

      <View style={styles.imageSection}>

         <TouchableOpacity onPress={this.handleChallengesView.bind(this, itemData)}>
          <Image style={styles.cardImage} source={{uri: itemData.challenge_media[0].path}} />
         </ TouchableOpacity>


        <View style={styles.imageFooter}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Progress.Bar progress={(Math.round(rowData.percentage_funded) / 100)} color={'#33CC32'} height={8} width={(width - 20)} borderWidth={0} unfilledColor={'rgba(128,132,131,0.8)'}/>
          </View>
          <View style={styles.contentVoteSection}>
            <Text style={{color: 'white' }}>$ {itemData.price} USD</ Text>
            <Text style={{color: 'white'}}>{Math.round(itemData.percentage_funded)}%</Text>
            <Text style={{color: 'white', textAlign: 'right'}}>{itemData.days_remaining} days left</Text>
          </View>
        </View>
        <View style={styles.imageFooterOpacity} />
      </View>

      <View style={styles.contentSection}>

      <View style={styles.contentDescriptionSection}>
             <Text style={{color: '#1266AF',fontWeight:'bold',}}>{itemData.handle_name} : <Text style={{color: '#000000'}}>{itemData.name}</Text></Text>
          <Text style={{fontSize:12}} numberOfLines={3}>{itemData.description}</Text>
      </View>

      </View>
    </View>
  }
}

class ChallengeList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      loaded: false,
      uuid:""
    }
  }
  props:ChallengeListProps
  componentWillReceiveProps (newProps) {
    if (!newProps.fetching) {
      this.setState({fetching:newProps.fetching, payload: newProps.payload})
    }else{
       this.setState({fetching:newProps.fetching, payload: newProps.payload})
    }
  }
  async getTokenItem() {
  //  AsyncStorage.getItem('MCUseruuid').then((value) =>{
  //   this.setState({uuid:value});

  // })

    await AsyncStorage.getItem('MCUseruuid', (err, result) => {
    console.log(result);
    //return result
    this.setState({uuid:result})

      //this.setState({uuid:value});
      // => {'name':'Chris','age':31,'traits':{'shoe_size':10,'hair':'brown','eyes':'blue'}}
    });
 }


 async _loadInitialState() {
    try {
      var value = await AsyncStorage.getItem('MCUseruuid');
      console.log(value);
      if (value !== null){
        this.setState({uuid: value});
        console.log('Recovered selection from disk');
        //this._appendMessage('Recovered selection from disk: ' + value);
      } else {
        console.log('Initialized with no selection from disk');

        //this._appendMessage('Initialized with no selection on disk.');
      }
    } catch (error) {
      console.log('error');
      //this._appendMessage('AsyncStorage error: ' + error.message);
    }
  }

 componentDidMount(){

 // this._loadInitialState().done();
  console.log(this.state.uuid);
 }

  componentWillMount () {
    //this.getTokenItem
     //const uuid = await AsyncStorage.getItem('MCUseruuid');
    console.log(this.state.uuid);
    this.props.getPendingChallenges()
    //this.props[this.props.requestType]()

  }

  render () {

      let payload = this.state && this.state.payload || {data: []}
      console.log(payload.data, 'playload.data', this.props.title)


      return <View style={styles.challengeListContainer}>

{ !this.state.fetching &&
      <AlertMessage title='Nothing to See Here, Move Along' show={(payload.data.length == 0)} />
      }
       { this.state.fetching &&
        <GiftedSpinner />
      }

        <ScrollView vertical>

          {
                        payload.data.map((rowData, index) => {
                          return <PendingChallengeViewItem itemData={rowData} key={index} />
                        })

            }
        </ScrollView>
      </View>

  }
}

class PendingChallengesView extends React.Component {

 render () {
    return (
      <View style={styles.mainDoubleNavContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <ConnectedChallengeList stateName='pendingChallenges' requestType='getPendingChallenges' title='Featured Challenges' />
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    fetching: state[props.stateName].fetching,
    payload: state[props.stateName].payload
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getPendingChallenges: () => dispatch(PendingChallengesActions.pendingChallengesRequest()),

  }
}

const ConnectedChallengeList = connect(mapStateToProps, mapDispatchToProps)(ChallengeList)
export default PendingChallengesView
