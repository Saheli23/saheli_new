// @flow

import { StyleSheet, Dimensions,Platform } from 'react-native'
import { Colors, Metrics, ApplicationStyles } from '../../Themes/'
const { width, height } = Dimensions.get('window')

export default StyleSheet.create({
    ...ApplicationStyles.screen,
    challengeListContainer: {
        padding: 1.5
    },
    boldLabel: {
        fontSize: 16,
        fontWeight: 'bold',
        paddingBottom: 10
    },
    progressWidth: {
        width: 250,
    },
    challenge: {
        marginBottom: 6
    },
    imageSection: {
        height: (width/2),
        position: 'relative'
    },
    cardImage:{
      height: (width/2),
  		width:(width),

  	},
    playButton:{
    position:'absolute',
    top:((width/2)/4),
    bottom:0,
    left:(width/2.5)
    },
    imageFooter: {
        height: 60,
        zIndex: 99,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'space-around'
    },
    imageFooterOpacity: {
        height: 60,
        backgroundColor: 'black',
        zIndex: 9,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        opacity: 0.7
    },
    contentSection: {
        padding: 5,
        paddingTop: 10
    },
    contentDescriptionSection: {
        height: (Platform.OS === 'ios') ? 70 : 50,
        flexDirection: 'row',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    },

    contentVoteSection: {
        flexDirection: 'row',
        width:(width - 20),justifyContent:'space-between',alignItems: 'stretch',marginTop:5
    },
    imageGrid: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
    },
    image: {
        width: 100,
        height: 100,
        margin: 10,
    },

    //view pager

    indicatorContainer: {
        backgroundColor: 'white',
        height: 48
    },
    indicatorText: {
        fontSize: 14,
        color: 'black'
    },
    indicatorSelectedText: {
        fontSize: 14,
        color: 'black'
    },
    selectedBorderStyle: {
        height: 3,
        backgroundColor: '#f9642f'
    },
    statusBar: {
        height: 24,
        backgroundColor: 0x00000044
    },
    toolbarContainer: {
        height: 40,
        backgroundColor: 'red',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16
    },
    backImg: {
        width: 16,
        height: 17
    },
    titleTxt: {
        marginLeft: 36,
        color: 'white',
        fontSize: 20
    },
    //FooterBar
    FooterBarTopBar: {
        height: 35,
        backgroundColor: '#EAEAEA',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        borderColor: '#D0CFCF',
        borderTopWidth: 3
    },
    FooterBarBottomBar: {
        height: 50,
        backgroundColor: 'white',
        marginBottom: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },JudgeFooterBarBottomBar:{
        height: 60,
        backgroundColor: 'white',
        marginBottom: 0,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    FooterBarTopBarInsideView: {
        flexDirection: 'row'
    },
    FooterBarTopBarInsideText: {
        marginLeft: 2,
        fontWeight: 'bold',
    }

})
