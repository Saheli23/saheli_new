// @flow

import { StyleSheet } from 'react-native'
import { Colors, Metrics, ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  challengeListContainer: {
    padding: 10
  },
  boldLabel: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingBottom: 10
  },
  challenge: {
    borderWidth: 1,
    borderColor: Colors.steel,
    marginRight: 5

  },
  imageSection: {
    height: 180,
    position: 'relative'
  },
  imageFooter: {
    height: 60,
    zIndex: 99,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'space-around'
  },
  imageFooterOpacity: {
    height: 60,
    backgroundColor: 'black',
    zIndex: 9,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    opacity: 0.7
  },
  contentSection: {
    width: 300,
    padding: 5,
    paddingTop: 10

  },
  contentDescriptionSection: {
    height: 70,
    flexDirection: 'column',
    alignItems: 'flex-start',
    flexWrap: 'wrap'
  },

  contentVoteSection: {
    flexDirection: 'row',
    width:(300/1.2),justifyContent:'space-between',alignItems: 'stretch',marginTop:5
  }

})
