// @flow

import { StyleSheet, Dimensions,Platform } from 'react-native'
import { Colors, Metrics, ApplicationStyles } from '../../Themes/'
const { width, height } = Dimensions.get('window')

export default StyleSheet.create({
	 ...ApplicationStyles.screen,
  challengeListContainer: {
    // padding: 1.5
  },
  boldLabel: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingBottom: 10
  },
	row: {
    flex: 1,
    marginVertical: Metrics.smallMargin,
    justifyContent: 'center',
		// borderWidth: 1,
    // borderColor: Colors.steel,
  },
  progressWidth:{
    width: 250,
  },
  challenge: {
    borderWidth: 1.5,
    borderColor: Colors.steel,
    marginBottom: 6
  },
  imageSection: {
		height:(width/2),
    position: 'relative',
		zIndex: (Platform.OS === 'ios') ? 0 : 9,
  },
	cardImage:{
		height: (width/2),
		width:(width),
		backgroundColor: 'white',
	},
  imageFooter: {
    height: 60,
    zIndex: 99,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'space-around'
  },
  imageFooterOpacity: {
    height: 60,
    backgroundColor: 'black',
    zIndex: 9,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    opacity: 0.7
  },
  contentSection: {
    padding: 5,
    paddingTop: 10
  },
  contentDescriptionSection: {
    height: 70,
    flexDirection: 'column',
    alignItems: 'flex-start',
    flexWrap: 'wrap'
  },

  contentVoteSection: {
    flexDirection: 'row',
		width:(width - 20),justifyContent:'space-between',alignItems: 'stretch',marginTop:5
  },
  imageGrid: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  image: {
    width: 100,
    height: 100,
    margin: 10,
  },
  checkbox: {
        width: 10,
        height: 10,
        marginLeft:5
    },
})
