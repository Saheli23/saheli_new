// @flow

import { StyleSheet,Dimensions, Platform } from 'react-native'
import { Colors, Metrics, ApplicationStyles } from '../../Themes/'
const { width, height } = Dimensions.get('window')

export default StyleSheet.create({
	 ...ApplicationStyles.screen,
  challengeListContainer: {
    padding: 5,
  },
	listContent: {
		flexDirection: 'row',
		flexWrap: 'wrap'
	},
  challenge: {
    width:((width/3)-4),
    height:((width/3)-4),
    borderWidth:1,
    borderColor:'#ccc',
  },
	boldLabel: {

		fontWeight: 'bold',
		alignSelf: 'flex-start',
		color: Colors.panther,
		textAlign: 'center',
		marginBottom: Metrics.smallMargin
	},
  imageSection: {
    width:((width/3)-4),
    height:((width/3)-4),
    position: 'relative'
  },
  image:{
    width:((width/3)-4),
    height:((width/3)-4),
  },
  imageFooter: {
    height: 60,
    zIndex: 99,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'space-around'
  },
  imageFooterOpacity: {
    height: 60,
    backgroundColor: 'black',
    zIndex: 9,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    opacity: 0.7
  },
  contentSection: {

    paddingTop: 10
  },
  contentDescriptionSection: {
    height: 70,
    flexDirection: 'row',
    alignItems: 'flex-start',
    flexWrap: 'wrap'
  },

  contentVoteSection: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  imageGrid: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },

})
