// @flow

import { StyleSheet, Dimensions, Platform } from 'react-native'
import { Colors, Metrics, ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Metrics.doubleNavBarHeight,
    backgroundColor: Colors.background
  }
})
