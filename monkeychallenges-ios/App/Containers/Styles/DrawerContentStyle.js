// @flow
import { Colors, Metrics, ApplicationStyles } from '../../Themes'
export default {
  container: {
    flex: 1,
    padding: 20
  },
  logo: {
    alignSelf: 'center',
    justifyContent: 'center',
    width: (Metrics.icons.xl + 5),
    height:Metrics.icons.xl,
  }
}
