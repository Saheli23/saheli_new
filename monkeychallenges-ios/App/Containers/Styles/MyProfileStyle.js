// @flow

import { StyleSheet, Dimensions, Platform } from 'react-native'
import { Colors, Metrics, ApplicationStyles } from '../../Themes/'
const { width, height } = Dimensions.get('window')
export default StyleSheet.create({

   ...ApplicationStyles.screen,
  profile: {
    backgroundColor: Colors.background
  },
  mainPanel:{
  	position: 'relative'
  },
  coverImage:{
  position:'relative',
  backgroundColor:'#ccc'

  },
  coverImageAndroid:{
    position:'relative',
  },
  profileImage:{
  	zIndex: 1,
  	top:(width/3.5),
  	left:0,
  	right:0,
  	position: 'absolute',
	  justifyContent: 'center',
  	alignItems: 'center',


  },
  profileImageInside:{
    height:(width/3.5),
    width: (width/3.5),
    borderRadius: ((width/3.5)/2),
    justifyContent: 'center',
    borderWidth: 3,
    borderColor: '#FFF'
  },
  profileEditImage:{
    height: 40,
    width:40,
    zIndex:2,
    marginTop:-70,
   },
  profileEditImageAndroid:{
    height: 40,
    width:40,

  },
  profileEditImageUpperIos:{
    zIndex:999,
    alignItems:'center',
    justifyContent: 'center',
  },
  profileEditImageUpperAndroid:{
    zIndex:999,
    alignItems:'center',
    justifyContent: 'center',
    marginTop:-45,
  },
  editCoverImageIos:{
    position:'relative',
    top:-100,
    right:10
  },
  editCoverImageAndroid:{
    // zIndex:1,
    // position:'relative',
    // top:-100,
    // right:10
    zIndex: 1,
  	top:(width/3.5),
  	left:0,
  	right:0,
  	position: 'absolute',
	  justifyContent: 'center',
  	alignItems: 'center',
  },
  boldLabel: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingBottom: 5
  },
  usernameLabel:{
    fontSize: 16,
    paddingBottom: 2,
    color:"#0095FF"
  },
  messageLabel:{
    fontSize: 16,
    paddingBottom: 10,
    color:"#000",
    marginLeft:10,
    marginRight:10,
    textAlign: 'justify',
  },

  challengeListContainer: {
    padding: 1.5,
    marginTop:15,
  },

  progressWidth:{
    width: 250,
  },
  challenge: {
    borderWidth: 1.5,
    borderColor: Colors.steel,
    marginBottom: 6,
    marginTop:5
  },
  imageSection: {
    height:(width/2),
    position: 'relative'
  },
  coverImageInside:{
    height: (width/2),
    backgroundColor:'#ccc'
  },
  imageFooter: {
    height: 60,
    zIndex: 99,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'space-around'
  },
  imageFooterOpacity: {
    height: 60,
    backgroundColor: 'black',
    zIndex: 9,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    opacity: 0.7
  },
  contentSection: {
    padding: 5,
    paddingTop: 10
  },
  contentDescriptionSection: {
    height: 70,
    flexDirection: 'row',
    alignItems: 'flex-start',
    flexWrap: 'wrap'
  },

  contentVoteSection: {
    flexDirection: 'row',
    justifyContent: 'space-around',

  },
  imageGrid: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  image: {
    width: 100,
    height: 100,
    margin: 10,
  },

})
