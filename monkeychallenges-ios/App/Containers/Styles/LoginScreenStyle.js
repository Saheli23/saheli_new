// @flow

import { StyleSheet,Dimensions,Platform } from 'react-native'
import { Colors, Metrics, ApplicationStyles } from '../../Themes'
const { width, height } = Dimensions.get('window')
export default StyleSheet.create({
   ...ApplicationStyles.screen,
  container: {
    paddingTop: 70,
    backgroundColor: Colors.background
  },
  backgroundImageLogin: {
        position: 'absolute',
        top:0,
        bottom:0,
        left:0,
        right:0,
        width: null,
        height: null,


    },
  form: {
    backgroundColor: Colors.transparent,
    margin: Metrics.baseMargin,
    borderRadius: 4,
    justifyContent: 'space-between',
    alignItems: 'stretch',
  },
  row: {
    paddingVertical: Metrics.baseMargin,
    paddingHorizontal: Metrics.baseMargin
  },
  rowLabel: {
    color: Colors.background
  },
  textInput: {
    height: (Platform.OS === 'ios') ? 30 : 40,
    fontSize:10,
    color: Colors.background
  },
  textInputReadonly: {
    height: (Platform.OS === 'ios') ? 25 : 40,
    fontSize:10,
    color: Colors.background
  },
  loginRow: {
    height:55,
    paddingBottom: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
    flexDirection: 'row'
  },
  loginButtonWrapper: {
    flex: 1
  },
  loginButton: {
    flex: 1,
    borderWidth: 1,
    borderColor: Colors.matchingColor,
    backgroundColor: Colors.matchingColor,
    padding: 7,
    marginLeft:3,

  },
  loginText: {
    textAlign: 'center',
    color: Colors.silver
  },
  topLogo: {
    width: 100, height: 90,marginTop:30,marginBottom:20,
    alignSelf: 'center',
    resizeMode: 'contain'

  }
})
