// @flow

import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  logo: {
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain'
  },
  LandingVerticalRow: {
  	flex: 1,
      flexDirection: 'column',
      justifyContent: 'flex-end',
      alignItems: 'stretch',
      padding: 10,

    },
  centered: {
    alignItems: 'center'
  },
  loginRow: {
    height:55,
    paddingBottom: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
    flexDirection: 'row'
  },
  loginButtonWrapper: {
    flex: 1
  },
  loginButton: {
    flex: 1,
    borderWidth: 1,
    borderColor: Colors.matchingColor,
    backgroundColor: Colors.matchingColor,
    padding: 8,
    marginLeft:3
  },
  loginText: {
    textAlign: 'center',
    color: Colors.silver
  },
})
