// @flow

import React from 'react'
import {
    View,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    Keyboard,
    LayoutAnimation,
    WebView,
    Linking,AsyncStorage
} from 'react-native'
import {connect} from 'react-redux'
import styles from '../Styles/WebLinksScreenStyle'
import {Images, Metrics} from '../../Themes'
import LoginActions from '../../Redux/LoginRedux'
//import FbLoginActions from '../../Redux/FbLoginRedux'
import {Actions as NavigationActions} from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import WebViewBridge from 'react-native-webview-bridge';

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'
import { StyleSheet } from 'react-native'

class Terms extends React.Component {
	constructor (props) {
    super(props)

       this.state = { canGoBack: false,MYuuid:"" };
  }
//   onBack() {
//    NavigationActions.postLogin();
//   //  NavigationActions.pop();
//   //this.refs[webviewbridge].goBack();
//
// }
componentWillMount(){
  this.getMYuuid()

}
getMYuuid() {
  AsyncStorage.getItem('MYuuid').then((value) =>{
    this.setState({MYuuid:value});
  })
 }
	render() {
    let {MYuuid} = this.state
		return (
     <ScrollView ref="myScrollView" keyboardDismissMode='interactive' style={[styles.mainContainer,{padding:10}]}>
        <View><Text style={{fontWeight: 'bold',fontSize:18}}>Terms and Conditions of Use</Text></View>
        <View><Text>PLEASE READ THIS DOCUMENT CAREFULLY. IT CONTAINS VERY IMPORTANT INFORMATION REGARDING YOUR RIGHTS AND OBLIGATIONS, INCLUDING LIMITATIONS AND EXCLUSIONS THAT MIGHT APPLY TO YOU.</Text></View>
        <View style={{paddingTop:10}}><Text>
          These Terms and Conditions of Use (hereinafter “Terms of Use”) apply to the website(s)provided by 1UP Challenges, Inc. dba Monkey Challenges (hereinafter referred to as the “site”).  The site allows individuals, clubs, associations, business, and charitable organizations to set up, promote, judge and raise revenue via their own ALS Ice Bucket Challenge styled promotion – hereinafter “Challenge”).
        </Text></View>
        <View><Text style={{fontWeight: 'bold',paddingTop:10}}>Levels of Access</Text></View>
        <View style={{paddingTop:10}}><Text>Access to the site will depend upon how it is used. There are five (5) levels of access available to the public:</Text></View>
        <View style={{paddingTop:10}}><Text><Text style={{textDecorationLine:'underline'}}>Visitor:</Text> Visitors refers to anyone who visits the site, but cannot participate, sponsor, or judge a Challenge.</Text></View>
        <View style={{paddingTop:10}}><Text><Text style={{textDecorationLine:'underline'}}>Participant:</Text> A Participant refers to those individuals who accept and participate in a Challenge promoted by the site. Each Participant must agree to these Terms of Use, as well as Monkey Challenges’ Privacy Policy and Terms of Sale, open and maintain an Account with Monkey Challenges for the duration of any Challenge which they actively participate.</Text></View>
       <View style={{paddingTop:10}}><Text><Text style={{textDecorationLine:'underline'}}>Sponsor:</Text>A Sponsor is an individual, organization or business who agrees to pay money in support of a Challenge. Each Sponsor must agree to these Terms of Use, as well as Monkey Challenges’ Privacy Policy and Terms of Sale. Sponsors must also open and maintain an Account with Monkey Challenges for the duration of any Challenge which they actively participate.</Text></View>
       <View style={{paddingTop:10}}><Text><Text style={{textDecorationLine:'underline'}}>Judge:</Text>A Judge is anyone who wishes to vote to determine if a Challenge has been satisfactorily performed by a Participant. Judges must agree to these Terms of Use, as well as Monkey Challenges Privacy Policy, and open and maintain an Account with Monkey Challenges for the duration of any Challenge which they wish to judge.</Text></View>
       <View style={{paddingTop:10}}><Text><Text style={{textDecorationLine:'underline'}}>Organizer:</Text>An Organizer is an individual, organization or charity which uses site to set up and run one or more Challenge. Along with Monkey Challenges, Organizers are required to monitor all Challenges performed for their benefit, and shall receive (or determine who shall receive) the proceeds raised from any challenge. Each Organizer must agree to these Terms of Use, as well as Monkey Challenges’ Privacy Policy and Terms of Sale. Sponsors must also open and maintain an Account with Monkey Challenges for the duration of any Challenge which they actively participate.</Text></View>
       <View style={{paddingTop:10}}><Text>Your use of the site, submission of content, or participation in a Monkey Challenge does not signify our acceptance of your Challenge, nor does it constitute confirmation of our offer to use or otherwise let you participate in a Challenge using the site.</Text></View>
       <View style={{paddingTop:10}}><Text>Monkey Challenges reserves the right at any time after receipt of your Challenge to accept, decline, or limit your Challenge for any reason or in any way.</Text></View>
       <View style={{paddingTop:10}}><Text>If your Challenge is suspended temporarily or is cancelled, you will receive prompt notification.</Text></View>
       <View style={{paddingTop:10}}><Text>We make every effort to maintain the availability of our site. However, should we experience technical difficulties, we are not responsible for orders that are not processed or accepted.</Text></View>
       <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}}>Agreement Upon Access</Text></View>
       <View style={{paddingTop:10}}><Text>By accessing the site, you agree:</Text>
          <View style={{paddingTop:10}}><Text>To be bound by and accept these Terms And Conditions of Use;</Text></View>
          <View style={{paddingTop:5}}><Text>To abide by all applicable local, state, national, and international laws, rules, and regulations;</Text></View>
          <View style={{paddingTop:5}}><Text>And affirm that you are authorized to bind any business, charity or organization on whose behalf you are using the site;</Text></View>
      </View>
      <View style={{paddingTop:10}}><Text>By accessing the site, you agree <Text style={{textDecorationLine:'underline'}}>not</Text> to:</Text>
        <View style={{paddingTop:10}}><Text>Distribute in any medium any Content of the site without Monkey Challenges prior written authorization;</Text></View>
        <View style={{paddingTop:10}}><Text>Alter or otherwise damages any part of the site;</Text></View>
        <View style={{paddingTop:10}}><Text>Access Content through any technology or means other than the video playback pages of the site itself, the Embeddable Player, or other explicitly authorized means our service may designate;</Text></View>
        <View style={{paddingTop:10}}><Text>Use the site for any of the following commercial uses unless you obtain Monkey Challenges’ prior written approval;</Text></View>
        <View style={{paddingTop:10}}><Text>Sell access to the site;</Text></View>
        <View style={{paddingTop:10}}><Text>Sell advertising, sponsorships, or promotions placed on or within the site or Content; or the sell of advertising, sponsorships, or promotions on any page of an ad-enabled blog or website containing Content delivered via the site, unless other material not obtained from Monkey Challenges appears on the same page and is of sufficient value to be the basis for such sales. Prohibited commercial uses do not include:
            Uploading an original video to Monkey Challenge;
            Showing Monkey Challenges videos through the Embeddable Player on an ad-enabled blog or website;
            Any use that Monkey Challenges expressly authorizes in writing.
        </Text></View>
      <View style={{paddingTop:10}}><Text>Use or launch any automated system, including without limitation, “robots,” “spiders,” or “offline readers,” that accesses the Service in a manner that sends more request messages to the site servers in a given period of time than a human can reasonably produce in the same period by using a conventional on-line web browser.</Text></View>
      <View style={{paddingTop:10}}><Text>Notwithstanding the foregoing, Monkey Challenges grants the operators of public search engines permission to use spiders to copy materials from the site for the sole purpose of and solely to the extent necessary for creating publicly available searchable indices of the materials, but not caches or archives of such materials. 1Up Challenges reserves the right to revoke these exceptions either generally or in specific cases. You agree not to collect or harvest any personally identifiable information, including account names, from the Service, nor to use the communication systems provided by the Service (e.g., comments, email) for any commercial solicitation purposes.</Text></View>
          <View><Text>Solicit, for commercial purposes, any users of the Service with respect to their Content.</Text></View>
          <View><Text>In your use of the Service, you will comply with all applicable laws.</Text></View>
          <View><Text>
            Monkey Challenges reserves the right to discontinue any aspect of the Service at any time.
            If you do not, or cannot agree to these terms and conditions, you should not use the site or participate in any way in any of the Monkey Challenges contained therein.</Text></View>
          <View><Text>These Terms And Condition of Use are subject to change by 1Up Challenges, Inc. dba Monkey Challenges. (referred to as “us” or “we”) without prior written notice at any time, in our sole discretion. The latest version of the terms and conditions will be posted on this site, and you should review these terms and conditions prior to use of the site</Text></View>
          <View><Text>These Terms And Conditions of Use are an integral part of the site, as are the <Text style={{textDecorationLine:'underline',fontWeight:'bold'}}>Terms of Sale</Text>, and <Text style={{textDecorationLine:'underline',fontWeight:'bold'}}>Privacy Policy</Text>, each of which applies generally to the use of our site.</Text></View>
     <View style={{paddingTop:10}}><Text>Through these Terms, we make clear that we do not want the site to be used for any unlawful purpose.</Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}}>Acceptance and Cancellation</Text></View>
     <View style={{paddingTop:10}}><Text >Your use of the site, submission of content, or participation in a Monkey Challenge does not signify acceptance of your Challenge by us, your Organizer, nor does it constitute confirmation of an offer to use or otherwise let you participate in a Challenge using the site.</Text></View>
     <View style={{paddingTop:10}}><Text > Monkey Challenges reserves the right at any time after receipt of your Challenge to accept, decline, or limit your Challenge for any reason or in any way. If your Challenge is suspended temporarily or is cancelled, you will receive prompt notification.</Text></View>
     <View style={{paddingTop:10}}><Text >We make every effort to maintain the availability of our site. However, should we experience technical difficulties, we are not responsible for orders that are not processed or accepted.</Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}}>Monkey Challenges Accounts</Text></View>
     <View style={{paddingTop:10}}><Text >In order to access some features of the site, and act as a Participant, Sponsor, Judge or Organizer, you will have to create an account with Monkey Challenges (hereinafter “Account”).</Text></View>
     <View style={{paddingTop:10}}><Text >In order to set up an Account, you must be an adult, that is 18-years-old or older. Minors age 13 to 17 may be Participant, Sponsors, or Judges provided they are fully supervised at all times by a responsible adult. Monkey Challenges reserves the right, in its sole discretion, to determine the level of adult supervision that is required.</Text></View>
     <View style={{paddingTop:10}}><Text >You may never use another’s account without permission.</Text></View>
     <View style={{paddingTop:10}}><Text >When creating your Account, you must provide accurate and complete information. By providing any information to Monkey Challenges, you are verifying its truthfulness and accuracy. By setting up an Account, you are solely liable for the activity that occurs on your Account. Monkey Challenges strongly recommends that you keep your account user name and password secure.</Text></View>
     <View style={{paddingTop:10}}><Text style={{textDecorationLine:'underline'}} >Notify Monkey Challenges immediately of any unauthorized use of your Account occurs. Monkey Challenges will not be liable for your losses caused by any unauthorized use of your account.</Text></View>
     <View style={{paddingTop:10}}><Text >If you change or deactivate the mobile phone number that you used to create a Monkey Challenges Account, you must update your account information through Settings within 72 hours to prevent us from sending to someone else messages intended for you.</Text></View>
     <View style={{paddingTop:10,}}><Text style={{fontWeight:'bold'}} >Data Charges and Mobile Phones</Text></View>
     <View style={{paddingTop:10}}><Text >You are solely responsible for any mobile charges that you may incur for using our site, including text-messaging and data charges. If you’re unsure what those charges may be, you should ask your service provider before using the site.</Text></View>
     <View style={{paddingTop:10,}}><Text style={{fontWeight:'bold'}}>Content</Text></View>
     <View style={{paddingTop:10}}><Text >As used herein, Content means the textual, visual, or aural content that is encountered as part of the user experience on websites. It may include, among other things, text, images, sounds, videos, and animations.</Text></View>
     <View style={{paddingTop:10}}><Text >Our site lets you create, upload, post, send, receive, and store Content.</Text></View>
     <View style={{paddingTop:10}}><Text >By creating, uploading, posting, sending, receiving, or storing Content by using our site, you agree to grant Monkey Challenges a worldwide, perpetual, royalty-free, licensable, and transferable license to host, store, use, display, reproduce, modify, adapt, edit, publish, create derivative works from, gain publicly form, broadcast, distribute, syndicate, promote, exhibit, and publicly display that content in any form and in any and all media or distribution methods (now known or later developed) without limitation or restriction. It is understood and agreed that Monkey Challenges will this license for the purpose of operating, developing, providing, promoting, and improving the Services; researching and developing new purposes; and making content submitted available to our business partners for syndication, broadcast, distribution, or publication.</Text></View>
     <View style={{paddingTop:10}}><Text >To the extent it’s necessary, you also grant Monkey Challenges the unrestricted, worldwide, perpetual right and license to use your name, likeness, and voice in any and all media and distribution channels (now known or later developed) in connection with any purpose. This means, among other things, that you will not be entitled to any compensation from Monkey Challenges if your name, likeness, or voice is conveyed through any other media for any other business purpose.</Text></View>
     <View style={{paddingTop:10}}><Text >While we’re not required to do so, we may access, review, screen, and delete Content at any time and for any reason, including if we think your content violates these Terms of Use, our Terms of Sale or our Privacy Policies. You alone though remain responsible for the Content you create, post, store, or send through the site.</Text></View>
     <View style={{paddingTop:10}}><Text >We always love to hear from you. But if you volunteer feedback or suggestions, just know that we can and will use your ideas without compensating you.</Text></View>
     <View style={{paddingTop:10}}><Text >You further agree that Content you submit to the Service will not contain third party copyrighted material, or material that is subject to other third party proprietary or intellectual property rights or restrictions, unless you have permission from the rightful owner of the material, or you are otherwise legally entitled to post the material and to grant Monkey Challenges all of the license rights granted herein.</Text></View>
     <View style={{paddingTop:10}}><Text >The Service does not endorse any Content submitted to the site by any user or other licensor, or any opinion, recommendation, or advice expressed therein, and Monkey Challenges expressly disclaims any and all liability in connection with Content. Monkey Challenges does not permit copyright infringing activities and infringement of intellectual property rights on the Service, and Monkey Challenges will remove all Content if properly notified that such Content infringes on another’s intellectual property rights. Monkey Challenges reserves the right to remove Content without prior notice.</Text></View>
     <View style={{paddingTop:10,}}><Text style={{fontWeight: 'bold',}}>The Content of Others</Text></View>
     <View style={{paddingTop:10}}><Text >ome of the Content on our site may be produced by people who do not have an active Monkey Challenges Account (hereinafter, “Third Parties.”) Whether that Content is posted publicly or sent privately, the Content is the sole responsibility of the person or organization that created it. Although Monkey Challenges reserves the right to review all content that appears on the Services and to remove any content that violates these Terms Of Use, we do not necessarily review all of it. So we cannot, and do not, take responsibility for any content that others provide through the site.</Text></View>
     <View style={{paddingTop:10}}><Text >Through these Terms, we make clear that we do not want the site to be used for any unlawful purpose.</Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}}>Privacy</Text></View>
     <View style={{paddingTop:10}}><Text >Your privacy matters to us. You can learn how we handle your information when you use our Services by reading our Privacy Polcy. We encourage you to give the privacy policy a careful look because, by using our Services, you agree that Monkey Challenges can collect and use your information consistent with that policy.</Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}}>Safety</Text></View>
     <View style={{paddingTop:10}}><Text>We try hard to keep our site a safe place for all users. But we can’t guarantee it. That’s where you come in. By using the site, you agree that you will not:
     </Text></View>
     <View style={{paddingTop:10}}><Text >Use the site for any purpose that is illegal or prohibited in these Terms And Conditions of Use;</Text></View>
     <View style={{paddingTop:10}}><Text >Use any robot, spider, crawler, scraper, or other automated means or interface to access the Site or extract other user’s information;
     </Text></View>
     <View style={{paddingTop:10}}><Text >Use or develop any third-party applications that interact with other users’ content or the Site without our written consent;</Text></View>
     <View style={{paddingTop:10}}><Text >Use the Site in a way that could interfere with, disrupt, negatively affect, or inhibit other users from fully enjoying the Site, or that could damage, disable, overburden, or impair the functioning of the Site;</Text></View>
     <View style={{paddingTop:10}}><Text >Use or attempt to use another user’s account, username, or password without their permission;</Text></View>
     <View style={{paddingTop:10}}><Text >Not solicit login credentials from another user;</Text></View>
     <View style={{paddingTop:10}}><Text >Not post content that contains pornography, graphic violence, threats, hate speech, or incitements to violence;</Text></View>
     <View style={{paddingTop:10}}><Text >Not upload viruses or other malicious code or otherwise compromise the security of the Site;</Text></View>
     <View style={{paddingTop:10}}><Text >Not attempt to circumvent any content-filtering techniques we employ, or attempt to access areas or features of the Site that you are not authorized to access;</Text></View>
     <View style={{paddingTop:10}}><Text >Not probe, scan, or test the vulnerability of our site or any system or network; and</Text></View>
     <View style={{paddingTop:10}}><Text >Not encourage or promote any activity that violates this Agreement .</Text></View>
     <View style={{paddingTop:10}}><Text >We also care about your safety while using our site. Do not use our site in a way that would distract you from obeying traffic or safety laws. And never put yourself or others in harm’s way just to accomplish a challenge.</Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}}>Terms And Condition of Sale</Text></View>
     <View style={{paddingTop:10}}><Text  >All terms and condition of any purchase made via the site are governed by Monkey Challenges Terms of Sale.</Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}}>Disclaimer, Limitation of Liability</Text></View>
     <View style={{paddingTop:10}}><Text >THE SERVICES ARE PROVIDED “AS IS” AND “AS AVAILABLE” WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT. IN ADDITION, WHILE 1UP CHALLENGES ATTEMPTS TO PROVIDE A GOOD USER EXPERIENCE, WE DO NOT REPRESENT OR WARRANT THAT: (A) THE SITE WILL ALWAYS BE SECURE, ERROR-FREE, OR TIMELY; (B) THE SITE WILL ALWAYS FUNCTION WITHOUT DELAYS, DISRUPTIONS, OR IMPERFECTIONS; OR (C) THAT ANY SITE CONTENT, USER CONTENT, OR INFORMATION YOU OBTAIN ON OR THROUGH THE SERVICES WILL BE TIMELY OR ACCURATE.</Text></View>
     <View style={{paddingTop:10}}><Text >MONKEY CHALLENGES TAKES NO RESPONSIBILITY AND ASSUMES NO LIABILITY FOR ANY CONTENT THAT YOU, ANOTHER USER, OR A THIRD PARTY CREATES, UPLOADS, POSTS, SENDS, RECEIVES, OR STORES ON OR THROUGH OUR SERVICES. YOU UNDERSTAND AND AGREE THAT YOU MAY BE EXPOSED TO CONTENT THAT MIGHT BE OFFENSIVE, ILLEGAL, MISLEADING, OR OTHERWISE INAPPROPRIATE, NONE OF WHICH MONKEY CHALLENGES WILL BE RESPONSIBLE FOR.</Text></View>
     <View style={{paddingTop:10}}><Text >BY USING THE SITE YOU EXPRESSLY  AGREE THAT YOUR USE SHALL BE AT YOUR SOLE RISK. TO THE FULLEST EXTENT PERMITTED BY LAW, 1UP CHALLENGES, INC. ITS OFFICERS, DIRECTORS, EMPLOYEES, AND AGENTS DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, IN CONNECTION WITH THE SITE, THE CHALLENGES CONTAINED THEREIN, AND THE INFOMRATION CONTAINED THEREON, AND YOUR USE OF THE SITE AND MAKES NO WARRANTIES OR REPRESENTATIONS ABOUT THE ACCURACY OR COMPLETENESS OF THIS SITE’S CONTENT OR THE CONTENT OF ANY SITES LINKED TO THIS SITE AND ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT, (II) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF OUR SERVICES, (III) ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN, (IV) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM OUR SERVICES, (IV) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE WHICH MAY BE TRANSMITTED TO OR THROUGH OUR SERVICES BY ANY THIRD PARTY, AND/OR (V) ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE SERVICES. MONKEY CHALLENGES DOES NOT WARRANT, ENDORSE, GUARANTEE, OR ASSUME RESPONSIBILITY FOR ANY PRODUCT OR SERVICE ADVERTISED OR OFFERED BY A THIRD PARTY THROUGH THE SERVICES OR ANY HYPERLINKED SERVICES OR FEATURED IN ANY BANNER OR OTHER ADVERTISING, AND MONKEY CHALLENGES WILL NOT BE A PARTY TO OR IN ANY WAY BE RESPONSIBLE FOR MONITORING ANY TRANSACTION BETWEEN YOU AND THIRD-PARTY PROVIDERS OF PRODUCTS OR SERVICES. AS WITH THE PURCHASE OF A PRODUCT OR SERVICE THROUGH ANY MEDIUM OR IN ANY ENVIRONMENT, YOU SHOULD USE YOUR BEST JUDGMENT AND EXERCISE CAUTION WHERE APPROPRIATE.</Text></View>
     <View style={{paddingTop:10}}><Text >IN NO EVENT SHALL MONKEY CHALLENGES, ITS OFFICERS, DIRECTORS, EMPLOYEES, OR AGENTS, BE LIABLE TO YOU FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES WHATSOEVER RESULTING FROM ANY (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT, (II) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF OUR SERVICES, (III) ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN, (IV) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM OUR SERVICES, (IV) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE, WHICH MAY BE TRANSMITTED TO OR THROUGH OUR SERVICES BY ANY THIRD PARTY, AND/OR (V) ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF YOUR USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE SERVICES, WHETHER BASED ON WARRANTY, CONTRACT, TORT, OR ANY OTHER LEGAL THEORY, AND WHETHER OR NOT THE COMPANY IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION.</Text></View>
     <View style={{paddingTop:10}}><Text >YOU SPECIFICALLY ACKNOWLEDGE THAT 1UP CHALLENGES SHALL NOT BE LIABLE FOR CONTENT OR THE DEFAMATORY, OFFENSIVE, OR ILLEGAL CONDUCT OF ANY THIRD PARTY AND THAT THE RISK OF HARM OR DAMAGE FROM THE FOREGOING RESTS ENTIRELY WITH YOU.</Text></View>
     <View style={{paddingTop:10}}><Text >The site is controlled and offered by Monkey Challenges from its facilities in the United States of America. Monkey Challenges makes no representations that the Service is appropriate or available for use in other locations. Those who access or use the Service from other jurisdictions do so at their own volition and are responsible for compliance with local law.</Text></View>
     <View style={{paddingTop:10}}><Text >Our responsibility for defects relating to the products and services available on our site is limited to the procedures described in our return policy set forth below.
     </Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}}>Warranty and Return Policy</Text></View>
     <View style={{paddingTop:10}}><Text >While we do not offer any warranties with respect to the products and services available through our site, we are committed to working with you to ensure that every product under warranty performs to the manufacturer’s specifications.
     </Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}} >Privacy and Customer Information</Text></View>
     <View style={{paddingTop:10}}><Text >We are committed to protecting your privacy. To make your shopping experience more convenient, we gather information about you. We maintain the privacy of your information using security technologies and adhere to policies that prevent unauthorized use of your personal information. See our Privacy Policy.</Text></View>
     <View style={{paddingTop:10}}><Text >At any time, you may update your customer account information by following the instructions posted elsewhere on this site. Here you may update your name, password, billing address, shipping address, e-mail address, telephone number, and credit card information.</Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}}>Service and Support</Text></View>
     <View style={{paddingTop:10}}><Text >Should you have any other questions or concerns, you should contact us by e-mail at __________ email address or by mail at:</Text></View>
     <View style={{paddingTop:10}}><Text >Monkey Challenges</Text></View>
     <View style={{paddingTop:10}}><Text >500 North State College Blvd., Suite 1100</Text></View>
     <View style={{paddingTop:10}}><Text >Orange, CA 92868</Text></View>
     <View style={{paddingTop:10}}><Text >Attention: Service & Support</Text></View>
     <View style={{paddingTop:10}}><Text >We will do our best to help you; however, we cannot guarantee that every problem will be resolved to your satisfaction.</Text></View>
     <View style={{paddingTop:10}}><Text >Except as explicitly noted on this site, the goods and services available through this site are offered by 1Up Challenges, Inc. dba Monkey Challenges, whose principal place of business is located at 500 North State College Blvd., Suite 1100 Orange, California 92868.</Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}}>Force Majeure</Text></View>
     <View style={{paddingTop:10}}><Text >In addition to any excuse provided by applicable law, we shall be excused from liability for non-delivery or delay in delivery of products and service available through our site arising from any event beyond our reasonable control, whether or not foreseeable by either party, including but not limited to, labor disturbance, war, fire, accident, adverse weather, inability to secure transportation, governmental act or regulation, and other causes or events beyond our reasonable control, whether or not similar to those which are enumerated above.</Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}}>Entire Agreement and Other Documents</Text></View>
     <View style={{paddingTop:10}}><Text >These terms and conditions constitute the entire agreement and understanding between us concerning the subject matter hereof and supersedes all prior agreements and understandings of the parties with respect thereto. These terms and conditions may NOT be altered, supplemented, or amended by the use of any other document(s). Any attempt to alter, supplement or amend this document or to enter an order for products or services which are subject to additional or altered terms and conditions shall be null and void, unless otherwise agreed to in a written agreement signed by you and us. To the extent that anything in or associated with this site is in conflict or inconsistent with these terms and conditions, these terms and conditions shall take precedence.</Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}}>Indemnity</Text></View>
     <View style={{paddingTop:10}}><Text >To the extent permitted by applicable law, you agree to defend, indemnify and hold harmless Monkey Challenges, its parent corporation, officers, directors, employees and agents, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney’s fees) arising from: (i) your use of and access to the Service; (ii) your violation of any term of these Terms of Service; (iii) your violation of any third party right, including without limitation any copyright, property, or privacy right; or (iv) any claim that your Content caused damage to a third party. This defense and indemnification obligation will survive these Terms of Service and your use of the Service.</Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}} >Governing Law and Statute of Limitations</Text></View>
     <View style={{paddingTop:10}}><Text >This site (excluding linked site) is controlled Monkey Challenges. It can be accessed from all fifty (50) states, as well as from other countries around the world. As each of these places has laws that may differ from those of California. By accessing this site both of us agree that the statutes and laws of the State of California, without regard to the conflicts of laws principles thereof, will apply to all matters relating to the use of this site and the purchase of products and services available through this site. Each of us agrees and hereby submits to the exclusive personal jurisdiction and venue of the Superior Court of Orange County, California and the United States District Court for the Central District of California with respect to such matters.</Text></View>
     <View style={{paddingTop:10}}><Text >Any cause of action brought by you against us or our Affiliates must be instituted within one year after the cause of action arises or be deemed forever waived and barred.</Text></View>
     <View style={{paddingTop:10}}><Text >We make no representation that the products and services available through our site are appropriate or available for use in locations outside of the United States, and accessing them from territories where such products and services are illegal is prohibited. Those who choose to access this site from other locations do so on their own initiative and are responsible for compliance with local laws.</Text></View>
     <View style={{paddingTop:10}}><Text style={{fontWeight: 'bold',paddingTop:10}}>Severability</Text></View>
     <View style={{paddingTop:10}}><Text >If any provision of these Terms and Conditions of Use is found unenforceable, then that provision will be severed from these Terms and not affect the validity and enforceability of any remaining provisions.</Text></View>

 </View>

       </ScrollView>);}


onNavigationStateChange(navState) {
    this.setState({
      canGoBack: navState.canGoBack
    });
  }

	onBridgeMessage (webViewData) {
			// console.log(webViewData);
	  //   let jsonData = JSON.parse(webViewData);
   //      if(jsonData.signupsuccess){
   //      	console.log('username', jsonData.username);
   //      	console.log('password', jsonData.password);
   //      	this.username =jsonData.username;
   //      	this.password =jsonData.password;

   //      }
	  //   if (jsonData.success) {
	  //    // Alert.alert(jsonData.message);
	  //     console.log('data received', jsonData.message);
	  //    // NavigationActions.login();
	  //     this.isAttempting = true;
	  //     this.props.attemptLogin(this.username, this.password, '', 'login')
	  //   }
	  //   console.log('data received', webViewData, jsonData);
	    //.. do some react native stuff when data is received
	  }


}


const mapStateToProps = (state) => {
  console.log(state)
  return {
          // fetching: state.login.fetching,
          // access_token: state.login.access_token
        }



}

const mapDispatchToProps = (dispatch) => {
  return {
   // attemptLogin: (username, password, facebook_token, loginType) => dispatch(LoginActions.loginRequest(username, password, facebook_token, loginType))
    //attemptFbLogin: (facebook_token) => dispatch(FbLoginActions.fbLoginRequest(facebook_token))

  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Terms)
