// @flow

import React from 'react'
import {
    View,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    Keyboard,
    LayoutAnimation,
    WebView,
    Linking,AsyncStorage
} from 'react-native'
import {connect} from 'react-redux'
import styles from '../Styles/WebLinksScreenStyle'
import {Images, Metrics} from '../../Themes'
import LoginActions from '../../Redux/LoginRedux'
//import FbLoginActions from '../../Redux/FbLoginRedux'
import {Actions as NavigationActions} from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import WebViewBridge from 'react-native-webview-bridge';

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'
import { StyleSheet } from 'react-native'

class PrivacyPolicy extends React.Component {
	constructor (props) {
    super(props)

       this.state = { canGoBack: false,MYuuid:"" };
  }
//   onBack() {
//    NavigationActions.postLogin();
//   //  NavigationActions.pop();
//   //this.refs[webviewbridge].goBack();
//
// }
componentWillMount(){
  this.getMYuuid()

}
getMYuuid() {
  AsyncStorage.getItem('MYuuid').then((value) =>{
    this.setState({MYuuid:value});
  })
 }
	render() {
    let {MYuuid} = this.state
		return (
     <ScrollView ref="myScrollView" keyboardDismissMode='interactive' style={[styles.mainContainer,{padding:10}]}>
     <View><View><Text style={{fontWeight: 'bold',fontSize:18}}>Privacy Policy</Text></View>
      <View><Text>
          The 1UP Challenges, Inc. dba Monkey Challenges (“Monkey Challenges”) offers this privacy policy (the “Policy”) to demonstrate our firm commitment to privacy. This Policy discloses our practices for our Website’s information gathering and dissemination.
      </Text></View>
            <View><Text style={{fontWeight: 'bold',paddingTop:10}}>Your IP Address</Text></View>
            <View><Text>Pursuant to our Policy, we use your IP address to help diagnose problems with our server, and to administer our Website. Your IP address is used to gather broad demographic information. This site automatically collects other information such as browser type, platform, and connection speed.</Text></View>
            <View><Text style={{fontWeight: 'bold',paddingTop:10}}>Links</Text></View>  
            <View><Text>Our Website contains links to other sites. Monkey Challenges is not responsible for the privacy practices or the content of such Websites.</Text></View>  
            <View><Text style={{fontWeight: 'bold',paddingTop:10}}>Customer Information</Text></View> 
            <View><Text>Our online inquiry forms may solicit visitors to voluntarily provide contact information such as name, email address, phone number, and mailing address and demographic information such as age, other personal information, and how you learned about us. Pursuant to our Policy, we use this contact data to facilitate communications with visitors. Visitors, guests, clients and members (hereinafter “customers”) may receive a follow-up email after submitting our online inquiry form, and may be further contacted only by the means specified by the customer in the form. Users may request email or telephone contact, or to subscribe to an opt-in newsletter updating them on program activities. The customer’s contact information may be also used to contact the visitor regarding their specific request or to send administrative notices regarding their use of the site users may opt-out of receiving future mailings (refer to the choice/opt-out section below). Demographic and profile data may be collected at our Website. We use this data to tailor our visitor’s experience at our Website, showing them content that we think they might be interested in, and displaying the content according to their preferences. All collected data is for internal use only and is never shared with third parties.</Text></View>   
            <View><Text style={{fontWeight: 'bold',paddingTop:10}}>Prohibited Content</Text></View> 
            <View><Text>Monkey Challenges in its sole discretion reserves the right to prohibit any language, communication, content or image(s) should in Monkey Challenges sole discretion violate its Terms of Service or which in the sole discretion of its management violates or may violate local, state or federal law. Examples of such prohibited content include but are not limited to nudity, pornography, gambling, extortion, blackmail, money laundering, terrorist activity, and anything which Monkey Challenges in its sole discretion considers dangers or likely to cause physical or mental harm to the person(s) performing the challenge or to other, and thing Monkey Challenges just considers stupid.</Text></View> 
            <View><Text>Monkey Challenges reserves the right to any content on its site, including any and all contact information of individual involves in such content over in its entirety to federal, state or local law enforcement agencies, either with or without a valid search warrant. By agreeing to these privacy terms, you expressly agree to permit Monkey Challenges to monitor all of your acivity on its website(s) and turn any such information over to law enforcement at Monkey Challenges sole discretion.</Text></View> 
            <View><Text style={{fontWeight: 'bold',paddingTop:10}}>Security</Text></View> 
            <View><Text>This site has instituted security measures to protect the loss, misuse and alteration of the information under our control. Specifically, (i) our network and database are highly secure; (ii) only authorized employees have access to contact data; (iii) all employees are educated on our privacy policies; (iv) all employees agree to adhere to our privacy policies as a condition of employment; (v) our network and database are password protected and are behind both a firewall and proxy server to ensure security; (vi) our web security is under on-going scrutiny and review to ensure that it meets the highest standards; and (vii) collected information is never shared with third parties.</Text></View> 
            <View><Text style={{fontWeight: 'bold',paddingTop:10}}>Choice/Opt-Out</Text></View> 
            <View><Text>Our Website provides users the opportunity to opt-in to receiving communications from us at the point where we request information about the visitor. This site gives users the following options for removing their information from our database to not receive future communications or to no longer receive our service: Monkey Challenges, 500 N. State College Blvd., Suite 1100 Orange, California 92868.</Text></View>  
            <View><Text style={{fontWeight: 'bold',paddingTop:10}}>Cookies</Text></View>
            <View><Text>The Monkey Challenges uses cookies exclusively for session maintenance when a user completes the online application section.</Text></View>  
            <View><Text style={{fontWeight: 'bold',paddingTop:10}}>Disclaimer</Text></View>
            <View><Text>We may disclose personal information when required by law or in the good-faith belief that such action is necessary in order to conform to the edicts of the law or comply with a legal process served on our Website.</Text></View>
            <View><Text style={{fontWeight: 'bold',paddingTop:10}}>Notification of Changes</Text></View>
            <View><Text>Information collected will only be used in a manner differing from this statement if active email consent is provided by the customer. Any changes to this statement will be communicated to the customer via email.</Text></View>
            <View><Text style={{fontWeight: 'bold',paddingTop:10}}>Contacting the Web Site</Text></View>
            <View><Text>If you have any questions about this privacy statement, the practices of this site, or your dealings with this Web site, you can contact us as follows:</Text></View>
            <View><Text>Monkey Challenges</Text></View>
            <View><Text>500 N. State College Blvd., Suite 1100</Text></View>
            <View><Text>Orange, California 92868</Text></View>
            </View>    
 </ScrollView>);}


onNavigationStateChange(navState) {
    this.setState({
      canGoBack: navState.canGoBack
    });
  }

	onBridgeMessage (webViewData) {
			// console.log(webViewData);
	  //   let jsonData = JSON.parse(webViewData);
   //      if(jsonData.signupsuccess){
   //      	console.log('username', jsonData.username);
   //      	console.log('password', jsonData.password);
   //      	this.username =jsonData.username;
   //      	this.password =jsonData.password;

   //      }
	  //   if (jsonData.success) {
	  //    // Alert.alert(jsonData.message);
	  //     console.log('data received', jsonData.message);
	  //    // NavigationActions.login();
	  //     this.isAttempting = true;
	  //     this.props.attemptLogin(this.username, this.password, '', 'login')
	  //   }
	  //   console.log('data received', webViewData, jsonData);
	    //.. do some react native stuff when data is received
	  }


}


const mapStateToProps = (state) => {
  console.log(state)
  return {
          // fetching: state.login.fetching,
          // access_token: state.login.access_token
        }



}

const mapDispatchToProps = (dispatch) => {
  return {
   // attemptLogin: (username, password, facebook_token, loginType) => dispatch(LoginActions.loginRequest(username, password, facebook_token, loginType))
    //attemptFbLogin: (facebook_token) => dispatch(FbLoginActions.fbLoginRequest(facebook_token))

  }
}


export default connect(mapStateToProps, mapDispatchToProps)(PrivacyPolicy)
