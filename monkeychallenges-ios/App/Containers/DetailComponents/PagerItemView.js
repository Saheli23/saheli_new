
import React, {Component} from 'react';
import {StyleSheet, View,Text, ScrollView} from 'react-native';

function generateShapePagerView(shapeStyle) {
    return () => {
        return (
             <View style={styles.container}>
            <ScrollView vertical>
            <View style={styles.pageContainer}>
            <Text>content</Text> 
            </View>
             </ScrollView>
             </View>
        );
    }
}




export class StoryView extends React.Component {
    render () {
        return (
        <View style={styles.container}>
            <ScrollView vertical>
            <View style={styles.pageContainer}>
            <Text>story</Text>    
            </View>
            </ScrollView>
         </View>
        );
    }
}

export class CommentView extends React.Component {
    render () {
        return (
        <View style={styles.container}>
            <ScrollView vertical>
            <View style={styles.pageContainer}>
            <Text>CommentView</Text>    
            </View>
            </ScrollView>
         </View>
        );
    }
}
export class FunderView extends React.Component {
    render () {
        return (
        <View style={styles.container}>
            <ScrollView vertical>
            <View style={styles.pageContainer}>
            <Text>FundView</Text>    
            </View>
            </ScrollView>
         </View>
        );
    }
}


const styles = StyleSheet.create({
    pageContainer: {
        flex:1,       
      alignItems: 'center',
      justifyContent: 'center',   
    },
    container: {
      flex: 1,
      marginBottom:0,
    },
    mainRec: {
        backgroundColor: 'black',
        width: 256,
        height: 48,
        marginBottom: 10,
        marginTop: 156
    },
    subRec: {
        backgroundColor: 'red',
        width: 256,
        height: 30,
        marginTop: 10
    },
    shapeBase: {
        width: 128,
        height: 128,
        backgroundColor: 'red'
    },
    square: {},
    circle: {
        borderRadius: 64
    },
    triangle: {
        borderTopWidth: 0,
        borderRightWidth: 70,
        borderBottomWidth: 128,
        borderLeftWidth: 70,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderLeftColor: 'transparent',
        borderBottomColor: 'red',
        backgroundColor: 'transparent',
        width: 140
    }
});


export const StoryPagerView = generateShapePagerView();
export const CommentsPagerView = generateShapePagerView(styles.triangle);
export const FundersPagerView = generateShapePagerView(styles.circle);