// @flow

import React from 'react'
import {
        StyleSheet,
        ScrollView,
        Text,
        KeyboardAvoidingView,
        Image,
        ListView,
        View,
        Platform,
        TouchableOpacity,
        AsyncStorage,
        Animated,
        AlertIOS,
        Dimensions,
        Alert
      } from 'react-native'
      const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import { Actions as NavigationActions } from 'react-native-router-flux'
import RoundedButton from '../Components/RoundedButton'

// progressBar
import * as Progress from 'react-native-progress'


import GetUuidActions from '../Redux/GetUuidRedux'
import VoteJudgeActions from '../Redux/VoteJudgeRedux'
import DetailJudgingChallengesActions from '../Redux/DetailJudgingChallengesRedux'

import JudgingChallengesActions from '../Redux/JudgingChallengesRedux'

//import MyJudgingChallengesActions from '../Redux/MyJudgingChallengesRedux'

// detail view components
import {StoryPagerView, CommentsPagerView, FundersPagerView} from './DetailComponents/PagerItemView'


import {IndicatorViewPager, PagerTitleIndicator} from 'rn-viewpager'

import NavItems from '../Navigation/NavItems'

import { Colors, Metrics, Images } from '../Themes'
// Styles
//import styles from './Styles/ChallengeDetailViewStyle'
import styles from './Styles/OpenChallengesDetailViewStyle'


import Icon from 'react-native-vector-icons/FontAwesome'

let moment = require('moment');

import * as _ from 'lodash'

import GiftedSpinner from 'react-native-gifted-spinner'
import AlertMessage from '../Components/AlertMessage'
import VideoPlayer from 'react-native-video-controls';
import Toast, {DURATION} from 'react-native-easy-toast';
//import Video from 'react-native-video'
//TitleIndicatorPage
//JudgingChallengeDetailView



class FooterBar extends React.Component{
componentWillReceiveProps (newProps) {
    console.log(newProps);
  }

  pressHandlerVote(data){
    this.props.getHandlerVote(data)

  }
  pressVoteAgain(){
   //console.log("voteagain");
    Alert.alert(
         'Alert Message',
         'Oops! Looks like you already voted.',
       )
  }
  render(){
        let itemData = this.props.itemData
        let startTime,
        endTime,
        duration,
        days,
        hours,
        mins;
        update = () => {
          console.log("triggered")
          startTime = moment(new Date()),
          endTime = moment(itemData.funding_expiration_date),
          duration = moment.duration(endTime.diff(startTime)),
          days = moment().isAfter(itemData.funding_expiration_date)? 0 : duration.days(),
          hours = moment().isAfter(itemData.funding_expiration_date)? 0 : duration.hours(),
          mins = moment().isAfter(itemData.funding_expiration_date)? 0 : duration.minutes();
        }

        update();
        //setInterval(update, 1000);
        console.log(itemData.funding_expiration_date);
        console.log(moment.duration(endTime.diff(startTime,'days')),hours,mins);
        let MYuuid = this.props.MYuuid
        let uservote = itemData.challenge_votes.filter(votes => votes['uuid'] == MYuuid)
        let winCount = itemData.challenge_votes.filter(votes => votes['vote'] == true)
        let failCount = itemData.challenge_votes.filter(votes => votes['vote'] == false)

        console.log(uservote);
        console.log(winCount);
        console.log(failCount);


    if(uservote.length > 0){
      //this.judged = true;
      console.log(uservote);
    }else{
      console.log(uservote);
    }
    return <View>

    <View style={styles.FooterBarTopBar}>
        <View style={styles.FooterBarTopBarInsideView}>
            <Text>Days</Text>
            <Text style={styles.FooterBarTopBarInsideText}>{days}</Text>
        </View>
        <View style={styles.FooterBarTopBarInsideView}>
            <Text>Hours</Text>
            <Text style={styles.FooterBarTopBarInsideText}>{hours}</Text>
        </View>
        <View style={styles.FooterBarTopBarInsideView}>
            <Text>Minutes</Text>
            <Text style={styles.FooterBarTopBarInsideText}>{mins}</Text>
        </View>
    </View>

        {uservote.length == 0 &&
            <View style={{height: 60,
        backgroundColor: 'white',
        margin: 0,
        padding:0,
        flexDirection: 'row'
        ,alignItems: 'stretch',justifyContent: 'space-around',}}>
        <TouchableOpacity style={{backgroundColor: "#33CC32",justifyContent: 'center',alignItems: 'center',flexGrow:1}} onPress={() => this.pressHandlerVote({type:'win'})}>
            <Text style={{fontWeight: 'bold',color: 'white',fontWeight: 'bold',}}>Yes! Win!</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{backgroundColor: "#E2340C",justifyContent: 'center',alignItems: 'center',flexGrow:1}} onPress={() => this.pressHandlerVote({type:'fail'})}>
            <Text style={{fontWeight: 'bold',color: 'white',fontWeight: 'bold',}}>No, Fail.</Text>
        </TouchableOpacity>
        </View>

        }
         {uservote.length > 0 &&
     <View style={{height: 60,
        backgroundColor: 'white',
        margin: 0,
        padding:0,
        flexDirection: 'row'
        ,alignItems: 'stretch',justifyContent: 'space-around',}}>
        <TouchableOpacity style={{backgroundColor: "#33CC32",justifyContent: 'center',alignItems: 'center',flexGrow:1}} onPress={() => this.pressVoteAgain()}>
            <Text style={{fontWeight: 'bold',color: 'white',fontWeight: 'bold',}}>{winCount.length} WINS</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{backgroundColor: "#E2340C",justifyContent: 'center',alignItems: 'center',flexGrow:1}} onPress={() => this.pressVoteAgain()}>
            <Text style={{fontWeight: 'bold',color: 'white',fontWeight: 'bold',}}>{failCount.length} FAILS</Text>
        </TouchableOpacity>

        </View>
        }
</View>
  }
}


class FunderView extends React.Component {
  render(){
     let itemData = this.props.itemData
    return <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: itemData.uuid })}} style={{width:(width - 20),flexDirection:'row',alignSelf:'center',paddingTop:5,marginTop:5,marginBottom:5}}>
    <View>
    <Image style={{ height: 50, width:50, borderRadius:25 }}
        source={{uri: itemData.latest_profile_image}}
        defaultSource={require('../Images/person.png')} />
    </ View>
    <View  style={{flex:1,flexDirection:'row',justifyContent: 'space-between',paddingLeft:5,}}>
        <View style={{width:120,flexDirection:'column',alignItems: 'flex-start',}}>
          <View><Text style={{color: '#1266AF',fontWeight:'bold',}}>{itemData.handle_name}</Text></View>
          <Text style={{fontSize:10,color:'#1266AF'}}>{itemData.username}</Text>
        </View>
        <View style={{flexDirection:'column',alignItems: 'flex-end', justifyContent: 'space-between',}}>
        <Text style={{fontWeight:'bold'}}>${itemData.amount_contributed}</Text>
      <Text style={{fontSize:12,color:'#9999A2'}}>{moment(itemData.created_at).startOf('hour').fromNow()}</Text>

        </View>
    </ View>


    </TouchableOpacity>
  }
}


class CommentView extends React.Component {
  render(){
     let itemData = this.props.itemData
    return <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: itemData.uuid })}} style={{width:(width - 20),flexDirection:'row',alignSelf:'center',paddingTop:5,marginTop:5,marginBottom:5}}>
    <View>
    <Image style={{ height: 50, width:50, borderRadius:25 }}
        source={{uri: itemData.latest_profile_image}}
        defaultSource={require('../Images/person.png')} />
    </ View>
      <View style={{paddingLeft:10,flexDirection:'column',alignItems: 'flex-start',position:'absolute',left:50,right:0,top:5}}>
       <Text style={{fontSize:12,color:'#9999A2',position:'absolute',right:0}}>{moment(itemData.created_at).startOf('hour').fromNow()}</Text>
        <View><Text style={{color: '#1266AF',fontWeight:'bold',}}>{itemData.handle_name}</Text></View>
        <Text style={{fontSize:10,color:'#1266AF'}}>{itemData.username}</Text>
        <Text style={{fontSize:12,marginTop:5}}>{itemData.comment}</Text>
        </View>
    </TouchableOpacity>
  }
}

class StoryView extends React.Component {
  render(){
     let itemData = this.props.itemData
    return <ScrollView vertical>
    <View style={{width:(width - 20), alignSelf:'center'}}>
    <View style={{paddingTop:10}}>
        <Text style={{fontSize:12,color:'#878787',fontWeight:'bold'}}>Posted : {moment(itemData.created_at).format("MMMM Do, YYYY")}</Text>
    </View>
    <View style={{paddingTop:10,flexDirection: 'row', flexWrap: 'wrap',}}>
        <Text style={{color: 'black',fontSize:13}}>
            <Text style={{color: '#f9642f',fontWeight:'bold'}}>Challenge description : </Text>{itemData.description}</Text>
    </View>
    <View style={{paddingTop:10,flexDirection: 'row', flexWrap: 'wrap'}}>
        <Text style={{color: 'black',fontSize:13}}>
            <Text style={{color: '#f9642f',fontWeight:'bold'}}>What to judge this challenge on : </Text>{itemData.description_3}</Text>
    </View>
    <View style={{paddingTop:10,flexDirection: 'row', flexWrap: 'wrap'}}>
        <Text style={{color: 'black',fontSize:13}}>
            <Text style={{color: '#f9642f',fontWeight:'bold'}}>What to expect with this challenge result : </Text>{itemData.description_2}</Text>
    </View>
    </View>
</ScrollView>

  }
}



class ChallengeViewPreState extends React.Component{

  constructor (props) {
   super(props)
   this.state = {
     isVideo:false,
     videoPath:'',
     isVideoShow:false,
     resizeMode: 'cover',
     isFullscreenOn:false,
     isFavourite:''
   }

   this.getStyle = this.getStyle.bind(this);
}
getUrlPath(){
  let itemData = this.props.itemData
itemData.challenge_media.filter((item)=>{
    if(item.media_type_id == 6){
      this.setState({isVideo:true,videoPath:item.path});
      return item.path;
    }else{
      this.setState({isVideo:false,videoPath:''});
      return false;
    }
  })
  // console.log(videoPath);
  // if(!videoPath){
  //   console.log(videoPath);
  //   this.setState({isVideo:true,videoPath:videoPath});
  // }

}
handlePlayVideo = () => {
//console.log(this.state.isVideoShow);
  this.setState({
    isVideoShow : !this.state.isVideoShow,
    rate: 1,
    volume: 1,
    muted: false,
    //resizeMode: 'stretch',
    resizeMode: 'contain',
    duration: 0.0,
    currentTime: 0.0,
    controls: false,
    paused: false,
    skin: 'custom',
    ignoreSilentSwitch: null,
    isBuffering: false,
  });
}
handlePassVideo = () => {

  this.setState({
    paused: !this.state.paused
  });
}
onLoad = (data) => {

  console.log('On load fired!');
   this.setState({duration: data.duration});
}
loadStart = () => {
  //this.player.seek(0)
}
onProgress = (data) => {
  console.log(data);
  this.setState({currentTime: data.currentTime});

}
onBuffer = ({ isBuffering }: { isBuffering: boolean }) => {
  this.setState({ isBuffering });
}

getCurrentTimePercentage = () => {
    if (this.state.currentTime > 0) {
      return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
    } else {
      return 0;
    }
  }


  renderSkinControl(skin) {
    const isSelected = this.state.skin == skin;
    const selectControls = skin == 'native' || skin == 'embed';
    return (
      <TouchableOpacity onPress={() => { this.setState({
          controls: selectControls,
          skin: skin
        }) }}>
        <Text style={[styles1.controlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
          {skin}
        </Text>
      </TouchableOpacity>
    );
  }

  renderRateControl(rate) {
  const isSelected = (this.state.rate == rate);

  return (
    <TouchableOpacity onPress={() => { this.setState({rate: rate}) }}>
      <Text style={[styles1.controlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
        {rate}x
      </Text>
    </TouchableOpacity>
  )
}

renderIgnoreSilentSwitchControl(ignoreSilentSwitch) {
   const isSelected = (this.state.ignoreSilentSwitch == ignoreSilentSwitch);

   return (
     <TouchableOpacity onPress={() => { this.setState({ignoreSilentSwitch: ignoreSilentSwitch}) }}>
       <Text style={[styles1.controlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
         {ignoreSilentSwitch}
       </Text>
     </TouchableOpacity>
   )
 }

renderResizeModeControl(resizeMode) {
  const isSelected = (this.state.resizeMode == resizeMode);

  return (
    <TouchableOpacity onPress={() => { this.setState({resizeMode: resizeMode}) }}>
      <Text style={[styles1.controlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
        {resizeMode}
      </Text>
    </TouchableOpacity>
  )
}

renderVolumeControl(volume) {
  const isSelected = (this.state.volume == volume);

  return (
    <TouchableOpacity onPress={() => { this.setState({volume: volume}) }}>
      <Text style={[styles1.controlOption, {fontWeight: isSelected ? "bold" : "normal"}]}>
        {volume * 100}%
      </Text>
    </TouchableOpacity>
  )
}

componentWillMount(){
  this.getUrlPath();
  let isFavouritestatus

  console.log('this.props',this.props);
  let itemData = this.props.itemData
  let uuid     = this.props.userId

   console.log('itemData',itemData);
   console.log('uuid',uuid);
  if (itemData['challenge_favorite'].length > 0){
  let favourite = _.result(_.find(itemData['challenge_favorite'], function(obj) {
        return ( obj['uuid'] == uuid );
    }), '_id');

    // console.log('favourite',favourite);
    isFavouritestatus = favourite ? true : false;
    //console.log(isFavourite);

    this.setState({isFavourite:isFavouritestatus})

  }
  else{
    this.setState({isFavourite:false})
  }
}
handleEndVideo(){
  console.log("ends")
}

getStyle(){
  if(this.state.isFullscreenOn){
    console.log('isFullscreenOn -- true');
    return [styles1.backgroundVideo,{width:width,height:height}];
  }else{
    console.log('isFullscreenOn -- false');
    return styles1.backgroundVideo;
  }
}
getOpenChallengesUnFavCall(unfav,page,itemDataId){
this.props.getJudgingChallenges(unfav,page,itemDataId);
 this.setState({isFavourite:false})
}
getOpenChallengesFavCall(fav,page,itemDataId){
this.props.getJudgingChallenges(fav,page,itemDataId);
this.setState({isFavourite:true})
}
render () {
  console.log(this.state);
  let itemData = this.props.itemData
  const flexCompleted = this.getCurrentTimePercentage() * 100;
  const flexRemaining = (1 - this.getCurrentTimePercentage()) * 100;
  //console.log(this.props.isVideo);
  const videoStyle = this.state.skin == 'embed' ? styles.nativeVideoControls : styles.fullScreen;
  let myOwnChallenge
  myOwnChallenge =this.props.ownChallenge
  console.log('myOwnChallenge',myOwnChallenge);
   return <View style={styles.challenge}>
      <View style={styles.imageSection}>
        {!this.state.isVideoShow &&
                  <Image style={styles.cardImage} source={{uri: itemData.challenge_media[0].path}} defaultSource={require('../Images/monkey_default.png')} />
        }
        {this.state.isVideoShow && !this.state.isFullscreenOn &&


  <VideoPlayer
          source={{uri: this.state.videoPath}}
          style={this.getStyle()}
          ref={(ref) => {
                this.player = ref;
              }}
          playWhenInactive={ false }
          resizeMode={ this.state.resizeMode }
          paused={ this.state.paused }
          repeat={ false }
          muted={ this.state.muted }
          title={ '' }
          volume={ this.state.volume }
          rate={ this.state.rate }
          seeking={ true }
          onError={ () => {alert("Error in video file!");} }
          onBack={ () => {NavigationActions.pop()} }
          onEnd={ () => {} }
          toggleFullscreen={()=>{
             if(this.state.isFullscreenOn){
               this.setState((a, b) => {
                 return {isFullscreenOn: false};
               });
             }
             else{
                this.setState(() => {
                  return {isFullscreenOn: true};
               });
             }
          }}

          controlTimeout={ 15000 }
          seekColor={ '#FFF' }
          videoStyle={{}}
  />


}

          {(itemData.charity == 1) &&
            <View style={{position:'absolute',backgroundColor:'black',padding:3,flexDirection:'row',top:10,left:5,borderRadius:5}}>
            <Text style={{color:'#FFFFFF',fontWeight:'bold',fontSize:10}}>CHARITY</Text>
            </View>
            }


            {this.state.isVideo && !this.state.isVideoShow &&
              <View style={[styles.playButton,{zIndex:99}]}>
                <TouchableOpacity onPress={this.handlePlayVideo}>
                            <Image style={{width:50,height:50}} source={Images.play}/>
                </TouchableOpacity>

  {/*
              <TouchableOpacity onPress={this.handlePlayVideo} style={{

                backgroundColor:'black',
                padding:10,
                flexDirection:'row',
                borderRadius:5,
                }}>
              <Text style={{color:'#FFFFFF',fontWeight:'bold',fontSize:15}}>Play</Text>
              </TouchableOpacity>

  */}
              </View>
            }

            {this.state.isFavourite && !myOwnChallenge &&
              <TouchableOpacity onPress={()=>{
                 this.getOpenChallengesUnFavCall("UnFavourite",null,itemData._id);
              //  this.props.getOpenChallenges("UnFavourite",null,itemData._id);
              }} style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
              <Icon name='heart' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
              </TouchableOpacity>
            }

            {!this.state.isFavourite  && !myOwnChallenge &&
              <TouchableOpacity onPress={()=>{
                this.getOpenChallengesFavCall("Favourite",null,itemData._id);
               //this.props.getOpenChallenges("Favourite",null,itemData._id);
              }}
               style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
              <Icon name='heart-o' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
              </TouchableOpacity>
            }



        {/*


          <View style={styles.imageFooter}>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Progress.Bar progress={(Math.round(itemData.percentage_funded) / 100)} color={'#33CC32'} height={8} width={(width - 20)} borderWidth={0} unfilledColor={'rgba(128,132,131,0.8)'}/>
            </View>
            <View style={styles.contentVoteSection}>
              <Text style={{color: 'white' }}>$ {itemData.price} USD</ Text>
              <Text style={{color: 'white'}}>{Math.round(itemData.percentage_funded)}%</Text>
              <Text style={{color: 'white', textAlign: 'right'}}>{itemData.days_remaining} days left</Text>
            </View>



          </View>
          */}

{!this.state.isVideoShow &&
        <View style={styles.imageFooter}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <View style={styles.contentVoteSection}>
          <Text style={{color: 'white',fontWeight:'bold', fontSize:30 }}>${parseInt(itemData.price)}<Text style={{fontSize:10}}> FUNDED</Text></ Text>
          {/*
          <Text style={{color: 'white'}}>{Math.round(itemData.percentage_funded)}%</Text>
          */}
          <View style={{flexDirection:'row'}}>

              <Image style={{width:30,height:30,marginRight:10}} source={Images.hammer}/>
            <View style={{flexDirection:'column',alignItems:'flex-start'}}>
              <Text style={{color: 'white', textAlign: 'right', fontWeight:'bold',fontFamily:'Bradley Hand'}}>Judging</Text>
              <Text style={{color: 'white',fontFamily:'Bradley Hand', textAlign: 'right',fontWeight:'bold'}}>Challenge</Text>
            </View>

            </View>
          </View>

          </View>

        </View>

}
{!this.state.isVideoShow &&
        <View style={styles.imageFooterOpacity} />
}

{this.state.isVideoShow &&

  <View style={styles1.controls}>
  {/*
  <View style={styles1.generalControls}>
              <View style={styles1.skinControl}>
                {this.renderSkinControl('custom')}
                {this.renderSkinControl('native')}
                {this.renderSkinControl('embed')}
              </View>
              <Text>{flexCompleted}</Text>
              <Text>{flexRemaining}</Text>
            </View>

    <View style={styles1.generalControls}>
    <View style={styles1.rateControl}>
          {this.renderRateControl(0.5)}
          {this.renderRateControl(1.0)}
          {this.renderRateControl(2.0)}
        </View>

        <View style={styles1.volumeControl}>
          {this.renderVolumeControl(0.5)}
          {this.renderVolumeControl(1)}
          {this.renderVolumeControl(1.5)}
        </View>

        <View style={styles1.resizeModeControl}>
          {this.renderResizeModeControl('cover')}
          {this.renderResizeModeControl('contain')}
          {this.renderResizeModeControl('stretch')}
        </View>
    </View>
    <View style={styles1.generalControls}>
               {
                 (Platform.OS === 'ios') ?
                   <View style={styles1.ignoreSilentSwitchControl}>
                     {this.renderIgnoreSilentSwitchControl('ignore')}
                     {this.renderIgnoreSilentSwitchControl('obey')}
                   </View> : null
               }
             </View>


    */}
  </View>
}
{this.state.isVideoShow && this.state.isFullscreenOn &&


<VideoPlayer
        source={{uri: this.state.videoPath}}
        style={this.getStyle()}
        ref={(ref) => {
              this.player = ref;
            }}
        playWhenInactive={ false }
        resizeMode={ this.state.resizeMode }
        paused={ this.state.paused }
        repeat={ false }
        muted={ this.state.muted }
        title={ '' }
        volume={ this.state.volume }
        rate={ this.state.rate }
        seeking={ true }
        onError={ () => {alert("Error in video file!");} }
        onBack={ () => {NavigationActions.pop()} }
        onEnd={ () => {} }
        toggleFullscreen={()=>{
           if(this.state.isFullscreenOn){
             this.setState((a, b) => {
               return {isFullscreenOn: false};
             });
           }
           else{
              this.setState(() => {
                return {isFullscreenOn: true};
             });
           }
        }}

        controlTimeout={ 15000 }
        seekColor={ '#FFF' }
        videoStyle={{}}
/>


}
      </View>


      <View style={[styles.contentSection,{zIndex:(Platform.OS === 'ios') ? -1 : 99}]}>

      <View style={[styles.contentDescriptionSection,{flexDirection:'column',width:(width - 20),alignSelf:'center'}]}>

    {/*
           <Text style={{color: '#1266AF',fontWeight:'bold',}}>{rowData.handle_name} :
            <Text style={{color: '#000000'}}> {rowData.name}</Text></Text>
            <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: rowData.uuid })}}>
    */}

    <View style={{flexDirection:'row',width:(width - 20),}}>
      <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: itemData.uuid })}}>
          <Text style={{color: '#1266AF',fontWeight:'bold',flexWrap: 'wrap'}}>{itemData.handle_name} :
          <Text numberOfLines={2} style={{color: '#000000',fontWeight:'bold',}}> {itemData.name}</Text></Text>
      </TouchableOpacity>
    </View>
          <Text style={{fontSize:12}} numberOfLines={2}>{itemData.description}</Text>
      </View>

      </View>
      </View>
  }
}

type JudgingChallengeDetailProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,
    uuid:string,
    getUuid: () => void,
    getDetails:() => void,
    setVote: () => void,

}
class JudgingChallengeDetailPage extends React.Component{

   props: JudgingChallengeDetailProps

    animationstate = {
        bgColor: new Animated.Value(0)
    };

   constructor (props) {
    super(props)
    this.state = {
      MYuuid:'',
      isVideo:false,
      videoPath:''
    }
  }

  getDateFormet(date){
    return new Date(date);

  }
 componentWillMount(){
   this.getMYuuid()
  // this.props.getDetails('default',this.props.item._id,undefined)
  this.props.getDetails(this.props.item._id)
  //console.log(this.props.item);

}



getMYuuid() {
  AsyncStorage.getItem('MYuuid').then((value) =>{
    this.setState({MYuuid:value});
  })
 }
  componentWillReceiveProps (newProps) {
    if (!newProps.fetching) {
       this.setState({fetching:newProps.fetching, payload: newProps.payload})
     }else{
        this.setState({fetching:newProps.fetching, payload: newProps.payload})
     }
  }


  _renderTitleIndicator(itemData) {
    let arr = [`Story`, `Comments (${itemData.challenge_comments.length})`, `Funders (${itemData.challenge_challengers.length})`];
      return (
          <PagerTitleIndicator
              style={styles.indicatorContainer}
              itemTextStyle={styles.indicatorText}
              selectedItemTextStyle={styles.indicatorSelectedText}
              selectedBorderStyle={styles.selectedBorderStyle}
              itemsContainerStyle={{flex:1}}
              titles={arr}
          />
      );
  }

  _onPageScroll(scrollData) {
      let {offset, position}=scrollData;
      if (position < 0 || position >= 2) return;
      this._setBgColor({bgColor: offset + position});
  }

  handleRequestVote = (data) => {

    if(data.type == "win"){
      console.log({'vote':true,'challenge_id':this.props.item._id});
      this.props.getDetails(this.props.item._id,'vote',true)
    }else if(data.type == "fail"){
      console.log({'vote':false,'challenge_id':this.props.item._id});
      this.props.getDetails(this.props.item._id,'vote',false)
    }
  }
    _setBgColor = Animated.event([{bgColor: this.animationstate.bgColor}]);





    // getUrlPath(itemData){
    // //  let itemData = this.props.itemData
    // console.log(itemData);
    // console.log(this.state.payload);
    // itemData.challenge_media.filter((item)=>{
    //     if(item.media_type_id == 6){
    //       this.setState({isVideo:true,videoPath:item.path});
    //       return item.path;
    //     }else{
    //       this.setState({isVideo:false,videoPath:''});
    //       return false;
    //     }
    //   })
    //   // console.log(videoPath);
    //   // if(!videoPath){
    //   //   console.log(videoPath);
    //   //   this.setState({isVideo:true,videoPath:videoPath});
    //   // }
    //
    // }


    render() {
      //let itemData = this.props.item
        let getJudgingChallenges = this.props.getJudgingChallenges;
        let ownChallenge =false;
        if(this.props.myChallenge && this.props.myChallenge == 'true'){

          ownChallenge = true
        }
        else {

          ownChallenge =false
        }
    // let itemData.created_at.date = this.props.item.created_at.date
    //  console.log(itemData);
        let bgColor = this.animationstate.bgColor.interpolate({
            inputRange: [0, 1, 2],
            outputRange: ['hsl(187, 74%, 47%)', 'hsl(89, 47%, 54%)', 'hsl(12, 97%, 59%)']
        });

        let payload = ( this.state && this.state.payload )|| { challenges: [] }
        console.log(payload.challenges, 'playload.data')

    // this.getUrlPath(this.state.payload)


        return (


     <View style={styles.mainContainer}>

    { this.state.fetching &&
     <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:(Platform.OS === 'ios') ? 9999 : -1,justifyContent: 'center',alignItems: 'center',}}>
    <GiftedSpinner size='large' />
    </View>
    }
{payload.challenges.length > 0 &&
  <View>
    <ScrollView vertical>

       <ChallengeViewPreState itemData={payload.challenges[0]} userId={this.state.MYuuid} getJudgingChallenges={getJudgingChallenges} ownChallenge={ownChallenge} />


        <IndicatorViewPager
            style={{flex: 1, flexDirection: 'column-reverse',minHeight:(Platform.OS === 'ios') ? 400 : 180,zIndex:(Platform.OS === 'ios') ? -1 : 999}}
            indicator={this._renderTitleIndicator(payload.challenges[0])}
            onPageScroll={this._onPageScroll.bind(this)}
        >
  <View style={{ flex:1,}}>
   <StoryView itemData={payload.challenges[0]} />
  </View>



 <View style={{ flex:1}}>


 {payload.challenges[0].challenge_comments.length < 1 &&

    <AlertMessage title='No comments Here, Move Along' show={(payload.challenges[0].challenge_comments.length < 1)} />

 }

    {
                payload.challenges[0].challenge_comments.map((rowData, index) => {
                  return <CommentView itemData={rowData} key={index} />
                })
    }

  </View>

  <View style={{ flex:1}}>

  {payload.challenges[0].challenge_challengers.length < 1 &&

     <AlertMessage title='No Funders Here, Move Along' show={(payload.challenges[0].challenge_challengers.length < 1)} />

  }

    {
                payload.challenges[0].challenge_challengers.map((rowData, index) => {
                  return <FunderView itemData={rowData} key={index} />
                })
    }

  </View>
</IndicatorViewPager>


</ScrollView>



<FooterBar itemData={payload.challenges[0]} MYuuid={this.state.MYuuid} getHandlerVote={data =>
                this.handleRequestVote(data)} />
  </View>
}
  </View>


        );
    }
}

var styles1 = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    width:width,

  },
  container: {
   flex: 1,
   justifyContent: 'center',
   alignItems: 'center',
   backgroundColor: 'black',
 },
 fullScreen: {
   position: 'absolute',
   top: 0,
   left: 0,
   bottom: 0,
   right: 0,
 },
 controls: {
   backgroundColor: "transparent",
   borderRadius: 5,
   position: 'absolute',
   bottom: 44,
   left: 4,
   right: 4,
 },
 progress: {
   flex: 1,
   flexDirection: 'row',
   borderRadius: 3,
   overflow: 'hidden',
 },
 innerProgressCompleted: {
   height: 20,
   backgroundColor: '#cccccc',
 },
 innerProgressRemaining: {
   height: 20,
   backgroundColor: '#2C2C2C',
 },
 generalControls: {
   flex: 1,
   flexDirection: 'row',
   overflow: 'hidden',
   paddingBottom: 10,
 },
 skinControl: {
   flex: 1,
   flexDirection: 'row',
   justifyContent: 'center',
 },
 rateControl: {
   flex: 1,
   flexDirection: 'row',
   justifyContent: 'center',
 },
 volumeControl: {
   flex: 1,
   flexDirection: 'row',
   justifyContent: 'center',
 },
 resizeModeControl: {
   flex: 1,
   flexDirection: 'row',
   alignItems: 'center',
   justifyContent: 'center'
 },
 ignoreSilentSwitchControl: {
   flex: 1,
   flexDirection: 'row',
   alignItems: 'center',
   justifyContent: 'center'
 },
 controlOption: {
   alignSelf: 'center',
   fontSize: 11,
   color: "white",
   paddingLeft: 2,
   paddingRight: 2,
   lineHeight: 12,
 },
 nativeVideoControls: {
   top: 184,
   height: 300
 }
});

const mapStateToProps = (state) => {
   return {
    fetching: state.detailJudgingChallenges.fetching,
    payload: state.detailJudgingChallenges.payload
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
     getDetails:(id, detail_type = 'default', vote = 'null') => {
     return dispatch(DetailJudgingChallengesActions.detailJudgingChallengesRequest(id, detail_type, vote))
},
getJudgingChallenges: (default_type = 'default', page = 1, challengeId = null) =>{
  console.log(default_type,page,challengeId);
return dispatch(JudgingChallengesActions.judgingChallengesRequest(default_type, page, challengeId))
}

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(JudgingChallengeDetailPage)
