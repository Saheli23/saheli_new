// @flow

import React from 'react'
import {ScrollView, Text, Image, View, TouchableOpacity,WebView,Linking,AsyncStorage} from 'react-native'
import {Images} from '../Themes'
import RoundedButton from '../Components/RoundedButton'
import {Actions as NavigationActions} from 'react-native-router-flux'
import WebViewBridge from 'react-native-webview-bridge';
// Styles
import Styles from './Styles/LandingScreenStyle'

const logoContainerStyle = {flex: 0.7, alignSelf: 'center'}
const loginButtonContainerStyle = {
    flexDirection:'row',justifyContent:'center',

      }



 var url="http://oneup-web-env-1.us-west-2.elasticbeanstalk.com/#/user/signup";
export default class LandingScreen extends React.Component {



  componentWillMount () {
    let MCAccessToken='';
    AsyncStorage.getItem('MCAccessToken').then((value) =>{
      if(value !=''){
     NavigationActions.postLogin();
  }
    MCAccessToken = value ;
    console.log(value);
    })

    // if(MCAccessToken !=''){
    //   debugger;
    //   console.log(MCAccessToken);
    //   NavigationActions.postLogin();
    // }
    // else{
    //   debugger;
    // }
  }

    handleSignUp(){
    //   Linking.canOpenURL(url).then(supported => {
    //   if (supported) {
    //     Linking.openURL(url);
    //   } else {
    //     console.log('Don\'t know how to open URI: ' + url);
    //   }
    // });
 //return (


    return (
      <WebViewBridge
        ref="webviewbridge"
       source={{uri: "http://google.com"}}/>
    );


    }
    render() {
        return (
            <View style={Styles.mainContainerwithOutNav}>
                <Image source={Images.landingBg} style={Styles.backgroundImage} resizeMode='cover'/>
                <View style={Styles.LandingVerticalRow}>


            <View style={[Styles.loginRow]}>

            <TouchableOpacity style={Styles.loginButtonWrapper} onPress={NavigationActions.signup}>
              <View style={Styles.loginButton}>
                <Text style={Styles.loginText}>Sign Up</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={Styles.loginButtonWrapper} onPress={NavigationActions.login}>
              <View style={Styles.loginButton}>
                <Text style={Styles.loginText}>Log In</Text>
              </View>
            </TouchableOpacity>
          </View>

                </View>
            </View>
        )
    }
}
