// @flow

import React from 'react'
import {
    View,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    Keyboard,
    LayoutAnimation,
    WebView,
    Linking
} from 'react-native'
import {connect} from 'react-redux'
import Styles from './Styles/LoginScreenStyle'
import {Images, Metrics} from '../Themes'
import LoginActions from '../Redux/LoginRedux'
import FbLoginActions from '../Redux/FbLoginRedux'
import {Actions as NavigationActions} from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import WebViewBridge from 'react-native-webview-bridge';
import { StyleSheet } from 'react-native'
// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'

type SignupScreenProps = {
    dispatch: () => any,
    fetching: boolean,
    loginType:string,
    access_token:string,
    attemptLogin: () => void,

}
class Signup extends React.Component {
	 props: SignupScreenProps

  state: {
        username: string,
        password: string,
        facebook_token: string,
        visibleHeight: number,
        topLogo: {
            width: number
        }
    }

    isAttempting: boolean
    username:string
    password:string

    constructor (props: SignupScreenProps) {
    super(props)

    this.state = {
      username: '',
      password: '',
      loginType: 'login',
      facebook_token:'',
      visibleHeight: Metrics.screenHeight,
      topLogo: {width: Metrics.screenWidth
      },
    }
    this.isAttempting = false
     this.state = { canGoBack: false };
  }
  componentWillReceiveProps (newProps) {
    this.forceUpdate()
        // Did the login attempt complete?
    console.log(newProps, 'newProps')
    if (this.isAttempting && !newProps.fetching && newProps.access_token) {
            // NavigationActions.pop()
      this.setState({fetching:newProps.fetching})

      NavigationActions.postLogin()
    }else{
      this.setState({fetching:newProps.fetching})

    }
  }

	// componentDidMount() {
 //    let chartData = {data: 'data from app'};

 //    // Send this chart data over to web view after 5 seconds.
 //    setTimeout(() => {
 //      this.refs.webviewbridge.sendToBridge(JSON.stringify(chartData));
 //    }, 5000);
 //  }


	render() {
		return (
       <View style={styles.container}>
       <TouchableOpacity

            onPress={this.onBack.bind(this)}
            >
        <View style={styles.topbar}>

            <Text  style={this.state.canGoBack ? styles.topbarText : styles.topbarTextDisabled}>Go Back To App</Text>

        </View>
        </TouchableOpacity>
      <WebViewBridge
        ref="webviewbridge"
        onNavigationStateChange={this._onLoad}
        onNavigationStateChange={this.onNavigationStateChange.bind(this)}
        onBridgeMessage={this.onBridgeMessage.bind(this)}
        startInLoadingState={true}
        source={{uri: webViewBaseLink+"/#/user/signup"}}/></View>);

	}

  onBack() {
    NavigationActions.landingScreen();
  //this.refs[webviewbridge].goBack();

}

 onNavigationStateChange(navState) {
    this.setState({
      canGoBack: navState.canGoBack
    });
  }



	onBridgeMessage (webViewData) {
			console.log(webViewData);
	    let jsonData = JSON.parse(webViewData);
        if(jsonData.signupsuccess){
        	console.log('username', jsonData.username);
        	console.log('password', jsonData.password);
        	this.username =jsonData.username;
        	this.password =jsonData.password;

        }
	    if (jsonData.success) {
	     // Alert.alert(jsonData.message);
	      console.log('data received', jsonData.message);
	     // NavigationActions.login();
	      this.isAttempting = true;
	      this.props.attemptLogin(this.username, this.password, '', 'login')
	    }
	    console.log('data received', webViewData, jsonData);
	    //.. do some react native stuff when data is received
	  }


}


const mapStateToProps = (state) => {
  console.log(state)
  return {
          fetching: state.login.fetching,
          access_token: state.login.access_token
        }



}

const mapDispatchToProps = (dispatch) => {
  return {
    attemptLogin: (username, password, facebook_token, loginType) => dispatch(LoginActions.loginRequest(username, password, facebook_token, loginType))
    //attemptFbLogin: (facebook_token) => dispatch(FbLoginActions.fbLoginRequest(facebook_token))

  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15, /* Padding to push below the navigation bar */
    backgroundColor: '#F5FCFF',
  },
  topbar: {
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
  topbarTextDisabled: {
    color: 'gray'
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Signup)
