// @flow

import React from 'react'
import {
    View,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    Keyboard,
    LayoutAnimation,
    WebView,
    Linking,AsyncStorage
} from 'react-native'
import {connect} from 'react-redux'

//import Styles from '../Styles/LoginScreenStyle'
import styles from './Styles/WebLinksScreenStyle'

import {Images, Metrics} from './../Themes'
import LoginActions from './../Redux/LoginRedux'
//import FbLoginActions from '../../Redux/FbLoginRedux'
import {Actions as NavigationActions} from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import WebViewBridge from 'react-native-webview-bridge';

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'
import { StyleSheet } from 'react-native'
import Toast, {DURATION} from 'react-native-easy-toast'



class ShareChallenge extends React.Component {
	constructor (props) {
    super(props)

     this.state = { canGoBack: false,MYuuid:"",sentfrom:"",mail:"",challengeLink:"",username:"" };
  }


componentWillMount(){
  this.getMYuuid()
  }
getMYuuid() {
      AsyncStorage.getItem('MCUserObj').then((value) =>{
        let userObj = JSON.parse(value);
        //this.setState({sentfrom:userObj.email});
        this.setState({mail:userObj.email,username:userObj.first_name+' '+userObj.last_name})
      })
   }

	render() {
    let {mail,challengeLink,username} = this.state   // myuuid where you are using?
    let {status, _id,name}  = this.props.itemData;
    let challengeName=name.replace(/\s/g, "%20");
    let UserName =username.replace(/\s/g, "%20");
    //console.log('username',username);
    let challengeLink1 = webViewBaseLink+`/#/sharechallenge/${status}/${challengeName}/${_id}/${mail}/${UserName}`;
     console.log(challengeLink1);
    return (
     <View style={styles.mainContainer}>
     {/*
     <TouchableOpacity

            onPress={this.onBack.bind(this)}
            >
        <View style={styles.topbar}>

            <Text  style={this.state.canGoBack ? styles.topbarText : styles.topbarTextDisabled}>Go Back To App</Text>

        </View>
        </TouchableOpacity>
        https://dev-1.monkeychallenges.com/#/myaccount/this.state.MYuuid/password
          //  source={{uri: "https://dev-1.monkeychallenges.com/#/login"}}/>
        */}


      <WebViewBridge
        ref="webviewbridge"
        onNavigationStateChange={this.onNavigationStateChange.bind(this)}
        onBridgeMessage={this.onBridgeMessage.bind(this)}
        startInLoadingState={true}
        source={{uri: challengeLink1}}/>

        <Toast
            ref="toast"
            style={{backgroundColor:'#f9642f'}}
            position='center'
            positionValue={100}
            fadeInDuration={600}
            fadeOutDuration={1000}
            opacity={1}
            textStyle={{color:'white'}}
        />

	     </View>

       );}


onNavigationStateChange(navState) {
    this.setState({
      canGoBack: navState.canGoBack
    });
  }

	onBridgeMessage (webViewData) {
    let jsonData = JSON.parse(webViewData);
		 console.log(jsonData);
	  //   let jsonData = JSON.parse(webViewData);
   //      if(jsonData.signupsuccess){
   //      	console.log('username', jsonData.username);
   //      	console.log('password', jsonData.password);
   //      	this.username =jsonData.username;
   //      	this.password =jsonData.password;

   //      }
	    if (jsonData.success) {
	      //alert(jsonData.message);
        this.refs.toast.show('Shared successfully, please go back to challenge using back button in header.',1000);
	      console.log('data received', jsonData.message);

	    }else{
        this.refs.toast.show('Something went wrong. Try again!',1000);
      }
	  //   console.log('data received', webViewData, jsonData);
	    //.. do some react native stuff when data is received
	  }


}


const mapStateToProps = (state) => {
  console.log(state)
  return {
          // fetching: state.login.fetching,
          // access_token: state.login.access_token
        }



}

const mapDispatchToProps = (dispatch) => {
  return {
   // attemptLogin: (username, password, facebook_token, loginType) => dispatch(LoginActions.loginRequest(username, password, facebook_token, loginType))
    //attemptFbLogin: (facebook_token) => dispatch(FbLoginActions.fbLoginRequest(facebook_token))

  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ShareChallenge)
