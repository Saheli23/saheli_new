// @flow

import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  KeyboardAvoidingView,
  Image,
  ListView,
  View,
  TouchableOpacity,
  TextInput,
  findNodeHandle,
  AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)

import { Metrics, Images } from '../Themes';
import RoundedButton from '../Components/RoundedButton'
// external libs
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions as NavigationActions } from 'react-native-router-flux';
import Animatable from 'react-native-animatable';


import styles from './Styles/StepFormStyle';

// alert message
import AlertMessage from '../Components/AlertMessage';

// I18n
import I18n from 'react-native-i18n';

// progressBar
import * as Progress from 'react-native-progress';

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner';
import * as _ from 'lodash';
import ReportChallengeActions   from '../Redux/ReportChallengeRedux'
import ModalPicker from 'react-native-modal-picker'
import CheckBox from 'react-native-checkbox';

import Toast, {DURATION} from 'react-native-easy-toast'

type ReportChallengeProps = {
  dispatch: () => any,
  fetching: boolean,
  payload: object,


};
class ReportChallenge extends Component {
  props: ReportChallengeProps
  constructor(props) {
    super(props);
    this.state = {
      imageSource:null,
      userFirstname:'',
      userLastname:'',
      userFullname:'',
      terms: false,
      name:'',
      phone_no: "",
      mail:"",
      reason :"",
      subject:"",
      description: "",
      challenge_link:"",
      showButton:true,

   };
  }
  getMYuuid() {
      AsyncStorage.getItem('MCUserObj').then((value) =>{
        let userObj = JSON.parse(value)
        let userFullname = `${userObj.first_name} ${userObj.last_name}`
        this.setState({userFullname:userFullname,mail:userObj.email});
      })
   }

  componentWillMount(){
  this.getMYuuid()
  let {status, _id}  = this.props.itemData;
  let challenge_url=`https://www.monkeychallenges.com/challenges/${status}/${_id}`;
  this.setState({challenge_link:challenge_url})
  }

    componentWillReceiveProps(newProps) {
      //  alert('data came');
       console.log(newProps);
        if(newProps.error){
          //  alert('inside error');
          //  console.log('inside Error',newProps);
            this.setState({ fetching: newProps.fetching,showButton:true});
            this.refs.toast.show('error: Something went wrong! Could not Reported.',1000);
        }else if(newProps.success){
            this.refs.toast.show('Reported successfully, please go back to challenge using back button in header.',1000);
            this.setState({ fetching: newProps.fetching,showButton:false});

          this.setState({showButton:false})
        }else{

            this.setState({ fetching: newProps.fetching,showButton:true});
            console.log('inside success',newProps);



        }

   }

   reprtChallenge (challengeInfo)  {
   // console.log('challengeData',challengeInfo);
    const {name, phone_no, mail,reason,subject,description,challenge_link,userFullname} = this.state
    //alert('test step1');
    if(this.state.userFullname==''){
      alert('Please enter your name');
    }
    else if(this.state.mail==''){
      alert('Please enter your email');
    }
    else if(this.state.reason==''){
      alert('Please select a reason');
    }
     else if(this.state.reason==''){
      alert('Please select a reason');
    }
    else if(this.state.challenge_link==''){
      alert('Please provide challenge link');
    }
    else if(!this.state.terms){
      alert('Please check our terms of use');
    }
    else{
      let mailTo  ="mainmonkey@monkeychallenges.com";

     this.props.reportChallenge(userFullname, phone_no, mail,reason,subject,description,challenge_link,mailTo);

   // NavigationActions.stepFour(challengeData);
    }
  }

  inputFocused(ref) {
   this._scroll(ref, 150);
 }

 inputBlurred(ref) {
   this._scroll(ref, 0);
 }

 _scroll(ref, offset) {
   setTimeout(() => {
     var scrollResponder = this.refs.myScrollView.getScrollResponder();
     scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
              findNodeHandle(this.refs[ref]),
                offset,
                true
            );
     });
  }

 checkBoxChanged()
    {
    this.state.terms?this.setState({terms: false}):this.setState({terms: true});
    }

  render() {

      let challengeInfo = this.props.itemData;
      // let {status, _id}  = challengeInfo;
      // let challenge_url=`https://www.monkeychallenges.com/challenges/${status}/${_id}`;
      //this.setState({challenge_link:challenge_url})
      let index = 0;

        const data = [
            { key: index++, label: 'This challenge is abusive and offensive to me' },
            { key: index++, label: 'This challenge is in violation of  Terms of Use and Policy' },
            { key: index++, label: 'This challenge is dangerous and life-threatening' }
        ];

    return (
      <ScrollView ref="myScrollView" keyboardDismissMode='interactive' style={[styles.mainContainer,{padding:10}]}>

      { this.state.fetching &&
             <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
           <GiftedSpinner size='large' />
            </View>
            }
      <Text style={{marginLeft:2,fontWeight:'bold'}}>Report a challenge </Text>
      <View style={{marginTop:10}}>
      <Text style={{paddingBottom:3}}>Your Name:</Text>
      <TextInput
        ref="name"
        style={{height: 30, borderColor: 'gray', borderWidth: 1,marginBottom:5}}
        onChangeText={(text) => this.setState({userFullname:text})}
        value={this.state.userFullname}
        onFocus={this.inputFocused.bind(this, 'name')}
        onBlur={this.inputBlurred.bind(this, 'name')}
      />
      </View>
      <View style={{marginTop:10}}>
      <Text style={{paddingBottom:3}}>Your Phone Number:</Text>
      <Text style={{paddingBottom:2,fontSize:12}}>Required in case our team needs to contact you for further details</Text>
      <TextInput
        ref="phone_no"
        style={{height: 35, borderColor: 'gray', borderWidth: 1,marginBottom:5}}
        keyboardType = 'numeric'
        onChangeText={(number) => this.setState({phone_no:number})}
        value={this.state.phone_no}
        onFocus={this.inputFocused.bind(this, 'phone_no')}
        onBlur={this.inputBlurred.bind(this, 'phone_no')}
        placeholder="1 123-123-1234"
      />
      </View>
      <View style={{marginTop:10}}>
      <Text style={{paddingBottom:3}}>Your Email:</Text>
      <Text style={{paddingBottom:2,fontSize:12}}>Please enter the email address associated with your Monkey Challenge account</Text>
      <TextInput
        ref="mail"
        style={{height: 35, borderColor: 'gray', borderWidth: 1,marginBottom:5}}
        onChangeText={(text) => this.setState({mail:text})}
        value={this.state.mail}
        onFocus={this.inputFocused.bind(this, 'mail')}
        onBlur={this.inputBlurred.bind(this, 'mail')}
        placeholder="enter your email"
      />
      </View>

      <View style={{flex:1,}}>
      <Text style={{marginTop:15}}>Report Reason:</Text>
        { /* Default mode: a clickable button will re rendered */ }

          <ModalPicker
                    // data={this.state.payload.cat_data}
                    data={data}
                    style={{ flex: 1 }}
                    initValue="Select a reason to report"
                    onChange={(option)=>{
                      this.setState({reason:option.key})
                      console.log(option)


                    }}>

            </ModalPicker>


      </View>
       <View style={{marginTop:10}}>
      <Text style={{paddingBottom:3}}>Subject:</Text>
      <Text style={{paddingBottom:2,fontSize:12}}>Please use a few words to summarize your question</Text>
      <TextInput
        ref="subject"
        style={{height: 35, borderColor: 'gray', borderWidth: 1,marginBottom:5}}
        onChangeText={(text) => this.setState({subject:text})}
        value={this.state.subject}
        onFocus={this.inputFocused.bind(this, 'subject')}
        onBlur={this.inputBlurred.bind(this, 'subject')}
        placeholder="Prohibited Content"
      />
      </View>
      <View style={{marginTop:10}}>
      <Text style={{paddingBottom:3}}>Description:</Text>
      <Text style={{paddingBottom:2,fontSize:12}}>`Please enter any additional details to help us understand your request.Please include as many specific details possible such as name of Challenge,Challenge description,Who's involved,Judging criteria and link`</Text>
      <TextInput
        ref="description"
        style={{height: 80, borderColor: 'gray', borderWidth: 1,marginBottom:5}}
        multiline={true}
        onChangeText={(text) => this.setState({description:text})}
        value={this.state.description}
        onFocus={this.inputFocused.bind(this, 'description')}
        onBlur={this.inputBlurred.bind(this, 'description')}
        placeholder="Description"
      />
      </View>
      <View style={{marginTop:10}}>
      <Text style={{paddingBottom:3}}>Link to Campaign:</Text>
      <TextInput
        ref="challenge_link"
        style={{height: 40, borderColor: 'gray', borderWidth: 1,marginBottom:15}}
        multiline={true}
        editable={false}
        onChangeText={(text) => this.setState({challenge_link:text})}
        value={this.state.challenge_link}
        onFocus={this.inputFocused.bind(this, 'challenge_link')}
        onBlur={this.inputBlurred.bind(this, 'challenge_link')}
        // placeholder="If you win the challenge, what are you going to do with the winnings? Your funders would like to know why you're doing the challenge and what you're going to do with the money once you win! This will help raise your funds so be descriptive!"
      />
      </View>
      <View style={{marginTop:20,marginBottom:20,flexDirection: 'row',}}>
      <View>
         <CheckBox
         checkboxStyle={{height:20,width:20,margin:0,padding:0}}
         label=''
         checked={this.state.terms}
         onChange={this.checkBoxChanged.bind(this)}
         />
         </View>
   <View style={{marginLeft:-10}}>
         <Text style={{fontSize:11,flexWrap: 'wrap'}} >I have read through the <Text  style={{color:'#ff6633',textDecorationLine:'underline'}} onPress={NavigationActions.privacypolicy}>Privacy policy</Text> and <Text  style={{color:'#ff6633',textDecorationLine:'underline'}} onPress={NavigationActions.terms}>Terms of service</Text> and i would still like to report this challenge
         </Text>
    </View>
         <View>


         </View>
         </View>
         {this.state.showButton &&
          <RoundedButton onPress={this.reprtChallenge.bind(this,challengeInfo)}>
             Report this Challenge!
          </RoundedButton>
        }
        {!this.state.showButton &&
          <View style={{justifyContent:'center',alignItems:'center',marginBottom:20}}>
          <Text  style={{color:'#ff6633',fontWeight:'bold'}}>Reported successfully, please go back to challenge using back button in header.</Text>
          </View>

        }

        <Toast
            ref="toast"
            style={{backgroundColor:'#f9642f'}}
            position='bottom'
            positionValue={100}
            fadeInDuration={600}
            fadeOutDuration={1000}
            opacity={1}
            textStyle={{color:'white'}}
        />

      </ScrollView>
    );
  }
}

const mapStateToProps = (state,props) => {
  console.log("res got!!");
  console.log(state);
      return {

        fetching: state.reportChallenge.fetching,
        payload: state.reportChallenge.payload,
        error: state.reportChallenge.error,
        success:state.reportChallenge.success
        }

}

const mapDispatchToProps = (dispatch) => {
  console.log("api called!!");

  return {
  reportChallenge:(name, phone_no, mail,reason,subject,description,challenge_link,mailTo)=> dispatch(ReportChallengeActions.reportChallengeRequest(name, phone_no, mail,reason,subject,description,challenge_link,mailTo))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReportChallenge)
