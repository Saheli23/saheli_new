// @flow

import React from 'react'
import {
    ScrollView,
    Text,
    KeyboardAvoidingView,
    Image,
    ListView,
    FlatList,
    StyleSheet,
    View,
    TouchableOpacity,
    RefreshControl,
    Dimensions,
    AsyncStorage,
    Platform,
 } from 'react-native'

   const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)


import OpenChallengesActions from '../Redux/OpenChallengesRedux'


import { Metrics, Images, Colors } from '../Themes'

// external libs
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions as NavigationActions } from 'react-native-router-flux'
import WebViewBridge from 'react-native-webview-bridge';
//import Animatable from 'react-native-animatable'

// Styles
//import styles from './Styles/OpenChallengesViewStyle'
//import styles from './Styles/JudgingChallengesViewStyle'
import styles from './Styles/JudgingChallengesViewStyle'

// alert message
import AlertMessage from '../Components/AlertMessage'

// I18n
import I18n from 'react-native-i18n'

// progressBar
import * as Progress from 'react-native-progress'

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'

import Toast, {DURATION} from 'react-native-easy-toast'
import * as _ from 'lodash';
import ProgressiveImage from 'react-native-progressive-image'


type ChallengeListProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,
    getTrending: () => void,
    getOpenChallenges:() => void,
}
class ChallengeList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      defaultfetching:false,
      refreshing: false,
      isLoadingTail: false,
      loaded: false,
      // dataSource: new ListView.DataSource({
      //         rowHasChanged: (row1, row2) => row1 !== row2,
      //       }),
      dataSource:[],
      extraData:[],
      category:[],
    }

  }
  props:ChallengeListProps
  componentWillReceiveProps (newProps) {
    console.log(newProps);

    console.log('new data--------',newProps.payload.data);

    if(newProps.error){
      alert("Error: Something went wrong!");
      this.setState({
        defaultfetching:newProps.defaultfetching,
        refreshing:newProps.refreshing,
        fetching:newProps.fetching,
        isLoadingTail:newProps.isLoadingTail,
        payload:newProps.payload,
        //cat:newProps.cat,
        // dataSource: this.state.dataSource.cloneWithRows(newProps.payload.data)
        dataSource: newProps.payload.data
      })
    }else{
      this.setState({
        defaultfetching:newProps.defaultfetching,
        refreshing:newProps.refreshing,
        fetching:newProps.fetching,
        isLoadingTail:newProps.isLoadingTail,
        payload:newProps.payload,
        //cat:newProps.cat,
        // dataSource: this.state.dataSource.cloneWithRows(newProps.payload.data)
        dataSource: newProps.payload.data
      })
    }
  }
getCategory(){

}
  componentWillMount () {
    this.getMYAccessToken()
    this.getMYuuid();
    this.getChallengeCategoryList();
  //  this.getCategory();
  //  this.props[this.props.requestType]('category',null)
  //  this.props.getOpenChallenges('category',null);
    console.log("test will mount");
    console.log(this.props);
    this.props[this.props.requestType]()
  }
  getMYAccessToken() {

    AsyncStorage.getItem('MCAccessToken').then((value) =>{
      this.setState({MCAccessToken:value});
    })
    AsyncStorage.getItem('MCRefreshToken').then((value) =>{
      this.setState({MCRefreshToken:value});
    })

   }
  getMYuuid() {
      AsyncStorage.getItem('MCUserObj').then((value) =>{
        let userObj = JSON.parse(value)
        this.setState({user:userObj});
      })
   }
   getChallengeCategoryList() {
       AsyncStorage.getItem('MCChallengeCategory').then((value) =>{
         let categoryObj = JSON.parse(value)
         this.setState({category:categoryObj});
       })
    }
  _onRefresh() {
      this.setState({refreshing: true});
      this.props.getOpenChallenges('refreshing',1)
  }
  _keyExtractor = (item, index) => item._id;

  renderRow = (rowData, d, f) => {

  //  console.log(this.state);
  //if( f < this.state.dataSource.getRowCount() ){
    let {user:{uuid}} = this.state;
    //console.log(cat);
    let isFavourite;
    let icon = (rowData.item.challenge_media[0].path == undefined) ? "../Images/monkey_default.png" : rowData.item.challenge_media[0].path;
    //console.log(rowData.item.challenge_category_type_id);

    // let getCategoryObj = cat.filter((item)=> {
    //   if(rowData.challenge_category_type_id == item.id){
    //     //console.log(item);
    //     return item
    //   }
    // });
    let getCategoryStore = _.result(_.find(this.state.category, function(obj) {
      //console.log(obj);
          return ( rowData.item.challenge_category_type_id == obj.id );
      }), 'name');

    // let getCategory = _.result(_.find(cat, function(obj) {
    //   console.log(obj);
    //       return ( rowData.challenge_category_type_id == obj.id );
    //   }), 'name');


    if (rowData.item['challenge_favorite'].length > 0){
    let favourite = _.result(_.find(rowData.item['challenge_favorite'], function(obj) {
          return ( obj['uuid'] == uuid );
      }), '_id');

      // console.log(favourite);
       isFavourite = favourite ? true : false;
      //console.log(isFavourite);
      //console.log("row data length is grater",rowData);
    //  console.log(rowData.item['challenge_favorite'].length);
    }

    return (
      <View style={styles.row}>

      <View style={styles.imageSection}>
        <TouchableOpacity onPress={()=>{NavigationActions.OpenChallengeDetailView({item:rowData.item, getOpenChallenges: mapDispatchToProps,userId:uuid})}}>
        {/*  <Image style={styles.cardImage} source={{uri: icon}} defaultSource={require('../Images/monkey_default.png')} />*/}
            <ProgressiveImage
              thumbnailSource={require('../Images/monkey_default.png')}
              imageSource={{uri:icon}}
              style={{  height: (width/2),
              width:(width), }}
            />
        </ TouchableOpacity>

        <View style={{position:'absolute',backgroundColor:'black',padding:3,flexDirection:'row',top:10,left:5,borderRadius:5}}>
        <Text style={{color:'#FFFFFF',fontWeight:'bold',fontSize:10}}>{getCategoryStore}</Text>
        </View>

{/*
        <TouchableOpacity style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
        <Icon name='heart-o' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
        </TouchableOpacity>
*/}

{isFavourite &&
  <TouchableOpacity onPress={()=>{
    this.props.getOpenChallenges("UnFavourite",null,rowData.item._id);
  }} style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
  <Icon name='heart' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
  </TouchableOpacity>
}

{!isFavourite &&
  <TouchableOpacity onPress={()=>{
    this.props.getOpenChallenges("Favourite",null,rowData.item._id);
  }}
   style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
  <Icon name='heart-o' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
  </TouchableOpacity>
}
           <View style={styles.imageFooter}>
            <View style={{alignItems: 'center', justifyContent:'center'}}>
             <Progress.Bar progress={(Math.round(rowData.item.percentage_funded) / 100)} color={'#33CC32'} height={8} width={(width - 20)} borderWidth={0} unfilledColor={'rgba(128,132,131,0.8)'}/>

              <View style={[styles.contentVoteSection]}>
                <Text style={{color: 'white',fontWeight:'bold' }}>${parseInt(rowData.item.total_amount_funded)}<Text style={{fontSize:10}}> OF ${parseInt(rowData.item.price)} FUNDED</Text></ Text>
                {/*
                <Text style={{color: 'white'}}>{Math.round(rowData.percentage_funded)}%</Text>
                */}
                <Text style={{color: 'white',fontWeight:'bold', textAlign: 'right'}}>{rowData.item.funding_days_remaining} <Text style={{fontSize:10}}>DAYS LEFT</Text></Text>
              </View>
          </View>
        </View>
         <View style={styles.imageFooterOpacity} />
      </View>
      <View style={styles.contentSection}>

      <View style={[styles.contentDescriptionSection,{flexDirection:'column',width:(width - 20),alignSelf:'center'}]}>
    {/*
           <Text style={{color: '#1266AF',fontWeight:'bold',}}>{rowData.handle_name} :
            <Text style={{color: '#000000'}}> {rowData.name}</Text></Text>
            <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: rowData.uuid })}}>
    */}

    <View style={{flexDirection:'row',width:(width - 20),}}>
      <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: rowData.item.uuid })}}>
          <Text style={{color: '#1266AF',fontWeight:'bold',flexWrap: 'wrap'}}>{rowData.item.handle_name} :
          <Text numberOfLines={2} style={{color: '#000000',fontWeight:'bold',}}> {rowData.item.name}</Text></Text>
      </TouchableOpacity>
    </View>
          <Text style={{fontSize:12}} numberOfLines={2}>{rowData.item.description}</Text>
      </View>

      </View>
      </View>
    )
  // }
  // else {
  //   return null;
  // }
  }


  noRowData () {
    return this.state.dataSource.length === 0
  }

  onEndReached = () => {
    let { isLoadingTail, payload:{meta:{pagination:{count,current_page,per_page,total,total_pages}}} } = this.state
    // console.log('on End fired!');
    // console.log('onEndReached', isLoadingTail);
    // console.log('count:',count,'current_page:',current_page,'per_page:',per_page,'total:',total,'total_pages:',total_pages);
       if (isLoadingTail) {
           return;
       }
      if(total_pages > current_page){
        this.setState({
            isLoadingTail: true,
        });
        this.props.getOpenChallenges('pagination', ++current_page);
      }else{
        console.log("totalpages",total_pages,"current",current_page);

        this.setState({
            isLoadingTail: false,
        });
        this.refs.toast.show('No More Data!',DURATION.LENGTH_SHORT);
      }
  }

  render () {
    let {MCAccessToken,MCRefreshToken,MCTokenExpires} = this.state;
    var now = new Date();
    now.setTime(now.getTime() + 6 * 3600 * 1000);
    let  UserTokenExpires = ";expires="+now.toUTCString();

    //document.cookie = "name=value; expires=" + now.toUTCString() + "; path=/";
    let jsCode = `
           document.cookie = 'userToken=` + MCAccessToken + UserTokenExpires+`';
           document.cookie = 'refreshToken=` + MCRefreshToken + UserTokenExpires+`';
           `;

       //console.log('UserTokenExpires',3600/(60*60*24));
       //let  RefreshTokenExpires = "expires="+MCTokenExpires;
      return <View style={styles.challengeListContainer}>
      <View style={{height:0.5, width:0.5}}>
        <WebViewBridge
          ref="webviewbridge"
          source={{uri: webViewBaseLink+`/setappcookie.html`}}
          injectedJavaScript={jsCode}
          />
      </View>
      { this.state.defaultfetching && Platform.OS != 'android' &&
       <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
     <GiftedSpinner size='large' />
      </View>
      }

      { !this.state.fetching && this.state.dataSource.length === 0 &&
         <AlertMessage title='Nothing to See Here, Move Along' show={this.noRowData()} />
      }
      { this.state.fetching &&
        <GiftedSpinner />
      }
      { this.state.dataSource.length > 0 &&
            <FlatList
            // refreshControl={
            //         <RefreshControl
            //           refreshing={this.state.refreshing}
            //           onRefresh={this._onRefresh.bind(this)}
            //         />
            //       }
              onEndReachedThreshold={0.5}
              contentContainerStyle={styles.listContent}
              data={this.state.dataSource}
              // extraData={this.state.extraData}
              renderItem={this.renderRow}
              keyExtractor={this._keyExtractor}
              onEndReached={()=>{
                let { isLoadingTail, payload:{meta:{pagination:{count,current_page,per_page,total,total_pages}}} } = this.state
                // console.log('on End fired!');
                // console.log('onEndReached', isLoadingTail);
                // console.log('count:',count,'current_page:',current_page,'per_page:',per_page,'total:',total,'total_pages:',total_pages);
                   if (isLoadingTail) {
                       return;
                   }
                  if(total_pages > current_page){

                    console.log("totalpages",total_pages,"current",current_page);
                    this.setState({
                        isLoadingTail: true,
                    });
                    this.props.getOpenChallenges('pagination', ++current_page);
                  }else{
                    console.log("totalpages",total_pages,"current",current_page);

                    this.setState({
                        isLoadingTail: false,
                    });
                    this.refs.toast.show('No More Data!',DURATION.LENGTH_SHORT);
                  }
              }}
            />


      }
      { this.state.isLoadingTail &&
        <GiftedSpinner />
      }

           <Toast
               ref="toast"
               style={{backgroundColor:'#f9642f'}}
               position='bottom'
               positionValue={250}
               fadeInDuration={750}
               fadeOutDuration={700}
               opacity={0.9}
               textStyle={{color:'white'}}
           />
      </View>

  }
}
 // /
class OpenChallengesView extends React.Component {
  render () {
    return (
       <View style={styles.mainDoubleNavContainer}>
          <ConnectedChallengeList stateName='openChallenges' requestType='getOpenChallenges' title='Open Challenges' />
         </View>
    )
  }
}
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  console.log(stateProps);
  console.log(dispatchProps);
  console.log(ownProps);
  return Object.assign({}, ownProps, stateProps, dispatchProps)

}

const mapStateToProps = (state, props) => {
  console.log(props);
  console.log(state);
  return {
    defaultfetching:state[props.stateName].defaultfetching,
    refreshing:state[props.stateName].refreshing,
    isLoadingTail:state[props.stateName].isLoadingTail,
    fetching: state[props.stateName].fetching,
    payload: state[props.stateName].payload,
    error: state[props.stateName].error,
    //cat: state[props.stateName].cat
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getOpenChallenges: (default_type = 'default', page = 1, challengeId = null) =>{
      console.log(default_type, page, challengeId);
      return dispatch(OpenChallengesActions.openChallengesRequest(default_type, page, challengeId))
    }

  }
}


const ConnectedChallengeList = connect(mapStateToProps, mapDispatchToProps, mergeProps)(ChallengeList)
export default OpenChallengesView
