// @flow

import React from 'react'
import {
    View,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    Keyboard,
    LayoutAnimation,
    WebView,
    Linking
} from 'react-native'
import {connect} from 'react-redux'
import Styles from './Styles/LoginScreenStyle'
import {Images, Metrics} from '../Themes'
import LoginActions from '../Redux/LoginRedux'
import FbLoginActions from '../Redux/FbLoginRedux'
import {Actions as NavigationActions} from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import WebViewBridge from 'react-native-webview-bridge';

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'
var BASEURL="www.monkeychallenges.com/#/"
type SignupScreenProps = {
    dispatch: () => any,
    fetching: boolean,
    loginType:string,
    access_token:string,
    attemptLogin: () => void,

}
class CreateChallenge extends React.Component {

  componentDidMount() {
    let appData = {data: 'FromApp'};

    // Send this chart data over to web view after 5 seconds.
    setTimeout(() => {
      this.refs.webviewbridge.sendToBridge(JSON.stringify(appData));
    }, 5000);
  }


	render() {
		return <WebViewBridge
        ref="webviewbridge"
        onNavigationStateChange={this._onLoad}
        onBridgeMessage={this.onBridgeMessage.bind(this)}
        startInLoadingState={true}
        source={{uri: "https://dev-1.monkeychallenges.com/#/login;troute=L2NoYWxsZW5nZS9jcmVhdGUvc3RlcC9hcHBzZWxlY3Q%3D"}}/>;

	}
  _onLoad(state) {
    console.log(state.url);
    console.log("indexof",state.url.indexOf(BASEURL + 'challenge/create/step/descriptionpreview/'));
    if (state.url.indexOf(BASEURL + 'challenge/create/step/descriptionpreview/') != -1) {
      let appData = {data: 'App'};

       this.refs.webviewbridge.sendToBridge(JSON.stringify(appData));
    }
  }

	onBridgeMessage (webViewData) {
			  let jsonData = JSON.parse(webViewData);
        if(jsonData.back){
        	NavigationActions.postLogin();

        }

	  }


}


const mapStateToProps = (state) => {
  console.log(state)
  return {

        }



}

const mapDispatchToProps = (dispatch) => {
  return {


  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateChallenge)
