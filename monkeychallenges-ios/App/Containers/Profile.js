// @flow

import React from 'react'
import {
    View,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    Keyboard,
    LayoutAnimation,
    WebView,
    Linking,AsyncStorage
} from 'react-native'
import {connect} from 'react-redux'
import styles from './Styles/WebLinksScreenStyle'
import {Images, Metrics} from '../Themes'
import LoginActions from '../Redux/LoginRedux'
import FbLoginActions from '../Redux/FbLoginRedux'
import {Actions as NavigationActions} from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import WebViewBridge from 'react-native-webview-bridge';

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'
import { StyleSheet } from 'react-native'

class Profile extends React.Component {
	constructor (props) {
    super(props)

     this.state = { canGoBack: false,MYuuid:"" };
  }
//   onBack() {
//    NavigationActions.postLogin();
//   //  NavigationActions.pop();
//   //this.refs[webviewbridge].goBack();
//
// }
componentWillMount(){
  this.getMYuuid()

}
getMYuuid() {
  AsyncStorage.getItem('MYuuid').then((value) =>{
    this.setState({MYuuid:value});
  })
 }
	render() {
    let {MYuuid} = this.state
		return (
     <View style={styles.mainContainer}>
     {/*
     <TouchableOpacity

            onPress={this.onBack.bind(this)}
            >
        <View style={styles.topbar}>

            <Text  style={this.state.canGoBack ? styles.topbarText : styles.topbarTextDisabled}>Go Back To App</Text>
profile
        </View>
        </TouchableOpacity>
        */}
      <WebViewBridge
        ref="webviewbridge"
        onNavigationStateChange={this._onLoad}
        onNavigationStateChange={this.onNavigationStateChange.bind(this)}
        onBridgeMessage={this.onBridgeMessage.bind(this)}
        startInLoadingState={true}
        source={{uri: `https://dev-1.monkeychallenges.com/#/myaccount/${MYuuid}/profile`}}/>
	     </View>);}


onNavigationStateChange(navState) {
    this.setState({
      canGoBack: navState.canGoBack
    });
  }

	onBridgeMessage (webViewData) {
			// console.log(webViewData);
	  //   let jsonData = JSON.parse(webViewData);
   //      if(jsonData.signupsuccess){
   //      	console.log('username', jsonData.username);
   //      	console.log('password', jsonData.password);
   //      	this.username =jsonData.username;
   //      	this.password =jsonData.password;

   //      }
	  //   if (jsonData.success) {
	  //    // Alert.alert(jsonData.message);
	  //     console.log('data received', jsonData.message);
	  //    // NavigationActions.login();
	  //     this.isAttempting = true;
	  //     this.props.attemptLogin(this.username, this.password, '', 'login')
	  //   }
	  //   console.log('data received', webViewData, jsonData);
	    //.. do some react native stuff when data is received
	  }


}


const mapStateToProps = (state) => {
  console.log(state)
  return {
          // fetching: state.login.fetching,
          // access_token: state.login.access_token
        }



}

const mapDispatchToProps = (dispatch) => {
  return {
   // attemptLogin: (username, password, facebook_token, loginType) => dispatch(LoginActions.loginRequest(username, password, facebook_token, loginType))
    //attemptFbLogin: (facebook_token) => dispatch(FbLoginActions.fbLoginRequest(facebook_token))

  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Profile)
