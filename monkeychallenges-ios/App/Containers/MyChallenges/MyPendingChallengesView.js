// @flow

import React from 'react'
import {
    AsyncStorage,
    ScrollView,
    Text,
    KeyboardAvoidingView,
    Image,
    ListView,
    FlatList,
    StyleSheet,
    View,
    TouchableOpacity,
    RefreshControl,
    Dimensions
   } from 'react-native'
   const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)

//import OpenChallengesActions from '../../Redux/OpenChallengesRedux'
import MyPendingChallengesActions from '../../Redux/MyPendingChallengesRedux'
//import PendingChallengesActions from '../../Redux/PendingChallengesRedux'

import { Metrics, Images } from '../../Themes'

// external libs
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions as NavigationActions } from 'react-native-router-flux'
import Animatable from 'react-native-animatable'

// Styles
//import styles from './Styles/MyOpenChallengesViewStyle'
//import styles from './Styles/JudgingChallengesViewStyle'
import styles from '../Styles/JudgingChallengesViewStyle'

// alert message
import AlertMessage from '../../Components/AlertMessage'

// I18n
import I18n from 'react-native-i18n'

// progressBar
import * as Progress from 'react-native-progress'

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'

import Toast, {DURATION} from 'react-native-easy-toast'
import * as _ from 'lodash';
import ProgressiveImage from 'react-native-progressive-image'

type ChallengeListProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,
    getPendingChallenges: () => void
}


class ChallengeList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      refreshing: false,
      isLoadingTail: false,
      loaded: false,
      // dataSource: new ListView.DataSource({
      //         rowHasChanged: (row1, row2) => row1 !== row2,
      //       }),
      dataSource:[],
      extraData:[],
      category:[],
    }
  }
  props:ChallengeListProps
  componentWillReceiveProps (newProps) {
    console.log(newProps);
     this.setState({
       refreshing:newProps.refreshing,
       fetching:newProps.fetching,
       isLoadingTail:newProps.isLoadingTail,
       payload:newProps.payload,
       // dataSource: this.state.dataSource.cloneWithRows(newProps.payload.data)
       dataSource: newProps.payload.data
     })
  }

  componentWillMount () {
    this.getChallengeCategoryList();
    console.log("test will mount");
    console.log(this.props);
      this.props[this.props.requestType]()
  }
  getChallengeCategoryList() {

      AsyncStorage.getItem('MCChallengeCategory').then((value) =>{
        let categoryObj = JSON.parse(value);
        console.log(categoryObj);
        this.setState({category:categoryObj});
      })

   }
  _onRefresh() {
      this.setState({refreshing: true});
      this.props.getPendingChallenges('refreshing',1)
  }
    _keyExtractor = (item, index) => item._id;

  // handleChallengesView = (itemData)=>{
  // NavigationActions.OpenChallengeDetailView({item:itemData})
  // }

  // componentDidReceiveProps (newProps){
  //    console.log(newProps);
  // }

  renderRow = (rowData) => {

let icon = (rowData.item.challenge_media[0].path == undefined) ? "../../Images/monkey_default.png" : rowData.item.challenge_media[0].path;

//console.log("test state data: ",this.state);

    let getCategory = _.result(_.find(this.state.category, function(obj) {
      console.log(obj);
          return ( rowData.item.challenge_category_type_id == obj.id );
      }), 'name');
      console.log(getCategory);
    return (
      <View style={styles.row}>
      <View style={styles.imageSection}>



          <TouchableOpacity onPress={()=>{NavigationActions.ChallengeDetailView({item:rowData.item})}}>

          {/*  <Image style={styles.cardImage} source={{uri: icon}} defaultSource={require('../Images/monkey_default.png')} />*/}
              <ProgressiveImage
                thumbnailSource={require('../../Images/monkey_default.png')}
                imageSource={{uri:icon}}
                style={{  height: (width/2),
                width:(width), }}
              />

        </ TouchableOpacity>


          <View style={{position:'absolute',backgroundColor:'black',padding:3,flexDirection:'row',top:10,left:5,borderRadius:5}}>
          <Text style={{color:'#FFFFFF',fontWeight:'bold',fontSize:10}}>{getCategory}</Text>
          </View>


      {/* {(rowData.charity == 1) &&
          <View style={{position:'absolute',backgroundColor:'black',padding:3,flexDirection:'row',top:10,left:5,borderRadius:5}}>
          <Text style={{color:'#FFFFFF',fontWeight:'bold',fontSize:10}}>CHARITY</Text>
          </View>
          }
          */}
        <View style={styles.imageFooter}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <View style={styles.contentVoteSection}>
          <Text style={{color: 'white',fontWeight:'bold', fontSize:30 }}>${parseInt(rowData.item.price)}<Text style={{fontSize:10}}> FUNDED</Text></ Text>
          {/*
          <Text style={{color: 'white'}}>{Math.round(rowData.percentage_funded)}%</Text>
          */}
            <View style={{justifyContent:'flex-end'}}>
              <Text style={{color: 'white', textAlign: 'right', fontWeight:'bold',fontFamily:'Bradley Hand'}}>Getting ready!</Text>
            </View>

          </View>
          </View>
        </View>

        <View style={styles.imageFooterOpacity} />
      </View>

      <View style={styles.contentSection}>

      <View style={[styles.contentDescriptionSection,{flexDirection:'column',width:(width - 20),alignSelf:'center'}]}>
    {/*
           <Text style={{color: '#1266AF',fontWeight:'bold',}}>{rowData.handle_name} :
            <Text style={{color: '#000000'}}> {rowData.name}</Text></Text>
            <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: rowData.uuid })}}>
    */}

    <View style={{flexDirection:'row',width:(width - 20),}}>
      <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: rowData.item.uuid })}}>
          <Text style={{color: '#1266AF',fontWeight:'bold',flexWrap: 'wrap'}}>{rowData.item.handle_name} :
          <Text numberOfLines={2} style={{color: '#000000',fontWeight:'bold',}}> {rowData.item.name}</Text></Text>
      </TouchableOpacity>
    </View>
          <Text style={{fontSize:12}} numberOfLines={2}>{rowData.item.description}</Text>
      </View>

      </View>
      </View>
    )
  }


  noRowData () {
    return this.state.dataSource.length === 0
  }

  onEndReached = () => {
    let { isLoadingTail, payload:{meta:{pagination:{count,current_page,per_page,total,total_pages}}} } = this.state
    // console.log('on End fired!');
    // console.log('onEndReached', isLoadingTail);
    // console.log('count:',count,'current_page:',current_page,'per_page:',per_page,'total:',total,'total_pages:',total_pages);
       if (isLoadingTail) {
           // We're already fetching
           return;
       }
      //  this.setState({
      //      isLoadingTail: true
      //  });

      if(total_pages > current_page){
        this.setState({
            isLoadingTail: true,
        });
        this.props.getPendingChallenges('pagination', ++current_page);
      }else{
        this.setState({
            isLoadingTail: false
        });
          this.refs.toast.show('No More Data!',DURATION.LENGTH_SHORT);
      }
      //console.log('payload', payload);
  }

  render () {
      //console.log(this.props);
      // let payload = this.state && this.state.payload || {data: []}
      //console.log(payload.data, 'playload.data', this.props.title)
      return <View style={styles.challengeListContainer}>
      { !this.state.fetching && this.state.dataSource.length === 0 &&
         <AlertMessage title='Nothing to See Here, Move Along' show={this.noRowData()} />
      }
      { this.state.fetching &&
        <GiftedSpinner />
      }
      { this.state.dataSource.length > 0 &&
            <FlatList
            // refreshControl={
            //         <RefreshControl
            //           refreshing={this.state.refreshing}
            //           onRefresh={this._onRefresh.bind(this)}
            //         />
            //       }
            onEndReachedThreshold={0.5}
            contentContainerStyle={styles.listContent}
            data={this.state.dataSource}
            // extraData={this.state.extraData}
            renderItem={this.renderRow}
            keyExtractor={this._keyExtractor}
              onEndReached={this.onEndReached}
            />
      }
      { this.state.isLoadingTail &&
        <GiftedSpinner />
      }
      <Toast
          ref="toast"
          style={{backgroundColor:'#f9642f'}}
          position='bottom'
          positionValue={250}
          fadeInDuration={750}
          fadeOutDuration={700}
          opacity={0.9}
          textStyle={{color:'white'}}
      />
      </View>

  }
}

class MyPendingChallengesView extends React.Component {

  render () {
    return (
       <View style={styles.mainDoubleNavContainer}>
          <ConnectedChallengeList stateName='myPendingChallenges' requestType='getPendingChallenges' title='Pending Challenges' />
      </View>
    )
  }

}

const mapStateToProps = (state, props) => {
  return {
    refreshing:state[props.stateName].refreshing,
    isLoadingTail:state[props.stateName].isLoadingTail,
    fetching: state[props.stateName].fetching,
    payload: state[props.stateName].payload
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getPendingChallenges: (default_type = 'default', page = 1) => dispatch(MyPendingChallengesActions.myPendingChallengesRequest(default_type, page))

  }
}


const ConnectedChallengeList = connect(mapStateToProps, mapDispatchToProps)(ChallengeList)
export default MyPendingChallengesView
