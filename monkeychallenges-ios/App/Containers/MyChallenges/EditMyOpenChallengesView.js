// @flow

import React from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Image, ListView, View, TouchableOpacity,Dimensions,AsyncStorage } from 'react-native'
const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)

//import OpenChallengesActions from '../../Redux/OpenChallengesRedux'
import MyOpenChallengesActions from '../../Redux/MyOpenChallengesRedux'

import { Metrics, Images } from '../../Themes'

// external libs
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions as NavigationActions } from 'react-native-router-flux'
import Animatable from 'react-native-animatable'

// Styles
//import styles from './Styles/MyOpenChallengesViewStyle'
//import styles from './Styles/JudgingChallengesViewStyle'
import styles from '../Styles/JudgingChallengesViewStyle'
import styles1 from '../Styles/PostLoginStyle'
// alert message
import AlertMessage from '../../Components/AlertMessage'

// I18n
import I18n from 'react-native-i18n'

// progressBar
import * as Progress from 'react-native-progress'

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'
import Toast, {DURATION} from 'react-native-easy-toast'

type ChallengeListProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,
    getOpenChallenges: () => void
}


class ChallengeList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoadingTail: false,
      loaded: false,
      dataSource: new ListView.DataSource({
              rowHasChanged: (row1, row2) => row1 !== row2,
            }),
      user:{}
    }
  }
  props:ChallengeListProps
  componentWillReceiveProps (newProps) {
    console.log(newProps);
    // if (!newProps.fetching) {
    //   this.setState({fetching:newProps.fetching, payload: newProps.payload})
    // }else{
    //    this.setState({fetching:newProps.fetching, payload: newProps.payload})
    // }
     //this.setState({fetching:newProps.fetching, payload: newProps.payload})
     this.setState({
       fetching:newProps.fetching,
       isLoadingTail:newProps.isLoadingTail,
       payload:newProps.payload,
       dataSource: this.state.dataSource.cloneWithRows(newProps.payload.data)
     })
  }


  // handleChallengesView = (itemData)=>{
  // NavigationActions.OpenChallengeDetailView({item:itemData})
  // }

  // componentDidReceiveProps (newProps){
  //    console.log(newProps);
  // }

  renderRow (rowData) {

    let icon = (rowData.challenge_media[0].path == undefined) ? "../../Images/monkey_default.png" : rowData.challenge_media[0].path;
    return (

      <View style={[styles1.challenge]}>
      <View style={styles1.imageSection}>
        <View style={{height: 180 }}>


          <TouchableOpacity onPress={()=>{NavigationActions.OpenChallengeDetailView({item:rowData,getOpenChallenges: mapDispatchToProps,myChallenge:'true' })}}>

          <Image style={{height: 180,backgroundColor: "#cccccc" }} source={{uri:icon}} defaultSource={require('../../Images/monkey_default.png')} />

        </ TouchableOpacity>
        {(rowData.charity == 1) &&
          <View style={{position:'absolute',backgroundColor:'black',padding:3,flexDirection:'row',top:10,left:5,borderRadius:5}}>
          <Text style={{color:'#FFFFFF',fontWeight:'bold',fontSize:10}}>CHARITY</Text>
          </View>
          }
        </View>

        <View style={styles1.imageFooter}>

          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Progress.Bar progress={(Math.round(rowData.percentage_funded) / 100)} color={'#33CC32'} height={8} width={(300/1.2)} borderWidth={0} unfilledColor={'rgba(128,132,131,0.8)'}/>
          <View style={styles1.contentVoteSection}>
            <Text style={{color: 'white',fontWeight:'bold' }}>${parseInt(rowData.total_amount_funded)}<Text style={{fontSize:10}}> OF ${parseInt(rowData.price)} FUNDED</Text></ Text>

            <Text style={{color: 'white',fontWeight:'bold', textAlign: 'right'}}>{rowData.days_remaining} <Text style={{fontSize:10}}>DAYS LEFT</Text></Text>
          </View>
          </View>

        </View>



        <View style={styles1.imageFooterOpacity} />
      </View>

      <View style={styles1.contentSection}>
        <View style={styles1.contentDescriptionSection}>
        <Text style={{color: '#1266AF',fontWeight:'bold',}}>{rowData.handle_name} : <Text style={{color: '#000000'}}>{rowData.name}</Text></Text>
     <Text style={{fontSize:12}} numberOfLines={3}>{rowData.description}</Text>
        </View>
      </View>
      </View>

    )
  }

  componentWillMount () {
    console.log("test will mount");
    console.log(this.props);
      this.props[this.props.requestType]()
      this.getMYuuid();


  }
  getMYuuid() {
      AsyncStorage.getItem('MCUserObj').then((value) =>{
        let userObj = JSON.parse(value)
        this.setState({user:userObj});
      })
   }
  noRowData () {
    return this.state.dataSource.getRowCount() === 0
  }

  onEndReached = () => {
    let { isLoadingTail, payload:{meta:{pagination:{count,current_page,per_page,total,total_pages}}} } = this.state
    console.log('on End fired!');
    console.log('onEndReached', isLoadingTail);
    console.log('count:',count,'current_page:',current_page,'per_page:',per_page,'total:',total,'total_pages:',total_pages);
       if (isLoadingTail) {
           // We're already fetching
           return;
       }
      //  this.setState({
      //      isLoadingTail: true
      //  });

      if(total_pages > current_page){
        this.props.getOpenChallenges('pagination', ++current_page);
      }else{
        this.setState({
            isLoadingTail: false
        });
          this.refs.toast.show('No More Data!',DURATION.LENGTH_SHORT);
      }
      //console.log('payload', payload);
  }

  render () {
      //console.log(this.props);
      // let payload = this.state && this.state.payload || {data: []}
      //console.log(payload.data, 'playload.data', this.props.title)
      return <View style={styles.challengeListContainer}>
      { !this.state.fetching && this.state.dataSource.getRowCount() === 0 &&
         <AlertMessage title='Nothing to See Here, Move Along' show={this.noRowData()} />
      }
      { this.state.fetching &&
        <GiftedSpinner />
      }
      { this.state.dataSource.getRowCount() > 0 &&
            <ListView
              horizontal={true}
              onEndReachedThreshold={100}
              contentContainerStyle={styles.listContent}
              dataSource={this.state.dataSource}
              renderRow={this.renderRow}
              onEndReached={this.onEndReached}
            />
      }
      { this.state.isLoadingTail &&
        <GiftedSpinner />
      }
      <Toast
          ref="toast"
          style={{backgroundColor:'#f9642f'}}
          position='bottom'
          positionValue={250}
          fadeInDuration={750}
          fadeOutDuration={700}
          opacity={0.9}
          textStyle={{color:'white'}}
      />
      </View>

  }
}

class EditMyOpenChallengesView extends React.Component {

  render () {
    return (
       <View>
          <ConnectedChallengeList stateName='myOpenChallenges' requestType='getOpenChallenges' title='Open Challenges' />
      </View>
    )
  }

}

const mapStateToProps = (state, props) => {
  return {
    isLoadingTail:state[props.stateName].isLoadingTail,
    fetching: state[props.stateName].fetching,
    payload: state[props.stateName].payload
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getOpenChallenges: (default_type = 'default', page = 1) => dispatch(MyOpenChallengesActions.myOpenChallengesRequest(default_type, page))

  }
}


const ConnectedChallengeList = connect(mapStateToProps, mapDispatchToProps)(ChallengeList)
export default EditMyOpenChallengesView
