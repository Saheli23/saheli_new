// @flow

import React from 'react'
import {
    ScrollView,
    Text,
    KeyboardAvoidingView,
    Image,
    ListView,
    StyleSheet,
    View,
    TouchableOpacity,
    RefreshControl,
    Dimensions,
    AsyncStorage
   } from 'react-native'

   const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)


import WinChallengesActions from '../Redux/WinChallengesRedux'


import { Metrics, Images, Colors } from '../Themes'

// external libs
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions as NavigationActions } from 'react-native-router-flux'
//import Animatable from 'react-native-animatable'

// Styles
//import styles from './Styles/OpenChallengesViewStyle'
//import styles from './Styles/JudgingChallengesViewStyle'
import styles from './Styles/JudgingChallengesViewStyle'

// alert message
import AlertMessage from '../Components/AlertMessage'

// I18n
import I18n from 'react-native-i18n'

// progressBar
import * as Progress from 'react-native-progress'

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'

import Toast, {DURATION} from 'react-native-easy-toast'
import * as _ from 'lodash';

type ChallengeListProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,
    getTrending: () => void,
    getWinChallenges:() => void,
}
class ChallengeList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      defaultfetching:false,
      refreshing: false,
      isLoadingTail: false,
      loaded: false,
      dataSource: new ListView.DataSource({
              rowHasChanged: (row1, row2) => row1 !== row2,
            }),
    }

  }
  props:ChallengeListProps
  componentWillReceiveProps (newProps) {
    console.log(newProps);

    if(newProps.error){
      alert("Error: Something went wrong!");
      this.setState({
        defaultfetching:newProps.defaultfetching,
        refreshing:newProps.refreshing,
        fetching:newProps.fetching,
        isLoadingTail:newProps.isLoadingTail,
        payload:newProps.payload,
        dataSource: this.state.dataSource.cloneWithRows(newProps.payload.data)
      })
    }else{
      this.setState({
        defaultfetching:newProps.defaultfetching,
        refreshing:newProps.refreshing,
        fetching:newProps.fetching,
        isLoadingTail:newProps.isLoadingTail,
        payload:newProps.payload,
        dataSource: this.state.dataSource.cloneWithRows(newProps.payload.data)
      })
    }




  }

  componentWillMount () {
    this.getMYuuid();
    console.log("test will mount");
    console.log(this.props);
      this.props[this.props.requestType]()
  }

  getMYuuid() {
      AsyncStorage.getItem('MCUserObj').then((value) =>{
        let userObj = JSON.parse(value)
        this.setState({user:userObj});
      })
   }
  _onRefresh() {
      this.setState({refreshing: true});
      this.props.getWinChallenges('refreshing',1)
  }

  renderRow = (rowData) => {
    let {user:{uuid}} = this.state;
    let isFavourite;
    let icon = (rowData.challenge_media[0].path == undefined) ? "../Images/monkey_default.png" : rowData.challenge_media[0].path;

    if (rowData['challenge_favorite'].length > 0){
    let favourite = _.result(_.find(rowData['challenge_favorite'], function(obj) {
          return ( obj['uuid'] == uuid );
      }), '_id');

       console.log(favourite);
       isFavourite = favourite ? true : false;
      //console.log(isFavourite);

      console.log("row data length is grater",rowData);
      console.log(rowData['challenge_favorite'].length);
    }

    return (
      <View style={styles.row}>

      <View style={styles.imageSection}>
        <TouchableOpacity onPress={()=>{NavigationActions.OpenChallengeDetailView({item:rowData})}}>
          <Image style={styles.cardImage} source={{uri: icon}} defaultSource={require('../Images/monkey_default.png')} />
        </ TouchableOpacity>
       {(rowData.charity == 1) &&
        <View style={{position:'absolute',backgroundColor:'black',padding:3,flexDirection:'row',top:10,left:5,borderRadius:5}}>
        <Text style={{color:'#FFFFFF',fontWeight:'bold',fontSize:10}}>CHARITY</Text>
        </View>
        }
{/*
        <TouchableOpacity style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
        <Icon name='heart-o' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
        </TouchableOpacity>
*/}

{isFavourite &&
  <TouchableOpacity onPress={()=>{
    this.props.getWinChallenges("UnFavourite",null,rowData._id);
  }} style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
  <Icon name='heart' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
  </TouchableOpacity>
}

{!isFavourite &&
  <TouchableOpacity onPress={()=>{
    this.props.getWinChallenges("Favourite",null,rowData._id);
  }}
   style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
  <Icon name='heart-o' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
  </TouchableOpacity>
}

        <View style={styles.imageFooter}>
          <View style={{alignItems: 'center', justifyContent:'center'}}>
           <Progress.Bar progress={(Math.round(rowData.percentage_funded) / 100)} color={'#33CC32'} height={8} width={(width - 20)} borderWidth={0} unfilledColor={'rgba(128,132,131,0.8)'}/>

          <View style={[styles.contentVoteSection]}>
            <Text style={{color: 'white',fontWeight:'bold' }}>${parseInt(rowData.total_amount_funded)}<Text style={{fontSize:10}}> OF ${parseInt(rowData.price)} FUNDED</Text></ Text>
            {/*
            <Text style={{color: 'white'}}>{Math.round(rowData.percentage_funded)}%</Text>
            */}
            <Text style={{color: 'white',fontWeight:'bold', textAlign: 'right'}}>{rowData.days_remaining} <Text style={{fontSize:10}}>DAYS LEFT</Text></Text>
          </View>
        </View>
        </View>
        <View style={styles.imageFooterOpacity} />
      </View>
      <View style={styles.contentSection}>

      <View style={[styles.contentDescriptionSection,{flexDirection:'column',width:(width - 20),alignSelf:'center'}]}>
    {/*
           <Text style={{color: '#1266AF',fontWeight:'bold',}}>{rowData.handle_name} :
            <Text style={{color: '#000000'}}> {rowData.name}</Text></Text>
            <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: rowData.uuid })}}>
    */}

    <View style={{flexDirection:'row',width:(width - 20),}}>
      <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: rowData.uuid })}}>
          <Text style={{color: '#1266AF',fontWeight:'bold',flexWrap: 'wrap'}}>{rowData.handle_name} :
          <Text numberOfLines={2} style={{color: '#000000',fontWeight:'bold',}}> {rowData.name}</Text></Text>
      </TouchableOpacity>
    </View>
          <Text style={{fontSize:12}} numberOfLines={2}>{rowData.description}</Text>
      </View>

      </View>
      </View>
    )
  }


  noRowData () {
    return this.state.dataSource.getRowCount() === 0
  }

  onEndReached = () => {
    let { isLoadingTail, payload:{meta:{pagination:{count,current_page,per_page,total,total_pages}}} } = this.state
    console.log('on End fired!');
    console.log('onEndReached', isLoadingTail);
    console.log('count:',count,'current_page:',current_page,'per_page:',per_page,'total:',total,'total_pages:',total_pages);
       if (isLoadingTail) {
           return;
       }
      if(total_pages > current_page){
        this.setState({
            isLoadingTail: true,
        });
        this.props.getWinChallenges('pagination', ++current_page);
      }else{
        this.setState({
            isLoadingTail: false,
        });
        this.refs.toast.show('No More Data!',DURATION.LENGTH_SHORT);
      }
  }

  render () {
      return <View style={styles.challengeListContainer}>
      { this.state.defaultfetching &&
       <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
     <GiftedSpinner size='large' />
      </View>
      }
      { !this.state.fetching && this.state.dataSource.getRowCount() === 0 &&
         <AlertMessage title='Nothing to See Here, Move Along' show={this.noRowData()} />
      }
      { this.state.fetching &&
        <GiftedSpinner />
      }
      { this.state.dataSource.getRowCount() > 0 &&
            <ListView
            // refreshControl={
            //         <RefreshControl
            //           refreshing={this.state.refreshing}
            //           onRefresh={this._onRefresh.bind(this)}
            //         />
            //       }
              onEndReachedThreshold={100}
              contentContainerStyle={styles.listContent}
              dataSource={this.state.dataSource}
              renderRow={this.renderRow}
              onEndReached={this.onEndReached}
            />


      }
      { this.state.isLoadingTail &&
        <GiftedSpinner />
      }

           <Toast
               ref="toast"
               style={{backgroundColor:'#f9642f'}}
               position='bottom'
               positionValue={250}
               fadeInDuration={750}
               fadeOutDuration={700}
               opacity={0.9}
               textStyle={{color:'white'}}
           />
      </View>

  }
}
 // /
class WinChallengesView extends React.Component {
  render () {
    return (
       <View style={styles.mainContainer}>
          <ConnectedChallengeList stateName='winChallenges' requestType='getWinChallenges' title='Won Challenges' />
         </View>
    )
  }
}
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  console.log(stateProps);
  console.log(dispatchProps);
  console.log(ownProps);
  return Object.assign({}, ownProps, stateProps, dispatchProps)

}

const mapStateToProps = (state, props) => {
  console.log(props);
  console.log(state);
  return {
    defaultfetching:state[props.stateName].defaultfetching,
    refreshing:state[props.stateName].refreshing,
    isLoadingTail:state[props.stateName].isLoadingTail,
    fetching: state[props.stateName].fetching,
    payload: state[props.stateName].payload,
    error: state[props.stateName].error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getWinChallenges: (default_type = 'default', page = 1, challengeId = null) =>{
      console.log(default_type, page, challengeId);
      return dispatch(WinChallengesActions.winChallengesRequest(default_type, page, challengeId))
    }

  }
}


const ConnectedChallengeList = connect(mapStateToProps, mapDispatchToProps, mergeProps)(ChallengeList)
export default WinChallengesView
