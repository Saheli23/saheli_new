import React from 'react';

import {
  ScrollView,
  Text,
  KeyboardAvoidingView,
  Image,
  ListView,
  View,
  TouchableOpacity,
  Button,
  TextInput,
  Platform,
  findNodeHandle,
  Dimensions,
  AsyncStorage
} from 'react-native';

const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)

//import OpenChallengesActions from '../../Redux/OpenChallengesRedux'

//import OpenChallengesActions from '../../Redux/OpenChallengesRedux'
import FavoritesMembersActions from '../../Redux/FavoritesMembersRedux';
import MyOpenChallengesActions from '../../Redux/MyOpenChallengesRedux';
import UserDetailsActions from '../../Redux/UserDetailsRedux';
import UserEditActions from '../../Redux/UserEditRedux';
import { Metrics, Images, Colors } from '../../Themes';

// external libs
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions as NavigationActions, ActionConst } from 'react-native-router-flux';
import Animatable from 'react-native-animatable';

// Styles
//import styles from './Styles/OpenChallengesViewStyle'
//import styles from './Styles/JudgingChallengesViewStyle'
// Styles
import styles from '../Styles/MyProfileStyle';

// alert message
import AlertMessage from '../../Components/AlertMessage';

// I18n
import I18n from 'react-native-i18n';

// progressBar
import * as Progress from 'react-native-progress';
import * as _ from 'lodash';
// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner';
import ImagePicker from 'react-native-image-picker'

import renderIf from './renderIf';
/**
 * EditProfileComponent
 */

 class EditProfile extends React.Component{
   render(){
     let itemData = this.props.itemData;

   }
 }



 class MemberProfile extends React.Component {
   render() {
   let itemData = this.props.itemData;

     return (
       <View style={styles.profile}>
         <View style={styles.mainPanel}>
           <View style={styles.coverImage}>
             <Image
               style={[styles.coverImageInside,{backgroundColor:'white'}]}
                  source={{uri: itemData.latest_cover_image}}
              // source={{uri: mediaCoverPath}}
               defaultSource={require('../../Images/monkey_default.png')}
             />
           </View>


             <View style={styles.profileImage}>
               <Image
                 style={[styles.profileImageInside,{backgroundColor:'white'}]}
                  source={{uri: itemData.latest_profile_image}}
                 //source={{uri: mediaPath}}
                 defaultSource={require('../../Images/person.png')}
               />
           </View>
         </View>

         <TouchableOpacity
           style={{
             zIndex: 2,
             padding: 5,
             alignItems: 'flex-end',
             marginRight: 5,
             marginTop: 5,
             alignSelf: 'flex-end',
             backgroundColor: '#ccc'
           }}
           onPress={()=>{NavigationActions.EditProfile()}}
           >
           <Text style={{ color: '#fff' }}>Edit Profile</Text>
         </TouchableOpacity>

         <View
           style={{
             alignItems: 'center',
             marginTop: 10,
             justifyContent: 'center'
           }}>
           <Text style={styles.boldLabel}>
             {itemData.first_name} {itemData.last_name}
           </Text>
           <Text style={styles.usernameLabel}>@{itemData.handle_name}</Text>
           <Text style={styles.messageLabel}>{itemData.message}</Text>
         </View>
         <View
           style={{
             flexDirection: 'row',
             alignItems: 'stretch',
             justifyContent: 'space-around',
             height: 50
           }}>
           <TouchableOpacity
             style={{
               backgroundColor: '#33CC32',
               justifyContent: 'center',
               alignItems: 'center',
               flexGrow: 1
             }}>
             <Text
               style={{
                 fontWeight: 'bold',
                 color: 'white',
                 fontWeight: 'bold',
                 alignItems: 'flex-start'
               }}>
               Funds Raised
             </Text>
             <Text
               style={{
                 fontWeight: 'bold',
                 color: 'white',
                 fontWeight: 'bold',
                 alignItems: 'flex-start'
               }}>
               ${itemData.total_won}
             </Text>
           </TouchableOpacity>
           <TouchableOpacity
             style={{
               backgroundColor: 'red',
               justifyContent: 'center',
               alignItems: 'center',
               flexGrow: 1
             }}>
             <Text
               style={{
                 fontWeight: 'bold',
                 color: 'white',
                 fontWeight: 'bold',
                 alignItems: 'flex-start'
               }}>
               Funds Missed
             </Text>
             <Text
               style={{
                 fontWeight: 'bold',
                 color: 'white',
                 fontWeight: 'bold',
                 alignItems: 'flex-start'
               }}>
               ${itemData.total_lost}
             </Text>
           </TouchableOpacity>
         </View>

       </View>
     );
   }
 }

 type EditProfileComponentProps = {
   dispatch: () => any,
   fetching: boolean,
   payload: object,
   editProfile: () => void,
   getUserDetails:() => void,

 };




  componentWillReceiveProps(newProps) {
    console.log("receive recive props:",newProps);
    this.setState({ fetching: newProps.fetching, payload: newProps.payload });
    // if (!newProps.fetching) {
    //   this.setState({ fetching: newProps.fetching, payload: newProps.payload });
    // } else {
    //   this.setState({ fetching: newProps.fetching, payload: newProps.payload });
    // }
  }

  componentWillMount(){
    this.getTokenItem()
    this.props.getUserDetails()

  }
  getTokenItem() {
    AsyncStorage.getItem('MCtoken').then((value) =>{
      this.setState({token:value});
    })
   }
  testProps = (itemData) => {
    alert("test");
  }


  

  inputFocused(ref) {
     this._scroll(ref, 120);
   }

   inputBlurred(ref) {
     this._scroll(ref, 0);
   }

   _scroll(ref, offset) {
     setTimeout(() => {
       var scrollResponder = this.refs.myScrollView.getScrollResponder();
       scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
                findNodeHandle(this.refs[ref]),
                  offset,
                  true
              );
       });
    }


const mapStateToProps = (state, props) => {
  console.log(props);
  return {
    fetching: state.userDetails.fetching,
    payload: state.userDetails.payload
  };
};

const mapDispatchToProps = dispatch => {
  return {
  getUserDetails: (profilemessage = null, default_type = 'userdetails') => dispatch(UserDetailsActions.userDetailsRequest(profilemessage, default_type)),
  editProfile: (profilemessage = null, default_type = 'useredit') => dispatch(UserDetailsActions.userDetailsRequest(profilemessage, default_type))
  //editProfile: (profilemessage) => dispatch(UserEditActions.userEditRequest(profilemessage))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileComponent)
