// @flow

import React from 'react'
import {
    ScrollView,
    Text,
    KeyboardAvoidingView,
    Image,
    ListView,
    StyleSheet,
    View,
    TouchableOpacity,
    RefreshControl,
    Dimensions,
    AsyncStorage
   } from 'react-native'

   const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)


import WinChallengesActions from '../Redux/WinChallengesRedux'

import SearchResultActions from '../Redux/SearchResultRedux'

import { Metrics, Images, Colors } from '../Themes'

// external libs
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions as NavigationActions } from 'react-native-router-flux'
//import Animatable from 'react-native-animatable'

// Styles
//import styles from './Styles/OpenChallengesViewStyle'
//import styles from './Styles/JudgingChallengesViewStyle'
import styles from './Styles/JudgingChallengesViewStyle'

// alert message
import AlertMessage from '../Components/AlertMessage'

// I18n
import I18n from 'react-native-i18n'

// progressBar
import * as Progress from 'react-native-progress'

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'

import Toast, {DURATION} from 'react-native-easy-toast'
import * as _ from 'lodash';

type ChallengeListProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,
    getTrending: () => void,
    getSearchChallenges:() => void,
}

 // /
class SearchResultView extends React.Component {
  constructor(props) {
    super(props);
    const rowHasChanged = (r1, r2) => r1 !== r2
    const sectionHeaderHasChanged = (s1, s2) => s1 !== s2
    this.state = {
      loaded: false,
      dataSource: new ListView.DataSource({rowHasChanged, sectionHeaderHasChanged}),
      noresults:false,
      searchCount:'',
    };
  }
  props: ChallengeListProps;
  componentWillReceiveProps(newProps) {
    console.log(newProps);

    if(newProps.payload.challenges == 0 && newProps.payload.charities == 0 && newProps.payload.companies == 0 && newProps.payload.users == 0){
      this.setState({noresults:true });
    }
    if(newProps.payload && newProps.payload.challenges){
      this.setState({  searchCount:newProps.payload.challenges.length});
    }
    this.setState({ payload: newProps.payload,
      defaultfetching:newProps.defaultfetching,
        fetching:newProps.fetching,
    dataSource: this.state.dataSource.cloneWithRowsAndSections(newProps.payload)
   });
  }
  componentWillMount () {



    let {challenge_name,keyword} = this.props;
    console.log(challenge_name,keyword);
    this.props.getSearchChallenges('default',1, null, challenge_name,keyword);
    // console.log();
    // this.getMYuuid();
    // console.log("test will mount");
    // console.log(this.props);
    // this.props[this.props.requestType]()
  }
  shouldComponentUpdate(nextProps, nextState){
    // return a boolean value

    return true;
}

  pressHandlerOpen = (rowData,sectionID) => {

    switch (rowData.status) {
case 'judge-open':
NavigationActions.JudgingChallengeDetailView({ item: rowData });
break;
case 'judge-vote':
NavigationActions.JudgingChallengeDetailView({ item: rowData });
break;
case 'judge-result':
NavigationActions.JudgingChallengeDetailView({ item: rowData });
break;
case 'win':
NavigationActions.OpenChallengeDetailView({ item: rowData });
break;
case 'pending':
NavigationActions.ChallengeDetailView({ item: rowData });
break;
case 'open':
NavigationActions.OpenChallengeDetailView({ item: rowData });
break;
case 'fail':
NavigationActions.OpenChallengeDetailView({ item: rowData });
break;
default:
NavigationActions.OpenChallengeDetailView({ item: rowData });
    }
  }

  pressHandlerOpenMember = (rowData,sectionID) => {

    NavigationActions.memberProfile({ userId: rowData.uuid })
  }
  renderRow = (rowData, sectionID) => {
    // You can condition on sectionID (key as string), for different cells
    // in different sections
    switch (sectionID) {
      case 'challenges':
      return (
        <View style={styles.row}>

        <View style={styles.imageSection}>
          <TouchableOpacity onPress={()=>{this.pressHandlerOpen(rowData,sectionID)}}>
            <Image style={styles.cardImage} source={{uri: rowData.challenge_media[0].path}} defaultSource={require('../Images/monkey_default.png')} />
          </ TouchableOpacity>

          <View style={{position:'absolute',backgroundColor:'black',padding:3,flexDirection:'row',top:10,left:5,borderRadius:5}}>
          <Text style={{color:'#FFFFFF',fontWeight:'bold',fontSize:10}}>{this.props.cat_name}</Text>
          </View>

  {/*
          <TouchableOpacity style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
          <Icon name='heart-o' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
          </TouchableOpacity>
  */}




         {rowData.status !='judge-open' && rowData.status !='pending' &&
          <View style={styles.imageFooter}>
            <View style={{alignItems: 'center', justifyContent:'center'}}>
             <Progress.Bar progress={(Math.round(rowData.percentage_funded) / 100)} color={'#33CC32'} height={8} width={(width - 20)} borderWidth={0} unfilledColor={'rgba(128,132,131,0.8)'}/>

            <View style={[styles.contentVoteSection]}>
              <Text style={{color: 'white',fontWeight:'bold' }}>${parseInt(rowData.total_amount_funded)}<Text style={{fontSize:10}}> OF ${parseInt(rowData.price)} FUNDED</Text></ Text>
              {/*
              <Text style={{color: 'white'}}>{Math.round(rowData.percentage_funded)}%</Text>
              */}
              <Text style={{color: 'white',fontWeight:'bold', textAlign: 'right'}}>{rowData.funding_days_remaining} <Text style={{fontSize:10}}>DAYS LEFT</Text></Text>
            </View>
          </View>
          </View>
        }
        {rowData.status=='pending' &&
        <View style={styles.imageFooter}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <View style={styles.contentVoteSection}>
          <Text style={{color: 'white',fontWeight:'bold', fontSize:30 }}>${parseInt(rowData.price)}<Text style={{fontSize:10}}> FUNDED</Text></ Text>
          {/*
          <Text style={{color: 'white'}}>{Math.round(rowData.percentage_funded)}%</Text>
          */}
            <View style={{justifyContent:'flex-end'}}>
              <Text style={{color: 'white', textAlign: 'right', fontWeight:'bold',fontFamily:'Bradley Hand'}}>Getting ready!</Text>
            </View>

          </View>
          </View>
        </View>
        }
          {rowData.status=='judge-open' &&
          <View style={styles.imageFooter}>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <View style={styles.contentVoteSection}>
            <Text style={{color: 'white',fontWeight:'bold', fontSize:30 }}>${parseInt(rowData.price)}<Text style={{fontSize:10}}> FUNDED</Text></ Text>
            {/*
            <Text style={{color: 'white'}}>{Math.round(rowData.percentage_funded)}%</Text>
            */}
            <View style={{flexDirection:'row'}}>

                  <Image style={{width:30,height:30,marginRight:10}} source={Images.hammer}/>

              <View style={{flexDirection:'column',alignItems:'flex-start'}}>
                <Text style={{color: 'white', textAlign: 'right', fontWeight:'bold',fontFamily:'Bradley Hand'}}>Judging</Text>
                <Text style={{color: 'white',fontFamily:'Bradley Hand', textAlign: 'right',fontWeight:'bold'}}>Challenge</Text>
              </View>
              </View>
            </View>
            </View>
          </View>
        }
          <View style={styles.imageFooterOpacity} />
        </View>
        <View style={styles.contentSection}>

        <View style={[styles.contentDescriptionSection,{flexDirection:'column',width:(width - 20),alignSelf:'center'}]}>
      {/*
             <Text style={{color: '#1266AF',fontWeight:'bold',}}>{rowData.handle_name} :
              <Text style={{color: '#000000'}}> {rowData.name}</Text></Text>
              <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: rowData.uuid })}}>
      */}

      <View style={{flexDirection:'row',width:(width - 20),}}>
        <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: rowData.uuid })}}>
            <Text style={{color: '#1266AF',fontWeight:'bold',flexWrap: 'wrap'}}>{rowData.handle_name} :
            <Text numberOfLines={2} style={{color: '#000000',fontWeight:'bold',}}> {rowData.name}</Text></Text>
        </TouchableOpacity>
      </View>
            <Text style={{fontSize:12}} numberOfLines={2}>{rowData.description}</Text>
        </View>

        </View>
        </View>


      )
        break;
        // case 'users':
        // return (
        //   <TouchableOpacity
        //     style={styles.challenge}
        //     onPress={() => {
        //       console.log(rowData.status);
        //       this.pressHandlerOpenMember(rowData,sectionID)
        //     }}>
        //     <View style={styles.imageSection}>
        //
        //       <Image
        //         style={styles.image}
        //         source={{ uri: rowData.latest_profile_image }}
        //         defaultSource={require('../Images/monkey_default.png')}
        //       />
        //
        //     </View>
        //   </TouchableOpacity>
        //
        //
        // )
        //   break;
          case 'charities':
          return (
            <TouchableOpacity
              style={styles.challenge}
              onPress={() => {
                console.log(rowData.status);
                this.pressHandlerOpen(rowData)
              }}>
              <View style={styles.imageSection}>

              <Text>{sectionID}</Text>
              </View>
            </TouchableOpacity>


          )
            break;
              case 'companies':
              return (
                <TouchableOpacity
                  style={styles.challenge}
                  onPress={() => {
                    console.log(rowData.status);
                    this.pressHandlerOpen(rowData)
                  }}>
                  <View style={styles.imageSection}>

                  <Text>{sectionID}</Text>
                  </View>
                </TouchableOpacity>


              )
                break;
      default:
      // return (
      //   <TouchableOpacity
      //     style={styles.challenge}
      //     onPress={() => {
      //       console.log(rowData.status);
      //       this.pressHandlerOpen(rowData)
      //     }}>
      //     <View style={styles.imageSection}>
      //
      //     <Text>{sectionID}</Text>
      //     </View>
      //   </TouchableOpacity>
      //
      //
      // )
      return (

           <View>
           </View>
       )
    }

  }
_renderEmptyView(){
  return (
    <View style={{backgroundColor:'red'}}><Text>Empty section : no data found</Text></View>
  )
}
handleUnfavorit = (id) => {
  this.props.getSearchChallenges('UnFavourite',1,id,null,null);
}
  renderHeader (data, sectionID) {
    console.log(data);
    console.log(sectionID);
    switch (sectionID) {
      case 'challenges':
      return <View style={{height:20,width:width,margin:10}}><Text style={styles.boldLabel}>Challenges</Text></View>
      // case 'users':
      // return <View style={{height:20,width:width,margin:10}}><Text style={styles.boldLabel}>Users</Text></View>
      case 'charities':
      return <View style={{height:20,width:width,margin:10}}><Text style={styles.boldLabel}>Charities
      </Text></View>
      case 'companies':
      return <View style={{height:20,width:width,margin:10}}><Text style={styles.boldLabel}>Companies
      </Text></View>

      default:
      // return <View style={{height:20,width:width,margin:10}}><Text style={styles.boldLabel}>default</Text></View>
      return <View style={{height:20,width:width,margin:10}}><Text style={styles.boldLabel}></Text></View>
    }
  }
  openModel(){
      NavigationActions.pop();
      //NavigationActions.ModelTest()
      setTimeout(function() { NavigationActions.ModelTest(); }, 10);

  }
  render () {
    console.log("this.state.payload",this.state.fetching,this.state);
    return (
      <View style={styles.mainContainer}>
      { this.state.defaultfetching &&
       <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
        <GiftedSpinner size='large' />
      </View>
      }
      {!this.state.fetching &&
      <View style={{alignItems:'center',marginTop:10}}>
      <Text style={{alignItems:'center'}}>Your search with <Text style={{fontWeight: 'bold',color:'#f9642f'}}>{this.props.keyword}</Text> have found</Text>
      <Text>{this.state.searchCount} result(s)</Text>
      </View>
      }
      {this.state.noresults&&
         <AlertMessage title='No records found, Try searching something else!' show={(this.state.noresults)} />

       }
       {!this.state.fetching && this.state.noresults&&
       <TouchableOpacity onPress={this.openModel} style={{alignItems:'center',}}>
        <Text style={{fontWeight: 'bold',color:'#f9642f'}}>Search Again</Text></TouchableOpacity>
      }

      { this.state.fetching &&
        <GiftedSpinner />
      }

        <ListView
          renderSectionHeader={this.renderHeader}
          contentContainerStyle={styles.listContent}
          dataSource={this.state.dataSource}
          renderRow={this.renderRow}

        />
      </View>
    );
  }
}
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  console.log(stateProps);
  console.log(dispatchProps);
  console.log(ownProps);
  return Object.assign({}, ownProps, stateProps, dispatchProps)

}

const mapStateToProps = (state, props) => {
  console.log(props);
  console.log(state);
  return {
     defaultfetching:state.searchResult.defaultfetching,
     fetching: state.searchResult.fetching,
     payload: state.searchResult.payload,
     error: state.searchResult.error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
     getSearchChallenges: (default_type = 'default',page = 1, challengeId = null, challenge_name = null, keyword = null) =>{
      console.log(default_type, page, challengeId, challenge_name, keyword);
   return dispatch(SearchResultActions.searchResultRequest(default_type, page, challengeId, challenge_name, keyword))
    }

  }
}


// const ConnectedChallengeList = connect(mapStateToProps, mapDispatchToProps, mergeProps)(ChallengeList)
// export default SearchResultView

export default connect(mapStateToProps, mapDispatchToProps)(SearchResultView)
