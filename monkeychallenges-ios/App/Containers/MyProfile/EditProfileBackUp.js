// @flow
import React from 'react';
import {
  ScrollView,
  Text,
  KeyboardAvoidingView,
  Image,
  ListView,
  View,
  TouchableOpacity,
  Button,
  AsyncStorage,
  Platform,
  TextInput
} from 'react-native';
import { connect } from 'react-redux';

// Add Actions - replace 'Your' with whatever your reducer is called :)

//import OpenChallengesActions from '../../Redux/OpenChallengesRedux'

//import OpenChallengesActions from '../../Redux/OpenChallengesRedux'
import FavoritesMembersActions from '../../Redux/FavoritesMembersRedux';
import MyOpenChallengesActions from '../../Redux/MyOpenChallengesRedux';
import UserDetailsActions from '../../Redux/UserDetailsRedux';
import UserEditActions from '../../Redux/UserEditRedux';
import { Metrics, Images, Colors } from '../../Themes';

// external libs
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions as NavigationActions } from 'react-native-router-flux';
import Animatable from 'react-native-animatable';

// Styles
//import styles from './Styles/OpenChallengesViewStyle'
//import styles from './Styles/JudgingChallengesViewStyle'
// Styles
import styles from '../Styles/MyProfileStyle';

// alert message
import AlertMessage from '../../Components/AlertMessage';

// I18n
import I18n from 'react-native-i18n';

// progressBar
import * as Progress from 'react-native-progress';
import * as _ from 'lodash';
// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner';
import ImagePicker from 'react-native-image-picker'
import EditProfileComponent from './EditProfileComponent'
type ChallengeListProps = {
  dispatch: () => any,
  fetching: boolean,
  payload: object,
  getTrending: () => void,
  editProfile: () => void,
  message: string
};



class EditProfile extends React.Component {
  props: ChallengeListProps
  state: {
        message: string,
        showTextbox: false,
        showSavebttn: false,
      }

  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      token:"",
      isUploading:false,
      showTextbox: false,
      showSavebttn:false,

      //itemData:
    };
    // console.log(this.props.item);
  }

  componentWillMount(){
    //this.getTokenItem()

  }

  render() {
    return (
  <ScrollView style={styles.mainContainer}>
        <EditProfileComponent item={{type:'EditProfile'}} />
 </ScrollView>

    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    fetching: state.userEdit.fetching,
    payload: state.userEdit.payload
  };
};

const mapDispatchToProps = dispatch => {
  return {
    editProfile: (profilemessage) => dispatch(UserEditActions.userEditRequest(profilemessage)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile)
