import React from 'react';

import {
  ScrollView,
  Text,
  KeyboardAvoidingView,
  Image,
  ListView,
  View,
  TouchableOpacity,
  Button,
  TextInput,
  Platform,
  findNodeHandle,
  Dimensions,
  AsyncStorage
} from 'react-native';

const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)

//import OpenChallengesActions from '../../Redux/OpenChallengesRedux'

//import OpenChallengesActions from '../../Redux/OpenChallengesRedux'
import FavoritesMembersActions from '../../Redux/FavoritesMembersRedux';
import MyOpenChallengesActions from '../../Redux/MyOpenChallengesRedux';
import UserDetailsActions from '../../Redux/UserDetailsRedux';
import UserEditActions from '../../Redux/UserEditRedux';
import { Metrics, Images, Colors } from '../../Themes';

// external libs
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions as NavigationActions, ActionConst } from 'react-native-router-flux';
import Animatable from 'react-native-animatable';

// Styles
//import styles from './Styles/OpenChallengesViewStyle'
//import styles from './Styles/JudgingChallengesViewStyle'
// Styles
import styles from '../Styles/MyProfileStyle';

// alert message
import AlertMessage from '../../Components/AlertMessage';

// I18n
import I18n from 'react-native-i18n';

// progressBar
import * as Progress from 'react-native-progress';
import * as _ from 'lodash';
// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner';
import ImagePicker from 'react-native-image-picker'

import renderIf from './renderIf';
/**
 * EditProfileComponent
 */

 class EditProfile extends React.Component{
   render(){
     let itemData = this.props.itemData;

   }
 }



 class MyProfile extends React.Component {
   render() {
   let itemData = this.props.itemData;

     return (
       <View style={styles.profile}>
         <View style={styles.mainPanel}>
           <View style={styles.coverImage}>
             <Image
               style={[styles.coverImageInside,{backgroundColor:'white'}]}
                  source={{uri: itemData.latest_cover_image}}
              // source={{uri: mediaCoverPath}}
               defaultSource={require('../../Images/monkey_default.png')}
             />
           </View>


             <View style={styles.profileImage}>
               <Image
                 style={[styles.profileImageInside,{ backgroundColor:(Platform.OS === 'ios') ? 'white' :'transparent' }]}
                  source={{uri: itemData.latest_profile_image}}
                 //source={{uri: mediaPath}}
                 defaultSource={require('../../Images/person.png')}
               />
           </View>
         </View>

         <TouchableOpacity
           style={{
             zIndex: 2,
             padding: 5,
             alignItems: 'flex-end',
             marginRight: 5,
             marginTop: 5,
             alignSelf: 'flex-end',
             backgroundColor: '#ccc'
           }}
           onPress={()=>{NavigationActions.EditProfile()}}
           >
           <Text style={{ color: '#fff' }}>Edit Profile</Text>
         </TouchableOpacity>

         <View
           style={{
             alignItems: 'center',
             marginTop: 10,
             justifyContent: 'center'
           }}>
           <Text style={styles.boldLabel}>
             {itemData.first_name} {itemData.last_name}
           </Text>
           <Text style={styles.usernameLabel}>@{itemData.handle_name}</Text>
           <Text style={styles.messageLabel}>{itemData.message}</Text>
         </View>
         <View
           style={{
             flexDirection: 'row',
             alignItems: 'stretch',
             justifyContent: 'space-around',
             height: 50
           }}>
           <TouchableOpacity
             style={{
               backgroundColor: '#33CC32',
               justifyContent: 'center',
               alignItems: 'center',
               flexGrow: 1
             }}>
             <Text
               style={{
                 fontWeight: 'bold',
                 color: 'white',
                 fontWeight: 'bold',
                 alignItems: 'flex-start'
               }}>
               Funds Raised
             </Text>
             <Text
               style={{
                 fontWeight: 'bold',
                 color: 'white',
                 fontWeight: 'bold',
                 alignItems: 'flex-start'
               }}>
               ${itemData.total_won}
             </Text>
           </TouchableOpacity>
           <TouchableOpacity
             style={{
               backgroundColor: 'red',
               justifyContent: 'center',
               alignItems: 'center',
               flexGrow: 1
             }}>
             <Text
               style={{
                 fontWeight: 'bold',
                 color: 'white',
                 fontWeight: 'bold',
                 alignItems: 'flex-start'
               }}>
               Funds Missed
             </Text>
             <Text
               style={{
                 fontWeight: 'bold',
                 color: 'white',
                 fontWeight: 'bold',
                 alignItems: 'flex-start'
               }}>
               ${itemData.total_lost}
             </Text>
           </TouchableOpacity>
         </View>

       </View>
     );
   }
 }

 type EditProfileComponentProps = {
   dispatch: () => any,
   fetching: boolean,
   payload: object,
   editProfile: () => void,
   getUserDetails:() => void,

 };



export class EditProfileComponent extends React.Component { // eslint-disable-line react/prefer-stateless-function
  props: EditProfileComponentProps
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      token:"",
      isUploading:false,
      showTextbox: false,
      showSavebttn:false,
      message: '',
    };
    // console.log(this.props.item);
  }

  componentWillReceiveProps(newProps) {
    console.log("receive recive props:",newProps);
    this.setState({ fetching: newProps.fetching, payload: newProps.payload });
    // if (!newProps.fetching) {
    //   this.setState({ fetching: newProps.fetching, payload: newProps.payload });
    // } else {
    //   this.setState({ fetching: newProps.fetching, payload: newProps.payload });
    // }
  }

  componentWillMount(){
    this.getTokenItem()
    this.props.getUserDetails()

  }
  getTokenItem() {
    AsyncStorage.getItem('MCtoken').then((value) =>{
      this.setState({token:value});
    })
   }
  testProps = (itemData) => {
    alert("test");
  }


  profileImageUpload = (data,getUuid,media_type_id = null) => {
    //const {uuid} = getUuid;
    console.log(data);
    this.setState({fetching:true});
    const { token } = this.state
  //console.log("useruuid",getUuid);
   let url = `https://api-dev.monkeychallenges.com/api/users/${getUuid}/media`;




   if(Platform.OS === 'ios'){
    console.log("video image path : ",{"name": data.fileName || data.uri.split('/').pop(),
        "uri" : data.uri,
        "type" : 'image/jpg'});

   }else{

   console.log("video image path : ",{"name": data.path.split('/').pop(),
        "uri" : `file:///${data.path}`,
        // "type" : `image/${data.path.split('.').pop()}`});
        "type" :data.type?data.type :'image/jpg'});
   }

  var xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    var formdata = new FormData();
    formdata.append('name', '');
    formdata.append('description', '');
    formdata.append('media_type_id', media_type_id);
  if(Platform.OS === 'ios'){
      formdata.append('file', {"name": data.fileName || data.uri.split('/').pop(),
        "uri" : data.uri,
        "type" : 'image/jpg'});
   }else{
    formdata.append('file', {"name": data.path.split('/').pop(),
        "uri" : `file:///${data.path}`,
        // "type" : `image/${data.path.split('.').pop()}`});
        "type" : data.type ? data.type :'image/jpg'});

   }
    xhr.setRequestHeader('Cache-Control', 'no-cache');
    xhr.setRequestHeader('Authorization', 'Bearer ' + token);
    var self = this;
    let{user:{latest_cover_image, latest_profile_image}} = this.state.payload;
    xhr.onload = function () {
          if(this.status >= 200 && this.status < 300){
            //resolve(xhr.response);
            console.log(xhr.response);
           let resData =  JSON.parse(xhr.response);
           console.log(resData.path);

            // if(media_type_id == 1){
            //   console.log(latest_cover_image, latest_profile_image);
            //   self.setState({
            //       fetching:false,
            //       payload:{
            //         user:{
            //           ...self.state.payload.user,
            //           latest_cover_image:resData.path
            //         }
            //     }});
            // }else if(media_type_id == 2){
            //     console.log(latest_cover_image, latest_profile_image);
            // //  self.setState({fetching:false,payload:{user:{latest_profile_image:resData.path}}});
            //   self.setState({
            //       fetching:false,
            //       payload:{
            //         user:{
            //           ...self.state.payload.user,
            //           latest_profile_image:resData.path
            //         }
            //     }});
            // }


           self.props.getUserDetails(null,'userImageUpload');
           if(Platform.OS === 'android'){
             setTimeout(() => {
               NavigationActions.pop();

             }, 3000);
           }

          }else{
            self.setState({fetching:false});
            alert("image upload fails!! try again.");
            console.log(this.status);
            console.log(xhr.statusText);
          }
    };

    xhr.onerror = function () {
          // reject({
          //   status: this.status,
          //   statusText: xhr.statusText
          // })
          self.setState({fetching:false});
          console.log(xhr.statusText);
    };

   xhr.send(formdata);

 }


  handleUploadCoverImage(itemData){
    // alert('image cover upload!')
    console.log("pick",itemData.uuid);
    const options = {
       title: 'Cover image Picker',
       takePhotoButtonTitle: 'Take Image...',
       mediaType: 'image',
       allowsEditing:true,
       imageQuality: 'medium'

     };

     ImagePicker.showImagePicker(options, (response) => {
       console.log('Response = ', response);

       if (response.didCancel) {
         console.log('User cancelled image picker');
       }
       else if (response.error) {
         console.log('ImagePicker Error: ', response.error);
       }
       else if (response.customButton) {
         console.log('User tapped custom button: ', response.customButton);
       }
       else {
         // this.setState({
         //   videoSource: response.uri
         // });
         console.log(response.uri);
         console.log("uploading started!!");
         this.profileImageUpload(response,itemData.uuid,1);
       }
     });
  }
  handleUploadProfileImage(itemData){
      //alert('image profile upload!')

      console.log("pick",itemData.uuid);
      const options = {
         title: 'Profile image Picker',
         takePhotoButtonTitle: 'Take Image...',
         mediaType: 'image',
         allowsEditing:true,
         imageQuality: 'medium'
       };

       ImagePicker.showImagePicker(options, (response) => {
         console.log('Response = ', response);

         if (response.didCancel) {
           console.log('User cancelled image picker');
         }
         else if (response.error) {
           console.log('ImagePicker Error: ', response.error);
         }
         else if (response.customButton) {
           console.log('User tapped custom button: ', response.customButton);
         }
         else {
           // this.setState({
           //   videoSource: response.uri
           // });
           console.log(response.uri);
           console.log("uploading started!!");
           this.profileImageUpload(response,itemData.uuid,2);
         }
       });
  }

  handleEditProfile(){
    this.setState({
          message:this.state.payload.user.message,
          showTextbox: !this.state.showTextbox,
          showSavebttn: !this.state.showSavebttn
    })

  }
  doEdit(){
    let { message } = this.state;
    console.log(message);
    this.setState({
         showTextbox: !this.state.showTextbox,
         showSavebttn: !this.state.showSavebttn
     });
   this.props.editProfile(message)
   if(Platform.OS === 'android'){
     setTimeout(() => {
       NavigationActions.pop();

     }, 2000);
   }



  }
  inputFocused(ref) {
     this._scroll(ref, 120);
   }

   inputBlurred(ref) {
     this._scroll(ref, 0);
   }

   _scroll(ref, offset) {
     setTimeout(() => {
       var scrollResponder = this.refs.myScrollView.getScrollResponder();
       scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
                findNodeHandle(this.refs[ref]),
                  offset,
                  true
              );
       });
    }
  render() {

console.log(this.props.item.type);
    if (this.props.item.type == 'ShowMyProfile') {
      console.log(this.state.payload, 'newuser');
      let payload = (this.state && this.state.payload) || { user: {} };
      console.log(payload, 'playload.Userdata', this.props.title);
      console.log(this.props.item.type);
      return (
        <ScrollView vertical>
          {Object.keys(payload.user).length === 0 && <GiftedSpinner />}
          {Object.keys(payload.user).length !== 0 &&
            <MyProfile itemData={payload.user}/>
            }
        </ScrollView>
      );
    }else if(this.props.item.type == 'EditProfile'){
      console.log(this.state.payload, 'newuser');
      let payload = (this.state && this.state.payload) || { user: {} };
      console.log(payload, 'playload.Userdata', this.props.title);
      console.log(this.props.item.type);

      return (
  <ScrollView vertical ref="myScrollView" item={{type:'EditMessage'}} keyboardDismissMode='interactive'>
        { this.state.fetching &&
         <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
        <GiftedSpinner size='large' />
        </View>
        }
        <View>

           <View>
             <View style={(Platform.OS === 'ios') ? styles.coverImage:styles.coverImageAndroid}>
               <Image
                  style={[styles.coverImageInside,{backgroundColor:'white'}]}
                 source={{uri: payload.user.latest_cover_image}}
                 defaultSource={require('../../Images/monkey_default.png')}
               />
             </View>
             <View style={styles.profileImage}>
               <Image
                 style={[styles.profileImageInside,{backgroundColor:(Platform.OS === 'ios') ? 'white': 'transparent'}]}
                  source={{uri: payload.user.latest_profile_image}}
                 //source={{uri: mediaPath}}
                 defaultSource={require('../../Images/person.png')}
               />
               <TouchableOpacity style={(Platform.OS === 'ios') ? styles.profileEditImageUpperIos:styles.profileEditImageUpperAndroid} onPress={this.handleUploadProfileImage.bind(this,payload.user)} >
                <Image
                 style={[(Platform.OS === 'ios') ? styles.profileEditImage:styles.profileEditImageAndroid,{opacity:0.7}]}
                 source={require('../../Images/cameraicon.png')}
                 defaultSource={require('../../Images/cameraicon.png')}
               />
                </TouchableOpacity>
            </View>
           </View>

           <View style={(Platform.OS === 'ios') ? styles.editCoverImageIos : styles.editCoverImageAndroid}>
           <TouchableOpacity
             style={{
               padding: 5,
               alignItems: 'flex-end',
               marginRight: 5,
               marginLeft:5,
               alignSelf: 'flex-end',
             }} onPress={this.handleUploadCoverImage.bind(this,payload.user)}>
             <Image
              style={{height: 40,width: 40,}}
              source={require('../../Images/cameraicon.png')}
              defaultSource={require('../../Images/cameraicon.png')}
            />
           </TouchableOpacity>
           </View>

{/*
           {renderIf(!this.state.showSavebttn,
             <TouchableOpacity
               style={{

                 padding: 5,
                 alignItems: 'flex-end',
                 marginRight: 5,
                 marginTop: 5,
                 alignSelf: 'flex-end',
                 backgroundColor: '#ccc'
               }}
               onPress={this.handleEditProfile.bind(this, payload.user)} >
               <Text style={{ color: '#fff' }}>Edit Status</Text>
             </TouchableOpacity>
                )}

                {renderIf(this.state.showSavebttn,
                  <TouchableOpacity
                     style={{

                       padding: 5,
                       paddingRight:10,
                       paddingLeft:10,
                       alignItems: 'flex-end',
                       marginRight: 5,
                       marginTop: 5,
                       alignSelf: 'flex-end',
                       backgroundColor: '#f9642f'
                           }} onPress={this.doEdit.bind(this, payload.user)}>
                     <Text style={{ color: '#fff' }}>Save</Text>
                   </TouchableOpacity>
                     )}

                     */}

{/*  */}
           { !this.state.showSavebttn ?
           (<TouchableOpacity
             style={{
               zIndex: 2,
               padding: 5,
               alignItems: 'flex-end',
               marginRight: 5,
               marginTop: 5,
               alignSelf: 'flex-end',
               backgroundColor: '#ccc'
             }}
             onPress={this.handleEditProfile.bind(this, payload.user)} >
             <Text style={{ color: '#fff' }}>Edit Status</Text>
           </TouchableOpacity>):null

           }

           {this.state.showSavebttn ?
          (<TouchableOpacity
             style={{
               zIndex: 3,
               padding: 5,
               paddingRight:10,
               paddingLeft:10,
               alignItems: 'flex-end',
               marginRight: 5,
               marginTop: 5,
               alignSelf: 'flex-end',
               backgroundColor: '#f9642f'
                   }} onPress={this.doEdit.bind(this, payload.user)}>
             <Text style={{ color: '#fff' }}>Save</Text>
           </TouchableOpacity>):null
           }



           <View
             style={{
               alignItems: 'center',
               marginTop: 1,
               justifyContent: 'center'
             }}>
             <Text style={styles.boldLabel}>
               {payload.user.first_name} {payload.user.last_name}
             </Text>
             <Text style={styles.usernameLabel}>@{payload.user.handle_name}</Text>
           </View>
           <View style={{marginTop:15,}}>
                 <View style={{borderColor:'#ccc',borderTopWidth:1}}></View>
         <View style={{padding:10,alignItems:'center'}}>
         {/*
        //  {renderIf(!this.state.showTextbox,
        //  <View><Text style={{color:'#ed7010',}}>{payload.user.message}</Text></View>)}
        // {renderIf(this.state.showTextbox,<View><TextInput
        //   ref="EditMessageInput"
        //   style={{height: 40, borderColor: 'gray', borderWidth: 1,width:(width-20)}}
        //   value={this.state.message}
        //   onChangeText={(text) => this.setState({message:text})}
        //   placeholder="Value"
        //   onFocus={this.inputFocused.bind(this, 'EditMessageInput')}
        //   onBlur={this.inputBlurred.bind(this, 'EditMessageInput')}
        //   /></View>)}
        */}
           {!this.state.showTextbox ?
             (<View><Text style={{color:'#ed7010',}}>{payload.user.message}</Text></View>):null
           }


          {this.state.showTextbox ?
           (<View><TextInput
           ref="EditMessageInput"
           style={{height: 40, borderColor: 'gray', borderWidth: 1,width:(width-20)}}
           value={this.state.message}
           onChangeText={(text) => this.setState({message:text})}
           placeholder="Value"
           onFocus={this.inputFocused.bind(this, 'EditMessageInput')}
           onBlur={this.inputBlurred.bind(this, 'EditMessageInput')}
           /></View>):null
           }
           </View>
                 <View style={{borderColor:'#ccc',borderBottomWidth:1}}></View>
           </View>
           </View>
  </ScrollView>
      );
    }
  }
}
const mapStateToProps = (state, props) => {
  console.log(props);
  return {
    fetching: state.userDetails.fetching,
    payload: state.userDetails.payload
  };
};

const mapDispatchToProps = dispatch => {
  return {
  getUserDetails: (profilemessage = null, default_type = 'userdetails') => dispatch(UserDetailsActions.userDetailsRequest(profilemessage, default_type)),
  editProfile: (profilemessage = null, default_type = 'useredit') => dispatch(UserDetailsActions.userDetailsRequest(profilemessage, default_type))
  //editProfile: (profilemessage) => dispatch(UserEditActions.userEditRequest(profilemessage))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileComponent)
