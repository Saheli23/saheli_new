// @flow
import React from 'react';
import {
  ScrollView,
  Text,
  KeyboardAvoidingView,
  Image,
  ListView,
  View,
  TouchableOpacity,
  Button,Dimensions
} from 'react-native';
const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)

//import OpenChallengesActions from '../../Redux/OpenChallengesRedux'

//import OpenChallengesActions from '../../Redux/OpenChallengesRedux'
import FavoritesMembersActions from '../../Redux/FavoritesMembersRedux';
import MyOpenChallengesActions from '../../Redux/MyOpenChallengesRedux';
import MemberOpenChallengesActions from '../../Redux/MemberOpenChallengesRedux';
import UserDetailsActions from '../../Redux/UserDetailsRedux';
import { Metrics, Images, Colors } from '../../Themes';

// external libs
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions as NavigationActions, ActionConst } from 'react-native-router-flux';
import Animatable from 'react-native-animatable';

// Styles
//import styles from './Styles/OpenChallengesViewStyle'
//import styles from './Styles/JudgingChallengesViewStyle'
// Styles
import styles from '../Styles/MyProfileStyle';

// alert message
import AlertMessage from '../../Components/AlertMessage';

// I18n
import I18n from 'react-native-i18n';

// progressBar
import * as Progress from 'react-native-progress';
import * as _ from 'lodash';
// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner';

import MemberProfileComponent from './MemberProfileComponent'

import MemberOpenChallengeView from '../MemberOpenChallengeView'

//import EditMyOpenChallengesView from '../MyChallenges/EditMyOpenChallengesView'
//import MyOpenChallengesView from '../MyChallenges/MyOpenChallengesView'

import NavigationStateHandler from 'react-native-router-flux-hooks'


type ChallengeListProps = {
  dispatch: () => any,
  fetching: boolean,
  payload: object,
  getTrending: () => void
};

class OpenChallengeView extends React.Component {
  handleChallengesView(itemData) {
    NavigationActions.OpenChallengeDetailView({ item: itemData });
    //this.props.levelSelected(levelNumber);
  }
  render() {
    let itemData = this.props.itemData;
    console.log('itemData',itemData);
    let icon = itemData.challenge_media[0].path == undefined
      ? 'https://facebook.github.io/react/img/logo_og.png'
      : itemData.challenge_media[0].path;
    return (
      <View style={styles.challenge}>
        <View style={styles.imageSection}>
          <View style={{ height: 180 }}>
            <TouchableOpacity
              onPress={this.handleChallengesView.bind(this, itemData)}>
              {itemData.challenge_media[0].path &&
                <Image
                  style={{ height: 180 }}
                  source={{ uri: icon }}
                  defaultSource={require('../../Images/monkey_default.png')}
                />}
            </TouchableOpacity>
          </View>

          <View style={styles.imageFooter}>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>

<Progress.Bar progress={(Math.round(itemData.percentage_funded) / 100)} color={'#33CC32'} height={8} width={(width - 20)} borderWidth={0} unfilledColor={'rgba(128,132,131,0.8)'}/>
            </View>
            <View style={styles.contentVoteSection}>
              <Text style={{ color: 'white' }}>$ {itemData.price} USD</Text>
              <Text style={{ color: 'white' }}>
                {Math.round(itemData.percentage_funded)}%
              </Text>
              <Text style={{ color: 'white', textAlign: 'right' }}>
                {itemData.days_remaining} days left
              </Text>
            </View>
          </View>
          <View style={styles.imageFooterOpacity} />
        </View>
        <View style={styles.contentSection}>

        <View style={styles.contentDescriptionSection}>
               <Text style={{color: '#1266AF',fontWeight:'bold',}}>{itemData.handle_name} : <Text style={{color: '#000000'}}>{itemData.name}</Text></Text>
            <Text style={{fontSize:12}} numberOfLines={3}>{itemData.description}</Text>
        </View>


        </View>
      </View>
    );
  }
}

class ChallengeList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      userid:'',
    };
  }
  props: ChallengeListProps;
  componentWillReceiveProps(newProps) {
    console.log("receive recive props:",newProps);
    if (!newProps.fetching) {
      this.setState({ fetching: newProps.fetching, payload: newProps.payload });
    } else {
      this.setState({ fetching: newProps.fetching, payload: newProps.payload });
    }
  }

  componentWillMount() {
    let {userId} = this.props.userId;
    this.props[this.props.requestType]('default',1,userId);
  }

  componentWillUpdate(nextProps, nextState){
    console.log('componentWillUpdate');
  }



  render() {


   let payload = (this.state && this.state.payload) || { data: [] };


   console.log(payload);



      return (

        <View>
          <Text style={{ marginLeft: 10,marginTop:10,marginBottom:10, fontWeight: 'bold' }}>
            {this.props.title}
          </Text>








        </View>
      );

  }
}



class OpenChallengesView extends React.Component {


  render() {
    console.log(this.props);
    return (
      <ScrollView style={styles.mainContainer}>
      <MemberProfileComponent userId={{userId:this.props.userId}}/>
      <MemberOpenChallengeView userId={{userId:this.props.userId}}/>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    fetching: state[props.stateName].fetching,
    payload: state[props.stateName].payload
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getMemberOpenChallenges: (default_type = 'default', page = 1,userid = "") =>{
     console.log(default_type,page,userid);
      return dispatch(MemberOpenChallengesActions.memberOpenChallengesRequest(default_type,page,userid))},

  };
};

const ConnectedChallengeList = connect(mapStateToProps, mapDispatchToProps)(
  ChallengeList
);

export default OpenChallengesView;
