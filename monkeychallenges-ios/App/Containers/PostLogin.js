// @flow

import React from 'react'
import {ScrollView, Text, KeyboardAvoidingView, Image, ListView, View} from 'react-native'
import {connect} from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import ChallengeListActions from '../Redux/ChallengeListRedux'
import TrendingActions from '../Redux/TrendingRedux'
import OpenFundingActions from '../Redux/OpenFundingRedux'
import JudgingChallengesActions from '../Redux/JudgingChallengesRedux'
import OpenChallengesActions from '../Redux/OpenChallengesRedux'
import {Metrics, Images} from '../Themes'

// external libs
import Icon from 'react-native-vector-icons/FontAwesome'
import Animatable from 'react-native-animatable'
import {Actions as NavigationActions} from 'react-native-router-flux'

// Styles
import styles from './Styles/PostLoginStyle'
// button
import RoundedButton from '../Components/RoundedButton'
// I18n
import I18n from 'react-native-i18n'

// progressBar
import * as Progress from 'react-native-progress'

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'

type PostLoginProps = {
    dispatch: () => any,
    fetching: boolean,
    getChangeList: () => void
}

type ChallengeListProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,
    getTrending: () => void
}

// ======================= open Challenge view

class OpenChallengeView extends React.Component {
  render () {
    let itemData = this.props.itemData

    return <View style={styles.challenge}>
      <View style={styles.imageSection}>
        <View style={{height: 180 }}>
          <Image style={{height: 180 }} source={{uri: itemData.challenge_media[0].path}} />
        </View>

        <View style={styles.imageFooter}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Progress.Bar progress={(Math.round(itemData.percentage_funded) / 100)} color={'#FFA500'} height={11} width={250} borderColor={'#CCCCCC'} />

          </View>
          <View style={styles.contentVoteSection}>
            <Text style={{color: 'white' }}>$ {itemData.price} USD</ Text>
            <Text style={{color: 'white'}}>{Math.round(itemData.percentage_funded)}%</Text>
            <Text style={{color: 'white', textAlign: 'right'}}>{itemData.days_remaining} days left</Text>
          </View>
        </View>
        <View style={styles.imageFooterOpacity} />
      </View>
      <View style={styles.contentSection}>
        <View style={styles.contentDescriptionSection}>
          <Text numberOfLines={3}><Text style={{color: '#337ab7'}}>{itemData.handle_name} : </Text>{itemData.description}</Text>
        </View>
      </View>
    </View>
  }
}

// ======================= trending Challenge view named as a featured challenges

class TrendingChallengeView extends React.Component {
  render () {
    let itemData = this.props.itemData
    return <View style={styles.challenge}>
      <View style={styles.imageSection}>
        <View style={{height: 180 }}>
          <Image style={{height: 180 }} source={{uri: itemData.challenge_media[0].path}} />
        </View>

        <View style={styles.imageFooter}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Progress.Bar progress={(Math.round(itemData.percentage_funded) / 100)} color={'#FFA500'} height={11} width={250} borderColor={'#CCCCCC'} />
          </View>
          <View style={styles.contentVoteSection}>
            <Text style={{color: 'white' }}>$ {itemData.price} USD</ Text>
            <Text style={{color: 'white'}}>{Math.round(itemData.percentage_funded)}%</Text>
            <Text style={{color: 'white', textAlign: 'right'}}>{itemData.days_remaining} days left</Text>
          </View>
        </View>
        <View style={styles.imageFooterOpacity} />
      </View>
      <View style={styles.contentSection}>
        <View style={styles.contentDescriptionSection}>
          <Text numberOfLines={3}><Text style={{color: '#337ab7'}}>{itemData.handle_name} : </Text>{itemData.description}</Text>
        </View>
      </View>
    </View>
  }
}

// ======================== Judging Challenges

class ChallengeView extends React.Component {
  render () {
    let itemData = this.props.itemData
    return <View style={styles.challenge}>
      <View style={styles.imageSection}>
        <View style={{height: 180 }}>
          <Image style={{height: 180 }} source={{uri: itemData.challenge_media[0].path}} />
        </View>
        <View style={styles.imageFooter}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Progress.Bar progress={(Math.round(itemData.percentage_funded) / 100)} color={'#FFA500'} height={11} width={250} borderColor={'#CCCCCC'} />
          </View>
          <View style={styles.contentVoteSection}>
            <Text style={{color: 'white' }}>$ {itemData.price} USD</ Text>
            <Text style={{color: 'white'}}>{Math.round(itemData.percentage_funded)}%</Text>
            <Text style={{color: 'white', textAlign: 'right'}}>{itemData.days_remaining} days left</Text>
          </View>
        </View>
        <View style={styles.imageFooterOpacity} />
      </View>

      <View style={styles.contentSection}>
        <View style={styles.contentDescriptionSection}>
          <Text numberOfLines={3}><Text style={{color: '#337ab7'}}>{itemData.handle_name} : </Text>{itemData.description}</Text>
        </View>
        <RoundedButton>
            WATCH VIDEO TO JUDGE
        </RoundedButton>

      </View>
    </View>
  }
}

class ChallengeList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      loaded: false
    }
  }
  props:ChallengeListProps
  componentWillReceiveProps (newProps) {
    if (!newProps.fetching) {
      this.setState({payload: newProps.payload})
    }
  }
  componentWillMount () {
    this.props[this.props.requestType]()
  }

  render () {
    if (this.props.title == 'Open Challenges') {
      let payload = this.state && this.state.payload || {data: []}
      console.log(payload.data, 'playload.data', this.props.title)
      return <View style={styles.challengeListContainer}>
        <Text style={styles.boldLabel}>{this.props.title}</Text>
        <ScrollView horizontal>
          {
                        payload.data.map((rowData, index) => {
                          return <OpenChallengeView itemData={rowData} key={index} />
                        })

            }
        </ScrollView>
      </View>
    } else if (this.props.title == 'Featured Challenges') {
      let payload = this.state && this.state.payload || {data: []}
      console.log(payload.data, 'playload.data', this.props.title)
      return <View style={styles.challengeListContainer}>
        <Text style={styles.boldLabel}>{this.props.title}</Text>

        <ScrollView horizontal>
          {
                        payload.data.map((rowData, index) => {
                          return <TrendingChallengeView itemData={rowData} key={index} />
                        })

            }
        </ScrollView>
      </View>
    } else {
      let payload = this.state && this.state.payload || {data: []}
      console.log(payload.data, 'playload.data', this.props.title)
      return <View style={styles.challengeListContainer}>
        <Text style={styles.boldLabel}>{this.props.title}</Text>
        <ScrollView horizontal>
          {
                        payload.data.map((rowData, index) => {
                          return <ChallengeView itemData={rowData} key={index} />
                        })

            }
        </ScrollView>
      </View>
    }
  }
}

class PostLogin extends React.Component {

  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <ConnectedChallengeList stateName='trending' requestType='getTrending' title='Featured Challenges' />
          <ConnectedChallengeList stateName='judgingChallenges' requestType='getJudgingChallenges' title='Judging Challenges' />
          <ConnectedChallengeList stateName='openChallenges' requestType='getOpenChallenges' title='Open Challenges' />
          <ConnectedChallengeList stateName='openFunding' requestType='getOpenFunding' title='Open Funding' />
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    fetching: state[props.stateName].fetching,
    payload: state[props.stateName].payload
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getTrending: () => dispatch(TrendingActions.trendingRequest()),
    getOpenFunding: () => dispatch(OpenFundingActions.openFundingRequest()),
    getJudgingChallenges: () => dispatch(JudgingChallengesActions.judgingChallengesRequest()),
    getOpenChallenges: () => dispatch(OpenChallengesActions.openChallengesRequest())
  }
}

const ConnectedChallengeList = connect(mapStateToProps, mapDispatchToProps)(ChallengeList)
export default PostLogin
