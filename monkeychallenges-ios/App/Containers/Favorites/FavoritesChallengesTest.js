// @flow

import React from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Image, ListView, View, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)

//import OpenChallengesActions from '../../Redux/OpenChallengesRedux'

import ChallengeListActions from '../../Redux/ChallengeListRedux'
import TrendingActions from '../../Redux/TrendingRedux'
import OpenFundingActions from '../../Redux/OpenFundingRedux'
import JudgingChallengesActions from '../../Redux/JudgingChallengesRedux'
import OpenChallengesActions from '../../Redux/OpenChallengesRedux'


import { Metrics, Images } from '../../Themes'

// external libs
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions as NavigationActions } from 'react-native-router-flux'
import Animatable from 'react-native-animatable'

// Styles
//import styles from './Styles/OpenChallengesViewStyle'
//import styles from './Styles/JudgingChallengesViewStyle'
import styles from '../Styles/FavoritesChallengesStyle'

// alert message
import AlertMessage from '../../Components/AlertMessage'

// I18n
import I18n from 'react-native-i18n'

// progressBar
import * as Progress from 'react-native-progress'

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'


type ChallengeListProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,
    getTrending: () => void
}

// ======================= open Challenge view

class OpenChallengeView extends React.Component {
  render () {
    let itemData = this.props.itemData

    return <TouchableOpacity style={styles.challenge}>
      <View style={styles.imageSection}>
        <View style={{height: 100 }}>
          <Image style={{height: 100 }} source={{uri: itemData.challenge_media[0].path}} defaultSource={require('../../Images/monkey_default.png')} />
        </View>
      </View>
    </TouchableOpacity>
  }
}

// ======================= trending Challenge view named as a featured challenges

class TrendingChallengeView extends React.Component {
  render () {
    let itemData = this.props.itemData
    return <TouchableOpacity style={styles.challenge}>
      <View style={styles.imageSection}>
        <View style={{height: 100 }}>
          <Image style={{height: 100 }} source={{uri: itemData.challenge_media[0].path}} defaultSource={require('../../Images/monkey_default.png')} />
        </View>
      </View>
    </TouchableOpacity>
  }
}

// ======================== Judging Challenges

class ChallengeView extends React.Component {
  render () {
    let itemData = this.props.itemData
    return <TouchableOpacity style={styles.challenge}>
      <View style={styles.imageSection}>
        <View style={{height: 100 }}>
          <Image style={{height: 100 }} source={{uri: itemData.challenge_media[0].path}} defaultSource={require('../../Images/monkey_default.png')} />
        </View>
      </View>
    </TouchableOpacity>
  }
}

class ChallengeList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      loaded: false
    }
  }
  props:ChallengeListProps
  componentWillReceiveProps (newProps) {
    if (!newProps.fetching) {
      this.setState({payload: newProps.payload})
    }
  }
  componentWillMount () {
    this.props[this.props.requestType]()
  }

  render () {
    if (this.props.title == 'Open Challenges') {
      let payload = this.state && this.state.payload || {data: []}
      console.log(payload.data, 'playload.data', this.props.title)
      return <View style={styles.challengeListContainer}>
      
        <Text style={styles.boldLabel}>{this.props.title}</Text>
      <View style={{flex:1,flexDirection: 'row',
    flexWrap: 'wrap',}}>
          {
                        payload.data.map((rowData, index) => {
                          return <OpenChallengeView itemData={rowData} key={index} />
                        })

            }
      </View>
      </View>
    } else if (this.props.title == 'Featured Challenges') {
      let payload = this.state && this.state.payload || {data: []}
      console.log(payload.data, 'playload.data', this.props.title)
      return <View style={styles.challengeListContainer}>
      { this.state.fetching &&
        <GiftedSpinner />
      }
        <Text style={styles.boldLabel}>{this.props.title}</Text>

        <View style={{flex:1,flexDirection: 'row',
    flexWrap: 'wrap',}}>
          {
                        payload.data.map((rowData, index) => {
                          return <TrendingChallengeView itemData={rowData} key={index} />
                        })

            }
         </View>
      </View>
    } else {
      let payload = this.state && this.state.payload || {data: []}
      console.log(payload.data, 'playload.data', this.props.title)
      return <View style={styles.challengeListContainer}>
      { this.state.fetching &&
        <GiftedSpinner />
      }
        <Text style={styles.boldLabel}>{this.props.title}</Text>
        <View style={{flex:1,flexDirection: 'row',
    flexWrap: 'wrap',}}>
          {
                        payload.data.map((rowData, index) => {
                          return <ChallengeView itemData={rowData} key={index} />
                        })

            }
         </View>
      </View>
    }
  }
}

class FavoritesChallengesTest extends React.Component {

  render () {
    return (
       <View style={styles.mainDoubleNavContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
         <ConnectedChallengeList stateName='favoritesChallenges' requestType='getOpenChallenges' title='Open' />
        
            </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    fetching: state[props.stateName].fetching,
    payload: state[props.stateName].payload
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getOpenChallenges: () => dispatch(OpenChallengesActions.openChallengesRequest())
  }
}


const ConnectedChallengeList = connect(mapStateToProps, mapDispatchToProps)(ChallengeList)
export default FavoritesChallengesTest

