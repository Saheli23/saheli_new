// @flow
import React from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Image, ListView, View, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)

//import OpenChallengesActions from '../../Redux/OpenChallengesRedux'

//import OpenChallengesActions from '../../Redux/OpenChallengesRedux'
import FavoritesMembersActions from '../../Redux/FavoritesMembersRedux'

import { Metrics, Images, Colors } from '../../Themes'

// external libs
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions as NavigationActions } from 'react-native-router-flux'
import Animatable from 'react-native-animatable'

// Styles
//import styles from './Styles/OpenChallengesViewStyle'
//import styles from './Styles/JudgingChallengesViewStyle'
import styles from '../Styles/FavoritesChallengesStyle'

// alert message
import AlertMessage from '../../Components/AlertMessage'

// I18n
import I18n from 'react-native-i18n'

// progressBar
import * as Progress from 'react-native-progress'

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'


type ChallengeListProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,
    getFavoritesMembers: () => void
}

class OpenChallengeView extends React.Component {

  handleChallengesView(itemData) {


   // NavigationActions.OpenChallengeDetailView({item:itemData})
        //this.props.levelSelected(levelNumber);
  }
  render () {
    let itemData = this.props.itemData
    //console.log(itemData);
    return <TouchableOpacity style={{padding:5,flexDirection: 'row',marginTop:5}}
    onPress={()=>{
      NavigationActions.memberProfile({ userId: itemData.uuid })
    }}>
    <View>

    <Image style={{ height: 50, width:50, borderRadius:25 }}
        source={{uri: itemData.updated_member.path}}
        defaultSource={require('../../Images/person.png')} />


        </ View>
        <View style={{paddingLeft:10,paddingBottom:5,flexDirection: 'column',alignItems: 'flex-start',position: 'absolute',left:50,right:0,top:5,borderBottomWidth: 1.5,
    borderColor: Colors.steel,}}>
    <TouchableOpacity onPress={()=>{
      this.props.onPressUnfav(itemData.uuid);
      //console.log(itemData.uuid);

    //  this.props.getFavoritesMembers('UnFavourite',itemData.uuid);
    }} style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
    <Icon name='heart' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
    </TouchableOpacity>
{/*
            <Text style={{fontSize:12,color: '#9999A2',position: 'absolute',right:0}}>love</Text>
            */}
            <Text style={{fontWeight:'bold'}}>{itemData.handle_name}</Text>
            <Text style={{fontSize:12,color: '#1266AF'}}>{itemData.username}</Text>
            <Text style={{fontSize:12,marginTop:5,fontWeight:'bold'}}><Text style={{fontSize:12,color: '#8C8C8C',fontWeight:'bold'}}>CHALLENGES</ Text> {itemData.total_challenges} OPEN</Text>

        </View>

    </TouchableOpacity>
  }
}



class ChallengeList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      loaded: false,
    }
  }
  props: ChallengeListProps
  componentWillReceiveProps (newProps) {
    console.log(newProps);
    if (!newProps.fetching) {
      //console.log(newProps);
      this.setState({fetching:newProps.fetching, payload: newProps.payload,defaultfetching:newProps.defaultfetching})
    }else{
      // console.log(newProps);
       this.setState({fetching:newProps.fetching,defaultfetching:newProps.defaultfetching, payload: newProps.payload})
    }
  }

  componentWillMount () {
      this.props[this.props.requestType]()
  }

  handleUnfav = (uuid) => {
      //console.log(uuid);
     this.props.getFavoritesMembers('UnFavourite',uuid);
  }


  render () {

       let payload = this.state && this.state.payload || {data: []}
      console.log(payload, 'playload', this.props.title)
      return <View style={styles.challengeListContainer}>

      { this.state.fetching &&
        <GiftedSpinner />
      }
      { !this.state.fetching && !this.state.defaultfetching && payload.data.length == 0 &&
         <AlertMessage title='No Favorite Members to see, Move Along' show={(payload.data.length == 0)} />
      }
      { this.state.defaultfetching &&
       <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:(Platform.OS === 'ios') ? 9999 : 0,justifyContent: 'center',alignItems: 'center',}}>
      <GiftedSpinner size='large' />
      </View>
      }

 { !this.state.fetching &&

        <ScrollView vertical>

          {

                        payload.data.map((rowData, index) => {
                          if(rowData.user_media.length > 0){
                            updatedrowData = Object.assign(rowData,{"updated_member":rowData.user_media[(rowData.user_media.length - 1)]})

                          }else{
                            updatedrowData = Object.assign(rowData,{"updated_member":{"path":'../Images/person.png'}})
                          }
                          return <OpenChallengeView onPressUnfav={(uuid)=>{
                            this.handleUnfav(uuid);
                          }} itemData={updatedrowData} key={index} />
                        })

            }
        </ScrollView>
}

      </View>

  }
}

class FavoritesMembersView extends React.Component {

  render () {
    return (
       <View style={styles.mainDoubleNavContainer}>
        <ScrollView style={styles.container}>
          <ConnectedChallengeList stateName='favoritesMembers' requestType='getFavoritesMembers' title='Favorites Members' />
            </ScrollView>
      </View>
    )
  }

}

const mapStateToProps = (state, props) => {
  return {
    fetching: state[props.stateName].fetching,
    payload: state[props.stateName].payload,
    defaultfetching: state[props.stateName].defaultfetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getFavoritesMembers: (default_type = 'default', followsUuid = null) =>{
      console.log(default_type,followsUuid);
      return dispatch(FavoritesMembersActions.favoritesMembersRequest(default_type,followsUuid))
    }

  }
}


const ConnectedChallengeList = connect(mapStateToProps, mapDispatchToProps)(ChallengeList)
export default FavoritesMembersView
