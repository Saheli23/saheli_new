// @flow

import React from 'react';
import {
  ScrollView,
  Text,
  KeyboardAvoidingView,
  Image,
  ListView,
  View,
  Dimensions,
  TouchableOpacity,
  AsyncStorage,
  Platform

} from 'react-native';
import { connect } from 'react-redux';

// Add Actions - replace 'Your' with whatever your reducer is called :)
import FavoritesChallengesActions from '../../Redux/FavoritesChallengesRedux';

import { Metrics, Images } from '../../Themes';
 const { width, height } = Dimensions.get('window')

// external libs
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions as NavigationActions } from 'react-native-router-flux';
import Animatable from 'react-native-animatable';

// Styles
import styles from '../Styles/FavoritesChallengesStyle';

// alert message
import AlertMessage from '../../Components/AlertMessage';

// I18n
import I18n from 'react-native-i18n';

// progressBar
import * as Progress from 'react-native-progress';

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner';
import * as _ from 'lodash';

type ChallengeListProps = {
  dispatch: () => any,
  fetching: boolean,
  payload: object,
  getTrending: () => void
};


class ChallengeList extends React.Component {
  constructor(props) {
    super(props);
    const rowHasChanged = (r1, r2) => r1 !== r2
    const sectionHeaderHasChanged = (s1, s2) => s1 !== s2
    this.state = {
      loaded: false,
      dataSource: new ListView.DataSource({rowHasChanged, sectionHeaderHasChanged}),
      user:{}
    };
  }
  props: ChallengeListProps;
  componentWillReceiveProps(newProps) {
    console.log(newProps);
    this.setState({ payload: newProps.payload,
      defaultfetching:newProps.defaultfetching,
        fetching:newProps.fetching,
    dataSource: this.state.dataSource.cloneWithRowsAndSections(newProps.payload)
   });
  }
  componentWillMount() {
      this.getMYuuid();
    this.props[this.props.requestType]();

  }
  getMYuuid() {
      AsyncStorage.getItem('MCUserObj').then((value) =>{
        let userObj = JSON.parse(value)
        this.setState({user:userObj});
      })
   }
  // handleOpenChallenge = itemData => {
  //   console.log(itemData);
  //   NavigationActions.OpenChallengeDetailView({ item: itemData });
  // };
  // handleTrendFeatureChallenge = itemData => {
  //   console.log(itemData);
  //   NavigationActions.PopularChallengeDetailView({ item: itemData });
  // };
  // handleJudgingChallenge = itemData => {
  //   console.log(itemData);
  //   NavigationActions.JudgingChallengeDetailView({ item: itemData });
  // };
  handleUnfavorit = (id) => {
    this.props.getFavoritesChallenges('UnFavourite',id);
  }

  pressHandlerOpen = (rowData) => {
    let {user:{uuid}} = this.state;
    switch (rowData.status) {
case 'judge-open':
NavigationActions.JudgingChallengeDetailView({ item: rowData });
break;
case 'judge-vote':
NavigationActions.JudgingChallengeDetailView({ item: rowData });
break;
case 'judge-result':
NavigationActions.JudgingChallengeDetailView({ item: rowData });
break;
case 'win':
NavigationActions.OpenChallengeDetailView({ item: rowData });
break;
case 'pending':
NavigationActions.ChallengeDetailView({ item: rowData });
break;
case 'open':
NavigationActions.OpenChallengeDetailView({ item: rowData,getOpenChallenges: mapDispatchToProps,userId:uuid });
break;
case 'fund':
NavigationActions.OpenChallengeDetailView({ item: rowData });
break;
case 'fail':
NavigationActions.OpenChallengeDetailView({ item: rowData });
break;
default:
NavigationActions.OpenChallengeDetailView({ item: rowData });
    }
  }

  renderRow = (rowData, sectionID) => {
    // You can condition on sectionID (key as string), for different cells
    // in different sections
    return (
      <TouchableOpacity
        style={styles.challenge}
        onPress={() => {
          console.log(rowData.status);
          this.pressHandlerOpen(rowData)
        }}>
        <View style={styles.imageSection}>

          <Image
            style={styles.image}
            source={{ uri: rowData.challenge_media[0].path }}
            defaultSource={require('../../Images/monkey_default.png')}
          />
          <TouchableOpacity onPress={()=>{
            this.handleUnfavorit(rowData._id)
            //this.props.onPressUnFavorive()
          }} style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
          <Icon name='heart' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>


    )
  }

  renderHeader (data, sectionID) {
    console.log(data);
    console.log(sectionID);
    switch (sectionID) {
      case 'judge-open':
      return <View style={{height:20,width:width,marginBottom:5}}><Text style={styles.boldLabel}>Judging</Text></View>
      case 'judge-vote':
      return <View style={{height:20,width:width,marginBottom:5,marginTop:5}}><Text style={styles.boldLabel}>Judge-vote</Text></View>
      case 'judge-result':
      return <View style={{height:20,width:width,marginBottom:5,marginTop:5}}><Text style={styles.boldLabel}>Judge-result
      </Text></View>
      case 'win':
      return <View style={{height:20,width:width,marginBottom:5,marginTop:5}}><Text style={styles.boldLabel}>Result-Won
      </Text></View>
      case 'pending':
      return <View style={{height:20,width:width,marginBottom:5,marginTop:5}}><Text style={styles.boldLabel}>Pending
      </Text></View>
      case 'open':
      return <View style={{height:20,width:width,marginBottom:5,marginTop:5}}><Text style={styles.boldLabel}>Open
      </Text></View>
      case 'fund':
      return <View style={{height:20,width:width,marginBottom:5,marginTop:5}}><Text style={styles.boldLabel}>Fund</Text></View>
      case 'fail':
      return <View style={{height:20,width:width,marginBottom:5,marginTop:5}}><Text style={styles.boldLabel}>Result-Failed</Text></View>
      default:
      return <View style={{height:20,width:width,marginBottom:5,marginTop:5}}><Text style={styles.boldLabel}>Others</Text></View>
    }
  }
  render() {

    return (
      <View style={styles.challengeListContainer}>
      { this.state.defaultfetching &&
       <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:(Platform.OS === 'ios') ? 9999 : 0,justifyContent: 'center',alignItems: 'center',}}>
        <GiftedSpinner size='large' />
      </View>
      }


      { this.state.fetching &&
        <GiftedSpinner />
      }
        <ListView
          renderSectionHeader={this.renderHeader}
          contentContainerStyle={styles.listContent}
          dataSource={this.state.dataSource}
          renderRow={this.renderRow}
        />
      </View>
    );




  }
}

class FavoritesChallenges extends React.Component {
  render() {
    return (
      <View style={styles.mainDoubleNavContainer}>
        <ScrollView style={styles.container}>
          <ConnectedChallengeList
            stateName="favoritesChallenges"
            requestType="getFavoritesChallenges"
            title="Open"
          />
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state, props) => {
console.log(state);
  return {
    fetching: state[props.stateName].fetching,
    payload:(state[props.stateName].payload == null || state[props.stateName].payload == undefined )? [] : _.groupBy(state[props.stateName].payload.challenges ,function(group) {
      return group.status
    }),
    defaultfetching: state[props.stateName].defaultfetching
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getFavoritesChallenges: (default_type = 'default',challengeId = null) =>{
      console.log(default_type,challengeId)
      return dispatch(FavoritesChallengesActions.favoritesChallengesRequest(default_type,challengeId))
      }
  };
};

const ConnectedChallengeList = connect(mapStateToProps, mapDispatchToProps)(
  ChallengeList
);
export default FavoritesChallenges;
