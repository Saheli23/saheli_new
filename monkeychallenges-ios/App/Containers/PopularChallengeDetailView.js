

// @flow

import React from 'react'
import {StyleSheet, ScrollView, Text, KeyboardAvoidingView, Image, ListView, View, Platform, TouchableOpacity, Animated,Dimensions } from 'react-native'
const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import { Actions as NavigationActions } from 'react-native-router-flux'
import RoundedButton from '../Components/RoundedButton'

// progressBar
import * as Progress from 'react-native-progress'


// detail view components
import {StoryPagerView, CommentsPagerView, FundersPagerView} from './DetailComponents/PagerItemView'


import {IndicatorViewPager, PagerTitleIndicator} from 'rn-viewpager'

import NavItems from '../Navigation/NavItems'

import { Colors, Metrics, Images } from '../Themes'
// Styles
//import styles from './Styles/ChallengeDetailViewStyle'
import styles from './Styles/OpenChallengesDetailViewStyle'

import AlertMessage from '../Components/AlertMessage'
import Icon from 'react-native-vector-icons/FontAwesome'

let moment = require('moment');


//TitleIndicatorPage
//PopularChallengeDetailView

class FooterBar extends React.Component{

  render(){
    let itemData = this.props.itemData

    let startTime,
    endTime,
    duration,
    days,
    hours,
    mins;
    update = () => {
    console.log("triggered")
      startTime = moment(new Date()),
      endTime = moment(itemData.funding_expiration_date),
      duration = moment.duration(endTime.diff(startTime)),
      days = moment().isAfter(itemData.funding_expiration_date)? 0 : duration.days(),
      hours = moment().isAfter(itemData.funding_expiration_date)? 0 : duration.hours(),
      mins = moment().isAfter(itemData.funding_expiration_date)? 0 : duration.minutes();
    }

    update();
  //  setInterval(update, 1000);



    console.log(itemData.funding_expiration_date);
    console.log(moment.duration(endTime.diff(startTime,'days')),hours,mins);
    return <View>

    <View style={styles.FooterBarTopBar}>
        <View style={styles.FooterBarTopBarInsideView}>
            <Text>Days</Text>
            <Text style={styles.FooterBarTopBarInsideText}>{days}</Text>
        </View>
        <View style={styles.FooterBarTopBarInsideView}>
            <Text>Hours</Text>
            <Text style={styles.FooterBarTopBarInsideText}>{hours}</Text>
        </View>
        <View style={styles.FooterBarTopBarInsideView}>
            <Text>Minutes</Text>
            <Text style={styles.FooterBarTopBarInsideText}>{mins}</Text>
        </View>
    </View>
    <View style={{height: 50,
        backgroundColor: 'white',
        marginBottom: 0,
        flexDirection: 'row'
        ,alignItems: 'center',justifyContent: 'space-around',}}>
        <View style={{
        flexDirection: 'column'
        ,alignItems: 'center',justifyContent: 'center',marginRight:10}}>
        <Text>Funding has ended.</Text>
        <Text>Stay tuned to watch this challenge!</Text>
        </ View>
        <TouchableOpacity style={{}}>
        <Image style={{width:30,height:30,marginRight:10}} source={Images.share}/>
        {/*
            <Icon name='share' size={Metrics.icons.medium} color={Colors.steel} style={{color: Colors.steel, marginRight: Metrics.smallMargin}} />
              */}
        </TouchableOpacity>

    </View>
</View>
  }
}


class FunderView extends React.Component {
  render(){
     let itemData = this.props.itemData
    return <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: itemData.uuid })}} style={{width:(width - 20),flexDirection:'row',alignSelf:'center',paddingTop:5,marginTop:5,marginBottom:5}}>
    <View>
    <Image style={{ height: 50, width:50, borderRadius:25 }}
        source={{uri: itemData.latest_profile_image}}
        defaultSource={require('../Images/person.png')} />
    </ View>
    <View  style={{flex:1,flexDirection:'row',justifyContent: 'space-between',paddingLeft:5,}}>
        <View style={{width:120,flexDirection:'column',alignItems: 'flex-start',}}>
          <View><Text style={{color: '#1266AF',fontWeight:'bold',}}>{itemData.handle_name}</Text></View>
          <Text style={{fontSize:10,color:'#1266AF'}}>{itemData.username}</Text>
        </View>
        <View style={{flexDirection:'column',alignItems: 'flex-end', justifyContent: 'space-between',}}>
        <Text style={{fontWeight:'bold'}}>${itemData.amount_contributed}</Text>
      <Text style={{fontSize:12,color:'#9999A2'}}>{moment(itemData.created_at).startOf('hour').fromNow()}</Text>

        </View>
    </ View>


    </TouchableOpacity>
  }
}


class CommentView extends React.Component {
  render(){
     let itemData = this.props.itemData
    return <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: itemData.uuid })}} style={{width:(width - 20),flexDirection:'row',alignSelf:'center',paddingTop:5,marginTop:5,marginBottom:5}}>
    <View>
    <Image style={{ height: 50, width:50, borderRadius:25 }}
        source={{uri: itemData.latest_profile_image}}
        defaultSource={require('../Images/person.png')} />
    </ View>
      <View style={{paddingLeft:10,flexDirection:'column',alignItems: 'flex-start',position:'absolute',left:50,right:0,top:5}}>
       <Text style={{fontSize:12,color:'#9999A2',position:'absolute',right:0}}>{moment(itemData.created_at).startOf('hour').fromNow()}</Text>
        <View><Text style={{color: '#1266AF',fontWeight:'bold',}}>{itemData.handle_name}</Text></View>
        <Text style={{fontSize:10,color:'#1266AF'}}>{itemData.username}</Text>
        <Text style={{fontSize:12,marginTop:5}}>{itemData.comment}</Text>
        </View>
    </TouchableOpacity>
  }
}

class StoryView extends React.Component {
  render(){
     let itemData = this.props.itemData
    return <ScrollView vertical>
    <View style={{width:(width - 20), alignSelf:'center'}}>
    <View style={{paddingTop:10}}>
        <Text style={{fontSize:12,color:'#878787',fontWeight:'bold'}}>Posted : {moment(itemData.created_at.date).format("MMMM Do, YYYY")}</Text>
    </View>
    <View style={{paddingTop:10,flexDirection: 'row', flexWrap: 'wrap',}}>
        <Text style={{color: 'black',fontSize:13}}>
            <Text style={{color: '#f9642f',fontWeight:'bold'}}>Challenge description : </Text>{itemData.description}</Text>
    </View>
    <View style={{paddingTop:10,flexDirection: 'row', flexWrap: 'wrap'}}>
        <Text style={{color: 'black',fontSize:13}}>
            <Text style={{color: '#f9642f',fontWeight:'bold'}}>What to judge this challenge on : </Text>{itemData.description_3}</Text>
    </View>
    <View style={{paddingTop:10,flexDirection: 'row', flexWrap: 'wrap'}}>
        <Text style={{color: 'black',fontSize:13}}>
            <Text style={{color: '#f9642f',fontWeight:'bold'}}>What to expect with this challenge result : </Text>{itemData.description_2}</Text>
    </View>
    </View>
</ScrollView>

  }
}


class PopularChallengeDetailView extends React.Component{
    animationstate = {
        bgColor: new Animated.Value(0)
    };

   constructor (props) {
    super(props)
    this.state = {}
  }

  getDateFormet(date){
    return new Date(date);

  }

  componentWillReceiveProps (newProps) {
    console.log(newProps);
  }
    _setBgColor = Animated.event([{bgColor: this.animationstate.bgColor}]);


    render() {
      let itemData = this.props.item

    // let itemData.created_at.date = this.props.item.created_at.date
      console.log(itemData);
        let bgColor = this.animationstate.bgColor.interpolate({
            inputRange: [0, 1, 2],
            outputRange: ['hsl(187, 74%, 47%)', 'hsl(89, 47%, 54%)', 'hsl(12, 97%, 59%)']
        });
        return (
     <View style={styles.mainContainer}>
            <ScrollView vertical>
               <ChallengeViewPreState itemData={itemData} />
                <IndicatorViewPager
                    style={{flex: 1, flexDirection: 'column-reverse',minHeight:400}}
                    indicator={this._renderTitleIndicator(itemData)}
                    onPageScroll={this._onPageScroll.bind(this)}
                >
          <View style={{ flex:1,}}>
           <StoryView itemData={itemData} />
          </View>



         <View style={{ flex:1}}>
         {itemData.challenge_comments.length < 1 &&

            <AlertMessage title='No comments Here, Move Along' show={(itemData.challenge_comments.length < 1)} />

         }

            {
                        itemData.challenge_comments.map((rowData, index) => {
                          return <CommentView itemData={rowData} key={index} />
                        })
            }

          </View>

          <View style={{ flex:1}}>
          {itemData.challenge_challengers.length < 1 &&

             <AlertMessage title='No Funders Here, Move Along' show={(itemData.challenge_challengers.length < 1)} />

          }

            {
                        itemData.challenge_challengers.map((rowData, index) => {
                          return <FunderView itemData={rowData} key={index} />
                        })
            }

          </View>
  </IndicatorViewPager>


  </ScrollView>
<FooterBar itemData={itemData} />
  </View>


        );
    }




    _renderTitleIndicator(itemData) {
      let arr = [`Story`, `Comments (${itemData.challenge_comments.length})`, `Funders (${itemData.challenge_challengers.length})`];
        return (
            <PagerTitleIndicator
                style={styles.indicatorContainer}
                itemTextStyle={styles.indicatorText}
                selectedItemTextStyle={styles.indicatorSelectedText}
                selectedBorderStyle={styles.selectedBorderStyle}
                itemsContainerStyle={{flex:1}}
                titles={arr}
            />
        );
    }

    _onPageScroll(scrollData) {
        let {offset, position}=scrollData;
        if (position < 0 || position >= 2) return;
        this._setBgColor({bgColor: offset + position});
    }

}


class ChallengeViewPreState extends React.Component{

render () {

  let itemData = this.props.itemData
   return <View style={styles.challenge}>
      <View style={styles.imageSection}>

          <Image style={styles.cardImage} source={{uri: itemData.challenge_media[0].path}} defaultSource={require('../Images/monkey_default.png')} />

        <View style={styles.imageFooter}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Progress.Bar progress={(Math.round(itemData.percentage_funded) / 100)} color={'#33CC32'} height={8} width={(width - 20)} borderWidth={0} unfilledColor={'rgba(128,132,131,0.8)'}/>

          <View style={styles.contentVoteSection}>
          <Text style={{color: 'white',fontWeight:'bold' }}>${parseInt(itemData.total_amount_funded)}<Text style={{fontSize:10}}> OF ${parseInt(itemData.price)} FUNDED</Text></ Text>
          {/*
          <Text style={{color: 'white'}}>{Math.round(itemData.percentage_funded)}%</Text>
          */}
          <Text style={{color: 'white',fontWeight:'bold', textAlign: 'right'}}>{itemData.days_remaining} <Text style={{fontSize:10}}>DAYS LEFT</Text></Text>
          </View>
            </View>
        </View>
        <View style={styles.imageFooterOpacity} />
      </View>
      <View style={styles.contentSection}>

      <View style={styles.contentDescriptionSection}>
             <Text style={{color: '#1266AF',fontWeight:'bold',}}>{itemData.handle_name} : <Text style={{color: '#000000'}}>{itemData.name}</Text></Text>
          <Text style={{fontSize:12}} numberOfLines={3}>{itemData.description}</Text>
      </View>

      </View>
    </View>
  }
}
//TitleIndicatorPage
//PopularChallengeDetailView
class TitleIndicatorPage extends React.Component {

  constructor (props) {
    super(props)
    this.state = {}
  }

  componentWillReceiveProps (newProps) {
    console.log(newProps);
  }


  render () {
 let itemData = this.props.item
    return (
    <View style={styles.mainContainer}>
        <ScrollView vertical style={styles.container}>
          <ChallengeViewPreState itemData={itemData} />

            <TitleIndicatorPage />


         </ScrollView>
      </View>
    )
  }
}


const mapStateToProps = (state) => {
  return {

  }
}

const mapDispatchToProps = (dispatch) => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PopularChallengeDetailView)
