// @flow

import React from 'react'
import {
    View,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    Keyboard,
    LayoutAnimation,
    Dimensions,
    AsyncStorage
} from 'react-native'
const {
  width,
  height
} = Dimensions.get("window");
import {connect} from 'react-redux'
import Styles from './Styles/LoginScreenStyle'
import {Images, Metrics} from '../Themes'
import LoginActions from '../Redux/LoginRedux'
//import FbLoginActions from '../Redux/FbLoginRedux'
import {Actions as NavigationActions} from 'react-native-router-flux'
import I18n from 'react-native-i18n'

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'

const FBSDK = require('react-native-fbsdk')
const {
    LoginButton,
    AccessToken
} = FBSDK

type LoginScreenProps = {
    dispatch: () => any,
    fetching: boolean,
    loginType:string,
    access_token:string,
    attemptLogin: () => void,
    attemptFbLogin:() => void
}

class LoginScreen extends React.Component {

  props: LoginScreenProps

  state: {
        username: string,
        password: string,
        facebook_token: string,
        visibleHeight: number,
        topLogo: {
            width: number
        }
    }

  isAttempting: boolean
  keyboardDidShowListener: Object
  keyboardDidHideListener: Object

  constructor (props: LoginScreenProps) {
    super(props)
    this.state = {
      username: 'saheli@capitalnumbers.com',
      password: '123456789',
      loginType: 'login',
      facebook_token:'',
      visibleHeight: Metrics.screenHeight,
      topLogo: {
      },
    }
    this.isAttempting = false
  }

  componentWillReceiveProps (newProps) {
    //this.forceUpdate()
        // Did the login attempt complete?
    console.log(newProps, 'newProps')
    if (this.isAttempting && !newProps.fetching && newProps.access_token && !newProps.error) {
            // NavigationActions.pop()
      this.setState({fetching:newProps.fetching})

      NavigationActions.postLogin()
    }else if(newProps.error && !newProps.fetching){
      console.log('inside error');
      this.setState({fetching:newProps.fetching})
        alert(newProps.message);

    }else{
      console.log('inside else');
      this.setState({fetching:newProps.fetching})
    }
  }

  componentWillMount () {

        // Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
        // TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)
  }


  componentWillUnmount () {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  keyboardDidShow = (e) => {
        // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    let newSize = Metrics.screenHeight - e.endCoordinates.height
    this.setState({
      visibleHeight: newSize,
      topLogo: {width: 100, height: 70}
    })
  }

  keyboardDidHide = (e) => {
        // Animation types easeInEaseOut/linear/spring

    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      visibleHeight: Metrics.screenHeight,
      topLogo: {width: 100, height: 90,marginTop:30,marginBottom:20}
    })
  }

  handleKeyDown = (e) => {
    if(e.nativeEvent.key == "Enter"){
        dismissKeyboard();
    }
  }

  handlePressLogin = () => {
    this.setState({loginType: 'login'})
    const {username, password, facebook_token, loginType} = this.state
    this.isAttempting = true
    // attempt a login - a saga is listening to pick it up from here.
    console.log(facebook_token);

    this.props.attemptLogin(username, password, facebook_token, loginType)
  }

  handlePressFbLogin = () => {
    //this.setState({loginType:"fblogin"})
     const {username, password, facebook_token, loginType} = this.state
    this.isAttempting = true
    // attempt a login - a saga is listening to pick it up from here.
    console.log(facebook_token);
    this.props.attemptLogin(username, password, facebook_token, loginType)
    // this.isAttempting = true
    // // attempt a login - a saga is listening to pick it up from here.
    // this.props.loginType = "fbLogin"
    // console.log(facebook_token);
    // this.props.attemptFbLogin(facebook_token)
  }


  handleChangeUsername = (text) => {
    this.setState({username: text})
  }

  handleChangePassword = (text) => {
    this.setState({password: text})
  }

  handleForgotPassword = (text) => {
    alert("Forgot Password!")
  }

  render () {

    const {username, password} = this.state
    const {fetching} = this.props
    const editable = !fetching
    const textInputStyle = editable ? Styles.textInput : Styles.textInputReadonly

    return (

      <View style={Styles.mainContainerwithOutNav}>
          <Image source={Images.loginScreenBg} style={Styles.backgroundImageLogin} resizeMode='stretch'/>
              { this.state.fetching &&
                 <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
                    <GiftedSpinner size='large' />
                </View>
              }



        <ScrollView vertical>

          <Image source={Images.logo} style={[Styles.topLogo, this.state.topLogo]}/>

           <View style={{alignItems:'center',paddingBottom:20,}}>
             <Text style={{color:'white',fontWeight:'bold'}}>Log In</ Text>
           </ View>
          <View style={{justifyContent: 'center',
      alignItems: 'center'}}>

                      <LoginButton
                      style={{width:220, height:30}}
                            publishPermissions={['publish_actions']}
                            onLoginFinished={
                                (error, result) => {
                                  if (error) {
                                    console.log('error:', error);
                                  } else if (result.isCancelled) {
                                    alert('login is cancelled.')
                                    console.log('login is cancelled:', result);
                                  } else {

                                    AccessToken.getCurrentAccessToken().then(
                                      (data) => {
                                        //alert(data.accessToken.toString())
                                        console.log('Access_Token:'+data.accessToken.toString())
                                        this.setState({facebook_token: data.accessToken.toString(),loginType:"fblogin"}, () => {
                                        this.handlePressFbLogin()
                                        })
                                      }
                                    )
                                  }
                                }
                              }
                            onLogoutFinished={() => {
                              this.setState({facebook_token: ''})
                               alert('logout.')
                            }}/>
                    </View>

                    <View style={{alignItems:'center',paddingTop:20,paddingBottom:20}}>
                      <Text style={{color:'white',fontSize:10,fontWeight:'bold'}}>Or Log in with your email.</ Text>
                    </ View>


                    <View style={{height: 0.5,backgroundColor: '#FFFFFF',width:width,}} />

                    <View style={{paddingLeft:20,paddingRight:20}}>
                              <View style={[Styles.row]}>
                                <TextInput
                                  ref='username'
                                  style={textInputStyle}
                                  value={username}
                                  editable={editable}
                                  keyboardType='email-address'
                                  returnKeyType='next'
                                  autoCapitalize='none'
                                  autoCorrect={false}
                                  onChangeText={this.handleChangeUsername}
                                  underlineColorAndroid='transparent'
                                  onSubmitEditing={() => this.refs.password.focus()}
                                  placeholderTextColor='#FFFFFF'
                                  placeholder={I18n.t('Email')} />
                              </View>
              <View style={{height: 0.5,backgroundColor: '#FFFFFF',}} />
                  <View style={Styles.row}>
                    <TextInput
                      ref='password'
                      style={textInputStyle}
                      value={password}
                      editable={editable}
                      keyboardType='default'
                      returnKeyType='done'
                      autoCapitalize='none'
                      autoCorrect={false}
                      secureTextEntry
                      onChangeText={this.handleChangePassword}
                      underlineColorAndroid='transparent'
                      onSubmitEditing={this.handleKeyDown }
                      placeholderTextColor='#FFFFFF'
                      placeholder={I18n.t('password')} />
                  </View>
              </View>
<View style={{height: 0.5,backgroundColor: '#FFFFFF',width:width,}} />

          <View style={[Styles.loginRow,{marginTop:30}]}>
            <TouchableOpacity style={[Styles.loginButtonWrapper,Styles.loginButton]} onPress={this.handlePressLogin}>
                <Text style={Styles.loginText}>Log in with Email</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[Styles.loginButtonWrapper,Styles.loginButton]} onPress={NavigationActions.pop}>
                <Text style={Styles.loginText}>{I18n.t('cancel')}</Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity style={{alignItems:'center',}} onPress={this.handleForgotPassword}>
                      <Text style={{color:'white',fontSize:12,fontWeight:'bold'}}>Forgot Password? Reset it</ Text>
          </ TouchableOpacity>

    </ScrollView>
 </View>

    )
  }

}

const mapStateToProps = (state) => {
  console.log(state)
      return {
          fetching: state.login.fetching,
          access_token: state.login.access_token,
          error:state.login.error,
          message:state.login.message
        }

}

const mapDispatchToProps = (dispatch) => {
  return {
    attemptLogin: (username, password, facebook_token, loginType) => dispatch(LoginActions.loginRequest(username, password, facebook_token, loginType)),
    // attemptFbLogin: (facebook_token) => dispatch(FbLoginActions.fbLoginRequest(facebook_token))

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
