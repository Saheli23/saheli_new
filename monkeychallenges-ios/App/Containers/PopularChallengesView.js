// @flow

import React from 'react'
import {
    ScrollView,
    Text,
    KeyboardAvoidingView,
    Image,
    ListView,
    FlatList,
    StyleSheet,
    View,
    TouchableOpacity,
    RefreshControl,
    Dimensions,
    AsyncStorage,
    Platform

   } from 'react-native'
   const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)

import TrendingActions from '../Redux/TrendingRedux'

// alert message
import AlertMessage from '../Components/AlertMessage'

// import {Images} from '../Themes'
import {Metrics, Images} from '../Themes'

// import YourActions from '../Redux/YourRedux'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions as NavigationActions } from 'react-native-router-flux'
import Animatable from 'react-native-animatable'

// Styles
import styles from './Styles/JudgingChallengesViewStyle'
//import styles from './Styles/PostLoginStyle'

// button
import RoundedButton from '../Components/RoundedButton'
// I18n
import I18n from 'react-native-i18n'

// progressBar
import * as Progress from 'react-native-progress'

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'


import Toast, {DURATION} from 'react-native-easy-toast'
import * as _ from 'lodash';
import ProgressiveImage from 'react-native-progressive-image'


type ChallengeListProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,
    getTrending: () => void
}
class ChallengeList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      defaultfetching:false,
      refreshing: false,
      isLoadingTail: false,
      loaded: false,
      // dataSource: new ListView.DataSource({
      //         rowHasChanged: (row1, row2) => row1 !== row2,
      //       }),
      dataSource:[],
      extraData:[],
      category:[],
    }
  }
  props:ChallengeListProps
  componentWillMount () {
    this.getMYuuid();
    this.getChallengeCategoryList();
    console.log("test will mount");
    console.log(this.props);
      this.props[this.props.requestType]()
  }
  componentWillReceiveProps (newProps) {
    console.log(newProps);
    if(newProps.error){
      alert("Error: Something went wrong!");
      this.setState({
        defaultfetching:newProps.defaultfetching,
        refreshing:newProps.refreshing,
        fetching:newProps.fetching,
        isLoadingTail:newProps.isLoadingTail,
        payload:newProps.payload,
        // dataSource: this.state.dataSource.cloneWithRows(newProps.payload.data)
        dataSource: newProps.payload.data
      })
    }else{
      this.setState({
        defaultfetching:newProps.defaultfetching,
        refreshing:newProps.refreshing,
        fetching:newProps.fetching,
        isLoadingTail:newProps.isLoadingTail,
        payload:newProps.payload,
        // dataSource: this.state.dataSource.cloneWithRows(newProps.payload.data)
        dataSource: newProps.payload.data
      })
    }
  }

  getMYuuid() {
      AsyncStorage.getItem('MCUserObj').then((value) =>{
        let userObj = JSON.parse(value)
        this.setState({user:userObj});
      })
   }
   getChallengeCategoryList() {
       AsyncStorage.getItem('MCChallengeCategory').then((value) =>{
         let categoryObj = JSON.parse(value)
         this.setState({category:categoryObj});
       })
    }
  _onRefresh() {
      this.setState({refreshing: true});
      this.props.getTrending('refreshing',1)
  }

  _keyExtractor = (item, index) => item._id;

  renderRow = (rowData) => {
    //console.log('rowData',rowData);
    let {user:{uuid}} = this.state;
    let isFavourite;
    let icon = (rowData.item.challenge_media[0].path == undefined) ? "../Images/monkey_default.png" : rowData.item.challenge_media[0].path;

    let getCategory = _.result(_.find(this.state.category, function(obj) {
    //  console.log(obj);
          return ( rowData.item.challenge_category_type_id == obj.id );
      }), 'name');

    if (rowData.item['challenge_favorite'].length > 0){
    let favourite = _.result(_.find(rowData.item['challenge_favorite'], function(obj) {
          return ( obj['uuid'] == uuid );
      }), '_id');

      // console.log(favourite);
       isFavourite = favourite ? true : false;
      //console.log(isFavourite);

      // console.log("row data length is grater",rowData);
      // console.log(rowData['challenge_favorite'].length);
    }
    return (
      <View style={styles.row}>
      <View style={styles.imageSection}>


        {rowData.item.status == 'open' &&
        <TouchableOpacity onPress={()=>{NavigationActions.OpenChallengeDetailView({item:rowData.item,getTrending: mapDispatchToProps,userId:uuid,populerPageRefresh:'true'})}}>
        {/*<Image style={styles.cardImage} source={{uri:icon}} defaultSource={require('../Images/monkey_default.png')} />*/}
        <ProgressiveImage
          thumbnailSource={require('../Images/monkey_default.png')}
          imageSource={{uri:icon}}
          style={{  height: (width/2),
          width:(width), }}
        />
        </ TouchableOpacity>
        }
        {rowData.item.status == 'pending' &&
        <TouchableOpacity onPress={()=>{NavigationActions.ChallengeDetailView({item:rowData.item})}}>
        {/*<Image style={styles.cardImage} source={{uri:icon}} defaultSource={require('../Images/monkey_default.png')} />*/}
        <ProgressiveImage
          thumbnailSource={require('../Images/monkey_default.png')}
          imageSource={{uri:icon}}
          style={{  height: (width/2),
          width:(width), }}
        />
        </ TouchableOpacity>
        }
        {rowData.item.status == 'judge-vote' &&
        <TouchableOpacity onPress={()=>{NavigationActions.JudgingChallengeDetailView({item:rowData.item})}}>
        {/*<Image style={styles.cardImage} source={{uri:icon}} defaultSource={require('../Images/monkey_default.png')} />*/}
        <ProgressiveImage
          thumbnailSource={require('../Images/monkey_default.png')}
          imageSource={{uri:icon}}
          style={{  height: (width/2),
          width:(width), }}
        />
        </ TouchableOpacity>
        }
        {rowData.item.status != 'judge-vote' && rowData.item.status != 'pending' && rowData.item.status != 'open' &&
        <TouchableOpacity onPress={()=>{NavigationActions.OpenChallengeDetailView({item:rowData.item,userId:uuid,populerPageRefresh:'true',getTrending: mapDispatchToProps})}}>
        {/*<Image style={styles.cardImage} source={{uri:icon}} defaultSource={require('../Images/monkey_default.png')} />*/}
        <ProgressiveImage
          thumbnailSource={require('../Images/monkey_default.png')}
          imageSource={{uri:icon}}
          style={{  height: (width/2),
          width:(width), }}
        />
        </ TouchableOpacity>
        }

        <View style={{position:'absolute',backgroundColor:'black',padding:3,flexDirection:'row',top:10,left:5,borderRadius:5}}>
        <Text style={{color:'#FFFFFF',fontWeight:'bold',fontSize:10}}>{getCategory}</Text>
        </View>

        {/*{(rowData.charity == 1) &&
          <View style={{position:'absolute',backgroundColor:'black',padding:3,flexDirection:'row',top:10,left:5,borderRadius:5}}>
          <Text style={{color:'#FFFFFF',fontWeight:'bold',fontSize:10}}>CHARITY</Text>
          </View>
          }
       */}

          {isFavourite &&
            <TouchableOpacity onPress={()=>{
              this.props.getTrending("UnFavourite",null,rowData.item._id);
            }} style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
            <Icon name='heart' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
            </TouchableOpacity>
          }

          {!isFavourite &&
            <TouchableOpacity onPress={()=>{
              this.props.getTrending("Favourite",null,rowData.item._id);
            }}
             style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
            <Icon name='heart-o' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
            </TouchableOpacity>
          }



        {rowData.item.status == 'open' &&
        <View style={styles.imageFooter}>
          <View style={{alignItems: 'center', justifyContent:'center'}}>
           <Progress.Bar progress={(Math.round(rowData.item.percentage_funded) / 100)} color={'#33CC32'} height={8} width={(width - 20)} borderWidth={0} unfilledColor={'rgba(128,132,131,0.8)'}/>

          <View style={[styles.contentVoteSection]}>
            <Text style={{color: 'white',fontWeight:'bold' }}>${parseInt(rowData.item.total_amount_funded)}<Text style={{fontSize:10}}> OF ${parseInt(rowData.item.price)} FUNDED</Text></ Text>
            {/*
            <Text style={{color: 'white'}}>{Math.round(rowData.percentage_funded)}%</Text>
            */}
            <Text style={{color: 'white',fontWeight:'bold', textAlign: 'right'}}>{rowData.item.days_remaining} <Text style={{fontSize:10}}>DAYS LEFT</Text></Text>
          </View>
        </View>
        </View>
        }
        {rowData.item.status == 'pending' &&
        <View style={styles.imageFooter}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <View style={styles.contentVoteSection}>
          <Text style={{color: 'white',fontWeight:'bold', fontSize:30 }}>${parseInt(rowData.item.price)}<Text style={{fontSize:10}}> FUNDED</Text></ Text>
          {/*
          <Text style={{color: 'white'}}>{Math.round(rowData.percentage_funded)}%</Text>
          */}
            <View style={{justifyContent:'flex-end'}}>
              <Text style={{color: 'white', textAlign: 'right', fontWeight:'bold',fontFamily:'Bradley Hand'}}>Getting ready!</Text>
            </View>

          </View>
          </View>
        </View>
        }
        {rowData.item.status == 'judge-vote' &&
        <View style={styles.imageFooter}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>


          <View style={styles.contentVoteSection}>
          <Text style={{color: 'white',fontWeight:'bold', fontSize:30 }}>${parseInt(rowData.item.price)}<Text style={{fontSize:10}}> FUNDED</Text></ Text>
          {/*
          <Text style={{color: 'white'}}>{Math.round(rowData.percentage_funded)}%</Text>
          */}
          <View style={{flexDirection:'row'}}>
            <Icon name='gavel' size={Metrics.icons.medium} color={'#FFFFFF'} style={{marginRight: Metrics.smallMargin}} />

            <View style={{flexDirection:'column',alignItems:'flex-start'}}>
              <Text style={{color: 'white', textAlign: 'right', fontWeight:'bold',fontFamily:'Bradley Hand'}}>Judging</Text>
              <Text style={{color: 'white',fontFamily:'Bradley Hand', textAlign: 'right',fontWeight:'bold'}}>Challenge</Text>
            </View>

            </View>

          </View>

          </View>

        </View>
        }
        {rowData.item.status != 'judge-vote' && rowData.item.status != 'pending' && rowData.item.status != 'open' &&
        <View style={styles.imageFooter}>
          <View style={{alignItems: 'center', justifyContent:'center'}}>
           <Progress.Bar progress={(Math.round(rowData.item.percentage_funded) / 100)} color={'#33CC32'} height={8} width={(width - 20)} borderWidth={0} unfilledColor={'rgba(128,132,131,0.8)'}/>

          <View style={[styles.contentVoteSection]}>
            <Text style={{color: 'white',fontWeight:'bold' }}>${parseInt(rowData.item.total_amount_funded)}<Text style={{fontSize:10}}> OF ${parseInt(rowData.item.price)} FUNDED</Text></ Text>
            {/*
            <Text style={{color: 'white'}}>{Math.round(rowData.percentage_funded)}%</Text>
            */}
            <Text style={{color: 'white',fontWeight:'bold', textAlign: 'right'}}>{rowData.item.days_remaining} <Text style={{fontSize:10}}>DAYS LEFT</Text></Text>
          </View>
        </View>
        </View>
        }


        <View style={styles.imageFooterOpacity} />
      </View>
      <View style={styles.contentSection}>

      <View style={[styles.contentDescriptionSection,{flexDirection:'column',width:(width - 20),alignSelf:'center'}]}>
    {/*
           <Text style={{color: '#1266AF',fontWeight:'bold',}}>{rowData.handle_name} :
            <Text style={{color: '#000000'}}> {rowData.name}</Text></Text>
            <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: rowData.uuid })}}>
    */}

    <View style={{flexDirection:'row',width:(width - 20),}}>
      <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: rowData.item.uuid })}}>
          <Text style={{color: '#1266AF',fontWeight:'bold',flexWrap: 'wrap'}}>{rowData.item.handle_name} :
          <Text numberOfLines={2} style={{color: '#000000',fontWeight:'bold',}}> {rowData.item.name}</Text></Text>
      </TouchableOpacity>
    </View>
          <Text style={{fontSize:12}} numberOfLines={2}>{rowData.item.description}</Text>
      </View>

      </View>
      </View>
    )
  }


  noRowData () {
    //return this.state.dataSource.getRowCount() === 0
    return this.state.dataSource.length === 0
  }

  onEndReached = () => {
    let { isLoadingTail, payload:{meta:{pagination:{count,current_page,per_page,total,total_pages}}} } = this.state
    // console.log('on End fired!');
    // console.log('onEndReached', isLoadingTail);
    // console.log('count:',count,'current_page:',current_page,'per_page:',per_page,'total:',total,'total_pages:',total_pages);
       if (isLoadingTail) {
           // We're already fetching
           return;
       }
      //  this.setState({
      //      isLoadingTail: true
      //  });

      if(total_pages > current_page){
        this.setState({
            isLoadingTail: true,
        });
        this.props.getTrending('pagination', ++current_page);
      }else{
        this.setState({
            isLoadingTail: false
        });
        this.refs.toast.show('No More Data!',DURATION.LENGTH_SHORT);
      }
      //console.log('payload', payload);
  }

  updateTreading(newProps){
    console.log('updateTreading', newProps);
    if(newProps.error){
      alert("Error: Something went wrong!");
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(newProps.payload.data)
      })
    }else{
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(newProps.payload.data)
      })
    }
  }

  render () {
      //console.log(this.props);
      // let payload = this.state && this.state.payload || {data: []}
      //console.log(payload.data, 'playload.data', this.props.title)

      console.log('this.state.dataSource',this.state.dataSource);
      return <View style={styles.challengeListContainer}>

      { this.state.defaultfetching && Platform.OS != 'android' &&
       <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
      <GiftedSpinner size='large' />
      </View>
      }

      { !this.state.fetching && this.state.dataSource.length === 0 &&
         <AlertMessage title='Nothing to See Here, Move Along' show={this.noRowData()} />
      }
      { this.state.fetching &&
        <GiftedSpinner />
      }
      { this.state.dataSource.length > 0 &&
        <FlatList

              onEndReachedThreshold={0.5}
              contentContainerStyle={styles.listContent}
              data={this.state.dataSource}
              // extraData={this.state.extraData}
              renderItem={this.renderRow}
              keyExtractor={this._keyExtractor}
              onEndReached={()=>{
                let { isLoadingTail, payload:{meta:{pagination:{count,current_page,per_page,total,total_pages}}} } = this.state
                // console.log('on End fired!');
                // console.log('onEndReached', isLoadingTail);
                // console.log('count:',count,'current_page:',current_page,'per_page:',per_page,'total:',total,'total_pages:',total_pages);
                   if (isLoadingTail) {
                       // We're already fetching
                       return;
                   }
                  //  this.setState({
                  //      isLoadingTail: true
                  //  });

                  if(total_pages > current_page){
                    this.setState({
                        isLoadingTail: true,
                    });
                    this.props.getTrending('pagination', ++current_page);
                  }else{
                    this.setState({
                        isLoadingTail: false
                    });
                    this.refs.toast.show('No More Data!',DURATION.LENGTH_SHORT);
                  }
              }}
            />
      }
      { this.state.isLoadingTail &&
        <GiftedSpinner />
      }
      <Toast
          ref="toast"
          style={{backgroundColor:'#f9642f'}}
          position='bottom'
          positionValue={250}
          fadeInDuration={750}
          fadeOutDuration={700}
          opacity={0.9}
          textStyle={{color:'white'}}
      />
      </View>

  }
}

class PopularChallengesView extends React.Component {

 render () {
    return (
      <View style={styles.mainDoubleNavContainer}>
          <ConnectedChallengeList stateName='trending' requestType='getTrending' title='Featured Challenges' />
      </View>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    defaultfetching:state[props.stateName].defaultfetching,
    refreshing:state[props.stateName].refreshing,
    isLoadingTail:state[props.stateName].isLoadingTail,
    fetching: state[props.stateName].fetching,
    payload: state[props.stateName].payload,
    error: state[props.stateName].error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getTrending: (default_type = 'default', page = 1, challengeId = null) =>{
        console.log(default_type, page, challengeId);
        return dispatch(TrendingActions.trendingRequest(default_type, page, challengeId))
    }
  }
}

const ConnectedChallengeList = connect(mapStateToProps, mapDispatchToProps)(ChallengeList)
export default PopularChallengesView
