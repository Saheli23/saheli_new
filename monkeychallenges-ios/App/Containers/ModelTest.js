import React from 'react';
import {View, Text, StyleSheet,TouchableHighlight, Animated, Dimensions, Button, TextInput, Picker, TouchableOpacity, Keyboard, AsyncStorage,Platform} from "react-native";

import { Actions as NavigationActions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome'
import Icons from '../Components/Icons'
import { Colors, Metrics } from '../Themes'
// button
import RoundedButton from '../Components/RoundedButton'
import I18n from 'react-native-i18n'
const {
  width,
  height: deviceHeight
} = Dimensions.get("window");
import ListViewSelect from 'react-native-list-view-select';
import ModalPicker from 'react-native-modal-picker'



var styles = StyleSheet.create({
  containerlist: {
      paddingTop: 100,
      paddingBottom: 100,
  },
  container: {
      position: "absolute",
      top:0,
      bottom:0,
      left:0,
      right:0,
      backgroundColor:'rgba(0,0,0,0.3)'
  },
  rowLabel: {
      color: Colors.background
  },
  textInput: {
    marginLeft:5,
    height: (Platform.OS === 'ios') ? 25 : 40,
    backgroundColor:'rgba(255,255,255,0.3)',
    color: Colors.background,
    fontSize:15,
  },
PopOverSelect:{
     marginLeft:5,
     marginTop:10,
     alignItems: 'center',
     height: 35,
     backgroundColor:'rgba(255,255,255,0.3)',
     borderWidth:0,
     borderRadius:0,
     borderColor:'red'


},
submitButton:{
     marginLeft:5,
     marginTop:10,
     justifyContent: 'center',
     alignItems: 'center',
     height: 35,
     backgroundColor:Colors.matchingColor,

},
  textInputReadonly: {
    height: 40,
    color: Colors.background
  },
});

class ModelTest extends React.Component {
    constructor(props){
        super (props);

        this.state = {
            keyword:'',
            city: '',
            item: "Select a country",
            isVisible: false,
            keyword:'',
            offset: new Animated.Value(deviceHeight),
          //  categoryTest: this.getChallengeCategoryListTest()
        };
    }

    componentDidMount() {
      //  this.getChallengeCategoryListTest();
        Animated.timing(this.state.offset, {
            duration: 150,
            toValue: 0
        }).start();

    }
componentWillMount () {
  this.getChallengeCategoryList();
}

 getChallengeCategoryList () {
  // let response = await AsyncStorage.getItem('MCChallengeCategory');
  // this.setState({category:JSON.parse(response)});
    AsyncStorage.getItem('MCChallengeCategory').then((value) =>{
      let categoryObj = JSON.parse(value)
      //console.log(categoryObj);
      let cat_arr = [];
      //console.log( payload, default_type );
      categoryObj.map((data,index) => {
              //console.log(data,index);
              cat_arr.push({key:data.id,label:data.name})
            })
      this.setState({category:cat_arr});
    })
 }
// getChallengeCategoryListTest = async () => {
//  //getChallengeCategoryListTest = () => {
//     let value = await AsyncStorage.getItem('MCChallengeCategory')
//     console.log(value);
//     this.setState({category:value});
//     return value
//   }


    closeModal() {
        Animated.timing(this.state.offset, {
            duration: 150,
            toValue: 0
        }).start(NavigationActions.pop);

        //{...propsToSetOnPreviousScene}
      //NavigationActions.pop({refresh: {variable: "true"} })
    }
 handleChangekeyword = (text) => {
    this.setState({keyword: text})
  }
  handleKeyDown = () => {
    this.setState({isVisible: true});
  }
  handleKeyDownEvent = (e) => {
    if(e.nativeEvent.key == "Enter"){
        dismissKeyboard();
        this.setState({isVisible: true});
    }
  }

   showPopover = () =>  {
    Keyboard.dismiss();
    this.setState({isVisible: true});
  }


  handleFundChallengesButton = (data) =>{
    alert(data);
  }

  closePopover = () =>  {
    this.setState({isVisible: false});
  }
  setItem = (item) => {
    this.setState({item: item});
  }
  handleSubmit = () => {
    Keyboard.dismiss();
    this.setState({isVisible: true});
    let {keyword, challenge_category_type_id, challenge_category_type_name} = this.state;
    if(keyword == ''){
      alert("Enter keyword");
    }else if (challenge_category_type_id == '') {
      alert("Select challenge category");
    }else{
      this.setState({keyword: '',challenge_category_type_id:'',challenge_category_type_name:''});
      NavigationActions.SearchResultView({'keyword':keyword,'challenge_name':challenge_category_type_id,'cat_name':challenge_category_type_name});

    }

    //console.log(keyword, challenge_category_type_id);

  }
    render(){
        const {keyword, country, category,} = this.state



        return (
            <Animated.View style={[styles.container,
                                  {transform: [{translateY: this.state.offset}]}]}>
            <View style={{flex:1,justifyContent: 'flex-start',}}>
             <View style={{width: (Metrics.screenWidth),
                            flexDirection:'row',
                            padding:5,
                            justifyContent: "center",
                            alignItems: "center",
                            backgroundColor:"black",}}>
{/* search icon enable
                                {Icons.searchButton()}
*/}
                                <Text style={{color:'#FFFFFF'}}>Search</Text>

            </View>

                <View style={{
                                flexDirection:'column',
                                borderRadius:10,
                                padding:5,
                                backgroundColor:"rgba(0,0,0,0.2)",margin:5 }}>

                  {/*  <Text style={{marginLeft:10,marginTop:5,color:'#fff',fontWeight:'bold',fontSize:12}}>Campaign Categories</Text> */}
                    <View style={{padding:10,flexDirection: 'row',
    flexWrap: 'wrap',}}>
                  {/*

<Icons.FundChallengesButton onDonePress={(itemData) => this.handleFundChallengesButton(itemData)}/>
                   */}
                      <Icons.FundChallengesButton />
                      <Icons.PendingChallengesButton />
                      <Icons.JudgeChallengesButton />


                    </View>
                    <View style={{padding:10,flexDirection: 'row',
    flexWrap: 'wrap',}}>
                    {Icons.WinnersButton()}
                    {Icons.FailedButton()}

                    {/* NearMeButton icon enable
                    {Icons.NearMeButton()}

                    */}


                    </View>

                </View>


                <View style={{
                                flexDirection:'column',
                                borderRadius:10,
                                padding:5,
                                backgroundColor:"rgba(0,0,0,0.2)",margin:5 }}>

{/* NearMeButton text enable

                     <Text style={{marginLeft:10,marginTop:5,color:'#fff',fontWeight:'bold',fontSize:12}}>Near Me</Text>

*/}





      <View style={{
                flexDirection:'column',
                padding:10,

                }}>
                <ModalPicker
                          // data={this.state.payload.cat_data}
                          data={this.state.category}
                          initValue="Select Category"
                          onChange={(option)=>{
                            console.log(option)
                            this.setState({challenge_category_type_id:option.key,challenge_category_type_name:option.label})

                          }}>
                          <TextInput
                          ref='Category'
                                                style={[styles.textInput,{paddingLeft:15,margin:5}]}
                                                  editable={false}
                                                  placeholderTextColor='#FFFFFF'
                                                  placeholder="Select Category"
                                                  value={this.state.challenge_category_type_name} />
                          </ModalPicker>


{/*
        <TouchableOpacity style={[styles.PopOverSelect,]} onPress={this.showPopover}>
          <Text style={{color: Colors.background,
    fontSize:15,}}>{this.state.item}</Text>
        </TouchableOpacity>
*/}

<View style={{height:10}}></View>





                  <TextInput
                    ref='keyword'
                    style={[styles.textInput,{paddingLeft:15}]}
                    value={keyword}
                    keyboardType='default'
                    returnKeyType='done'
                    autoCapitalize='none'
                    onChangeText={this.handleChangekeyword}
                    autoCorrect={false}
                    underlineColorAndroid='transparent'
                    onSubmitEditing={() => this.handleKeyDownEvent}
                    placeholder="Search Keyword"
                    placeholderTextColor='#FFFFFF' />

  <TouchableOpacity style={[styles.submitButton,]}
  onPress={()=>{
this.handleSubmit();
  //  NavigationActions.SearchResultView();
  }}>
          <Text style={{color: Colors.background,
    fontSize:15,}}>Submit</Text>

        </TouchableOpacity>


        </View>




                </View>

{/* */}
                </View>
    {/*
   <ListViewSelect containerStyle={{top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    position: 'relative',
    justifyContent: 'center',
    backgroundColor: 'transparent',}} list={items} isVisible={this.state.isVisible} onClick={this.setItem} onClose={this.closePopover} />
*/}
                <TouchableOpacity onPress={this.closeModal.bind(this)} style={{width: (Metrics.screenWidth),
                            flexDirection:'row',
                            padding:5,
                            justifyContent: "center",
                            alignItems: "center",
                            backgroundColor:"rgba(0,0,0,0)",}}>

        <Icon name='chevron-down'
          size={Metrics.icons.small}
          color={Colors.background}
          style={{color: Colors.steel,marginRight: Metrics.smallMargin,}}
        />
            </TouchableOpacity>


            </Animated.View>
        );
    }
}

export default ModelTest
