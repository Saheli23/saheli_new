// @flow

import React from 'react'
import {
    ScrollView,
    Text,
    KeyboardAvoidingView,
    Image,
    ListView,
    StyleSheet,
    View,
    TouchableOpacity,
    Dimensions,Button
   } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)

import Modal from 'react-native-modalbox'
import Slider from 'react-native-slider'
var screen = Dimensions.get('window');


import OpenChallengesActions from '../Redux/OpenChallengesRedux'


import { Metrics, Images } from '../Themes'

// external libs
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions as NavigationActions } from 'react-native-router-flux'
import Animatable from 'react-native-animatable'

// Styles
//import styles from './Styles/OpenChallengesViewStyle'
//import styles from './Styles/JudgingChallengesViewStyle'
import styles from './Styles/JudgingChallengesViewStyle'

// alert message
import AlertMessage from '../Components/AlertMessage'

// I18n
import I18n from 'react-native-i18n'

// progressBar
import * as Progress from 'react-native-progress'

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'



type ChallengeListProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,
    getTrending: () => void
}

class OpenChallengeView extends React.Component {
  handleChallengesView(itemData) {
    NavigationActions.OpenChallengeDetailView({item:itemData})
        //this.props.levelSelected(levelNumber);
  }
  render () {
    console.log("test open challenge view");
    let itemData = this.props.itemData
    let icon = (itemData.challenge_media[0].path == undefined) ? "https://facebook.github.io/react/img/logo_og.png" : itemData.challenge_media[0].path;
    return <View style={styles.challenge}>
      <View style={styles.imageSection}>
        <View style={{height: 180 }}>
        <TouchableOpacity onPress={this.handleChallengesView.bind(this, itemData)}>
        {

          itemData.challenge_media[0].path &&
          <Image style={{height: 180,backgroundColor: "#cccccc" }} source={{uri: icon}} defaultSource={require('../Images/monkey_default.png')} />
        }
        </ TouchableOpacity>
        </View>

        <View style={styles.imageFooter}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
           <Progress.Bar progress={(Math.round(rowData.percentage_funded) / 100)} color={'#33CC32'} height={8} width={(width - 20)} borderWidth={0} unfilledColor={'rgba(128,132,131,0.8)'}/>

          </View>
          <View style={styles.contentVoteSection}>
            <Text style={{color: 'white' }}>$ {itemData.price} USD</ Text>
            <Text style={{color: 'white'}}>{Math.round(itemData.percentage_funded)}%</Text>
            <Text style={{color: 'white', textAlign: 'right'}}>{itemData.days_remaining} days left</Text>
          </View>
        </View>
        <View style={styles.imageFooterOpacity} />
      </View>
      <View style={styles.contentSection}>
        <View style={styles.contentDescriptionSection}>
          <Text numberOfLines={3}><Text style={{color: '#337ab7'}}>{itemData.handle_name} : </Text>{itemData.description}</Text>
        </View>
      </View>
    </View>
  }
}



class ChallengeList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      loaded: false,

    }
  }
  props:ChallengeListProps
  componentWillReceiveProps (newProps) {
    console.log(newProps);
    // if (!newProps.fetching) {
    //   this.setState({fetching:newProps.fetching, payload: newProps.payload})
    // }else{
    //    this.setState({fetching:newProps.fetching, payload: newProps.payload})
    // }
     this.setState({fetching:newProps.fetching, payload: newProps.payload})
  }

  // componentDidReceiveProps (newProps){
  //    console.log(newProps);
  // }

  componentWillMount () {
    console.log("test will mount");
    console.log(this.props);
      this.props[this.props.requestType]()
  }
  // componentWillUpdate (){
  //   console.log("test Will Update");
  //   console.log(this.props);
  // }
  //
  // componentDidMount(){
  //    console.log("test Did Mount");
  //    console.log(this.props);
  // }
  // componentWillUnmount(){
  //    console.log("test Will Unmount");
  //    console.log(this.props);
  // }
  // componentDidUpdate(){
  //    console.log("test Did Update");
  //    console.log(this.props);
  // }




  render () {
  console.log(this.props);
       let payload = this.state && this.state.payload || {data: []}
      console.log(payload.data, 'playload.data', this.props.title)
      return <View style={styles.challengeListContainer}>

      { !this.state.fetching &&
      <AlertMessage title='Nothing to See Here, Move Along' show={(payload.data.length == 0)} />
      }
      { this.state.fetching &&
        <GiftedSpinner />
      }
        <ScrollView vertical>

          {
                        payload.data.map((rowData, index) => {
                          return <OpenChallengeView itemData={rowData} key={index} />
                        })

            }
        </ScrollView>
      </View>

  }
}
 // /
class OpenChallengesView extends React.Component {
  render () {
    return (
       <View style={styles.mainDoubleNavContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <ConnectedChallengeList stateName='openChallenges' requestType='getOpenChallenges' title='Open Challenges' />
        </ScrollView>
         </View>
    )
  }

}
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  console.log(stateProps);
  console.log(dispatchProps);
  console.log(ownProps);
  return Object.assign({}, ownProps, stateProps, dispatchProps)

}

const mapStateToProps = (state, props) => {
  console.log(props);
  console.log(state);
  return {
    fetching: state[props.stateName].fetching,
    payload: state[props.stateName].payload
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getOpenChallenges: () => dispatch(OpenChallengesActions.openChallengesRequest())

  }
}


const ConnectedChallengeList = connect(mapStateToProps, mapDispatchToProps, mergeProps)(ChallengeList)
export default OpenChallengesView
