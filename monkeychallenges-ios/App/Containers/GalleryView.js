// @flow

import React from 'react'
import {Alert, ScrollView, Text, KeyboardAvoidingView, Image, ListView, View, StyleSheet, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import { Actions as NavigationActions } from 'react-native-router-flux'
import RoundedButton from '../Components/RoundedButton'
import CameraRollPicker from 'react-native-camera-roll-picker';



// progressBar
import * as Progress from 'react-native-progress'


import VideoUploadActions from '../Redux/VideoUploadRedux'


// Styles
//import styles from './Styles/ChallengeDetailViewStyle'
import styles from './Styles/JudgingChallengesViewStyle'

// camera image enable
// import CameraRoll from 'rn-camera-roll'

//import RNFS from 'react-native-fs'



//import RNFetchBlob from 'react-native-fetch-blob'




//import RNFS from 'react-native-fs'

let PHOTOS_COUNT_BY_FETCH = 24;


type GalleryViewProps = {
    dispatch: () => any,
    fetching: boolean,
    attemptVideoUpload: () => void
}

class GalleryView extends React.Component {
  props: GalleryViewProps

  constructor (props) {
    super(props)

    this.state = {
      num: 0,
      selected: [],
      blobSelectedFile: [],
      token:"",
      isUploading:false
    };

  }

 

  getSelectedImages(videos, current) {
    var num = videos.length;

    this.state.blobSelectedFile.length = 0;

     videos.map((rowData, index) => {
      this.state.blobSelectedFile.push({
        name: rowData.filename,
        uri : rowData.uri,
        type : 'video/mp4',
      })
     })

    this.setState({
      num: num,
      selected: videos,
    });
   
  }


  componentWillReceiveProps (newProps) {
    console.log(newProps);
  }


  componentWillMount(){
  this.getTokenItem()
  }
 getTokenItem() {
  AsyncStorage.getItem('MCtoken').then((value) =>{
    this.setState({token:value});
  })
  
 }




videoUploadFile = () => {
    const { selected, blobSelectedFile, token } = this.state
    const { _id ,uuid } = this.props.item
    let url = `https://api.monkeychallenges.com/api/users/${uuid}/challenges/${_id}/story`;
    
    console.log(selected[0]);
    console.log(blobSelectedFile[0]);

  return new Promise(function(resolve, reject){
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    var formdata = new FormData();
    formdata.append('name', 'mar 2 name fetch');
    formdata.append('description', 'mar 2 fetch Discription');
    formdata.append('media_type_id', '6');
    formdata.append('file', blobSelectedFile[0]);
    xhr.setRequestHeader('Cache-Control', 'no-cache');
    xhr.setRequestHeader('Authorization', 'Bearer ' + token);

    xhr.onload = function () {
          if(this.status >= 200 && this.status < 300){
            resolve(xhr.response);
          }else{
            reject({
              status: this.status,
              statusText: xhr.statusText
            })
          }
    };

    xhr.onerror = function () {
          reject({
            status: this.status,
            statusText: xhr.statusText
          })
    };

   xhr.send(formdata);

   });
 }

  handleUploadVideos = () => {
     const { selected } = this.state
    if(selected.length < 1){

      alert("please select your video to upload!");
    }else{

  // ========================== calling video upload =============

        this.videoUploadFile().then(function(data) {
          Alert.alert(
              `success`,
              `Uploaded successfull!`
            );
        console.log(data);
        // body...
        }).catch(function(err){

          Alert.alert(
              `Upload failed status :${err.status}`,
              `message: ${err.statusText}`
            );
        console.log(err)
        })


    }
}





  render () {
    let itemData = this.props.item

    return (
      <View style={styles.container}>
        <View style={cssskin.content}>
          <Text style={cssskin.text}>
            <Text style={cssskin.bold}> {this.state.num} </Text> images has been selected
          </Text>
        </View>
        <CameraRollPicker
          scrollRenderAheadDistance={500}
          initialListSize={1}
          pageSize={3}
          removeClippedSubviews={false}
          groupTypes='All'
          batchSize={5}
          maximum={3}
          selected={this.state.selected}
          assetType='All'
          imagesPerRow={3}
          imageMargin={5}
          callback={this.getSelectedImages.bind(this)} />

         

        <RoundedButton onPress={this.handleUploadVideos}>
           Upload your Challenge videos!
        </RoundedButton>
      </View>
    );

  }
}

const cssskin = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F6AE2D',
  },
  content: {
    marginTop: 15,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  text: {
    fontSize: 16,
    alignItems: 'center',
    color: '#fff',
  },
  bold: {
    fontWeight: 'bold',
  },
  info: {
    fontSize: 12,
  },
});


const mapStateToProps = (state) => {
return {
    fetching: state.videoUpload.fetching,
    payload: state.videoUpload.payload
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    attemptVideoUpload: (_id , uuid, data) => dispatch(VideoUploadActions.videoUploadRequest(_id , uuid, data))

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GalleryView)
