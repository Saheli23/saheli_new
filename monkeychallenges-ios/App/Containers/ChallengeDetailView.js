// @flow

import React from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Image, ListView, View, AsyncStorage, Platform,Dimensions,TouchableOpacity } from 'react-native'
   const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import { Actions as NavigationActions } from 'react-native-router-flux'
import RoundedButton from '../Components/RoundedButton'

// progressBar
import * as Progress from 'react-native-progress'

// Styles
//import styles from './Styles/ChallengeDetailViewStyle'
import styles from './Styles/JudgingChallengesViewStyle'
//var ImagePicker = require('react-native-image-picker');
import ImagePicker from 'react-native-image-picker'
import Toast, {DURATION} from 'react-native-easy-toast'

class ChallengeViewPreState extends React.Component{

render () {

  let itemData = this.props.itemData
   return <View style={styles.challenge}>
      <View style={styles.imageSection}>

          <Image style={styles.cardImage} source={{uri: itemData.challenge_media[0].path}} />
          {(itemData.charity == 1) &&
            <View style={{position:'absolute',backgroundColor:'black',padding:3,flexDirection:'row',top:10,left:5,borderRadius:5}}>
            <Text style={{color:'#FFFFFF',fontWeight:'bold',fontSize:10}}>CHARITY</Text>
            </View>
            }
          <View style={styles.imageFooter}>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>


            <View style={styles.contentVoteSection}>
            <Text style={{color: 'white',fontWeight:'bold', fontSize:30 }}>${parseInt(itemData.price)}<Text style={{fontSize:10}}> FUNDED</Text></ Text>
            {/*
            <Text style={{color: 'white'}}>{Math.round(rowData.percentage_funded)}%</Text>
            */}
              <View style={{justifyContent:'flex-end'}}>
                <Text style={{color: 'white', textAlign: 'right', fontWeight:'bold',fontFamily:'Bradley Hand'}}>Getting ready!</Text>
              </View>

            </View>
            </View>
          </View>
        <View style={styles.imageFooterOpacity} />
      </View>





      <View style={styles.contentSection}>

      <View style={[styles.contentDescriptionSection,{flexDirection:'column',width:(width - 20),alignSelf:'center'}]}>

    {/*
           <Text style={{color: '#1266AF',fontWeight:'bold',}}>{rowData.handle_name} :
            <Text style={{color: '#000000'}}> {rowData.name}</Text></Text>
            <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: rowData.uuid })}}>
    */}

    <View style={{flexDirection:'row',width:(width - 20),}}>
      <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: itemData.uuid })}}>
          <Text style={{color: '#1266AF',fontWeight:'bold',flexWrap: 'wrap'}}>{itemData.handle_name} :
          <Text numberOfLines={2} style={{color: '#000000',fontWeight:'bold',}}> {itemData.name}</Text></Text>
      </TouchableOpacity>
    </View>
          <Text style={{fontSize:12}} numberOfLines={2}>{itemData.description}</Text>
      </View>

      </View>
      </View>
  }
}



class ChallengeDetailView extends React.Component {
// /
  constructor (props) {
    super(props)
    this.state = {
       token:"",
      isUploading:false
    }
  }


componentWillMount(){
  this.getTokenItem()
  }

  videoUploadFile = (data) => {
    let self=this;
    const { token } = this.state
    const { _id ,uuid } = this.props.item

    console.log("video file",data);
    //  /api/users/${uuid}/challenges/${_id}/media
    // let url = `https://api-dev.monkeychallenges.com/api/users/${uuid}/challenges/${_id}/story`;
   let url = `https://api-dev.monkeychallenges.com/api/users/${uuid}/challenges/${_id}/media`;

   console.log("token : ",token);
   console.log("_id : ",_id);
   console.log("uuid : ",uuid);
   console.log("url : ",url);




   if(Platform.OS === 'ios'){
    console.log("video image path : ",{"name": data.uri.split('/').pop(),
        "uri" : data.uri,
        "type" : 'video/mov'});

   }else{

   console.log("video image path : ",{"name": data.path.split('/').pop(),
        "uri" : `file:///${data.path}`,
        "type" : 'video/mov'});
   }



  var xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    var formdata = new FormData();
    formdata.append('name', 'mar 2 name fetch');
    formdata.append('description', 'mar 2 fetch Discription');
    formdata.append('media_type_id', '6');





  if(Platform.OS === 'ios'){
      formdata.append('file', {"name": data.fileName || data.uri.split('/').pop(),
        "uri" : data.uri,
        "type" : 'video/mov'});
   }else{
    formdata.append('file', {"name": data.path.split('/').pop(),
        "uri" : `file:///${data.path}`,
        "type" : 'video/mp4'});
   }




    xhr.setRequestHeader('Cache-Control', 'no-cache');
    xhr.setRequestHeader('Authorization', 'Bearer ' + token);
    this.refs.toast.show('Please wait ,Uploading your challenge video',4500);
    xhr.onload = function () {
          if(this.status >= 200 && this.status < 300){
            //resolve(xhr.response);

          // alert('Challenge video uploaded successfully.Challenge goes to juding');
            self.refs.toast.show('Please Wait,Processing uploaded video',4000);
            setTimeout(() => {
              NavigationActions.pop();
              NavigationActions.JudgingChallengeDetailView({item:self.props.item,getJudgingChallenges: 'mapDispatchToProps',myChallenge:'true'})
              //NavigationActions.JudgingChallengesView();
            }, 4000);

            console.log(xhr.response);
          }else{
            // reject({
            //   status: this.status,
            //   statusText: xhr.statusText
            // })
            self.refs.toast.show('Upload failed!Please try again',1000);
            console.log(this.status);
            console.log(xhr.statusText);
          }
    };

    xhr.onerror = function () {
          // reject({
          //   status: this.status,
          //   statusText: xhr.statusText
          // })
          console.log(xhr.statusText);
    };

   xhr.send(formdata);

  // return new Promise(function(resolve, reject){


  //  });


 }

 getTokenItem() {
  AsyncStorage.getItem('MCtoken').then((value) =>{
    this.setState({token:value});
  })
 }



  componentWillReceiveProps (newProps) {
    console.log(newProps);
  }
  handleUploadVideos(){
   // NavigationActions.GalleryView({item:this.props.item})

   const options = {
      title: 'Video Picker',
      takePhotoButtonTitle: 'Take Video...',
      mediaType: 'video',
      videoQuality: 'medium'
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled video picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        // this.setState({
        //   videoSource: response.uri
        // });
        console.log(response.uri);
        console.log("uploading started!!");

        this.videoUploadFile(response);



      }
    });

  }


  handlePassChallenge(){
    alert("Pass I can't do this!");


  }

  render () {
 let itemData = this.props.item
    return (
    <View style={styles.mainContainer}>
        <ScrollView vertical style={styles.container}>
        <ChallengeViewPreState itemData={itemData} />
        <RoundedButton onPress={this.handleUploadVideos.bind(this)}>
           Upload your Challenge videos!
        </RoundedButton>
        <RoundedButton onPress={this.handlePassChallenge.bind(this)}>Pass, I cannot do this challenge</RoundedButton>{ this.state.videoSource &&
          <Text style={{margin: 8, textAlign: 'center'}}>{this.state.videoSource}</Text>
        }
        <Toast
            ref="toast"
            style={{backgroundColor:'#f9642f',marginTop:45}}
            position='bottom'
            positionValue={250}
            fadeInDuration={750}
            fadeOutDuration={700}
            opacity={0.9}
            textStyle={{color:'white'}}
        />
         </ScrollView>

      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {

  }
}

const mapDispatchToProps = (dispatch) => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChallengeDetailView)
