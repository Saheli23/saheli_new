// @flow

import React, { Component } from 'react'
import { ScrollView, Image, BackAndroid, View, Text } from 'react-native'
import styles from './Styles/DrawerContentStyle'
import { Images } from '../Themes'
import DrawerButton from '../Components/DrawerButton'
import { Actions as NavigationActions } from 'react-native-router-flux'


const FBSDK = require('react-native-fbsdk')
const {
    LoginButton,
    AccessToken,
    LoginManager
} = FBSDK
var FBLoginManager = require('NativeModules').FBLoginManager;
class DrawerContent extends Component {

  componentDidMount () {
    BackAndroid.addEventListener('hardwareBackPress', () => {
      if (this.context.drawer.props.open) {
        this.toggleDrawer()
        return true
      }
      return false
    })
  }

  toggleDrawer () {
    this.context.drawer.toggle()
  }

  handlePressComponents = () => {
    this.toggleDrawer()
    NavigationActions.componentExamples()
  }

  handlePressUsage = () => {
    this.toggleDrawer()
    NavigationActions.usageExamples()
  }

  handlePressAPI = () => {
    this.toggleDrawer()
    NavigationActions.apiTesting()
  }

  handlePressTheme = () => {
    this.toggleDrawer()
    NavigationActions.theme()
  }

  handlePressDevice = () => {
    this.toggleDrawer()
    NavigationActions.deviceInfo()
  }
  handlePressLogOut= () => {
   // alert("log out implimentation!");
   //FBLoginManager.logout()
    this.toggleDrawer()
    NavigationActions.landingScreen()
  }

  handlePressHome = () =>{
    this.toggleDrawer()
    NavigationActions.Home()

  }
  handlePresschangePassword = () =>{
    this.toggleDrawer()
    NavigationActions.changePassword()

  }
  handlePressmyFunds = () =>{
    this.toggleDrawer()
    NavigationActions.myFunds()

  }
handlePresspaymentOptions = () =>{
  this.toggleDrawer()
  NavigationActions.paymentOptions()

}
handlePresstransactionHistory = () =>{
  this.toggleDrawer()
  NavigationActions.transactionHistory()

}
handlePressprivacy = () =>{
  this.toggleDrawer()
  NavigationActions.privacy()

}
handlePressdeleteAccount = () =>{
  this.toggleDrawer()
  NavigationActions.deleteAccount()

}
  handlePressProfile = () =>{
    this.toggleDrawer()
    NavigationActions.profile()
//   NavigationActions.MyProfile();

  }
  handlePressCreatechallenge = () =>{
    this.toggleDrawer()
    NavigationActions.createchallenge();
    //  NavigationActions.tab4();

  }
  handlePressterms = () =>{
    this.toggleDrawer()
    NavigationActions.terms();
    //  NavigationActions.tab4();

  }
  handlePressPrivacyPolicy = () =>{
    this.toggleDrawer()
    NavigationActions.privacypolicy();
    //  NavigationActions.tab4();

  }


  render () {
    return (
      <ScrollView style={styles.container}>
      <View style={{marginTop:50,marginBottom:20}}>
          <Text>ACCOUNT SETTINGS</Text>
      </View>


      <DrawerButton text='Profile Settings' onPress={this.handlePressProfile} />
      {/*
        <DrawerButton text='Home' onPress={this.handlePressHome} />
       <DrawerButton text='Create Challenege' onPress={this.handlePressCreatechallenge} />
       <View style={{marginTop:30,}}></View>
       */}


          <DrawerButton text='Change Password' onPress={this.handlePresschangePassword} />
          <DrawerButton text='My Funds' onPress={this.handlePressmyFunds} />
          {/*
          <DrawerButton text='Payment Options' onPress={this.handlePresspaymentOptions} />
          */}
          <DrawerButton text='Transaction History' onPress={this.handlePresstransactionHistory} />
          {/* <DrawerButton text='Privacy' onPress={this.handlePressprivacy} />*/}
          <DrawerButton text='Delete Account' onPress={this.handlePressdeleteAccount} />
          <DrawerButton text='Terms of Service' onPress={this.handlePressterms} />
          <DrawerButton text='Privacy Policy ' onPress={this.handlePressPrivacyPolicy} />
          <DrawerButton text='Log Out' onPress={this.handlePressLogOut} />



      </ScrollView>
    )
  }

}

DrawerContent.contextTypes = {
  drawer: React.PropTypes.object
}

export default DrawerContent
