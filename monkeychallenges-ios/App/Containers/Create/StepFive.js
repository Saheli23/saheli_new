// @flow

import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  KeyboardAvoidingView,
  Image,
  ListView,
  View,
  Dimensions,
  TouchableOpacity,
  Clipboard,
  AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)

import { Metrics, Images } from '../../Themes';
import RoundedButton from '../../Components/RoundedButton'
// external libs
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions as NavigationActions , ActionConst} from 'react-native-router-flux';
import Animatable from 'react-native-animatable';


import styles from '../Styles/StepFormStyle';

// alert message
import AlertMessage from '../../Components/AlertMessage';

// I18n
import I18n from 'react-native-i18n';

// progressBar
import * as Progress from 'react-native-progress';

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner';
import * as _ from 'lodash';
import Communications from 'react-native-communications';
const { width, height } = Dimensions.get('window')


class StepFive extends Component {
  handleStepFinesh = () => {

    NavigationActions.MyOpenChallengesView
   // NavigationActions.stepFive()

  }
  _setContent(newShareLink) {


     Clipboard.setString(newShareLink);
    //  Clipboard.getString().then((content) => {
    //    console.log('content',content);
     //
    //  });
  }
  // Clipboard.getString().then((content) => {
  //   console.log(content);
  //
  // });
  //   async _getContent() {
  //   var content = await Clipboard.getString();
  // }
  render() {
    let itemData = this.props.challengeInfo;
    let imagePath = this.props.imageSource;
    console.log(itemData);
      let newShareLink  =`http://oneup-web-env-1.us-west-2.elasticbeanstalk.com/#/challenges/${itemData.status}/${itemData._id}`
    return (
       <ScrollView ref="myScrollView" keyboardDismissMode='interactive' style={styles.mainContainer}>
      <View style={{paddingTop:10,}}>

   <View style={{width:(width - 20),alignSelf:'center'}}>
   <Text style={{fontWeight: 'bold'}}>Sucessfully Posted!</Text>
   </View>

    {/*  <View style={{marginTop:5,height: (width/2.2),flexDirection:'row',width:(width - 20),alignSelf:'center',}}>
         <View style={{marginTop:5,marginBottom:15,height: (width/2.2),width:(width - 20),alignSelf:'center',}}>
    */}
         <View style={{marginTop:5,marginBottom:15,height: (width/2),width:(width - 20),alignSelf:'center',}}>
          <View style={{justifyContent:'flex-start',alignItems:'flex-start',}}>
            <Image style={{height: (width/3),width:(width/2) }} source={{uri: imagePath}} />
          </View>
{/*
          <View style={{paddingLeft:5}}>
            <Text numberOfLines={3}><Text style={{color: '#337ab7'}}>{itemData.handle_name} : </Text>{itemData.name}</Text>
            <Text numberOfLines={3}>{itemData.description}</Text>
           </View>
*/}
    <View style={{paddingLeft:3,}}>
        <Text style={{color: '#1266AF',fontWeight:'bold',flexWrap: 'wrap'}}>{itemData.handle_name} :
        <Text numberOfLines={2} style={{color: '#000000',fontWeight:'bold',}}> {itemData.name}</Text></Text>
          <Text style={{flexWrap: 'wrap',}} numberOfLines={5}>{itemData.description}</Text>
    </View>

      </View>


        {/*share*/}
        <View style={{flexDirection:'column',}}>
        <Text  style={{width:(width - 20),alignSelf:'center',paddingTop:25,paddingBottom:2,fontWeight: 'bold'}}>Share</Text>

        <View style={{flexDirection:'row',}}>
        <TouchableOpacity onPress={() => Communications.text('',newShareLink)} style={{width:(width/3),height:(width/3),borderBottomWidth:1,borderTopWidth:1,borderColor:'#ccc',alignItems:'center',justifyContent:'space-around'}}>
          <Image
              style={{ height:(width/5.5),width:(width/5.5),marginTop:10 }}
                  source={require('../../Images/sms.png')}
                  defaultSource={require('../../Images/sms.png')}
                />
            <Text style={{color:'#4d4d4d'}}>SMS</Text></TouchableOpacity>
            <TouchableOpacity onPress={()=>{NavigationActions.sharechallenge({ itemData: itemData })}} style={{width:(width/3),height:(width/3),borderBottomWidth:1,borderTopWidth:1,borderLeftWidth:1,borderColor:'#ccc',alignItems:'center',justifyContent:'space-around'}}>
              <Image
                  style={{ height:(width/5.5),width:(width/5.5),marginTop:10 }}
                  source={require('../../Images/email.png')}
                  defaultSource={require('../../Images/email.png')}
                />
            <Text style={{color:'#4d4d4d'}}>Email</Text></TouchableOpacity>
            <TouchableOpacity onPress={()=>{this._setContent(newShareLink)}}  style={{width:(width/3),height:(width/3),borderBottomWidth:1,borderTopWidth:1,borderLeftWidth:1,borderColor:'#ccc',alignItems:'center',justifyContent:'space-around'}}>
              <Image
                  style={{ height:(width/5.5),width:(width/5.5),marginTop:10 }}
                  source={require('../../Images/copylink.png')}
                  defaultSource={require('../../Images/copylink.png')}
                />
            <Text style={{color:'#4d4d4d'}}>Copy Link</Text></TouchableOpacity>
        </View>

        <View style={{flexDirection:'row'}}>
        <TouchableOpacity onPress={()=>{NavigationActions.facebookshare({ itemData: itemData })}} style={{width:(width/3),height:(width/3),borderBottomWidth:1,borderColor:'#ccc',alignItems:'center',justifyContent:'space-around'}}>
          <Image
              style={{ height:(width/5.5),width:(width/5.5),marginTop:10 }}
                  source={require('../../Images/facebook.png')}
                  defaultSource={require('../../Images/facebook.png')}
                />
                <Text style={{color:'#4d4d4d'}}>Facebook</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{NavigationActions.twittershare({ itemData: itemData })}} style={{width:(width/3),height:(width/3),borderBottomWidth:1,borderLeftWidth:1,borderColor:'#ccc',alignItems:'center',justifyContent:'space-around'}}>
              <Image
                  style={{ height:(width/5.5),width:(width/5.5),marginTop:10 }}
                  source={require('../../Images/twitter.png')}
                  defaultSource={require('../../Images/twitter.png')}
                />
                <Text style={{color:'#4d4d4d'}}>Twitter</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{width:(width/3),height:(width/3),borderBottomWidth:1,borderLeftWidth:1,borderColor:'#ccc',alignItems:'center',justifyContent:'space-around'}}>
                  <Image
                      style={{ height:(width/5.5),width:(width/5.5),marginTop:10 }}
                  source={require('../../Images/instagram.png')}
                  defaultSource={require('../../Images/instagram.png')}
                />
                 <Text style={{color:'#4d4d4d'}}>Instagram</Text>
            </TouchableOpacity>
        </View>
        </View>
        <TouchableOpacity style={{width:(width - 20),alignSelf:'center',marginTop:20,marginBottom:10,backgroundColor:'#F16622',alignItems:'center',justifyContent:'center',padding:10}} onPress={() => { NavigationActions.MyOpenChallengesView()}}>
        <Text style={{color:'white',fontWeight:'bold'}}>Your Recent Challenges</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{width:(width - 20),alignSelf:'center',marginTop:20,marginBottom:10,backgroundColor:'#F16622',alignItems:'center',justifyContent:'center',padding:10}} onPress={() => { AsyncStorage.setItem('FromHome', 'home');NavigationActions.OpenChallengesView(); }}>
        <Text style={{color:'white',fontWeight:'bold'}}>Go to Home</Text>
        </TouchableOpacity>

        {/*
         <RoundedButton onPress={()=>{
           //NavigationActions.MyOpenChallengesView();
           NavigationActions.MyOpenChallengesView()
          //  setTimeout(()=> NavigationActions.refresh(), 500)
         }}>
           Your recent challenges
        </RoundedButton>
*/}


      </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
      return {

        }

}

const mapDispatchToProps = (dispatch) => {
  return {
    // createChallenge:(default_type = 'stepSubmitEnd') => dispatch(CreateChallengeActions.createChallengeRequest(default_type))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StepFive)
