// @flow

import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  KeyboardAvoidingView,
  Image,
  ListView,
  View,
  TouchableOpacity,
  Platform,
  AsyncStorage,
} from 'react-native';
import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)

import { Metrics, Images } from '../../Themes';
import RoundedButton from '../../Components/RoundedButton'
// external libs
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions as NavigationActions } from 'react-native-router-flux';
import Animatable from 'react-native-animatable';


import styles from '../Styles/StepFormStyle';

// alert message
import AlertMessage from '../../Components/AlertMessage';

// I18n
import I18n from 'react-native-i18n';

// progressBar
import * as Progress from 'react-native-progress';

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner';
import * as _ from 'lodash';
import ImagePicker from 'react-native-image-picker'


type StepTwoProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,

}

class StepTwo extends Component {
  props:StepTwoProps;
  constructor(props) {
    super(props);
  this.state = {
      imageSource:null,
      originalPath:null,
       token:"",
   };
   }

   componentWillMount(){
    this.getTokenItem()

  }
  getTokenItem() {
    AsyncStorage.getItem('MCtoken').then((value) =>{
      this.setState({token:value});
    })
   }
  handleStepThree = (data,challengeId,userUuid,challengeInfo) => {
    //alert('test step1');
  //  NavigationActions.stepThree()
    if(data !=null){

    this.setState({fetching:true});
    const { token } = this.state
  //console.log("useruuid",getUuid);
   let url = `https://api-dev.monkeychallenges.com/api/users/${userUuid}/challenges/${challengeId}/media`;



   if(Platform.OS === 'ios'){
    console.log("video image path : ",{"name": data.fileName || data.uri.split('/').pop(),
        "uri" : data.uri,
        "type" : 'image/jpg'});

   }else{

   console.log("video image path : ",{"name": data.path.split('/').pop(),
        "uri" : `file:///${data.path}`,
        "type" : 'image/jpg'});
   }

  var xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    var formdata = new FormData();
    formdata.append('name', '');
    formdata.append('description', '');
    formdata.append('media_type_id', 3);
  if(Platform.OS === 'ios'){
      formdata.append('file', {"name": data.fileName || data.uri.split('/').pop(),
        "uri" : data.uri,
        "type" : 'image/jpg'});
   }else{
    formdata.append('file', {"name": data.path.split('/').pop(),
        "uri" : `file:///${data.path}`,
        "type" : `image/${data.path.split('.').pop()}`});
   }
    xhr.setRequestHeader('Cache-Control', 'no-cache');
    xhr.setRequestHeader('Authorization', 'Bearer ' + token);
    var self = this;
    //let{user:{latest_cover_image, latest_profile_image}} = this.state.payload;
    xhr.onload = function () {
          if(this.status >= 200 && this.status < 300){
            //resolve(xhr.response);
           console.log(xhr.response);
           let resData =  JSON.parse(xhr.response);
           console.log(resData.path);
           self.setState({fetching:false});
           NavigationActions.stepThree({resData:resData,challengeInfo:challengeInfo})
           //self.props.getUserDetails(null,'userImageUpload')
          }else{
            self.setState({fetching:false});
            alert("image upload fails!! try again.");
            console.log(this.status);
            console.log(xhr.statusText);
          }
    };

    xhr.onerror = function () {
          // reject({
          //   status: this.status,
          //   statusText: xhr.statusText
          // })
          self.setState({fetching:false});
          console.log(xhr.statusText);
    };

   xhr.send(formdata);
 }
 else{
      alert("Please upload an Image by clicking on camera icon");
     }
  }

  handleUploadChallengeImage(){
      //alert('image profile upload!')
    var self = this;
   //console.log(self.imageSource);
      const options = {
         title: 'Profile image Picker',
         takePhotoButtonTitle: 'Take Image...',
         mediaType: 'image',
         allowsEditing:true,
         imageQuality: 'medium',

       };



       ImagePicker.showImagePicker(options, (response) => {
         console.log('Response = ', response);

         if (response.didCancel) {
           console.log('User cancelled image picker');
         }
         else if (response.error) {
           console.log('ImagePicker Error: ', response.error);
         }
         else if (response.customButton) {
           console.log('User tapped custom button: ', response.customButton);
         }
         else {
          console.log(response.uri);
           console.log("uploading started!!");
           console.log(self.imageSource);
            let source = { uri: response.uri };
           self.setState({
             imageSource: source,
             originalPath:response
           });
           //this.profileImageUpload(response,itemData.uuid,2);
         }
       });
  }
  render() {
     let challengeId = this.props.challengeid;
     let userUuid    = this.props.uuid;
     let challengeInfo =this.props.challengeinfo;
    // console.log('challengeId',challengeId);
   //  console.log('challengeCreator',userUuid);
    return (
      <View style={styles.mainContainer}>
      { this.state.fetching &&
             <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
           <GiftedSpinner size='large' />
            </View>
            }
      <Text style={{marginTop:10,marginLeft:25,fontWeight: 'bold',fontSize:15}}>Upload Your Challege Image</Text>
      <Text style={{marginTop:5,marginLeft:25,color:'#4d4d4d'}}>This will be a part of your Challenge description!</Text>

      <TouchableOpacity style={{ height: 40,alignItems:'center',justifyContent: 'center',marginTop:20}} onPress={this.handleUploadChallengeImage.bind(this)}  >
                <Image
                 style={{ height: 80,width:80,}}
                 source={require('../../Images/camera_black.png')}
                 defaultSource={require('../../Images/camera_black.png')}
               />
      </TouchableOpacity>

      <View style={{ height: 80,alignItems:'center',justifyContent: 'center',marginTop:50,marginBottom:50,}} >
      <Image
       style={{ height: 150,width:150,alignItems:'center',justifyContent: 'center',}}
       source={this.state.imageSource}
       defaultSource={this.state.imageSource}
     />
     </View>
     <RoundedButton onPress={() => this.handleStepThree(this.state.originalPath,challengeId,userUuid,challengeInfo)}>
           Continue to Step 3
        </RoundedButton>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
      return {
        }

}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StepTwo)
