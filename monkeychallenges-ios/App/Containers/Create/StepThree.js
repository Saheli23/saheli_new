// @flow

import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  KeyboardAvoidingView,
  Image,
  ListView,
  View,
  TouchableOpacity,
  TextInput,
  findNodeHandle,
} from 'react-native';
import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)

import { Metrics, Images,Colors } from '../../Themes';
import RoundedButton from '../../Components/RoundedButton'
// external libs
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions as NavigationActions } from 'react-native-router-flux';
import Animatable from 'react-native-animatable';


import styles from '../Styles/StepFormStyle';

// alert message
import AlertMessage from '../../Components/AlertMessage';

// I18n
import I18n from 'react-native-i18n';

// progressBar
import * as Progress from 'react-native-progress';

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner';
import * as _ from 'lodash';
import EditChallengeActions   from '../../Redux/EditChallengeRedux'


type StepThreeProps = {
  dispatch: () => any,
  fetching: boolean,
  payload: object,


};
class StepThree extends Component {
  props: StepThreeProps
  constructor(props) {
    super(props);
    this.state = {

      description:'',
      description_2:'',
      description_3:'',
      imageSource:null,
   };
  }
  componentWillMount(){
     let challengeData = this.props.resData;
     this.setState({
             imageSource: challengeData.path,

           });
  }
    componentWillReceiveProps (newProps) {
if(newProps.stepthree){

    console.log("BeforenewProps",newProps);
    if(newProps.error){
     alert('error: Something went wrong! Could not create new challenge.');
    }else{

        this.setState({ fetching: newProps.fetching, payload: newProps.payload, error:newProps.error});
        if(newProps.default_type =='stepThreeSubmit' && newProps.payload && newProps.status == 'success' ){
          NavigationActions.stepFour({ challengeInfo: newProps.payload.data,imageSource:this.state.imageSource})
        }
    }
}

   }
   handleStepFour (challengeInfo)  {
   // console.log('challengeData',challengeInfo);
    const {description, description_2, description_3} = this.state
    //alert('test step1');
    if(this.state.description==''){
      alert('Challenge description is required.');
    }
    else if(this.state.description_2==''){
      alert('Please enter What to Judge the Challenge On');
    }
    else if(this.state.description_3==''){
      alert('Please enter What to Expect with this Challenge');
    }
    else{
      let price                      = challengeInfo.price;
      let challenge_category_type_id = challengeInfo.challenge_category_type_id;
      let challenge_type_id          = challengeInfo.challenge_type_id;
      let name                       = challengeInfo.name;
      let start_date                 = challengeInfo.start_date;
      let title                      = challengeInfo.title;
      let zip_code                   = challengeInfo.zip_code;
      let is_published               =false;

     this.props.editChallenge('stepThreeSubmit',price,name,zip_code,challenge_category_type_id,start_date,challenge_type_id,challengeInfo._id,description, description_2, description_3,is_published);

   // NavigationActions.stepFour(challengeData);
    }
  }

  inputFocused(ref) {
   this._scroll(ref, 400);
 }

 inputBlurred(ref) {
   this._scroll(ref, 0);
 }

 _scroll(ref, offset) {
   setTimeout(() => {
     var scrollResponder = this.refs.myScrollView.getScrollResponder();
     scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
              findNodeHandle(this.refs[ref]),
                offset,
                true
            );
     });
  }

  render() {

    let challengeInfo = this.props.challengeInfo;

    return (
      <ScrollView ref="myScrollView" keyboardDismissMode='interactive' style={[styles.mainContainer,{padding:10}]}>
      { this.state.fetching &&
             <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
           <GiftedSpinner size='large' />
            </View>
            }
      <Text style={{marginTop:5,marginLeft:2,fontWeight: 'bold',fontSize:15}}>Enter Your Challenge Description </Text>
      <Text style={{marginTop:5,marginLeft:2,color:'#4d4d4d',fontSize:12}}>Describe your challenge , include what you are going to do, what you should be judged on and what to expect in the end.</Text>
      <View style={{marginTop:10}}>
      <Text style={{paddingBottom:3}}>Challenge Description:</Text>
      <TextInput
        ref="description"
        style={{height: 80, borderColor: '#c2c2c2', borderWidth: 1,marginBottom:15,paddingLeft:5}}

        onChangeText={(text) => this.setState({description:text})}
        value={this.state.description}
        onFocus={this.inputFocused.bind(this, 'description')}
        onBlur={this.inputBlurred.bind(this, 'description')}
          returnKeyType='next'
          onSubmitEditing={() => this.refs.description_2.focus()}
        // placeholder="Please enter your challenge details. Describe what you're going to do and how you're going to do it. These should be the preparation steps to achieve your challenge goals. The more you detail, the more your viewers will want to help fund your cause! Be Creative!"
      />
      </View>
      <View style={{marginTop:10}}>
      <Text style={{paddingBottom:3}}>What to Judge the Challenge On:</Text>
      <TextInput
        ref="description_2"
        style={{height: 80, borderColor: '#c2c2c2', borderWidth: 1,marginBottom:15,paddingLeft:5}}

        onChangeText={(text) => this.setState({description_2:text})}
        value={this.state.description_2}
        onFocus={this.inputFocused.bind(this, 'description_2')}
        onBlur={this.inputBlurred.bind(this, 'description_2')}
        returnKeyType='next'
        onSubmitEditing={() => this.refs.description_3.focus()}
        // placeholder="Explain what your viewers are going to judge you on. This should match exactly what you will be recording yourself in the video. Be brief and only express what the video will show. Remember, the video should only be 1 minute long so make sure you capture the importance of your challenge within that timeframe!"
      />
      </View>
      <View style={{marginTop:10}}>
      <Text style={{paddingBottom:3}}>What to Expect with this Challenge:</Text>
      <TextInput
        ref="description_3"
        style={{height: 80, borderColor: '#c2c2c2', borderWidth: 1,marginBottom:15,paddingLeft:5}}
        returnKeyType='done'
        onChangeText={(text) => this.setState({description_3:text})}
        value={this.state.description_3}
        onFocus={this.inputFocused.bind(this, 'description_3')}
        onBlur={this.inputBlurred.bind(this, 'description_3')}
        //  onSubmitEditing={this.handleStepFour.bind(this,challengeInfo) }
        // placeholder="If you win the challenge, what are you going to do with the winnings? Your funders would like to know why you're doing the challenge and what you're going to do with the money once you win! This will help raise your funds so be descriptive!"
      />
      </View>

      {/*   <RoundedButton onPress={this.handleStepFour.bind(this,challengeInfo)}>
           Preview this Challenge!
        </RoundedButton> */}
        <TouchableOpacity style={{
        marginTop:15,
        justifyContent: 'center',
        alignItems: 'center',
        height: 35,
        backgroundColor:Colors.matchingColor,}}
        onPress={this.handleStepFour.bind(this,challengeInfo)}>
                <Text style={{color: Colors.background,
          fontSize:15,}}> Preview this Challenge!</Text>

              </TouchableOpacity>


      </ScrollView>
    );
  }
}

const mapStateToProps = (state,props) => {
  console.log("res got!!");
  console.log(state);
      return {
        status:state.editChallenge.status,
        steptwo:state.editChallenge.steptwo,
        stepthree:state.editChallenge.stepthree,
        stepfour:state.editChallenge.stepfour,
        default_type:state.editChallenge.default_type,
        fetching: state.editChallenge.fetching,
        payload: state.editChallenge.payload,
        error: state.editChallenge.error,

        }

}

const mapDispatchToProps = (dispatch) => {
  console.log("api called!!");
  return {
  editChallenge:(default_type = 'stepThreeSubmit',price,name,zip_code,challenge_category_type_id,start_date,challenge_type_id,challenge_id,description, description_2, description_3,is_published)=> dispatch(EditChallengeActions.editChallengeRequest(default_type,price,name,zip_code,challenge_category_type_id,start_date,challenge_type_id,challenge_id,description, description_2, description_3,is_published))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StepThree)
