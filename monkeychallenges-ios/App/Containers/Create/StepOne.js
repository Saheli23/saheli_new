// @flow

import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  KeyboardAvoidingView,
  Image,
  ListView,
  View,
  TextInput,
  findNodeHandle,
  TouchableOpacity,
  Picker,
  Dimensions,
  Alert,
  Platform,
  AsyncStorage
} from 'react-native';
const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux';
// Add Actions - replace 'Your' with whatever your reducer is called :)

import { Metrics, Images,Colors } from '../../Themes';
import RoundedButton from '../../Components/RoundedButton'
// external libs
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions as NavigationActions} from 'react-native-router-flux';
import Animatable from 'react-native-animatable';


import styles from '../Styles/StepFormStyle';

// alert message
import AlertMessage from '../../Components/AlertMessage';

// I18n
import I18n from 'react-native-i18n';

// progressBar
import * as Progress from 'react-native-progress';

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner';
import * as _ from 'lodash';
import DatePicker from 'react-native-datepicker'
import ModalPicker from 'react-native-modal-picker'
import ChallengeCategoryActions from '../../Redux/ChallengeCategoryRedux'
import CreateChallengeActions   from '../../Redux/CreateChallengeRedux'
{/*

// redux form test not working

import { reduxForm } from 'redux-form'
import {
  ActionsContainer,
  Button,
  FieldsContainer,
  Fieldset,
  Form,
  FormGroup,
  Label,
} from 'react-native-clean-form'
import {
  Input,
  Select,
  Switch
} from 'react-native-clean-form/redux-form-immutable'

 */}
let index = 0;
const minFund=5.00;
const maxFund=9999;
const countryOptions = [
  {key: index++,label: 'Denmark', value: 'DK'},
  {key: index++,label: 'Germany', value: 'DE'},
  {key: index++,label: 'United State', value: 'US'}
]

type StepOneProps = {
  dispatch: () => any,
  fetching: boolean,
  payload: object,
  getChallengeCategory:() => void,

};

class StepOne extends Component {
  props: StepOneProps
	constructor(props) {
    super(props);
    this.state = {
      minDateform:new Date(),
      challenge_goal: '',
      price:'',
      name:'',
      zip_code:'',
      start_date:'',
      challenge_category_type_id:'',
      new_start_date:'',
      LastPath:''

   };
  }

  componentWillMount(){
  //this.props.getChallengeCategory();

  this.props.createChallenge('category');
  }

  componentWillReceiveProps(newProps) {

    //this.setState({price: ''});
    console.log("BeforenewProps", newProps, this.props);
    AsyncStorage.getItem('FromHome').then((value) =>{
      this.setState({LastPath:value});
      console.log('lastpath',this.state.LastPath);
    })
    //this.setState({price: ''});
    // let cat_arr = []
    // if(!newProps.fetching){
    //     newProps.payload.data.map((data,index) => {
    //       console.log(data,index);
    //       cat_arr.push({key:data.id,lable:data.name})
    //     })
    //     this.setState({ fetching: newProps.fetching, data:cat_arr });
    //      console.log(cat_arr);
    //      console.log(this.state.data);
    // }else{

    // }
    // this.setState({

    //   fetching:newProps.fetching,
    //   payload:newProps.payload,
    //   error:newProps.error

    // })
    //  this.setState ({
    //
    //    challenge_goal: '',
    //    price:'',
    //    name:'',
    //    zip_code:'',
    //    start_date:'',
    //    challenge_category_type_id:'',
    //    new_start_date:'',
    //
    // });
    // if(!newProps.default_type || newProps.default_type !='stepOneSubmit'){
    //    console.log("newProps.cat=true",newProps);
    //   this.setState ({
    //
    //     challenge_goal: '',
    //     price:'',
    //     name:'',
    //     zip_code:'',
    //     start_date:'',
    //     challenge_category_type_id:'',
    //     new_start_date:'',
    //
    //  });
    // }


    if (newProps.default_type == 'nothing' && !newProps.stepone && newProps.status == 'nothing' && this.state.LastPath =='home') {
      this.setState({

        challenge_goal: '',
        price: '',
        name: '',
        zip_code: '',
        start_date: '',
        challenge_category_type_id: '',
        new_start_date: '',

      });
    }
    if(newProps.default_type == 'stepFourSubmit' && this.state.LastPath =='home'){
      this.setState({

        challenge_goal: '',
        price: '',
        name: '',
        zip_code: '',
        start_date: '',
        challenge_category_type_id: '',
        new_start_date: '',

      });
    }
    if (newProps.stepone) {


      console.log("newProps", newProps);
      if (newProps.error) {
        alert('error: Something went wrong! Could not create new challenge.');
      } else {

            this.setState({
              fetching: newProps.fetching,
              payload: newProps.payload,
              error: newProps.error
            });
        if (newProps.default_type == 'stepOneSubmit' && newProps.payload && newProps.status == 'success') {
          // alert('in');
          // alert(newProps.default_type);
          // alert(newProps.name);
            NavigationActions.stepTwo({
              challengeid: newProps.payload.data._id,
              uuid: newProps.payload.data.uuid,
              challengeinfo: newProps.payload.data
            })
            console.log("newProps", newProps);
            //AsyncStorage.setItem('AtHome', 'nothome');
            this.setState({LastPath:'nothome'});
            this.props.createChallenge('nothing');
          }

      }
    } else if (newProps.cat) {

          if (newProps.default_type != 'stepOneSubmit') {
            console.log("newProps.cat=true", newProps);
            this.setState({

              challenge_goal: '',
              price: '',
              name: '',
              zip_code: '',
              start_date: '',
              challenge_category_type_id: '',
              new_start_date: '',

            });
          }
          this.setState({
            fetching: newProps.fetching,
            payload: newProps.payload,
            error: newProps.error
          });

    }

    // else if(newProps.payload.data != null){
    //    this.setState({ fetching: newProps.fetching, payload: newProps.payload, error:newProps.error});

    //     NavigationActions.stepTwo({ challengeid: newProps.payload.data._id ,uuid:newProps.payload.data.uuid})
    // }
    // else{
    //   this.setState({ fetching: newProps.fetching, payload: newProps.payload, error:newProps.error});

    // }

  }

	handleStepOne = () => {
		//alert('test step1');
    const {price, name, zip_code, challenge_category_type_id} = this.state

    if (this.state.price<minFund || this.state.price>maxFund)
    {

      Alert.alert(
            'Error Message',
            'Price should not be empty and not less than $5 and greater than $9,999',
          )
    }
    else if(this.state.name==''){
      alert('Challenge name is required.');
    }
    else if(this.state.zip_code==''){
      alert('zip code  is required.');
    }
    else if(this.state.zip_code.length<5){
      alert('zip code must be 5 digits');
    }
    else if(this.state.start_date==''){
      alert('Challenge Date is required');
    }
    else if(this.state.challenge_category_type_id==''){
      alert('Challenge Category is required');
    }
    else{
      let new_date=this.state.start_date;
      var replace = /\//g;
      var joinwith = `-`;
      let newSelDate = new_date.replace(replace, joinwith);
      console.log(newSelDate);
      var dateArr =newSelDate.split("-");
      let timedate=dateArr[2]+'-'+dateArr[0]+'-'+dateArr[1]+' '+"23:59:00";
      console.log(timedate);
      //let timedate=new_date+' '+"23:59:00"
     // this.setState({start_date:timedate});
      //console.log(price, name, zip_code, challenge_category_type_id,timedate);
      this.props.createChallenge('stepOneSubmit',price, name, zip_code, challenge_category_type_id,timedate);
      // NavigationActions.stepTwo()
    }



	}

inputFocused(ref) {
   this._scroll(ref, 150);
 }

 inputBlurred(ref) {
   this._scroll(ref, 0);
 }

 _scroll(ref, offset) {
   setTimeout(() => {
     var scrollResponder = this.refs.myScrollView.getScrollResponder();
     scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
              findNodeHandle(this.refs[ref]),
                offset,
                true
            );
     });
  }
  render() {
  	const { handleSubmit, submitting } = this.props

   let index = 0;

        // const data = [
        //     { key: index++, label: 'Business' },
        //     { key: index++, label: 'Charity' },
        //     { key: index++, label: 'Community' },
        //     { key: index++, label: 'Creative' },
        //     { key: index++, label: 'Events' },
        //     { key: index++, label: 'Faith' },
        //     { key: index++, label: 'Family' },
        //     { key: index++, label: 'National News' },
        //     { key: index++, label: 'Newlywed' },
        //     { key: index++, label: 'Other' },
        //     { key: index++, label: 'Travel' },
        //     { key: index++, label: 'Wishes' }

        // ];
     //  let cat_data = [];
       let payload = (this.state && this.state.payload) || { cat_data: [],data: [] };
     // // console.log("payload",payload.cat_data);
     //  cat_data = payload.cat_data.map(function (row,index){
     //      return {key:row.key,label:row.lable}
     //  });
     // console.log(cat_data);
       console.log(payload);
       console.log(payload.cat_data);

    return (
      <ScrollView ref="myScrollView" keyboardDismissMode='interactive' style={[styles.mainContainer,{padding:10}]}>
          {/*

              <RoundedButton onPress={() => this.handleStepOne()}>
                   Continue to STEP 2
                </RoundedButton>

                { this.state.fetching &&
                  <View style={{backgroundColor:'rgba(0,0,0,0)',width: width,
                 height: height,justifyContent: 'center',alignItems: 'center',}}>
                <GiftedSpinner size='large' />
                 </View>
                 }

         */}


      { this.state.fetching && Platform.OS === 'ios' &&
             <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
           <GiftedSpinner size='large' />
            </View>
            }
      {this.state.fetching && Platform.OS ==='android' &&
            <View style={{backgroundColor:'rgba(0,0,0,0)',top: 0, bottom: 0, left: 0, right: 0,justifyContent: 'center',alignItems: 'center',}}>
             <GiftedSpinner size='large' />
             </View>
          }

  		<View>
  		<Text style={{fontWeight: 'bold',fontSize:15}}>Post a Challenge</Text>
  		</View>

  		<View style={{marginTop:10}}>
  		<Text>Enter your Challenge Goal:</Text>
  		<Text>Minimum price is $5 and Maximum price is $9,999</Text>
  		<TextInput
        style={{height: 40,paddingLeft:5, borderColor: '#c2c2c2', borderWidth: 1,backgroundColor:'#ceffc8',marginBottom:15}}
        onChangeText={(number) => this.setState({price:number})}
        returnKeyType='next'

        value={this.state.price}
        keyboardType = 'numeric'
        placeholder="$100"
      />
  		</View>


  		<View>
  		<Text>Name of your Challenge:</Text>
  		<TextInput
        ref='name_of_challenge'
        style={{height: 40,paddingLeft:5, borderColor: '#c2c2c2', borderWidth: 1,marginBottom:15}}
        onChangeText={(text) => this.setState({name:text})}
        value={this.state.name}
          returnKeyType='next'
          onSubmitEditing={() => this.refs.zip_code.focus()}
        placeholder="i.e.: Climb Mount Everest"
      />
  		</View>

  		<View>
  		<Text>Zip Code:</Text>
  		<TextInput
        ref="zip_code"
        style={{height: 40, paddingLeft:5, borderColor: '#c2c2c2', borderWidth: 1,marginBottom:15}}
        onChangeText={(text) => this.setState({zip_code:text})}
        value={this.state.zip_code}
        maxLength={5}
        keyboardType = 'numeric'
        onFocus={this.inputFocused.bind(this, 'zip_code')}
        onBlur={this.inputBlurred.bind(this, 'zip_code')}
        placeholder="Keep you connected locally"
      />
  		</View>


  		<View>
  		<Text>Challenge Date:</Text>

        <DatePicker
            style={{width: 200,}}
            date={this.state.start_date}
            mode="date"
            placeholder="select date"
            format="MM/DD/YYYY"
            minDate={this.state.minDateform}
            maxDate="2016-06-01"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0
              },
              dateInput: {
                marginLeft: 36,
                borderColor: '#c2c2c2',
              },

              // ... You can check the source to find the other keys.
            }}
            onDateChange={(date) => {this.setState({start_date: date})}}
          />
  		</View>
	<View style={{flex:1,}}>
  		<Text style={{marginTop:15}}>Challenge Category:</Text>
  	    { /* Default mode: a clickable button will re rendered */ }

          <ModalPicker
                    // data={this.state.payload.cat_data}
                    data={payload.cat_data}
                    style={{ flex: 1 }}
                    initValue="Select challenge Category!"
                    onChange={(option)=>{
                      this.setState({challenge_category_type_id:option.key})
                      console.log(option)


                    }}>

            </ModalPicker>


  		</View>


    {/*  <RoundedButton  onPress={() => this.handleStepOne()}>
           Continue to STEP 2
        </RoundedButton> */}
        <TouchableOpacity style={{
        marginTop:15,
        justifyContent: 'center',
        alignItems: 'center',
        height: 35,
        backgroundColor:Colors.matchingColor,}}
        onPress={()=>{
      this.handleStepOne();
        //  NavigationActions.SearchResultView();
        }}>
                <Text style={{color: Colors.background,
          fontSize:15,}}> Continue to STEP 2</Text>

              </TouchableOpacity>


      </ScrollView>



    );
  }
}


// StepOne = reduxForm({
// 	form: 'StepOne',
//   validate: values => {
//     const errors = {}

//     values = values.toJS()

//     if (!values.first_name) {
//       errors.first_name = 'First name is required.'
//     }
//     if(isNaN(values.first_name)){
//      errors.first_name = 'required number.'
//     }

//     if (!values.last_name) {
//       errors.last_name = 'Last name is required.'
//     }

//     if (!values.email) {
//       errors.email = 'Email is required.'
//     }

//     return errors
//   }
// })(StepOne)

const mapStateToProps = (state,props) => {
   console.log("res got!!");
  console.log(state);
      return {
        status:state.createChallenge.status,
        cat:state.createChallenge.cat,
        stepone:state.createChallenge.stepone,
        steptwo:state.createChallenge.steptwo,
        default_type:state.createChallenge.default_type,
        fetching: state.createChallenge.fetching,
        payload: state.createChallenge.payload,
        error: state.createChallenge.error,

        }

}

const mapDispatchToProps = (dispatch) => {
  console.log("api called!!");
  return {
 // getChallengeCategory: () => dispatch(ChallengeCategoryActions.challengeCategoryRequest()),
  createChallenge:(default_type = 'stepOneSubmit',price, name, zip_code, challenge_category_type_id,start_date)=> dispatch(CreateChallengeActions.createChallengeRequest(default_type,price, name, zip_code, challenge_category_type_id,start_date))
  }
}

 StepOne = connect(mapStateToProps, mapDispatchToProps)(StepOne)

 export default StepOne
