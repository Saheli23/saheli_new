// @flow

import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  KeyboardAvoidingView,
  Image,
  ListView,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput
} from 'react-native';
import { connect } from 'react-redux';
const { width, height } = Dimensions.get('window')
// Add Actions - replace 'Your' with whatever your reducer is called :)

import { Metrics, Images } from '../../Themes';
import RoundedButton from '../../Components/RoundedButton'
// external libs
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions as NavigationActions } from 'react-native-router-flux';
import Animatable from 'react-native-animatable';


//import styles from '../Styles/StepFormStyle';
import styles from '../Styles/JudgingChallengesViewStyle'
// alert message
import AlertMessage from '../../Components/AlertMessage';

// I18n
import I18n from 'react-native-i18n';

// progressBar
import * as Progress from 'react-native-progress';

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner';
import * as _ from 'lodash';
import CheckBox from 'react-native-checkbox';
import EditChallengeActions   from '../../Redux/EditChallengeRedux'

type StepFourProps = {
  dispatch: () => any,
  fetching: boolean,
  payload: object,


};
class StepFour extends Component {
    props: StepThreeProps
  constructor(props) {
    super(props);
    this.state = {
    terms: false,
    imageSource:null,
    challengeDetails:null,
   };

  }
  componentWillMount(){
     let challengeData = this.props.imageSource;
     this.setState({
             imageSource: challengeData,
           });
     let itemData = this.props.challengeInfo;
     this.setState({
             challengeDetails: itemData,
           });
  }

  componentWillReceiveProps (newProps) {

    console.log("BeforenewProps",newProps);
    if(newProps.stepfour){
    if(newProps.error){
     alert('error: Something went wrong! Could not create new challenge.');
    }else{

        this.setState({ fetching: newProps.fetching, payload: newProps.payload, error:newProps.error});
        if(newProps.default_type =='stepFourSubmit' && newProps.payload && newProps.status == 'success' ){
           NavigationActions.stepFive({ challengeInfo: this.state.challengeDetails,imageSource:this.state.imageSource})
         // NavigationActions.stepFive();
        }
    }
    }

   }
   checkBoxChanged()
    {
    this.state.terms?this.setState({terms: false}):this.setState({terms: true});
    }

    handleStepFive = (itemData) => {
      if(!this.state.terms){
         alert('Please check our terms of use');
      }
      else{

      let price                      = itemData.price;
      let challenge_category_type_id = itemData.challenge_category_type_id;
      let challenge_type_id          = itemData.challenge_type_id;
      let name                       = itemData.name;
      let start_date                 = itemData.start_date;
      let title                      = itemData.title;
      let zip_code                   = itemData.zip_code;
      let description                = itemData.description;
      let description_2              = itemData.description_3;
      let description_3              = itemData.description_2;
      let is_published               = true;
      this.props.editChallenge('stepFourSubmit',price,name,zip_code,challenge_category_type_id,start_date,challenge_type_id,itemData._id,description, description_2, description_3,is_published);
      }

    //alert('test step1');
    // NavigationActions.stepFive()
  }
  render() {
    let itemData = this.props.challengeInfo;
    let imagePath = this.props.imageSource;
    console.log(itemData);
    return (

      <ScrollView ref="myScrollView" keyboardDismissMode='interactive' style={styles.mainContainer}>

          { this.state.fetching &&
             <View style={{backgroundColor:'rgba(0,0,0,0)',position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,zIndex:9999,justifyContent: 'center',alignItems: 'center',}}>
           <GiftedSpinner size='large' />
            </View>
            }
      <View>
      <View style={{justifyContent:'flex-start',alignItems:'flex-start',padding:15,}}>
          <Text style={{fontWeight: 'bold',}}>Preview Challenge</Text>
      </View>

<View style={{backgroundColor:'#FFFFFF'}}>

     <View style={{marginBottom: 6,backgroundColor:'#DEDEDE'}}>

                <View style={{height: 180,position: 'relative',marginTop:15,}}>

                            <View style={{height: 180 }}>
                              <Image style={{height: 180 }} source={{uri: imagePath}} />
                            </View>

                            <View style={styles.imageFooter}>

                              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                                   <Progress.Bar progress={(Math.round(itemData.percentage_funded) / 100)} color={'#33CC32'} height={8} width={(width - 20)} borderWidth={0} unfilledColor={'rgba(128,132,131,0.8)'}/>


                              <View style={styles.contentVoteSection}>
                              <Text style={{backgroundColor: 'rgba(0,0,0,0)',color: 'white',fontWeight:'bold' }}>${parseInt(itemData.total_amount_funded)}<Text style={{fontSize:10}}> OF ${parseInt(itemData.price)} FUNDED</Text></ Text>
                              {/*
                              <Text style={{color: 'white'}}>{Math.round(itemData.percentage_funded)}%</Text>
                              */}
                              <Text style={{backgroundColor: 'rgba(0,0,0,0)',color: 'white',fontWeight:'bold', textAlign: 'right'}}>{itemData.days_remaining} <Text style={{fontSize:10}}>DAYS LEFT</Text></Text>



                              </View>
                              </View>

                            </View>

                  <View style={styles.imageFooterOpacity} />

                </View>




      <View style={[styles.contentSection,styles.contentDescriptionSection,{flexDirection:'column',width:(width - 20),alignSelf:'center',}]}>
         <View style={{flexDirection:'row',width:(width - 20),}}>
                    <View>
                        <Text style={{color: '#1266AF',fontWeight:'bold',flexWrap: 'wrap'}}>{itemData.handle_name} :
                        <Text numberOfLines={2} style={{color: '#000000',fontWeight:'bold',}}> {itemData.name}</Text></Text>
                        <Text>{itemData.description}</Text>
                    </View>

          </View>
      </View>


{/*
            <View style={{justifyContent:'flex-start',alignItems:'flex-start',paddingTop:5,paddingLeft:5}}>
              <View>
                <Text numberOfLines={3}><Text style={{color: '#337ab7'}}>{itemData.handle_name} : </Text>{itemData.name}</Text>

              </View>
              <View>
                <Text numberOfLines={3}><Text style={{color: '#ff6633'}}>Challenge Description : </Text>{itemData.description}</Text>
              </View>
            </View>
*/}

</View>





      <View style={{marginTop:20,width:(width - 20),alignSelf:'center'}}>
        <View style={{marginTop:20,marginBottom:20,height:18,flexDirection: 'row',marginLeft:5,}}>
         <CheckBox
         checkboxStyle={{width:15,height:15,}}
         label=''
         checked={this.state.terms}
         onChange={this.checkBoxChanged.bind(this)}
         />
         <Text style={{fontSize:11,flexWrap:'wrap',marginLeft:-10}} >Check this box to acknowledge the <Text  style={{color:'#ff6633',textDecorationLine:'underline'}} onPress={NavigationActions.terms}>terms of use
         </Text></Text>
         </View>
         <TouchableOpacity style={{backgroundColor:'#F16622',alignItems:'center',justifyContent:'center',padding:10}} onPress={() => this.handleStepFive(itemData)}>
         <Text style={{color:'white',fontWeight:'bold'}}>Post this Challenge!</Text>
         </TouchableOpacity>
         {/*
      <RoundedButton onPress={() => this.handleStepFive(itemData)}>
           Post this Challenge!
        </RoundedButton>
        */}
      </View>
    </View>
    </View>
     </ScrollView>
    );
  }
}

const mapStateToProps = (state,props) => {
  console.log("res got!!");
  console.log(state);
      return {
        status:state.editChallenge.status,
        steptwo:state.editChallenge.steptwo,
        stepthree:state.editChallenge.stepthree,
        stepfour:state.editChallenge.stepfour,
        default_type:state.editChallenge.default_type,
        fetching: state.editChallenge.fetching,
        payload: state.editChallenge.payload,
        error: state.editChallenge.error,

        }

}

const mapDispatchToProps = (dispatch) => {
  console.log("api called!!");
  return {
  editChallenge:(default_type = 'stepFourSubmit',price,name,zip_code,challenge_category_type_id,start_date,challenge_type_id,challenge_id,description, description_2, description_3,is_published)=> dispatch(EditChallengeActions.editChallengeRequest(default_type,price,name,zip_code,challenge_category_type_id,start_date,challenge_type_id,challenge_id,description, description_2, description_3,is_published))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StepFour)
