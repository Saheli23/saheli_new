// @flow

import React from 'react'
import {
    View,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    Image,
    Keyboard,
    LayoutAnimation,
    WebView,
    Linking,AsyncStorage
} from 'react-native'
import {connect} from 'react-redux'
//import Styles from '../Styles/LoginScreenStyle'
import styles from './Styles/WebLinksScreenStyle'

import {Images, Metrics} from './../Themes'
import LoginActions from './../Redux/LoginRedux'
//import FbLoginActions from '../../Redux/FbLoginRedux'
import {Actions as NavigationActions} from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import WebViewBridge from 'react-native-webview-bridge';

// gif spinner
import GiftedSpinner from 'react-native-gifted-spinner'
import { StyleSheet } from 'react-native'
import Toast, {DURATION} from 'react-native-easy-toast'

class FacebookShare extends React.Component {
	constructor (props) {
    super(props)

     this.state = { canGoBack: false,MYuuid:"",sentfrom:"",mail:"" };
  }


componentWillMount(){
  this.getMYuuid()

 let {mail} = this.state  ;
 console.log('itemdata',this.props.itemData);
  let {status, _id,name}  = this.props.itemData;

   
   
   //this.setState({challengeLink:challengeLink1})
  

  }

getMYuuid() {
      AsyncStorage.getItem('MCUserObj').then((value) =>{
        let userObj = JSON.parse(value);
        console.log(userObj)
        //this.setState({sentfrom:userObj.email});
        this.setState({mail:userObj.email})
        console.log("mail1",this.state.mail);
      })
   }
 
	render() {
    let {mail} = this.state   // myuuid where you are using? 
    let {status, _id,name}   = this.props.itemData;
    let challengeName        =name.replace(/\s/g, "%20");
    //let newShareLink         =`http://www.monkeychallenges.com/#/challenges/${status}/${_id}`
   
    let newShareLink         =`http://oneup-web-env-1.us-west-2.elasticbeanstalk.com/#/challenges/${status}/${_id}`
    let challengeLink        = `https://www.facebook.com/sharer/sharer.php?u=${newShareLink}?u=${newShareLink}`

  // https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Foneup-web-env-1.us-west-2.elasticbeanstalk.com%2F%23%2Fchallenges%2Fopen%2F5926b12f741c3818a901bd22?u=http%3A%2F%2Foneup-web-env-1.us-west-2.elasticbeanstalk.com%2F%23%2Fchallenges%2Fopen%2F5926b12f741c3818a901bd22
   
		
    return (
     <View style={styles.mainContainer}>
     {/*
     <TouchableOpacity

            onPress={this.onBack.bind(this)}
            >
        <View style={styles.topbar}>

            <Text  style={this.state.canGoBack ? styles.topbarText : styles.topbarTextDisabled}>Go Back To App</Text>

        </View>
        </TouchableOpacity>
        https://dev-1.monkeychallenges.com/#/myaccount/this.state.MYuuid/password
          //  source={{uri: "https://dev-1.monkeychallenges.com/#/login"}}/>
        */}

       <View style={{backgroundColor:'#f9642f',justifyContent: 'center', alignItems: 'center',marginLeft:5,marginRight:5,padding:10,marginBottom:5,marginTop:5}} ><Text style={{color:'white'}} >After Posting, please go back to challenge using back button in header</Text></View>
      <WebViewBridge
        ref="webviewbridge"
        onNavigationStateChange={this.onNavigationStateChange.bind(this)}
        onBridgeMessage={this.onBridgeMessage.bind(this)}
        startInLoadingState={true}
        source={{uri: challengeLink}}/>
       
        <Toast
            ref="toast"
            style={{backgroundColor:'#f9642f'}}
            position='bottom'
            positionValue={100}
            fadeInDuration={600}
            fadeOutDuration={1000}
            opacity={1}
            textStyle={{color:'white'}}
        />

	     </View>
       
       );}


onNavigationStateChange(navState) {
    this.setState({
      canGoBack: navState.canGoBack
    });
  }

	onBridgeMessage (webViewData) {
    let jsonData = JSON.parse(webViewData);
		 console.log(jsonData);
	  //   let jsonData = JSON.parse(webViewData);
   //      if(jsonData.signupsuccess){
   //      	console.log('username', jsonData.username);
   //      	console.log('password', jsonData.password);
   //      	this.username =jsonData.username;
   //      	this.password =jsonData.password;

   //      }
	    if (jsonData.success) {
	      //alert(jsonData.message);
        this.refs.toast.show('Shared successfully, please go back to challenge using back button in header.',1000);
	      console.log('data received', jsonData.message);
	     
	    }
	  //   console.log('data received', webViewData, jsonData);
	    //.. do some react native stuff when data is received
	  }


}


const mapStateToProps = (state) => {
  console.log(state)
  return {
          // fetching: state.login.fetching,
          // access_token: state.login.access_token
        }



}

const mapDispatchToProps = (dispatch) => {
  return {
   // attemptLogin: (username, password, facebook_token, loginType) => dispatch(LoginActions.loginRequest(username, password, facebook_token, loginType))
    //attemptFbLogin: (facebook_token) => dispatch(FbLoginActions.fbLoginRequest(facebook_token))

  }
}


export default connect(mapStateToProps, mapDispatchToProps)(FacebookShare)
