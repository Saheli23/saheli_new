// @flow
import React from 'react'
import {StyleSheet, ScrollView, Text, KeyboardAvoidingView, Image, ListView, View, Platform, TouchableOpacity, Animated,Dimensions, TextInput,AsyncStorage} from 'react-native'
const { width, height } = Dimensions.get('window')
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import { Actions as NavigationActions } from 'react-native-router-flux'
import RoundedButton from '../Components/RoundedButton'
// progressBar
import * as Progress from 'react-native-progress'

// detail view components
import {StoryPagerView, CommentsPagerView, FundersPagerView} from './DetailComponents/PagerItemView'


import {IndicatorViewPager, PagerTitleIndicator} from 'rn-viewpager'

import NavItems from '../Navigation/NavItems'

import { Colors, Metrics, Images } from '../Themes'
// Styles
//import styles from './Styles/ChallengeDetailViewStyle'
import styles from './Styles/OpenChallengesDetailViewStyle'

import AlertMessage from '../Components/AlertMessage'
import Icon from 'react-native-vector-icons/FontAwesome'

import { ActionSheetCustom as ActionSheet } from 'react-native-actionsheet'
import OpenChallengesActions from '../Redux/OpenChallengesRedux'
import * as _ from 'lodash';
import TrendingActions from '../Redux/TrendingRedux'
let moment = require('moment');


//TitleIndicatorPage
//OpenChallengeDetailView

class FooterBar extends React.Component{
  constructor (props) {
   super(props)
   this.state = {}
 }

  handleFundChallengeView(itemData) {
    //alert(itemData);
    //console.log(itemData);
    NavigationActions.FundChallenge({item:itemData})
    //this.props.levelSelected(levelNumber);
  }
   pressHandlerShare(itemData) {
    this.props.onDonePressOpen(itemData);
  }




  //let self = this.state;
  update = () => {
    let startTime,
    endTime,
    duration,
    days,
    hours,
    mins;
   //console.log("triggered")
    startTime = moment(new Date()),
    endTime = moment(this.props.itemData.funding_expiration_date),
    duration = moment.duration(endTime.diff(startTime)),
    days = moment().isAfter(this.props.itemData.funding_expiration_date)? 0 : duration.days(),
    hours = moment().isAfter(this.props.itemData.funding_expiration_date)? 0 : duration.hours(),
    mins = moment().isAfter(this.props.itemData.funding_expiration_date)? 0 : duration.minutes();
    //console.log(startTime,this.props.itemData.funding_expiration_date,duration.days(),days,hours,mins);
    this.setState({days,hours,mins})
  }


  // componentDidMount(){
  //   this.update();
  // his._interval = setInterval(this.update, 1000);
  // }

  componentWillMount(){
    this.update();
  this._interval = setInterval(this.update, 1000);
  }

  componentWillUnmount() {
    clearInterval(this._interval);
  }

  render(){



let itemData = this.props.itemData





let {days,hours,mins} = this.state;

    return <View>

    <View style={styles.FooterBarTopBar}>
        <View style={styles.FooterBarTopBarInsideView}>
            <Text>Days</Text>
            <Text style={styles.FooterBarTopBarInsideText}>{days}</Text>
        </View>
        <View style={styles.FooterBarTopBarInsideView}>
            <Text>Hours</Text>
            <Text style={styles.FooterBarTopBarInsideText}>{hours}</Text>
        </View>
        <View style={styles.FooterBarTopBarInsideView}>
            <Text>Minutes</Text>
            <Text style={styles.FooterBarTopBarInsideText}>{mins}</Text>
        </View>
    </View>
    <View style={styles.FooterBarBottomBar}>

          {/* <View style={{flexDirection: 'column', alignItems: 'center',}}>
                <Text style={{fontSize: 20, fontWeight: 'bold',}}>$</ Text>
                <Text>USD</Text>
            </View>
        <View style={{backgroundColor: "#ccc",padding:8}}>
          <Text style={{fontWeight: 'bold',}}>{itemData.price}</Text>*/}
          {/*
          <Text style={{fontWeight: 'bold',}}>{itemData.price}</Text>
            <TextInput
              ref='FundAmount'
              style={{
              width:60,
              height: 20,
              backgroundColor:'rgba(255,255,255,0.3)',
              color: Colors.background,
              fontSize:15,}}
              value={itemData.price}
              keyboardType='default'
              returnKeyType='next'
              autoCapitalize='none'
              placeholder="Fund"
              placeholderTextColor='#FFFFFF' />
              */}
         {/*  </View>*/}
         <View style={{alignItems:'center',paddingLeft:10}}>
        <TouchableOpacity style={{backgroundColor: "#33CC32",padding:8 ,width:265,alignItems:'center',}} onPress={this.handleFundChallengeView.bind(this, itemData)} >
            <Text style={{fontWeight: 'bold',color: 'white'}}>FUND CHALLENGE</Text>
        </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={() => this.pressHandlerShare("test Share")}>
        {/*
            <Icon name='share' size={Metrics.icons.medium} color={Colors.steel} style={{color: Colors.steel, marginRight: Metrics.smallMargin}} />
*/}
            <Image style={{width:30,height:30,marginRight:10}} source={Images.share}/>
        </TouchableOpacity>
    </View>
</View>
  }
}


class FunderView extends React.Component {
  render(){
     let itemData = this.props.itemData
    return <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: itemData.uuid })}} style={{width:(width - 20),flexDirection:'row',alignSelf:'center',paddingTop:5,marginTop:5,marginBottom:5}}>
    <View>
    <Image style={{ height: 50, width:50, borderRadius:25 }}
        source={{uri: itemData.latest_profile_image}}
        defaultSource={require('../Images/person.png')} />
    </ View>
    <View  style={{flex:1,flexDirection:'row',justifyContent: 'space-between',paddingLeft:5,}}>
        <View style={{width:120,flexDirection:'column',alignItems: 'flex-start',}}>
          <View><Text style={{color: '#1266AF',fontWeight:'bold',}}>{itemData.handle_name}</Text></View>
          <Text style={{fontSize:10,color:'#1266AF'}}>{itemData.username}</Text>
        </View>
        <View style={{flexDirection:'column',alignItems: 'flex-end', justifyContent: 'space-between',}}>
        <Text style={{fontWeight:'bold'}}>${itemData.amount_contributed}</Text>
      <Text style={{fontSize:12,color:'#9999A2'}}>{moment(itemData.created_at).startOf('hour').fromNow()}</Text>

        </View>
    </ View>


    </TouchableOpacity>
  }
}

class CommentView extends React.Component {
  render(){
     let itemData = this.props.itemData
    return <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: itemData.uuid })}} style={{width:(width - 20),flexDirection:'row',alignSelf:'center',paddingTop:5,marginTop:5,marginBottom:5}}>
    <View>
    <Image style={{ height: 50, width:50, borderRadius:25 }}
        source={{uri: itemData.latest_profile_image}}
        defaultSource={require('../Images/person.png')} />
    </ View>
      <View style={{paddingLeft:10,flexDirection:'column',alignItems: 'flex-start',position:'absolute',left:50,right:0,top:5}}>
       <Text style={{fontSize:12,color:'#9999A2',position:'absolute',right:0}}>{moment(itemData.created_at).startOf('hour').fromNow()}</Text>
        <View><Text style={{color: '#1266AF',fontWeight:'bold',}}>{itemData.handle_name}</Text></View>
        <Text style={{fontSize:10,color:'#1266AF'}}>{itemData.username}</Text>
        <Text style={{fontSize:12,marginTop:5}}>{itemData.comment}</Text>
        </View>
    </TouchableOpacity>
  }
}

class StoryView extends React.Component {
  render(){
     let itemData = this.props.itemData
    return <ScrollView vertical>
    <View style={{width:(width - 20), alignSelf:'center'}}>
    <View style={{paddingTop:10}}>
        <Text style={{fontSize:12,color:'#878787',fontWeight:'bold'}}>Posted : {moment(itemData.created_at.date).format("MMMM Do, YYYY")}</Text>
    </View>
    <View style={{paddingTop:10,flexDirection: 'row', flexWrap: 'wrap',}}>
        <Text style={{color: 'black',fontSize:13}}>
            <Text style={{color: '#f9642f',fontWeight:'bold'}}>Challenge description : </Text>{itemData.description}</Text>
    </View>
    <View style={{paddingTop:10,flexDirection: 'row', flexWrap: 'wrap'}}>
        <Text style={{color: 'black',fontSize:13}}>
            <Text style={{color: '#f9642f',fontWeight:'bold'}}>What to judge this challenge on : </Text>{itemData.description_3}</Text>
    </View>
    <View style={{paddingTop:10,flexDirection: 'row', flexWrap: 'wrap'}}>
        <Text style={{color: 'black',fontSize:13}}>
            <Text style={{color: '#f9642f',fontWeight:'bold'}}>What to expect with this challenge result : </Text>{itemData.description_2}</Text>
    </View>
    </View>
</ScrollView>

  }
}

const CANCEL_INDEX = 0
const options = [
  'Cancel',
  'Share via email',
  'Share on Facebook',
  'Share on Twitter',
  'Ask the Monkey',
  'Report this Challenge',
]
const title = <Text style={{color: '#000', fontSize: 18}}>Which one do you like?</Text>

class OpenChallengeDetailView extends React.Component{
    animationstate = {
        bgColor: new Animated.Value(0)
    };

   constructor (props) {
    super(props)
    this.state = {
       selected: '',
       user:{}
    }
  }
  componentWillMount () {

  //  this.getMYuuid();
  }
  // getMYuuid() {
  //     AsyncStorage.getItem('MCUserObj').then((value) =>{
  //       let userObj = JSON.parse(value)
  //       this.setState({user:userObj});
  //     })
  //  }
   componentWillReceiveProps(newProps) {
     console.log('newProps',newProps);

   }
   showActionSheet = () => {
    this.ActionSheet.show()
  }
  handlePress = (i) => {
    console.log(i);
    // this.setState({
    //   selected: i
    // },() => {
    //   console.log(this.state.selected);
    //   if(this.state.selected == 1){
    //     console.log("share");
    //   }else if(this.state.selected == 2){
    //     console.log("Ask the Monkey");
    //   }else if(this.state.selected == 3){
    //     console.log("Report this Challenge");
    //   }else{
    //     console.log("cancled....");
    //   }
    // })

   // console.log(i);
  }
  getDateFormet(date){
    return new Date(date);

  }

  componentWillReceiveProps (newProps) {
    console.log(newProps);
  }
 componentWillUpdate(nextProps, nextState) {
  console.log(nextProps, nextState);

}

  handleOpenShareData = itemData => {
    //console.log(itemData);
    this.showActionSheet()
  };
    _setBgColor = Animated.event([{bgColor: this.animationstate.bgColor}]);


    render() {
      let ownChallenge =false;
      let populerPageRefresh = false;
      let itemData = this.props.item;
      let userId   =this.props.userId;
      let getOpenChallenges = this.props.getOpenChallenges;
      if(this.props.myChallenge && this.props.myChallenge == 'true'){
        ownChallenge = true
      }
      else {
        ownChallenge =false
      }
      let getTrending = this.props.getTrending;
      if(this.props.populerPageRefresh && this.props.populerPageRefresh == 'true'){
        populerPageRefresh = true;
      }
      else{
        populerPageRefresh = false;
      }
      //  console.log(this.state);
      //  let {user:{uuid}} = this.state;
      //  console.log('uuid',uuid);

      // let itemData.created_at.date = this.props.item.created_at.date
        //console.log(itemData);
        let bgColor = this.animationstate.bgColor.interpolate({
            inputRange: [0, 1, 2],
            outputRange: ['hsl(187, 74%, 47%)', 'hsl(89, 47%, 54%)', 'hsl(12, 97%, 59%)']
        });
        return (
     <View style={styles.mainContainer}>
     <ActionSheet
          ref={o => this.ActionSheet = o}
          options={options}
          cancelButtonIndex={CANCEL_INDEX}
          onPress={(i)=>{
                    this.setState({
              selected: i
            },() => {
              console.log(this.state.selected);
              if(this.state.selected == 1){
                console.log("Share via email",itemData);
                NavigationActions.sharechallenge({ itemData: itemData });
              }
              else if(this.state.selected == 2){
                NavigationActions.facebookshare({ itemData: itemData })
              }
              else if(this.state.selected == 3){
                NavigationActions.twittershare({ itemData: itemData })
              }
              else if(this.state.selected == 4){
                console.log("Ask the Monkey");
              }else if(this.state.selected == 5){
                console.log("Report this Challenge",itemData);
                NavigationActions.report({ itemData: itemData });
              }else{
                console.log("cancled....");
              }
            })

          }}
        />
            <ScrollView vertical>
               <ChallengeViewPreState itemData={itemData} userId={userId} getOpenChallenges={getOpenChallenges} ownChallenge={ownChallenge} getTrending={getTrending} populerPageRefresh={populerPageRefresh} />
                <IndicatorViewPager
                    style={{flex: 1, flexDirection: 'column-reverse',minHeight:400}}
                    indicator={this._renderTitleIndicator(itemData)}
                    onPageScroll={this._onPageScroll.bind(this)}
                >
          <View style={{ flex:1,}}>
           <StoryView itemData={itemData} />
          </View>

         <View style={{ flex:1}}>

              {itemData.challenge_comments.length < 1 &&

                 <AlertMessage title='No comments Here, Move Along' show={(itemData.challenge_comments.length < 1)} />

              }

            {
                        itemData.challenge_comments.map((rowData, index) => {
                          return <CommentView itemData={rowData} key={index} />
                        })
            }

          </View>

          <View style={{ flex:1}}>

          {itemData.challenge_challengers.length < 1 &&

             <AlertMessage title='No Funders Here, Move Along' show={(itemData.challenge_challengers.length < 1)} />

          }

            {
                        itemData.challenge_challengers.map((rowData, index) => {
                          return <FunderView itemData={rowData} key={index} />
                        })
            }

          </View>
  </IndicatorViewPager>


  </ScrollView>
   <FooterBar  onDonePressOpen={itemData =>
                        this.handleOpenShareData(itemData)} itemData={itemData} />
  </View>


        );
    }




    _renderTitleIndicator(itemData) {

//${itemData.challenge_comments.length > 0 ? (${itemData.challenge_comments.length}):null}
      let arr = [`Story`, `Comments (${itemData.challenge_comments.length})`, `Funders (${itemData.challenge_challengers.length})`];
        return (
            <PagerTitleIndicator
                style={styles.indicatorContainer}
                itemTextStyle={styles.indicatorText}
                selectedItemTextStyle={styles.indicatorSelectedText}
                selectedBorderStyle={styles.selectedBorderStyle}
                itemsContainerStyle={{flex:1}}
                titles={arr}
            />
        );
    }

    _onPageScroll(scrollData) {
        let {offset, position}=scrollData;
        if (position < 0 || position >= 2) return;
        this._setBgColor({bgColor: offset + position});
    }

}

type ChallengeDetailsProps = {
    dispatch: () => any,
    fetching: boolean,
    payload:object,
    getTrending: () => void,
    getOpenChallenges:() => void,
}
class ChallengeViewPreState extends React.Component{

  constructor (props) {
    super(props)
    this.state = {
      defaultfetching:false,
      refreshing: false,
      isLoadingTail: false,
      loaded: false,
      dataSource: new ListView.DataSource({
              rowHasChanged: (row1, row2) => row1 !== row2,
            }),
      category:[],
      isFavourite:''

    }


  }
  props:ChallengeDetailsProps

  getOpenChallengesUnFavCall(unfav,page,itemDataId){
  this.props.getOpenChallenges(unfav,page,itemDataId);
  if(this.props.getTrending && this.props.populerPageRefresh){
    this.props.getTrending('refreshing',1)
  }
   this.setState({isFavourite:false})
  }
  getOpenChallengesFavCall(fav,page,itemDataId){
  this.props.getOpenChallenges(fav,page,itemDataId);
  if(this.props.getTrending && this.props.populerPageRefresh){
    this.props.getTrending('refreshing',1)
  }
  this.setState({isFavourite:true})
  }
  componentWillMount(){

    let isFavouritestatus

    console.log(this.props);
    let itemData = this.props.itemData
    let uuid     = this.props.userId

     //console.log('itemData',itemData);
    if (itemData['challenge_favorite'].length > 0){
    let favourite = _.result(_.find(itemData['challenge_favorite'], function(obj) {
          return ( obj['uuid'] == uuid );
      }), '_id');

      // console.log('favourite',favourite);
      isFavouritestatus = favourite ? true : false;
      //console.log(isFavourite);

      this.setState({isFavourite:isFavouritestatus})

    }
    else{
      this.setState({isFavourite:false})
    }
  }
render () {
  //let isFavourite;
  let myOwnChallenge
  myOwnChallenge =this.props.ownChallenge
  console.log(this.props);
  //console.log("isFavourite",isFavourite);
   let itemData = this.props.itemData
  // let uuid     = this.props.userId
   console.log('itemData',itemData);

  // if (itemData['challenge_favorite'].length > 0){
  // let favourite = _.result(_.find(itemData['challenge_favorite'], function(obj) {
  //       return ( obj['uuid'] == uuid );
  //   }), '_id');
  //
  //    console.log(favourite);
  //   isFavourite = favourite ? true : false;
  //   console.log(isFavourite);
  //
  // }

   return <View style={styles.challenge}>
      <View style={styles.imageSection}>

          <Image style={styles.cardImage} source={{uri: itemData.challenge_media[0].path}} defaultSource={require('../Images/monkey_default.png')} />
          {(itemData.charity == 1) &&
            <View style={{position:'absolute',backgroundColor:'black',padding:3,flexDirection:'row',top:10,left:5,borderRadius:5}}>
            <Text style={{color:'#FFFFFF',fontWeight:'bold',fontSize:10}}>CHARITY</Text>
            </View>
            }

            {/*
            <TouchableOpacity style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
            <Icon name='heart-o' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
            </TouchableOpacity>
            */}
            {this.state.isFavourite && !myOwnChallenge &&
              <TouchableOpacity onPress={()=>{
                 this.getOpenChallengesUnFavCall("UnFavourite",null,itemData._id);
              //  this.props.getOpenChallenges("UnFavourite",null,itemData._id);
              }} style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
              <Icon name='heart' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
              </TouchableOpacity>
            }

            {!this.state.isFavourite && !myOwnChallenge &&
              <TouchableOpacity onPress={()=>{
                this.getOpenChallengesFavCall("Favourite",null,itemData._id);
               //this.props.getOpenChallenges("Favourite",null,itemData._id);
              }}
               style={{position:'absolute',padding:3,flexDirection:'row',top:10,right:5,borderRadius:5}}>
              <Icon name='heart-o' size={Metrics.icons.small} color={'#D80D0A'} style={{ marginRight: Metrics.smallMargin}} />
              </TouchableOpacity>
            }

          <View style={styles.imageFooter}>
            <View style={{alignItems: 'center', justifyContent:'center'}}>
             <Progress.Bar progress={(Math.round(itemData.percentage_funded) / 100)} color={'#33CC32'} height={8} width={(width - 20)} borderWidth={0} unfilledColor={'rgba(128,132,131,0.8)'}/>

            <View style={[styles.contentVoteSection]}>
              <Text style={{color: 'white',fontWeight:'bold' }}>${parseInt(itemData.total_amount_funded)}<Text style={{fontSize:10}}> OF ${parseInt(itemData.price)} FUNDED</Text></ Text>
              {/*
              <Text style={{color: 'white'}}>{Math.round(itemData.percentage_funded)}%</Text>
              */}
              <Text style={{color: 'white',fontWeight:'bold', textAlign: 'right'}}>{itemData.funding_days_remaining} <Text style={{fontSize:10}}>DAYS LEFT</Text></Text>
            </View>
          </View>
          </View>
        <View style={styles.imageFooterOpacity} />
      </View>


      <View style={styles.contentSection}>

      <View style={[styles.contentDescriptionSection,{flexDirection:'column',width:(width - 20),alignSelf:'center'}]}>
    {/*
           <Text style={{color: '#1266AF',fontWeight:'bold',}}>{rowData.handle_name} :
            <Text style={{color: '#000000'}}> {rowData.name}</Text></Text>
            <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: rowData.uuid })}}>
    */}

    <View style={{flexDirection:'row',width:(width - 20),}}>
      <TouchableOpacity onPress={()=>{NavigationActions.memberProfile({ userId: itemData.uuid })}}>
          <Text style={{color: '#1266AF',fontWeight:'bold',flexWrap: 'wrap'}}>{itemData.handle_name} :
          <Text numberOfLines={2} style={{color: '#000000',fontWeight:'bold',}}> {itemData.name}</Text></Text>
      </TouchableOpacity>
    </View>
          <Text style={{fontSize:12}} numberOfLines={2}>{itemData.description}</Text>
      </View>

      </View>
      </View>
  }
}






const mapStateToProps = (state) => {
  console.log('state',state);
  return {
    default_type:state.openChallenges.default_type,
    fetching: state.openChallenges.fetching,
    payload: state.openChallenges.payload,
    error: state.openChallenges.error,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getOpenChallenges: (default_type = 'default', page = 1, challengeId = null) =>{
      console.log(default_type, page, challengeId);
      return dispatch(OpenChallengesActions.openChallengesRequest(default_type, page, challengeId))
    },
    getTrending: (default_type = 'default', page = 1, challengeId = null) =>{
        console.log(default_type, page, challengeId);
        return dispatch(TrendingActions.trendingRequest(default_type, page, challengeId))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OpenChallengeDetailView)
