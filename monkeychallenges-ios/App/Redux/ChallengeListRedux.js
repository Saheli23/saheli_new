import {createReducer, createActions} from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  challengeListRequest: ['data'],
  challengeListSuccess: ['payload'],
  challengeListFailure: null
})

export const ChallengeListTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  trending: [],
  open_funding: [],
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, {data}) => {
  return state.merge({fetching: true, data, payload: null})
}

// successful api lookup
export const success = (state, action) => {
  const {payload} = action
  return state.merge({fetching: false, error: null, [state.data]: payload.data})
}

// Something went wrong somewhere.
export const failure = state =>
    state.merge({fetching: false, error: true, [state.data]: []})

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.CHALLENGE_LIST_REQUEST]: request,
  [Types.CHALLENGE_LIST_SUCCESS]: success,
  [Types.CHALLENGE_LIST_FAILURE]: failure
})
