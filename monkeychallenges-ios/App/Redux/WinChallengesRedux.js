import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import mergers from 'seamless-immutable-mergers'
//var mergers = reuqire("seamless-immutable-mergers");

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  winChallengesRequest: ['default_type','page','challengeId'],
  winChallengesSuccess: ['payload','default_type', 'challengeId'],
  winChallengesFailure: null
})

export const WinChallengesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  defaultfetching:null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state:Object, action:Object) =>{
  let {default_type, page, challengeId} = action;

  console.log(default_type, page);
// if(default_type == 'default'){
//   return state.merge({ fetching: true, payload: {'data':[]} })
// }


switch (default_type) {
  case 'pagination':
    console.log('pagination',default_type, page);
    return state.merge({isLoadingTail:true,defaultfetching:false, error: false})
    break;
    case 'refreshing':
      console.log('refreshing',default_type, page);
      return state.merge({refreshing:true,defaultfetching:false, error: false})
      break;
      case 'Favourite':
        console.log('Favourite',default_type, page, challengeId);
        return state.merge({defaultfetching:true, error: false})
        break;
        case 'UnFavourite':
          console.log('Favourite',default_type, page, challengeId);
          return state.merge({defaultfetching:true, error: false})
          break;
      
  default:
    return state.merge({error: false, defaultfetching:false,refreshing:false,isLoadingTail:false,fetching: true, payload: {'data':[]} })
}

}


// successful api lookup
export const success = (state:Object, action:Object) => {
  const { payload, default_type, challengeId } = action
  //console.log(default_type);
  //debugger;
switch (default_type) {
  case 'pagination':
  console.log(payload);
  console.log(state);
  let Paginationdata =  state.payload.data.concat(payload.data)
  let newPayload = Object.assign({},{error: false,isLoadingTail:false,payload:{data:Paginationdata,meta:payload.meta}})
    //state.payload.meta.merge(payload.meta)
    //console.log(Paginationdata);
    //let newPayload = Object.assign({},...state,{isLoadingTail:false,payload:payload})
   //console.log(newPayload);
   return state.merge(newPayload)
    break;
    case 'refreshing':
      console.log('refreshing',default_type);
      return state.merge({ fetching: false, error: false,refreshing:false, payload })
      break;


      case 'Favourite':
    let FavnewPayload =  Object.assign({},state,{
      fetching: false,
      error: false,
      defaultfetching:false,
      payload:{
        data: state.payload.data.map((item,index)=>{
                      if(item._id == challengeId){
                        return Object.assign({}, item, {
                          challenge_favorite:[
                              ...item.challenge_favorite,
                              payload
                          ]
                      })
                      }
            return item
          }),
          meta: state.payload.meta
    }});
        return state.merge(FavnewPayload)
        break;
        case 'UnFavourite':
          let UnFavnewPayload =  Object.assign({},state,{
            fetching: false,
            error: false,
            defaultfetching:false,
            payload:{
              data: state.payload.data.map((item,index)=>{
                            if(item._id == challengeId){
                              return Object.assign({}, item, {
                                challenge_favorite:item.challenge_favorite.map((inneritem,index)=>inneritem.challenge_id != challengeId)
                            })
                            }
                  return item
                }),
                meta: state.payload.meta
          }});
          console.log(UnFavnewPayload)
          return state.merge(UnFavnewPayload)
          break;




  default:
  console.log(payload);
  return state.merge({ fetching: false,error: false, payload })
}

  //Object.assign({fetching: false, error: null, payload})
}

// Something went wrong somewhere.
export const failure = state =>  state.merge({defaultfetching:false, fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.WIN_CHALLENGES_REQUEST]: request,
  [Types.WIN_CHALLENGES_SUCCESS]: success,
  [Types.WIN_CHALLENGES_FAILURE]: failure
})
