

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  
  editChallengeRequest: ['default_type','price', 'name', 'zip_code','challenge_category_type_id','start_date','challenge_type_id','challenge_id','description', 'description_2', 'description_3','is_published'],
  editChallengeSuccess: ['payload','default_type'],
  editChallengeFailure: null
})

export const EditChallengeTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  default_type:null,
  status:null,
  steptwo:false,stepthree:false,stepfour:false,

})

/* ------------- Reducers ------------- */



export const request = (state:Object, action:Object) =>{
  
  let {default_type, price, name,zip_code, challenge_category_type_id, start_date,challenge_type_id,challenge_id,description, description_2, description_3,is_published} = action;
  console.log(default_type, price, name,zip_code, challenge_category_type_id, start_date,challenge_type_id,challenge_id,description, description_2, description_3);



switch (default_type) {
  
    case 'stepThreeSubmit':
    return state.merge({status:'request',steptwo:false,stepthree:true,stepfour:false,fetching: true,payload:state.payload})
    break; 
    case 'stepFourSubmit':
    return state.merge({status:'request',steptwo:false,stepthree:false,stepfour:true,fetching: true,payload:state.payload})
    break; 
  default:
    return state.merge({status:'request',fetching: true,stepthree:false,stepfour:true})
}

}



export const success = (state:Object, action:Object) => {
  const { payload, default_type } = action
  //console.log(default_type);
switch (default_type) {
  
    case 'stepThreeSubmit':
   console.log( payload, default_type );

    return state.merge({status:'success',steptwo:false,stepthree:true,stepfour:false,fetching:false,error:null,payload:{data:payload},default_type:default_type});
    break;
    case 'stepFourSubmit':
     // console.log( payload, default_type );

    return state.merge({status:'success',steptwo:false,stepthree:false,stepfour:true,fetching:false,error:null,payload:{data:'payload'},default_type:default_type});
    break;
    
  default:
  console.log(payload);
  return state.merge({ status:'success',stepone:false,steptwo:false,stepthree:false,stepfour:false,fetching: false, error: null, payload})
}

  //Object.assign({fetching: false, error: null, payload})
}

// Something went wrong somewhere.
export const failure = (state = {})=>{
  state.merge({ fetching: false, error: true,})
   //state = Object.assign({ fetching: false, error: true, payload: null })
  // console.log(state);
   return state
}

/* ------------- Hookup Reducers To Types ------------- */

//export const reducer = fromJS({})

export const reducer = createReducer(INITIAL_STATE, { 
  [Types.EDIT_CHALLENGE_REQUEST]: request,
  [Types.EDIT_CHALLENGE_SUCCESS]: success,
  [Types.EDIT_CHALLENGE_FAILURE]: failure
})
