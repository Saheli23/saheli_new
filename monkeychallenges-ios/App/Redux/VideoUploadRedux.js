import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  videoUploadRequest: ['_id','uuid','data'],
  videoUploadSuccess: ['payload'],
  videoUploadFailure: null
})

export const VideoUploadTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state: Object) => {
  state.merge({ fetching: true})
}

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state => {
  state.merge({ fetching: false, error: true, payload: null })
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.VIDEO_UPLOAD_REQUEST]: request,
  [Types.VIDEO_UPLOAD_SUCCESS]: success,
  [Types.VIDEO_UPLOAD_FAILURE]: failure
})
