

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({

  createChallengeRequest: ['default_type','price', 'name', 'zip_code','challenge_category_type_id','start_date','challenge_type_id','challenge_id','description', 'description_2', 'description_3','is_published'],
  createChallengeSuccess: ['payload','default_type'],
  createChallengeFailure: null
})

export const CreateChallengeTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  default_type:null,
  status:null,
  stepone:false,steptwo:false,cat:false

})

/* ------------- Reducers ------------- */



export const request = (state:Object, action:Object) =>{

  let {default_type, price, name,zip_code, challenge_category_type_id, start_date,challenge_type_id,challenge_id,description, description_2, description_3,is_published} = action;
  console.log(default_type, price, name,zip_code, challenge_category_type_id, start_date,challenge_type_id,challenge_id,description, description_2, description_3);



switch (default_type) {
  case 'category':
    return state.merge({status:'request',cat:true,stepone:false,steptwo:false, fetching: true, payload: null,error:null })
    break;
    case 'stepOneSubmit':
    return state.merge({status:'request',cat:false, stepone:true,steptwo:false,fetching: true,payload:state.payload})
    break;
    case 'nothing':
    return state.merge({status:'nothing',fetching: false,default_type:'nothing',stepone:false})
    break;

  default:
    return state.merge({status:'request',fetching: true})
}

}



export const success = (state:Object, action:Object) => {
  const { payload, default_type } = action
  //console.log(default_type);
switch (default_type) {
  case 'category':
        let cat_arr = [];
        console.log( payload, default_type );
        payload.map((data,index) => {
                console.log(data,index);
                cat_arr.push({key:data.id,label:data.name})
              })
        let newCatPayload = Object.assign({},{status:'success',cat:true,stepone:false,steptwo:false,fetching:false,error:null,payload:{cat_data:cat_arr, data: null},default_type:default_type})
  return state.merge(newCatPayload)
    break;

    case 'stepOneSubmit':
   console.log( payload, default_type );

    return state.merge({status:'success',cat:false,stepone:true,steptwo:false,fetching:false,error:null,payload:{data:payload,cat_data:state.payload.cat_data},default_type:default_type});
    break;
    case 'nothing':
    return state.merge({status:'nothing',fetching: false,default_type:'nothing',stepone:false})
    break;

  default:
  console.log(payload);
  return state.merge({ status:'success',stepone:false,steptwo:false,fetching: false, error: null, payload})
}

  //Object.assign({fetching: false, error: null, payload})
}

// Something went wrong somewhere.
export const failure = (state = {})=>{
  state.merge({ fetching: false, error: true,})
   //state = Object.assign({ fetching: false, error: true, payload: null })
  // console.log(state);
   return state
}

/* ------------- Hookup Reducers To Types ------------- */

//export const reducer = fromJS({})

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CREATE_CHALLENGE_REQUEST]: request,
  [Types.CREATE_CHALLENGE_SUCCESS]: success,
  [Types.CREATE_CHALLENGE_FAILURE]: failure
})
