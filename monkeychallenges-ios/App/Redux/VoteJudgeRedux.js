import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  voteJudgeRequest: ['vote','challenge_id'],
  voteJudgeSuccess: ['payload'],
  voteJudgeFailure: null
})

export const VoteJudgeTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state: Object) => {
  state.merge({ fetching: true})
}

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state => {
  state.merge({ fetching: false, error: true, payload: null })
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.VOTE_JUDGE_REQUEST]: request,
  [Types.VOTE_JUDGE_SUCCESS]: success,
  [Types.VOTE_JUDGE_FAILURE]: failure
})
