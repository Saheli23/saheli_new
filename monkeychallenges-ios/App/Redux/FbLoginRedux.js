// @flow

import {createReducer, createActions} from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  fbLoginRequest: ['facebook_token'],
  fbLoginSuccess: ['data'],
  fbLoginFailure: ['error'],
  fbLogout: null
})

export const FbLoginTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  username: null,
  error: null,
  fetching: false,
  fblogin:false,
  
})

/* ------------- Reducers ------------- */

// we're attempting to login
export const request = (state: Object) => {
  return state.merge({fetching: true,fblogin:true})
}

// we've successfully logged in
export const success = (state: Object, action: Object) => {
  let {data} = action
  
  return state.merge({fetching: false, error: null,fblogin:true, access_token: data.access_token})
}

// we've had a problem logging in
export const failure = (state: Object, {error}: Object) =>
    state.merge({fetching: false, error,fblogin:true})

// we've logged out
export const logout = (state: Object) => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.FB_LOGIN_REQUEST]: request,
  [Types.FB_LOGIN_SUCCESS]: success,
  [Types.FB_LOGIN_FAILURE]: failure,
  [Types.FB_LOGOUT]: logout
})

/* ------------- Selectors ------------- */

// Is the current user logged in?

