import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  userEditRequest: ['message'],
  userEditSuccess: ['payload'],
  userEditFailure: null
})

export const UserEditTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>{
console.log("request triggers");
  return state.merge({ fetching: true, data, payload: null })
  }

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
   console.log("success triggers", action);
  console.log(payload);
  return state.merge({ fetching: false, error: null, payload})
  //return Object.assign({fetching: false, error: null, payload})
}

// Something went wrong somewhere.

export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.USER_EDIT_REQUEST]: request,
  [Types.USER_EDIT_SUCCESS]: success,
  [Types.USER_EDIT_FAILURE]: failure
})


// /* ------------- Hookup Reducers To Types ------------- */

// export const reducer = createReducer(INITIAL_STATE, {
//   [Types.USER_DETAILS_REQUEST]: request,
//   [Types.USER_DETAILS__SUCCESS]: success,
//   [Types.USER_DETAILS__FAILURE]: failure
// })
