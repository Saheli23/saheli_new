import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import * as _ from 'lodash';
/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  favoritesChallengesRequest: ['default_type','challengeId'],
  favoritesChallengesSuccess: ['payload','default_type', 'challengeId'],
  favoritesChallengesFailure: null
})

export const FavoritesChallengesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state:Object, action:Object) =>{
  let {default_type, challengeId} = action;
switch (default_type) {
  case 'UnFavourite':
    console.log(default_type, challengeId);
    return state.merge({ fetching: false, defaultfetching:true, payload: state.payload })
    break;
  default:
  return state.merge({ fetching: true, defaultfetching:false, payload:null })
}


  //state = Object.assign({ fetching: true, data, payload: null })
  // console.log(state);
   //return state
}

// successful api lookup
export const success = (state:Object, action:Object) => {
  const { payload, default_type, challengeId } = action


  switch (default_type) {
    case 'UnFavourite':
      console.log(payload, default_type, challengeId);

      let UnFavnewPayload =  Object.assign({},state,{
        fetching: false,
        error: false,
        defaultfetching:false,
        payload:{
          challenges: state.payload.challenges.filter((item)=>{
                        if(item._id !== challengeId){
                          return item
                        }
            })

      }});
      console.log(UnFavnewPayload)

      return state.merge(UnFavnewPayload)
      break;
    default:

      console.log(payload);
      return state.merge({ fetching: false, error: null, payload})
  }



   //state = Object.assign({ fetching: false, error: null, payload: {"data": payload.follows}})
  // console.log(state);
   //return state
}

// Something went wrong somewhere.
export const failure = (state = {})=>{
  state.merge({ fetching: false, error: true, payload: null })
   //state = Object.assign({ fetching: false, error: true, payload: null })
  // console.log(state);
   return state
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.FAVORITES_CHALLENGES_REQUEST]: request,
  [Types.FAVORITES_CHALLENGES_SUCCESS]: success,
  [Types.FAVORITES_CHALLENGES_FAILURE]: failure
})
