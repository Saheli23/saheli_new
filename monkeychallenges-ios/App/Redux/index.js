// @flow

import {combineReducers} from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'

export default () => {
    /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
   //temperature: require('./TemperatureRedux').reducer,
  //fbLogin: require('./FbLoginRedux').reducer,
   // search: require('./SearchRedux').reducer,
    login: require('./LoginRedux').reducer,
    trending: require('./TrendingRedux').reducer,
    openFunding: require('./OpenFundingRedux').reducer,
    openChallenges: require('./OpenChallengesRedux').reducer,
    judgingChallenges: require('./JudgingChallengesRedux').reducer,
    pendingChallenges: require('./PendingChallengesRedux').reducer,
    popularChallenges: require('./PopularChallengesRedux').reducer,
    videoUpload: require('./VideoUploadRedux').reducer,
    favoritesMembers: require('./FavoritesMembersRedux').reducer,
    favoritesChallenges: require('./FavoritesChallengesRedux').reducer,
    myOpenChallenges: require('./MyOpenChallengesRedux').reducer,
    myJudgingChallenges: require('./MyJudgingChallengesRedux').reducer,
    myPendingChallenges: require('./MyPendingChallengesRedux').reducer,
    memberOpenChallenges: require('./MemberOpenChallengesRedux').reducer,
    userDetails: require('./UserDetailsRedux').reducer,
    userEdit: require('./UserEditRedux').reducer,
    createChallenge:require('./CreateChallengeRedux').reducer,
    getUuid:require('./GetUuidRedux').reducer,
    voteJudge:require('./VoteJudgeRedux').reducer,
    // judgingChallengesDetail: require('./JudgingChallengesDetailRedux').reducer,
    detailJudgingChallenges: require('./DetailJudgingChallengesRedux').reducer,
    challengeCategory:require('./ChallengeCategoryRedux').reducer,
    editChallenge:require('./EditChallengeRedux').reducer,
    reportChallenge:require('./ReportChallengeRedux').reducer,
    winChallenges: require('./WinChallengesRedux').reducer,
    failChallenges: require('./FailChallengesRedux').reducer,
    searchResult: require('./SearchResultRedux').reducer,
  })

  return configureStore(rootReducer, rootSaga)
}
