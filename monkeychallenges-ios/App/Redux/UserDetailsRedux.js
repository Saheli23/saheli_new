import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  userDetailsRequest: ['profilemessage','default_type','userid'],
  userDetailsSuccess: ['payload'],
  userDetailsFailure: null
})

export const UserDetailsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state: Object, action:Object) =>{
  // console.log("request triggers");
  // console.log(data);
    let {profilemessage, default_type,userid} = action
    if(default_type == 'useredit'){
      return state.merge({ fetching: true,})
    }else if(default_type == 'userImageUpload'){
      return state.merge({ fetching: true,})
    }else if(default_type == 'memberdetails'){
      return state.merge({ fetching: true,payload: null})
    }
    else{
      return state.merge({ fetching: true, payload: null })
    }

  }

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
   console.log("success triggers", action);
  console.log(payload);
  return state.merge({ fetching: false, error: null, payload })
  //Object.assign({fetching: false, error: null, payload})
}

// Something went wrong somewhere.

export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.USER_DETAILS_REQUEST]: request,
  [Types.USER_DETAILS_SUCCESS]: success,
  [Types.USER_DETAILS_FAILURE]: failure
})


// /* ------------- Hookup Reducers To Types ------------- */

// export const reducer = createReducer(INITIAL_STATE, {
//   [Types.USER_DETAILS_REQUEST]: request,
//   [Types.USER_DETAILS__SUCCESS]: success,
//   [Types.USER_DETAILS__FAILURE]: failure
// })
