

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({

  reportChallengeRequest: ['name', 'phone_no', 'mail','reason','subject','description','challenge_link','mailTo'],
  reportChallengeSuccess: ['payload'],
  reportChallengeFailure: null
})

export const ReportChallengeTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  success:null,


})

/* ------------- Reducers ------------- */



export const request = (state:Object, action:Object) =>{

  let {name, phone_no, mail,reason,subject,description,challenge_link,mailTo} = action;
  console.log(name, phone_no, mail,reason,subject,description,challenge_link,mailTo);



    return state.merge({fetching: true,})


}



export const success = (state:Object, action:Object) => {
  const { payload } = action
  //console.log(default_type);


    return state.merge({fetching:false,error:false,success:true,payload:{data:payload}});


  //Object.assign({fetching: false, error: null, payload})
}

// Something went wrong somewhere.
export const failure = (state = {})=>{
  state.merge({ fetching: false, error: true})
   //state = Object.assign({ fetching: false, error: true, payload: null })
  // console.log(state);
   return state
}

/* ------------- Hookup Reducers To Types ------------- */

//export const reducer = fromJS({})

export const reducer = createReducer(INITIAL_STATE, {
  [Types.REPORT_CHALLENGE_REQUEST]: request,
  [Types.REPORT_CHALLENGE_SUCCESS]: success,
  [Types.REPORT_CHALLENGE_FAILURE]: failure
})
