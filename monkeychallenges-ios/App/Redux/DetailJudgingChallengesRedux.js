import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  detailJudgingChallengesRequest: ['id','detail_type','vote'],
  detailJudgingChallengesSuccess: ['payload'],
  detailJudgingChallengesFailure: null
})

export const DetailJudgingChallengesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state: Object, action:Object) =>{
  console.log(action);
  let {detail_type, id, vote} = action
  if(detail_type == 'default'){
    return state.merge({ fetching: true, payload:null})
  }else{
    return state.merge({ fetching: true})
  }

  }

// successful api lookup
export const success = (state, action) => {
  const { payload} = action
  //console.log(state);
  console.log(payload.challenges[0]);
  return state.merge({ fetching: false, error: null, payload:payload })
}

// Something went wrong somewhere.
export const failure = state =>{
  console.log(state);
  return state.merge({ fetching: false, error: true, payload: null })
}
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.DETAIL_JUDGING_CHALLENGES_REQUEST]: request,
  [Types.DETAIL_JUDGING_CHALLENGES_SUCCESS]: success,
  [Types.DETAIL_JUDGING_CHALLENGES_FAILURE]: failure
})
