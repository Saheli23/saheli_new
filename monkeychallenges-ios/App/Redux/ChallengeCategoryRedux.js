import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  challengeCategoryRequest: ['data'],
  challengeCategorySuccess: ['payload'],
  challengeCategoryFailure: null
})

export const ChallengeCategoryTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state:Object, { data }) =>{
  return state.merge({ fetching: true, data, payload: null,error:null })
  //state = Object.assign({ fetching: true, data, payload: null })
  // console.log(state);
   //return state
}

// successful api lookup
export const success = (state:Object, action:Object) => {
  const { payload } = action
  console.log(payload);
  let cat_arr = [];
   payload.data.map((data,index) => {
          console.log(data,index);
          cat_arr.push({key:data.id,lable:data.name})
        })
  let newCatPayload = Object.assign({},{fetching:false,error:null,payload:{cat_data:cat_arr}})
  return state.merge(newCatPayload)
  // state = Object.assign({ fetching: false, error: null, payload: {"data": payload.challenge_category_types}})
  // console.log(state);
   //return state
}

// Something went wrong somewhere.
export const failure = (state = {})=>{
 return state.merge({ fetching: false, error: true, payload: null })
 //  state = Object.assign({ fetching: false, error: true, payload: null })
  // console.log(state);
  // return state
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CHALLENGE_CATEGORY_REQUEST]: request,
  [Types.CHALLENGE_CATEGORY_SUCCESS]: success,
  [Types.CHALLENGE_CATEGORY_FAILURE]: failure
})
