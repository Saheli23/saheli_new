import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  memberOpenChallengesRequest: ['default_type','page','userid'],
  memberOpenChallengesSuccess: ['payload','default_type'],
  memberOpenChallengesFailure: null
})

export const MemberOpenChallengesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state:Object, action:Object) =>{
  let {default_type, page,userid} = action;
  console.log(default_type, page,userid);
//  console.log(default_type, page);
// if(default_type == 'default'){
//   return state.merge({ fetching: true, payload: {'data':[]} })
// }


switch (default_type) {
  case 'pagination':
    console.log('pagination',default_type, page);
    return state.merge({isLoadingTail:true})
    break;
    case 'refreshing':
      console.log('refreshing',default_type, page);
      return state.merge({refreshing:true})
      break;
  default:
    return state.merge({refreshing:false,isLoadingTail:false,fetching: true, payload: {'data':[]} })
}

}


// successful api lookup
export const success = (state:Object, action:Object) => {
  const { payload, default_type } = action
  //console.log(default_type);
switch (default_type) {
  case 'pagination':
  console.log(payload);
  console.log(state);
  let Paginationdata =  state.payload.data.concat(payload.data)
  let newPayload = Object.assign({},{isLoadingTail:false,payload:{data:Paginationdata,meta:payload.meta}})
    //state.payload.meta.merge(payload.meta)
    //console.log(Paginationdata);
    //let newPayload = Object.assign({},...state,{isLoadingTail:false,payload:payload})
   //console.log(newPayload);
   return state.merge(newPayload)
    break;
    case 'refreshing':
      console.log('refreshing',default_type);
      return state.merge({ fetching: false, error: null,refreshing:false, payload })
      break;
  default:
  console.log(payload);
  return state.merge({ fetching: false, error: null, payload })
}

  //Object.assign({fetching: false, error: null, payload})
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.MEMBER_OPEN_CHALLENGES_REQUEST]: request,
  [Types.MEMBER_OPEN_CHALLENGES_SUCCESS]: success,
  [Types.MEMBER_OPEN_CHALLENGES_FAILURE]: failure
})
