// @flow

import {createReducer, createActions} from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  loginRequest: ['username', 'password', 'facebook_token','loginType'],
  loginSuccess: ['username', 'data'],
  loginFailure: ['error'],
  logout: null
})

export const LoginTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  username: null,
  error: null,
  fetching: false,
})

/* ------------- Reducers ------------- */

// we're attempting to login
export const request = (state: Object) => {
  return state.merge({fetching: true})
}

// we've successfully logged in
export const success = (state: Object, action: Object) => {
  let {data, username} = action
  return state.merge({fetching: false, error: false, username, access_token: data.access_token, message:null})
}

// we've had a problem logging in
export const failure = (state: Object, error: Object) =>{
  console.log(error);
  let {error:{message}} = error
//  console.log(message, status_code);
  return state.merge({fetching: false, error: true, message: message, access_token:null, username:null})
}

// we've logged out
export const logout = (state: Object) => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_REQUEST]: request,
  [Types.LOGIN_SUCCESS]: success,
  [Types.LOGIN_FAILURE]: failure,
  [Types.LOGOUT]: logout
})

/* ------------- Selectors ------------- */

// Is the current user logged in?
export const isLoggedIn = (loginState: Object) => loginState.username !== null
