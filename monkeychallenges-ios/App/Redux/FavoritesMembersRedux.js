import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  favoritesMembersRequest: ['default_type','followsUuid'],
  favoritesMembersSuccess: ['payload','default_type', 'followsUuid'],
  favoritesMembersFailure: null
})

export const FavoritesMembersTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state:Object, action:Object) =>{
  let {default_type,followsUuid} = action;
  //state.merge({ fetching: true, data, payload: null })

   console.log(default_type,followsUuid);
   switch (default_type) {
     case 'UnFavourite':

     state = Object.assign({defaultfetching:true, fetching: false, payload: state.payload })
      return state
       break;
     default:
     state = Object.assign({ fetching: true, payload: null })
      return state
   }
}

// successful api lookup
export const success = (state:Object, action:Object) => {
  const { payload,default_type,followsUuid } = action
  console.log( payload,default_type,followsUuid);
  //return state.merge({ fetching: false, error: null, payload })


   switch (default_type) {
     case 'UnFavourite':

      console.log( payload,default_type,followsUuid);
      console.log(state.payload);

      // debugger;
      // state = Object.assign({defaultfetching:false, fetching: false, error: null,})
      // return state
      //window.myObj = state.payload;

      let UnFavnewPayload =  Object.assign({},state,{
        fetching: false,
        defaultfetching:false,
        error: false,
        payload:{
          data: state.payload.data.filter((item)=>{
                        if(item.uuid !== followsUuid){
                          return item
                        }
            })

      }});

      console.log(UnFavnewPayload);
      return UnFavnewPayload;


       break;
     default:
     state = Object.assign({ fetching: false, error: null, payload: {"data": payload.follows}})
     return state
   }
}

// Something went wrong somewhere.
export const failure = (state = {})=>{
  //state.merge({ fetching: false, error: true, payload: null })
   state = Object.assign({ fetching: false, error: true, payload: null })
  // console.log(state);
   return state
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.FAVORITES_MEMBERS_REQUEST]: request,
  [Types.FAVORITES_MEMBERS_SUCCESS]: success,
  [Types.FAVORITES_MEMBERS_FAILURE]: failure
})
