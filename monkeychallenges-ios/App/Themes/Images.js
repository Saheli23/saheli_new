// @flow

// leave off @2x/@3x
const images = {
  logo: require('../Images/mc_logo.png'),
  logoDefault: require('../Images/monkey_default.png'),
  logoBlack: require('../Images/mc_logo_black.png'),
  clearLogo: require('../Images/mc_logo.png'),
  ignite: require('../Images/mc_logo.png'),
  tileBg: require('../Images/tile_bg.png'),
  background: require('../Images/BG_white.png'),
  landingBg: require('../Images/landing_BG.png'),
  loginScreenBg: require('../Images/loginScreen_Bg.png'),
  profileCoverBg: require('../Images/member_ban_back.jpg'),
  play: require('../Images/play.png'),
  hammer: require('../Images/hammer.png'),
  share: require('../Images/share.png'),
  person: require('../Images/person.png'),

}

export default images
