/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to Sagas/index.js
*  - This template uses the api declared in Sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import JudgingChallengesActions from '../Redux/JudgingChallengesRedux'

export function * getJudgingChallenges (api, action) {
  const { default_type, page, challengeId } = action

  console.log(default_type, page, challengeId);

  switch (default_type) {
    case 'pagination':
      console.log('pagination',default_type, page);
      const responsePage = yield call(api.getChallengeList, 'new', 'judge-open', page)
      // success?
      console.log(responsePage);
        if (responsePage.ok) {
        // You might need to change the response here - do this with a 'transform',
        // located in ../Transforms/. Otherwise, just pass the data back from the api.
        console.log(default_type);
        yield put(JudgingChallengesActions.judgingChallengesSuccess(responsePage.data,default_type))
      } else {
        yield put(JudgingChallengesActions.judgingChallengesFailure())
      }
      break;

      case 'Favourite':
         console.log('Favourite',default_type, page, challengeId);
         const responseFavourite = yield call(api.selectFavourite, challengeId)

         console.log(responseFavourite);

         if (responseFavourite.ok) {
           yield put(JudgingChallengesActions.judgingChallengesSuccess(responseFavourite.data,default_type,challengeId))
         }else {
            yield put(JudgingChallengesActions.judgingChallengesFailure())
          }
        break;
        case 'UnFavourite':
           console.log('UnFavourite',default_type, page, challengeId);
           const responseUnFavourite = yield call(api.unFavourite, challengeId)
           console.log(responseUnFavourite);
           if (responseUnFavourite.ok) {
             yield put(JudgingChallengesActions.judgingChallengesSuccess(responseUnFavourite.data,default_type,challengeId))
           }else {
              yield put(JudgingChallengesActions.judgingChallengesFailure())
            }
          break;



    default:
    // make the call to the api
    const response = yield call(api.getChallengeList, 'new', 'judge-open', page)
    // success?
      if (response.ok) {
      // You might need to change the response here - do this with a 'transform',
      // located in ../Transforms/. Otherwise, just pass the data back from the api.
      yield put(JudgingChallengesActions.judgingChallengesSuccess(response.data,default_type))
    } else {
      yield put(JudgingChallengesActions.judgingChallengesFailure())
    }
  }

}
