/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to Sagas/index.js
*  - This template uses the api declared in Sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import MyOpenChallengesActions from '../Redux/MyOpenChallengesRedux'

export function * getMyOpenChallenges (api, action) {
  const { default_type, page } = action

  console.log(default_type, page);

  switch (default_type) {
    case 'pagination':
      console.log('pagination',default_type, page);
      const responsePage = yield call(api.getMyChallengeList, 'open', 4, page)
      // success?
      console.log(responsePage);
        if (responsePage.ok) {
        // You might need to change the response here - do this with a 'transform',
        // located in ../Transforms/. Otherwise, just pass the data back from the api.
        console.log(default_type);
          yield put(MyOpenChallengesActions.myOpenChallengesSuccess(responsePage.data,default_type))
      } else {
        yield put(MyOpenChallengesActions.myOpenChallengesFailure())
      }
      break;
    default:
    // make the call to the api
    const response = yield call(api.getMyChallengeList, 'open', 4, page)
    // success?
      if (response.ok) {
      // You might need to change the response here - do this with a 'transform',
      // located in ../Transforms/. Otherwise, just pass the data back from the api.
        yield put(MyOpenChallengesActions.myOpenChallengesSuccess(response.data,default_type))
    } else {
      yield put(MyOpenChallengesActions.myOpenChallengesFailure())
    }
  }
}
