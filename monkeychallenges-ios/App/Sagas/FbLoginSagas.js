import {call, put} from 'redux-saga/effects'
import FbLoginActions from '../Redux/FbLoginRedux'

// attempts to login
export function * fbLogin (api, action) {
  let {facebook_token} = action
  console.log(action)
 
  const response = yield call(api.fblogin, facebook_token)

 console.log(response);
  

  if (response.ok) {
  	 api.setAutoToken(response.data.access_token)
    yield put(FbLoginActions.fbLoginSuccess(response.data))
  } else {
    yield put(FbLoginActions.fbLoginFailure('WRONG'))
  }
}
