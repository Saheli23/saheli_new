


import { call, put } from 'redux-saga/effects'
import UserDetailsActions from '../Redux/UserDetailsRedux'

export function * getUserDetails (api, action) {
  const { profilemessage,default_type,userid } = action
  // make the call to the api
  console.log(profilemessage,default_type,userid);
  if(default_type == 'useredit'){
      const responseEdit = yield call(api.updateUserDetails,profilemessage)
      console.log(responseEdit);
      if (responseEdit.ok) {
        yield put(UserDetailsActions.userDetailsSuccess({'user':responseEdit.data},default_type))
        // const responseAfterEdit = yield call(api.getUserDetails)
        // if (responseAfterEdit.ok) {
        //   // You might need to change the response here - do this with a 'transform',
        //   // located in ../Transforms/. Otherwise, just pass the data back from the api.
        //   yield put(UserDetailsActions.userDetailsSuccess(responseAfterEdit.data))
        // } else {
        //   yield put(UserDetailsActions.userDetailsFailure())
        // }
      } else {
        yield put(UserDetailsActions.userDetailsFailure())
      }
  }
  else if(default_type =='memberdetails'){
    const response = yield call(api.getMemberDetails,userid)
      console.log(response);
    // success?
    if (response.ok) {
      // You might need to change the response here - do this with a 'transform',
      // located in ../Transforms/. Otherwise, just pass the data back from the api.
      yield put(UserDetailsActions.userDetailsSuccess(response.data,default_type))
    } else {
      yield put(UserDetailsActions.userDetailsFailure())
    }
  }
  else{
    const response = yield call(api.getUserDetails)
      console.log(response);
    // success?
    if (response.ok) {
      // You might need to change the response here - do this with a 'transform',
      // located in ../Transforms/. Otherwise, just pass the data back from the api.
      yield put(UserDetailsActions.userDetailsSuccess(response.data,default_type))
    } else {
      yield put(UserDetailsActions.userDetailsFailure())
    }
  }

}
