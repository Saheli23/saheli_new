/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to Sagas/index.js
*  - This template uses the api declared in Sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import FavoritesChallengesActions from '../Redux/FavoritesChallengesRedux'

export function * getFavoritesChallenges (api, action) {
  const { default_type, challengeId } = action
  // make the call to the api
switch (default_type) {
  case 'UnFavourite':
  console.log('UnFavourite',default_type, challengeId);
  const responseUnFavourite = yield call(api.unFavourite, challengeId)
  console.log(responseUnFavourite);
  if (responseUnFavourite.ok) {
    yield put(FavoritesChallengesActions.favoritesChallengesSuccess(responseUnFavourite.data,default_type,challengeId))
  }else {
     yield put(FavoritesChallengesActions.favoritesChallengesFailure())
   }
    break;
  default:

  const response = yield call(api.getMyFavoriteschallenges)

  // success?
  if (response.ok) {
    //  console.log(response);
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(FavoritesChallengesActions.favoritesChallengesSuccess(response.data))
  } else {
    yield put(FavoritesChallengesActions.favoritesChallengesFailure())
  }

}


}
