import {takeLatest, takeEvery} from 'redux-saga'
//import API from '../Services/Api'
import MCApi from '../Services/MCApi'
//import FixtureAPI from '../Services/FixtureApi'
//import DebugSettings from '../Config/DebugSettings'

/* ------------- Types ------------- */

//import {StartupTypes} from '../Redux/StartupRedux'
//import {TemperatureTypes} from '../Redux/TemperatureRedux'
import {LoginTypes} from '../Redux/LoginRedux'
//import {FbLoginTypes} from '../Redux/FbLoginRedux'
import {OpenScreenTypes} from '../Redux/OpenScreenRedux'
import {ChallengeListTypes} from '../Redux/ChallengeListRedux'
import {TrendingTypes} from '../Redux/TrendingRedux'
import {OpenFundingTypes} from '../Redux/OpenFundingRedux'
import {JudgingChallengesTypes} from '../Redux/JudgingChallengesRedux'
//import {JudgingChallengesDetailTypes} from '../Redux/JudgingChallengesRedux'
import {OpenChallengesTypes} from '../Redux/OpenChallengesRedux'

import {PopularChallengesTypes} from '../Redux/PopularChallengesRedux'

import {PendingChallengesTypes} from '../Redux/PendingChallengesRedux'
import {VideoUploadTypes} from '../Redux/VideoUploadRedux'


import {FavoritesMembersTypes} from '../Redux/FavoritesMembersRedux'
import {FavoritesChallengesTypes} from '../Redux/FavoritesChallengesRedux'

import {MyOpenChallengesTypes} from "../Redux/MyOpenChallengesRedux"
import {MyJudgingChallengesTypes} from "../Redux/MyJudgingChallengesRedux"
import {MyPendingChallengesTypes} from "../Redux/MyPendingChallengesRedux"

import {MemberOpenChallengesTypes} from "../Redux/MemberOpenChallengesRedux"


import {DetailJudgingChallengesTypes} from "../Redux/DetailJudgingChallengesRedux"
import {UserDetailsTypes} from "../Redux/UserDetailsRedux"


import { GetUuidTypes } from '../Redux/GetUuidRedux'

import { VoteJudgeTypes } from '../Redux/VoteJudgeRedux'

import {UserEditTypes} from "../Redux/UserEditRedux"
import {ChallengeCategoryTypes} from "../Redux/ChallengeCategoryRedux"
import {CreateChallengeTypes} from "../Redux/CreateChallengeRedux"
import {EditChallengeTypes} from "../Redux/EditChallengeRedux"
import {ReportChallengeTypes} from "../Redux/ReportChallengeRedux"
import {WinChallengesTypes} from "../Redux/WinChallengesRedux"
import {FailChallengesTypes} from "../Redux/FailChallengesRedux"

import {SearchResultTypes} from "../Redux/SearchResultRedux"
/* ------------- Sagas ------------- */

//import {startup} from './StartupSagas'
import {login} from './LoginSagas'
//import {fbLogin} from './FbLoginSagas'
//import {getTemperature} from './TemperatureSagas'
import {getChallengeList} from './ChallengeListSagas'
import {openScreen} from './OpenScreenSagas'
import {getTrending} from './TrendingSagas'
import {getOpenFunding} from './OpenFundingSagas'
import {getJudgingChallenges} from './JudgingChallengesSagas'
//import {getJudgingDetailChallenges} from './JudgingChallengesDetailSagas'

import {getOpenChallenges} from './OpenChallengesSagas'
import {getPendingChallenges} from './PendingChallengesSagas'
import {getWinChallenges} from './WinChallengesSagas'
import {getFailChallenges} from './FailChallengesSagas'
import {getVideoUpload} from './VideoUploadSagas'

import {getFavoritesMembers} from './FavoritesMembersSagas'
import {getFavoritesChallenges} from './FavoritesChallengesSagas'

import {getPopularChallenges} from './PopularChallengesSagas'
import {getMyOpenChallenges} from './MyOpenChallengesSagas'
import {getMyJudgingChallenges} from './MyJudgingChallengesSagas'
import {getMyPendingChallenges} from './MyPendingChallengesSagas'

import {getMemberOpenChallenges} from './MemberOpenChallengesSagas'


import {getDetailJudgingChallenges} from './DetailJudgingChallengesSagas'
import {getUserDetails} from './UserDetailsSagas'

import {updateUserDetails} from './UserEditSagas'
import { getUuid } from './GetUuidSagas'

import { voteForJudge } from './VoteJudgeSagas'
import { getChallengeCategory } from './ChallengeCategorySagas'
import { createChallenge } from './CreateChallengeSagas'
import { editChallenge } from './EditChallengeSagas'
import { reportChallenge } from './ReportChallengeSagas'

import {searchResult} from './SearchResultSagas'
/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
// const api = DebugSettings.useFixtures ? FixtureAPI : API.create()
const mcApi = MCApi.create()
//const challengeApi = MCApi.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield [
        // some sagas only receive an action
    //takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(LoginTypes.LOGIN_REQUEST, login, mcApi),
    //takeLatest(FbLoginTypes.FB_LOGIN_REQUEST, fbLogin, mcApi),
    //takeLatest(OpenScreenTypes.OPEN_SCREEN, openScreen),
    takeLatest(ChallengeListTypes.CHALLENGE_LIST_REQUEST, getChallengeList, mcApi),
    takeLatest(TrendingTypes.TRENDING_REQUEST, getTrending, mcApi),
    takeLatest(OpenFundingTypes.OPEN_FUNDING_REQUEST, getOpenFunding, mcApi),
    takeLatest(JudgingChallengesTypes.JUDGING_CHALLENGES_REQUEST, getJudgingChallenges, mcApi),
    takeLatest(OpenChallengesTypes.OPEN_CHALLENGES_REQUEST, getOpenChallenges, mcApi),

    takeLatest(PendingChallengesTypes.PENDING_CHALLENGES_REQUEST, getPendingChallenges, mcApi),

    takeLatest(WinChallengesTypes.WIN_CHALLENGES_REQUEST, getWinChallenges, mcApi),
    takeLatest(FailChallengesTypes.FAIL_CHALLENGES_REQUEST, getFailChallenges, mcApi),
    takeLatest(MyOpenChallengesTypes.MY_OPEN_CHALLENGES_REQUEST, getMyOpenChallenges, mcApi),
    takeLatest(MyJudgingChallengesTypes.MY_JUDGING_CHALLENGES_REQUEST, getMyJudgingChallenges, mcApi),
    takeLatest(MyPendingChallengesTypes.MY_PENDING_CHALLENGES_REQUEST, getMyPendingChallenges, mcApi),

    takeLatest(MemberOpenChallengesTypes.MEMBER_OPEN_CHALLENGES_REQUEST, getMemberOpenChallenges, mcApi),

    takeLatest(PopularChallengesTypes.POPULAR_CHALLENGES_REQUEST, getPopularChallenges, mcApi),
    takeLatest(VideoUploadTypes.VIDEO_UPLOAD_REQUEST, getVideoUpload, mcApi),
    takeLatest(FavoritesMembersTypes.FAVORITES_MEMBERS_REQUEST, getFavoritesMembers, mcApi),
    takeLatest(FavoritesChallengesTypes.FAVORITES_CHALLENGES_REQUEST, getFavoritesChallenges, mcApi),
    takeLatest(UserDetailsTypes.USER_DETAILS_REQUEST, getUserDetails, mcApi),

    takeLatest(GetUuidTypes.GET_UUID_REQUEST, getUuid, mcApi),
    takeLatest(VoteJudgeTypes.VOTE_JUDGE_REQUEST, voteForJudge, mcApi),
    takeLatest(DetailJudgingChallengesTypes.DETAIL_JUDGING_CHALLENGES_REQUEST, getDetailJudgingChallenges, mcApi),
    takeLatest(UserEditTypes.USER_EDIT_REQUEST, updateUserDetails, mcApi),
    takeLatest(ChallengeCategoryTypes.CHALLENGE_CATEGORY_REQUEST, getChallengeCategory, mcApi),
    takeLatest(CreateChallengeTypes.CREATE_CHALLENGE_REQUEST, createChallenge, mcApi),
    takeLatest(EditChallengeTypes.EDIT_CHALLENGE_REQUEST, editChallenge, mcApi),
    takeLatest(ReportChallengeTypes.REPORT_CHALLENGE_REQUEST, reportChallenge, mcApi),

    takeLatest(SearchResultTypes.SEARCH_RESULT_REQUEST, searchResult, mcApi),
    // some sagas receive extra parameters in addition to an action


  ]
}
