/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to Sagas/index.js
*  - This template uses the api declared in Sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import SearchResultActions from '../Redux/SearchResultRedux'

export function * searchResult (api, action) {
console.log(action);
  const {default_type, page, challengeId, challenge_name, keyword } = action

  console.log(default_type, page, challengeId, challenge_name, keyword );

  switch (default_type) {
    case 'pagination':
      console.log('pagination',default_type, page);
      const responsePage = yield call(api.getChallengeList, '', 'open', page)
      // success?
      console.log(responsePage);
        if (responsePage.ok) {
        // You might need to change the response here - do this with a 'transform',
        // located in ../Transforms/. Otherwise, just pass the data back from the api.
        console.log(default_type);
        yield put(SearchResultActions.searchResultSuccess(responsePage.data,default_type))
      } else {
        yield put(SearchResultActions.searchResultFailure())
      }
      break;



      case 'Favourite':
         console.log('Favourite',default_type, page, challengeId);
         const responseFavourite = yield call(api.selectFavourite, challengeId)

         console.log(responseFavourite);

         if (responseFavourite.ok) {
           yield put(SearchResultActions.searchResultSuccess(responseFavourite.data,default_type,challengeId))
         }else {
            yield put(SearchResultActions.searchResultFailure())
          }
        break;
        case 'UnFavourite':
           console.log('UnFavourite',default_type, page, challengeId);
           const responseUnFavourite = yield call(api.unFavourite, challengeId)
           console.log(responseUnFavourite);
           if (responseUnFavourite.ok) {
             yield put(SearchResultActions.searchResultSuccess(responseUnFavourite.data,default_type,challengeId))
           }else {
              yield put(SearchResultActions.searchResultFailure())
            }
          break;




    default:
    // make the call to the api
    console.log(challenge_name, keyword);
    const response = yield call(api.searchResult, challenge_name, keyword)
    console.log(response);

    // success?
      if (response.ok) {
        yield put(SearchResultActions.searchResultSuccess({'payload':response.data,},default_type))
      
    } else {
      yield put(SearchResultActions.searchResultFailure())
    }
  }

}
