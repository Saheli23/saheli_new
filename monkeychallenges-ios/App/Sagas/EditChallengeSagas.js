import {call, put} from 'redux-saga/effects'
import EditChallengeActions from '../Redux/EditChallengeRedux'
import {AsyncStorage} from "react-native"
// attempts to login
export function * editChallenge (api, action) {
  let {default_type,price, name, zip_code, challenge_category_type_id,start_date,challenge_type_id,challenge_id,description, description_2, description_3,is_published} = action
  console.log(action);
  
  switch (default_type) {
    
      case 'stepThreeSubmit':
     
      const responseAfterSubmitStepThree = yield call(api.editChallenge,price, name, zip_code, challenge_category_type_id,start_date,challenge_type_id,challenge_id,description, description_2, description_3,is_published)
     // console.log(default_type,responseAfterSubmitStepThree);
        if (responseAfterSubmitStepThree.ok) {
          console.log("sagathreesubmit",responseAfterSubmitStepThree.data);
        yield put(EditChallengeActions.editChallengeSuccess(responseAfterSubmitStepThree.data, default_type))

      } else {
        yield put(EditChallengeActions.editChallengeFailure())
      }
      break;
      case 'stepFourSubmit':
     
      const responseAfterSubmitStepFour = yield call(api.editChallenge,price, name, zip_code, challenge_category_type_id,start_date,challenge_type_id,challenge_id,description, description_2, description_3,is_published)
      
        if (responseAfterSubmitStepFour.ok) {
         
        yield put(EditChallengeActions.editChallengeSuccess(responseAfterSubmitStepFour.data, default_type))

      } else {
        yield put(EditChallengeActions.editChallengeFailure())
      }
      break;
    default:
    // make the call to the api
    const response = yield call(api.createChallenge,price, name, zip_code, challenge_category_type_id,start_date)
    // success?
      if (response.ok) {
      
      yield put(EditChallengeActions.editChallengeSuccess(response.data))
    } else {
      yield put(EditChallengeActions.editChallengeFailure(response.data))
    }
  }
}
