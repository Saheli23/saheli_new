


import { call, put } from 'redux-saga/effects'
import UserEditActions from '../Redux/UserEditRedux'

export function * updateUserDetails (api, action) {
 let {message} = action
 console.log(message);
  // make the call to the api
  const response = yield call(api.updateUserDetails,message)
    console.log(response);
  // success?
  if (response.ok) {

    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(UserEditActions.userEditSuccess({'user':response.data}))
  } else {
    yield put(UserEditActions.userEditFailure())
  }
}
