/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to Sagas/index.js
*  - This template uses the api declared in Sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import FavoritesMembersActions from '../Redux/FavoritesMembersRedux'

export function * getFavoritesMembers (api, action) {
  const { default_type,followsUuid } = action
  // make the call to the api
console.log(default_type,followsUuid);

switch (default_type) {
  case 'UnFavourite':
  console.log('UnFavourite',default_type, followsUuid);
  const responseUnFavourite = yield call(api.unFavouriteMember, followsUuid)
  //console.log(responseUnFavourite);
  //window.myObj = responseUnFavourite;
  //debugger;
  if (responseUnFavourite.ok) {
    //console.log(responseUnFavourite);
    yield put(FavoritesMembersActions.favoritesMembersSuccess(responseUnFavourite.data,default_type,followsUuid))
  }else {
     yield put(FavoritesMembersActions.favoritesMembersFailure())
   }

    break;
  default:
  const response = yield call(api.getMyFavoritesMembers)
  // success?
  if (response.ok) {
    console.log(response);
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(FavoritesMembersActions.favoritesMembersSuccess(response.data))
  } else {
    yield put(FavoritesMembersActions.favoritesMembersFailure())
  }
}


}
