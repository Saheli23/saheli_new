import {call, put} from 'redux-saga/effects'
import LoginActions from '../Redux/LoginRedux'
import {AsyncStorage} from "react-native"
// attempts to login
export function * login (api, action) {
  let {username, password, facebook_token, loginType} = action
  console.log(action);
  // if(facebook_token == ''){
  // 	const response = yield call(api.login, username, password)

  // }else{
  // 	//facebook api
  // 	console.log("working");
  // }
  const response = yield call(api.login, username, password, facebook_token, loginType)

  // const response  = (facebook_token == '') ? yield call(api.login, username, password, facebook_token) : "nothing"


 console.log(response);
  if (response.ok) {
   console.log(response);
   let responseuser =  yield call(api.setAutoToken, response.data.access_token)
   console.log(responseuser.data.user.uuid);
   console.log(responseuser);
   api.setUuid(responseuser.data.user.uuid);
   //yield call(api.setUuid , responseuser.data.user.uuid)
   AsyncStorage.setItem('MCAccessToken',response.data.access_token)
   AsyncStorage.setItem('MCRefreshToken',response.data.refresh_token)
   AsyncStorage.setItem('MCUseruuid', responseuser.data.user.uuid);
   AsyncStorage.setItem('MCUserObj', JSON.stringify(responseuser.data.user));
   console.log(responseuser);
     const responsecategoryPage = yield call(api.getChallengeCategory);
     if(responsecategoryPage){
     AsyncStorage.setItem('MCChallengeCategory', JSON.stringify(responsecategoryPage.data.challenge_category_types));
   }
    yield put(LoginActions.loginSuccess(username, response.data))
  } else {
    yield put(LoginActions.loginFailure(response.data))
  }
}
