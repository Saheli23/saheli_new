/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to Sagas/index.js
*  - This template uses the api declared in Sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import PendingChallengesActions from '../Redux/PendingChallengesRedux'
//import {AsyncStorage} from "react-native"

export function * getPendingChallenges (api, action) {
  //let {uuid} = action
  //const { uuid } = action
  //console.log(uuid);
  //console.log(uuid);
  //const value = await AsyncStorage.getItem('MCUseruuid');
  // AsyncStorage.getItem('MCUseruuid', (err, result) => {
  //     console.log(result);
  //     var result = result;
  //     //this.setState({uuid:value});
  //     // => {'name':'Chris','age':31,'traits':{'shoe_size':10,'hair':'brown','eyes':'blue'}}
  //   });

  // make the call to the api eb4bed44-9034-42c0-b4a4-7412dfcca601
  //let responseuser =  yield call(api.setAutoToken, response.data.access_token)
      const response = yield call(api.getMyChallengeList, 'pending', 4)

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(PendingChallengesActions.pendingChallengesSuccess(response.data))
  } else {
    yield put(PendingChallengesActions.pendingChallengesFailure())
  }
}
