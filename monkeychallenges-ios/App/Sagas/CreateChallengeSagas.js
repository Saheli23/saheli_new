import {call, put} from 'redux-saga/effects'
import CreateChallengeActions from '../Redux/CreateChallengeRedux'
import {AsyncStorage} from "react-native"
// attempts to login
export function * createChallenge (api, action) {
  let {default_type,price, name, zip_code, challenge_category_type_id,start_date,challenge_type_id,challenge_id,description, description_2, description_3,is_published} = action
  console.log(action);
  console.log('default_type',default_type);
  

  switch (default_type) {
    case 'category':
      console.log('category',default_type);
      const responsePage = yield call(api.getChallengeCategory)
      console.log(default_type,responsePage);
        if (responsePage.ok) {

       yield put(CreateChallengeActions.createChallengeSuccess(responsePage.data.challenge_category_types,default_type))

      } else {
        yield put(CreateChallengeActions.createChallengeFailure())
      }
      break;

      case 'stepOneSubmit':

      const responseAfterSubmitStepOne = yield call(api.createChallenge,price, name, zip_code, challenge_category_type_id,start_date)
      console.log(default_type,responseAfterSubmitStepOne);
        if (responseAfterSubmitStepOne.ok) {
        yield put(CreateChallengeActions.createChallengeSuccess(responseAfterSubmitStepOne.data, default_type))

      } else {
        yield put(CreateChallengeActions.createChallengeFailure())
      }
      break;
      case 'nothing':
      console.log('nothing case');
      break;

    default:
    // make the call to the api
    const response = yield call(api.createChallenge,price, name, zip_code, challenge_category_type_id,start_date)
    // success?
      if (response.ok) {

      yield put(CreateChallengeActions.createChallengeSuccess(response.data))
    } else {
      yield put(CreateChallengeActions.createChallengeFailure(response.data))
    }
  }
}
