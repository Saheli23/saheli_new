import {call, put} from 'redux-saga/effects'
import ReportChallengeActions from '../Redux/ReportChallengeRedux'
import {AsyncStorage} from "react-native"
// attempts to login
export function * reportChallenge (api, action) {
  let {name, phone_no, mail,reason,subject,description,challenge_link,mailTo} = action
  console.log(action);


    // make the call to the api
    const response = yield call(api.reportChallenge,name, phone_no, mail,reason,subject,description,challenge_link,mailTo)
    // success?
      if (response.ok) {

      yield put(ReportChallengeActions.reportChallengeSuccess(response.data))
    } else {
      yield put(ReportChallengeActions.reportChallengeFailure())
    }

}
