/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to Sagas/index.js
*  - This template uses the api declared in Sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import DetailJudgingChallengesActions from '../Redux/DetailJudgingChallengesRedux'

export function * getDetailJudgingChallenges (api, action) {
  const {id, detail_type, vote} = action
  // make the call to the api
  console.log(id, detail_type, vote);
  switch (detail_type) {
     case 'vote':
      console.log(id, detail_type, vote);

       const responseVote = yield call(api.voteForJudge, vote, id)
      //const response = yield call(api.getChallengeDetail, id)
    // success?
    console.log(responseVote)
      console.log(responseVote.ok)
    if (responseVote.ok) {
       // You might need to change the response here - do this with a 'transform',
      // located in ../Transforms/. Otherwise, just pass the data back from the api.
      //  yield put(DetailJudgingChallengesActions.detailJudgingChallengesSuccess(response.data))

          const response_after_vote = yield call(api.getChallengeDetail, id)
            console.log(response_after_vote)
            console.log(response_after_vote.ok)
          if(response_after_vote.ok){
              console.log("response_after_vote.ok")
            yield put(DetailJudgingChallengesActions.detailJudgingChallengesSuccess(response_after_vote.data))
          }else {
            console.log("response_after_vote.ok else")
            yield put(DetailJudgingChallengesActions.detailJudgingChallengesFailure())
          }
    } else {
      yield put(DetailJudgingChallengesActions.detailJudgingChallengesFailure())
    }







      break;
    default:
    const response = yield call(api.getChallengeDetail, id)
  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(DetailJudgingChallengesActions.detailJudgingChallengesSuccess(response.data))
  } else {
    yield put(DetailJudgingChallengesActions.detailJudgingChallengesFailure())
  }
  }
//   console.log(id, detail_type, vote);
// //  const response = yield call(api.voteForJudge, vote, id)
//     const response = yield call(api.getChallengeDetail, id)
//   // success?
//   if (response.ok) {
//     // You might need to change the response here - do this with a 'transform',
//     // located in ../Transforms/. Otherwise, just pass the data back from the api.
//     yield put(DetailJudgingChallengesActions.detailJudgingChallengesSuccess(response.data))
//   } else {
//     yield put(DetailJudgingChallengesActions.detailJudgingChallengesFailure())
//   }
}
