// a library to wrap and simplify api calls
import apisauce from 'apisauce'

//import RNFS from 'react-native-fs'

import {AsyncStorage} from "react-native"

//import RNFetchBlob from 'react-native-fetch-blob'

// const _XHR = GLOBAL.originalXMLHttpRequest ?
//     GLOBAL.originalXMLHttpRequest :
//     GLOBAL.XMLHttpRequest

// XMLHttpRequest = _XHR

// our "constructor"
// http://api-dev.monkeychallenges.com/    api.monkeychallenges.com
const create = (baseURL = 'https://api-dev.monkeychallenges.com') => {
    // ------
    // STEP 1
    // ------
    //
    // Create and configure an apisauce-based api object.
    //
  const api = apisauce.create({
        // base URL is read from the "constructor"
    baseURL,
        // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
      // 'Content-Type': 'application/x-www-form-urlencoded'
    },
        // 10 second timeout...
    timeout: 10000
  })

    // Wrap api's addMonitor to allow the calling code to attach
    // additional monitors in the future.  But only in __DEV__ and only
    // if we've attached Reactotron to console (it isn't during unit tests).
  if (__DEV__ && console.tron) {
    api.addMonitor(console.tron.apisauce)
  }
  let uuid1;
    // ------
    // STEP 2
    // ------
    //
    // Define some functions that call the api.  The goal is to provide
    // a thin wrapper of the api layer providing nicer feeling functions
    // rather than "get", "post" and friends.
    //
    // I generally don't like wrapping the output at this level because
    // sometimes specific actions need to be take on `403` or `401`, etc.
    //
    // Since we can't hide from that, we embrace it by getting out of the
    // way at this level.
    //
const login = (username, password, facebook_token, loginType) => {
    console.log(facebook_token);
if(loginType == 'login' ){
 return api.post('oauth/access_token', {
    username: username,
    password: password,
    grant_type: 'password',
    client_id: 'gsqm2F7Yxv7fcJaux20D',
    client_secret: '6eLhtyD9KgEuXyq6qVef'
  })
}else if(loginType == 'fblogin'){
return api.post('oauth/access_token', {
    facebook_token: facebook_token,
    grant_type: 'facebook',
    client_id: 'gsqm2F7Yxv7fcJaux20D',
    client_secret: '6eLhtyD9KgEuXyq6qVef'
  })
}


}


const fblogin = (facebook_token) => {
   console.log(facebook_token);
 return api.post('oauth/access_token', {
    facebook_token: facebook_token,
    grant_type: 'facebook',
    client_id: 'gsqm2F7Yxv7fcJaux20D',
    client_secret: '6eLhtyD9KgEuXyq6qVef'
  })
}

const updateUserDetails = (message) => {
console.log('message',message);
console.log(`/api/users/${uuid1}`);
 return api.put(`/api/users/${uuid1}`, {
    message: message,
    grant_type: 'password',
    client_id: 'gsqm2F7Yxv7fcJaux20D',
    client_secret: '6eLhtyD9KgEuXyq6qVef'
  })



}

const createChallenge = (price,name,zip_code,challenge_category_type_id,start_date) => {

 return api.post(`/api/users/${uuid1}/challenges`, {
    price: price,
    name: name,
    challenge_category_type_id: challenge_category_type_id,
    start_date: start_date,
    challenge_type_id:1,
    zip_code:zip_code,
    title:name,
    is_published:false
  })



}

const editChallenge = (price,name,zip_code,challenge_category_type_id,start_date,challenge_type_id,challenge_id,description, description_2, description_3,is_published) => {

 return api.put(`/api/users/${uuid1}/challenges/${challenge_id}`, {
    price: price,
    name:  name,
    challenge_category_type_id: challenge_category_type_id,
    challenge_type_id:1,
    zip_code: zip_code,
    title: name,
    start_date: start_date,
    description: description,
    description_2:description_3,
    description_3:description_2,
    is_published:is_published
  })



}

const reportChallenge = (name, phone_no, mail,reason,subject,description,challenge_link,mailTo) => {
  let message="Name :"+name+" , Phone :"+phone_no+" , email :"+mail+"  , Reason :"+reason+" , Link to Campaign :"+challenge_link+" , Subject :"+subject+" , Description :"+description;
 return api.post(`api/message`, {
    from: mail,
    message:  message,
    to: mailTo,

  })
}

const getPopularUsers = (filter_by) => api.get('api/popular', {
    filter_by: filter_by
})

  const getChallengeList = (filter_by, by_status, page) =>{

    console.log('filter_by:',filter_by,'by_status:', by_status,'page:', page);
    return api.get('api/challenges', {
      filter_by: filter_by,
      status: by_status,
      limit: 3,
      sort:'desc',
      page: page,
    })
  }

  const selectFavourite = (challengeId) => {
    return api.post(`/api/users/${uuid1}/favorites/challenges`, {
      "uuid": uuid1,
      "challenge_id": challengeId
     })
  }



  const unFavourite = (challengeId) =>{
   return api.delete(`/api/users/${uuid1}/favorites/challenges/${challengeId}`, {
     "uuid": uuid1,
     "challenge_id": challengeId
    })
  }

  const unFavouriteMember = (follows_uuid) =>{
console.log(follows_uuid)
   return api.delete(`/api/users/${uuid1}/follows/${follows_uuid}`, {
     "uuid": uuid1,
     "follows_uuid": follows_uuid
    })
  }

  const getMyChallengeList = (status, limit, page) => {
    // console.log(uuid1);
    // console.log(status, limit);
    return api.get('api/challenges', {
    status: status,
    limit: 3,
    uuid:uuid1,
    page:page,
    filter_by:'new',
  })
}
const searchResult = (challenge_name, keyword) =>{
console.log(challenge_name, keyword);
  // /api/search?name=challenge&challenge_category_type_id=1&limit=10   name: status,
  return api.get('/api/search', {

  challenge_category_type_id: challenge_name,
  name:keyword,
  uuid:uuid1,

})
}
const getMemberChallengeList = (status, limit, page, userid) => {
     console.log(status, limit, page, userid);
    // console.log(status, limit);
    return api.get('api/challenges', {
    status: status,
    limit: 3,
    uuid:userid,
    page:page,
    filter_by:'new',
  })
}




 const getMyFavoritesMembers = () => {
  //console.log(`/api/public/users/${uuid1}/social`);
  return api.get(`/api/public/users/${uuid1}/social`)
}

 const getMyFavoriteschallenges = () => {
  //console.log(`/api/public/users/${uuid1}/social`);
  return api.get(`/api/users/${uuid1}/favorites/challenges`)
}

const getUserDetails = () =>  {
     return api.get(`/api/users/${uuid1}`)
}

const getMemberDetails = (userid) =>  {
  console.log(userid);
     return api.get(`/api/public/users/${userid}`)
}


//test story api
//   POST /api/users/{uuid}/challenges/{challenge_id}/story Create a user challenge story
// uuid,challenge_id,description,media_type_id=7,file

// const getStoryList = (uuid, challenge_id, description, media_type_id, file) => api.post('api/challenges', {
//     uuid : uuid,
//     challenge_id : challenge_id,
//     description : description,
//     media_type_id : 6,
//     file:file
//   })

// test video upload selected

// const getVideoUpload = (selected) =>api.post('api/challenges', {
//     uuid : uuid,
//     challenge_id : "58aeb9572a8f7c305a2262b2",
//     description : "Test description",
//     media_type_id : 6,
//     file:selected



//   })

const getStoryUpload = ( _id , uuid, data) => {
return RNFetchBlob.fetch('POST', 'https://api.monkeychallenges.com/api/users/{uuid}/challenges/{_id}/story', {
    'Cache-Control': 'no-cache',
    Authorization: 'Bearer GERDkLFhUZYs9U2Ngxr6HMDk8XijwsQrcrNXqlGr',
    'Content-Type' : 'multipart/form-data',
  },data).then((resp) => {
    console.log(resp);
    return resp
  }).catch((err) => {
    return err
  })

}


  //console.log(_id , uuid, data)

//  /api/users/{uuid}/challenges/{challenge_id}/story
// RNFS.uploadFiles({
//   toUrl: "https://api.monkeychallenges.com/api/users/eb4bed44-9034-42c0-b4a4-7412dfcca601/challenges/58aeb9572a8f7c305a2262b2/story",
//   files: selected,
//   method: 'POST',
//   headers: {
//     'Cache-Control': 'no-cache',
//     'Authorization': 'Bearer zkHesjafuBf8Lmi6KhOkxqVcvdqlRgfnUCWkGNxk'
//   },
//   fields: {
//     description : "Test description",
//     media_type_id : 1,

//   }
// }).promise.then((response) => {
//     if (response.statusCode == 200) {
//       console.log('FILES UPLOADED!'); // response.statusCode, response.headers, response.body
//     } else {
//       console.log('SERVER ERROR',response);
//     }
//   })
//   .catch((err) => {
//     if(err.description === "cancelled") {
//       // cancelled by user
//     }
//     console.log(err);
//   });


//  /api/users
// /api/challenges/588ad3b42a8f7c57870d5462
 const userid = (filter_by, by_status) => api.get('/api/users', {
    filter_by: filter_by,
    status: by_status
  })

 const perticularChallangDetail = (challenge_id) => api.get('/api/challenges/588ad3b42a8f7c57870d5462', {
    challenge_id:challenge_id

  })



  const getPendingList = (filter_by, by_status) => api.get('api/challenges', {
    filter_by: filter_by,
    status: by_status
  })



// async setTokenItem(token) {
//   try {
//    await AsyncStorage.setItem('MCtoken', token)
//   } catch (error) {
//     console.log(error)
//     // Handle errors here
//   }
// }



  const setAutoToken = (token) => {
     //console.log(token)
     AsyncStorage.setItem('MCtoken', token)
     //this.setTokenItem(token)
    api.setHeader('Authorization', 'Bearer ' + token)
    return api.get('api/users')
  }

  const setUuid = (data) => {
    //console.log(data);
    AsyncStorage.setItem('MYuuid', data)
    uuid1 = data;
  }

  const getUuid = () => uuid1

  const voteForJudge = (vote, challenge_id) => {
     console.log(vote, challenge_id)
     //{"vote":vote}
     // /api/users/{uuid}/challenges/{challenge_id}/votes
     // console.log(`/api/users/${uuid1}/challenges/${challenge_id}/votes`);
      return api.post(`/api/users/${uuid1}/challenges/${challenge_id}/votes`, {
       "vote": vote
      })
   // return {ok:true,data:{vote:true}}
  }

  const getChallengeCategory = () =>{

    return api.get(`/api/types/category`)
  }

  const getChallengeDetail = (id) =>{
    console.log(id);
    return api.get(`/api/challenges/${id}`)
  }



    // ------
    // STEP 3
    // ------
    //
    // Return back a collection of functions that we would consider our
    // interface.  Most of the time it'll be just the list of all the
    // methods in step 2.
    //
    // Notice we're not returning back the `api` created in step 1?  That's
    // because it is scoped privately.  This is one way to create truly
    // private scoped goodies in JavaScript.
    //
  return {
    // a list of the API functions from step 2
    login,
    fblogin,
    setUuid,
    getUuid,
    voteForJudge,
    getChallengeList,
    getChallengeDetail,
    getChallengeCategory,
    getMyChallengeList,
    getMemberChallengeList,
    selectFavourite,
    unFavourite,
    searchResult,
    unFavouriteMember,
    getMyFavoriteschallenges,
    getUserDetails,
    getMemberDetails,
    getMyFavoritesMembers,
    getPopularUsers,
    setAutoToken,
    updateUserDetails,
    createChallenge,
    editChallenge,
    reportChallenge
  }
}

// let's return back our create method as the default.
export default {
  create
}
