// @flow

export default {
  // Functions return fixtures
  getCity: (city: string) => {
    // This fixture only supports Boise or else returns toronto
    const boiseData = require('../Fixtures/boise.json')
    const torontoData = require('../Fixtures/toronto.json')

    return new Promise((resolve, reject) => {
      resolve({
        ok: true,
        data: city.toLowerCase() === 'boise' ? boiseData : torontoData
      })
    })
  },

  login: (username: string, password: string) => {
    return {
      ok: true,
      data: username
    }
  }
}
