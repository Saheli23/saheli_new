// @flow
import React from 'react'
import {Text, Dimensions, Platform,View,Image } from 'react-native'
import { Scene, Router, TabBar, Modal, Schema, Actions, Reducer, ActionConst} from 'react-native-router-flux'
import Styles from './Styles/NavigationContainerStyle'
import NavigationDrawer from './NavigationDrawer'
import NavItems from './NavItems'
import CustomNavBar from '../Navigation/CustomNavBar'

// screens identified by the router
import PresentationScreen from '../Containers/PresentationScreen'
import LandingScreen from '../Containers/LandingScreen'
import AllComponentsScreen from '../Containers/AllComponentsScreen'
import UsageExamplesScreen from '../Containers/UsageExamplesScreen'
import LoginScreen from '../Containers/LoginScreen'
import ListviewExample from '../Containers/ListviewExample'
import ListviewGridExample from '../Containers/ListviewGridExample'
import ListviewSectionsExample from '../Containers/ListviewSectionsExample'
import ListviewSearchingExample from '../Containers/ListviewSearchingExample'
import MapviewExample from '../Containers/MapviewExample'
import APITestingScreen from '../Containers/APITestingScreen'
import ThemeScreen from '../Containers/ThemeScreen'
import DeviceInfoScreen from '../Containers/DeviceInfoScreen'
import PostLogin from '../Containers/PostLogin'

import JudgingChallengesView from '../Containers/JudgingChallengesView'
import OpenChallengesView from '../Containers/OpenChallengesView'
import PopularChallengesView from '../Containers/PopularChallengesView'
import ChallengeDetailView from '../Containers/ChallengeDetailView'
import PendingChallengesView from '../Containers/PendingChallengesView'
import GalleryView from '../Containers/GalleryView'
import WinChallengesView from '../Containers/WinChallengesView'
import FailChallengesView from '../Containers/FailChallengesView'
import FavoritesChallenges from "../Containers/Favorites/FavoritesChallenges"




import FavoritesMembersView from "../Containers/Favorites/FavoritesMembers"


import SearchResultView from '../Containers/SearchResultView'

import MyOpenChallengesView from "../Containers/MyChallenges/MyOpenChallengesView"
import MyJudgingChallengesView from "../Containers/MyChallenges/MyJudgingChallengesView"
import MyPendingChallengesView from "../Containers/MyChallenges/MyPendingChallengesView"


import OpenChallengeDetailView from '../Containers/OpenChallengeDetailView'
//import JudgingChallengeDetailView from '../Containers/JudgingChallengeDetailView'

import JudgingChallengeDetailPage from '../Containers/JudgingChallengeDetailPage'

import PopularChallengeDetailView from '../Containers/PopularChallengeDetailView'
import ReportChallenge from '../Containers/ReportChallenge'

import MyProfile from '../Containers/MyProfile/MyProfile'
import EditProfile from '../Containers/MyProfile/EditProfile'


import MemberProfile from '../Containers/MemberProfile/MemberProfile'

// Create step forms
import StepOne from '../Containers/Create/StepOne'
import StepTwo from '../Containers/Create/StepTwo'
import StepThree from '../Containers/Create/StepThree'
import StepFour from '../Containers/Create/StepFour'
import StepFive from '../Containers/Create/StepFive'


//web links
import ChangePassword from '../Containers/AccountSettings/ChangePassword'
import DeleteAccount from '../Containers/AccountSettings/DeleteAccount'
import MyFunds from '../Containers/AccountSettings/MyFunds'
import PaymentOptions from '../Containers/AccountSettings/PaymentOptions'
import Privacy from '../Containers/AccountSettings/Privacy'
import Terms from '../Containers/AccountSettings/Terms'
import PrivacyPolicy from '../Containers/AccountSettings/PrivacyPolicy'
import TransactionHistory from '../Containers/AccountSettings/TransactionHistory'





import Home from '../Containers/Home'
import TabView from '../Containers/TabView'

import ModelTest from '../Containers/ModelTest'


import FundChallenge from '../Containers/FundChallenge'
import ShareChallenge from '../Containers/ShareChallenge'
import FacebookShare from '../Containers/FacebookShare'
import TwitterShare from '../Containers/TwitterShare'

//tab icons

import TabIcon from '../Components/TabIcon'
import TopTabIcon from '../Components/TopTabIcon'
import NavigationDrawerTab from '../Navigation/NavigationDrawerTab';
import Signup from '../Containers/Signup'
import Profile from '../Containers/Profile'

import CreateChallenge from '../Containers/CreateChallenge'
import {Images, Metrics} from '../Themes'
import resolveAssetSource from 'resolveAssetSource';



import { Actions as NavigationActions } from 'react-native-router-flux'


import NavigationStateHandler from 'react-native-router-flux-hooks'
const navigationStateHandler = new NavigationStateHandler()
/* **************************
* Documentation: https://github.com/aksonov/react-native-router-flux

***************************/

const HEADER_PATTERN = resolveAssetSource(require('../Images/mc_logo.png'));
//==========================================

const reducerCreate = params=>{
    const defaultReducer = Reducer(params);
    return (state, action)=>{
        console.log("ACTION:", action);
          return defaultReducer(state, action);
    }
};





//===========================


const renderLogo = () => {
  return (
    <View style={{ alignItems: 'center', width: 25, height: 25 }}>
      <Image source={Images.clearLogo}
             style={{ width: 40, height: 40 }} />

     </View>
  );
};

// /
const refreshOnBack = () => {

   Actions.pop();
   //Actions.MyProfile();
//    setTimeout(() => {
// // Actions.refresh({name:'zzzzar'});
// //Actions.refresh();
// //Actions.JudgingChallengesView();
// //Actions.refresh({key:'JudgingChallengesView',name:'zzzzar'});
//  //Actions.pop({ refresh: { test: true }})
//
//  //navigationStateRouter.addEvent(ActionConst.RESET);
//  console.log("zzzz");
//
// }, 500);
//Actions.refresh({name:'MyProfile'});
}
class NavigationRouter extends React.Component {

  render () {
    let webViewBaseLink = 'https://dev-1.monkeychallenges.com';
    global.webViewBaseLink=webViewBaseLink;
    return (
  <Router createReducer={reducerCreate} panHandlers={null}  createReducer={navigationStateHandler.getReducer.bind(navigationStateHandler)}
   navigationStateHandler={navigationStateHandler}>


    <Scene key='drawer' component={NavigationDrawer} open={false}>

        <Scene key='drawerChildrenWrapper' navigationBarStyle={Styles.navBar} hideNavBar={true} titleStyle={Styles.title} leftButtonIconStyle={Styles.leftButton} rightButtonTextStyle={Styles.rightButton}>




        {/*
          <Scene key="authentication" initial type={ActionConst.PUSH_OR_POP} >
            <Scene initial key='landingScreen'  component={LandingScreen} title='Monkey Challenges' hideNavBar={true} />
           <Scene key='login' type={ActionConst.PUSH_OR_POP} component={LoginScreen} title='Login' hideNavBar={true} />
           <Scene key='signup' type={ActionConst.PUSH_OR_POP} component={Signup} title='Signup' hideNavBar={true} />
           </Scene>
          */}
            <Scene initial key='landingScreen' type={ActionConst.PUSH_OR_POP} initial component={LandingScreen} title='Monkey Challenges' hideNavBar={true} />
           <Scene key='login' type={ActionConst.PUSH_OR_POP} component={LoginScreen} title='Login' hideNavBar={true} />
           <Scene key='signup' type={ActionConst.PUSH_OR_POP} component={Signup} title='Signup' hideNavBar={true} titleStyle={Styles.title} />

            <Scene key='ModelTest' type={ActionConst.PUSH_OR_POP}  style={{backgroundColor:"rgba(0,0,0,0)"}} component={ModelTest} schema="modal" direction="vertical" hideNavBar={true}/>


    <Scene key="postLogin" type={ActionConst.RESET} navigationBarStyle={Styles.navBar} leftButtonIconStyle={Styles.leftButton} rightButtonTextStyle={Styles.rightButton}>


{/* web links */}
 <Scene key='sharechallenge' component={ShareChallenge} title="Share Challenge" hideNavBar={false} renderLeftButton={NavItems.backButton} />
 <Scene key='facebookshare' component={FacebookShare} title="Share challenge" hideNavBar={false} renderLeftButton={NavItems.backButton} />
 <Scene key='twittershare' component={TwitterShare} title="Share challenge" hideNavBar={false} renderLeftButton={NavItems.backButton} />
    <Scene key='profile' component={Profile} title="Profile" hideNavBar={false} renderLeftButton={NavItems.backButton} titleStyle={Styles.title} />
    <Scene key='changePassword' component={ChangePassword} title="Change Password" hideNavBar={false} renderLeftButton={NavItems.backButton} titleStyle={Styles.title} />
<Scene key='deleteAccount' component={DeleteAccount} title="Delete Account" hideNavBar={false} renderLeftButton={NavItems.backButton} titleStyle={Styles.title} />
  <Scene key='myFunds' component={MyFunds} title="My Funds" hideNavBar={false} renderLeftButton={NavItems.backButton} titleStyle={Styles.title} />
    <Scene key='paymentOptions' component={PaymentOptions} title="Payment Options" hideNavBar={false} renderLeftButton={NavItems.backButton} titleStyle={Styles.title} />
      <Scene key='privacy' component={Privacy} title="Privacy" hideNavBar={false} renderLeftButton={NavItems.backButton} titleStyle={Styles.title} />
    <Scene key='transactionHistory' component={TransactionHistory} title="Transaction History" hideNavBar={false} renderLeftButton={NavItems.backButton} titleStyle={Styles.title} />
    <Scene key='terms' component={Terms} title="Terms of Service" hideNavBar={false} renderLeftButton={NavItems.backButton} titleStyle={Styles.title} />
    <Scene key='privacypolicy' component={PrivacyPolicy} title="Privacy Policy" hideNavBar={false} renderLeftButton={NavItems.backButton} titleStyle={Styles.title} />

     <Scene key='terms' component={Terms} title="Terms" hideNavBar={false} renderLeftButton={NavItems.backButton} />
            <Scene key='createchallenge' component={CreateChallenge} title="Create challenge" hideNavBar={false} renderLeftButton={NavItems.backButton} />

      <Scene key='ChallengeDetailView' titleStyle={Styles.title} component={ChallengeDetailView}  title='Challenge Detail' hideNavBar={false} renderLeftButton={NavItems.backButton}/>

      <Scene key='GalleryView' titleStyle={Styles.title} component={GalleryView}  title='Pending Challenge GalleryView' hideNavBar={false} renderLeftButton={NavItems.backButton}/>
      <Scene key='OpenChallengeDetailView' titleStyle={Styles.title} component={OpenChallengeDetailView}  title='Open Challenge Detail' hideNavBar={false} renderLeftButton={NavItems.backButton}/>
       <Scene key='report' titleStyle={Styles.title} component={ReportChallenge}  title='Report Challenge' hideNavBar={false} renderLeftButton={NavItems.backButton}/>
      {/*
      <Scene key='JudgingChallengeDetailView' component={JudgingChallengeDetailView}  title='Judging Challenge Detail' hideNavBar={false} renderLeftButton={NavItems.backButton}/>
      */}


      <Scene key='JudgingChallengeDetailView' titleStyle={Styles.title} component={JudgingChallengeDetailPage}  title='Judging Challenge Detail' hideNavBar={false} renderLeftButton={NavItems.backButton}/>

      <Scene key='PopularChallengeDetailView' titleStyle={Styles.title} component={PopularChallengeDetailView} title='Popular Challenge Detail' hideNavBar={false} renderLeftButton={NavItems.backButton}/>
      <Scene key='FundChallenge' component={FundChallenge} title='Fund Challenge Detail'    hideNavBar={false} renderLeftButton={NavItems.backButton}/>

 <Scene key="memberProfile" title="Member Profile" component={MemberProfile} hideNavBar={false} renderLeftButton={NavItems.backButton}/>
 <Scene key="winChallengesView" title="Won Challenges" component={WinChallengesView} hideNavBar={false} renderLeftButton={NavItems.backButton}/>



 <Scene key="SearchResultView" type={ActionConst.PUSH_OR_POP} title="Search Results" component={SearchResultView} hideNavBar={false} leftButtonIconStyle={Styles.leftButton} renderLeftButton={NavItems.hamburgerButton}  />
<Scene key="failChallengesView" title="Failed Challenges" component={FailChallengesView} hideNavBar={false} renderLeftButton={NavItems.backButton}/>
<Scene key='Home' initial={true}>
    <Scene key="main" tabs tabBarStyle={Styles.tabBarStyle} tabBarSelectedItemStyle={Styles.tabBarSelectedItemStyle}>
      <Scene key="tab1" initial title="HOME"  icon={TabIcon} >
        <Scene key="tabbar2"  title="HOME" tabs={true} tabBarStyle={Styles.tabBarTopStyle} tabBarSelectedItemStyle={Styles.tabBarTopSelectedItemStyle} titleStyle={Styles.title}>

        <Scene navBar={CustomNavBar}  key="OpenChallengesView"  title="Open" component={OpenChallengesView} icon={TopTabIcon} initial={true} navigationBarStyle={Styles.navBar} titleStyle={Styles.title} hideNavBar={false} leftButtonIconStyle={Styles.leftButton} renderLeftButton={NavItems.hamburgerButton} renderRightButton={NavItems.searchButton} />

        <Scene navBar={CustomNavBar} key="JudgingChallengesView" title="Judging" component={JudgingChallengesView} icon={TopTabIcon} navigationBarStyle={Styles.navBar} titleStyle={Styles.title} leftButtonIconStyle={Styles.leftButton} rightButtonTextStyle={Styles.rightButton} hideNavBar={false} renderLeftButton={NavItems.hamburgerButton} renderRightButton={NavItems.searchButton} />
        <Scene navBar={CustomNavBar} key="PopularChallengesView" title="Popular" component={PopularChallengesView} icon={TopTabIcon} navigationBarStyle={Styles.navBar} titleStyle={Styles.title} leftButtonIconStyle={Styles.leftButton} rightButtonTextStyle={Styles.rightButton} hideNavBar={false} renderLeftButton={NavItems.hamburgerButton} renderRightButton={NavItems.searchButton} />
       </Scene>
    </Scene>

        <Scene key="tab2" title="FAVORITES" icon={TabIcon}>
         <Scene key="tab21" title="FAVORITES" tabs={true} tabBarStyle={Styles.tabBarTopStyle} tabBarSelectedItemStyle={Styles.tabBarTopSelectedItemStyle} titleStyle={Styles.title}>
{/*
<Scene navBar={CustomNavBar} key="FavoritesChallengeskey" title="Challanges" component={FavoritesChallenges} icon={TopTabIcon} initial={true} navigationBarStyle={Styles.navBar} titleStyle={Styles.title} hideNavBar={false} leftButtonIconStyle={Styles.leftButton} renderLeftButton={NavItems.hamburgerButton} renderRightButton={NavItems.searchButton} />

  */}
          <Scene navBar={CustomNavBar} key="FavoritesChallengeskey" title="Challanges" component={FavoritesChallenges} icon={TopTabIcon} initial={true} navigationBarStyle={Styles.navBar} titleStyle={Styles.title} hideNavBar={false} leftButtonIconStyle={Styles.leftButton} renderLeftButton={NavItems.hamburgerButton} renderRightButton={NavItems.searchButton} />
           <Scene navBar={CustomNavBar} key="FavoritesMembers" title="Members" component={FavoritesMembersView} icon={TopTabIcon} navigationBarStyle={Styles.navBar} titleStyle={Styles.title} leftButtonIconStyle={Styles.leftButton} rightButtonTextStyle={Styles.rightButton} hideNavBar={false} renderLeftButton={NavItems.hamburgerButton} renderRightButton={NavItems.searchButton} />

          </Scene>
        </Scene>






<Scene key="tab3" title="CREATE"  icon={TabIcon} titleStyle={Styles.title}  navigationBarStyle={Styles.navBar} leftButtonIconStyle={Styles.leftButton} rightButtonTextStyle={Styles.rightButton}>
    <Scene key="stepOne" navBar={CustomNavBar} hideTabBar={true} initial={true} title="stepOne" component={StepOne} icon={TopTabIcon} navigationBarStyle={Styles.navBar} titleStyle={Styles.title} />
    <Scene key='stepTwo' component={StepTwo} title='Post Challenge - Step 2' hideNavBar={false} renderLeftButton={NavItems.backButton}/>
    <Scene key='stepThree' component={StepThree} title='Post Challenge - Step 3' hideNavBar={false} renderLeftButton={NavItems.backButton}/>
    <Scene key='stepFour' component={StepFour} title='Post Challenge - Step 4' hideNavBar={false} renderLeftButton={NavItems.backButton}/>
    <Scene key='stepFive' component={StepFive} navBar={CustomNavBar} title='Post Challenge - Step 5' hideNavBar={false}  />
</Scene>



       <Scene key="tab4" title="MY CHALLENGES" icon={TabIcon}>
       <Scene key="tabbar4" title="MY CHALLENGES" tabs={true} tabBarStyle={Styles.tabBarTopStyle} tabBarSelectedItemStyle={Styles.tabBarTopSelectedItemStyle} titleStyle={Styles.title}>

        <Scene key="MyPendingChallengesView" navBar={CustomNavBar} title="Pending" component={MyPendingChallengesView} icon={TopTabIcon} initial={true} navigationBarStyle={Styles.navBar} titleStyle={Styles.title} leftButtonIconStyle={Styles.leftButton} rightButtonTextStyle={Styles.rightButton} hideNavBar={false} renderLeftButton={NavItems.hamburgerButton} renderRightButton={NavItems.searchButton} />


        <Scene key="MyOpenChallengesView" navBar={CustomNavBar} title="Open" component={MyOpenChallengesView} icon={TopTabIcon} navigationBarStyle={Styles.navBar} titleStyle={Styles.title} leftButtonIconStyle={Styles.leftButton} rightButtonTextStyle={Styles.rightButton} hideNavBar={false} renderLeftButton={NavItems.hamburgerButton} renderRightButton={NavItems.searchButton}/>

        <Scene key="MyJudgingChallengesView" navBar={CustomNavBar} title="Judging" component={MyJudgingChallengesView} icon={TopTabIcon} navigationBarStyle={Styles.navBar} titleStyle={Styles.title} leftButtonIconStyle={Styles.leftButton} rightButtonTextStyle={Styles.rightButton} hideNavBar={false} renderLeftButton={NavItems.hamburgerButton} renderRightButton={NavItems.searchButton} />
         </Scene>
         </Scene>


    <Scene key="tab5" navBar={CustomNavBar}  title="MY PROFILE" icon={TabIcon} titleStyle={Styles.title}>

  <Scene key="MyProfile" navBar={CustomNavBar} initial={true} title="MY PROFILE" component={MyProfile} icon={TopTabIcon} navigationBarStyle={Styles.navBar} titleStyle={Styles.title} leftButtonIconStyle={Styles.leftButton} rightButtonTextStyle={Styles.rightButton} hideNavBar={false} renderLeftButton={NavItems.hamburgerButton} renderRightButton={NavItems.searchButton} />

    </Scene>


{/*

<Scene key="tab5" navBar={CustomNavBar}  title="MY PROFILE" type={ActionConst.POP_AND_REPLACE} component={MyProfile} icon={TabIcon} navigationBarStyle={Styles.navBar} titleStyle={Styles.title} leftButtonIconStyle={Styles.leftButton} rightButtonTextStyle={Styles.rightButton} hideNavBar={false} renderLeftButton={NavItems.hamburgerButton} renderRightButton={NavItems.searchButton} />

  */}

    </Scene>

</Scene>
        <Scene key='EditProfile' component={EditProfile}  onBack={refreshOnBack} hideNavBar={false}/>





{/*
 <Scene key='postLoginhome' component={PostLogin}  title='Monkey Challenges' hideNavBar={false} renderLeftButton={NavItems.hamburgerButton}/>

            <Scene key='componentExamples' component={AllComponentsScreen} title='Components' />
            <Scene key='usageExamples' component={UsageExamplesScreen} title='Usage' rightTitle='Example' onRight={()=> window.alert('Example Pressed')} />
             <Scene key='listviewExample' component={ListviewExample} title='Listview Example' />
                <Scene key='listviewGridExample' component={ListviewGridExample} title='Listview Grid' />
                <Scene key='listviewSectionsExample' component={ListviewSectionsExample} title='Listview Sections' />
                <Scene key='listviewSearchingExample' component={ListviewSearchingExample} title='Listview Searching' navBar={CustomNavBar} />
                <Scene key='mapviewExample' component={MapviewExample} title='Mapview Example' />
                <Scene key='apiTesting' component={APITestingScreen} title='API Testing' />
                <Scene key='theme' component={ThemeScreen} title='Theme' />

                <Scene key='deviceInfo' component={DeviceInfoScreen} title='Device Info' />
*/}

                        </Scene>



            </Scene>

        </Scene>

</Router>
    )
  }
}



export default NavigationRouter
