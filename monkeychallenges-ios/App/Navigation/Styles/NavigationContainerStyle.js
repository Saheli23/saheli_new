// @flow

import {Colors, Fonts} from '../../Themes/'
import {Dimensions, Platform } from 'react-native'


export default {
  container: {
    flex: 1
  },

  navBar: {
    backgroundColor: Colors.background,

  },

  title: {
    color: Colors.panther,
    fontSize: 15,
    
  },
  leftButton: {
    tintColor: Colors.panther,
    
  },
  rightButton: {
    color: Colors.panther
  },

  tabBarStyle: {
    height:65,
    backgroundColor: '#ffffff',
    borderTopWidth : 3,
    borderColor    : '#ccc',
    position:'relative',
          

  },
  tabBarSelectedItemStyle: {
  backgroundColor: '#ffffff',   
  

  },
  tabBarTopStyle: {
    backgroundColor: '#ffffff',
      height:50,
    position:'absolute',
    top:(Platform.OS === 'ios') ? 64 : 54, 

  },
  tabBarTopSelectedItemStyle: {
    borderBottomWidth : 3,
    borderColor    : '#f9642f',

  },
}
