import { Colors, Metrics, Fonts } from '../../Themes/'

export default {
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: Metrics.navBarHeight,
    paddingTop: Metrics.smallMargin,
    paddingHorizontal: 5,
    backgroundColor: Colors.background,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth : 3,
    borderColor    : '#ccc',
  },
  title: {
    flex: 1,
    textAlign: 'center',
    color: Colors.panther,
    marginTop: Metrics.doubleBaseMargin,
    backgroundColor: Colors.transparent,
    fontWeight: 'bold',
    fontSize: Fonts.size.input
  },
  logo: {

    height: Metrics.icons.large,
    width: (Metrics.icons.large + 5)
  },
  rightButtons: {
    alignSelf: 'center',
    flex: 1,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  leftButtons: {
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  settingsButtons: {
    alignSelf: 'center',
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'row'
  }
}
