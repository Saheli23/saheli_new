// @flow

import React, { PropTypes,} from 'react'
import {
  TouchableOpacity,
  AsyncStorage
 } from 'react-native'
import styles from './Styles/NavItemsStyle'
import { Actions as NavigationActions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Colors, Metrics } from '../Themes'




const openDrawer = () => {
  NavigationActions.refresh({
    key: 'drawer',
    open: true
  })
}
const openSearch = () => {
 // alert("search!!");
  //NavigationActions.ModalExample()
  NavigationActions.ModelTest()
}

const goHome = ()=>{
AsyncStorage.setItem('FromHome', 'home')
  NavigationActions.OpenChallengesView()

}


export default {

  backButton () {
    return (
      <TouchableOpacity onPress={NavigationActions.pop}>
        <Icon name='angle-left'
          size={Metrics.icons.large}
          color={Colors.panther}
          style={styles.backButton}
        />
      </TouchableOpacity>
    )
  },

  hamburgerButton () {
    return (
      <TouchableOpacity onPress={openDrawer}>
        <Icon name='cog'
          size={Metrics.icons.medium}
          color={Colors.steel}
          style={styles.navButtonLeft}
        />
      </TouchableOpacity>
    )
  },
  HomeButton () {
    return (
      <TouchableOpacity onPress={goHome}>
        <Icon name='home'
          size={Metrics.icons.medium}
          color={Colors.steel}
          style={styles.navButtonLeft}
        />
      </TouchableOpacity>
    )
  },
  shareButton () {
    return (
      <TouchableOpacity onPress={alert("share")}>
        <Icon name='share'
          size={Metrics.icons.medium}
          color={Colors.steel}
          style={styles.navButtonLeft}
        />
      </TouchableOpacity>
    )
  },

  searchButton (callback: Function) {
    return (

      <TouchableOpacity onPress={openSearch}>
        <Icon name='search'
          size={Metrics.icons.medium}
          color={Colors.panther}
          style={styles.searchButton}
        />
      </TouchableOpacity>

    )
  }


}
