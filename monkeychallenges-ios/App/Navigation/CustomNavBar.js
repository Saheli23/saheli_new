import React, { PropTypes } from 'react'
import { View, Image, LayoutAnimation, ScrollView,Text,StyleSheet,TouchableOpacity,
    Dimensions,Button  } from 'react-native'
import NavItems from './NavItems'
import styles from './Styles/CustomNavBarStyle'
import SearchBar from '../Components/SearchBar'
import { connect } from 'react-redux'
import { Metrics, Images } from '../Themes'
import SearchActions from '../Redux/SearchRedux'


import Modal from 'react-native-modalbox'
import Slider from 'react-native-slider'
var screen = Dimensions.get('window');
const styles1 = StyleSheet.create({

  wrapper: {
    paddingTop: 50,
    flex: 1
  },

  modal: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  modal1:{
    backgroundColor:'red',

  },

  modal2: {
    height: 230,
    backgroundColor: "#3B5998"
  },

  modal3: {
    height: 300,
    width: 300
  },

  modal4: {
    height: 300
  },

  btn: {
    margin: 10,
    backgroundColor: "#3B5998",
    color: "white",
    padding: 10
  },

  btnModal: {
    position: "absolute",
    top: 0,
    right: 0,
    width: 50,
    height: 50,
    backgroundColor: "transparent"
  },

  text: {
    color: "black",
    fontSize: 22
  }

});
class CustomNavBar extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      showSearchBar: false,
      isOpen: false,
      isDisabled: false,
      swipeToClose: true,
      sliderValue: 0.3
    }
  }
  onClose() {
    console.log('Modal just closed');
  }

  onOpen() {
    console.log('Modal just openned');
  }

  onClosingState(state) {
    console.log('the open/close of the swipeToClose just changed');
  }
  showSearchBar = () => {
    this.setState({showSearchBar: true})
  }

  cancelSearch = () => {
    this.setState({showSearchBar: false})
    this.props.cancelSearch()
  }

  // onSearch = (searchTerm) => {
  //   this.props.performSearch(searchTerm)
  // }

  renderMiddle () {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    if (this.state.showSearchBar) {
      return null
      {/*
      <SearchBar onSearch={this.props.performSearch} searchTerm={this.props.searchTerm} onCancel={this.cancelSearch} />
      */}
    } else {
      return (
        <Image style={styles.logo} source={Images.clearLogo} />
      )
    }
  }

  renderRightButtons () {
    if (this.state.showSearchBar) {
      return <View style={{width: Metrics.icons.medium}} />
    } else {
      return (
        <View style={styles.rightButtons}>
          {NavItems.searchButton(this.showSearchBar)}
        </View>
      )
    }
  }

  renderLeftButtons () {
    if (this.state.showSearchBar) {
      return null
    } else {
      return (
        <View style={styles.settingsButtons}>
          {NavItems.hamburgerButton()}
        </View>
      )
    }
  }
  renderHomeButtons(){
    if (this.state.showSearchBar) {
      return null
    } else {
      return (
        <View style={styles.settingsButtons}>
          {NavItems.HomeButton()}
        </View>
      )
    }
  }

  render () {
    let state = this.props.navigationState
    let selected = state.children[state.index]
    while (selected.hasOwnProperty('children')) {
      state = selected
      selected = selected.children[selected.index]
    }
    console.log('create',this.props);


     if (this.props.title == "stepOne" || this.props.name == "stepFive"){
    return (
      <View style={styles.container}>
        {this.renderHomeButtons()}
        {this.renderMiddle()}
        {this.renderRightButtons()}

      </View>
    )
  }
  else{
    return (
      <View style={styles.container}>
        {this.renderLeftButtons()}
        {this.renderMiddle()}
        {this.renderRightButtons()}

      </View>
    )
  }
  }
}

CustomNavBar.propTypes = {
  navigationState: PropTypes.object,
  navigationBarStyle: View.propTypes.style
}

const mapStateToProps = (state) => {
  return {
  //  searchTerm: state.search.searchTerm
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    //performSearch: (searchTerm) => dispatch(SearchActions.search(searchTerm)),
    //cancelSearch: () => dispatch(SearchActions.cancelSearch())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomNavBar)
