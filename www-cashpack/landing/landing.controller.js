var landingModule = angular.module('landingModule');
landingModule.controller('landingController', ['$scope','$state','$stateParams', '$cookieStore', 'headerFactory', '$location',function($scope,$state,$stateParams,$cookieStore, headerFactory, $location) {
    // $scope.extensionpath = "http://www.cashpack.org/extension/cashpack-extension.crx";
    // if ($location.host() == "localhost" || $location.host() == "dev02.developer24x7.com") { $scope.extensionpath = "http://" + $location.host() + "/www-cashpack/extension/cashpack-extension.crx"; }
    // if($location.host() =="webtest.cashpack.org")
    // {
    // $scope.extensionpath = "http://webtest.cashpack.org/extension/cashpack-extension.crx";

    // }
        
        //$scope.resetFrminput={"hash":$location.$$url.substring(8),"password2":''};
        // $scope.hasher =  


        // console.log($location);
 if($location.$$path=='/forget')
 {
    // $scope.resetFrminput.hash=$location.$$url.substring(8);
   angular.element('#reset-password-pop').openModal();            
 }
  if($location.$$path=='/signup')
 {
    angular.element('#signup-pop').openModal();            
 }
   
    $scope.swiper = {};
    $scope.inst=function(){
        chrome.webstore.install('https://chrome.google.com/webstore/detail/hcggnikhmdneeojjdcnbojmpjbglobme',
         function(e){
            //console.log(e);
            }, function(er){
            //console.log(er);
            })
    };
       $scope.playIt = function() {
      if ($("#video1").get(0).paused) {$("#video1").get(0).play();}
      else {$("#video1").get(0).pause();}
    };
        $scope.start=function(){
            angular.element(".vidcls").css("display", "block");
           angular.element('#vidModal').openModal();
            angular.element(".vidcls").css("z-index", "9999");
        $scope.playIt();
        
        };

        $scope.close=function(){
            $("#video1").get(0).pause();
            $("#video1").get(0).currentTime = 0;
            angular.element('#vidModal').closeModal();
            angular.element('.lean-overlay').css("display", "none");
        };

    if ($scope.breakpoints == 'eventSlider') {
        var bp = { 1024: { slidesPerView: 6, spaceBetween: 0 }, 768: { slidesPerView: 5, spaceBetween: 0 }, 600: { slidesPerView: 4, spaceBetween: 0 }, 500: { slidesPerView: 3, spaceBetween: 0 }, 400: { slidesPerView: 2, spaceBetween: 0 }, 200: { slidesPerView: 1, spaceBetween: 0 } };
    }
    $scope.sliderList = [];
    $scope.allList = [];
    $scope.toptrendingList = [];
    $scope.className = "";
    $scope.offerList = [];

       $scope.isLoggedIn = function() {
        return ($cookieStore.get('userToken') && $cookieStore.get('userId')) ? true : false

    }
    $scope.loggedIn = $scope.isLoggedIn();



    $scope.showOrHide = function() {
        $scope.className = $scope.className ? "" : "show";
    };

    headerFactory
        . getstoreAllList({source:'website'})
        .success(function(result) {
            if (result && result.meta.error === false) {
                $scope.getstoreAllList = result.data;
            }
        })
        .error(function(err) {
            Materialize.toast(err.data, 4000);
        });
 

    /** Get All List **/
    $scope.data = {
        userToken: $cookieStore.get('userToken'),
        userId: $cookieStore.get('userId'),
        source: 'website'
    };

    headerFactory
        .getAllList({source:'website'})
        .success(function(result) {
            
            if (result && result.meta.error === false) {
                $scope.allList = result.data;
            }
        })
        .error(function(err) {
            Materialize.toast(err.data, 4000);
            //console.log("err:", err);
        });



    headerFactory
        .getLatestofferList({source:'website'})
            .success(function(result) {

                if (result && result.data.length > 0) {
                    $scope.getLatestofferList = result.data;
                }
            }).
             error(function(errors) {
                Materialize.toast(errors.data, 4000);
            //console.log("errors:", errors);
             });

    /** Get Top Trending List **/
    headerFactory
        .gettoptrending($scope.data)
        .success(function(result) {
            if (result && result.meta.error === false) {
                $scope.toptrendingList = result.data;
            }
        });

 ;

    headerFactory.getLatestofferList({source:'website'})
        .success(function(result) {
            
            if (result && result.data.length > 0) {
                $scope.offerList = result.data;
            }
        }).
    error(function(errors) {
        Materialize.toast(err.data, 4000);
        //console.log("errors:", errors);
    });


    $scope.myVar = false;


    $scope.toggle = function() {
        $scope.myVar = !$scope.myVar;
        $scope.isToggle = ($scope.isToggle) ? false : true;
    };



    var obj = { "records": 3 };
    $scope.obj1 = {};   
    $scope.obj2 = {};
    $scope.obj3 = {};

    $scope.topMerchantList = {};
    // Top Merchant for specific merchants
    headerFactory.getMarchentDetails('macys.com',{source:'website', country : 'US'})
        .success(function(result) {
            // console.log("result",result);
            var total = (result.data[0].deals).length + (result.data[0].codes).length;

            $scope.obj1 = { "name": result.data[0].name, "cashbackValue": result.data[0].cashbackValue, "cashbackType": result.data[0].cashbackType, "sum": total, "domain": result.data[0].domain,"image":'top-merchants1.png',"id":result.data[0].id,"favorite":result.data[0].favorite,"favId":result.data[0].favoriteId };
            
        });

    headerFactory.getMarchentDetails('barnesandnoble.com',{source:'website', country : 'US'})
        .success(function(result) {
             
                var total = (result.data[0].deals).length + (result.data[0].codes).length;

            $scope.obj2 = { "name": result.data[0].name, "cashbackValue": result.data[0].cashbackValue, "cashbackType": result.data[0].cashbackType, "sum": total, "domain": result.data[0].domain,"image":'top-merchants2.png',"id":result.data[0].id,"favorite":result.data[0].favorite,"favId":result.data[0].favoriteId };


            

        });

    headerFactory.getMarchentDetails('walmart.com',{source:'website', country : 'US' })
        .success(function(result) {
            // console.log("result2",result);
              var total = (result.data[0].deals).length + (result.data[0].codes).length;

            $scope.obj3 = { "name": result.data[0].name, "cashbackValue": result.data[0].cashbackValue, "cashbackType": result.data[0].cashbackType, "sum": total, "domain": result.data[0].domain,"image":'top-merchants3.png',"id":result.data[0].id,"favorite":result.data[0].favorite,"favId":result.data[0].favoriteId };

            
        });





}]);
