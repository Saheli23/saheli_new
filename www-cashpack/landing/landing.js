var landingModule = angular.module('landingModule', ['ui.router']);

landingModule.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.get = { 'Content-Type': 'application/x-www-form-urlencoded' };
    $httpProvider.defaults.headers.post = { 'Content-Type': 'application/x-www-form-urlencoded' };
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};

    $stateProvider
        .state('main.base.landing', {
            url: '',
            cache: false,
            data: {
              pageTitle: 'Cashpack Home'
            },
            views: {
                'content': {
                    templateUrl: 'landing/landing_temp.html',
                    controller: 'landingController'
                }
            }  
        })
        .state('main.base.landing.forget', { 
            url: 'forget?HashTerm', 
            cache: false,
            data: {
              pageTitle: 'Forget password'
            },
            controller: 'landingController',
            params: {
                HashTerm : null,
            } 
         })
        .state('main.base.landing.signup', { 
            data: {
              pageTitle: 'Signup'
            },
            url: 'signup?HashTerm',
            cache: false,
          controller: 'landingController',
            params: {
                HashTerm : null,
            } 
        })
        .state('main.base.landing.login', { 
           
            data: {
              pageTitle: 'Login'
            },
            cache: false,
            url: 'login',
             onEnter: function($timeout) { $timeout(function() {angular.element('#login-pop').openModal();},4000)
         } 
        })
        .state('main.base.landing.forgetpass', { 
           
            data: {
              pageTitle: 'mail us'
            },
            cache: false,
            url: 'forpass',
             onEnter: function($timeout,$cookieStore,$state,$location) { if($cookieStore.get('userToken') && $cookieStore.get('userId')){
                        $state.go("main.base.settings");
                        $timeout(function() {$location.hash('faqpass');},1000)
                      } 
                      else{
                        $timeout(function() {angular.element('#wrong-pop').openModal();},4000)
                      }}
        });

    $urlRouterProvider.otherwise('/');
});
