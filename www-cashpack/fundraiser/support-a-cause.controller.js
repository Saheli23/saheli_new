fundraiserModule = angular.module('fundraiserModule');

fundraiserModule.controller('supportacauseController',['$scope', '$location','$document','headerFactory','$state','$stateParams','$timeout', function($scope, $location,$document,headerFactory,$state,$stateParams, $timeout) {
    $scope.catFm = {'records' : 100,'start' : 0 };
    //$scope.choiceval = $stateParams.selectTerm;
    $scope.serachtext = $stateParams.searchcatTerm;
    $scope.serachfrstat = $stateParams.searchcatSt;
    $scope.serachcity = $stateParams.searchcatCty;
    $scope.serachzip = $stateParams.searchcatZip;
    $scope.page = 0;
    $scope.SearchText=$stateParams.searchcatTerm;

    $document.on('click', function(e) {
        if(!$(e.target).hasClass('Autcom')){
            // $scope.allList=[];
            // $scope.showAdvanced=true;
            angular.element('.Autcom').css({
                "display" : "none"
            });
            // location.href=$location.$$absUrl;
            angular.element('.advanced-charity').css({
                "display" : "block !important"
            });
             angular.element('.advanced-charity').removeClass('padup');

        }
    });

    headerFactory
      .statesList()
      .success(function(res) {
          $scope.statelist = res;
          $scope.statelist.unshift({"name" : "Select a State","abbreviation" : ""})
          $scope.selected=$scope.statelist[0];
                          angular.forEach($scope.statelist, function(value, key) {
                    if (value.abbreviation == $scope.serachfrstat) {
                        $scope.selected = value;
                    }
                })
      });
    //change select dropdown
    $scope.searcher=function(valid){
        if (valid) {
            // console.log("srh",$scope.state,$scope.SearchCity,$scope.SearchZip);
            $state.go("main.base.supportcauselisting", {searchcatSt: $scope.selected.abbreviation, searchcatCty: angular.element("#city").val(), searchcatZip: angular.element("#Zipcode").val(), searchcatTerm: angular.element("#SearchText").val()},{reload:true});
            // $state.go("main.base.supportcauselisting", { selectTerm: $scope.select.value1, searchcatTerm: angular.element("#SearchText").val()},{reload:true});
        }
    };
     $scope.allList = [];
     $scope.SearchKeyword='';
     
     $scope.searchonkey=function(){
      $scope.SearchKeyword=angular.element('#SearchText').val();
       if( $scope.SearchKeyword !=''){

        headerFactory
            .getQuickSearch({ searchTerm: $scope.SearchKeyword })
            .success(function(response) {
                $scope.allList = response.data;
                self.loadingMsg = '';
                if($scope.allList.length<1){
                  angular.element('.advanced-charity').removeClass('padup');
                };
                if($scope.allList.length>2){
                  angular.element('.advanced-charity').addClass('padup');
                };
            })
            .error(function(error) {
            })
          
        }
        if($scope.allList.length<5){
          angular.element("ul.Autocom").css({"overflow": "none !important;"});
        };
        if($scope.SearchKeyword==''){
           $scope.allList = [];
        }
    };
    $scope.closer = function(){
      angular.element("ul.Autocom").css({"display" : "none"});
    };
    $scope.formatLabel = function(model) {
      console.log(model);
    for (var i=0; i< $scope.allList.length; i++) {
      if (model === $scope.allList[i].ein) {
        return $scope.allList[i].charity;
      }
    }
  }

$(".advanced-charity span img").click(function(){
$scope.closer();
});
    // $scope.addTocatFm = function() {
    //   $scope.searcher();
    //     // $scope.choiceval = choiceval;
        // $scope.statechange();
    //     if($scope.SearchText.length==4){
    //       $timeout(function(){
    //           $state.go("main.base.supportcauselisting", {searchcatSt: $scope.selected.abbreviation, searchcatCty: angular.element("#city").val(), searchcatZip: angular.element("#Zipcode").val(), searchcatTerm: angular.element("#SearchText").val()});
    //       },2000);
    //     }
    // }
   // $scope.select = {
   //      value: "category",
   //      choices: [{
   //          key : 'School',
   //          value : 'School'
   //      }, {
   //          key : 'Sports',
   //          value : 'Sports'
   //      }, {
   //          key : 'Non-profit',
   //          value : 'Non-Profit'
   //      }]
   //  };zipCode,city,state
    $scope.statechange = function(){
      // $state.go("main.base.supportcauselisting", { selectTerm: $scope.choiceval, searchcatTerm: $scope.serachtext});
        $state.go("main.base.supportcauselisting", {searchcatSt: $scope.selected.abbreviation, searchcatCty: angular.element("#city").val(), searchcatZip: angular.element("#Zipcode").val(), searchcatTerm: angular.element("#SearchText").val()},{reload:true});

    }

    $scope.searchResult = function(){
      // if($scope.choiceval === 'School'){
      //   var obj = {"searchTerm": $scope.serachtext , "records": 24 ,"page": $scope.page };
      //   headerFactory
      //       .schoolsList(obj)
      //       .success(function(result) {
      //           $scope.images="Mask.png";
      //           $scope.listsupport = result.data.records;
      //           $scope.totalRecords = result.data.totalRecords;
      //           $scope.recordsPerPage = result.data.recordsPerPage;
      //           self.loadingMsg = '';
      //       })

      //       .error(function(err) { self.loadingMsg = ''; });
      // }else{
        //$scope.category = $scope.choiceval !== 'Sports' ? "?" : "N";
        // var obj = {"query": $scope.serachtext, "category": $scope.category, "records": 24 ,"start": $scope.page };
        var obj = {"query": $scope.serachtext,"state" : $scope.serachfrstat,"city": $scope.serachcity,"zipCode": $scope.serachzip, "records": 24 ,"start": $scope.page };
        headerFactory
            .orgCharity(obj)
            .success(function(result) {
                // if($scope.choiceval==='Sports'){
                //   $scope.images="sportcause.png";
                // }
                // else{
                //   $scope.images="causeall.png";
                // }
                $scope.listsupport = result.data.data;
                $scope.totalRecords = result.data.data.length > 0 ? result.data.data[0].recordCount : "";
                $scope.recordsPerPage = 24;
                self.loadingMsg = '';
            })
            .error(function(err) { self.loadingMsg = ''; });
      // }
    }
    $scope.callSearchSupport = function(valid) {
        if(valid)
        {
          // $scope.catFm = {'records' : 100,'start' : 0 };
          // $scope.catFm[$scope.choiceval] =  angular.element("#searchterm").val();
          // headerFactory.
          //   orgCharity(obj)
          //   .success(function(res){
          //       $scope.listsupport=res.data.data;
          //   })
          //   .error(function(err){
          //       console.log("error",err);
          //   })
          // $scope.page = 1; //initialize page number to 1 if clicked on search button
          $scope.serachtext = angular.element("#searchterm").val();
          $scope.statechange();
        }
        else{
          Materialize.toast("Please select category and enter keywords to search causes", 4000);
        }
    }

      $scope.findNextSupport = function(page){
          // console.log(page);
          $scope.page = parseInt(page);
          $scope.serachtext = angular.element("#searchterm").val();
          $scope.searchResult();
      }

      $scope.searchResult();

       $scope.searchperfect=function(value){
      $state.go("main.base.supportcauselisting", {searchcatSt: $scope.selected.abbreviation, searchcatCty: angular.element("#city").val(), searchcatZip: angular.element("#Zipcode").val(), searchcatTerm: value},{reload:true});

    };
      $scope.AddFavCause = function(res){
        var obj = {"ein": res.ein},
            name = res.charityName.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        
        headerFactory
            .AddUserCauses(obj)
            .success(function(result) {
              console.log("res",result);
              Materialize.toast(name + " from " + res.state + "<br/> has been added to your Causes",6000);
            })
            .error(function(err) {
              console.log("res",result);
            });
      }
}]);
