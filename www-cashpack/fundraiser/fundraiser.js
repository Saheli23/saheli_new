var fundraiserModule = angular.module('fundraiserModule', ['ui.router']);

fundraiserModule.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

    $stateProvider
        .state('main.base.fundraiser', {
            url: 'fundraiser',
            cache: false,
            data: {
              pageTitle: 'Cashpack-Support a cause'
            },
            views: {
                'content': {
                    templateUrl: 'fundraiser/fundraiser.html',
                    controller: 'fundraiserController'
                }
            },
            ncyBreadcrumb: {
                parent: 'Home',

                label: 'fundraiser'
            }
        })
        .state('main.base.createfundraiser', {
            url: 'createfundraiser',
            cache: false,
            data: {
              pageTitle: 'Cashpack-Create a cause'
            },
            views: {
                'content': {
                
                    templateUrl: 'fundraiser/create-fundraiser.html',
                    controller: 'createfundraiserController'
                }
            },
            ncyBreadcrumb: {
                parent: 'fundraiser',
                label: 'create fundraiser '
            }
        })
        .state('main.base.mycauses', {
            url: 'mycauses',
            cache: false,

            views: {
                'content': {
                    templateUrl: 'fundraiser/my-causes.html',

                }
            },
            ncyBreadcrumb: {
                parent: 'mycauses',
                label: 'mycauses '
            }
        })

        .state('main.base.supportcauses', {
            url: 'supportcauses/:searchOption',
            cache: false,
            data: {
              pageTitle: 'Cashpack-Support a cause'
            },
            views: {
                'content': {
                    templateUrl: 'fundraiser/support-a-cause.html',
                    controller: 'createfundraiserController'
                }
            },
            params: {
                searchOption: ''
            },
            ncyBreadcrumb: {
                parent: 'supportcauses',
                label: 'supportcauses '
            }
        })
        .state('main.base.fundraiserdonateform', {
            url: 'fundraiserdonateform',
            cache: false,
            data: {
              pageTitle: 'Cashpack-Create a form'
            },
            views: {
                'content': {
                    templateUrl: 'fundraiser/fundraiser-donate-form.html',
                    controller: 'donateController'
                }
            },
            ncyBreadcrumb: {
                parent: 'fundraiser-donate-form',
                label: 'fundraiser-donate-form '
            }
        })
        .state('main.base.supportcauselisting', {
            // url: 'supportcauselisting?selectTerm&searchcatTerm',
            url: 'supportcauselisting',
            // url: 'supportcauselisting?searchcatTerm&searchcatSt&searchcatCty&searchcatZip',


            cache: false,
            data: {
              pageTitle: 'Cashpack-Search a cause'
            },
            views: {
                'content': {
                    templateUrl: 'fundraiser/support-a-cause-listing.html',
                    controller: 'supportacauseController'
                    //controllerAs: 'createfundraiserController'
                }
            },
            params: {
                // selectTerm   : null,
                searchcatTerm: null,
                searchcatSt:null,
                searchcatCty:null,
                searchcatZip:null
            },
            ncyBreadcrumb: {
                parent: 'supportcauselisting',
                label: 'supportcauselisting'
            }
        })
        .state('main.base.mycausesaftergoalseting', {
            url: 'mycausesaftergoalseting',
            cache: false,

            views: {
                'content': {
                    templateUrl: 'fundraiser/my-causes-after-goal-seting.html',
                    controller: 'createfundraiserController',

                }
            },
            ncyBreadcrumb: {
                parent: 'mycausesaftergoalseting',
                label: 'mycausesaftergoalseting'
            }
        })
       .state('main.base.payment', {
            url: 'payment',
            cache: false,
            data: {
              pageTitle: 'Cashpack-Payment'
            },
            views: {
                'content': {
                    templateUrl: 'fundraiser/payment.html',
                    controller: 'paymentController'
                }
            },
            params: {
               
                paymentMethod: null,
                ein: null,
                charityname: null,
                Zip: null,
                city: null
            },
            ncyBreadcrumb: {
                parent: 'payment',
                label: 'payment'
            },
            resolve: {
             data: function($q, $state,$timeout,$cookieStore) {
                    var deferred = $q.defer();
                     $timeout(function() {
                      if (angular.isUndefined($cookieStore.get('userId'))) {
                          $state.go('main.base.landing');
                          Materialize.toast("Please login or signup first", 4000);

                          deferred.reject();
                      } else {
                        deferred.resolve();
                       }
                    });

                    return deferred.promise;
                  }
    }
        })
       .state('main.base.supportacauseabout', {
            url: 'supportacauseabout/:einDomainName',
            cache: false,
            data: {
              pageTitle: 'Cashpack-About a cause'
            },
            views: {
                'content': {
                    templateUrl: 'fundraiser/supportacause-about.html',
                    controller: 'aboutSupportController'
                }
            },
            params: {
                einDomainName: null
            },
            ncyBreadcrumb: {
                parent: 'supportacauseabout',
                label: 'supportacauseabout'
            }
        })
       .state('main.base.refer', {
            url: 'refer',
            cache: false,
            data: {
              pageTitle: 'Cashpack-Refer a Friend'
            },
            views: {
                'content': {
                    templateUrl: 'fundraiser/refer.html',
                    controller: 'inviteController'
                }
            },
            ncyBreadcrumb: {
                parent: 'refer',
                label: 'refer'
            },
            resolve: {
             data: function($q, $state,$timeout,$cookieStore) {
                    var deferred = $q.defer();
                     $timeout(function() {
                      if (angular.isUndefined($cookieStore.get('userId'))) {
                          $state.go('main.base.landing');
                          Materialize.toast("Please login or signup first", 4000);

                          deferred.reject();
                      } else {
                        deferred.resolve();
                       }
                    });

                    return deferred.promise;
                  }
    }
        })
       .state('main.base.feedback', {
            url: 'feedback',
            cache: false,
            data: {
              pageTitle: 'Cashpack-Feedback'
            },
            views: {
                'content': {
                    templateUrl: 'fundraiser/feedback.html',
                    controller: 'feedbackController'
                }
            },
            ncyBreadcrumb: {
                parent: 'feedback',
                label: 'feedback'
            }
        })
       .state('main.base.schoolsignup', {
            url: 'schoolsignup',
            cache: false,
            data: {
              pageTitle: 'Cashpack-Create a cause'
            },
            views: {
                'content': {
                    templateUrl: 'fundraiser/schoolsignup.html',
                    controller: 'fundraiserController'
                }
            },
            ncyBreadcrumb: {
                parent: 'schoolsignup',
                label: 'schoolsignup'
            }
        })
       .state('main.base.MyCreatedCauses', {
            url: 'MyCreatedCauses',
            cache: false,
            data: {
              pageTitle: 'Cashpack-Created causes'
            },
            views: {
                'content': {
                    templateUrl: 'fundraiser/MyCreatedCauses.html',
                    controller: 'fundraiserController'
                }
            },
            ncyBreadcrumb: {
                parent: 'MyCreatedCauses',
                label: 'MyCreatedCauses'
            },
            resolve: {
             data: function($q, $state,$timeout,$cookieStore) {
                    var deferred = $q.defer();
                     $timeout(function() {
                      if (angular.isUndefined($cookieStore.get('userId'))) {
                          $state.go('main.base.landing');
                          Materialize.toast("Please login or signup first", 4000);

                          deferred.reject();
                      } else {
                        deferred.resolve();
                       }
                    });

                    return deferred.promise;
                  }
    }
        });

});
