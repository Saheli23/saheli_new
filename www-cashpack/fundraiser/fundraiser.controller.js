fundraiserModule = angular.module('fundraiserModule');
fundraiserModule.controller('createfundraiserController', ['$scope','$document', '$location','$state','$stateParams','headerFactory','$sessionStorage','$timeout',function($scope, $document, $location,$state,$stateParams,headerFactory,$sessionStorage,$timeout) {
    //$scope.SearchText='';

    headerFactory
            .statesList()
            .success(function(res) {
                $scope.statelist = res;
                $scope.statelist.unshift({"name" : "Select a State","abbreviation" : ""})
                $scope.selected=$scope.statelist[0];

            });
    $scope.allList = [];
     $scope.SearchKeyword='';
    
    $document.on('click', function(e) {
        if(!$(e.target).hasClass('Autcom')){
            // $scope.allList=[];
            // $scope.showAdvanced=true;
            angular.element('.Autcom').css({
                "display" : "none"
            });
            // location.href=$location.$$absUrl;
            angular.element('.advanced-charity').css({
                "display" : "block !important"
            });
             angular.element('.advanced-charity').removeClass('padup');

        }
    }); 
     $scope.searchonkey=function(){
      $scope.SearchKeyword=angular.element('#SearchText').val();
       if( $scope.SearchKeyword !=''){

        headerFactory
            .getQuickSearch({ searchTerm: $scope.SearchKeyword })
            .success(function(response) {
                $scope.allList = response.data;
                self.loadingMsg = '';
                if($scope.allList.length<1){
                  angular.element('.advanced-charity').removeClass('padup');
                };
                if($scope.allList.length>2){
                  angular.element('.advanced-charity').addClass('padup');
                };
            })
            .error(function(error) {
            })
        if($scope.allList.length<5){
          angular.element("ul.Autocom").css({"overflow-y": "auto !important;"});
        }
        }
        if($scope.SearchKeyword==''){
           $scope.allList = [];
        }
    };
    $scope.closer = function(){
      angular.element("ul.Autocom").css({"display" : "none"});
    };
            // $timeout($scope.late,3000);
            // $scope.late=function()
            // {if (!angular.isUndefined($scope.selected.abbreviation)) {
            //                 $scope.state = $scope.selected.abbreviation;
            //             } else {
            //                 $scope.state = "";
            //             }
            //           };
    //var urlarr = $location.absUrl().split('/');
    //angular.element(".adv-src .chosen-container-single a.chosen-default span").html("States");
    //$scope.select.value1 = (urlarr[urlarr.length - 1]) ? urlarr[urlarr.length - 1] : "School";

    // $scope.supportoptions = {
    //     choices: ["School", "Sports", "Non-profit"]
    // };
    $scope.searchperfect=function(value){
      $state.go("main.base.supportcauselisting", {searchcatSt: $scope.selected.abbreviation, searchcatCty: angular.element("#city").val(), searchcatZip: angular.element("#Zipcode").val(), searchcatTerm: value},{reload:true});

    };
    $scope.searcher=function(valid){
        if (valid) {
          // console.log("srh",$scope.state,$scope.SearchCity,$scope.SearchZip);
            $state.go("main.base.supportcauselisting", {searchcatSt: $scope.selected.abbreviation, searchcatCty: angular.element("#city").val(), searchcatZip: angular.element("#Zipcode").val(), searchcatTerm: angular.element("#SearchText").val()},{reload:true});

            // $state.go("main.base.supportcauselisting", { selectTerm: $scope.select.value1, searchcatTerm: angular.element("#SearchText").val()},{reload:true});
            
        }
    }


}]);

fundraiserModule.controller('fundraiserController', ['$scope', '$location',function($scope, $location) {
    $scope.select = {
        value: "Title",
        choices: ["Title", "I'm an option", "This is materialize", "No, this is Patrick."]
    };
    $scope.Location = {
        value: "Location",
        choices: ["Location", "I'm an option", "This is materialize", "No, this is Patrick."]
    };

}]);








fundraiserModule.controller('donateController', ['$scope', '$location',function($scope, $location) {

    $scope.select = {
        value: "Option",
        choices: ["Option1", "I'm an option", "This is materialize", "No, this is Patrick."]
    };
    var urlarr = $location.absUrl().split('/');

    $scope.select.value1 = (urlarr[urlarr.length - 1]) ? urlarr[urlarr.length - 1] : "School";

    $scope.supportoptions = {
        choices: ["School", "Sports", "Non-Profit"]
    };
     $scope.donate=function(valid){
     	if(valid){
     		console.log(donatelist);
     	}
     };
     $scope.planingdonate=function(valid){
     	if(valid){
     		//console.log(donatelist1);
     	}
     };
     $scope.monthlyplaningdonate=function(valid){
     	if(valid){
     		//console.log(donatelist2);
     	}
     };
     $scope.planingdonatemore=function(valid){
     	if(valid){
     		//console.log(donatelist3);
     	}
     };


}]);

fundraiserModule.controller('aboutSupportController', ['$scope', '$stateParams', '$cookieStore', 'headerFactory', '$document','$location','$state',function($scope, $stateParams, $cookieStore, headerFactory, $document,$location,$state) {
    $scope.param = {ein: $location.$$url.substring(20)};
    headerFactory.
        GetAboutInfo($scope.param)
            .success(function(response) {
                $scope.about=response.data.data;
                $scope.createMarker($scope.about);
            })
            .error(function(err) {
            });
    $scope.createMarker = function(info) {
        var myCenter = new google.maps.LatLng(info.latitude,info.longitude);
          var mapCanvas = document.getElementById("map");
          var mapOptions = {center: myCenter, zoom: 5};
          var map = new google.maps.Map(mapCanvas, mapOptions);
          var marker = new google.maps.Marker({position:myCenter});
          marker.setMap(map);

          google.maps.event.addListener(marker,'click',function() {
            var infowindow = new google.maps.InfoWindow({
              content:"<div style='width: 200px;height: auto;word-spacing: 1px;font-size: 10px;line-height: 15px;'><span style='font-weight: bold;'>"+info.name+"</span><br /><div>"+info.street + ", " + info.country + ", " + info.zipCode + "</div></div>"
            });
            infowindow.open(map,marker);
          });
    }
    $scope.AddFavCause = function(res){
        var obj = {"ein": res.ein};        
        headerFactory
            .AddUserCauses(obj)
            .success(function(result) {
              Materialize.toast(res.name + " from " + res.state + "<br/> has been added to your Causes",6000);
            })
            .error(function(err) {
            });
      }       
    $scope.donate=function(){
      $state.go("main.base.payment", { 'paymentMethod': "donation",'ein': $scope.about.ein, 'charityname': $scope.about.name, 'Zip': $scope.about.zipCode, 'city': $scope.about.city},{reload:true});

    };
    

}]);

