fundraiserModule = angular.module('fundraiserModule');

fundraiserModule.controller('inviteController', ['$scope', '$location','headerFactory','$timeout',function($scope, $location,headerFactory,$timeout) {
$scope.inviteref='';
headerFactory.getUser($scope.inviteref)
            .success(function(result) {
              $scope.inviteref=result.data.inviteCode;
              //console.log('code', $scope.inviteref);
            })

   $scope.copy=function(){
    Materialize.toast("the text is copied", 4000);
   };
   $scope.invite={ 'emailAddress':'',
                   'subject':'',
                   'message':''
               };
   $scope.inviteOthers=function(valid,form){
        if (valid) {
          if(angular.element("#g-recaptcha-response").val() !== '') {
              headerFactory.inviteFriend($scope.invite)
              .success(function(result) {
              Materialize.toast("invitation successfull ", 4000);
              angular.element('form[name="frmInvite"]')[0].reset();
              $scope.invite = {};
              form.$setPristine();
              form.$setUntouched();
              })
              .error(function(err){
                  //console.log('error is',err);
              });
              $timeout(()=>{
                  grecaptcha.reset();
              },2000);
            }else {
              Materialize.toast("plese fill recaptcha ", 4000);
            }
        }
        else{ 
          Materialize.toast("Please fill the details", 4000);
        }
    }


    angular.element(".referlist li a").on('click',function(e){
      e.preventDefault();
    	var txt=angular.element('.cashbacktext').text();
    	var type=angular.element(this).attr('data-tag');
    	if(type=='facebook')
    	{
    		angular.element('#sociallink').attr('socialshare-url',txt)
    		angular.element('#sociallink').attr('socialshare-provider','facebook')
    	}
    	if(type=='twitter')
    	{
    		angular.element('#sociallink').attr('socialshare-url',txt)
    		angular.element('#sociallink').attr('socialshare-provider','twitter')
	    	angular.element('#sociallink').trigger('click');
    	}
    	if(type=='pintrest')
    	{
    		angular.element('#sociallink').attr('socialshare-url',txt)
    		angular.element('#sociallink').attr('socialshare-provider','pinterest')
    	}
    });


}]);