fundraiserModule = angular.module('fundraiserModule');

fundraiserModule.controller('feedbackController', ['$scope','headerFactory','$state','$timeout',function($scope,headerFactory,$state,$timeout) {


    $scope.fbForm={
        'name':'',
        'message':'',
        'emailAddress':'',
        'phoneNumber':'',
        'organization':''
    };
    $scope.feedback=function(valid){
        if(valid){
            
            if(angular.element("#g-recaptcha-response").val() !== '')
                {
                            headerFactory.
                            SendFeedback($scope.fbForm)
                            .success(function(result) {
                            Materialize.toast("feedback sent ", 4000);
                            angular.element('form[name="feedbackForm"]')[0].reset();
                            Materialize.updateTextFields();
                            $scope.fbForm = {};
                            $scope.feedbackForm.$setPristine();
                            $scope.feedbackForm.$setUntouched();
                            })
                            .error(function(err){
                            
                            });
                            $timeout(()=>{
                                grecaptcha.reset();
                            },2000);
                            
                }
            else{
                 Materialize.toast("plese fill recaptcha ", 4000);
            }
        //console.log(fbForm);
        }
    };
// $scope.resetForm = function(fbForm) {
//       //Even when you use form = {} it does not work
//       angular.copy({},form);
//     }
}]);