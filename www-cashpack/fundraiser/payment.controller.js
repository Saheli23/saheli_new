fundraiserModule = angular.module('fundraiserModule');
fundraiserModule.controller('paymentController', ['$scope','headerFactory', '$location','$state','$stateParams',function($scope,headerFactory, $location,$state,$stateParams) {
    $scope.donatedetails=$stateParams;
    $scope.about={
        paymentMethod:$scope.donatedetails.paymentMethod,
        paymentType:"",
        amount:0,
        ein:$scope.donatedetails.ein ? $stateParams.ein : '',
        name:"",
        address:"",
        suite:"",
        city:$scope.donatedetails.city ? $stateParams.city : '',
        state:"",
        zipCode:"",
        country:"",
        paypalAddress:""
    };
        console.log("param",$scope.about);
    $scope.count= $location.$$url.split('?')[1];
    if($scope.donatedetails.paymentMethod=='donation'){
      $scope.paymentoption='donate';
    }
    else{
      $scope.paymentoption='withdraw';
    };
    headerFactory
        .getTotal()
        .success(function(res) {
             $scope.price =res.data.processedFunds + res.data.availableFunds;
            //console.log("res:",res);
        })
        .error(function(data, status, headers, config) {
        });

    $scope.submit=function(paymentType){   
        $scope.about.paymentType = paymentType ;
        headerFactory.
          addPayment($scope.about)
          .success(function(response) {
                      
                      console.log(response);
                  })
                  .error(function(err) {
                      console.log(err);
                  });
            };

}]);