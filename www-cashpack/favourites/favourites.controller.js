var favouritesModule = angular.module('favouritesModule');
favouritesModule.controller('favouritesController', ['$scope', 'headerFactory' ,'$cookieStore','$location','$rootScope',function($scope, headerFactory ,$cookieStore,$location,$rootScope) {
	$scope.favouriteList=[];
	$scope.couponsList  =[];
	$scope.dealsList    =[];
	$scope.numLimit     = 10;
	$scope.textLimit    = 25;
	$scope.getMyFav=function(){
		headerFactory
		 .getFavourites()
			  .success(function(res){
			  
			  	$scope.favouriteList=res.data.merchants;
			  	$scope.couponsList=res.data.codes;
			  	$scope.dealsList=res.data.deals;
			 	 })
			  .error(function(err){
			  	
			  	Materialize.toast(err.data, 4000);
			 	 });

	};

	$scope.getMyFav();
	$rootScope.$on("getFavList", function(){
        $scope.getMyFav();
    });

  	$scope.removeFav=function(fid,ftype){
	  	var data =  {
	                    "id"        : fid,
	                    "type"      : ftype,
	                };
			headerFactory.deleteFavourites(data).success(function(res){
			
					$scope.getMyFav();

			 	 })
		    .error(function(err){
		    	Materialize.toast("Unable to remove favorite,try again", 4000);
		 	 });
		  	
  	};

  	document.onclick = function (event) {  
  		var curTarget  = angular.element(event.target);
        if(!curTarget.closest('.sh-list-icone').hasClass('sh-list-icone')){
            var socialUl = angular.element('.social-ul');
            if(socialUl.hasClass("display-toggle")){
                socialUl.addClass("social-display");
                socialUl.removeClass("display-toggle");
            }
        }
  	}

  	$scope.toggleShare = function(event){
  		var socialUl = angular.element(event.target).closest('.sh-list-icone').find('.social-ul');
        angular.element('.social-ul').not(socialUl).removeClass("display-toggle").addClass("social-display");
        if(socialUl.hasClass("social-display")){
            socialUl.removeClass("social-display");
            socialUl.addClass("display-toggle");
        }else{
            socialUl.addClass("social-display");
            socialUl.removeClass("display-toggle");
        }
  	}

}]);
