(function($){
	var App = {
		globals: {
			saveUrl: 'process/saveInfo.php',
			getUrl: 'process/showInfo.php'
		},
		selectors: {
			infoFrom: $('#checkoutFromInfo'),
			saveBtn: $('#saveInformation'),
			website: $('#website'),
			tableBody: $('#websiteTable tbody'),
			tableBodyTamplate: $('#tableBodyTemplate').html()
		},
		garbageVal: null,
		init: function(){
			this.bindEvents();
			$('select').material_select();
		},
		bindEvents: function(){
			App.selectors.saveBtn.click(App.saveInformation);
			App.selectors.website.donetyping(function(t){
				App.garbageVal = t.val();
				App.getInformationFromWebsiteName(App.garbageVal);
			});
		},
		saveInformation: function(){
			App.selectors.saveBtn.prop('disabled', true);
			App.addProgress();

			$.ajax({
				url: App.globals.saveUrl,
				type: 'POST',
				dataType: 'json',
				data: App.selectors.infoFrom.serialize(),
			})
			.done(function(success) {
				var s = success;
				if(s.status === 'success'){
					Materialize.toast('Success! All data saved.', 4000);
					App.getInformationFromWebsiteName(App.garbageVal);
				}
				if(s.status === 'error'){
					App.showErrorDetails(s);
				}
				App.removeProgress();
				App.selectors.saveBtn.prop('disabled', false);
				App.selectors.infoFrom[0].reset();
			})
			.fail(function(error) {
				App.showErrorDetails(error);
				App.removeProgress();
				App.selectors.saveBtn.prop('disabled', false);
				App.selectors.infoFrom[0].reset();
			});			
		},
		getInformationFromWebsiteName: function(wb){
			App.addProgress();
			$.getJSON(App.globals.getUrl, { "website": wb }, function(json, textStatus) {
					var tableBodyTemplateScript = App.selectors.tableBodyTamplate,
						tableBodyTemplate = Handlebars.compile(tableBodyTemplateScript),
						tableBodyCompiledHTML = tableBodyTemplate(json);
					
					App.selectors.tableBody.empty();
					App.selectors.tableBody.append(tableBodyCompiledHTML);
					App.removeProgress();
			});
		},
		addProgress: function(){
			if(!App.selectors.infoFrom.find('.progress').length){
				App.selectors.infoFrom.append('<div class="progress"><div class="indeterminate"></div></div>');
			}
		},
		removeProgress: function(){
			if(App.selectors.infoFrom.find('.progress').length){
				App.selectors.infoFrom.find('.progress').remove();
			}
		},
		showErrorDetails: function(info){
			Materialize.toast('Error! ' + info.message + '<br> Please check your console for more info.', 4000);
			console.error("A error occurred: ", info);
		}
	};

	$(function() {
		App.init();

		Handlebars.registerHelper("inc", function(value, options){
		    return parseInt(value) + 1;
		});

		Handlebars.registerHelper("ifCond", function(value, options){
			return (value !== 'error') ? options.fn(this) : options.inverse(this);
		});
	});

	$.fn.donetyping = function(callback, delay) {
	    delay || (delay = 800);
	    var timeoutReference;
	    var doneTyping = function(elt) {
	        if (!timeoutReference) return;
	        timeoutReference = null;
	        callback(elt);
	    };

	    this.each(function() {
	        var self = $(this);
	        self.on('keyup', function() {
	            if (timeoutReference) clearTimeout(timeoutReference);
	            timeoutReference = setTimeout(function() {
	                doneTyping(self);
	            }, delay);
	        }).on('blur', function() {
	            doneTyping(self);
	        });
	    });

	    return this;
	};
})(jQuery);