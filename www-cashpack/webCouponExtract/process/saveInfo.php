<?php
	require '../config.php';
	require '../db.Class/MysqliDb.php';

	$db = new MysqliDb ($config['host'], $config['username'], $config['password'], $config['database']);

	if($_SERVER['REQUEST_METHOD'] === 'POST'){
		$p = $_POST;

		if(!empty($p['input_fields']) && !empty($p['apply_button']) && !empty($p['website']) && !empty($p['checkout_url']) && !empty($p['price_field']) && !empty($p['submit_type'])){

			$db->where('website', $p['website']);
			$website = $db->getOne('applycoupon');

			if($db->count > 0){
				echo json_encode(array('status' => 'error', 'message' => 'This website already present in our DB'));
				exit();
			}else{
				$data = Array ("input_fields" => $p['input_fields'],
				               "apply_button" => $p['apply_button'],
				               "change_button" => $p['change_button'],
				               "form_field" => $p['form_field'],
				               "website" => $p['website'],
				               "checkout_url" => $p['checkout_url'],
				               "price_field" => $p['price_field'],
				               "error_field" => $p['error_field'],
				               "submit_type" => $p['submit_type']
				);

				$id = $db->insert ('applycoupon', $data);

				if($id){
				    echo json_encode(array('status' => 'success', 'message' => 'All your information are saved.'));
				    exit();
				}else{
					echo json_encode(array('status' => 'error', 'message' => $db->getLastError()));
					exit();
				}
			}
		}else{
			echo json_encode(array('status' => 'error', 'message' => 'Please fill the required fields.'));
			exit();
		}
	}else{
		echo json_encode(array('status' => 'error', 'message' => 'Error in response.'));
		exit();
	}
?>