<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

require '../config.php';
require '../db.Class/MysqliDb.php';

$db = new MysqliDb ($config['host'], $config['username'], $config['password'], $config['database']);

if($_SERVER['REQUEST_METHOD'] === 'GET'){
	$p = $_GET;

	if(!empty($p['website'])){
		$db->where('website', $p['website']);
		$website = $db->getOne('applycoupon');

		if($db->count > 0){
			echo json_encode(array('status' => 'success', 'message' => 'Please check the website info', 'attributes' => $website));
		}else{
			echo json_encode(array('status' => 'error', 'message' => 'No website info found for this domain.'));
			exit();
		}
	}else{
		echo json_encode(array('status' => 'error', 'message' => 'Please give a website name.'));
		exit();
	}
}else{
	echo json_encode(array('status' => 'error', 'message' => 'Error in response.'));
	exit();
}
?>