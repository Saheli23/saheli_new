-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2016 at 03:36 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cashpack`
--

-- --------------------------------------------------------

--
-- Table structure for table `applycoupon`
--

CREATE TABLE IF NOT EXISTS `applycoupon` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `input_fields` varchar(254) NOT NULL,
  `apply_button` varchar(254) NOT NULL,
  `change_button` varchar(254) NOT NULL,
  `form_field` varchar(254) NOT NULL,
  `website` varchar(300) NOT NULL,
  `checkout_url` varchar(300) NOT NULL,
  `price_field` varchar(254) NOT NULL,
  `submit_type` enum('Ajax','Reload') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `applycoupon`
--

INSERT INTO `applycoupon` (`id`, `input_fields`, `apply_button`, `change_button`, `form_field`, `website`, `checkout_url`, `price_field`, `submit_type`) VALUES
(1, '.voucherInput', '.btn.btn-inverse.btn--inline', '', '', '1likenoother.co.uk', 'http://www.1likenoother.co.uk/basket', '.price-totals__grand-total.font-calson.text-uppercase .text-emphasis.pull-right', 'Reload'),
(2, '#coupon_full', 'form[name=couponForm_full] button[type=submit]', '', 'form[name=couponForm_full]', '1800petmeds.com', 'http://www.1800petmeds.com/cart.jsp', '.checkout .price', 'Reload'),
(3, '#coupon_code', '#btn-apply-coupon', '', '#discount-coupon-form', '123inkjets.com', 'https://www.123inkjets.com/checkout/cart/', '.grand-total .price', 'Reload'),
(4, '#couponcode', '.CouponCode input[type=submit]', '', '.CouponCode form', '101inks.com', 'https://www.101inks.com/cart.php', '.SubTotal.gtotal.Last .ProductPrice', 'Ajax'),
(5, '#promo_code', '#promo-form button[type=submit]', '', '#promo-form', '1800lighting.com', 'http://www.1800lighting.com/scripts/cart.cfm', '#totalamt', 'Ajax'),
(6, '#voucher_input', '#voucher_code_submit', '', '', '365tickets.com', 'https://www.365tickets.com/basket', '#basket_summary tr:last td:last strong', 'Ajax'),
(7, '#AXISVoucherField', '.applyButton', '', '', '365ink.co.uk', 'https://www.365ink.co.uk/checkoutBasket.asp', '#basketOrderTotal span', 'Ajax'),
(8, '#voucher_input', '#voucher_code_submit', '', '', '365tickets.co.uk', 'https://www.365tickets.co.uk/basket', '#basket_summary tr:last td:last strong', 'Ajax'),
(9, 'input#c_code', 'input#submit_coupon', '', '', '39dollarglasses.com', 'https://www.39dollarglasses.com/ord/basket.html', 'span#cart_total>b', 'Reload'),
(10, 'input#ctl00_ContentPlaceHolder1_tbCouponCode', 'input#ctl00_ContentPlaceHolder1_btnCouponCode', 'a#ctl00_ContentPlaceHolder1_lbChange', '', '4wd.com', 'https://www.4wd.com/shoppingcart.aspx', 'span#ctl00_ContentPlaceHolder1_litTotal', 'Reload'),
(11, 'input#coupon_code', 'button#btn-apply-coupon', 'form#discount-coupon-cancel-form-1>span', '', '4inkjets.com', 'https://www.4inkjets.com/checkout/cart/', 'tr.grand-total>td:nth-child(2)>strong>span', 'Ajax'),
(12, 'input#coupon_code', 'form#discount-coupon-form>div>div>button', '', 'form#discount-coupon-form', '5pointz.co.uk', 'https://www.5pointz.co.uk/checkout/cart/', 'table#shopping-cart-totals-table>tfoot>tr>td:nth-child(2)>span', 'Reload'),
(13, 'input.a-span8', 'button#button-add-gcpromo-announce', '', '', '6pm.com', 'https://secure-www.6pm.com/checkout/pay', 'table.a-normal>tbody>tr:nth-child(7)>td:nth-child(2)', 'Ajax'),
(14, 'input#ctl00_ContentPlaceHolder1_txtCouponCode', 'a#ctl00_ContentPlaceHolder1_btnEnterCoupon', '', '', '999inks.co.uk', 'http://www.999inks.co.uk/ViewShoppingCart.aspx', 'span#ctl00_ContentPlaceHolder1_lblTotal', 'Ajax'),
(15, 'input#coupon_code', 'form#discount-coupon-form>fieldset>button', '', 'form#discount-coupon-form', 'accuquilt.com', 'http://www.accuquilt.com/shop/checkout/cart/', 'table#shopping-cart-totals-table>tfoot>tr:nth-child(4)>td:nth-child(2)>strong>span', 'Reload'),
(16, 'input#PromoCode', 'input.purpleButton', '', '', 'activitysuperstore.com', 'https://www.activitysuperstore.com/Basket', 'td.total', 'Reload'),
(17, 'input#dwfrm_cart_couponCode', 'button#add-coupon', '', '', 'adidasgolf.com', 'https://adidasgolf.com/cart', 'div.order-total>span:nth-child(2)', 'Reload'),
(18, 'input.ct-active', 'div.fieldwithbutton>input', '', '', 'adinionline.co.uk', 'http://www.adinionline.co.uk/basket/', 'div.container-2x1-marginless>div:nth-child(2)', 'Reload'),
(19, 'input#promoCode', 'div.spR>div>div:nth-child(2)>a', '', '', 'advanceautoparts.com', 'http://shop.advanceautoparts.com/web/OrderItemDisplay', 'div#shipping_total_wrapper_c2', 'Ajax'),
(20, 'input#gv_redeem_code', 'button#gv_button_redeem>span', 'button#gv_button_remove>span', '', 'advancedmp3players.co.uk', 'http://www.advancedmp3players.co.uk/shop/shopping_cart.php', 'span#cart_ot_total>b', 'Ajax'),
(21, 'input#promoBx', 'td.promoApplyCell>a', '', '', 'aeropostale.com', 'http://www.aeropostale.com/cart/index.jsp', 'table#taxShippingSummary>tbody>tr:nth-child(7)>td:nth-child(2)>b', 'Reload'),
(22, 'div#enter-promo>input:nth-child(3)', 'input.redeem', '', '', 'affordablesupplements.co.uk', 'http://www.affordablesupplements.co.uk/checkout/cart/', 'td.cart-total-value>span', 'Reload');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
