var allCuponsModule = angular.module('allCuponsModule', ['ui.router']);

allCuponsModule.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('main.base.allCupons', {
        url: 'allcoupons?searchTerm',
        cache: false,
        data: {
              pageTitle: 'Cashpack-Allcoupons'
            },
        views: {
            'content': {
                templateUrl: 'allcupons/allcupons_content.html',
                controller: 'allCuponsController',
                'controllerAs': 'cuponsCtrl'

            }
        },
        params: {
                searchTerm: ''
            }, 
        ncyBreadcrumb: {
                  label: 'allcupons page'
              }
    })
});