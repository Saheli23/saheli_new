var allCuponsModule = angular.module('allCuponsModule');
allCuponsModule.controller('allCuponsController', ['$scope','$state','headerFactory','$stateParams',function($scope,$state,headerFactory,$stateParams) {
    
var self=this;
    
    
    $scope.allList  = [];
    if($stateParams.searchTerm)
    {
        headerFactory
        .searchCouponList({searchTerm:$stateParams.searchTerm})
         .success(function(response){
        $scope.allList = response.data;
        console.log(response.data);

     })
        .error(function(error){
       Materialize.toast(error.data, 4000);
     })
    }
    else
    {
       



         $scope.findNextOffer=function(page)
           {
                var obj={"records":24,"page":page,source:'website'};
                self.loadingMsg='Loading...';

                headerFactory
                 .getCouponAllList(obj)
                    .success(function(result) {
                        if (result && result.meta.error == false) {
                            $scope.allList = result.data.records;
                            if(page==1)
                            {
                                $scope.totalPages=result.data.totalPages;
                                $scope.totalRecords=result.data.totalRecords;
                                $scope.recordsPerPage=result.data.recordsPerPage;
                                //console.log($scope.totalPages);
                            }                           
                            self.loadingMsg='';
                        }
                    })
                    .error(function(error){
                        console.log(error);
                            self.loadingMsg='';

                    });
           };

           $scope.findNextOffer(1);


     
    }


    $scope.search = function(valid){
        if (valid) {
            $state.go("main.base.allMarchent", { searchTerm: $scope.SearchText });
        }
    };

}]);