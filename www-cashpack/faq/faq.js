var faqModule = angular.module('faqModule', ['ui.router']);

//to return innerHTML of element
faqModule.directive('updateContent', ['$rootScope', '$compile',
  function($rootScope,$compile) {
    return {
      restrict: 'EA',
      scope : {
        content : '@'
      },
      link: function(scope, element) {

        $(element).html(scope.content);
        $compile(element.contents())(scope);
        // $rootScope.$apply();
      }
    };
  }
]);

faqModule.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('main.base.faq', {
            url: 'faq',
            cache: false,
            data: {
              pageTitle: 'Cashpack-FAQ'
            },
            views: {
                'content': {
                    templateUrl: 'faq/faq.html',
                    controller: 'faqController'
                }
            }
        });
});
