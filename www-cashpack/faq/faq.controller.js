faqModule = angular.module('faqModule');
faqModule.controller('faqController', ['$scope','$document','headerFactory','$timeout','$state','$cookieStore',function($scope,$document,headerFactory,$timeout,$state,$cookieStore) {
    $scope.className = "";
    $scope.catName="";
    $scope.faqFm ={
        'name':'',
        'message':'',
        'emailAddress':'',
        'phoneNumber':'',
        'organization':''
    };
    $scope.isLoggedIn = function() {
        return ($cookieStore.get('userToken') && $cookieStore.get('userId')) ? true : false

    };
        $scope.showOrHide = function(form) {
            $scope.className = $scope.className ? "" : "show";
            $scope.catName=$scope.catName ? "" : "catfaq";
            if($scope.className === "show"){
                grecaptcha.reset();
                angular.element('form[name="faqFrm"]')[0].reset();
                form.$setPristine();
                form.$setUntouched();
            }
    };
    $scope.closeModal = function(){
        angular.element('.coupon-overlay').css({
            "display" : "none"
        });
    };
    $scope.close=function(){
         $scope.className= "";
         $scope.catName="";
    };

        $scope.inst = function() {
        chrome.webstore.install('https://chrome.google.com/webstore/detail/hcggnikhmdneeojjdcnbojmpjbglobme',
            function(e) {
                //console.log(e);
            },
            function(er) {
                //console.log(er);
            });
    };

        $document.on('click', function(a, b) {

           var isshow=angular.element('.category-select-mega').is(":visible");
            if(isshow)
            {
                angular.element('.category-search').trigger('click');
            }
        });
        //  angular.element('#frgtPwd').click(function(event){
        //    if ($cookieStore.get('userToken') && $cookieStore.get('userId')) {
        //     $scope.closeModal();
        //       angular.element('.lean-overlay').css({
        //         "display" : "none !important"
        //     });
        // }

        // });

       angular.element('.category-search').click(function(event){
             event.stopPropagation();
        });
       angular.element('.category-select-mega').click(function(event){
             event.stopPropagation();
        });
        headerFactory.
        getFaqInfo($scope.infores)
        .success(function(result) {
            $scope.infores=result;
            // console.log($scope.infores);
        })
        .error(function(err) {
            //console.log(err);
        })
        
 $scope.faqDropALine=function(valid,form){
        if(valid){if(angular.element("#g-recaptcha-response").val() !== '')
        {
          //console.log($scope.faqFm);
          headerFactory.
            SendFeedback($scope.faqFm)
            .success(function(result) {
            Materialize.toast("Your Suggestion sent ", 4000);
            angular.element('form[name="faqFrm"]')[0].reset();
            $scope.faqFm = {};
            form.$setPristine();
            form.$setUntouched();
            Materialize.updateTextFields();
            })
            .error(function(err){
                            
            });
            $timeout(()=>{
                grecaptcha.reset();
            },2000);
          }
      else
      {
        Materialize.toast("plese fill recaptcha ", 4000);
      }
        }
        else{
            Materialize.toast("Please fill the details", 4000);
        }
    };

    $scope.autoheight=function()
    {
            if($scope.faqFrm.$submitted && $scope.faqFrm.$invalid)
              return '715px';
            else if($scope.faqFrm.$dirty)
              return '715px';
            else
            return '615px';
    }

    $scope.openAnother=function(id)
    {
      angular.element("#ques"+id).trigger('click');
      angular.element('html,body').animate({
                scrollTop:  angular.element("#ques"+id).offset().top-120
            }, 700);
    }

}]);
