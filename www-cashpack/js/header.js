(function($){
    $(function(){
        $('.button-collapse').sideNav();
        $('.parallax').parallax();
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15// Creates a dropdown of 15 years to control year
        });
        $('ul.tabs').tabs();       

    }); // end of document ready
})(jQuery); // end of jQuery name space





$(document).ready(function(){
    $(".profile-img-link").click(function(){
        $(".drop-down").slideToggle();
    });
    $(".cross-section span").click(function(){
        $(".drop-down").hide();
    });
    $(".search").click(function(){
        $(".search-section").slideToggle();
    });
     
});