var allMarchentModule = angular.module('allMarchentModule', ['ui.router']);

allMarchentModule.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

    $stateProvider
    .state('main.base.allMarchent', {
        url: 'allstores',
        cache: false,
        data: {
              pageTitle: 'Cashpack-Allstores'
            },
        views: {
            'content': {
                templateUrl: 'allmarchent/allmarchent_content.html',
                controller: 'allMarchentController',
                "controllerAs":'merCtrl'
            }
        },
        params: {
                searchTerm: null
            }, 
        ncyBreadcrumb: {
                  label: 'allmarchent page'
              }
    })
        .state('main.base.Featuredcoupons', {
        url: 'Featuredcoupons',
        cache: false,
        data: {
              pageTitle: 'Cashpack-Featuredcoupons'
            },
        views: {
            'content': {
                templateUrl: 'allmarchent/Featuredcoupons.html',
                controller: 'FeaturedcouponsController',
            }
        },
        ncyBreadcrumb: {
                  label: 'allmarchent page'
              }
    })

});