var allMarchentModule = angular.module('allMarchentModule');
allMarchentModule.controller('allMarchentController', ['$scope', '$state', 'headerFactory', '$stateParams', '$document',function($scope, $state, headerFactory, $stateParams, $document) {
    var self = this;

    $scope.SearchText = $stateParams.searchTerm;
    $scope.paramText = $scope.SearchText;
    $scope.catcr=function(e)
    {
        
        $scope.checkedElements = angular.element('input[type="checkbox"]:checked');
        $scope.checkedElementsValues = [].slice.call($scope.checkedElements).map(function (e) 
            {return e.id;});
    headerFactory
     .searchList({categories:$scope.checkedElementsValues, source:'website'})
        .success(function(response) {
                $scope.allList = response.data;
                $scope.SearchText='';
               //console.log(response.data);
                 if($scope.allList.length==0){
                    $scope.cat='categories are not available';
                 }
                self.loadingMsg = '';
                $scope.close();
            })
        .error(function(error) {
                self.loadingMsg = '';
                Materialize.toast(error.data, 4000);
            })
    };
    $scope.allList = [];
    $scope.totalPages = 0;
    $scope.totalRecords = 0;
    $scope.recordsPerPage = 0;
    var charObj = {};
    $scope.char = '';
    $scope.SearchKeyword='';

    self.loadingMsg = '';
    $scope.searchonkey=function(){
         $scope.SearchKeyword=angular.element('#SearchText').val();
        if($scope.SearchKeyword.length>1){
        headerFactory
            .searchList({ searchTerm: $scope.SearchText ,source:'website'})
            .success(function(response) {
                $scope.allList = response.data;
                //console.log(response.data);
                self.loadingMsg = '';

            })
            .error(function(error) {
                self.loadingMsg = '';
                Materialize.toast(error.data, 4000);
            })
        }
    };
    if ($stateParams.searchTerm) {
        self.loadingMsg = 'Loading...';
        //$scope.catcr();
        headerFactory
            .searchList({ searchTerm: $stateParams.searchTerm ,source:'website'})
            .success(function(response) {
                $scope.allList = response.data;
                //console.log(response.data);
                self.loadingMsg = '';

            })
            .error(function(error) {
                self.loadingMsg = '';
                Materialize.toast(error.data, 4000);
            })
    } else {

        $scope.findNextData = function(page) {
            var obj = { "records": 24, "page": page ,source: 'website'};

            self.loadingMsg = 'Loading...';

            headerFactory
                .topMerchantList(obj)
                .success(function(result) {
                    if (result && result.meta.error == false) {
                        $scope.allList = result.data.records;
                        if (page == 1) {
                            $scope.totalPages = result.data.totalPages;
                            $scope.totalRecords = result.data.totalRecords;
                            $scope.recordsPerPage = result.data.recordsPerPage;
                        }

                        self.loadingMsg = '';
                    }
                })
                .error(function(error) {
                    self.loadingMsg = '';

                });
        };

        $scope.findNextData(1);


    }


    $scope.search = function(valid) {
        if (valid) {
            $state.go("main.base.allMarchent", { searchTerm: $scope.SearchText });
        }
    };
    $scope.className = "";
    $scope.catName="";
    $scope.showOrHide = function() {
        $scope.className = $scope.className ? "" : "show";
        $scope.catName=$scope.catName ? "" : "catback";
    };
    $scope.charterSearch = function(reqTxt) {
        $state.go("main.base.allMarchent", { searchTerm: reqTxt });
    };
    $scope.close=function(){
         $scope.className= "";
         $scope.catName="";
    };
    $scope.clean=function(){
        $('div input[type="checkbox"]').prop('checked',false);
    };


    $scope.findNextDataAlphabet = function(page) {
        charObj = { "records": 24, "page": page, "startingLetter": $scope.char ,source: 'website'};
        //console.log($scope.char);
        self.loadingMsg = 'Loading...';
        headerFactory
            .merchantAlphabetSearch(charObj)
            .success(function(result) {
               //console.log(result);
                if (result && result.meta.error == false) {
                    $scope.allList = result.data.records;
                    if (page == 1) {
                        $scope.totalPages = result.data.totalPages;
                        $scope.totalRecords = result.data.totalRecords;
                        $scope.recordsPerPage = result.data.recordsPerPage;
                    }

                    self.loadingMsg = '';
                }
            })
            .error(function(error) {
                self.loadingMsg = '';

            });
    };

    $scope.searchByAlphabet = function(txt) {
        $scope.char = txt;
        $scope.findNextDataAlphabet(1);
    };

    // $scope.dynamicPopover = {
    //     content: 'Hello, World!',
    //     templateUrl: 'myPopoverTemplate.html',
    //     title: 'Title'
    // };

    // angular.element(document).ready(function() {
    //     $("#vertical-scrollbar-demo").customScrollbar();
    // });


    // headerFactory
    //     .getMarchentCategory()
    //     .success(function(response) {
    //         console.log(response);

    //     });

    $(".catdata").on("click",function(){
        var current = $(this),
            parent = current.parent();
        if (parent.next().length > 0) {
            var list = parent.next(),
                childlist = list.find('li div > .catdata');
            if (current.is(":checked")) {
                childlist.prop('checked', true);
            } else {
                childlist.prop('checked',false);
            }
        }else{
            var list = parent.closest('ul'),
                childlist = list.find('li div > .catdata'),
                checkedChildlist = list.find('li div > .catdata:checked');
            if(checkedChildlist.length == childlist.length){
                current.closest('ul').prev().find('.catdata').prop('checked', true);
            }else{
                current.closest('ul').prev().find('.catdata').prop('checked', false);
            }
        }
    });

    $document.on('click', function(a, b) {

        var isshow = angular.element('.category-select-mega').is(":visible");
        if (isshow) {
            angular.element('.category-search').trigger('click');

        }
        //$('div input[type="checkbox"]').prop('checked',false);
    });

    angular.element('.category-search').click(function(event) {
        event.stopPropagation();
    });
    angular.element('.category-select-mega').click(function(event) {
        event.stopPropagation();
    });

}]);
allMarchentModule.controller('FeaturedcouponsController', ['$scope','headerFactory',function($scope,headerFactory){
    
    $scope.getstoreAllList=[];
    headerFactory
        . getstoreAllList()
        .success(function(result) {
            if (result && result.meta.error === false) {
                $scope.getstoreAllList = result.data;
            }
        })
        .error(function(err) {
            Materialize.toast(err.data, 4000);
        });

}]);
