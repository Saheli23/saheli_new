var homeModule = angular.module('homeModule', ['ui.router']);

homeModule.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('main.base.home', {
            url: 'home',
            cache: false,
            data: {
              pageTitle: 'Cashpack Home'
            },
            views: {
                'content': {
                    
                    templateUrl: 'landing/landing_temp.html',
                    controller: 'homeController'
                }
            },
            ncyBreadcrumb: {
                label: 'Home page'
            },
             resolve: {
             data: function($q, $state,$timeout,$cookieStore) {
                    var deferred = $q.defer();
                     $timeout(function() {
                      if (angular.isUndefined($cookieStore.get('userId'))) {
                          $state.go('main.base.landing');
                          Materialize.toast("Please login or signup first", 4000);

                          deferred.reject();
                      } else {
                        deferred.resolve();
                       }
                    });

                    return deferred.promise;
                  }
    }
});
    
});
