var homeModule = angular.module('homeModule');
homeModule.controller('homeController',['$scope', 'headerFactory', '$cookieStore', '$state','$stateParams', '$localStorage', function($scope, headerFactory, $cookieStore, $state, $stateParams, $localStorage) {
    
    $scope.showOrHide = false;
    $scope.numLimit=20;
    
    // (function(){
    //     if(location.hash.includes('showCode')){
    //         var $index = location.hash.split("?")[1].split("=")[1];
    //         $scope.coupon = $localStorage.listObj;
    //         // console.log("test:",$scope.coupon)
    //         angular.element(".modal-copyCode").css({
    //             "display" : "block",
    //             "z-index" : 1000
    //         });
    //         angular.element(".coupon-overlay").css({
    //             "display" : "block"
    //         });
    //     }
    // })();

    $scope.copyTxt = "Click to copy";
    $scope.allList = [];
    $scope.offerList = [];
    $scope.toptrendingList = [];
    $scope.getFavourites = {};
   
    $scope.isToggle=false;
    $scope.isLoggedIn = function() {
        return ($cookieStore.get('userToken') && $cookieStore.get('userId')) ? true : false

    }
    // $scope.showSubscribe = function(param){
    //     if(param === "emailSubscribe"){
    //         $scope.emailSubscribe = !$scope.emailSubscribe;
    //         $scope.phoneSubscribe = "";
    //     }else{
    //         $scope.phoneSubscribe = !$scope.phoneSubscribe;
    //         $scope.emailSubscribe = "";
    //     }
    // };
    // $scope.copyCode = function(e){
    //     var copyTarget = angular.element(e.target);
    //     angular.element(copyTarget).text('Copied');
    //     setTimeout(function() {
    //         angular.element(copyTarget).text('Copy');
    //     }, 2000);
    // }
    
    // $scope.closeModal = function(){
    //     angular.element('.modal-copyCode,.coupon-overlay').css({
    //         "display" : "none"
    //     });
    // };

    $scope.loggedIn = $scope.isLoggedIn();

    headerFactory.gettoptrending({source:'website'})
        .success(function(result) {
            $scope.toptrendingList = result.data;
           
        });


    headerFactory.getLatestofferList({source:'website'})
        .success(function(result) {
            
            if (result && result.data.length > 0) {
                $scope.offerList = result.data;
            }
        }).
    error(function(errors) {
        Materialize.toast(errors.data, 4000);
        //console.log("errors:", errors);
    });

   //Featured Coupons

    headerFactory.getAllList({source:'website'})
        .success(function(result) {

            //console.log("hii");
            if (result && result.meta.error == false) {

                $scope.allList = result.data;
            }
        });
    headerFactory
        . getstoreAllList({source:'website'})
        .success(function(result) {
            if (result && result.meta.error === false) {
                $scope.getstoreAllList = result.data;
            }
        })
        .error(function(err) {
            Materialize.toast(err.data, 4000);
            //console.log("err:", err);
        });
    
    $scope.showHide = function(){
        $scope.showOrHide = $scope.showOrHide == false ? true : false;
    };

   //Top Merchant



    $scope.obj1 = {};   
    $scope.obj2 = {};
    $scope.obj3 = {};

    headerFactory
        .getLatestofferList({source:'website'})
        .success(function(result) {
            //console.log("new",result);
            $scope.getLatestofferList = result.data;
        })
        .error(function(err) {
            Materialize.toast(err.data, 4000);
            //console.log("err:", err);
        });
    // Top Merchant for specific merchants
    headerFactory.getMarchentDetails('macys.com',{source:'website', country : 'US'})
        .success(function(result) {
            // console.log("result",result);
            var total = (result.data[0].deals).length + (result.data[0].codes).length;

            $scope.obj1 = { "name": result.data[0].name, "cashbackValue": result.data[0].cashbackValue, "cashbackType": result.data[0].cashbackType, "sum": total, "domain": result.data[0].domain,"image":'top-merchants1.png',"id":result.data[0].id,"favorite":result.data[0].favorite,"favId":result.data[0].favoriteId };
            
        });

    headerFactory.getMarchentDetails('barnesandnoble.com',{source:'website', country : 'US'})
        .success(function(result) {
            // console.log("result1",result);
                var total = (result.data[0].deals).length + (result.data[0].codes).length;

            $scope.obj2 = { "name": result.data[0].name, "cashbackValue": result.data[0].cashbackValue, "cashbackType": result.data[0].cashbackType, "sum": total, "domain": result.data[0].domain,"image":'top-merchants2.png',"id":result.data[0].id,"favorite":result.data[0].favorite,"favId":result.data[0].favoriteId };


            

        });

    headerFactory.getMarchentDetails('walmart.com',{source:'website', country : 'US'})
        .success(function(result) {
            // console.log("result2",result);
              var total = (result.data[0].deals).length + (result.data[0].codes).length;

            $scope.obj3 = { "name": result.data[0].name, "cashbackValue": result.data[0].cashbackValue, "cashbackType": result.data[0].cashbackType, "sum": total, "domain": result.data[0].domain,"image":'top-merchants3.png',"id":result.data[0].id,"favorite":result.data[0].favorite,"favId":result.data[0].favoriteId };

            
        });


    var obj={"records":3};


        $scope.myVar = false;


    $scope.toggle = function() {
        $scope.myVar = !$scope.myVar;
        $scope.isToggle=($scope.isToggle)? false:true;
    };
  
}]);
