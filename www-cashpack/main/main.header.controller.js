var mainModule = angular.module('mainModule');


mainModule.controller('searchController', ['$scope', '$state', '$rootScope', '$document', '$window','$timeout', '$localStorage', function($scope, $state, $rootScope, $document, $window,$timeout, $localStorage) {
// seperate controller for header search
    $scope.hdrsearch = function(valid) {
        if (valid) {
            $state.go("main.base.allMarchent", {
                searchTerm: $scope.scrTxt
            });
            $timeout(function(){
                $state.reload();
            },2000);
            $scope.$parent.ShowOrHide();
            $scope.scrTxt = "";
        }
    };
// on click outside of the header search 
    $document.on('click', function() {
    
        var isShow = angular.element('.search-bg-section').is(":visible");
        if (isShow == true) {
            angular.element('.search-bg-section').removeClass('show');
            angular.element('.search-bg-section').addClass('hide');
        }


    });

    angular.element('.topsearch').click(function(event) {
        event.stopPropagation();
    });
    angular.element('.search-bg-section').click(function(event) {
        event.stopPropagation();
    });


}]);


mainModule.controller('headerController', ['$scope', '$timeout', '$state', '$cookieStore', 'headerFactory', '$sessionStorage', '$timeout', '$rootScope', '$window', '$location', '$http', '$document', '$stateParams','$localStorage', function($scope, $timeout, $state, $cookieStore, headerFactory, $sessionStorage, $timeout, $rootScope, $window, $location, $http, $document, $stateParams,$localStorage) {
    $scope.strLimit = 40;
    $document.on('click', function(e) {
        
        if(!$(e.target).parent().parent().hasClass('imgmobclick') ){
            var isshow = angular.element('#drop-down-mob-header').css("display");
            if (isshow == "block") {
                angular.element('.imgmobclick').trigger('click');
            }
        }

        if($(e.target).hasClass('coupon-overlay')){
            
            angular.element('.modal-copyCode,.coupon-overlay').css({
                "display" : "none"
            });
            location.href=$location.$$absUrl.split("?")[0];
        }
    });


    (function(){
        if(location.hash.includes('showCode') && $localStorage.listObj){
            var $index = location.hash.split("?")[1].split("=")[1];
            $scope.coupon = $localStorage.listObj;
            angular.element(".modal-copyCode,.coupon-overlay").css({
                "display" : "block"
            });
        }

        if(location.search.includes('?privacy')){
            location.search = '';
            if(location.origin.includes('http://www.cashpack.org')){
                location.href = 'http://www.cashpack.org/#/privacy';
            }else{
                location.href = 'http://localhost/www-cashpack/#/privacy';
            }
        }
    })();
    

    $scope.copyCode = function(e){
        var copyTarget = angular.element(e.target).parent().find('button');
        var spanElem   = copyTarget.parent().find('span')[0],
            spanWdth   = spanElem.offsetWidth,
            spanHght   = spanElem.offsetHeight;
        copyTarget.css({
            "width"  : spanWdth,
            "height" : spanHght
        });
        copyTarget.css("visibility","visible");
        angular.element(copyTarget).text('Copied');
        setTimeout(function() {
            copyTarget.css("visibility","hidden");
            // angular.element(copyTarget).text('Copy');
        }, 2000);
    }
    
    $scope.closeModal = function(){
       
        
        angular.element('.modal-copyCode,.coupon-overlay').css({
            "display" : "none"
        });
        location.href=$location.$$absUrl.split("?")[0];
        // $location.go($location.$$path);
    };

    $scope.showSubscribe = function(param){
        if(param === "emailSubscribe"){
            $scope.emailSubscribe = !$scope.emailSubscribe;
            angular.element('.coupon-email').addClass('activeForm');
            angular.element('.coupon-text').removeClass('activeForm');
            $scope.phoneSubscribe = "";
        }else{
            $scope.phoneSubscribe = !$scope.phoneSubscribe;
            angular.element('.coupon-text').addClass('activeForm');
            angular.element('.coupon-email').removeClass('activeForm');
            $scope.emailSubscribe = "";
        }
    };
    
    // Event trigger on click of the Show more button.
    $scope.showMore = function() {
        $scope.strLimit = $scope.coupon.offer.length;
    };

    // Event trigger on click on the Show less button.
    $scope.showLess = function() {
        $scope.strLimit = 50;
    };

    $scope.inst = function() {
        chrome.webstore.install('https://chrome.google.com/webstore/detail/hcggnikhmdneeojjdcnbojmpjbglobme',
            function(e) {
                //console.log(e);
            },
            function(er) {
                //console.log(er);
            });
    };
        function convertBase64FromURL(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            var reader = new FileReader();
            reader.onloadend = function() {
                callback(reader.result);
            }
            reader.readAsDataURL(xhr.response);
        };
        xhr.open('GET', url);
        xhr.responseType = 'blob';
        xhr.send();
    }
    $rootScope.$on('$locationChangeSuccess', function(event, current, previous) {
        if ($scope.dropdownContent == 'show') {
            $scope.dropdownContent = 'hide';
        }
    });
    $scope.getdata = function(param) {
        var params = window.location.search.substr(1).split('&');
        for (var i = 0; i < params.length; i++) {
            var p = params[i].split('=');
            if (p[0] == param) {
                return decodeURIComponent(p[1]);
            }
        }
        return false;
    };
    
    $scope.resetFrminput={"hash":$location.$$url.substring(8),"password2":''};
    $rootScope.$on('$locationChangeSuccess', function(event, current, previous) {
        var hashIndex = previous.indexOf('forget?'),
         signIndex = previous.indexOf('signup'),
          logIndex = previous.indexOf('login');

        var oldRoute = previous.substr(hashIndex + 7);
        $scope.foundstate = 0;
        angular.forEach($state.get(), function(data) {
            var pathdata = data.url;
            if (oldRoute.indexOf(pathdata) !== -1) {
                $scope.foundstate = 1;
            }
        });
        $timeout(function() {
            if (hashIndex !== -1) {
                angular.element('#reset-password-pop').openModal();                  
            }
            
        }, 1000);



    });
    $scope.hashword=$location.$$url.substring(8);
    $scope.name = "login Please";
    $scope.dropdownContent = "hide";
    $scope.scrTxt = "";
    $scope.loginError = '';
    $scope.signinError = '';
    $scope.registerForm = {
        emailAddress: "",
        password    : "",
        inviteCode  :$scope.hashword
    };
    $scope.confirmForm = {
        confirmpassword: ""
    };
    $scope.loginForm = {
        emailAddress: "",
        password: "",
        logcheck: ""
    };
    $scope.wrongForm = {
        emailAddress: "",
    };
    $scope.updateUser = {
        emailAddress: "",
        password: ""
    };
    $scope.a1=function(){
        $state.go("main.base.profile",{},{reload:true});
    };
    $scope.a2=function(){
        $state.go("main.base.favourites",{},{reload:true});
    };
    $scope.a3=function(){
        $state.go("main.base.settings",{},{reload:true});
    };
    $scope.a4=function(){
        $state.go("main.base.causes",{},{reload:true});
    };
    $scope.headercurrectpath = ($location.$$path).replace("/", "");
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        $window.scrollTo(0, 0);
        if ($sessionStorage.userName != undefined) {
            $scope.userName = $sessionStorage.userName;
            $scope.userLogo = $sessionStorage.userLogo;
        } else {
            $scope.getData();
        }

        //$scope.headercurrectpath = toState.url;
        $scope.headercurrectpath = ($location.$$path).replace("/", "");
    });

    if ($sessionStorage.userName != undefined) {
        $scope.userName = $sessionStorage.userName;
    }

    if ($sessionStorage.userLogo != undefined) {
        $scope.userLogo = $sessionStorage.userLogo;
    }





    $scope.fillin = function() {
       // alert("hi");
    };

    $scope.submitForm = function(isValid) {
        if (isValid) {}
    };

    $scope.ShowOrHide = function() {
        $scope.dropdownContent = ($scope.dropdownContent == 'hide') ? "show" : "hide";

        angular.element('.side-nav').sideNav('hide');
    };

    $scope.hideSearch = function() {
        $scope.dropdownContent = "hide";
    };



    $scope.ResetPassword = function(valid) {
                        if (valid) {
                            var obj = {
                                "hash": $scope.hashword,
                                "password": $scope.resetFrminput.password2
                            };
                            headerFactory.resetPassWithHash(obj).success(function(res) {
                
                                Materialize.toast("Reset Request Send", 4000);
                                angular.element('#reset-password-close').trigger('click');
                                angular.element('.lean-overlay').css("display", "none");
                                //$scope.resetFrm.$setPristine();
                                $state.go("main.base.home");
                                angular.element('#login-pop').openModal();
                            }).error(function(err) {
                                $scope.signinError = err.data;
                                Materialize.toast("Reset password link is Already used, Please try again", 4000);
                                angular.element('#reset-password-close').trigger('click');
                                // $scope.resetFrm.$setPristine();
                                // $scope.resetFrminput.$setPristine();
                                // $state.go("main.base.landing");
                            });
                        }
                    };
    $scope.register = function(valid) {
        if (valid) {
            var ele = angular.element("#signsubbtn");
            ele.attr('disabled', 'disabled');
            ele.text('Signing In..');
        if(!$location.$$url.substring(8))
            {
                delete $scope.registerForm['inviteCode']
               
            }
            headerFactory.registerUser($scope.registerForm).success(function(res) {

                $scope.registerForm = res;
                $cookieStore.put('userToken', res.data.token);
                $cookieStore.put('userId', res.data.userId);
                $scope.loggedIn = $scope.isLoggedIn();
                $scope.getData();
                Materialize.toast("Register Successfully", 4000);
                angular.element('#signup-close').trigger('click');
                // $scope.myRegForm.$setPristine();
                $state.go("main.base.settings");
                $timeout(NewUserShow, 3100);
                    function NewUserShow() {
                        Materialize.toast("Please Update Your Details", 4000);
                    }
            }).error(function(err) {
                $scope.signinError = err.data;
                Materialize.toast($scope.signinError, 4000);
            });
        }
    };

    $scope.modalRef=function(form){
       // angular.copy({},form);\
        angular.element('form[name="myRegForm"]')[0].reset();
        angular.element('form[name="myLogForm"]')[0].reset();
       
        if(form !='' && form !=undefined){
            form.$setPristine();
        form.$setUntouched(); 
            
        }
        else{
        }

    };
    $scope.login = function(valid) {

        if (valid) {
            var ele = angular.element("#logsubbtn");
            ele.attr('disabled', 'disabled');
            ele.text('Processing..');

            headerFactory.loginUser($scope.loginForm, $scope.Error)
                .success(function(res) {
                    $scope.loginForm = res;
                    $cookieStore.put('userToken', res.data.token);
                    $cookieStore.put('userId', res.data.userId);
                    $scope.loggedIn = $scope.isLoggedIn();
                    $scope.getData();
                    ele.removeAttr('disabled');
                    ele.text('Login');
                    $state.go("main.base.home");
                    $timeout(UserInfoShow, 8000);

                    function UserInfoShow() {
                        $state.reload();
                        angular.element('#login-close').trigger('click');
                        Materialize.toast("Welcome back " + ($sessionStorage.userName == "null null" ?  "" :  $sessionStorage.userName)+ "!", 3000);
                    }
                })
                .error(function(err) {
                    $scope.loginError = err.data;
                    ele.removeAttr('disabled');
                    ele.text('Login');
                    Materialize.toast($scope.loginError, 4000);
                });
                if($scope.loginForm.logcheck==false){
                    $timeout(function(){
                            $scope.logout();
                    },86400000);
                }
        }

    };




    $scope.WrongPassword = function(valid) {
        if (valid) {
            headerFactory.wrongUser($scope.wrongForm).success(function(res) {
                    $scope.wrongForm = res;
                    angular.element('#wrong-pop-close').trigger('click');
                    $state.reload();
                    Materialize.toast("your password has been reset, please allow a few minutes for us to send an email and Check Your mail", 4000);
                })
                .error(function(err) {
                    $scope.wrongError=err.data;
                    Materialize.toast($scope.wrongError, 4000);

                });
        }
    };

    $scope.getData = function() {
        if ($cookieStore.get('userToken') && $cookieStore.get('userId')) {
            var obj = {
                userToken: $cookieStore.get('userToken'),
                userId: $cookieStore.get('userId')
            };
            headerFactory.getUser(obj).success(function(response) {

                $sessionStorage.userName = response.data.firstName + " " + response.data.lastName;
                $sessionStorage.userLogo = response.data.image;
                $sessionStorage.userState = response.data.state;
                $scope.userName = response.data.firstName + " " + response.data.lastName;
                $scope.userLogo = response.data.image;
                $scope.isBase64 = true;
                $sessionStorage.userName = response.data.firstName + " " + response.data.lastName;
                $scope.isbase64 = false;

                if (response.data.facebookVerified===true) {
                    convertBase64FromURL(response.data.image, function(base64Image) {
                        $scope.userLogo = $sessionStorage.userbase64logo = base64Image;
                    });
                }


            }).error(function(err) {});

        }
    };
    $scope.getData();

    $scope.isLoggedIn = function() {
        return ($cookieStore.get('userToken') && $cookieStore.get('userId')) ? true : false

    };

    $scope.loggedIn = $scope.isLoggedIn();
    
    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function() {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function() {
            backToTop();
        });
        $('#back-to-top').on('click', function(e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    $scope.exp1 = false;
    $scope.menuHeader = "";




    // logout
    $scope.logout = function() {
        $cookieStore.remove('userToken');
        $cookieStore.remove('userId');
        $scope.loggedIn = $scope.isLoggedIn();
        Materialize.toast("Please wait while we are logging you out", 4000);
        headerFactory
        .logOutUser()
        .success(function(result) {
                console.log("success:", result);
            }).error(function(error) {
                console.log("errors:", error);
             });
        $state.go("main.base.landing",{},{reload: true});
        
        delete $sessionStorage.userLogo;
        delete $sessionStorage.userName;
        delete $sessionStorage.userState;
        delete $sessionStorage.userbase64logo;
        $localStorage.$reset();
        sessionStorage.removeItem('authCode');
        
        $timeout(cookiedlt, 1000);
        

        function cookiedlt() {
            angular.element('#logout-pop').openModal();
            $scope.closeModal();

       }             
    };


    $scope.isLoading = function () {
      return $http.pendingRequests.length === 0;
    };

    $scope.$watch($scope.isLoading, function (value) {
          if (value) {
            angular.element('#logout-pop').closeModal();
            angular.element('.lean-overlay').css("display", "none");
            // Materialize.toast("Please login again to continue our service", 4000);
          }
    });




    /* For login Using Facebook Start */
        
        headerFactory.faceBookLogin( function(e) {
            // console.log(e.origin);
            if (e.origin != "http://www.cashpack.org") {
                return
            }
            facebookWindow.close();
            window.sessionStorage.setItem('authCode', e.data.token);
            $http.post('https://api.cashpack.org/facebook', { "authCode": e.data.token }, {
                "headers": {
                    "content-type": "application/json",
                    "cache-control": "no-cache"
                }
            }).success(function(res) {

                $scope.loginForm = res;
                $cookieStore.put('userToken', res.data.token);
                $cookieStore.put('userId', res.data.userId);
                $scope.loggedIn = $scope.isLoggedIn();
                $scope.getData();

                //$scope.myLogForm.$setPristine();
                $state.go("main.base.home");
                $timeout(UserInfoShow, 3000);

                function UserInfoShow() {
                    $state.reload();

                    Materialize.toast("Welcome back " + ($sessionStorage.userName == "undefined" ? "" : $sessionStorage.userName) + "!", 4000);

                }
            }).error(function(err) {
                //console.log(err);
                $scope.fbError=err.data;
                Materialize.toast($scope.fbError, 4000);
            });
        });

        $scope.fb_login = function() {
            var Lurl = 'https://www.facebook.com/dialog/oauth?client_id=899762153462852&response_type=code&scope=public_profile,email&display=popup&redirect_uri=http://www.cashpack.org/AuthSuccess/success.php';
            facebookWindow = window.open(Lurl, 'Facebook Login', 'toolbar=no,scrollbars=no,resizable=no,width=420,height=650');
        };
    /* For login Using Facebook End */
    

    $scope.accessToken = function(value) {
        // console.log('accessToken', value);
    };

    // $rootScope.$on('$viewContentLoaded', function(event, viewName, viewContent) {
    //     angular.element("#page-loading").fadeOut();
    // });

    $scope.ShowOrHideAfter = function(exp1) {
        $scope.exp1 = !$scope.exp1;
        console.log("exp1",$scope.exp1);
    };

    $scope.ShowOrHideHeader = function(exp1) {
        $scope.menuHeader = $scope.menuHeader ? "" : "show";
    };

    angular.element($window).bind('resize', function() {
        angular.element("#drop-down-mob").css({ 'display': 'none', 'opacity': '0' });
    });
    if ($cookieStore.get('userToken') && $cookieStore.get('userId'))
    {
        headerFactory
            .getTotal()
            .success(function(res) {
                 //$scope.ALLprice =res.data;
                 $scope.price =res.data.processedFunds + res.data.availableFunds;
                console.log("res:",res);
            })
            .error(function(data, status, headers, config) {
                
                //Materialize.toast('something went wrong', 4000);
            });
    };


    $(document).ready(function() {
        $('.tooltipped').tooltip({ delay: 50 });
    });
    $('.title-section-menu ul li a').on('click', function() {
    self.location = $(this).attr('href');
});
    
}]);
