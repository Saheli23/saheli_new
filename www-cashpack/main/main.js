var mainModule = angular.module('mainModule', ['ui.router']);

// mainModule.directive('loading', ['$http', function ($http) {
//     return {
//       restrict: 'A',
//       link: function (scope, element, attrs) {
//         scope.isLoading = function () {
//           return $http.pendingRequests.length > 0;
//         };
//         scope.$watch(scope.isLoading, function (value) {
//           if (value) {
//             if(element[0].baseURI.split('/').splice(-1)[0] === "home" || element[0].baseURI.split('/').splice(-1)[0] === ""){
//                 element.removeClass('ng-hide');
//                 $("body").addClass("modal-open");
//             }
//           } else {
//             element.addClass('ng-hide');
//             $("body").removeClass("modal-open");
//           }
//         });
//       }
//     };
// }]);

mainModule.directive('updateTitle', ['$rootScope', '$timeout',
  function($rootScope, $timeout) {
    return {
      restrict: 'EA',
      link: function(scope, element) {

        var listener = function(event, toState) {

          var title = 'CashPack';
          if (toState.data && toState.data.pageTitle) title = toState.data.pageTitle;

          $timeout(function() {
            element.text(title);
          }, 0, false);
        };

        $rootScope.$on('$stateChangeSuccess', listener);
      }
    };
  }
]);


mainModule.config(['$stateProvider','$httpProvider','$cookiesProvider','$locationProvider', function( $stateProvider,$httpProvider,$cookiesProvider,$locationProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.get    = {'Content-Type' : 'application/json'};
    $httpProvider.defaults.headers.post   = {};
    $httpProvider.defaults.headers.put    = {};
    $httpProvider.defaults.headers.patch  = {};
    $httpProvider.defaults.headers.delete = {};
    $httpProvider.defaults.withCredentials = false;
    $httpProvider.defaults.useXDomain     = true;
    $httpProvider.defaults.headers.common['Access-Control-Allow-Headers'] = '*';
    $httpProvider.defaults.headers.common['Access-Control-Allow-Methods'] = 'GET, POST,DELETE,PUT,OPTIONS';
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
     $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    $stateProvider
        .state('main', {
            abstract: true,
            url: '/',
            cache: false,
            views: {
                '': {
                    templateUrl: 'main/main.view.html'
                }
            }
        })
        .state('main.base', {
            abstract: true,
            url: '',
            cache: false,
            views: {
                'header': {
                    templateUrl: 'main/templetes/main.header.html',
                    controller: 'headerController'
                },
                'container': {
                    templateUrl: 'main/templetes/main.content.html'
                },
                'footer': {
                    templateUrl: 'main/templetes/main.footer.html',
                    controller: 'footerController'
                }
            }
        })

           



}]);
// mainModule.run(function($rootScope,$route) {
//  $rootScope.$on("$routeChangeSuccess", function(currentRoute, previousRoute){
//     //Change page title, based on Route information
//     $rootScope.title = $route.current.title;
//   });
// });
