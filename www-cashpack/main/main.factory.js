var mainModule = angular.module('mainModule');

mainModule.factory('headerFactory', ['$http','$cookieStore','$stateParams',function($http,$cookieStore,$stateParams) {
    return {
        registerUser: function(Obj) {
            return $http.post('https://api.cashpack.org/user', JSON.stringify(Obj), {
                "headers": {
                    "content-type": "application/json"
                }
            });
        },

        loginUser: function(Obj) {
            return $http.post('https://api.cashpack.org/authenticate', JSON.stringify(Obj), {
                "headers": {
                    "content-type": "application/json"
                }
            });

        },
        wrongUser: function(Obj) {
            return $http.post('https://api.cashpack.org/forgotpassword', JSON.stringify(Obj), {
                "headers": {
                    "content-type": "application/json"
                }
            });
        },
        

        getUser: function(){
           return $http({
                     method: 'GET',
                     url: 'https://api.cashpack.org/user',
                     headers: {
                       'x-cp-user-id': $cookieStore.get('userId'),
                       'x-cp-token'  : $cookieStore.get('userToken')
                     },
           });

         },

        updateUser: function(data) {
            return $http.put('https://api.cashpack.org/user', data,{
                "headers": {
                    "content-type": "application/json",
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                }
            });
        },
        addFavourites: function(data){
            return $http.post('https://api.cashpack.org/user/favorites', JSON.stringify(data),{
                "headers": {
                    "content-type": "application/json",
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                }
            });
        },
        getFavourites: function(){
            return $http.get('https://api.cashpack.org/user/favorites',{
                "headers": {
                    "content-type": "application/json",
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                }
            });

           
        },
        deleteFavourites: function(data){
           return $http.delete('https://api.cashpack.org/user/favorites',{
                data: JSON.stringify(data),
                "headers": {
                    "content-type": "application/json",
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                }
            });
        },
        faceBookApi: function(data,config){
            return $http.post('https://api.cashpack.org/facebook',$.param(data),{ 
                "headers" :
             {"content-type": "application/x-www-form-urlencoded"}
             })
        },
        gettoptrending : function(Obj){
         return $http.post('https://api.cashpack.org/offers/trending',JSON.stringify(Obj),{
                "headers": {
                    "x-cp-token" :$cookieStore.get('userToken'),
                    "x-cp-user-id": $cookieStore.get('userId')
                    }
         });
        },
        getAllList : function(Obj) {
         return $http.post('https://api.cashpack.org/domain/featured',JSON.stringify(Obj), {
                "headers": {
                    "x-cp-token" :$cookieStore.get('userToken'),
                    "x-cp-user-id": $cookieStore.get('userId')
                    }
                }
                
             ); 
        },
        getstoreAllList : function(Obj) {
         return $http.post('https://api.cashpack.org/offers/featured',JSON.stringify(Obj), {
                "headers": {
                    "x-cp-token" :$cookieStore.get('userToken'),
                    "x-cp-user-id": $cookieStore.get('userId')
                    }
                }
             ); 
        },
        getLatestofferList : function(Obj) {
            return $http.post('https://api.cashpack.org/offers/latest',JSON.stringify(Obj),{
                "headers": {
                    "x-cp-token" :$cookieStore.get('userToken'),
                    "x-cp-user-id": $cookieStore.get('userId')
                    }
            });

        },
        searchList: function(Obj){
            return $http.post('https://api.cashpack.org/domain/search',JSON.stringify(Obj),{
                "headers":{
                    "Content-Type":'application/json' 
                }
            });
        },
        getMarchentAllList: function(Obj) {
            return $http.post('https://api.cashpack.org/domain/' + $stateParams.merchantDomainName,JSON.stringify(Obj),{
                "headers": {
                    "content-type": "application/json",
                     "x-cp-token" :$cookieStore.get('userToken'),
                     "x-cp-user-id": $cookieStore.get('userId') 
                }
            });
        },
        getMarchentCategory: function() {
            return $http.get('allmarchent/category.json');
        },        
        
        getMarchentsearchlist: function() {
            return $http.post('https://api.cashpack.org/domain/all', JSON.stringify({source:'website'}));
        },
        Marchentsubmitcode: function( Obj ) {                  
            return $http.post('https://api.cashpack.org/submitcode', JSON.stringify(Obj),{
                "headers": {
                    "content-type": "application/json",
                     "x-cp-token" :$cookieStore.get('userToken'),
                     "x-cp-user-id": $cookieStore.get('userId') 
                }
            });
        },
        getCouponAllList : function(Obj) {
          return $http.post('https://api.cashpack.org/offers/all',JSON.stringify(Obj),{
                "headers":{
                    "Content-Type":'application/json',
                    "x-cp-token" :$cookieStore.get('userToken'),
                    "x-cp-user-id": $cookieStore.get('userId') 
                }
            });

        },
        searchCouponList: function(Obj){
            return $http.post('https://api.cashpack.org/offers/search',JSON.stringify(Obj),{
                "headers":{
                    "Content-Type":'application/json' ,
                     "x-cp-token" :$cookieStore.get('userToken'),
                    "x-cp-user-id": $cookieStore.get('userId') 
                }
            });
        },
        topMerchantList: function(Obj){

           return $http.post('https://api.cashpack.org/domain/top',JSON.stringify(Obj),{
                "headers":{
                    "content-type":'application/json',
                     "x-cp-token" :$cookieStore.get('userToken'),
                    "x-cp-user-id": $cookieStore.get('userId') 
                }
            })
        },
        logOutUser: function(Obj){
          return $http.get('logoutProcessToClear.php');
        },
        // schoolsList: function(Obj){
        //    return $http({
        //              method: 'POST',
        //              url: 'https://api.cashpack.org/school',
        //              headers: {
        //                 "Content-Type" : "application/json"
                       
        //              },
        //              data: JSON.stringify(Obj)
        //    });
            //           return $http.post('https://api.cashpack.org/school',JSON.stringify(Obj),{
            //     "headers":{
            //         "content-type":'application/json',
            //          "x-cp-token" :$cookieStore.get('userToken'),
            //         "x-cp-user-id": $cookieStore.get('userId') 
            //     }
            // })

         // },logoutProcessToClear.php
        schoolsList: function(Obj){
           return $http({
                     method: 'POST',
                     url: 'https://api.cashpack.org/schools/search',
                     headers: {
                       "Content-Type" : "application/json"
                     },
                     data: JSON.stringify(Obj)
           });

         },

        charityList: function(Obj){
           return $http({
                     method: 'GET',
                     url: 'https://data.orghunter.com/v1/charitysearch?user_key=e9cf64aa0e7ee97f7168a0764b643344',
                     headers: {
                        "Cache-Control": "no-cache",
                        "Content-Type" : "application/x-www-form-urlencoded"
                     }
           });
      

         },
        charityDetails: function(Obj){
           return $http({
                     method: 'GET',
                     url: 'https://data.orghunter.com/v1/charitysearch?user_key=e9cf64aa0e7ee97f7168a0764b643344',
                     headers: {
                       "Content-Type" : "application/json"
                     }
           });
      

         } ,
         resetPassWithHash: function(Obj) {
            return $http.post('https://api.cashpack.org/resetpassword', JSON.stringify(Obj),{
                "headers": {
                    "content-type": "application/json"
                }
            });
        },
        statesList: function(){
          return $http.get('settings/states.json');
        },  
        merchantAlphabetSearch: function(Obj){
          return $http.post('https://api.cashpack.org/domain/byletter',JSON.stringify(Obj), {
                "headers": {
                    "content-type": "application/json"
                    }
                }
             );
        }, 
        getMarchentDetails: function(domainname,Obj) {
            return $http.post('https://api.cashpack.org/domain/' + domainname,JSON.stringify(Obj),{
                "headers": {
                    "content-type": "application/json",
                     "x-cp-token" :$cookieStore.get('userToken'),
                     "x-cp-user-id": $cookieStore.get('userId') 
                }
            });
        },
        getOfferFeatured : function() {
         return $http.post('https://api.cashpack.org/offers/featured', {
                "headers": {
                    "x-cp-token" :$cookieStore.get('userToken'),
                    "x-cp-user-id": $cookieStore.get('userId')
                    }
                }
             ); 
        },
        inviteFriend : function(Obj) {
         return $http.post('https://api.cashpack.org/invite',JSON.stringify(Obj), {
                "headers": {
                    "content-type": "application/json",
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                    }
                }
             ); 
        },
        SendFeedback : function(Obj) {
         return $http.post('https://api.cashpack.org/feedback',JSON.stringify(Obj), {
                "headers": {
                    "content-type": "application/json",
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                    }
                }
             ); 
        },
        orgCharity : function(Obj) {
            return $http.post('https://api.cashpack.org/charities',JSON.stringify(Obj),  {
                "headers": {
                    "content-type": "application/json"
                    }
                });
        },
        getOrder : function(Obj) {
            return $http.get('https://api.cashpack.org/user/orders',{
                "headers": {
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                    }
                }
             );
        },
        // new API
        getTotal : function() {
            return $http.get('https://api.cashpack.org/user/cashpack',{
                "headers": {
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                    }
                }
             );
        },
        GetUserDonations : function() {
            return $http.get('https://api.cashpack.org/user/donations',{
                "headers": {
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                    }
                }
             );
        },
        GetUserCauses : function() {
            return $http.get('https://api.cashpack.org/user/causes',{
                "headers": {
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                    }
                }
             );
        },
        AddUserCauses : function(Obj) {
            return $http.post('https://api.cashpack.org/user/causes',JSON.stringify(Obj),{
                "headers": {
                    "content-type": "application/json",
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                    }
                }
             );
        },
        RemoveUserCauses : function(data) {
            return $http.delete('https://api.cashpack.org/user/causes',{
                data: JSON.stringify(data),
                "headers": {
                    "content-type": "application/json",
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                }
              }
             );
        },
                        //end  
                        // new fact
        getQuickSearch : function(data) {
            return $http.post('https://api.cashpack.org/charities/search',
               JSON.stringify(data),{
                "headers": {
                    "content-type": "application/json",
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                }
              }
             );
        },
        getPayment : function() {
            return $http.get('https://api.cashpack.org/user/payments',{
                "headers": {
                    "content-type": "application/json",
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                }
              }
             );
        },
        addPayment : function(Obj) {
            return $http.post('https://api.cashpack.org/user/payments',JSON.stringify(Obj),{
                "headers": {
                    "content-type": "application/json",
                    'x-cp-user-id': $cookieStore.get('userId'),
                    'x-cp-token'  : $cookieStore.get('userToken')
                    }
                }
             );
        },
                        //end
        GetCharityFinancial: function(Obj) {
            return $http.post('https://api.cashpack.org/charityfinancial',JSON.stringify(Obj));
        },
        GetCharityCategories: function(){
             return $http.get('https://api.cashpack.org/charitycategories');
        },
        GetAboutInfo: function(Obj){
            return $http.post('https://api.cashpack.org/charitylocation',JSON.stringify(Obj),  {
                "headers": {
                    "content-type": "application/json"
                    }
                });
        },
        GetMarcode:function() {
            return $http.post('https://api.cashpack.org/domain/'+$stateParams.merchantDomainName+'/codes'
              ,JSON.stringify(Obj),{
                "headers": {
                    "content-type": "application/json",
                     "x-cp-token" :$cookieStore.get('userToken'),
                     "x-cp-user-id": $cookieStore.get('userId') 
                }
            });
        },
        GetMardeal:function() {
            return $http.post('https://api.cashpack.org/domain/'+$stateParams.merchantDomainName+'/deals',JSON.stringify(Obj),{
                "headers": {
                    "content-type": "application/json",
                     "x-cp-token" :$cookieStore.get('userToken'),
                     "x-cp-user-id": $cookieStore.get('userId') 
                }
            });
        },
        getFaqInfo: function() {
            return $http.get('faq/faq.json');
        },
        
        faceBookLogin: (function(){
            var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
            var eventer = window[eventMethod];
            var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message",
                facebookWindow = "",
                cal = 123,
                callback = function(e){
                    cal(e);
                };
            eventer(messageEvent, callback,false);

            return function(cl){
                cal = cl;
            }
        }())




    };
}]);
