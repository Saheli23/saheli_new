var marchentModule = angular.module('marchentModule');
marchentModule.controller('marchentController', ['$scope', '$stateParams','$location', '$cookieStore', 'headerFactory', '$document', function($scope, $stateParams,$location, $cookieStore, headerFactory, $document) {
//console.log($scope.isLoggedIn);
    $scope.toggle = "show more..";
    $scope.isfull = false;
    $scope.ClassName = "";
    $scope.alldealList = [];
    $scope.allcodeList = [];
    $scope.allList = [];
    $scope.param = $stateParams.param;
    $scope.count = $location.$$url.split('?')[1];
    
    $scope.numLimit = 300;
    $scope.submitmsg = '';
    $scope.togglename = "more";
        
    $scope.country={"country" : $scope.count };
    headerFactory.getMarchentAllList($scope.country)
        .then(function(result) {

            $scope.allList = result.data.data[0];
            $scope.alldealList = result.data.data[0].deals;
            $scope.allcodeList = result.data.data[0].codes;
            $scope.data.domain = result.data.data[0].domain;
            $scope.data.merchantId = result.data.data[0].id;
            if ($scope.allcodeList.length == 0 && $scope.alldealList.length > 0) {
                angular.element(".tab:eq(1) > a").trigger('click');
            }

        });
$scope.isLoggedIn = function() {
        return ($cookieStore.get('userToken') && $cookieStore.get('userId')) ? true : false
   
   };
    $scope.data = {
        domain: "",
        merchantId: "",
        code: "",
        description: "",
        expirationDate: ""
    };

    $scope.readMore = function() {
        $scope.numLimit = 10000;
        $scope.toggle = "";
    };
    $scope.toggleState = function() {

        $scope.isfull = !$scope.isfull;

        if ($scope.isfull)
            $scope.togglename = "less";
        else
            $scope.togglename = "more";
    };
    $scope.ShowOrHide = function(form) {

        if (angular.isUndefined($cookieStore.get('userId'))) {
            Materialize.toast("Please login or signup first", 4000);
        } else {
            angular.element('.category-select-mega').toggleClass('show');
            //$scope.ClassName = $scope.ClassName ? "" : "show";
        }
        angular.element('form[name="frmCode"]')[0].reset();
                form.$setPristine();
                form.$setUntouched();
    };

    $scope.close=function(){
         //$scope.ClassName= "";
         angular.element('.category-select-mega').removeClass('show');
    };

    $scope.submit = function(valid,form) {
        if (angular.isUndefined($cookieStore.get('userId'))) {
            Materialize.toast("Please login or signup first", 4000);

        } else {

            if (valid) {

                headerFactory.Marchentsubmitcode($scope.data)
                    .success(function(response) {

                        $scope.submitmsg = "thank you for sharing the code with us";

                            $scope.data = {     
                                                domain: $scope.data.domain,
                                                merchantId: $scope.data.merchantId,
                                                code: "",
                                                description: "",
                                                expirationDate: ""
                                            };
                        $scope.ShowOrHide(form);
                        Materialize.toast($scope.submitmsg, 4000);

                    })
                    .error(function(err) {

                        Materialize.toast("something went wrong", 4000);

                        $scope.ShowOrHide();
                    });

            } else {
                Materialize.toast("Please fill the details", 4000);


            }
        }



    };
    $scope.other=function(){
        //if ($scope.allcodeList.length == 0 && $scope.alldealList.length > 0) {
                angular.element(".tab:eq(1) > a").trigger('click');
            //}
    };
    $scope.other1=function(){
        //if ($scope.allcodeList.length == 0 && $scope.alldealList.length > 0) {
                angular.element(".tab:eq(0) > a").trigger('click');
            //}
    };

    



    $document.on('click', function(e) {

        var isshow = angular.element('.category-select-mega').is(":visible");
        //var isshow = angular.element('.category-select-mega').hasClass('show');
        var container =  angular.element(".category-select-mega, .noData-text:first, .cupon-Merchant-right > div.button-wrap > span");
            if (!container.is(e.target) && container.has(e.target).length === 0) {

        //if (isshow) {
            //angular.element('.sub-but').trigger('click');
            angular.element('.category-select-mega').removeClass('show');
        }

    });

    angular.element('.sub-but').click(function(event) {
        event.stopPropagation();
    });
    angular.element('.category-select-mega').click(function(event) {
        event.stopPropagation();
    });





}]);
