var marchentModule = angular.module('marchentModule', ['ui.router']);
/*****************Scroll Directive****************/
marchentModule.directive('modelOpen', function($cookieStore) {
  return {
    restrict: 'EA',
    link: function(scope, $elm) {
    // var iframeContainer  = angular.element(".h3-container").offset();
      $elm.on('click', function() {
        if(!angular.isUndefined($cookieStore.get('userId'))){
            console.log("called");
            angular.element('.category-select-mega').addClass('show');
        }
        else{
            Materialize.toast("Please login or signup first", 4000);
        }
      });
    }
  };
});

marchentModule.config(function( $stateProvider, $httpProvider, $urlRouterProvider)  {

    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.get    = {};
    $httpProvider.defaults.headers.post   = {};
    $httpProvider.defaults.headers.put    = {};
    $httpProvider.defaults.headers.patch  = {};
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];


    $stateProvider.
    state('main.base.merchant', {
        url: 'store/:merchantDomainName?marCount',
        cache: false,
        data: {
              pageTitle: 'Cashpack-Store'
            },
        views: {
            'content': {
                templateUrl: 'marchent/marchent.html',
                controller: 'marchentController'
            }
        },
        params: {
                marCount:null,
            },
        ncyBreadcrumb: {
            parent: 'home page',
            label: 'marchent page'
        }
    });


});
// marchentModule.run(['$rootScope', '$route', function($rootScope, $route) {
//     $rootScope.$on('$routeChangeSuccess', function() {
//         document.title = $route.current.title;
//     });
// }]);
// marchentModule.run(['$rootScope', function($rootScope) {
//     $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
//         $rootScope.title = current.$$route.title;
//     });
// }]);
