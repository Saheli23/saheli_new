var aboutUsModule = angular.module('aboutUsModule', ['ui.router']);

aboutUsModule.config(function($stateProvider, $urlRouterProvider,$locationProvider) {


    $stateProvider
        .state('main.base.aboutUs', {
            url: 'aboutUs',
            cache: false,
            data: {
              pageTitle: 'Cashpack-AboutUs'
            },
            views: {
                'content': {
                    templateUrl: 'aboutUs/about.html',
                    controller: 'aboutUscontroller'
                }
            }
        });


});
