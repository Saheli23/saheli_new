var settingsModule = angular.module('settingsModule');
settingsModule.controller('settingsController', ['$scope', '$timeout', '$state', '$cookieStore', '$sessionStorage', 'headerFactory', function($scope, $timeout, $state, $cookieStore, $sessionStorage, headerFactory) {
    $scope.isBase64 = false;
    $scope.updateUserdata = {};
    $scope.selected = {};
    $scope.selectedCountry = {};
    $scope.currentTime = new Date();
    $scope.maxDate = (new Date($scope.currentTime.getTime())).toISOString();
    function convertBase64FromURL(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            var reader = new FileReader();
            reader.onloadend = function() {
                callback(reader.result);
            }
            reader.readAsDataURL(xhr.response);
        };
        xhr.open('GET', url);
        xhr.responseType = 'blob';
        xhr.send();
    }
    $scope.groupSetup = {
        formatNoMatches : 'No Data Found'
    };

    $scope.getData = function() {
         headerFactory
             .getFavourites()
                  .success(function(res){
                    //console.log(res.data.merchants);
                    $scope.favouriteList=res.data.merchants;
                   })
                  .error(function(err){
                    //console.log(err);

                     });
        headerFactory
            .statesList()
            .success(function(res) {
                $scope.statelist = res;
                angular.forEach($scope.statelist, function(value, key) {
                    if (value.abbreviation == $sessionStorage.userState) {
                        $scope.selected = value;
                    }
                })

            });
        headerFactory
            .getUser()
            .success(function(data, status, headers, config) {
                $scope.updateUserdata = data.data;

                $scope.updateUserdata.country = [{
                                    "name" : "US",
                                    "abbreviation" : "US"
                                }];
                $scope.selectedCountry = $scope.updateUserdata.country[0];
                //console.log("datas",data.data);
                if (angular.isUndefined($scope.selected.abbreviation)) {
                    $scope.updateUserdata.state = "";
                } else {
                    $scope.updateUserdata.state = $scope.selected.abbreviation;
                }
                if(data.data.image== null){
                    $scope.isbase64 = false;
                }
                else{
                     $scope.isbase64 = true;

                }
               
                $sessionStorage.userName = data.data.firstName + " " + data.data.lastName;
                
                if(data.data.facebookVerified){
                convertBase64FromURL(data.data.image, function(base64Image){
                    $sessionStorage.userbase64logo = base64Image;
                });
               }

                angular.forEach($scope.notifications, function(nVal){
                    if($.inArray( nVal.value , $scope.updateUserdata.notificationsDeliveryOptions.deliveryOptions ) >= 0)  nVal.selected = true;
                    else nVal.selected = false;
                });

                angular.forEach($scope.expirations, function(nVal){
                    if($.inArray( nVal.value , $scope.updateUserdata.notificationsFavoriteOffers.expirationOptions ) >= 0)  nVal.selected = true;
                    else nVal.selected = false;
                });

                //after a while check whether all the deliveryOptions are checked or not
                //if checked then checke 'all' checkbox otherwise leave 'all' checkbox deselected
                //setTimeout(function(){
                    // checkChildStatus();
                //}, 900);

            })
            .error(function(data, status, headers, config) {

            });

    };
    $scope.getData();

    $scope.userSubmit = function(valid) {
        if (valid) {
            console.log("form",valid);
            if (!angular.isUndefined($scope.file)) {
                if ($scope.file.base64 !== "") {
                    $scope.updateUserdata.image = $scope.file.base64;
                    $scope.isbase64 = true;
                }
            }

            if($sessionStorage.userbase64logo != undefined){
                $scope.updateUserdata.image = $sessionStorage.userbase64logo;
                $scope.isbase64 = true;
            }

            if (!angular.isUndefined($scope.selected.abbreviation)) {
                $scope.updateUserdata.state = $scope.selected.abbreviation;
            } else {
                $scope.updateUserdata.state = "";
            }

            if (!angular.isUndefined($scope.selectedCountry.abbreviation)) {
                $scope.updateUserdata.country = $scope.selectedCountry.abbreviation;
            } else {
                $scope.updateUserdata.country = "";
            }
            // $scope.updateUserdata.phoneNumber = $scope.updateUserdata.phoneNumber !== undefined ||  $scope.updateUserdata.phoneNumber !== null ? $scope.updateUserdata.phoneNumber : '';

            var notificationArray = [],
            expirationArray= [];

            angular.forEach($scope.notifications, function(notification){
              if (notification.selected)
                notificationArray.push(notification.value);
            });

            angular.forEach($scope.expirations, function(expiration){
              if (expiration.selected)
                expirationArray.push(expiration.value);
            });
            $scope.updateUserdata.notificationsDeliveryOptions = notificationArray;

            $scope.updateUserdata.notificationsExpirationOptions = expirationArray;

            if($scope.updateUserdata.phoneNumber && $scope.updateUserdata.phoneNumber.length <= 12){
                        headerFactory
                            .updateUser($scope.updateUserdata)
                            .success(function(res) {
                                $scope.getData();
                                Materialize.toast("your settings have been saved", 4000);
                                $state.go('main.base.profile');
                                $timeout(function() {
                                    $state.reload();
                                }, 2000);
            
                            })
                            .error(function(err) {
                                // console.log('updateUser->err', err);
                                Materialize.toast("please provide phone Number, email address and Profile Picture." , 4000);
                            });
            }else{
                Materialize.toast("Phone Number allow max 12 digits." , 4000);
            }
        }
        else{
            Materialize.toast("Fill up the Form" , 4000);
        }
    };

    $scope.initNotification = function(){
        $scope.notifications = [{
            'value' :  'email',
            'label' :  'Email',
            'id'    :  'email-notification',
            'selected' : false
        }, {
            'value' :  'web',
            'label' :  'Web',
            'id'    :  'web-notification',
            'selected' : false
        }, {
            'value' :  'text',
            'label' :  'Text',
            'id'    :  'SMS-notification',
            'selected' : false
        }];

        $scope.expirations= [
        {
            'id' : '1 day before',
            'value':  1,
            'selected' : false
        },
        {
            'id' : '3 day before',
            'value':  3,
            'selected' : false
        },
        {
            'id' : '7 day before',
            'value':  7,
            'selected' : false
        }];
  }

    $scope.initNotification();
     console.log("img before",$scope.file);
    $scope.chooseFile = function() {
        angular.element("#uploadFile").trigger('click');
        $scope.$watch($scope.file,
              function(value) {
                //$scope.file=$scope.updateUserdata.image;
                console.log("img after2",$scope.file);
              }
             );
        console.log("img after",$scope.file);
    };

    $scope.allNotify=function(){
        if(angular.element('.child-checkbox:checked').length !== angular.element('.child-checkbox').length){
            angular.element('.child-checkbox').prop('checked', true);
            angular.forEach($scope.notifications, function(notification){
                notification.selected = true;
            });
        }else{            
            angular.element('.child-checkbox').prop('checked', false);
            angular.forEach($scope.notifications, function(notification){
                notification.selected = false;
            });
        }


    };
    $scope.allexpiry=function(){
                if(angular.element('.exp-checkbox:checked').length !== angular.element('.exp-checkbox').length){
            angular.element('.exp-checkbox').prop('checked', true);
            angular.forEach($scope.expirations, function(expiration){
               expiration.selected= true;
            });
        }else{            
            angular.element('.exp-checkbox').prop('checked', false);
            angular.forEach($scope.expirations, function(expiration){
                expiration.selected= false;
            });
        }
    };

    $scope.userSubmit3 = function(valid) {
        if (valid) {

                if (!angular.isUndefined($scope.selected.abbreviation)) {
                    $scope.updateUserdata.state = $scope.selected.abbreviation;
                } else {
                    $scope.updateUserdata.state = "";
                }

                // $scope.updateUserdata.phoneNumber = $scope.updateUserdata.phoneNumber !== undefined ||  $scope.updateUserdata.phoneNumber !== null ? $scope.updateUserdata.phoneNumber : '';

                var notificationArray = [],
                expirationArray= [];

                angular.forEach($scope.notifications, function(notification){
                  if (notification.selected)
                    notificationArray.push(notification.value);
                });
                var element = document.getElementById('first_name');
                
                angular.forEach($scope.expirations, function(expiration){
                  if (expiration.selected)
                    expirationArray.push(expiration.value);
                });
                $scope.updateUserdata.notificationsDeliveryOptions = notificationArray;

                $scope.updateUserdata.notificationsExpirationOptions = expirationArray;
                if($scope.updateUserdata.emailAddress=="" || $scope.updateUserdata.emailAddress == undefined){
                    Materialize.toast("Please add an email to get notified" , 4000);
                    element.scrollIntoView();
                    angular.element('#email').focus();
                }
                else if($scope.updateUserdata.phoneNumber=="" || $scope.updateUserdata.phoneNumber== undefined){
                    Materialize.toast("Please add phone number to get notified" , 4000);
                    element.scrollIntoView();
                    angular.element('#phoneNum').focus();

                }
                else{
                    headerFactory
                                .updateUser($scope.updateUserdata)
                                .success(function(res) {
                                    $scope.getData();
                                    Materialize.toast("your settings have been saved" , 4000);
                                    $timeout(function() {
                                        
                                        $state.go('main.base.settings');
                                    }, 2000);
                
                                })
                                .error(function(err) {
                                    console.log('updateUser->err', err);
                                    // Materialize.toast(err , 4000);
                                });
                }
        }
    };
}]);
