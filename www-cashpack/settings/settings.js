var settingsModule = angular.module('settingsModule', ['ui.router']);

settingsModule.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('main.base.settings', {
            url: 'settings',
            cache: false,
            data: {
              pageTitle: 'Cashpack-Settings'
            },
            views: {
                'content': {
                    templateUrl: 'settings/settings.html',
                    controller: 'settingsController'

                },

               
            },
             resolve: {
                     data: function($q, $state,$timeout,$cookieStore) {
                            var deferred = $q.defer();
                             $timeout(function() {
                              if (angular.isUndefined($cookieStore.get('userId'))) {
                                  $state.go('main.base.landing');
                                  Materialize.toast("Please login or signup first", 4000);

                                  deferred.reject();
                              } else {
                                deferred.resolve();
                               }
                            });

                            return deferred.promise;
                          }
                }
        });
});
