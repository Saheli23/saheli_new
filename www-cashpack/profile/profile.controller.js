var profileModule = angular.module('profileModule');
profileModule.controller('profileController', ['$scope', '$cookieStore', 'headerFactory', '$sessionStorage','$state', '$stateParams', function($scope, $cookieStore, headerFactory, $sessionStorage, $state, $stateParams) {

     
    if($stateParams.scroll) $($stateParams.scroll)[0].scrollIntoView();
    $scope.usersData = [];
    $scope.favHist = [];
    $scope.charList = [];
    headerFactory
        .getUser()
        .success(function(data, status, headers, config) {
            
            $scope.usersData = data.data;


             headerFactory
            .statesList()
            .success(function(res) {
                $scope.statelist = res;
                angular.forEach($scope.statelist, function(value, key) {
                    if (value.abbreviation == $scope.usersData.state) {
                        $scope.selectedstate = value;
                    }
                })

            });



        })
        .error(function(data, status, headers, config) {
            
            Materialize.toast('something went wrong', 4000);
        });
        headerFactory
            .statesList()
            .success(function(res) {
                $scope.statelist = res;
                angular.forEach($scope.statelist, function(value, key) {
                    if (value.abbreviation == $sessionStorage.userState) {
                        $scope.selected = value;
                    }
                })

            });
        
        $scope.price=0;
        angular.element("a.price1").addClass("accountlink");

    headerFactory
        .getOrder()
        .success(function(res) {
            $scope.favHist =res.data;
             angular.element('#datatable').DataTable( {
        data: $scope.favHist,
        columns: [
            { data: "dateAdded",type: "date ",
             render:function (data) {
        var date = new Date(data);
        var month = date.getMonth() + 1;
        return date.getDate() + "/" + (month.length > 1 ? month : "0" + month) + "/" + date.getFullYear();}},
            { data: "merchantName" },
            { data: "finalCommission",render:function(data){
                var amt= data;
                return "USD " + data
            } },
            { data: "saleAmount",render:function(data){
                var amt= data;
                return "USD " + data
            } },
            { data: "cashpackStatus",render:function(data){
                var status = data;

                return status.toUpperCase()
            } }
            
        ]
    } );
            // console.log("res:",res);
        })
        .error(function(data, status, headers, config) {
            
            Materialize.toast('something went wrong', 4000);
        });
    headerFactory
        .getTotal()
        .success(function(res) {
             $scope.ALLprice =res.data;
             $scope.price =res.data.processedFunds + res.data.availableFunds;
            // console.log("res:",res);
        })
        .error(function(data, status, headers, config) {
            
            Materialize.toast('something went wrong', 4000);
        });

       

    $scope.Total=function(){
        $scope.price=$scope.ALLprice.processedFunds + $scope.ALLprice.availableFunds;
        angular.element("a.price1").addClass("accountlink");
        angular.element("a.price2").removeClass("accountlink");
        angular.element("a.price3").removeClass("accountlink");
    };
    $scope.avail=function(){
        angular.element("a.price2").addClass("accountlink");
        angular.element("a.price1").removeClass("accountlink");
        angular.element("a.price3").removeClass("accountlink");
        $scope.price=$scope.ALLprice.availableFunds;
    };
    $scope.Pending=function(){
        angular.element("a.price3").addClass("accountlink");
        angular.element("a.price2").removeClass("accountlink");
        angular.element("a.price1").removeClass("accountlink");
        $scope.price=$scope.ALLprice.pendingFunds;
    };
    $scope.Withdraw=function(){
      $state.go("main.base.payment", { 'paymentMethod': "cashout"},{reload:true});
    };
}]);
