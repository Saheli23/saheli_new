var profileModule = angular.module('profileModule', ['ui.router']);

profileModule.config(function($stateProvider ,$urlRouterProvider) {

    $stateProvider
        .state('main.base.profile', {
            url: 'profile',
            cache: false,
            data: {
              pageTitle: 'Cashpack-Account'
            },
            params :{scroll : ""},
            views: {
                'content': {
                    title: 'profile',
                    templateUrl: 'profile/profile.html',
                    controller: 'profileController'
                }
            },
             resolve: {
                     data: function($q, $state,$timeout,$cookieStore) {
                            var deferred = $q.defer();
                             $timeout(function() {
                              if (angular.isUndefined($cookieStore.get('userId'))) {
                                  $state.go('main.base.landing');
                                  Materialize.toast("Please login or signup first", 4000);

                                  deferred.reject();
                              } else {
                                deferred.resolve();
                               }
                            });

                            return deferred.promise;
                          }
                }
        });
});
