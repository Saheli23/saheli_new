var privacyModule = angular.module('privacyModule', ['ui.router']);

privacyModule.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('main.base.privacy', {
            url: 'privacy',
            cache: false,
            data: {
              pageTitle: 'Cashpack-privacy'
            },
            views: {
                'content': {
                    templateUrl: 'privacy/privacy.html',
                    controller: 'privacyController'
                }
            }
        })
        .state('main.base.terms', {
            url: 'terms',
            cache: false,
            data: {
              pageTitle: 'Cashpack-Terms'
            },
            views: {
                'content': {
                    templateUrl: 'privacy/terms.html'
                    
                }
            }
        })
        .state('main.base.howitworks',{
            url:'howitworks',
            cache: false,
            data: {
              pageTitle: 'Cashpack-howitworks'
            },
            views: {
                'content': {
                    templateUrl: 'privacy/howitworks.html',
                     controller: 'privacyController'
                }
            }
        })
});
