privacyModule = angular.module('privacyModule');
privacyModule.controller('privacyController', ['$scope', '$location', '$sessionStorage', function($scope, $location, $sessionStorage) {
    $scope.extensionpath = "http://www.cashpack.org/extension/cashpack-extension.crx";
    if ($location.host() == "localhost" || $location.host() == "dev02.developer24x7.com") { $scope.extensionpath = "http://" + $location.host() + "/www-cashpack/extension/cashpack-extension.crx"; }
    if ($location.host() == "webtest.cashpack.org") {
        $scope.extensionpath = "http://webtest.cashpack.org/extension/cashpack-extension.crx";

    }
   
    angular.element("#hiw2").css("display", "none");

    if(angular.isUndefined($sessionStorage.userName)) {
        $scope.islogincahspack = false;
    } else {
        $scope.islogincahspack = true;

    }
    //console.log(angular.element("#hiw1").is(':visible'));
     if(!angular.element("#hiw1").is(':visible')){
        angular.element("#hiw2").css("display", "block");
     };

        $scope.inst=function(){
        chrome.webstore.install('https://chrome.google.com/webstore/detail/hcggnikhmdneeojjdcnbojmpjbglobme',
         function(e){
            
            console.log(e);
            }, function(er){
            console.log(er);
            })
    };
           $scope.playIt = function() {
      if ($("#video1").get(0).paused) {$("#video1").get(0).play();}
      else {$("#video1").get(0).pause();}
    };
        $scope.start=function(){
            angular.element(".vidcls").css("display", "block");
           angular.element('#vidModal').openModal();
            angular.element(".vidcls").css("z-index", "9999");
        $scope.playIt();
        
        };

        $scope.close=function(){
            $("#video1").get(0).pause();
            $("#video1").get(0).currentTime = 0;
            angular.element('#vidModal').closeModal();
            angular.element('.lean-overlay').css("display", "none");
        };

}]);
