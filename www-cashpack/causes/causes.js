var causesModule = angular.module('causesModule', ['ui.router']);

causesModule.config(function($stateProvider ,$urlRouterProvider) {

    $stateProvider
        .state('main.base.causes', {
            url: 'causes',
            cache: false,
            data: {
              pageTitle: 'Cashpack-My Causes'
            },
            views: {
                'content': {
                    templateUrl: 'causes/causes.html',
                    controller: 'causesController',
                    'controllerAs': 'causesCtrl'

                }
            }
            ,
             resolve: {
                     data: function($q, $state,$timeout,$cookieStore) {
                            var deferred = $q.defer();
                             $timeout(function() {
                              if (angular.isUndefined($cookieStore.get('userId'))) {
                                  $state.go('main.base.landing');
                                  Materialize.toast("Please login or signup first", 4000);

                                  deferred.reject();
                              } else {
                                deferred.resolve();
                               }
                            });

                            return deferred.promise;
                          }
                }
        });
});
