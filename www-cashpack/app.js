var mainApp = angular.module('cashpack', [
    'ui.router',
    'ui.materialize',
    'ui.bootstrap',
    'ksSwiper',
    'localytics.directives',
    'ngclipboard',
    'ncy-angular-breadcrumb', // It will remove later
    'angularUtils.directives.uiBreadcrumbs',
    'ngCookies',
    'ngResource',
    'ngStorage',
    'angular-loading-bar',
    '720kb.socialshare',
    'angularMoment',
    'naif.base64',
    'mainModule',
    'landingModule',
    'homeModule',
    'marchentModule',
    'aboutUsModule',
    'faqModule',
    'allCuponsModule',
    'allMarchentModule',
    'profileModule',
    'favouritesModule',
    'settingsModule',
    'causesModule',
    'privacyModule',
    'fundraiserModule'

])

.directive('equalizeHeight', ['$timeout', '$window', function($timeout, $window) {
    return {
        restrict: 'A',
        controller: function($scope, $element, $attrs) {
            var current = $attrs.equalizeHeight;

            var elements = [],
                self = this;
            self.addElement = function(element) {
                elements.push(element);
            }

            // resize elements once the last element is found
            self.resize = function() {
                $timeout(function() {
                    if (current.length > 0) {
                        if (angular.element("."+current).is(":hidden")) {
                            angular.element("."+current).css({ 'visibility': 'hidden', 'display': 'block' })
                        }
                    }
                    var tallest = 0,
                        height;
                    angular.forEach(elements, function(el) {

                        el[0].style['height'] = 'auto';
                        height = el[0].offsetHeight;
                        if (height > tallest)
                            tallest = height;
                    });

                    // resize
                    angular.forEach(elements, function(el) {
                        el[0].style['height'] = tallest + 'px';
                    });
                    if (current.length > 0) {
                        if (angular.element("."+current).css("visibility") == 'hidden') {
                            angular.element("."+current).removeAttr('style');
                            angular.element("."+current).css({ 'display': 'none' });
                        }
                    }

                }, 0);
            };

            angular.element($window).bind('resize', function() {

                self.resize();

            });
        }
    };
}])

.directive('equalizeHeightAdd', ['$timeout', '$window', function($timeout, $window) {
        return {
            restrict: 'A',
            require: '^^equalizeHeight',
            link: function(scope, element, attrs, ctrl_for) {

                ctrl_for.addElement(element);
                if (scope.$last)
                    ctrl_for.resize();



            }
        };
    }])
    .directive('share', ['$document',function($document) {
            return {
                scope: true,
                template: '<a class="hovshare"><div ng-click="showOrHide($event)" class="share"></div></a>' +
                    '<div class="social-share social-display" >' +
                    '<ul>' +
                    '<li>' + '<a socialshare="" socialshare-provider="twitter" socialshare-text=""  socialshare-url="{{$location.$$absUrl}}">' + '<i class="fa fa-twitter-square" aria-hidden="true">' + '</i></a></li>' +
    
                    '<li>' + '<a  socialshare="" socialshare-provider="facebook" socialshare-text="}"  socialshare-url="{{$location.$$absUrl}}">' + '<i class="fa fa-facebook-square" aria-hidden="true">' + '</i></a></li>' +
    
                    '<li>' + '<a  socialshare="" socialshare-provider="email" socialshare-text=""  socialshare-url="{{$location.$$absUrl}}">' + '<i class="fa fa-envelope" aria-hidden="true">' + '</i></a></li>' +
    
                    '<li>' + '<a  socialshare="" socialshare-provider="pinterest" socialshare-text=""  socialshare-url="{{$location.$$absUrl}}">' + '<i class="fa fa-pinterest-square" aria-hidden="true">' + '</i></a></li>' +
                    '</ul>' +
                    '</div>{{newpath}}',
                link: function(scope,element,attrs) {
                    scope.showOrHide = function(event) {
                        var socialUl = angular.element(event.target).closest('share').find('.social-share');
                        angular.element('.social-share').not(socialUl).removeClass("display-toggle").addClass("social-display");
                        if(socialUl.hasClass("social-display")){
                            socialUl.removeClass("social-display");
                            socialUl.addClass("display-toggle");
                        }else{
                            socialUl.addClass("social-display");
                            socialUl.removeClass("display-toggle");
                        }
                    }

                    $document.on('click' , function(event){
                        var curTarget  = angular.element(event.target);
                        if(!curTarget.hasClass('share')){
                            var socialUl = angular.element('.social-share');
                            if(socialUl.hasClass("display-toggle")){
                                socialUl.addClass("social-display");
                                socialUl.removeClass("display-toggle");
                            }
                        }
                    });
                }
            }
        }])
    .directive('pwCheck', [function() {
        return {
            require: 'ngModel',
            link: function(scope, elem, attrs, ctrl) {
                var firstPassword = '#' + attrs.pwCheck;
                elem.add(firstPassword).on('keyup', function() {
                    scope.$apply(function() {
                        var v = elem.val() === $(firstPassword).val();
                        ctrl.$setValidity('pwmatch', v);
                    });
                });
            }
        }
    }])
    
    .directive('copy', ['$state','$document','$timeout','$localStorage', function($state, $document, $timeout, $localStorage) {
            return {
                template:   '<div class="cupon-code-border" >' +
                                '<a href="javascript:void(0)" ng-click="open()">' +
                                    '<span  class="btn button getdeal grad-copy">Show Code</span>' +
                                '</a>' +  
                            '</div>',
                scope: {
                    user: "=user"
                },
                link: function(scope) {
                    scope.open = function(){
                        $localStorage.listObj = scope.user;

                        if(window.location.href.indexOf('showCode') >= 0){
                            window.open(window.location.href,"_blank");
                        }else{
                            window.open(window.location.href+"?showCode","_blank");
                        }
                        window.location.href = scope.user.startLink;
                    };
                    
                }
    
            };
        }])
    .directive('search', [function() {
            return {
                template: '<div class="search-bar">' +
                    '<form name="Searchform" novalidate ng-submit="ctrl.search(Searchform.$valid)">' +
                    '<div class="search-bar-left">' +
                    '<input type="text" class="txtbox" placeholder="Search for Store"  name="SearchText" ng-model="SearchText">' +
                    '</div>' +
                    '<div class="search-bar-right">' +
                    '<input type="submit" class="submit" value="" ng-hide="!SearchText.length || SearchText.length<3" >' +
                    '</div>' +
                    '</form>' +
                    '</div>',
                scope: {
                    SearchText: "@SearchText"
                },
                controller: function($state, $scope, $stateParams) {
                    var ctrl = this;
                    ctrl.search = function(valid) {
                        if (valid) {
                            $state.go("main.base.allMarchent", { searchTerm: $scope.SearchText });
                        }
                    }
                },
                controllerAs: "ctrl",
                bindToController: true
            };
        }])

.directive('favouite', ['headerFactory','$localStorage', function(headerFactory, $localStorage) {
    return {
        template: '<a class="hoverfav"  ng-click="FavouriteList(userfav.favorite);"  data-message="{{message}}" >' +
            '<div class="star" style="background:#fff {{image}};  z-index:1;"></div>' +
            '</a>',
        scope: {
            userfav: "=user"

        },
        controller: function($scope, $cookieStore) {
            $scope.image = "url('images/sprite.png') no-repeat left 6px top -350px";
            if (angular.isUndefined($scope.userfav)) {
                $scope.userfav = {};
                $scope.image = " url('images/like1.png') no-repeat 10px"

            } else {
                if ($scope.userfav.favorite == true) {

                    $scope.image = " url('images/like1.png') no-repeat 10px"
                }
            }



            $scope.FavouriteList = function(isfav) {


                if ($cookieStore.get('userToken')) {
                    var data;



                    if (angular.isUndefined($scope.userfav.code)) {

                        data = {
                            id: $scope.userfav.id,
                            type: 'merchant',
                            source:'website'
                        },
                        data1 = {
                            id: $scope.userfav.favoriteId,
                            type: 'merchant',
                            source:'website'
                        }
                    } else {

                        if ($scope.userfav.code != "") {
                            data = {
                                id: $scope.userfav.id,
                                type: 'offer',
                                source:'website'
                            },
                            data1 = {
                                id: $scope.userfav.favoriteId,
                                type: 'offer',
                                source:'website'
                            }

                        } else {
                            data = {
                                id: $scope.userfav.id,
                                type: 'offer',
                                source:'website'
                            },
                            data1 = {
                                id: $scope.userfav.favoriteId,
                                type: 'offer',
                                source:'website'
                            }

                        }
                    }

                    if (!isfav) {


                        headerFactory
                            .addFavourites(data)
                            .success(function(response) {
                                $scope.fav = response.data;
                                $localStorage.favId = response.data.favoriteId;
                                $scope.userfav.favorite = true;
                                $scope.message = "Added to Favorites";
                                $scope.image = " url('images/like1.png') no-repeat 10px";
                                Materialize.toast("<a style='color:#fff;' href='#/favourites'>"+$scope.message+"</a>", 4000);
                            
                            })
                            .error(function(err) {
                                $scope.message = "error on add to Favorites";
                                Materialize.toast($scope.message, 4000);


                            });
                    } else {
                        // DELETE FAV
                        
                        if($scope.fav){
                            data12 = {
                                id        : $scope.fav.favoriteId,
                                type      : $scope.fav.type,
                                source    :'website'
                            }
                            headerFactory.deleteFavourites(data12).success(function(res){
                                    
                                    $scope.userfav.favorite=false;
                                    $scope.image    = "url('images/sprite.png') no-repeat left 6px top -350px";
                                    $scope.message  ="favorite removed";
                                    Materialize.toast($scope.message, 4000);
                                 })
                            .error(function(err){
                              console.log(err);
                                     $scope.message  =   "error on remove favorite";
                                     Materialize.toast($scope.message, 4000);
                             });
                        }else{
                            headerFactory.deleteFavourites(data1).success(function(res){
                                    $scope.userfav.favorite=false;
                                    $scope.image    = "url('images/sprite.png') no-repeat left 6px top -350px";
                                    $scope.message  ="favorite removed";
                                    Materialize.toast($scope.message, 4000);
                                 })
                            .error(function(err){
                              console.log(err);
                                     $scope.message  =   "error on remove favorite";
                                     Materialize.toast($scope.message, 4000);
                             });
                        }
                        
                    }


                } else {
                    $scope.message = "please login or signup first";
                    Materialize.toast($scope.message, 4000);
                };

            }



        }
    };
}])

.directive('favShow', ['headerFactory', '$cookieStore','$localStorage', function(headerFactory,$cookieStore,$localStorage) {
    return {
        restrict : 'EA',
        transclude : true,
        template: '{{message1}}<div class="imgFav" style="background: {{image}};  z-index:1; left:6%;"></div>',
        scope: {
            userfav: "=user"

        },
        link: function($scope, element) {
            $scope.image = "url('images/sprite.png') no-repeat left -4px top -362px";
            $scope.message1 = "Add to Favorites";
            if (angular.isUndefined($scope.userfav)) {
                $scope.userfav = {};
                $scope.image = " url('images/like1.png') no-repeat left 1px top 0px";
                $scope.message1 = "Add to Favorites";
            } else {
                if ($scope.userfav.favorite == true) {
                    $scope.image = " url('images/like1.png') no-repeat left 1px top 0px";
                    $scope.message1 ="Delete Favorite";
                }
            }
            $scope.isfav = $scope.userfav.favorite;
            element.bind("click", function(e){
                if ($cookieStore.get('userToken')) {
                    var data;



                    if (angular.isUndefined($scope.userfav.code)) {

                        data = {
                            id: $scope.userfav.id,
                            type: 'merchant',
                            source:'website'
                        },
                        data1 = {
                                id: $localStorage.favId,
                                type: 'merchant',
                                source:'website'
                            }
                    } else {

                        if ($scope.userfav.code != "") {
                            data = {
                                id: $scope.userfav.id,
                                type: 'offer',
                                source:'website'
                            },
                            data1 = {
                                id: $localStorage.favId,
                                type: 'offer',
                                source:'website'
                            }

                        } else {
                            data = {
                                id: $scope.userfav.id,
                                type: 'offer',
                                source:'website'
                            },
                            data1 = {
                                id: $scope.userfav.favoriteId,
                                type: 'offer',
                                source:'website'
                            }

                        }
                    }

                    if (!$scope.isfav) {


                        headerFactory
                            .addFavourites(data)
                            .success(function(response) {
                                $scope.userfav.favorite = true;
                                 $scope.favo = response.data;
                                $scope.isfav = $scope.userfav.favorite;
                                $scope.message = "Added to Favorites";
                                $scope.message1 ="Delete Favorite";
                                $scope.image = " url('images/like1.png') no-repeat left 1px top 0px";
                                Materialize.toast("<a style='color:#fff;' href='#/favourites'>"+$scope.message+"</a>", 4000);
                            })
                            .error(function(err) {
                                $scope.message = "Cannot add to Favorites";
                                Materialize.toast($scope.message, 4000);


                            });
                    } else {
                        if($scope.favo){
                           if(!$scope.favo.favoriteId==0){
                                data12 = {
                                           id        : $scope.favo.favoriteId,
                                           type      : $scope.favo.type,
                                           source    :'website'
                                         }
                            }else{
                                data12 = {
                                           id        : $scope.userfav.favoriteId,
                                           type      : $scope.favo.type,
                                           source    :'website'
                                         }
                            }
                            headerFactory.deleteFavourites(data12).success(function(res){
                                $scope.userfav.favorite=false;
                                $scope.isfav = $scope.userfav.favorite;
                                $scope.image    =   "url('images/sprite.png') no-repeat left -4px top -362px";
                                $scope.message="favorite deleted";
                                $scope.message1 = "Add to Favorites";
                                Materialize.toast($scope.message, 4000);

                            })
                            .error(function(err){
                                $scope.message  =   "already deleted favorite";
                                Materialize.toast($scope.message, 4000);
                            });
                        }else{
                             headerFactory.deleteFavourites(data1).success(function(res){
                                $scope.userfav.favorite=false;
                                $scope.isfav = $scope.userfav.favorite;
                                $scope.image = "url('images/sprite.png') no-repeat left -4px top -362px";
                                $scope.message="favorite deleted";
                                $scope.message1 = "Add to Favorites";
                                Materialize.toast($scope.message, 4000);
                             })
                            .error(function(err){
                                $scope.message  =   "already deleted favorite";
                                Materialize.toast($scope.message, 4000);
                             });
                        }



                    }


                } else {
                    $scope.message = "please login or signup first";
                    Materialize.toast($scope.message, 4000);

                };

            });
        }
    };
}])
.directive('favDelete', ['headerFactory', '$cookieStore','$rootScope', function(headerFactory,$cookieStore, $rootScope) {
    return {
        restrict : 'EA',
        transclude : true,
        template: '{{message1}}<div class="imgFav" style="background: {{image}};  z-index:1;"></div>',
        scope: {
            userfav: "=user"
        },
        link: function($scope, element) {
            $scope.image = "";
            if (angular.isUndefined($scope.userfav)) {
                $scope.userfav = {};
                $scope.image = " url('images/like1.png') no-repeat left 1px top 0px";
                $scope.message1 = "Add to Favorites";
            } else {
                if ($scope.userfav.favorite == true) {
                    $scope.image = " url('images/like1.png') no-repeat left 1px top 0px";
                    $scope.message1 ="Delete Favorite";
                }else{
                    $scope.message1 ="Favorite Removed";
                }
            }
            
            element.bind("click", function(e){
                if ($cookieStore.get('userToken')) {
                    var data;

                    if (angular.isUndefined($scope.userfav.code)) {

                        data = {
                            id: $scope.userfav.id,
                            type: 'merchant',
                            source:'website'
                        },
                        data1 = {
                                id: $scope.userfav.favoriteId,
                                type: 'merchant',
                                source:'website'
                            }
                    } else {

                        if ($scope.userfav.code != "") {
                            data = {
                                id: $scope.userfav.id,
                                type: 'offer',
                                source:'website'
                            },
                            data1 = {
                                id: $scope.userfav.favoriteId,
                                type: 'offer',
                                source:'website'
                            }

                        } else {
                            data = {
                                id: $scope.userfav.id,
                                type: 'offer',
                                source:'website'
                            },
                            data1 = {
                                id: $scope.userfav.favoriteId,
                                type: 'offer',
                                source:'website'
                            }

                        }
                    }

                    if($scope.userfav.favorite){
                        headerFactory.deleteFavourites(data1).success(function(res){
                            $scope.userfav.favorite=false;
                            $scope.image    = "";
                            $scope.message  = "favorite deleted";
                            $scope.message1 = "Favorite Removed";
                            $rootScope.$emit("getFavList", {});
                            Materialize.toast($scope.message, 4000);
                        })
                        .error(function(err){
                            $scope.message  = "Error on delete favorite";
                            Materialize.toast($scope.message, 4000);
                        });
                    }

                } else {
                    $scope.message = "please login or signup first";
                    Materialize.toast($scope.message, 4000);
                };

            });

        }
    };
}])
.directive('favouitemer', ['headerFactory', function(headerFactory) {
    return {
        template: '<a class="hoverfav"  ng-click="FavouriteList(userfav.favorite);"  data-message="{{message}}" >' +
            '<div class="star" style="background: {{image}};  z-index:1;"></div>' +
            '</a>',
        scope: {
            userfav: "=favdata",
            ufid: "=fid",
            ufavorite: "=favorite"

        },
        controller: function($scope, $cookieStore) {

            $scope.image = "url('images/sprite.png') no-repeat left 6px top -350px";
            $scope.$watch("ufid", function(newValue, OldValue, scope) {
                if (newValue) {
                    $scope.ufid = newValue;
                }
            });

            $scope.$watch("ufavorite", function(newValue, OldValue, scope) {
                if (newValue) {
                    $scope.ufavorite = newValue;
                    if ($scope.ufavorite == true) {

                        $scope.image = " url('images/like1.png') no-repeat 10px"
                    }
                }
            });


            $scope.FavouriteList = function(isfav) {

                if ($cookieStore.get('userToken')) {
                    var data = {
                        id    : $scope.ufid,
                        type  : 'merchant',
                        source: 'website'
                    },
                    data1 = {
                        id    : $scope.userfav.favId,
                        type  : 'merchant',
                        source: 'website'
                    }

                    if (!isfav) {


                        headerFactory
                            .addFavourites(data)
                            .success(function(response) {
                                $scope.ufavorite = true;
                                $scope.datamar= response.data;
                                $scope.message = "added to favorite";
                                $scope.image = " url('images/like1.png') no-repeat 10px";
                                Materialize.toast($scope.message, 4000);
                            })
                            .error(function(err) {
                                $scope.message = "error on add to favorite";
                                Materialize.toast($scope.message, 4000);


                            });
                    } else {
                        if($scope.datamar){
                            data12={
                               id        : $scope.datamar.favoriteId,
                                type      : $scope.datamar.type,
                                source    :'website' 
                            }
                    headerFactory.deleteFavourites(data12).success(function(res){
                                    
                                     $scope.ufavorite = false;
                                    $scope.userfav.favorite=false;
                                    $scope.isfav = $scope.userfav.favorite;
                                    $scope.image = "url('images/sprite.png') no-repeat left 6px top -350px"
                                    $scope.message="favorite deleted";
                                    Materialize.toast($scope.message, 4000);
    
                                 })
                            .error(function(err){
                                     $scope.message  =   "error on delete favorite";
                                     Materialize.toast($scope.message, 4000);
    
    
                             });                                 
                                                                        }
                        else
                        {
                    headerFactory.deleteFavourites(data1).success(function(res){
                                    
                                    $scope.ufavorite = false;
                                    $scope.userfav.favorite=false;
                                    $scope.isfav = $scope.userfav.favorite;
                                    $scope.image = "url('images/sprite.png') no-repeat left 6px top -350px"
                                    $scope.message="favorite deleted";
                                    Materialize.toast($scope.message, 4000);
    
                                 })
                            .error(function(err){
                                     $scope.message  =   "error on delete favorite";
                                     Materialize.toast($scope.message, 4000);
    
    
                             });
                        }


                        


                    }


                } else {
                    $scope.message = "please login or signup first";
                    Materialize.toast($scope.message, 4000);

                }

            };


        }



    };
}])



.directive('favDlt', ['headerFactory', function(headerFactory) {
        return {
            template: '<a ng-click="Favouritedlt();" class="wish-list-icone">' + '<i class="material-icons">' + 'favorite</i>' + '</a>'


        }
    }])
    .directive("owlCarousel",[function() {
            return {
                restrict: 'E',
                transclude: false,
                link: function(scope) {
                    scope.initCarousel = function(element) {
                        var defaultOptions = {
                            items: 4,
                            navigation: false
    
                        };
                        var customOptions = scope.$eval($(element).attr('data-options'));
                        // combine the two options objects
                        for (var key in customOptions) {
                            if (customOptions.hasOwnProperty(key)) {
                                defaultOptions[key] = customOptions[key];
                            }
                        }
                        $(element).owlCarousel(defaultOptions);
                    };
                }
            };
        }])
    .directive('owlCarouselItem', ['$timeout', function($timeout) {
        return {
            restrict: 'EA',
            transclude: false,
            link: function(scope, element) {
                if (scope.$last) {
                    $timeout(function() {
                        scope.initCarousel(element.parent());
                    }, 50);
                }
            }
        };
    }])
    .controller("metaController", ['$scope', '$location', function($scope, $location) {


        $scope.metaimgpath = "http://www.cashpack.org/images/video-back.png";
        if ($location.host() == "localhost" || $location.host() == "dev02.developer24x7.com") { $scope.metaimgpath = "http://" + $location.host() + "/www-cashpack/images/video-back.png"; }
        if ($location.host() == "webtest.cashpack.org") {
            $scope.metaimgpath = "http://webtest.cashpack.org/images/video-back.png";

        }


    }]);




PopupCenter = function(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
    return newWindow;
}
