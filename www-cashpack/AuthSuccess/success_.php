<?php
	#require( 'lib/kint-master/Kint.class.php' );
	#d($_GET);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>CashPack Login Success</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<style type="text/css">
		#profileImg{
			-webkit-box-shadow: 7px 9px 51px -10px rgba(0,0,0,0.75);
			-moz-box-shadow: 7px 9px 51px -10px rgba(0,0,0,0.75);
			box-shadow: 7px 9px 51px -10px rgba(0,0,0,0.75);
		}
		/* Absolute Center Spinner */
		.loading {
		  position: fixed;
		  z-index: 999;
		  height: 2em;
		  width: 2em;
		  overflow: show;
		  margin: auto;
		  top: 0;
		  left: 0;
		  bottom: 0;
		  right: 0;
		}

		/* Transparent Overlay */
		.loading:before {
		  content: '';
		  display: block;
		  position: fixed;
		  top: 0;
		  left: 0;
		  width: 100%;
		  height: 100%;
		  background-color: rgba(0,0,0,0.3);
		}

		/* :not(:required) hides these rules from IE9 and below */
		.loading:not(:required) {
		  /* hide "loading..." text */
		  font: 0/0 a;
		  color: transparent;
		  text-shadow: none;
		  background-color: transparent;
		  border: 0;
		}

		.loading:not(:required):after {
		  content: '';
		  display: block;
		  font-size: 10px;
		  width: 1em;
		  height: 1em;
		  margin-top: -0.5em;
		  -webkit-animation: spinner 1500ms infinite linear;
		  -moz-animation: spinner 1500ms infinite linear;
		  -ms-animation: spinner 1500ms infinite linear;
		  -o-animation: spinner 1500ms infinite linear;
		  animation: spinner 1500ms infinite linear;
		  border-radius: 0.5em;
		  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
		  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
		}

		/* Animation */

		@-webkit-keyframes spinner {
		  0% {
		    -webkit-transform: rotate(0deg);
		    -moz-transform: rotate(0deg);
		    -ms-transform: rotate(0deg);
		    -o-transform: rotate(0deg);
		    transform: rotate(0deg);
		  }
		  100% {
		    -webkit-transform: rotate(360deg);
		    -moz-transform: rotate(360deg);
		    -ms-transform: rotate(360deg);
		    -o-transform: rotate(360deg);
		    transform: rotate(360deg);
		  }
		}
		@-moz-keyframes spinner {
		  0% {
		    -webkit-transform: rotate(0deg);
		    -moz-transform: rotate(0deg);
		    -ms-transform: rotate(0deg);
		    -o-transform: rotate(0deg);
		    transform: rotate(0deg);
		  }
		  100% {
		    -webkit-transform: rotate(360deg);
		    -moz-transform: rotate(360deg);
		    -ms-transform: rotate(360deg);
		    -o-transform: rotate(360deg);
		    transform: rotate(360deg);
		  }
		}
		@-o-keyframes spinner {
		  0% {
		    -webkit-transform: rotate(0deg);
		    -moz-transform: rotate(0deg);
		    -ms-transform: rotate(0deg);
		    -o-transform: rotate(0deg);
		    transform: rotate(0deg);
		  }
		  100% {
		    -webkit-transform: rotate(360deg);
		    -moz-transform: rotate(360deg);
		    -ms-transform: rotate(360deg);
		    -o-transform: rotate(360deg);
		    transform: rotate(360deg);
		  }
		}
		@keyframes spinner {
		  0% {
		    -webkit-transform: rotate(0deg);
		    -moz-transform: rotate(0deg);
		    -ms-transform: rotate(0deg);
		    -o-transform: rotate(0deg);
		    transform: rotate(0deg);
		  }
		  100% {
		    -webkit-transform: rotate(360deg);
		    -moz-transform: rotate(360deg);
		    -ms-transform: rotate(360deg);
		    -o-transform: rotate(360deg);
		    transform: rotate(360deg);
		  }
		}
	</style>
	
</head>
<body>
	<div class="loading">Loading&#8230;</div>
	<div class="details" style="display: none; text-align: center;">
		<img id="profileImg" src="" alt="">
		<div style="display: block; margin-bottom: 35px;">Name: <span id="userNm"></span></div>
		<div style="display: block;">Email: <span id="userEmail"></span></div>
	</div>
	
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="http://connect.facebook.net/en_US/all.js"></script>
	<script type="text/javascript">
		var resCode = "<?php echo $_GET['code']; ?>";
		$.ajax({
			url: 'https://graph.facebook.com/v2.3/oauth/access_token',
			type: 'GET',
			dataType: 'json',
			data: {
				client_id: '899762153462852',
				redirect_uri: 'http://webtest.cashpack.org/AuthSuccess/success_.php',
				client_secret: '02b0f556d811ad23a76eeaa7d9d461ee',
				code: resCode
			},
		})
		.done(function(res) {
			$.ajax({
				url: 'https://graph.facebook.com/debug_token',
				type: 'GET',
				dataType: 'json',
				data: {
					input_token: res.access_token,
					access_token: res.access_token
				}
			})
			.done(function(res) {
				console.log("success 2", res);

			    FB.init({
			        appId: '899762153462852',
			        status: true, // check login status
			        cookie: true, // enable cookies to allow the server to access the session
			        xfbml: true // parse XFBML
			    });

			    FB.getLoginStatus(function(response) {
			        if (response.status == 'connected') {
			            getCurrentUserInfo(response.authResponse.userID);
			        } else {
			            FB.login(function(response) {
			                if (response.authResponse) {
			                    getCurrentUserInfo(response.authResponse.userID);
			                } else {
			                    alert('Auth cancelled. Please try again.');
			                }
			            }, {
			                scope: 'email'
			            });
			        }
			    });

			    function getCurrentUserInfo(userId) {
			    	FB.api(
			            "/" + userId, {
			                fields: 'email,name,gender,picture'
			            },
			            function(response) {
			                console.info("response:", response);
			                if (response && !response.error) {
			                    document.getElementById("userEmail").innerHTML = response.email;
			                    document.getElementById("userNm").innerHTML = response.name;
			                    // /* handle the result */
			                }else{
			                	alert('Error: Unable to get the user information.');
			                }
			            }
			        );
			        
		            FB.api(
			            "/" + userId + "/picture/?type=large",
			            function(response) {
			                if (response && !response.error) {
			                	document.getElementById('profileImg').src=response.data.url;
			                    console.log("response", response);
			                }else{
			                	alert('Error: Unknown error on getting the image.');
			                }
			                $('.loading').hide();
			                $('.details').show(function(){
			                	//window.close();
			                	window.opener.accessToken(resCode);
			                	window.close();
			                });
			            }
			        );
			    }
			})
			.fail(function() {
				console.log("error");
			});
		})
		.fail(function(err) {
			console.log("error", err);
		});
	</script>
</body>
</html>