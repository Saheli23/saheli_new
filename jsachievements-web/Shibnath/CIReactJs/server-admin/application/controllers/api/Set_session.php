<?php

require APPPATH.'libraries/REST_Controller.php';

class Set_session extends REST_Controller {

	public function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == 'OPTIONS') {
			die();
		}
	}

	public function set_session_post() {

		$sessionData = array(

			'token' => trim($this->post('token')),
			'email' => trim($this->post('email'))
		);

		$setToken = array('token' => $sessionData['token']);

		$this->db->where('email', $sessionData['email']);
		$update = $this->db->update('pic_gal_signup', $setToken);

		$this->response(
			['loginSuccess' => true, 'email' => $sessionData['token'], 'update' => $update, 'where' => $where], REST_Controller::HTTP_OK
		);
	}
}