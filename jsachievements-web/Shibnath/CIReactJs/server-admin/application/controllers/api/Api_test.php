<?php
require APPPATH.'libraries/REST_Controller.php';
class Api_test extends REST_Controller {

	public function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

	}

	public function test_user_get() {

		$this->load->model('api/Test_api_model');

		$db_users = $this->Test_api_model->get_all_users();

		if ($db_users) {
			$this->response($db_users->result_array(), REST_Controller::HTTP_OK);
		} else {
			$this->response([
					'status'  => FALSE,
					'message' => 'No users were found'
				], REST_Controller::HTTP_NOT_FOUND);// NOT_FOUND (404) being the HTTP response code
		}

	}
}

?>