<?php

require APPPATH.'libraries/REST_Controller.php';

class Check_email extends REST_Controller {

	public function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == 'OPTIONS') {
			die();
		}
	}

	public function check_post() {

		$found = false;
		$email = trim($this->post('email'));

		$this->load->model('api/All_user_model');
		$response_model = $this->All_user_model->get_all_users()->result_array();

		if (count($response_model) != 0) {
			for ($i = 0; $i < count($response_model); $i++) {

				if ($response_model[$i]['email'] == $email) {
					$found = true;
					break;
				}
			}
			if ($found) {
				$this->response(
					[
						'duplicate' => true
					], REST_Controller::HTTP_OK
				);
			} else {
				$this->response(
					[
						'duplicate' => false
					], REST_Controller::HTTP_OK
				);
			}
		} else {
			$this->response(
				[
					'duplicate' => false
				], REST_Controller::HTTP_OK
			);
		}

	}
}