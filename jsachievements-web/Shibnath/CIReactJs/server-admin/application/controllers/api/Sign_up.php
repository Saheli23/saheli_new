<?php

require APPPATH.'libraries/REST_Controller.php';

class Sign_up extends REST_Controller {

	public function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == 'OPTIONS') {
			die();
		}
	}

	public function signup_post() {

		$postData = array(

			'fname'       => trim($this->post('fname')),
			'lname'       => trim($this->post('lname')),
			'email'       => trim($this->post('email')),
			'profile_pic' => trim($this->post('profile_pic')),
			'password'    => trim($this->post('password'))
		);

		$this->load->model('api/Sign_up_model');
		$model_res    = $this->Sign_up_model->saveSignupData($postData);
		$responseData = array('status' => 'ok');
		if ($model_res) {
			$this->response($responseData, REST_Controller::HTTP_OK);
		} else {
			$this->response([
					'status'  => FALSE,
					'message' => 'No users were found'
				], REST_Controller::HTTP_NOT_FOUND);// NOT_FOUND (404) being the HTTP response code
		}
	}
}