<?php

require APPPATH.'libraries/REST_Controller.php';

class User_exist extends REST_Controller {

	public function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == 'OPTIONS') {
			die();
		}
	}

	public function isExist_post() {

		$this->db->where('email', trim($this->post('email')));
		$result = $this->db->get('pic_gal_signup');
		$data   = $result->result_array();

		if (!empty($result->result_array())) {
			$this->response(['userExist' => true, 'userId' => $data[0]['user_id']], REST_Controller::HTTP_OK);
		} else {
			$this->response(['userExist' => false], REST_Controller::HTTP_OK);
		}

		if (FALSE) {
			$this->response([
					'status'  => FALSE,
					'message' => 'No users were found'
				], REST_Controller::HTTP_NOT_FOUND
			);// NOT_FOUND (404) being the HTTP response code
		}

	}
}
