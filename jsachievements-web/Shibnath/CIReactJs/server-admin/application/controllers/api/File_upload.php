<?php

require APPPATH.'libraries/REST_Controller.php';

class File_upload extends REST_Controller {

	public function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == 'OPTIONS') {
			die();
		}
	}

	public function file_upload_post() {

		$config['upload_path']   = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		//$config['file_name']     = "testname.jpg";
		/*$config['max_size']      = '100';
		$config['max_width']     = '1366';
		$config['max_height']    = '768';*/

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('img')) {
			$error = array('error' => $this->upload->display_errors());

			$errRes = array(
				'status' => "Error",
				'userId' => $this->post('userId')
			);

			$this->response($errRes, REST_Controller::HTTP_OK);

		} else {
			$data = array('upload_data' => $this->upload->data());
			//Renaming the file
			$old_name_with_path = $data['upload_data']['full_path'];
			$only_path          = $data['upload_data']['file_path'];
			$file_ext           = $data['upload_data']['file_ext'];

			$userId             = $this->post('userId');
			$new_file_name      = $userId.'@'.$data['upload_data']['file_name'];
			$new_name_with_path = $only_path.$new_file_name;

			rename($old_name_with_path, $new_name_with_path);

			$uploadRes = array(
				'uploadStatus' => true,
				'file_info'    => $data['upload_data'],
				'duplicate'    => true,
			);

			$pic_upload_db_data = array(

				'pic_id'       => $new_file_name,
				'name'         => $data['upload_data']['file_name'],
				'size'         => $data['upload_data']['file_size'],
				'user_id'      => $userId,
				'fb_share'     => '0',
				'tw_share'     => '0',
				'google_share' => '0',
			);

			$this->load->model('api/Get_all_photos');
			$all_photos = $this->Get_all_photos->getAllPhotos($userId);

			$k = 0;
			foreach ($all_photos->result_array() as $row) {
				if (strcmp($row['pic_id'], $new_file_name) == 0) {
					$k = 1;
					break;
				}
			}

			if ($k != 0) {
				$uploadRes['duplicate'] = true;
				$this->response($uploadRes, REST_Controller::HTTP_OK);

			} else {
				$this->load->model('api/Add_photos');
				$model_res = $this->Add_photos->addNewPhotos($pic_upload_db_data);

				if ($model_res) {
					$uploadRes['duplicate'] = false;
					$this->response($uploadRes, REST_Controller::HTTP_OK);
				} else {
					$this->response([
							'uploadStatus' => false,
							'message'      => 'Something went wrong while file uploading...'
						], REST_Controller::HTTP_NOT_FOUND);// NOT_FOUND (404) being the HTTP response code
				}
			}

		}

	}
}