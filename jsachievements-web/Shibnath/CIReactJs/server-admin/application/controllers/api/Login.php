<?php

require APPPATH.'libraries/REST_Controller.php';

class Login extends REST_Controller {

	public function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == 'OPTIONS') {
			die();
		}
	}

	public function login_post() {

		$email = trim($this->post('email'));
		$pass  = trim($this->post('password'));

		$match = false;

		$this->load->model('api/All_user_model');
		$response_model = $this->All_user_model->get_all_users()->result_array();

		if (count($response_model) > 0) {
			for ($i = 0; $i < count($response_model); $i++) {
				if ($response_model[$i]['email'] == $email) {
					if ($response_model[$i]['password'] == $pass) {
						$match = true;
						break;
					}
				}
			}
			if ($match) {
				$this->response(
					['loginSuccess' => true, ], REST_Controller::HTTP_OK
				);
			} else {
				$this->response(
					['loginSuccess' => false, ], REST_Controller::HTTP_OK
				);
			}
		} else {
			$this->response(
				['loginSuccess' => false, ], REST_Controller::HTTP_OK
			);
		}

	}
}