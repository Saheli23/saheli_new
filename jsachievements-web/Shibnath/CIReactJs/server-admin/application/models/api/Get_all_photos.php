<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get_all_photos extends CI_Model {

	public function getAllPhotos($userId) {

		$this->db->where('user_id', $userId);
		$allPhotos = $this->db->get('pic_gal_photos');
		return $allPhotos;
	}

}
