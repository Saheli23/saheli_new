<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_photos extends CI_Model {

	public function addNewPhotos($pic_upload_db_data) {

		if ($this->db->insert('pic_gal_photos', $pic_upload_db_data)) {
			return true;
		} else {

			return false;
		}
	}

}
