<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sign_up_model extends CI_Model {

	public function saveSignupData($postData) {

		if ($this->db->insert('pic_gal_signup', $postData)) {
			return true;
		} else {

			return false;
		}
	}

}
