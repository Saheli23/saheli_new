import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './jsx/App.jsx'
import registerServiceWorker from './registerServiceWorker';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

//React router
import {BrowserRouter} from 'react-router-dom';

//meterial MUI initializer

//React Redux
import {createStore,applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk'
import IndexReducer from './jsx/reducers/index.jsx'

let middleware=[thunk];

middleware=[...middleware];


class IndexApp extends React.Component{

	render(){

		const store=createStore(IndexReducer,
			applyMiddleware(...middleware)
		);

		return(
			<Provider store={store}>
			{/*/jsachievements-web/Shibnath/CIReactJs/picture-gallery-app/build/#/  during git push*/}
				<BrowserRouter basename="/">
					<MuiThemeProvider>
						<App/>		
					</MuiThemeProvider>
				</BrowserRouter>
			</Provider>
		)
	}
}

ReactDOM.render(<IndexApp />, document.getElementById('root'));
registerServiceWorker();
