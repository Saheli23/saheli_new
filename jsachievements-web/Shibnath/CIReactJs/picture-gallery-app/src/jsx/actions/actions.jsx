import userIdExistApi from '../api/userId_exist.jsx'

export function isSidebarOpen(status) {
	
  return{
    type:'SET_SIDEBAR',
    payload:!status
  }
}

export function userIdExist(email,userId) {
  
  return dispatch=>{

  	userIdExistApi(email,userId).then((response)=>{
  		
  		dispatch({
  			type:'USER_ID_EXIST',
  			payload:response
  		})
  	})

  }
}
