import React from 'react';

import Paper from 'material-ui/Paper'

class Container extends React.Component{

	render(){

		const style={
			marginTop:130,
			marginBottom:50,
			marginRight:40,
			height:this.props.height,
			width:this.props.width,
			padding:35
		}

		return(
			
			<div align={this.props.align}>
				<Paper zDepth={2} style={style}>
						{
							this.props.children
						}

				</Paper>
			</div>
			
		)
	}
}

export default Container;