import React from 'react';

//Material-ui components
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import NavigationArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import {List, ListItem} from 'material-ui/List';

//Material-ui Icons
import ActionFlipToBack from 'material-ui/svg-icons/action/flip-to-back'
import ContentBackspace from 'material-ui/svg-icons/content/backspace';

//Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {isSidebarOpen} from '../../actions/actions.jsx'

class Sidebar extends React.Component{

	constructor(props){
		super(props);
		this.state={
			open:false
		}
		
	}


	openMenu(){
		this.props.isSidebarOpen(this.props.sidebarStatus)
	}

	render(){

		const style={
			
			rightIconAppbar:{
				color:'rgb(255, 255, 255)',
				marginTop:12,
				marginRight:5,
				cursor: 'pointer'

			}
				

		}
		return(

			<div>
				<Drawer 
					docked={false}
          			width={200}
					open={this.props.sidebarStatus}
					
				>
		          <AppBar 
		          	title="AppBar" 
		          	iconElementLeft={
		          		<ContentBackspace 
			          		style={style.rightIconAppbar} 
		          		/>
		          	}
		          	onLeftIconButtonClick={this.openMenu.bind(this)}

		          />
		        </Drawer>
			</div>
		)
	}
}

function mapDispatchToProps(dispatch) {
	
	return bindActionCreators({
		isSidebarOpen:isSidebarOpen	
	},dispatch)
}

function mapStateToProps(state){

	return{
		sidebarStatus:state.sidebarStatus
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(Sidebar);