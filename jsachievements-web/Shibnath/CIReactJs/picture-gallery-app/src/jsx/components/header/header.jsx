import React from 'react';

import AppBar from 'material-ui/AppBar';

import Sidebar from '../sidebar/sidebar.jsx'

//Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {isSidebarOpen} from '../../actions/actions.jsx'

class Header extends React.Component{
	
	constructor(props){
		super(props);
		this.state={
			open:false,
			reloadRender:false
		}
	}

	openMenu(){
		this.props.isSidebarOpen(this.state.open)
	}

	render(){

		return(
			<div>
				<AppBar 
					title={<b>Picture Gallery</b>} 
					style={{position:'fixed',top:0}} 
					iconElementRight={this.props.rightIcon}
					onLeftIconButtonClick={this.openMenu.bind(this)}
				>
			    </AppBar>
			    <Sidebar/>
			</div>
		)
	}
}


function mapStateToProps(state){

	return{
		sidebarStatus:state.sidebarStatus
	}
}


function mapDispatchToProps(dispatch) {
	
	return bindActionCreators({
		isSidebarOpen:isSidebarOpen	
	},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Header);