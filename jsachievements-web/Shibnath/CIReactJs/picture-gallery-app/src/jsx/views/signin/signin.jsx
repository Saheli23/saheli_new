import React from 'react';
import Container from '../../components/container/container.jsx';

//material-ui components 
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import CircularProgress from 'material-ui/CircularProgress';

//Material-ui form validator
import { ValidatorForm } from 'react-form-validator-core';
import { TextValidator} from 'react-material-ui-form-validator';
import FlatButton from 'material-ui/FlatButton';
import SocialPersonAdd from 'material-ui/svg-icons/social/person-add'

//APIs
import signin from '../../api/signin.jsx';

import {Redirect,Link} from 'react-router-dom'

var randomstring = require("randomstring")

class SignIn extends React.Component{

	constructor(props){
		super(props);
		this.state={
				
			user: {},
			wrngPass:false,
			showLoader:false,
			redirect:false
		}
		this.handleChange = this.handleChange.bind(this);
	}

	componentWillMount(){
		if(typeof(Storage)!==undefined){
			localStorage.removeItem("userName")
			localStorage.removeItem("userId");

		}
	}

	handleChange(event) {
        const { user } = this.state;
        user[event.target.name] = event.target.value;
        this.setState({ user });
    }

    handleSubmit(){
    	
    	this.setState({
    		showLoader:true
    	})
    	signin(this.state.user).then((response)=>{
    	
    		if(!response.loginSuccess){
    			this.setState({
    				wrngPass:true,
    				showLoader:false
    			})
    		}
    		else{
    			
				if(typeof(Storage)!=="undefined"){
					localStorage.setItem("userName",this.state.user.email)
				}
    			
    			this.setState({
    				redirect:true,
    				showLoader:false
    			})
    			
    		}
    	})

    }

	render(){
		const { user } = this.state;
		const style={
			
			layout:{
				width:450,
				height:'auto',
				align:'center',
				
			},
			font:{
				fontSize: 30,
			},
			flateBtnLabel:{
				color:'rgb(68, 133, 244)',
				fontWeight:'bold'
			}
		}

		const emailReqErr=(<div style={{float:'left',color:'#d63a00'}}>Enter an email</div>)
		const passReqErr=(<div style={{float:'left',color:'#d63a00'}}>Enter account password</div>)
		const wrngPassMsg=(<div style={{float:'left',color:'#d63a00'}}>Wrong password or invalid email. Try again.</div>)

		return(
			<Container {...style.layout}>
				{
					this.state.redirect?( <Redirect to="/"/>):null
				}
				<div>
					<img src="./icons/root-icon4.ico" height="65" width="65" />
				</div>
				<div>
					<div style={style.font}>Sign in</div>	
				</div>
				<br/>
				<br/>
				
				<ValidatorForm
			        onSubmit={this.handleSubmit.bind(this)}
			    >
					<TextValidator
				                    
				    	onChange={this.handleChange}
				    	name="email"
				    	type="email"
				    	validators={['required']}
				    	errorMessages={[emailReqErr]}
				    	value={user.email}
				    	floatingLabelText="Enter your Email"
				    	fullWidth={true}
					/>
					<br/>
					<TextValidator
				    	floatingLabelText="Enter your Password"
				    	onChange={this.handleChange}
				    	name="password"
				    	type="password"
				    	validators={['required']}
				    	errorMessages={[passReqErr]}
				    	value={user.password}
				    	fullWidth={true}
				    	errorText={this.state.wrngPass?(wrngPassMsg):null}
					/>
					<br/>
					<br/>
					<br/>
					<RaisedButton type="submit" label={<b style={{color:'white'}}>Sign In</b>} backgroundColor='#4485f4'/>

					<br/>
			                <br/>
			                <br/>
			                <br/>
			                {
			                	this.state.showLoader?( 
			                		
			                		<Dialog
							          
							          modal={true}
							          open={true}
							        >
							          <div align="center">
							          	<CircularProgress size={90} thickness={6}/>
							          </div>
							        </Dialog>

			                	):null
			                }
				</ValidatorForm>
				<div>
					<Link to="/signup">
						<FlatButton
						  label="Create Account"
						  labelPosition="after"
					      icon={<SocialPersonAdd color={'rgb(68, 133, 244)'}/>}
					      labelStyle={style.flateBtnLabel}
					    />
				    </Link>
				</div>
				

			</Container>
		)
	}
}

export default SignIn;