import React from 'react';
import Container from '../../components/container/container.jsx'

//matrial-ui components
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
//import Avatar from 'material-ui/Avatar'
import RaisedButton from 'material-ui/RaisedButton'
//import Divider from 'material-ui/Divider';
import CircularProgress from 'material-ui/CircularProgress'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton';
import SocialPersonAdd from 'material-ui/svg-icons/social/person-add';
import ActionLock from 'material-ui/svg-icons/action/lock'

//React Router
import {Link,Redirect} from 'react-router-dom'

//Material-ui form validator
import { ValidatorForm } from 'react-form-validator-core';
import { TextValidator} from 'react-material-ui-form-validator';
//APIs
import signup from '../../api/signup.jsx';
import check_email from '../../api/check_email.jsx';


class SignUp extends React.Component{

	constructor(props){
			super(props);
			this.state={
				err:false,
				conPassVer:false,
				user: {},
				showLoader:false,
				redirect:false,
				emailExist:false,
				formValid:false
			}
			this.handleChange = this.handleChange.bind(this);
	}

	componentWillMount() {
        // custom rule will have name 'isPasswordMatch'
        ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
            if (value !== this.state.user.password) {
                return false;
            }
            return true;
        });

      
    }

    handleChange(event) {
        const { user } = this.state;
        user[event.target.name] = event.target.value;
        this.setState({ user });
        
        if(event.target.name=='email'){
        	this.setState({ 		
	            formValid:false
	        })
        	check_email(event.target.value).then((response)=>{
            	
	            if (response.duplicate) {
	            	this.setState({
	            		emailExist:true,
	            		formValid:false
	            	})
	            }
	            else{
	            	this.setState({
	            		emailExist:false,
	            		formValid:true
	            	})
	            }
            	
            })
        }

    }

	handleSubmit(event) {
    	event.preventDefault();
    	
    	if (this.state.formValid) {
    		this.setState({
    			showLoader:true
    		})
	    	signup(this.state.user).then((response)=>{
	    		
		    	this.setState({
		    		showLoader:false,
		    		redirect:true
	    		})
	    	})
    	}
    	
    	
        //window.location="https://www.google.co.in";
    }


	render(){

		const { user } = this.state;

		const style={
			
			layout:{
				width:450,
				height:'auto',
				align:'center'
			},

			font:{
				fontSize: 30,
			},
			flateBtnLabel:{
				color:'rgb(68, 133, 244)',
				fontWeight:'bold'
			}

		}
		const fieldReqErr=(<div style={{float:'left'}}>This field is required</div>)
		const validEmailErr=(<div style={{float:'left'}}>Enter a valid email address</div>)
		const passMissMatchErr=(<div style={{float:'left'}}>Password missmatched</div>)
		const emailExistErr=(<div style={{float:'left'}}>Email address already exist, Please with another email</div>)

		return(

			<Container {...style.layout} >
				{
					this.state.redirect?( <Redirect to="/signin"/>):null
				}

				<div align='center'>	
					<div>
						<img src="./icons/root-icon4.ico" height="65" width="65" />
					</div>
					<div>
						<div style={style.font}>Sign up</div>
						
					</div>
					<br/>
					<br/>
					<div>
						<ValidatorForm
			                onSubmit={this.handleSubmit.bind(this)}
			            >	
			            	<TextValidator
			                    
			                    onChange={this.handleChange}
			                    name="fname"
			                    type="text"
			                    validators={['required']}
			                    errorMessages={[fieldReqErr]}
			                    value={user.fname}
			                    floatingLabelText="First Name"
			                    fullWidth={true}
			                />
			                <br/>
			                <TextValidator
			                    floatingLabelText="Last Name"
			                    onChange={this.handleChange}
			                    name="lname"
			                    type="text"
			                    validators={['required']}
			                    errorMessages={[fieldReqErr]}
			                    value={user.lname}
			                    fullWidth={true}
			                />
			                <br/>
			                <TextValidator
			                    floatingLabelText="Email"
			                    onChange={this.handleChange}
			                    name="email"
			                    type="email"
			                    validators={['required','isEmail']}
			                    errorMessages={[fieldReqErr,validEmailErr]}
			                    value={user.email}
			                    fullWidth={true}
			                    errorText={this.state.emailExist ? emailExistErr :null}
			                />
			                <br/>
			                <TextValidator
			                    floatingLabelText="Password"
			                    onChange={this.handleChange}
			                    name="password"
			                    type="password"
			                    validators={['required']}
			                    errorMessages={[fieldReqErr]}
			                    value={user.password}
			                    fullWidth={true}
			                /><br/>
			                <TextValidator
			                    floatingLabelText="Confirm password"
			                    onChange={this.handleChange}
			                    name="repeatPassword"
			                    type="password"
			                    validators={['required','isPasswordMatch']}
			                    errorMessages={[fieldReqErr,passMissMatchErr]}
			                    value={user.repeatPassword}
			                    fullWidth={true}
			                /><br/>
			                <br/>
			                <br/>
			                <RaisedButton type="submit" label={<b style={{color:'white'}}>Sign Up</b>} backgroundColor='#4485f4' />

			                <br/>
			                <br/>
			                <br/>
			                <br/>
			                {
			                	this.state.showLoader?( 
			                		
			                		<Dialog
							          
							          modal={true}
							          open={true}
							        >
							          <div align="center">
							          	<CircularProgress size={90} thickness={8}/>
							          </div>
							        </Dialog>

			                	):null
			                }

			            </ValidatorForm>
					</div>
				</div>
				
				<div>
					<Link to="/signin">
						<FlatButton
						  label="Sign in"
						  labelPosition="after"
					      icon={<ActionLock color={'rgb(68, 133, 244)'}/>}
					      labelStyle={style.flateBtnLabel}
					    />
				    </Link>
				</div>
			</Container>
		)
	}
}

export default SignUp;