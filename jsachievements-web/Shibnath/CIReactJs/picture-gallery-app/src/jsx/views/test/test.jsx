import React from 'react';
import ReactDOM from 'react-dom';
import Container from '../../components/container/container.jsx';
import {Link} from 'react-router-dom'

class Test extends React.Component{

	render(){

		let layout = {
			height:'auto',
			width:450
		}

		return(
			<Container {...layout}>
				<div>
					<h1>Test Module</h1>
				</div>
			</Container>
		)
	}
}

export default Test;