import React from 'react';
import Container from '../components/container/container.jsx';
import Header from '../components/header/header.jsx';

import CircularProgress from 'material-ui/CircularProgress';

import {Redirect} from 'react-router-dom'

//API
import userExist from '../api/user_exist.jsx'



class Index extends React.Component{

	constructor(props){
		super(props);
		this.state={
			redirect:false,
			shwLoader:true,
			redirectTo:""
		}
	}

	componentDidMount(){
		if(typeof(Storage)!=="undefined"){
			if(localStorage.getItem("userName")!==null){
				
				var email=localStorage.getItem("userName");
				userExist(email).then((response)=>{
					//console.log(response)
					if(response.userExist){

						localStorage.setItem("userId",response.userId)

						this.setState({
							redirect:true,
							redirectTo:'/photos'
						})
					}
					else{
						this.setState({
							redirect:true,
							redirectTo:'/signin'
						})
					}
				})
				
			}
			else{
				this.setState({
					redirect:true,
					redirectTo:'/signin'
				})
			}
		}
	}

	render(){

		const style={
			
			layout:{
				width:450,
				height:'auto',
				align:'center',
				
			},
			font:{
				fontSize: 30,
			}
		}
		return(
			<div>
				<Header/>
				<Container {...style.layout}>
					{
						this.state.redirect?( <Redirect to={this.state.redirectTo} />):null
					}

					{
						this.state.shwLoader?(
							<div align="center">
								<CircularProgress size={80} thickness={6}/>
							</div>
						):null
					}

				</Container>
			</div>

		)
	}
}

export default Index;