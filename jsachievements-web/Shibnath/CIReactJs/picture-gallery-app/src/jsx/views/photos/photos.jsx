import React from 'react';
import Container from '../../components/container/container.jsx';
import Header from '../../components/header/header.jsx';

//material-ui components 
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import CircularProgress from 'material-ui/CircularProgress';
import FlatButton from 'material-ui/FlatButton';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';

//Material-ui Icons
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import FileFileUpload from 'material-ui/svg-icons/file/file-upload';
import FileCloudUpload from 'material-ui/svg-icons/file/cloud-upload'

//Material-ui form validator
import { ValidatorForm } from 'react-form-validator-core';
import { TextValidator} from 'react-material-ui-form-validator';

import {Redirect} from 'react-router-dom'
//Actions and Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {isSidebarOpen,userIdExist} from '../../actions/actions.jsx';

//Material-UI colors 
import {teal500,teal600,lightBlue600} from 'material-ui/styles/colors';

class Photos extends React.Component{
	
	constructor(props){
		super(props);
		this.state={
			render:false,
			signOut:false,
			showLoader:false,
			dialogOpenState:false
		}
	}

	componentWillMount(){
		if(typeof(Storage)!==undefined){

			var email=localStorage.getItem("userName");
			var userId=parseInt(localStorage.getItem("userId"));

			this.props.userIdExist(email,userId);
			
		}
	}

	signOut(){

		this.setState({
			showLoader:true
		})
		
		setTimeout(function(){
			if(typeof(Storage)!==undefined){
				localStorage.removeItem("userName")
				localStorage.removeItem("userId");			
			}
			this.setState({
				showLoader:false,
				signOut:true
			})
		}.bind(this),3000)
		

	}

	dialogOpen(){
		this.setState({
			dialogOpenState:!this.state.dialogOpenState
		})
		
	}
	
	render(){
		
		const redirectTag=(
			this.props.userIdExistStatus===null ? (<CircularProgress size={80} thickness={6} />):(
				!this.props.userIdExistStatus.userExist ? (
					<Redirect to='/signin' />
				):null
			)
		)

		const style={
			
			layout:{
				width:'75%',
				height:'auto',
				align:'center'
				
			},
			font:{
				fontSize: 30,
			},
			hideInputFile: {
			    cursor: 'pointer',
			    position: 'absolute',
			    top: 0,
			    bottom: 0,
			    right: 0,
			    left: 0,
			    width: '100%',
			    opacity: 0,
			},
			RaisedButton:{
				
			},
			modal:{

				body:{

					backgroundColor:teal500
				},
				title:{

					backgroundColor:teal500,
					color:'white',
					fontSize:30
				},
				content:{

					width:550
				},
				textAreas:{

					marginTop:200
				}
			},
			IconButton:{
				largeIcon: {
				    width: 60,
				    height: 60,
				},
				large: {
				    width: 120,
				    height: 120,
				    padding: 30,
				}
			}

		}

		const rightIcon=(
			<div>
				<RaisedButton 
					label={<b style={{color:'rgb(0, 188, 212)'}}>Add Photo</b>}
					containerElement="label"
					onClick={this.dialogOpen.bind(this)}
				/>
				

				<IconMenu
			    
				    iconButtonElement={
				      <IconButton><MoreVertIcon /></IconButton>
				    }
			    
			   >
			    <MenuItem primaryText="Profile" />
			    <MenuItem primaryText="Sign out" onClick={this.signOut.bind(this)} />
			  </IconMenu>
				
			</div>
		)

		const actions=[
			<FlatButton
		        label="Cancel"
		        primary={true}
		        onClick={this.dialogOpen.bind(this)}
		    />
		]

		return(

			<div>
				<Header rightIcon={rightIcon}/>
				<Container {...style.layout}>

					{
						this.state.signOut ? (<Redirect to='/'/>):null
					}
					{
						this.state.showLoader ? (<CircularProgress size={80} thickness={6} />):null
					}
					{redirectTag}					
					

				</Container>

				<Dialog
		          title={<h4>Add New Photo</h4>}
		          actions={actions}
		          modal={true}
		          open={this.state.dialogOpenState}
		          contentStyle={style.modal.content}
				  titleStyle={style.modal.title}
		        >
		          <div align="center" style={{marginTop:35}}>

		          	<form>

		          		<IconButton 
		          			tooltip="Choose Photo from your devices" touch={true} tooltipPosition="bottom-center"
		          			iconStyle={style.IconButton.largeIcon}
      						style={style.IconButton.large}
		          		>
					      <FileCloudUpload/>
					      
					    </IconButton>

					    <br/>

			          	<RaisedButton 
							label={<b style={{color:'rgb(0, 188, 212)'}}>Upload Photo</b>}
							containerElement="label"
						>
							<input type="file" name="file" style={style.hideInputFile}/>
						</RaisedButton>


					</form>
		          </div>
		        </Dialog>

			</div>
		)
	}
}


function mapStateToProps(state){

	return{
		sidebarStatus:state.sidebarStatus,
		userIdExistStatus:state.userIdExistStatus
	}
}


function mapDispatchToProps(dispatch) {
	
	return bindActionCreators({
		isSidebarOpen:isSidebarOpen,
		userIdExist:userIdExist
	},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Photos);