import React from 'react';
import axios from 'axios';

export default function(signInData){

	return new Promise((resolve,reject)=>{
		axios({
			url:'http://localhost/jsachievements-web/Shibnath/CIReactJs/server-admin/index.php/api/Login/login',
			method:'post',
			data:{
				email:signInData.email,
				password:signInData.password
			}
		})
		.then((response)=>{
			resolve(response.data);

		})
		.then((error)=>{
			reject(error);
		})
	})

}