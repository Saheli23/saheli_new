import React from 'react';
import axios from 'axios';

export default function(signUpFormData){

	var signupData={
		fname:signUpFormData.fname,
		lname:signUpFormData.lname,
		email:signUpFormData.email,
		profile_pic:'',
		password:signUpFormData.password,
		user_id:'',
		token:''
	}

	return new Promise((resolve,reject)=>{
		axios({
			url:'http://localhost/jsachievements-web/Shibnath/CIReactJs/server-admin/index.php/api/Sign_up/signup',
			method:'post',
			data:signupData
		})
		.then((response)=>{
			resolve(response.data);
		})
		.then((error)=>{
			reject(error);
		})
	})

}