import {combineReducers} from 'redux';

import sidebarStatus from './sidebar_status.jsx'
import userIdExistReducer from './userId_exist.jsx'

const IndexReducer=combineReducers({

	sidebarStatus:sidebarStatus,
	userIdExistStatus:userIdExistReducer
})

export default IndexReducer;