import React, { Component } from 'react';

//Components 
import Header from './components/header/header.jsx';

//Views
import SignUp from './views/signup/signup.jsx';

//React Router
import {AllRoutes} from './routes/routes.jsx'

class App extends Component{

  render(){
    return(
    	
      <div>
      	{AllRoutes}
      	
      </div>
      
    )
  }

}

export default App;
