import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Success from '../test_modules/success.jsx';
import Test from '../views/test/test.jsx';
import SignUp from '../views/signup/signup.jsx';
import SignIn from '../views/signin/signin.jsx';
import Photos from '../views/photos/photos.jsx';
import Index from '../views/index.jsx'

export const AllRoutes=(

	<Switch>
      	<Route exact path='/' component={Index}/>
      	<Route path='/test' component={Test}/>
      	<Route path='/success' component={Success}/>
      	<Route path='/signin' component={SignIn}/>
      	<Route path='/signup' component={SignUp}/>
      	<Route path='/photos' component={Photos}/>			
    </Switch>
)