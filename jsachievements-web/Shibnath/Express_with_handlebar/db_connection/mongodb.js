var async = require('async');
var MongoClient = require('mongodb').MongoClient;

module.exports={

  connect: function( callback ) {
    MongoClient.connect( "mongodb://admin:admin@ds249727.mlab.com:49727/new_demo_user", function( err, db ) {

      if (err) {
	  	console.log("Error in DB connection");
	  	console.log(err, db);
	  }else{
		console.log("Successfully connected!!!")
	  }
      db_export = db;
      return callback( err );
    });
  },

  getDb: function() {
    return db_export;
  }
}