var mongoose=require('mongoose');

var Schema=mongoose.Schema;

var EmpSchema=new Schema({
	id:String,
	name:String,
	joined:Date
});


module.exports=mongoose.model('Emp',EmpSchema);