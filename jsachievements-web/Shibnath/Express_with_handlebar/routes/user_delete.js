var express=require('express');
var router=express.Router();

var mongodb=require('../db_connection/mongodb');

router.get('/user/delete/:email',function(req,res,next) {
	mongodb.connect(function(err){
		var db=mongodb.getDb();
		var myDB=db.db('new_demo_user');
		var myCollection=myDB.collection('first_db');

		myCollection.deleteMany({ 
		   email:req.params.email
		})
		.then(function(result) {
		  res.redirect('/mongodb')
		})
		
	})
})

module.exports=router;