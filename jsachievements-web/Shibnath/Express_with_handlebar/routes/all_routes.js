//-----------------------Routes importing---------------------------
var indexRoute=require('./index.js')
var regRoute=require('./reg.js');
var formSubmit=require('./reg_form_submit.js');
var fileUpload=require('./file_upload');
var fileUploadSubmit=require('./file_upload_submit');
var email=require('./email');
var email_sent=require('./email_sent');
var mongodb=require('./mongodb.js')
var mongodb_form_submit=require('./mongodb_form_submit')
var delete_user=require('./user_delete.js')
var user_update=require('./user_update.js')
var user_update_submit=require('./user_update_submit')
var test=require('./test');
var mongojs=require('./mongojsTest.js')
var mongoose=require('./mongoose.js')

//------------------------------------------------------------------
var express=require('express');
var app=express();

 app.get('/',indexRoute)
	.get('/reg',regRoute)
	.get('/test',test)
	.post('/reg/submit',formSubmit)
	.get('/upload',fileUpload)
	.post('/upload/submit',fileUploadSubmit)
	.get('/email',email)
	.post('/email/sent',email_sent)
	.get('/mongodb',mongodb)
	.post('/mongodb/form/submit',mongodb_form_submit)
	.get('/user/delete/:email',delete_user)
	.get('/update/:email',user_update)
	.post('/user/update/submit',user_update_submit)
	.get('/mongojs',mongojs)
	.get('/mongoose',mongoose)

module.exports=app;