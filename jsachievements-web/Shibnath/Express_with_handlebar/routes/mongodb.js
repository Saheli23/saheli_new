var express=require('express');
var router=express.Router();
var mongodb=require('../db_connection/mongodb');

router.get('/mongodb',function (req,res,next) {
	
	mongodb.connect(function(err){
		var db=mongodb.getDb();
		var myDB=db.db('new_demo_user');
		var myCollection=myDB.collection('first_db');
		myCollection.find().toArray(function(err,result){
			if(err){
				console.log("Error in data reading.....")
			}
			res.render('mongodb',{title:"MongoDB",body:"MongoDB Test",usersData:result});
		})
	})

})

module.exports=router;