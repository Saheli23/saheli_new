var express=require('express');
var router=express.Router();
var mongodb=require('../db_connection/mongodb');

router.get('/update/:email',function(req,res) {

	mongodb.connect(function(err){
		var db=mongodb.getDb();
		var myDB=db.db('new_demo_user');
		var myCollection=myDB.collection('first_db');
		myCollection.find({email:req.params.email}).toArray(function(err,result){
			if(err){
				console.log("Error in data reading.....")
			}
			res.render('user_update',{title:"MongoDB User update",body:"MongoDB user update demo",usersData:result});
		})
	})
})

module.exports=router;