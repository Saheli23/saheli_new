var express=require('express');
var router=express.Router();
var mongodb=require('../db_connection/mongodb');

router.post('/mongodb/form/submit',function(req,res,next) {
	mongodb.connect(function(err){
		var db=mongodb.getDb();
		var myDB=db.db('new_demo_user');
		var myCollection=myDB.collection('first_db');
		var regData={
			fname:req.body.fname,
			lname:req.body.lname,
			email:req.body.email
		}
		myCollection.insert(regData,function (err,result) {
			if(err){
		    	console.log("Error in data insertion...")
		    	console.log(err)
		    }	
		    else{
		    	console.log("entry saved");
		    	res.redirect('/mongodb');
		    	//console.log(result)
		    }
		})

		db.close();
	})
})

module.exports=router;