var express=require('express');
var router=express.Router();
var mongodb=require('../db_connection/mongodb');

router.post('/user/update/submit',function(req,res) {

	mongodb.connect(function(err){
		var db=mongodb.getDb();
		var myDB=db.db('new_demo_user');
		var myCollection=myDB.collection('first_db');

		var updateData={
			fname:req.body.fname,
			lname:req.body.lname,
			email:req.body.email
		}

		myCollection.update(
		  { email: req.body.email },
		  { $set: updateData })
		.then(function(result) {
		  res.redirect('/mongodb')
		})
	})
})

module.exports=router;