var express=require('express');
var router=express.Router();
var os=require('os')
console.log(os.type()); 
console.log(os.release()); 
console.log(os.platform());
console.log(os.totalmem())
console.log("--------------------------------------------------------------")
const dns = require('dns');

dns.lookup('iana.org', (err, address, family) => {
  console.log('address: %j family: IPv%s', address, family);
});

dns.resolve4('www.google.com', (err, addresses) => {
  if (err) throw err;

  console.log(`addresses: ${JSON.stringify(addresses)}`);

  addresses.forEach((a) => {
    dns.reverse(a, (err, hostnames) => {
      if (err) {
        throw err;
      }
      console.log(`reverse for ${a}: ${JSON.stringify(hostnames)}`);
    });
  });
});
router.get('/test',function(req,res,next) {

	//os.arch(): x64 OS arch bit
	//os.cpus()

	res.send("<h2>"+os.type()+"  /  "+os.release()+"  /  "+os.platform()+"</h2>")
})

module.exports=router;

//------------------OS module---------------------

