var express=require('express');
var hbs=require('express-handlebars');
var path=require('path');
var bodyParser=require('body-parser');
var net=require('net');

var app=express();

var allRoutes=require('./routes/all_routes.js')

app.engine('hbs',hbs({
	extname:'hbs',
	defaultLayout:'layout',
	layoutsDir:__dirname+'/views/layouts',
	partialsDir: __dirname + '/views/partials',
}))

app.set('views',path.join(__dirname,'/views'));

app.set('view engine','hbs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}))
//app.use(express.static(path.join(__dirname,'public')))

app.use(allRoutes)

//---------------Net package------------------------
const server = net.createServer((socket) => {
  socket.end('goodbye\n');
}).on('error', (err) => {
  // handle errors here
  throw err;
});
server.listen(() => {
  console.log('opened server on', server.address());
});

//------------------Setting Port--------------------
app.listen(2000)