import React from 'react';
import ReactDOM from 'react-dom';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';//for dark theme
import {pinkA100} from 'material-ui/styles/colors';//custom color
//Appbar
import AppBar from 'material-ui/AppBar'
//Drawer(Sidebar)
import Drawer from 'material-ui/Drawer';
//List
import {List,ListItem} from 'material-ui/List'
//Icons
import CommunicationContacts from 'material-ui/svg-icons/communication/contacts'//contacts
import MapsMap from 'material-ui/svg-icons/maps/map'//maps
import SocialPersonAdd from 'material-ui/svg-icons/social/person-add'//add person
import ActionInfo from 'material-ui/svg-icons/action/info'
import ActionDelete from 'material-ui/svg-icons/action/delete';//delete
import ContentRemove from 'material-ui/svg-icons/content/remove';//Remove

//Avatar
import Avatar from 'material-ui/Avatar';
//Paper
import Paper from 'material-ui/Paper';
//Grid
import {GridList, GridTile} from 'material-ui/GridList';


import injectTapEventPlugin from 'react-tap-event-plugin';

//Customizing the theme
import muiThemeable from 'material-ui/styles/muiThemeable';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
//importing color from material-ui directory
import {blue500,greenA400,green500,teal500} from 'material-ui/styles/colors'
import Test from './test-files/test-jsx.jsx'

/*
	const muiTheme = getMuiTheme({
	  palette: {
	    textColor: cyan500,
	  },
	  appBar: {
	    height: 50,
	  },
	});
*/


//injectTapEventPlugin();

class MuiTest extends React.Component{

	constructor(props) {
    	super();
    	this.state={
    		contacts:[

    			{
					"name":"Shibnath Salui","mobile":"8900138373","gender":"m"
				},
				{
					"name":"Ricky Ponting","mobile":"46546444","gender":"m"
				},
				{
					"name":"Riya","mobile":"545454754","gender":"f"
				},
				{
					"name":"Adam Gilchrish","mobile":"5465787454","gender":"m"
				},
				{
					"name":"Dale Stayne","mobile":"546512165","gender":"m"
				},
				{
					"name":"Pooja Jha","mobile":"452184788","gender":"f"
				},
				{
					"name":"K. Peterson","mobile":"5418744588","gender":"m"
				},
				{
					"name":"AB De Villers","mobile":"876165476456","gender":"m"
				},
				{
					"name":"Kirty Gayen","mobile":"4418454548","gender":"f"
				}

    		]
    	}
  	}

	deleteContact(mobile){
		
		var allContacts=this.state.contacts;
		var i;
		for(i in allContacts){
			if(allContacts[i].mobile===mobile){
				break;
			}
		}
		allContacts.splice(i,1);
		console.log(allContacts);
		this.setState({
			contacts:allContacts
		})

	}

	render(){

		const style={
				
				paper1:{
					marginLeft:265,
					marginTop:100,
					marginRight:25,
					height:100
				},
				paper2:{
					marginLeft:265,
					marginTop:80,
					marginRight:25,
					
					padding:25
				}
				
		}
		
		/*const allContactList=(
			<div>

				{
					this.state.contacts.map(contact=>{

						return(
							<Paper zDepth={1} style={{margin:10}} key={contact.mobile}>
								<ListItem
							        primaryText={contact.name}
							        secondaryText={contact.mobile}
							        leftAvatar={contact.gender==="m" ?<Avatar src="../icons/contact-male-icon.png" /> 
							        				:<Avatar src="../icons/contact-female-icon.png" />
							        			}
							        rightIcon={<div onClick={()=>this.deleteContact(contact.mobile)}><ActionDelete/></div>}

							    />	
							</Paper>
						)
					})
				}

				
			</div>
		)*/

		return(

			<MuiThemeProvider>
				<div>
					<AppBar 
						title="Contact Manager application" onLeftIconButtonTouchTap={()=>this.toggleDrawer()}
						showMenuIconButton={false}
						style={{ position: 'fixed',top: 0}}
					></AppBar>
					<Drawer containerStyle={{top: 64}}>
						<List>

							<Paper zDepth={1} style={{margin:15}}>
								<ListItem 
									primaryText="Contacts" 
									leftIcon={<CommunicationContacts/>}
									//leftAvatar={<Avatar icon={<CommunicationContacts />} />}
								/>
							</Paper>
							<Paper zDepth={1} style={{margin:15}}>
								<ListItem
									primaryText="Contacts on Gmap"
									leftIcon={<MapsMap/>}
									//leftAvatar={<Avatar icon={<MapsMap />} />}
								/>
							</Paper>
							<Paper zDepth={1} style={{margin:15}}>
								<ListItem
									primaryText="Add new contact"
									leftIcon={<SocialPersonAdd/>}
									//leftAvatar={<Avatar icon={<SocialPersonAdd />} />}
								/>
							</Paper>
							<Paper zDepth={1} style={{margin:15}}>
								<ListItem
									primaryText="About App"
									leftIcon={<ActionInfo/>}
									//leftAvatar={<Avatar icon={<ActionInfo />} />}
								/>
							</Paper>
							
						</List>
					</Drawer>
				
					{/*<Paper zDepth={2} style={style.paper1} />*/}
					{/*<Paper zDepth={2} style={style.paper2} >
						{allContactList}
					</Paper>*/}

					<Test/>
					
				</div>
			</MuiThemeProvider>

		)
	
	}

}

export default MuiTest;





















//Customize Mui theme
		/*const myMuiTheme=getMuiTheme({
			  palette: {
			    textColor: pinkA100,
			  },
			  appBar: {
			    height: 60,

			  },
		})*/