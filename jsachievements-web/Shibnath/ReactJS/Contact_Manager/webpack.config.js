var path=require('path');

module.exports={

	entry:'./jsx/index.jsx',
	output:{

		path:path.resolve(__dirname,'index.html'),
		filename: 'transpiled.js'
	},
	module:{

		loaders:[

			{

				test:/\.jsx?$/,
				loaders:'babel-loader',
				exclude:/node_modules/,
				query:{

					presets:['es2015','react']
				}
			}
		]
	}

}