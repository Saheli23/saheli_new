import React from 'react';
import firebase from '../config/firebase.jsx'

var database=firebase.database();

export const addNewContact =(newContact)=>{

	//console.log("in action",newContact);

	return{

		type:'ADD_NEW_CONTACT',
		payload:newContact
	}
}

export function getAllContacts() {
  //console.log("getAllContact() action has been called");
  var ref=database.ref('contacts_test');
  
  return dispatch=>{
  	ref.once('value').then(function(snap){
  		dispatch({
  			type:'GET_ALL_CONTACTS',
  			payload:snap.val()
  		})
  	})
  }
}

export function viewContactDetails(contact) {
  
  //console.log("Action :",contact)
  return{

    type:'VIEW_CONTACT_DETAILS',
    payload:contact
  }
}

export function deleteContact(key){
  //console.log(key)
  var ref=database.ref('contacts_test');
  ref.child(key).remove();
  return{
    type:'DELETE_CONTACT',
    payload:"Contact Deleted!"
  }
}