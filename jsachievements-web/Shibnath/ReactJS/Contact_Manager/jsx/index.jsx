import React from 'react';
import ReactDOM from 'react-dom';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
//React router
import {BrowserRouter} from 'react-router-dom';
import { Switch, Route,Link } from 'react-router-dom';
import {AllRoutes} from './routes/routes.jsx'

//components 
import Header from './components/header/header.jsx';
import Sidebar from './components/sidebar/sidebar.jsx';
import Container from './components/container/container.jsx';

//views
import Menus from './views/menus/menu.jsx';
import AllContacts from './views/allContacts/all-contacts.jsx';
import AddContact from './views/addContact/add-contact.jsx';
//creating global store scope
import {createStore,applyMiddleware} from 'redux';
import allContactsReducer from './reducers/index.jsx'
import {Provider} from 'react-redux';

import firebase from 'firebase'

import thunk from 'redux-thunk'

let middleware=[thunk];

middleware=[...middleware];
//this contact manager app

class Index extends React.Component{
	
	render(){

		const store=createStore(allContactsReducer,
			applyMiddleware(...middleware)
		);

		

		return(

			/*basename="/jsachievements-web/Shibnath/ReactJS/Contact_Manager/build/#/"*/
			
			<Provider store={store}>
				<BrowserRouter basename="/#">
					<div>
						<MuiThemeProvider>
							<div>
								{AllRoutes}
								
								<Header/>
								<Sidebar>
									<Menus/>
								</Sidebar>
								
								{/*<AddContact/>*/}
								{/*<AllContacts/>*/}

							</div>
						</MuiThemeProvider>
						
					</div>
				</BrowserRouter>
			</Provider>
		)
	}
}

ReactDOM.render(
	
		<Index/>
	,document.getElementById('index')
)