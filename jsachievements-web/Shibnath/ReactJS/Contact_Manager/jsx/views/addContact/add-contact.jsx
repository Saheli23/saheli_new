import React from 'react';

import Container from '../../components/container/container.jsx'

//material-ui components
import Paper from 'material-ui/Paper';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Dialog from 'material-ui/Dialog'
import FlatButton from  'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

//colors 
import {teal500,teal600,lightBlue600} from 'material-ui/styles/colors';

//Icons
import ContentAdd from 'material-ui/svg-icons/content/add';

//redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {addNewContact} from '../../actions/action.jsx'

//Router
import {Redirect} from 'react-router-dom'

//firebase
import firebase from '../../config/firebase.jsx'


var database=firebase.database();

var contact={
	"name":"",
	"mob":"",
	"gender":"m",
	"contry":"",
	"address":""
}


class AddContact extends React.Component{

	constructor(props) {
	    super(props);
	    this.state = {
	    	open:false,
			redirect:false,
			aftrAct:false
	    };
  	};
	
	isOpen(){

		this.setState({
			open:!this.state.open,
			aftrAct:false
		})
	};

	handleInputs(event){
		
		if (event.target.name==="name") {
			contact.name=event.target.value;

		}		
		if (event.target.name==="mob") {
			contact.mob=event.target.value;
			
		}
		if (event.target.name==="gender") {
			contact.gender=event.target.value;

		}
		if (event.target.name==="address") {
			contact.address=event.target.value;

		}	
	};

	handleAddNewContact(){

		this.props.addNewContact(contact);
		this.setState({aftrAct:true})
		

	}

	render(){

		const style={

			text:{

				fontSize:25,
				marginTop:20
			},
			actionBtn:{
				margin:10
			},
			radioButton:{
				marginBottom: 16,
			}

		}
		const modal={

			body:{

				backgroundColor:teal500
			},
			title:{

				backgroundColor:teal500,
				color:'white',
				fontSize:30
			},
			content:{

				width:550
			},
			textAreas:{

				marginTop:200
			}
		}

        if(this.state.aftrAct){
        	let ref=database.ref('contacts_test');
        	ref.push(this.props.addedContact);
        	return(<Redirect to='all-contacts'/>)
        }

		const actions = [
		     
		      <RaisedButton 
		       label="Cancel" 
		       primary={true}
		       onClick={()=>this.isOpen()}
		       style={style.actionBtn}

		      />,
		      <RaisedButton 
		       label="Save" 
		       primary={true}
		       onClick={this.handleAddNewContact.bind(this)}
		      />
		      
	    ];


		return(
			<Container>
				<div align="center">
					
						<div>
							<FloatingActionButton onClick={()=>this.isOpen()}>
						      <ContentAdd/>
						    </FloatingActionButton>
						</div>
					
					<div style={style.text}>Add a new contact</div>

				</div>

				<Dialog
		          title="Add New Contact"
		          actions={actions}
		          modal={true}
		          open={this.state.open}
		          //bodyStyle={modal.body}
		          contentStyle={modal.content}
		          //overlayStyle={{backgroundColor:'rgba(18, 169, 244,0.4)'}}
		          //actionsContainerStyle={modal.body}
		          //style={modal.modalBody}
		          titleStyle={modal.title}
		        >
		         	<div >
		         		
  						<TextField
    						hintText="Enter New Contact Name"
    						floatingLabelText="Contact Name"
    						fullWidth={true}
    						name="name"
    						onChange={this.handleInputs.bind(this)} 
  						/><br/>
  						<TextField
    						hintText="Enter New Contact Number"
    						floatingLabelText="Contact Number"
    						fullWidth={true}
    						name="mob"
    						onChange={this.handleInputs.bind(this)} 
  						/><br/>
  						<TextField
    						hintText="Enter New Contact Address"
    						floatingLabelText="Contact Address"
    						fullWidth={true}
    						name="address"
    						onChange={this.handleInputs.bind(this)} 
  						/><br/>
  						
  						<RadioButtonGroup onChange={this.handleInputs.bind(this)}  name="gender" defaultSelected="m">
					      <RadioButton
					        value="m"
					        label="Male"
					        style={style.radioButton}
					        
					        ref="gender"
					        
					      />
					      <RadioButton
					        value="f"
					        label="Female"
					        style={style.radioButton}
					        
					        ref="gender" 
					      />
					      
					    </RadioButtonGroup>
	  					<div>
	  						<Paper>
	  						</Paper>
	  					</div>
		         	</div>

		        </Dialog>

			</Container>
		)
	}
}



function mapStateToProps(state){

	return{
		allContacts:state.allContacts,
		addedContact:state.addNewContact
	}
}

function mapDispatchToProps(dispatch) {
	
	return bindActionCreators({addNewContact:addNewContact},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddContact);