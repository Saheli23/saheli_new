/**/import React from 'react';

import Sidebar from '../../components/sidebar/sidebar.jsx'

//icons
import CommunicationContactPhone from 'material-ui/svg-icons/communication/contact-phone';
import SocialPersonAdd from 'material-ui/svg-icons/social/person-add'
import MapsMap from 'material-ui/svg-icons/maps/map'
import ActionInfo from 'material-ui/svg-icons/action/info'

//importing material-ui components
import {List,ListItem} from 'material-ui/List';
import AllRoutes from '../../routes/routes.jsx'
import Paper from 'material-ui/Paper';

import {Link} from 'react-router-dom';
import {BrowserRouter} from 'react-router-dom';
import { Switch, Route } from 'react-router-dom';

class Menus extends React.Component{

	render(){

		const style={

			paper:{
				margin:5
			}
		}

		return(
			<Sidebar>
				
				<div>
					
					<List>
						<Link to='all-contacts' style={{ textDecoration: 'none' }}>
							<Paper zDepth={2} style={style.paper}>
								<ListItem primaryText="All Contacts" leftIcon={<CommunicationContactPhone/>} />
							</Paper>
						</Link>

						<Link to="/add-contact" style={{ textDecoration: 'none' }}>
							<Paper zDepth={2} style={style.paper}>
								<ListItem primaryText="Add contact" leftIcon={<SocialPersonAdd/>} />
							</Paper>
						</Link>

						<Paper zDepth={2} style={style.paper}>
							<ListItem primaryText="Contacts on Gmap" leftIcon={<MapsMap/>} />
						</Paper>

						<Paper zDepth={2} style={style.paper}>
							<ListItem primaryText="Aboute App" leftIcon={<ActionInfo/>} />
						</Paper>
					</List>
                </div>
                
			</Sidebar>
		)
	}
}

export default Menus;