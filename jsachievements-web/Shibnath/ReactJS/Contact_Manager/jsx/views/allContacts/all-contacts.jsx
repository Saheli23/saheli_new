import React from 'react';

import Container from '../../components/container/container.jsx';

//material-ui components
import {List,ListItem} from 'material-ui/List'
import Paper from 'material-ui/Paper';
import Avatar from 'material-ui/Avatar/Avatar';
import Divider from 'material-ui/Divider';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import Snackbar from 'material-ui/Snackbar'

//Loaders/progress bars
import LinearProgress from 'material-ui/LinearProgress';
import CircularProgress from 'material-ui/CircularProgress';

//material-ui Icons
import ActionDelete from 'material-ui/svg-icons/action/delete';
import ContentDeleteSweep from 'material-ui/svg-icons/content/delete-sweep';
import ActionDeleteForever from 'material-ui/svg-icons/action/delete-forever'

//Colors 
import {teal500,teal600,lightBlue600} from 'material-ui/styles/colors';

//Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getAllContacts,viewContactDetails,deleteContact} from '../../actions/action.jsx'

//Router
import {Redirect} from 'react-router-dom'

var updateContact={
	"name":"",
	"mob":"",
	"gender":"",
	"contry":"",
	"address":""
}


class AllContacts extends React.Component{

	constructor(props){
		super(props);
		this.state={
			load:false,
			dialogBoxopen:false,
			key:"",
			delete:false,
			snackbarOpen:false,
			updateBtnDisabled:true,
			name:""
		}
	}

	componentDidMount(){
		this.props.getAllContacts();
		//console.log("ConponentDidmount!!")
	}
	viewContact(contact,key){
		let self=this;
		self.props.viewContactDetails(contact);
		self.setState({
			dialogBoxopen:true,
			key:key,
			delete:false,
			snackbarOpen:false
		})
	}

	doCancel(){
		let self=this;
		self.setState({
			dialogBoxopen:false,
			delete:false,
			snackbarOpen:false
		})
	};

	handleDeleteContact(key){
		this.props.deleteContact(key);
		this.props.getAllContacts();
		this.setState({
			dialogBoxopen:false,
			delete:false,
			load:false,
			snackbarOpen:true
		});
	}

	updateSelectedContact(){
		console.log(updateContact)
	}

	handelSnackbarClose(){
		this.setState({
			dialogBoxopen:false,
			delete:false,
			load:false,
			snackbarOpen:false
		});
	}

	
	render(){ 

		let self=this;
		let selectedContact=self.props.contactDetails;

		const allContactsList=(

			this.props.allContacts!==null?(
				
				Object.keys(this.props.allContacts).map((key) => {
			    	
			    	let contact=this.props.allContacts
			    	
			    	return( 
						<div key={key} onClick={self.viewContact.bind(self,contact[key],key)}> 
	                   		<ListItem 
	                   			primaryText={contact[key].name}
	                   			secondaryText={contact[key].mob}
	                   			leftAvatar={

	                   				contact[key].gender==="m" ?<Avatar src="../../../icons/contact-male-icon.png" /> 
								    :<Avatar src="../icons/contact-female-icon.png" />
								}							
	                   		/>
	                   		<Divider inset={true} />
	                   	</div>
					)
			    })

			):(<div align="center"><CircularProgress size={80} thickness={5} /></div>)
		);
		const style={

			text:{

				fontSize:25,
				marginTop:20
			},
			actionBtn:{
				margin:10
			},
			radioButton:{
				marginBottom: 16,
			}

		}
		const modal={

			body:{

				backgroundColor:teal500
			},
			title:{

				backgroundColor:'rgb(61, 188, 212)',
				color:'white',
				fontSize:30
			},
			content:{

				width:550
			},
			textAreas:{

				marginTop:200
			}
		}
		const actions = [
		     
		      <RaisedButton 
		       label="Cancel" 
		       primary={true}
		       onClick={self.doCancel.bind(self)}
		       style={style.actionBtn}

		      />,
		      <RaisedButton 
		       label="Update" 
		       primary={true}
		       disabled={this.state.updateBtnDisabled}
		       onClick={self.updateSelectedContact.bind(self)}
		      />
		      
	    ];
		const viewContactDet=(
			<Dialog
		          title="Container Details"
		          actions={actions}
		          modal={true}
		          open={self.state.dialogBoxopen}
		          //bodyStyle={modal.body}
		          contentStyle={modal.content}
		          //overlayStyle={{backgroundColor:'rgba(61, 188, 212,0.4)'}}
		          //actionsContainerStyle={modal.body}
		          //style={modal.modalBody}
		          titleStyle={modal.title}
		    >
		       {
		       		selectedContact!==null ? (

		       			
						<div>
			         		
	  						<TextField
	    						hintText="Enter New Contact Name"
	    						floatingLabelText="Contact Name"
	    						fullWidth={true}
	    						name="name"
	    						value={selectedContact.name}
	  						/><br/>
	  						<TextField
	    						hintText="Enter New Contact Number"
	    						floatingLabelText="Contact Number"
	    						fullWidth={true}
	    						name="mob"
	    						value={selectedContact.name}
	  						/><br/>
	  						<TextField
	    						hintText="Enter New Contact Address"
	    						floatingLabelText="Contact Address"
	    						fullWidth={true}
	    						name="address"
	    						value={selectedContact.address}
	  						/><br/>
	  						
	  						<RadioButtonGroup  
	  						 name="gender" 
	  						 defaultSelected={selectedContact.gender==="m" ? "m":"f"}
	  						>
						      <RadioButton
						        value="m"
						        label="Male"
						        style={style.radioButton}
						        
						        ref="gender"
						        
						      />
						      <RadioButton
						        value="f"
						        label="Female"
						        style={style.radioButton}
						        
						        ref="gender" 
						      />
						      
						    </RadioButtonGroup>
		  					
						    
							
								<IconButton 
									onClick={self.handleDeleteContact.bind(self,self.state.key)} 
									tooltip="Delete Contact"
									/*touch={true} */
									tooltipPosition="bottom-right"
								>
								  <ActionDelete/>
								</IconButton>
						
						    {}
			         	</div>		       			
		       			
		       		):""
		       }  	

		    </Dialog>
		)

		const messages=(
			this.state.snackbarOpen ? (
				<Snackbar
		          open={this.state.snackbarOpen}
		          message={"Contact Deleted!"}
		          autoHideDuration={3000}
		          onRequestClose={this.handelSnackbarClose.bind(this)}
		        />
			):""
		)

		return(
			<Container>
				<div>
					<List>
						<div>
							{allContactsList}
							
							{viewContactDet}
							
							{messages}

						</div>
					</List>

				</div>
			</Container>
		)
	}
}

function mapStateToProps(state){

	return{
		allContacts:state.allContacts,
		contactDetails:state.viewContact,
		deleteTheContact:state.deleteContact
	}
}


function mapDispatchToProps(dispatch) {
	
	return bindActionCreators({
			getAllContacts:getAllContacts,
			viewContactDetails:viewContactDetails,
			deleteContact:deleteContact
		},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AllContacts);