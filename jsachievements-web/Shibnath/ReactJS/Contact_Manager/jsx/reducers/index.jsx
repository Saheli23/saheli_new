import {combineReducers} from 'redux';
//import allContacts from './contacts/contacts.jsx';
import contactListReducer from './getAllContacts/getAllContacts.jsx'
import addNewContactReducer from './addNewContact/addNewContact.jsx'
import viewContactDetailsReducer from './viewContactDetails/viewContactDetails.jsx';
import deleteContactReducer from './deleteContact/deleteContact.jsx';
const allContactsReducer=combineReducers({

	allContacts:contactListReducer,
	addNewContact:addNewContactReducer,
	viewContact:viewContactDetailsReducer,
	deleteContact:deleteContactReducer
})

export default allContactsReducer;