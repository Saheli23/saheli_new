import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Menus from '../views/menus/menu.jsx';
import AllContacts from '../views/allContacts/all-contacts.jsx';
import AddContact from '../views/addContact/add-contact.jsx';

export const AllRoutes=(

	<Switch>
      				<Route exact path='/' component={AllContacts}/>
      				<Route path="/all-contacts" component={AllContacts}/>
      				<Route path="/add-contact" component={AddContact}/>
      				
    </Switch>
)