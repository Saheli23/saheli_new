import firebase from 'firebase';
const config = {
    apiKey: "AIzaSyCmQCLCTh_NlEjRbNeQD03LeYX9A0hRGg0",
    authDomain: "fir-test-766f0.firebaseapp.com",
    databaseURL: "https://fir-test-766f0.firebaseio.com",
    projectId: "fir-test-766f0",
    storageBucket: "fir-test-766f0.appspot.com",
    messagingSenderId: "693060080510"
};

firebase.initializeApp(config);

export default firebase;