import React from 'react';

import Paper from 'material-ui/Paper';

class Container extends React.Component{
	
	render(){

		const style={

			container:{

				marginLeft:270,
				marginTop:100,
				marginRight:25,
				padding:30,
				height:'auto'
			}
		}

		return(
			<div>
				<Paper zDepth={2} style={style.container}>
					<div>
						{this.props.children}
					</div>
				</Paper>
			</div>
		)
	}
}


export default Container;