import React from 'react';

//Paper
import Paper from 'material-ui/Paper';

class Sidebar extends React.Component{
	
	render(){

		const style={
			sidebar:{
				height:'100%',
				width:'auto',
				position:'fixed',
				top:64
			}
		}

		return(
			<Paper zDepth={4} style={style.sidebar}>
				<div>
					{
	                    this.props.children
	                }
				</div>
			</Paper>

		)
	}
}

export default Sidebar;