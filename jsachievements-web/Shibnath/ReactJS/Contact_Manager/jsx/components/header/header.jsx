import React from 'react';

import AppBar from 'material-ui/AppBar'

class Header extends React.Component{
	
	render(){

		return(
			<AppBar title="Contact Manager App" showMenuIconButton={false} style={{position:'fixed',top:0}}/>
		)
	}
}

export default Header;