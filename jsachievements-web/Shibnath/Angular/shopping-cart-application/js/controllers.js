/*import {do} from 'egister.js';*/
//alert("controllers enter")
var myApp=angular.module('Index', ['ui.router']);

var updatedCart=[];

myApp.config(['$stateProvider',function($stateProvider) {
    
    $stateProvider
        .state('index',{
            url:'/index',
            sticky: true,
            views:{

                'index':{

                    templateUrl:'templates/index.html',
                    
                }
            }
        })
        /*.state('viewCart',{
            url:'/viewCart',
            sticky: true,
            views:{

                'viewCart':{

                    templateUrl:'templates/view-cart.html',
                    
                }
            }
        })
        */


}])
    
myApp
    .controller('gen', ['$scope','cartServices', function($scope,cartServices){
        
        var promise=cartServices.getCartDetails();
            promise.then(function(response){
                var cart=response.data;
                updatedCart=cart;
        })
    }])
    .controller('header', ['$scope', function($scope){
        $scope.header="templates/header.html";
    }])
    .controller('container', ['$scope', function($scope){
        $scope.container="templates/container.html";
    }])
    .controller('showLoginModal', ['$scope', function($scope){
        $scope.showLoginModal="templates/loginModal.html"
    }])
    .controller('login', ['$scope', function($scope){
        $scope.login="templates/login.html";
        $scope.title="Login";
    }])
    .controller('reg', ['$scope', function($scope){
        $scope.reg="templates/reg.html";
        $scope.title="Regitration";
        
    }])
    .controller('indexController', ['$scope','$state', function($scope,$state){
        $state.go('index');
    }])
    .controller('editUser', ['$scope', function ($scope) {
        $scope.editUser="templates/edit-user-profile-modal.html"
    }])
    .controller('regForm', ['$scope','userServices', function($scope,userServices){
        
        $scope.register=function(){
            var fname=($scope.fname);
            var lname=($scope.lname);
            var email=($scope.email);
            var mob=($scope.mob);
            var pass=($scope.pass);
            var conPass=($scope.conPass);
            var newUser={"fname":fname,"lname":lname,"email":email,"mob":mob,"pass":pass};
            

            if(fname!==null && lname!==null && email!==null && mob!==null && pass!==null && conPass!=null ){

                if(pass===conPass){
            
                    var promise=userServices.getUsers();
                    promise.then(function(response){

                        var users=response.data;
                        var c=0;
                        for(var i in users){
                            if(users[i].email===email || users[i].mob===mob){
                                c++;
                                break;
                            }
                        }

                        if(c!=0){
                            $scope.regStat="User already exist!, Please try with outher email or mobile number!";
                        }
                        else{
                            users.push(newUser);
                            $scope.regStat="You are successfully register with us, Please login";
                            //$scope.loginLink="Please login";
                            //console.log("New user added!");
                            console.log(users)
                        }

                    })
                }
                else{
                    //console.log("Password mismatched!");
                    $scope.passMismatched="Password mismatched..."
                }
            }
            else{
                $scope.regStat="Please fill all the credential...."
            }
           
        }

    }])
    .controller('catagories', ['$scope', function ($scope) {
        $scope.catagories="templates/catagory-list.html";
    }])
    .controller('productAndCatagory', ['$scope','productsServices','cartServices', function($scope,productsServices,cartServices){
       
        var promise=productsServices.getProducts();
        promise.then(function(response){

            //showing all products
            products=response.data;
            $scope.products=products;

            //showing all uniqe catagoris
            var unqCatagory=[];
            var i,j,k=0,l;
            for(i in products){
                l=0;
                
                for(j=0;j<i;j++){
                    if(products[j].catagory===products[i].catagory){
                        l++;
                        break;
                    }
                }
                if(l==0){
                   unqCatagory[k]=products[i].catagory;
                    k++; 
                }

            }
            $scope.catagories=unqCatagory;
            
            //showing all the product as per the catagory
            $scope.filterBtn=function(cat){
                var filterProducts=[],k=0;
                for(var i in products){

                    if(products[i].catagory===cat){
                        filterProducts[k]=products[i];
                        k++;
                    }
                }

                $scope.products=filterProducts;
            }



        //showing all the products whenevr we click on "show all products"
        $scope.showAllproducts=function(){
            $scope.products=products;
        }

        //showing products details

        $scope.productDetails=function(productId){

            var i;
                for(var j in products){
                    if(products[j].productId===productId){
                        i=j;
                        break;
                    }
                }
                $scope.productImg=products[i].pic;
                $scope.productName=products[i].name;
                $scope.price=products[i].price;
                $scope.rating=products[i].rating;
                $scope.desc=products[i].desc;

        } 

        //enabling and disabling the "Add to cart" button as per the user loggen in 
        
        if(typeof(Storage) !== "undefined") {
                
               var userEmail=localStorage.getItem("userEmail");
               var userName=localStorage.getItem("userName");

               
               
               if(userEmail!=null){
                    
                    $scope.disabled=false;
                    $scope.tooltips="Click here to add this to the cart";
                    $scope.addToCartBtn=function(productId){
                        
                        var onlyItems={productId:productId,quantity:1};
                        
                        var userWithItem={  
                                        userId:userEmail,
                                        items:[{productId:productId,quantity:1}]
                                     }
                        //var onlyUser={userId:userEmail,item:[]}          
                            
                            var i,j,l=0,m=0;
                            
                            for(i in updatedCart){

                                if(updatedCart[i].userId===userEmail){

                                    //console.log("User exist in cart!-------productID: "+productId)
                                    var k=0;
                                    for(j in updatedCart[i].items){

                                        if(updatedCart[i].items[j].productId===productId){
                                            k++;
                                            m++;
                                            break;
                                            
                                        }
                                    }
                                    if(k==0){
                                        updatedCart[i].items.push(onlyItems);
                                        m++;
                                        console.log("A item pushed to part")
                                    }
                                    else{
                                        m++;
                                        updatedCart[i].items[j].quantity=updatedCart[i].items[j].quantity+1;
                                    }
                                    break;
                                }
                                else{
                                    l++;
                                }

                            }

                            if(l!=0 && m==0 ){
                                updatedCart.push(userWithItem);

                            }

                            //updatedCart=cart;

                            console.log(updatedCart)

                        

                    }
               }
               else{
                    $scope.disabled=true;
                    $scope.tooltips="Please login"
               }

            } 
            

        })


    }])
    .controller('productDetails', ['$scope', function ($scope) {
        $scope.productDetails="templates/product-details-modal.html"
    }])
    .controller('products', ['$scope', function ($scope) {
        $scope.products="templates/all-products.html";
    }])
    .controller('loginForm', ['$scope','userServices','$state', function($scope,userServices,$state){
        
        $scope.login=function(){
            var email=$scope.email;
            var pass=$scope.pass;
            
            if(email!=null && pass!=null){
                
                var promise=userServices.getUsers();
                promise.then(function(response){
                    var users=response.data;
                    console.log(users)
                    var j=0,c=0;
                    for(var i in users){
                        if(users[i].email===email){
                            j=i;
                            c++;
                            break;
                        }
                    }
                    
                    if(c==0){
                        $scope.loginStat="User doesn't exist"
                    }
                    else if(c!=0){

                        if(users[j].pass===pass){
                           // $scope.loginStat="Password matched!!..welcome!!";
                           console.log("login successfully....naviagting to welcome page!");
                           if (typeof(Storage) !== "undefined") {
                                
                                localStorage.setItem("userEmail",users[j].email);
                                localStorage.setItem("userName",users[j].fname+" "+users[j].lname);
                              
                                $state.go('index', {}, { reload: true });


                            } else {
                               console.log("Sorry! Your browser doesn't support localStorage..");
                            }
                           

                        }
                        else{
                            $scope.loginStat="Password mismatched...please try again";
                        }
                    }

                })
            }
            else{
                $scope.loginStat="Please fillup all the credential...";
            }
        }
    }])
    .controller('headerController', ['$scope','$state','cartServices','productsServices', function($scope,$state,cartServices,productsServices){
        

        if (typeof(Storage) !== "undefined") {
           
           var userEmail=localStorage.getItem("userEmail");
           var userName=localStorage.getItem("userName");

           if(userEmail!=null){
                
                $scope.disabled=false;
                $scope.showLoggedUser=true;
                $scope.userName=userName;

                $scope.userLogout=function(){
                    localStorage.clear();
                    console.log("local storage has been cleared!!");
                    console.log(updatedCart)
                    $state.go('index', {}, { reload: true });
                }
                $scope.viewCart="templates/view-cart.html";

                

                $scope.seeCart=function(){
                    var i=0;
                    
                    for(i in updatedCart){

                        if(updatedCart[i].userId===userEmail){
                            break;
                        }
                    }
                    var productInCart=[]
                    var promise=productsServices.getProducts();
                    promise.then(function(response){

                        products=response.data;

                        console.log("updatedCart",updatedCart);

                        var m=0,j=0,k;
                        
                        for(j in products){

                            for(k in updatedCart[i].items){

                                if(products[j].productId===updatedCart[i].items[k].productId){

                                    productInCart[m]=products[j];
                                    productInCart[m].qnt=updatedCart[i].items[k].quantity;
                                    m++;
                                }
                            }
                        }
                        if(productInCart.length==0){
                            $scope.msg='Your cart is empty...please add some item to your cart';
                        }
                        else{
                            $scope.msg='';
                        }
                        $scope.cartItems=productInCart;
                        console.log("Items details",$scope.cartItems);
                        


                        //console.log("Items details",$scope.cartItems;
                    })

                    $scope.remove=function(pId){

                        var i=0;
                    
                        for(i in updatedCart){

                            if(updatedCart[i].userId===userEmail){
                                break;
                            }
                        }


                        var x;
                        for(x in productInCart){
                            if(productInCart[x].productId===pId){
                                break;
                            }

                        }
                        var y;
                        for(y in updatedCart[i].items){

                            if(updatedCart[i].items[y].productId===pId){
                                break;
                            }
                        }
                        productInCart.splice(x,1);
                        updatedCart[i].items.splice(y,1);

                        $scope.cartItems=productInCart;
                        if(updatedCart[i].items.length==0){
                            $scope.msg="Your cart is empty...please add some item to your cart";
                        }

                    }

                    
                }
           }
           else{
                $scope.showLoginBtn=true;
                $scope.disabled=true;
                $scope.tooltips="Please Login!"

           }
        } 
        
    }])



myApp.service('userServices', ['$http','$q', function($http,$q){
    
    var deferred=$q.defer();
    $http.get('json/users.json').then(function(data){
        deferred.resolve(data);
    });
    this.getUsers=function(){
        return deferred.promise;
    }
}])
.service('productsServices', ['$http','$q', function($http,$q){
    
    var deferred=$q.defer();
    $http.get('json/products.json').then(function(data){
        deferred.resolve(data);
    });
    this.getProducts=function(){
        return deferred.promise;
    }
}])
.service('cartServices', ['$http','$q', function($http,$q){
    
    var deferred=$q.defer();
    $http.get('json/cart.json').then(function(data){
        deferred.resolve(data);
    });
    this.getCartDetails=function(){
        return deferred.promise;
    }
}])