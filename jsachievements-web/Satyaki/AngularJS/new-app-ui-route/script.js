var myApp = angular.module('myApp', ['ui.router']);

myApp.service('test', function() {
    this.names = [
    {Name:'Jani',Country:'Norway',isChecked:false},
    {Name:'Carl',Country:'Sweden',isChecked:false},
    {Name:'Margareth',Country:'England',isChecked:false},
    {Name:'Hege',Country:'Norway',isChecked:false},
    {Name:'Joe',Country:'Denmark',isChecked:false},
    {Name:'Gustav',Country:'Sweden',isChecked:false},
    {Name:'Birgit',Country:'Denmark',isChecked:false},
    {Name:'Mary',Country:'England',isChecked:false},
    {Name:'Kai',Country:'Norway',isChecked:false}
  ];
});

myApp.config(['$stateProvider',function($stateProvider) {
  var showFull = {
    name: 'showFullArray',
    url: '/showFullArray',
    templateUrl: 'showFullList.html',
    controller: 'PostsCtrl'
  }

  var editAddList = {
    name: 'editAddArray',
    url: '/editAddArray',
    templateUrl: 'editAddArray.html',
    controller: 'EditArrayCtrl'
  }

  var searchInList = {
    name: 'searchName',
    url: '/searchName',
    templateUrl: 'searchName.html',
    controller: 'SerachCtrl'
  }

  $stateProvider.state(showFull);
  $stateProvider.state(editAddList);
  $stateProvider.state(searchInList);
}]);

myApp.controller('EditArrayCtrl', function($scope, test){

  $scope.names = test.names;
  $scope.normal = true;
  $scope.showButton = true;
  $scope.input = null;
  $scope.inputCountry = null;

  $scope.user ={
    roles:[]
  }

  $scope.add = function() {
    if($scope.input.placeholder !== 'Name' && $scope.inputCountry.placeholder !== 'Country')
    {
      $scope.names.push({'Name': $scope.input, 'Country': $scope.inputCountry, 'isChecked': false});
      $scope.input = null;
      $scope.inputCountry = null;
      console.log($scope.names);
    }
  };

  $scope.remove = function(index) {
    $scope.names.splice(index, 1);
  };

  $scope.edit = function(index) {
    $scope.normal = false;
    $scope.input = $scope.names[index]['Name'];
    $scope.inputCountry = $scope.names[index]['Country'];
    $scope.updateIndex = index;
  };

  $scope.modifyName = function() {
    $scope.names[$scope.updateIndex]={'Name': $scope.input, 'Country': $scope.inputCountry, 'isChecked': false};
    $scope.input = null;
    $scope.inputCountry = null;
    $scope.updateIndex = null;
    $scope.normal = true;
  };

  $scope.deleteMultiple = function(){
    for (var i = 0; i < $scope.names.length; i++) {
        if ($scope.names[i].isChecked) {
            var namesName = i;
            $scope.user.roles.push(namesName);
        }
    }
    console.log($scope.user.roles);
    var len = $scope.user.roles.length;
    for(var m=(len-1); m>=0; m--){
      // if($scope.names.indexOf($scope.user.roles[m])>0){
      //   $scope.names.splice(m, 1);
      //   console.log($scope.names);
      // }
      $scope.names.splice($scope.user.roles[m], 1);
      console.log($scope.names);
    }
    $scope.user.roles = [];
    console.log($scope.user.roles);
  }
});

myApp.controller('SerachCtrl', function($scope, $filter, test){
  $scope.names = test.names;
  $scope.counted = $scope.names.length;
  $scope.$watch("test", function(query){
    $scope.filteredData = $filter("filter")($scope.names, query);
  });

  $scope.textBlock = false;

  $scope.clear = function(){
    $scope.myOrderBy ={};
    $scope.test = '';
    $scope.textBlock = false;
  };

  $scope.orderByMe = function(x) {
    $scope.myOrderBy ={};
    $scope.newText = x;
    console.log($scope.test);
    if($scope.test !== null){
      if($scope.newText === 'Name'){
        $scope.myOrderBy = {Name:$scope.test};
        console.log($scope.myOrderBy);
      }
      else{
        $scope.myOrderBy = {Country:$scope.test};
        console.log($scope.myOrderBy);
      }
    }
  };
});

myApp.controller('PostsCtrl', function($scope, test){
  $scope.names = test.names;
});
