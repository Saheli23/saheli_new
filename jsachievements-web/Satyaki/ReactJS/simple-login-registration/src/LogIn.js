import React, { Component } from 'react';
import Request from 'superagent';
import _ from 'lodash';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Registration from './Registration';

class LogIn extends Component {
    constructor(props) {
        super(props);
        this.state = {userEmail: '', userPassword:'', recievedEmails: []};
    }

    componentDidMount(){
        this.loadDataForLogIn();
    }

    loadDataForLogIn(){
        if(localStorage.getItem("userDetailList"))
        {
            var retrievedDataLogIn = JSON.parse(localStorage.getItem("userDetailList"));
            this.setState({"recievedEmails": retrievedDataLogIn});  
            console.log(this.state.recievedEmails);   
        }
    }
    
    handleChangeEmail(event){
        let val=event.target.value;
        this.setState({"userEmail": val});
    }
    handleChangePass(event){
        let pas=event.target.value;
        this.setState({"userPassword": pas});
    }

    handleSubmit(){
        console.log(this.state.userEmail, "......", this.state.userPassword);
        let userArr= this.state.recievedEmails;
        let userArrLength = userArr.length;
        for(var i=0; i<userArrLength; i++){
            //var isPresent = _.indexOf(userArr[i].E_Mail,this.state.userEmail);
            //console.log(isPresent,".....");  
            if(this.state.userEmail == userArr[i].E_Mail && this.state.userPassword == userArr[i].Password){
                console.log(i,".....");
            }
        }
      }

    render() {
        return(
            <div>
                <h1>
                    User Log IN
                </h1>
                <label>E-Mail :- </label>
                <input type="text" name="name"  onChange={this.handleChangeEmail.bind(this)}/>
                <br/>
                <label>Password :- </label>
                <input type="password" name="name"  onChange={this.handleChangePass.bind(this)}/>
                <br/>
                <button onClick={this.handleSubmit.bind(this)}>Log In</button>
            </div>
        );
    }
}

export default LogIn;