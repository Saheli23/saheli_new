import {  
    applyMiddleware,
    combineReducers,
    createStore,
  } from 'redux';

  export const userRegistered = RegistrationReducer =>({
      type: 'REGISTER_USER',
      RegistrationReducer,
  });

  export const addToUserDetails = (RegistrationReducer) =>{
      return {
        type: 'ADD_USER_IN_ARRAY',
        payload: RegistrationReducer,
      };
  };

  export const addToEmailDetails = (RegistrationReducer) => {
      return {
          type: 'ADD_USER_EMAIL_IN_ARRAY',
          payload: RegistrationReducer,
      }
  }

  export const userLogIn = LogInReducer => ({
      type: 'LOG_IN_USER',
      LogInReducer,
  }) 

  export const RegistrationReducer = (state = {
    userEmail: '', 
    userName:'', 
    userPassword: '', 
    confirmPassword: '', 
    userDetails: [], 
    ListUserEmail: []
  }, action) => {
      switch (action.type){
        case 'REGISTER_USER':
            return action.RegistrationReducer;
        case 'ADD_USER_IN_ARRAY':
            return {
                ...state.userDetails,
                userDetails: state.userDetails.concat(action.payload)
            };
        case 'ADD_USER_EMAIL_IN_ARRAY':
            return {
                ...state.ListUserEmail,
                ListUserEmail: state.ListUserEmail.concat(action.payload)
            }
        default:
            return state;
      }
  };

  export const LogInReducer = (state ={
    userEmail: '', 
    userPassword:'', 
    recievedEmails: []
  }, action) => {
      switch(action.type){
        case 'LOG_IN_USER':
            return action.LogInReducer;
        case 'CHECKING_USER':
            return {}
        default:
            return state;
      }
  };


  export const reducers = combineReducers({  
    RegistrationReducer,
    LogInReducer
  });
  export function configureStore(initialState = {}) {  
    const store = createStore(reducers, initialState);
    return store;
  };
  
  export const store = configureStore();  