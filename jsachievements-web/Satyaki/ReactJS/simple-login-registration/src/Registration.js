import React, { Component } from 'react';
import Request from 'superagent';
import _ from 'lodash';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import LogIn from './LogIn';

class Registration extends Component {

    constructor(props) {
        super(props);
        this.state = {userEmail: '', userName:'', userPassword: '', confirmPassword: '', userDetails: [], ListUserEmail: []};
    }

    componentDidMount(){
      this.loadData();
    } 


    loadData()
    {
        if(localStorage.getItem("UserEmailList"))
        {
            var retrievedData = JSON.parse(localStorage.getItem("UserEmailList")); 
            this.setState({"ListUserEmail": retrievedData});
        }
        console.log(this.state.ListUserEmail);
        if(localStorage.getItem("userDetailList"))
        {
            var retrievedData = JSON.parse(localStorage.getItem("userDetailList")); 
            this.setState({"userDetails": retrievedData});
        }
        console.log(this.state.userDetails);
    }

    handleChangeName(event){
        let name=event.target.value;
        this.setState({"userName": name});
    }
    handleChangeEmail(event){
        let val=event.target.value;
        console.log(val,event)
        this.setState({"userEmail": val});
    }
    handleChangePass(event){
        let pas=event.target.value;
        this.setState({"userPassword": pas});
    }
    handleChangeConfirmPass(event){
        let conPas=event.target.value;
        this.setState({"confirmPassword": conPas});
    }

    handleSubmit(){
        let self=this;
        console.log(this.state.userName, "......", this.state.userEmail, "......", this.state.userPassword, "......", this.state.confirmPassword);

       let  emailArr=this.state.ListUserEmail;
       if(emailArr !== []){
            var isPresent = _.indexOf(emailArr,this.state.userEmail);
            console.log(emailArr,this.state.userEmail,isPresent);
            if(isPresent == -1){
                if(this.state.userPassword === this.state.confirmPassword){
                    emailArr.push(this.state.userEmail)
                    localStorage.setItem("UserEmailList", JSON.stringify(emailArr));
                    this.loadData();
                    let self=this,TempArr = (_.isUndefined(this.state.userDetails))?[]:this.state.userDetails;
                    TempArr.push ({
                        'Name':this.state.userName,
                        'E_Mail': this.state.userEmail,
                        'Password': this.state.userPassword,
                        'Confirm_Pass': this.state.confirmPassword            
                    })
                    this.setState({"userDetails": TempArr});
                    localStorage.setItem("userDetailList", JSON.stringify(this.state.userDetails));
                }
            }
       }
       else{
            if(this.state.userPassword === this.state.confirmPassword){
                emailArr.push(this.state.userEmail)
                localStorage.setItem("UserEmailList", JSON.stringify(emailArr));
                this.loadData();
                let self=this,TempArr = (_.isUndefined(this.state.userDetails))?[]:this.state.userDetails;
                TempArr.push ({
                    'Name':this.state.userName,
                    'E_Mail': this.state.userEmail,
                    'Password': this.state.userPassword,
                    'Confirm_Pass': this.state.confirmPassword            
                })
                this.setState({"userDetails": TempArr});
                localStorage.setItem("userDetailList", JSON.stringify(this.state.userDetails));
            }
        }
      }

    render() {
        return(
            <div>
                <h1>
                    User Registration
                </h1>
                <label>Name :- </label>
                <input type="text"  name="name"  onChange={this.handleChangeName.bind(this)}/>
                <br/>
                <label>E-Mail :- </label>
                <input type="email"  name="name"  onChange={this.handleChangeEmail.bind(this)}/>
                <br/>
                <label>Password :- </label>
                <input type="password"  name="name"  onChange={this.handleChangePass.bind(this)}/>
                <br/>
                <label>Confirm Password :- </label>
                <input type="password"  name="name"  onChange={this.handleChangeConfirmPass.bind(this)}/>
                <br/>
                <button onClick={this.handleSubmit.bind(this)}>Registration</button>
            </div>
        );
    }
}

export default Registration;