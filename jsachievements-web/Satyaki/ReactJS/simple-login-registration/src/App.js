import React, { Component } from 'react';
import Request from 'superagent';
import _ from 'lodash';
import logo from './logo.svg';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import './App.css';
import LogIn from './LogIn';
import Registration from './Registration';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <h1 className="App-title">Simple Log In and Registration</h1>
            </header>
            <ul>
              <li>
                <Link to="/">Log In</Link>
              </li>
              <li>
                <Link to="/registration">Registration</Link>
              </li>
            </ul>

            <hr />

            <Route exact path="/" component={LogIn} />
            <Route path="/registration" component={Registration} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
