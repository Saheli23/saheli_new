import logo from './logo.svg';
import './App.css';
import React, {  
  Component,
} from 'react';

import { connect } from 'react-redux';

import {  
  activateGeod,
  closeGeod,
  addToArray
} from './redux';

let newTodo = [];

// App.js
export class App extends Component {

  componentDidMount(){
    console.log("AAAAAAAAA....", this.props.geod.todo);
  } 
  
  handleChange(event){
    let val=event.target.value;
    this.setState({"textValue": val});
  }

  handleSubmit(){
    this.props.geod.value = this.state.textValue;
     if(this.props.geod.value !== null && this.props.geod.value !== ''){
      //alert('A new name is submitted: ' + this.props.geod.value);
      console.log(this.props.addToArray(this.state.textValue));
      // setTimeout(() => {
      //   newTodo= this.props.geod.todo;
      //   console.log(newTodo);
      // }, 2000)      
      return false;      
     }
  }


  render() {
    newTodo= this.props.geod.todo;
    console.log(newTodo);
    return (
      <div className="App"> 
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to React-Redux</h1>
      </header>
        <div>
        {newTodo.map((item,index) =>
            <li key={index}>{item}</li>)}
          <label>
              Name:-
              <input type="text" name="name"  onKeyPress={this.handleChange.bind(this)} />
          </label>
          <button onClick={this.handleSubmit.bind(this)}>
            Submit
          </button>
        </div>
      </div>
    );
  }

}

// AppContainer.js
const mapStateToProps = (state, ownProps) => ({
  geod: state.geod,
});

const mapDispatchToProps = {  
  activateGeod,
  closeGeod,
  addToArray,
};

const AppContainer = connect(  
  mapStateToProps,
  mapDispatchToProps
)(App);

export default AppContainer;  