// import {  
//     applyMiddleware,
//     combineReducers,
//     createStore,
//   } from 'redux';

// export const userReducer = (state = {
//     value:'',
//     toDo: []
// }, action) => {
//     switch(action.type){
//         case "ADD_NAME":
//             state = {
//                 ...state,
//                 value: state.value,
//                 toDo: [...state.toDo, action.payload]
//             };
//         break;
//         case "DELETE_NAME":
//             state = {
//                 ...state,
//                 value: state.value,
//                 toDo: [...state.toDo, action.payload]
//             }; 
//         break;
//         default:

//         break;
//     }
//     return state;
// };

// export const addName = userReducer => ({  
//     type: 'ADD_NAME',
//     userReducer,
//   });
  
//   export const deleteName = () => ({  
//     type: 'DELETE_NAME',
//   });

// export function configureStore(initialState = {}) {  
//     const store = createStore(userReducer, initialState);
//     return store;
//   };
  
// export const store = configureStore();  

// store.subscribe(() => {
//     //console.log("Store Updated..", store.getState());
// });

import {  
    applyMiddleware,
    combineReducers,
    createStore,
  } from 'redux';
  
  // actions.js
  export const activateGeod = geod => ({  
    type: 'ACTIVATE_GEOD',
    geod,
  });

  export const addToArray = (geod) =>{
    console.log(geod)
    return {
      type: 'ADD_IN_ARRAY',
      payload: geod,
    };
  } 

  export const closeGeod = () => ({  
    type: 'CLOSE_GEOD',
  });
  
  // reducers.js
  export const geod = (state = {
      value: '',
      todo: []
  }, action) => {  

    switch (action.type) {
      case 'ACTIVATE_GEOD':
        return action.geod;
      case 'CLOSE_GEOD':
        return {};
      case 'ADD_IN_ARRAY': 
      console.log("QWERRTYUIOP...")
      return {
        ...state.todo,
        todo: state.todo.concat(action.payload)
      };
      default:
        return state;
    }
  };
  
  export const reducers = combineReducers({  
    geod,
  });
  
  // store.js
  export function configureStore(initialState = {}) {  
    const store = createStore(reducers, initialState);
    return store;
  };
  
  export const store = configureStore();  


// import { createStore, combineReducers, applyMiddleware } from "redux";
// import logger from "redux-logger";

// const mathReducer = (state = {
//     result: 1,
//     lastValues: []
// }, action) => {
//     switch(action.type){
//         case "ADD":
//             state = {
//                 ...state,
//                 result: state.result + action.payload,
//                 lastValues: [...state.lastValues, action.payload]
//             };
//         break;
//         case "SUBSTRACT":
//             state = {
//                 ...state,
//                 result: state.result - action.payload,
//                 lastValues: [...state.lastValues, action.payload] 
//             }; 
//         break;
//         default:
//         break;
//     }
//     return state;
// };

// const myLogger = (store) => (next) => (action) => {
//     console.log("Logged Action: ", action);
//     next(action);
// }

// const store = createStore(
//     combineReducers({mathReducer, userReducer}),
//     {},
//     applyMiddleware(logger)
// );

// store.dispatch({
//     type: "ADD",
//     payload: 100
// });

// store.dispatch({
//     type: "SUBSTRACT",
//     payload: 35
// });

// store.dispatch({
//     type: "SET_AGE",
//     payload: 40
// });