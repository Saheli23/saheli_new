import React, { Component } from 'react';
import './style.css';
import _ from 'lodash';

class Favourites extends Component {

  constructor(props) {
    super(props);
    this.state = {favourites: []};
  }

   render() {
     this.state.favourites = JSON.parse(localStorage.getItem("favourites"));
      return (
        <div className="txt-center" className="selectionClass">
          <h2>My Favourite Links Are:- </h2>
          <ul className="block--list">
          {this.state.favourites.map((item,index) =>
              <li key={index}><a><strong>{item}</strong></a></li>
          )}
          </ul>
        </div>
      );
   }
}
export default Favourites;
