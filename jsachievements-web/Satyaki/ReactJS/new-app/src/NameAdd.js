import React, { Component } from 'react';
import update from 'immutability-helper';

class NameAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {value: '', todo: []};

    this.handleChange = this.handleChange.bind(this);
  }

    handleChange(event){
      this.setState({value: event.target.value});
    }

    handleSubmit(){
        if(this.state.value !== null && this.state.value !== ''){
          //alert('A new name is submitted: ' + this.state.value);
          var newToDo = this.state.todo;
          console.log(newToDo);
          newToDo.push(this.state.value);
          this.setState({todo: newToDo});
        //  return false;
        }
    }

    handleDelete(index){
      this.setState(prevState => ({
        todo: update(prevState.todo, {$splice: [[index, 1]]})
      }))
    }

    render(){
      return(
        <div>
          <ul>
          {this.state.todo.map((item,index) =>
            <li key={index}>{item}
              <button>
                Edit
              </button>
              <button onClick={this.handleDelete.bind(this, index)}>
              Delete
              </button>
            </li>
          )}
          </ul>
          <label>
            Name:
            <input type="text" value={this.state.value} onChange={this.handleChange} />
          </label>
          <button onClick={this.handleSubmit.bind(this)}>
           Submit
         </button>
        </div>
      );

    }
  }

  export default NameAdd;
