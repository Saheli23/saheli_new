// import React, { Component } from 'react';
// import Request from 'superagent';
// import _ from 'lodash';
// import './style.css';
// import { BrowserRouter as Router, Route, Link } from "react-router-dom";
//
//
// class FetchDataFromAPI extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {bodies: [],bodyDetails: {},isOpen: -1};
//   }
//
//   ComponentWillMount() {
//     this.UserList();
//   }
//
//
//   UserList(){
//     var url = "https://jsonplaceholder.typicode.com/posts";
//     Request.get(url).then((response) => {
//       this.setState({
//         bodies: response.body,
//       });
//       console.log(this.state.bodies);
//     });
//   }
//
//   BodyExpanssion(index){
//     if(this.state.isOpen === this.state.bodyDetails.id){
//       this.setState({
//         isOpen: -1
//       });
//     }
//     else{
//       var url = "https://jsonplaceholder.typicode.com/posts/"+index;
//       Request.get(url).then((response) => {
//         this.setState({
//           bodyDetails: response.body,
//           isOpen: index
//         });
//         console.log(this.state.bodyDetails.body);
//       });
//     }
//   }
//
//   render(){
//     var bodies = _.map(this.state.bodies, (body) => {
//       //<Router>
//         <div>
//         return <p>
//             <li key={body.id} className="block--Header" onClick={() => this.BodyExpanssion(body.id)}>
//               //<Link to={"/"+body.id}>{body.title}</Link>
//             </li>
//         <span key={this.state.bodyDetails.id} className={(this.state.isOpen===body.id) ? '':'hidden'}>{this.state.bodyDetails.body}</span>
//       </p>;
//     //<Route path={"/"+body.id} component={Comments} />
//     </div>
//     //</Router>
//     });
//
//     const Comments = () => (
//       <div>
//         <h2>
//           <span key={this.state.bodyDetails.id} className={(this.state.isOpen===this.state.bodies.body.id) ? '':'hidden'}>{this.state.bodyDetails.body}</span>
//         </h2>
//       </div>
//     );
//
//     var bodyDetails = (
//       <div key={this.state.bodyDetails.id} class="block--Header">{this.state.bodyDetails.body}</div>
//     );
//     return <div>
//       <button onClick={this.ComponentWillMount.bind(this)}>
//        Get Data
//        </button>
//       <ul>
//         {bodies}
//       </ul>
//
//     </div>
//   }
// }
//
//   export default FetchDataFromAPI;

import React, { Component } from 'react';
import Request from 'superagent';
import _ from 'lodash';
import './style.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";


class FetchDataFromAPI extends Component{
  constructor(props) {
    super(props);
    this.state = {bodies: [],bodyDetails: {},isOpen: -1};
  }

  ComponentWillMount() {
    this.UserList();
  }

  UserList(){
    var url = "https://jsonplaceholder.typicode.com/posts";
    Request.get(url).then((response) => {
      this.setState({
        bodies: response.body,
      });
      console.log(this.state.bodies);
    });
  }

  BodyExpanssion(index){
    if(this.state.isOpen === this.state.bodyDetails.id){
      this.setState({
        isOpen: -1
      });
    }
    else{
      var url = "https://jsonplaceholder.typicode.com/posts/"+index;
      Request.get(url).then((response) => {
        this.setState({
          bodyDetails: response.body,
          isOpen: index
        });
        console.log(this.state.bodyDetails.body);
      });
    }
  }

  render(){
    var bodies = _.map(this.state.bodies, (body) => {
      return <li key={body.id} className="block--Header" onClick={() => this.BodyExpanssion(body.id)}>
        <p>{body.title}</p>
        <span key={this.state.bodyDetails.id} className={(this.state.isOpen===body.id) ? '':'hidden'}>{this.state.bodyDetails.body}</span>
    </li>;
    });
    const Comments = () => (
      <div>
        <h2>
          <span key={this.state.bodyDetails.id} className={(this.state.isOpen===this.state.bodies.body.id) ? '':'hidden'}>{this.state.bodyDetails.body}</span>
        </h2>
      </div>
    );

    var bodyDetails = (
      <div key={this.state.bodyDetails.id} class="block--Header">{this.state.bodyDetails.body}</div>
    );
    return <div>
      <button onClick={this.ComponentWillMount.bind(this)}>
       Get Data
       </button>
      <ul>
        {bodies}
      </ul>
    </div>
  }
}

  export default FetchDataFromAPI;
