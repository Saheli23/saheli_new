import React, { Component } from 'react';
import Request from 'superagent';
import _ from 'lodash';
import './style.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Favourites from './Favourites';


class GetData extends Component {

  constructor(props) {
    super(props);
    this.state = {bodies: [],bodyDetails: {},isOpen: -1, favourites: [], favouritesID: [], copyFavouritesID: [], updatedTitle: '', updatedID: '', isFavourite:false};
  }

  ComponentWillMount() {
    this.UserList();
    if(JSON.parse(localStorage.getItem("favouritesID")) !== null){
      var NewfavouritesID = JSON.parse(localStorage.getItem("favouritesID"));
      console.log(NewfavouritesID);
      this.setState({'favouritesID': NewfavouritesID});
      console.log("AAAAAAA...."+this.state.favouritesID);
    }
  }

  AddedToFavourite(){
    var tempID = -1;
    if(JSON.parse(localStorage.getItem("favouritesID")) !== null){
      let copyFavouritesID = JSON.parse(localStorage.getItem("favouritesID"));
      console.log(copyFavouritesID);
      var len = copyFavouritesID.length;
      for(var i=0; i<len; i++){
        if(copyFavouritesID[i] === this.state.updatedID){
          tempID = i;
          console.log('TempID  '+tempID)
        }
      }
      if(tempID === -1){
        var favouritesID=this.state.favouritesID; favouritesID.push(this.state.updatedID);
        this.setState({'favouritesID': favouritesID});
        var favourites=this.state.favourites; favourites.push(this.state.updatedTitle);
        this.setState({'favourites': favourites});

        localStorage.setItem("favourites", JSON.stringify(this.state.favourites));
        localStorage.setItem("favouritesID", JSON.stringify(this.state.favouritesID));
      }
      else{
        isFavourite: false;
        var favourites=JSON.parse(localStorage.getItem("favourites"));
        console.log("TempList..."+favourites);
        favourites.splice(tempID, 1);
        console.log("NewList..."+favourites);
        localStorage.setItem("favourites", JSON.stringify(favourites));
        var favouritesID=JSON.parse(localStorage.getItem("favouritesID"));
        console.log("TempListID..."+favouritesID);
        favouritesID.splice(tempID, 1);
        console.log("NewListID..."+favouritesID);
        localStorage.setItem("favouritesID", JSON.stringify(favouritesID));
      }
    }
    else{
      var favouritesID=this.state.favouritesID; favouritesID.push(this.state.updatedID);
      this.setState({'favouritesID': favouritesID});
      var favourites=this.state.favourites; favourites.push(this.state.updatedTitle);
      this.setState({'favourites': favourites});

      localStorage.setItem("favourites", JSON.stringify(this.state.favourites));
      localStorage.setItem("favouritesID", JSON.stringify(this.state.favouritesID));
    }
  }

  UserList(){
    var url = "https://jsonplaceholder.typicode.com/posts";
    Request.get(url).then((response) => {
      this.setState({
        bodies: response.body,
      });
      console.log(this.state.bodies);
    });
  }

  BodyExpanssion(index){
    let current = this;
    let copyFavouritesID = JSON.parse(localStorage.getItem("favouritesID"));
    if(copyFavouritesID !== null){
      var len = copyFavouritesID.length;
      for(var i=0; i<len; i++){
        if(copyFavouritesID[i]=== index){
          console.log("11111111"+this.state.isFavourite);
          this.state.isFavourite = true;
          console.log("22222222"+this.state.isFavourite);
        }
        else{
          this.state.isFavourite = false;
          console.log("33333333"+this.state.isFavourite);
        }
      }
    }    
    if(this.state.isOpen === this.state.bodyDetails.id){
      this.setState({
        isOpen: -1,

      });
    }
    else{
      var url = "https://jsonplaceholder.typicode.com/posts/"+index;
      Request.get(url).then((response) => {
        current.setState({
          bodyDetails: response.body,
          isOpen: index,
          updatedTitle: response.body.title,
          updatedID: response.body.id
        });
      });
    }
  }

   render() {
     var bodies = _.map(this.state.bodies, (body) => {
       return <li key={body.id} className="block--Header" onClick={() => this.BodyExpanssion(body.id)}>
         <p>{body.title}</p>
         <span key={this.state.bodyDetails.id} className={(this.state.isOpen===body.id) ? '':'hidden'}>{this.state.bodyDetails.body}  
         <button onClick={this.AddedToFavourite.bind(this)}>
            {(!this.state.isFavourite) ? 'Favourite':'Remove'}
        </button></span>
     </li>;
     });
     const Comments = () => (
       <div>
         <h2>
           <span key={this.state.bodyDetails.id} className={(this.state.isOpen===this.state.bodies.body.id) ? '':'hidden'}>{this.state.bodyDetails.body}</span>
         </h2>
       </div>
     );

     var bodyDetails = (
       <div key={this.state.bodyDetails.id} class="block--Header">{this.state.bodyDetails.body}</div>
     );
      return (
         <div className="txt-center">
         <button className="customButton" onClick={this.ComponentWillMount.bind(this)}>
          Get Data
          </button>
         <ul>
           {bodies}
         </ul>
         </div>
      );
   }
}
export default GetData;
