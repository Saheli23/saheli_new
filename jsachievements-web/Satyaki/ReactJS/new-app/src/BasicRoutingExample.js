import React, { Component } from 'react';
import Request from 'superagent';
import _ from 'lodash';
import './style.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import GetData from './GetData';
import Favourites from './Favourites';


class BasicRoutingExample extends Component{
  constructor(props) {
    super(props);
    this.state = {bodies: [],bodyDetails: {},isOpen: -1};
  }

  render(){
    return <div>
    <Router>
      <div>
        <header className="headerSection">
          <h1>~My New ReactJS Project~</h1>
        </header>
        <ul>
          <li>
            <Link to="/">Get Data</Link>
          </li>
          <li>
            <Link to="/favourites">Favourites</Link>
          </li>
        </ul>

        <hr />

        <Route exact path="/" component={GetData} />
        <Route path="/favourites" component={Favourites} />
      </div>
    </Router>
    </div>
  }
}

// const GetData = () => (
//   <div>
//     <h2>Home</h2>
//   </div>
// );

// const Favourites = () => (
//   <div>
//     <h2>About</h2>
//   </div>
// );

const Topics = ({ match }) => (
  <div>
    <h2>Topics</h2>
    <ul>
      <li>
        <Link to={`${match.url}/rendering`}>Rendering with React</Link>
      </li>
      <li>
        <Link to={`${match.url}/components`}>Components</Link>
      </li>
      <li>
        <Link to={`${match.url}/props-v-state`}>Props v. State</Link>
      </li>
    </ul>

    <Route path={`${match.url}/:topicId`} component={Topic} />
    <Route
      exact
      path={match.url}
      render={() => <h3>Please select a topic.</h3>}
    />
  </div>
);

const Topic = ({ match }) => (
  <div>
    <h3>{match.params.topicId}</h3>
  </div>
);

export default BasicRoutingExample;
