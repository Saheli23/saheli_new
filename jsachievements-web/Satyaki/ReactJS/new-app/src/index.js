import React from 'react';
import ReactDOM from 'react-dom';
//import NameAdd from './NameAdd';
import BasicRoutingExample from './BasicRoutingExample'
import { BrowserRouter } from 'react-router-dom'

//ReactDOM.render(<NameAdd />, document.getElementById('root'));
ReactDOM.render(<BrowserRouter basename="/jsachievements-web/Satyaki/ReactJS/new-app/build/#/"><BasicRoutingExample /></BrowserRouter>, document.getElementById('root'));

// import React from 'react';
// import ReactDOM from 'react-dom';
// // import './index.css';
// import BasicRoutingExample from './BasicRoutingExample'
// import registerServiceWorker from './registerServiceWorker';
// import { BrowserRouter } from 'react-router-dom'



// ReactDOM.render(<BrowserRouter basename="/jsachievements-web/Satyaki/ReactJS/new-app/build/#/"><BasicRoutingExample/></BrowserRouter>, document.getElementById('root'));
// registerServiceWorker();
