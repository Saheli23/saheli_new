import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase';
import login from '@/components/login'
import registration from '@/components/registration'
import header from '@/components/header'
import footer from '@/components/footer'
import sidebar from '@/components/sidebar'
import dashboard from '@/components/dashboard'
import beforelogin from '@/components/beforelogin'
import afterlogin from '@/components/afterlogin'
import resetpassword from '@/components/resetpassword'
import success from '@/components/success'
import pagenotfound from '@/components/pagenotfound'


Vue.use(Router)

let router = new Router({
    // mode: 'history',
    routes: [{
            path: '*',
            name: 'pagenotfound',
            component: pagenotfound,
        },
        {
            path: '/',
            redirect: '/login',
        },
        {
            path: '/login',
            name: 'login',
            component: login,
        },
        {
            path: '/success',
            name: 'success',
            component: success,
        },
        {
            path: '/registration',
            name: 'registration',
            component: registration
        },
        {
            path: '/resetpassword',
            name: 'resetpassword',
            component: resetpassword
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: dashboard,
            meta: {
                requiresAuth: true
            }
        },
    ]
})

router.beforeEach((to, from, next) => {
    let currentUser = firebase.auth().currentUser;
    let requiresAuth = to.matched.some(record => record.meta.requiresAuth);

    if (requiresAuth && !currentUser) next('login')
    else if (!requiresAuth && currentUser) next('dashboard')
    else next()
        // if (from) {
        //     alert('hi');
        // } else {
        //     alert('hello');
        // }
});

export default router