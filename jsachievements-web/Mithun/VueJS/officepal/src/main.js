// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue';
import VeeValidate from 'vee-validate';
import Icon from 'vue-awesome/components/Icon'
import VueResource from 'vue-resource';
import firebase from 'firebase';

// Bootstrap
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'bootstrap/dist/css/bootstrap.css';

import 'vue-awesome/icons'

require('../static/css/style.css')



Vue.config.productionTip = false

Vue.use(BootstrapVue);
Vue.use(VeeValidate);
Vue.use(VueResource);

Vue.component('icon', Icon)

export const EventBus = new Vue();

// Initialize Firebase
let app;
let config = {
    apiKey: "AIzaSyCuWV7WY26lAa23IMTxO53UEoyQ3tqxmf0",
    authDomain: "officepal-64b92.firebaseapp.com",
    databaseURL: "https://officepal-64b92.firebaseio.com",
    projectId: "officepal-64b92",
    storageBucket: "officepal-64b92.appspot.com",
    messagingSenderId: "45126350058"
};
firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(function(user) {
    if (!app) {
        /* eslint-disable no-new */
        app = new Vue({
            el: '#app',
            template: '<App/>',
            components: { App },
            router,

        })
    }
})