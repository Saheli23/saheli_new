// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueSession from 'vue-session'
import VeeValidate from 'vee-validate'
import Pagination from 'vue-pagination'
import axios from 'axios'
import VueAxios from 'vue-axios'


Vue.config.productionTip = false

Vue.use(VueSession);
Vue.use(VeeValidate, {
    dictionary: {
        en: {
            custom: {
                name: {
                    required: 'Your name is empty'
                },
                email: {
                    required: 'Your email is empty',
                    //email: 'ssssss',
                },
                password: {
                    required: 'Your password is empty' // messages can be strings as well.
                },
                repassword: {
                    required: 'Your confirm password is empty',
                    confirmed: "Your password doesn't match",
                },
                checkbox: {
                    required: 'You must check above terms'
                },
                loginemail: {
                    required: 'Your email field is empty'
                },
                loginpassword: {
                    required: 'Your password is empty'
                }

            }
        }
    }
});
Vue.use(Pagination);
Vue.use(VueAxios, axios)

// CSS Included
require('../static/css/bootstrap.min.css');
require('../static/css/style.css')

// JS Included
require('../static/js/bootstrap.min.js')
require('../static/js/script.js')



/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: {
        App
    }
})
