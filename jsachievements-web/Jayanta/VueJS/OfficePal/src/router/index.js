import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index'
import LoginPage from '@/components/login'
import RegistrationPage from '@/components/registration'
import ForgotPassword from '@/components/forgotpassword'
import PageNotFound from '@/components/pagenotfound'
import ToggleStyle from '@/components/toggle'



Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'index',
            component: index
        },
        {
            path: '/login',
            name: 'LoginPage',
            component: LoginPage
        },
        {
            path: '/registration',
            name: 'RegistrationPage',
            component: RegistrationPage
        },
        {
            path: '/forgotpassword',
            name: 'ForgotPassword',
            component: ForgotPassword
        },
        {
            path: '/toggle',
            name: 'ToggleStyle',
            component: ToggleStyle
        },
        {
            path: "*",
            name: 'PageNotFound',
            component: PageNotFound
        }
  ]
})
