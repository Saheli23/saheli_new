$(document).on('ready', function () {
    var colors = ["red", "green", "yellow", "blue", "orange", "megenta", "cyan", "brown"];
    var colors2 = [];
    //$('.main-frame .color-block').length;
    function colorBlink() {
        $('.color-block').removeClass("red green yellow blue orange megenta cyan brown");
        //console.log($('.main-frame .color-block'))
        $.each($('.main-frame .color-block'), function (index, element) {
            var rand = Math.floor(Math.random() * colors.length);
            $(element).addClass(colors[rand]);
            colors2.push(colors[rand]);
            colors.splice(rand, 1);

            //console.log($(element));
        });
        $('.color-block').fadeTo(500, 0).fadeTo(700, 1);
        //$('.color-block').fadeTo(60, 0.1).fadeTo(80, 0.6).fadeTo(100, 0).fadeTo(600, 1.0);
        colors = colors2;
        colors2 = [];
    };

    colorBlink();
    setInterval(function () {
        colorBlink();
    }, 3000);
});
