$(document).on('ready', function () {


    function callthecloudleft() {
        var cloudspeed = Math.floor(Math.random() * 10000) + 2000,
            containerHeight = $('.container').height(),
            containerWidth = $('.container').width(),
            rancloudpos = Math.floor(Math.random() * containerHeight) + 280;

        $this = $('<div class="cloudimg"><img src="images/cloud1.png" /></div>');
        $this.appendTo('.main-frame');
        var cloudposition = $('.cloudimg').offset().left;
        //console.log(cloudposition);
        $this.css({
            //left: containerWidth,
            //top: Math.floor(Math.random() * containerHeight),
        }).animate({
            left: -250,
            width: '100%',
            opacity: 0,
        }, {
            duration: cloudspeed,
            complete: function () {
                $(this).remove();
            }
        });
    };

    function callthecloudright() {
        var cloudspeed = Math.floor(Math.random() * 10000) + 2000,
            containerHeight = $('.container').height(),
            containerWidth = $('.container').width(),
            rancloudpos = Math.floor(Math.random() * containerHeight) + 280;

        $this = $('<div class="cloudimg"><img src="images/cloud1.png" /></div>');
        $this.appendTo('.main-frame');
        var cloudposition = $('.cloudimg').offset().left;
        //console.log(cloudposition);
        $this.css({
            //left: containerWidth,
            //top: Math.floor(Math.random() * containerHeight),
        }).animate({
            left: containerWidth + 200,
            width: '100%',
            opacity: 0,
        }, {
            duration: cloudspeed,
            complete: function () {
                $(this).remove();
            }
        });
    };

    function callthecloudtop() {
        var cloudspeed = Math.floor(Math.random() * 8000) + 1000,
            containerHeight = $('.container').height(),
            containerWidth = $('.container').width(),
            rancloudpos = Math.floor(Math.random() * containerHeight) + 280;

        $this = $('<div class="cloudimg cloudlarge"><img src="images/cloud1.png" /></div>');
        $this.appendTo('.main-frame');
        var cloudposition = $('.cloudimg').offset().left;
        //console.log(cloudposition);
        $this.css({
            //left: containerWidth,
            //top: Math.floor(Math.random() * containerHeight),
        }).animate({
            left: containerWidth - 250,
            width: '100%',
            opacity: 0,
        }, {
            duration: cloudspeed,
            complete: function () {
                $(this).remove();
            }
        });
    };

    setInterval(function () {
        // Run Function every .1 seconds
        callthecloudtop();
        callthecloudleft();
        callthecloudright();
    }, 2000);



});
