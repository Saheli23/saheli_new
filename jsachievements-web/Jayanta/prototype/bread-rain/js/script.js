$(document).on('ready', function () {
    
    // Randome Bread drop
    function calltherain() {
        var randomspeed = Math.floor(Math.random() * 2500) + 500;
        //var randomspeed = 1000;
        //var rainspeed = Math.floor(Math.random() * 20);
        var containerWidth = $('.main-frame').width();
        //console.log(rainspeed);
        var raindrops = Math.floor(Math.random() * containerWidth);

        $this = $('<div class="fromleft rain-drops"><img src="images/bread-img.png" /></div>');
        $this.appendTo('.bread-rain');
        $this.css({
            left: raindrops,
        }).animate({
            bottom: -50,
            left: (raindrops - 150),
        }, {
            duration: randomspeed,
            complete: function () {
                $(this).remove();
            }
        });
       
    };
    
    // Constant Bread drop
    function calltheconstantrain() {
        //var randomspeed = Math.floor(Math.random() * 1000) + 500;
        var randomspeed = 1500;
        //var rainspeed = Math.floor(Math.random() * 20);
        var containerWidth = $('.main-frame').width();
        //console.log(rainspeed);
        var raindrops = Math.floor(Math.random() * containerWidth);

        $this = $('<div class="fromleft rain-drops2"><img src="images/bread-img.png" /></div>');
        $this.appendTo('.bread-rain');
        $this.css({
            left: raindrops,
        }).animate({
            bottom: -50,
            left: (raindrops - 150),
        }, {
            duration: randomspeed,
            complete: function () {
                $(this).remove();
            }
        });
    };
    
    setInterval(function() {
      // Run Function every .1 seconds
        calltherain();
        calltheconstantrain();
    }, 200);

    
});
