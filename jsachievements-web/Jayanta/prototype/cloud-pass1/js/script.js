$(document).on('ready', function () {
    // from right Cloud Pass
    function callthecloud() {
        var cloudspeed = Math.floor(Math.random() * 10000) + 2000,
            containerHeight = $('.container').height(),
            containerWidth = $('.container').width(),
            rancloudpos = Math.floor(Math.random() * containerHeight) + 280;

        $this = $('<div class="cloudimg"><img src="images/cloud1.png" /></div>');
        $this.appendTo('.main-frame');
        var cloudposition = $('.cloudimg').offset().left;
        console.log(cloudposition);
        $this.css({
            left: containerWidth,
            top: Math.floor(Math.random() * containerHeight),
        }).animate({
            left: -250,
        }, {
            duration: cloudspeed,
            complete: function () {
                $(this).remove();
            }
        });
    };

    // from left Cloud Pass
    function callthecloudleft() {
        var cloudspeed = Math.floor(Math.random() * 25000) + 2000,
            containerHeight = $('.container').height(),
            containerWidth = $('.container').width(),
            rancloudpos = Math.floor(Math.random() * containerHeight) + 280;

        $this = $('<div class="cloudimg cloudsmall"><img src="images/cloud1.png" /></div>');
        $this.appendTo('.main-frame');
        var cloudposition = $('.cloudimg').offset().left;
        console.log(cloudposition);
        $this.css({
            left: -250,
            top: Math.floor(Math.random() * containerHeight),
        }).animate({
            left: containerWidth + 250,
        }, {
            duration: cloudspeed,
            complete: function () {
                $(this).remove();
            }
        });
    };

    setInterval(function () {
        // Run Function every .1 seconds
        callthecloud();
        callthecloudleft();
    }, 2000);

});
