import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
   <h1 style="text-align:center">{{title}}</h1>
   <nav style="text-align:center">
     <a routerLink="/dashboard">Dashboard</a>
     <a routerLink="/heroes">Heroes</a>
   </nav>
   <router-outlet></router-outlet>
 `
})
export class AppComponent {
  title = 'League of Spectacular Heroes';
}