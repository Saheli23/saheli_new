 import { Hero } from './hero';

 export const HEROES: Hero[] = [
  { id: 1, name: 'Naruto', desc: "hero of world of ninjas" },
  { id: 2, name: 'Minato', desc: "father of naruto" },
  { id: 3, name: 'Midoriya', desc: "new host of one for all quirk, worlds greatest superhero" },
  { id: 4, name: 'Ichigo', desc: "savior ofhuman and shinigami world" },
  { id: 5, name: 'Batman', desc: "aka bruce wayne,vigilanti cape cruseder of gotham" },
  { id: 6, name: 'Luffy', desc: "best pirate captain and rubber man monkey d luffy" },
  { id: 7, name: 'Boruto', desc: "son of naruto" },
  { id: 8, name: 'Dr IQ', desc: "iq level 100" },
  { id: 9, name: 'Magma', desc: "all" },
  { id: 10, name: 'Tornado', desc: "all" }
];
