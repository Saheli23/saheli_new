(function(window, document, $, undefined){

	

	

// Main Application

var App = {

	

	/****************************************************

	*

	* 	GLOBALS

	*

	****************************************************/

	globals: {

		winobj : { // window object

			window : $(window),

			winW : '',

			winH : '',

			scrollPos : '',

			hash : $(location).attr('hash')

		},

		screenType : { // screentype object

			desktop : false,

			tablet : false,

			mobile : false

		},

		preloader : { // preloader object

			main : $('#preloader'),

			loadingbar : $('#preloader .progress span'),

			manifest : [

				"images/green-pattern.png",

				"images/slide-2.jpg",

				"images/slide-1.jpg",

				"images/slide-3.jpg",

				"images/green-pattern-tilted.png",

				"images/white-pattern-tilted.png",

				"images/iphone.png",

				"images/iphone-1.png",

				"images/iphone-2.png",

				"images/iphone-3.png",

				"images/iphone-4.png",

				"images/iphone-5.png",

				"images/iphone-6.png",

				"images/iphone-1.jpg",

				"images/iphone-2.jpg",

				"images/iphone-3.jpg",

				"images/iphone-4.jpg",

				"images/iphone-5.jpg",

				"images/iphone-6.jpg",
				"images/tablet_phoneimage_1.png",
				"images/tablet_phoneimage_2.png",
				"images/tablet_phoneimage_3.png",
				"images/tablet_phoneimage_4.png",
				"images/tablet_phoneimage_5.png",
				"images/tablet_phoneimage_6.png",

				"images/user-story-bg-1.jpg",

				"images/user-story-bg-2.jpg",

				"images/user-story-bg-3.jpg",

				"images/btn-app-store.png",

				"images/btn-google-play.png"

			]

		},

		banner : { // banner object

			selector : $('#bannerSlides'),

			parent : $('.banner-wrap')

		},

		customScroll : { // custom scroll object

			scTime : 0.5,

			scDistance : 170,

			manualScrolltoDistance : 0

		},

		mainNavButtons : { // nav object

			navFeature : $('.nav-feature'),

			navUserStory : $('.nav-user-story'),

			navDownload : $('.nav-download')

		},

		header : { // header obj

			main : $('#pageHeader')

		},

		features : { // features obj

			main : $('#features'),

			featureTrigger : $('#feature-wrapper'),

			featureWrap : $('#feature-wrapper .wrapper'),

			theIPhone : $('#iphone-wrap'),

			theIPhoneHeight : 742,

			singleText : $('#feature-wrapper .single'),

			appSlides : $('#feature-wrapper .slides'),

			featureNav : $('#feature-wrapper .nav-buttons'),

			featureMid : $('#feature-wrapper .mid-phone'),

			featureRight : $('#feature-wrapper .right-content'),

			featureNavScrollPos : [0, 0, 0, 0, 0, 0],

			featureSlideContainer : $('#feature-wrapper .slide-container'),

			features : []

		},

		userStory : { // user story

			main : $('#userStory'),

			storyTrigger : $('#user-story-trigger'),

			storyWrap : $('#user-story-trigger .wrapper'),

			userPin : $('#user-story-trigger .user-pin'),

			storyLeft : $('#user-story-trigger .left'),

			storyLeftW : $('#user-story-trigger .left').outerWidth(),

			tabMargin : 70,

			tabNav : $('#user-story-trigger .tab-nav'),

			slides : $('#user-story-trigger .slides'),

			slideTimeStamp : $('#user-story-trigger .time-stamp'),

			parallax : $('#user-story-trigger .parallax'),

			tiltSpace : 100,

			tabNavScrollPos : [0, 0, 0],

			feedContainer : $('#feeds'),

			feeds : []

		}

	},

	/****************************************************

	*

	* 	END GLOBALS

	*

	****************************************************/

	

	

	/****************************************************

	*

	* 	INITIATE

	*

	****************************************************/

	init: function(){

		

		// call preloader

		App.appPreloader();

		

		// assign available values

		App.globals.winobj.winW = $(App.globals.winobj.window).width(); // get window width

		App.globals.winobj.winH = $(App.globals.winobj.window).height(); // get window width

		App.globals.winobj.scrollPos = $(App.globals.winobj.window).scrollTop(); // get window scroll top

		//console.log('WinW:'+App.globals.winobj.winW+'\nWinH:'+App.globals.winobj.winH);

		

		// detect screentype

		if(App.globals.winobj.winW >= 1200){ // detect desktop

			App.globals.screenType.desktop = true;

			//console.log('Screen Type - Desktop tablet mobile : '+App.globals.screenType.desktop+' '+App.globals.screenType.tablet+' '+App.globals.screenType.mobile);

		}

		else if(App.globals.winobj.winW > 767 && App.globals.winobj.winW < 1200){ // detect tablet

			App.globals.screenType.tablet = true;

			//console.log('Screen Type - Desktop tablet mobile : '+App.globals.screenType.desktop+' '+App.globals.screenType.tablet+' '+App.globals.screenType.mobile);

		}

		else if(App.globals.winobj.winW <= 767){ // detect mobile

			App.globals.screenType.mobile = true;

			//console.log('Screen Type - Desktop tablet mobile : '+App.globals.screenType.desktop+' '+App.globals.screenType.tablet+' '+App.globals.screenType.mobile);

		}

		

		// call application controller

		App.appController();

		

	},

	/****************************************************

	*

	* 	END INITIATE

	*

	****************************************************/

	

	

	/****************************************************

	*

	* 	PRELOADER

	*

	****************************************************/

	appPreloader: function(){

		var queue = new createjs.LoadQueue(true, null, true);

		queue.loadManifest(App.globals.preloader.manifest);

		

		queue.on("complete", function(){

			$(App.globals.preloader.main).delay(800).fadeOut(400).remove();

		}, this);

		

		queue.on("progress", function(e){

			$(App.globals.preloader.loadingbar).stop().animate({width: ((e.progress*100)*2)+'%'}, 400);

			//console.log(e.progress);

			//console.log((e.progress*100)*2);

		});

		

		queue.on("error", function(e){

			console.log("error");

		});

	},

	/****************************************************

	*

	* 	END PRELOADER

	*

	****************************************************/

	

	

	/****************************************************

	*

	* 	APP CONTROLLER

	*

	****************************************************/

	appController: function(){

		

		if(App.globals.screenType.desktop){

			App.viewDesktop(); // destop view handler

		}

		else if (App.globals.screenType.tablet){

			App.viewTabletMobile(); // tablet view handler

		}

		else{

			App.viewTabletMobile(); // mobile view handler

		}

		

	},

	/****************************************************

	*

	* 	END APP CONTROLLER

	*

	****************************************************/

	

	

	/****************************************************

	*

	* 	DESKTOP VIEW

	*

	****************************************************/

	viewDesktop: function(){

		

		//console.log("DesktopView Activate");

		

		controller = new ScrollMagic();

		

		// assign new scroll amount and new scroll time (if needed) and call custom scoll

		App.globals.customScroll.scDistance = 150;

		App.customScroll();

		// END assign new scroll amount and new scroll time (if needed) and call custom scoll

		

		// doc ready handlers

		$(document).ready(function(e) {

			

			// reset scroll top

			$(App.globals.winobj.window).scrollTop(0,0);

            

			// activate banner slider

			$(document).on('animating.slides', App.globals.banner.selector, function() {

				$('body').find('.bannerText').each(function(index, element) {

                    $(this).addClass('animating');

                });

			});

			

			$(App.globals.banner.selector).superslides({

				play : 7000,

				inherit_width_from: App.globals.banner.parent,

				inherit_height_from: App.globals.banner.parent

			});

			

			$(document).on('animated.slides', App.globals.banner.selector, function() {

				$('body').find('.bannerText').each(function(index, element) {

                    $(this).removeClass('animating');

                });

			});

			// END activate banner slider

			

			// activate promo modal

			App.promoModal();

			// END activate promo modal

			

			// check if scrolling divs exist and activate scroll controller accordingly

			if ( $(App.globals.features.main).length ){ 

				App.viewDesktopFeatureScroll(); // features scroll

			}

			if ( $(App.globals.userStory.main).length ){ 

				App.viewDesktopUserstoryScroll(); // user story scroll

				App.socialFeedController(); // social feed controller

			} 

			// END check if scrolling divs exist and activate scroll controller accordingly

			

			// top nav scroll

			$(App.globals.mainNavButtons.navFeature).on('click', function(e){

				$(App.globals.winobj.window).scrollTop(App.globals.features.featureNavScrollPos[0]);

			});

			

			$(App.globals.mainNavButtons.navUserStory).on('click', function(e){

				$(App.globals.winobj.window).scrollTop(App.globals.userStory.tabNavScrollPos[0]);

			});

			

			$(App.globals.mainNavButtons.navDownload).on('click', function(e){

				$(App.globals.winobj.window).scrollTop($('#download').offset().top);

			});

			// END top nav scroll

			

			// back to top

			$('.backtotop').on('click', function(e){

				$(App.globals.winobj.window).scrollTop(0);

			});

			

			if($('body').hasClass('home')){

				$('#logo').on('click', function(e){

					e.preventDefault();

					$(App.globals.winobj.window).scrollTop(0);

				});

			}

			// END back to top

			

			// add margin top in download section to have parallaz effect

			if($('body').hasClass('home')){

				$('#download').css({marginTop: -($('#download').outerHeight() + $('#pageFooter').outerHeight())+'px'});

			}

			// END add margin top in download section to have parallaz effect

			

        });

		// END doc ready handlers

		

		// window load handlers

		$(App.globals.winobj.window).on('load', function(e){

			

			// hash scroll

			var appHash = App.globals.winobj.hash;

			if(appHash == '#feature-wrapper'){

				//console.log(App.globals.features.featureNavScrollPos[0]);

				$(App.globals.winobj.window).scrollTop(App.globals.features.featureNavScrollPos[0]);

				//console.log($(App.globals.winobj.window).scrollTop());

			}

			if(appHash == '#userStory'){

				$(App.globals.winobj.window).scrollTop(App.globals.userStory.tabNavScrollPos[0]);

			}

			if(appHash == '#download'){

				$(App.globals.winobj.window).scrollTop($('#download').offset().top);

			}

			// END hash scroll

			

		});

		// END window load handlers

		

		// window scroll handlers

		$(App.globals.winobj.window).on('scroll', function(e){

			

			//console.log('Window Scrolled');

			

			// update scroll position

			App.globals.winobj.scrollPos = $(App.globals.winobj.window).scrollTop(); // update window scroll top

			// END update scroll position

			

			// fix the header if scroll pos greater than first panel

			if($('body').hasClass('home')){

				if(App.globals.winobj.scrollPos >= App.globals.winobj.winH/2){

					$(App.globals.header.main).addClass('fixed');
				}

				else{

					$(App.globals.header.main).removeClass('fixed');

				}

			}

			else{

				if(App.globals.winobj.scrollPos >= 100){
					$(App.globals.header.main).addClass('fixed');

				}

				else{

					$(App.globals.header.main).removeClass('fixed');

				}

			}

			// END fix the header

			

		});

		// END window scroll handlers

		

		// window resize handlers

		$(App.globals.winobj.window).on('resize', function(e){

			location.reload();

		});

		// END window resize handlers

		

	},

	/****************************************************

	*

	* 	END DESKTOP VIEW

	*

	****************************************************/

	

	

	/****************************************************

	*

	* 	TABLET VIEW

	*

	****************************************************/

	viewTabletMobile: function(){

		

		//console.log("TabletView Activate");

		

		// assign new scroll amount and new scroll time (if needed) and call custom scoll

		App.globals.customScroll.scDistance = 150;

		App.customScroll();

		// END assign new scroll amount and new scroll time (if needed) and call custom scoll

		

		// doc ready handlers

		$(document).ready(function(e) {

			

			// reset scroll top

			$(App.globals.winobj.window).scrollTop(0,0);

			

			// mobile menu

			$('.mob-menu-trigger').on('click', function(e){

				$('#pageHeader .left #mainNav').toggleClass('active');

			});

            

			// activate banner slider

			$(App.globals.banner.parent).animate({height: (App.globals.winobj.winH + 151)}, 0, function(){

				$(App.globals.banner.selector).find('li').each(function(index, element) {

					$(this).find('.bannerText').removeClass('left').addClass('center');

				});

				

				$(document).on('animating.slides', App.globals.banner.selector, function() {

					$('body').find('.bannerText').each(function(index, element) {

						$(this).addClass('animating');

					});

				});

				

				$(App.globals.banner.selector).superslides({

					play : 7000,

					inherit_width_from: App.globals.banner.parent,

					inherit_height_from: App.globals.banner.parent

				});

				

				$(document).on('animated.slides', App.globals.banner.selector, function() {

					$('body').find('.bannerText').each(function(index, element) {

						$(this).removeClass('animating');

					});

				});

				

				$(App.globals.banner.selector).swipeleft(function() {

					$(App.globals.banner.selector).data('superslides').animate('next');

				});

				

				$(App.globals.banner.selector).swiperight(function() {

					$(App.globals.banner.selector).data('superslides').animate('prev');

				});

			});

			// END activate banner slider

			

			// activate slick for feature section

			App.tabMobileRTB();

			// END activate slick for feature section

			

			// Activate slick for user story

			$('#user-story-trigger .left .content .tab-nav a.story-1').addClass('active');

			

			$('#user-story-trigger .left .content .slides-container').slick({

				accessibility: false,

				adaptiveHeight :true,

				arrows : false

			});

			

			$('#user-story-trigger .left .content .slides-container').on('init', function(event, slick){

			  $('#user-story-trigger .left .content .tab-nav a.story-1').addClass('active');

			});

			

			$('#user-story-trigger .left .content .slides-container').on('afterChange', function(event, slick, currentSlide){

			  //console.log(currentSlide);

			  if(currentSlide == 0){

				  $('#user-story-trigger .left .content .tab-nav a.story-1').addClass('active');

				  $('#user-story-trigger .left .content .tab-nav a.story-2').removeClass('active');

				  $('#user-story-trigger .left .content .tab-nav a.story-3').removeClass('active');

			  }

			  if(currentSlide == 1){

				  $('#user-story-trigger .left .content .tab-nav a.story-1').removeClass('active');

				  $('#user-story-trigger .left .content .tab-nav a.story-2').addClass('active');

				  $('#user-story-trigger .left .content .tab-nav a.story-3').removeClass('active');

			  }

			  if(currentSlide == 2){

				  $('#user-story-trigger .left .content .tab-nav a.story-1').removeClass('active');

				  $('#user-story-trigger .left .content .tab-nav a.story-2').removeClass('active');

				  $('#user-story-trigger .left .content .tab-nav a.story-3').addClass('active');

			  }

			});

			

			$('#user-story-trigger .left .content .tab-nav a.story-1').on('click', function(e){

				$('#user-story-trigger .left .content .slides-container').slick('slickGoTo', 0)

			});

			$('#user-story-trigger .left .content .tab-nav a.story-2').on('click', function(e){

				$('#user-story-trigger .left .content .slides-container').slick('slickGoTo', 1)

			});

			$('#user-story-trigger .left .content .tab-nav a.story-3').on('click', function(e){

				$('#user-story-trigger .left .content .slides-container').slick('slickGoTo', 2)

			});

			// END Activate slick for user story

			

			// activate promo modal

			App.promoModal();

			// END activate promo modal

			

			App.socialFeedController(); // social feed controller

			

			// top nav scroll

			$(App.globals.mainNavButtons.navFeature).on('click', function(e){

				var targertOffset = $(App.globals.features.featureWrap).offset().top - $('#pageHeader').outerHeight();

				$('body, html').animate({scrollTop : targertOffset}, 500, 'easeOutBack', function(e){

					$('#pageHeader .left #mainNav').removeClass('active');

				});

			});

			

			$(App.globals.mainNavButtons.navUserStory).on('click', function(e){

				var targertOffset = $(App.globals.userStory.main).offset().top - $('#pageHeader').outerHeight();

				$('body, html').animate({scrollTop : targertOffset}, 500, 'easeOutBack', function(e){

					$('#pageHeader .left #mainNav').removeClass('active');

				});

			});

			

			$(App.globals.mainNavButtons.navDownload).on('click', function(e){

				var targertOffset = $('#download').offset().top - $('#pageHeader').outerHeight();

				$('body, html').animate({scrollTop : targertOffset}, 500, 'easeOutBack', function(e){

					$('#pageHeader .left #mainNav').removeClass('active');

				});

			});

			// END top nav scroll

			

			// back to top

			$('.backtotop').on('click', function(e){

				$(App.globals.winobj.window).scrollTop(0);

			});

			

			if($('body').hasClass('home')){

				$('#logo').on('click', function(e){

					e.preventDefault();

					$(App.globals.winobj.window).scrollTop(0);

				});

			}

			// END back to top

			

        });

		// END doc ready handlers

		

		// window load handlers

		$(App.globals.winobj.window).on('load', function(e){

			

			// smart banner

			$().smartWebBanner({

				title: "Last Minute Golfer", // What the title of the "app" should be in the banner | Default: "Web App"

				titleSwap: true, // Whether or not to use the title specified here has the default label of the home screen icon (otherwise uses the page's <title> tag) | Default: true

				url: '/', // URL to mask the page as before saving to home screen (allows for having it save the homepage of a site no matter what page the visitor is on) | Default: ""

				author: "Golf Channel", // What the author of the "app" should be in the banner | Default: "Save to Home Screen"

				speedIn: 300, // Show animation speed of the banner | Default: 300

				speedOut: 400, // Close animation speed of the banner | Default: 400

				useIcon: true, // Whether or not it should show site's apple touch icon (located via <link> tag) | Default: true

				iconOverwrite: "images/LMG_Icon.png", // Force the URL of the icon (even if found via <link> tag) | Default: ""

				iconGloss: "auto", // Whether or not to show the gloss over the icon (true/false/"auto" [auto doesn't show if precomposed <link> tag is used]) | Default: "auto"

				showFree: true, // Whether or not to show "Free" at bottom of info | Default: true

				daysHidden: 15, // Duration to hide the banner after being closed (0 = always show banner) | Default: 15

				daysReminder: 90, // Duration to hide the banner after "Save" is clicked *separate from when the close button is clicked* (0 = always show banner) | Default: 90

				popupDuration: 6000, // How long the instructions are shown before fading out (0 = show until manually closed) | Default: 6000

				popupSpeedIn: 200, // Show animation speed of the popup | Default: 200

				popupSpeedOut: 900, // Close animation speed of the popup | Default: 900

				theme: "iOS 7", // Change between "auto", "iOS 7", "iOS 6" & "dark" themes to fit your site design | Default: "auto"

				autoApp: false, // Whether or not it should auto-add the mobile-web-app meta tag that makes it open as an app rather than in mobile safari | Default: false

				debug: true // Whether or not it should always be shown (even for non-iOS devices & if cookies have previously been set) *This is helpful for testing and/or previewing | Default: false

			});

			// END smart banner

			

			// hash scroll

			var appHash = App.globals.winobj.hash;

			if(appHash == '#feature-wrapper'){

				//console.log(App.globals.features.featureNavScrollPos[0]);

				$(App.globals.winobj.window).scrollTop(App.globals.features.featureNavScrollPos[0]);

				//console.log($(App.globals.winobj.window).scrollTop());

			}

			if(appHash == '#userStory'){

				$(App.globals.winobj.window).scrollTop(App.globals.userStory.tabNavScrollPos[0]);

			}

			if(appHash == '#download'){

				$(App.globals.winobj.window).scrollTop($('#download').offset().top);

			}

			// END hash scroll

			

		});

		// END window load handlers

		

		// window resize handlers

		$(App.globals.winobj.window).on('resize', function(e){

			//location.reload();

			

			$(App.globals.banner.parent).animate({height: ($(App.globals.winobj.window).height() + 151)}, 0);

		});

		// END window resize handlers

		

		// window scroll handlers

		$(App.globals.winobj.window).on('scroll', function(e){

			

			//console.log('Window Scrolled');

			

			// update scroll position

			App.globals.winobj.scrollPos = $(App.globals.winobj.window).scrollTop(); // update window scroll top

			// END update scroll position

			

			// fix the header if scroll pos greater than first panel

			if($('body').hasClass('home')){

				if(App.globals.winobj.scrollPos >= (App.globals.winobj.winH/2 - 200)){


					$(App.globals.header.main).addClass('fixed');

				}

				else{

					$(App.globals.header.main).removeClass('fixed');

				}

			}

			else{

				if(App.globals.winobj.scrollPos >= 25){

					$(App.globals.header.main).addClass('fixed');

				}

				else{

					$(App.globals.header.main).removeClass('fixed');

				}

			}

			// END fix the header

			

		});

		// END window scroll handlers

		

	},

	/****************************************************

	*

	* 	END TABLET VIEW

	*

	****************************************************/

	

	

	/****************************************************

	*

	* 	DESKTOP VIEW SCROLL CONTROLLER FOR FEATURE SECTION

	*

	****************************************************/

	viewDesktopFeatureScroll: function(){

		

		//console.log("DesktopView Feature Scroll");

		

		var windowHeight = App.globals.winobj.winH,

			singleText = App.globals.features.singleText,

			numOfSingleText = singleText.length,

			appSlides = App.globals.features.appSlides,

			offset = $(App.globals.features.featureTrigger).offset().top;

			

			//console.log('singleTextLength: '+numOfSingleText);

			

			// set single text height and other containers height

			$(singleText).height(windowHeight);

			$(App.globals.features.featureNav).height(windowHeight);

			$(App.globals.features.featureMid).height(windowHeight);

			$(App.globals.features.featureRight).height(windowHeight);

			

			// scale phone image if larger than window height

			if(App.globals.features.theIPhoneHeight > windowHeight){

				var scaleVal = parseFloat(windowHeight / App.globals.features.theIPhoneHeight).toFixed(2);

				scaleVal -= 0.05;

				TweenMax.fromTo( App.globals.features.theIPhone, 0, { scale: 1}, { scale: scaleVal, ease:Linear.easeNone})

			}

			

			// create new scene for pin setting and trigger when feature is at the top

			var scene = new ScrollScene({

				triggerElement : App.globals.features.featureTrigger,

				triggerHook: 0,

				duration: windowHeight*(numOfSingleText)

			})

			.setPin(App.globals.features.featureWrap)

			.addTo(controller)

			.on('enter', function(event){

				App.updateMainNav(App.globals.mainNavButtons.navFeature);

			})

			.on('leave', function(event){

				$(App.globals.mainNavButtons.navFeature).removeClass('active');

				

				$('#features .nav-buttons').find('.active').removeClass('active');

			});

			// END create new scene for pin setting and trigger when feature is at the top

			

			// code for adding tweens in each single text | animating app slides

			$.each(singleText, function(index, singleText){

				var singleText = $(singleText),

					content = $(singleText).find('.content-inner'),

					appSlide = '#features .mid-phone .slide-container .slides.feature-',

					featureNav = '#features .nav-buttons a.feature-',

					opacity = (index == 0) ? 1 : 0,

					time = index == 5 ? 0.25 : 0.5,

					repeat = (index != (numOfSingleText - 1)) ? 1 : 0;

				

				// define tween	

				var tween = new TimelineMax()

					.add([

						TweenMax.fromTo( singleText, 0.25, { opacity: 0, visibility: 'hidden' }, { opacity: 1, visibility: 'visible' }),

						TweenMax.fromTo( content, 2, { marginTop: (windowHeight*0.7)+'px' }, {marginTop: 0, ease: Linear.easeNone }),

						TweenMax.fromTo( content, 0.5, { opacity: 0 }, {opacity: 1, ease: Linear.easeNone, repeat: 1, repeatDelay: 1, yoyo:true, ease: Linear.easeNone })

					]);

					

				// define scene

				var scene = new ScrollScene({

					duration: windowHeight,

					offset: windowHeight*(index+1)

				})

				.addTo(controller)

				.setTween(tween)

				.on('enter', function(event){

					

					$(appSlide+(index+1)).removeClass('done'); // animate the app image in

					$(featureNav+(index+1)).siblings().removeClass('active');

					$(featureNav+(index+1)).addClass('active'); // update feature nav

					

					//console.log(App.globals.features.featureNavScrollPos);

					

				})

				.on('leave', function(event){

					

					if ( event.scrollDirection != 'REVERSE' ) { // if scrolling to top

						if(index != (numOfSingleText - 1)){ // if not last app image

							$(appSlide+(index+1)).addClass('done'); // animate the app image out

						}

					}

					

				});

				

				// update feature nav scrollpos array for individual click scroll

				if(index == (numOfSingleText - 1)){

					App.globals.features.featureNavScrollPos[index] = scene.scrollOffset() + windowHeight/1.5;

				}

				else{

					App.globals.features.featureNavScrollPos[index] = scene.scrollOffset() + windowHeight/1.5;

				}

				

			});

			// END code for adding tweens in each single text | animating app slides

			

			// attach click handlers for feature nav buttons

			$(App.globals.features.featureNav).find('a').each(function(index, element) {

                $(this).on('click', function(e){

					$(window).scrollTop(App.globals.features.featureNavScrollPos[index]);

					$(this).siblings('a').removeClass('active');

					$(this).addClass('active');

				});

            });

			// END attach click handlers for feature nav buttons

		

	},

	/****************************************************

	*

	* 	END DESKTOP VIEW SCROLL CONTROLLER FOR FEATURE SECTION

	*

	****************************************************/

	

	

	/****************************************************

	*

	* 	DESKTOP VIEW SCROLL CONTROLLER FOR USER STORY SECTION

	*

	****************************************************/

	viewDesktopUserstoryScroll: function(){

		

		//console.log("DesktopView User Story Scroll");

		

		var windowHeight = App.globals.winobj.winH,

			windowWidth = App.globals.winobj.winW,

			tabNav = App.globals.userStory.tabNav,

			slides = App.globals.userStory.slides,

			parallax = App.globals.userStory.parallax,

			slideTimeStamp = App.globals.userStory.slideTimeStamp,

			numOfslides = slides.length,

			offset = $(App.globals.userStory.storyTrigger).offset().top;

			

			//console.log('singleTextLength: '+numOfSingleText);

			

			// set single text height and other containers height

			$(slides).height(windowHeight);

			$(parallax).height(windowHeight+App.globals.userStory.tiltSpace); // add tilt space else call - $(parallax).height(windowHeight);

			$(App.globals.userStory.storyLeft).height(windowHeight+App.globals.userStory.tiltSpace); // add tilt space else call - $(App.globals.userStory.storyLeft).height(windowHeight);

			

			// set tab top and timestamp position

			var tabH = $(tabNav).outerHeight();

				tabMargin = App.globals.userStory.tabMargin;

				topBottomValue = parseInt((windowHeight - (tabH+tabMargin))/2);

			$(tabNav).css({paddingTop: topBottomValue});

			$(slideTimeStamp).each(function(index, element) {

                $(this).css({bottom: parseInt(topBottomValue-tabMargin)});

            });

			

			// set parallax background and width

			$(parallax).each(function(index, element) {

				$(this).width(parseFloat(App.globals.userStory.storyLeftW + ((windowWidth - $(App.globals.userStory.userPin).width())/2)));

                $(this).css({backgroundImage: 'url('+$(this).children('img').attr('src')+')'});

            });

			

			// create new scene for pin setting and trigger when user story is at the top

			var scene = new ScrollScene({

				triggerElement : App.globals.userStory.storyTrigger,

				triggerHook: 0,

				duration: windowHeight*(numOfslides+2)

			})

			.setPin(App.globals.userStory.storyWrap)

			.addTo(controller)

			.on('enter', function(event){

				App.updateMainNav(App.globals.mainNavButtons.navUserStory);

				

				// animate download panel elements

				/*TweenMax.fromTo( $('#download p'), 0.5, { opacity: 1, y: 0 }, { opacity: 0, y: 20, ease:Linear.easeNone })

				TweenMax.fromTo( $('#download .ios'), 0.5, { opacity: 1, x: 0 }, { opacity: 0, x: -100, ease:Linear.easeNone })

				TweenMax.fromTo( $('#download .android'), 0.5, { opacity: 1, x: 0 }, { opacity: 0, x: 100, ease:Linear.easeNone })*/

			})

			.on('leave', function(event){

				$(App.globals.mainNavButtons.navUserStory).removeClass('active');

				if ( event.scrollDirection != 'REVERSE' ) { // if scrolling to top

					App.updateMainNav(App.globals.mainNavButtons.navDownload);

					

					$('#user-story-trigger .left .content .tab-nav').find('.active').removeClass('active');

					

					// animate download panel elements

					/*TweenMax.fromTo( $('#download p'), 0.3, { opacity: 0, y: 20 }, { opacity: 1, y: 0, delay: 0.2, ease:Linear.easeNone })

					TweenMax.fromTo( $('#download .ios'), 0.3, { opacity: 0, x: -100 }, { opacity: 1, x: 0, delay: 0.2, ease:Back.easeOut.config(1.7) })

					TweenMax.fromTo( $('#download .android'), 0.3, { opacity: 0, x: 100 }, { opacity: 1, x: 0, delay: 0.2, ease:Back.easeOut.config(1.7) })*/

				}

			})

			.on("progress", function (event) {

				//console.log("User Scene progress changed to " + event.progress);

				if(event.progress >= 0.84){

					$('#download').addClass('active');

					$('#userStory').addClass('lock');

				}

				else{

					$('#download').removeClass('active');

					$('#userStory').removeClass('lock');

				}

			});

			// END create new scene for pin setting and trigger when feature is at the top

			

			// code for adding tweens in each single text | animating app slides

			$.each(slides, function(index, slides){

				var slides = $(slides),

					parallax = $(slides).find('.parallax'),

					timestamp = $(slides).find('.time-stamp'),

					tabNav = '#user-story-trigger .left .content .tab-nav a.story-',

					time = index == (numOfslides - 1) ? 0.25 : 0.5,

					repeat = (index != (numOfslides - 1)) ? 1 : 0,

					opacity = (index == 0) ? 1 : 0,

					visibility = (index == 0) ? 'visible' : 'hidden';

				

				// define tween	

				var tween = new TimelineMax()

					.add([

						TweenMax.fromTo( slides, 0.15, { opacity: opacity}, { opacity: 1, ease: Linear.easeNone }),

						TweenMax.fromTo( timestamp, 0.5, { opacity: 0, y: 200 }, { opacity: 1, y: 0, ease:Linear.easeNone, repeat: 0, repeatDelay: 1, yoyo:false, ease: Linear.easeNone }),

						TweenMax.fromTo( parallax, 0.5, {backgroundPosition: '50% 100%' }, {backgroundPosition: '50% 50%', ease: Linear.easeNone, repeat: 0, repeatDelay: 1, yoyo:false, ease: Linear.easeNone })

						//TweenMax.fromTo( parallax, time, {visibility: visibility, backgroundPosition: '50% 100%' }, {visibility: 'visible', backgroundPosition: '50% 50%', ease: Linear.easeNone, repeat: repeat, repeatDelay: 1, yoyo:true, ease: Linear.easeNone })

					]);

					

				// define scene

				var scene = new ScrollScene({

					duration: windowHeight,

					offset: offset + windowHeight*(index+0.5)

				})

				.addTo(controller)

				.setTween(tween)

				.on('enter', function(event){

					

					$(tabNav+(index+1)).siblings().removeClass('active');

					$(tabNav+(index+1)).addClass('active'); // update feature nav

					

					//console.log(App.globals.userStory.tabNavScrollPos);

					

				})

				.on('leave', function(event){

					

					

					

				});

				

				// update feature nav scrollpos array for individual click scroll

				if(index == (numOfslides - 1)){

					App.globals.userStory.tabNavScrollPos[index] = scene.scrollOffset() + windowHeight;

				}

				else{

					App.globals.userStory.tabNavScrollPos[index] = scene.scrollOffset() + windowHeight;

				}

				

			});

			// END code for adding tweens in each single text | animating app slides

			

			// attach click handlers for user story nav buttons

			$(App.globals.userStory.tabNav).find('a').each(function(index, element) {

                $(this).on('click', function(e){

					$(window).scrollTop(App.globals.userStory.tabNavScrollPos[index]);

					$(this).siblings('a').removeClass('active');

					$(this).addClass('active');

				});

            });

			// END attach click handlers for user story nav buttons

		

	},

	/****************************************************

	*

	* 	END DESKTOP VIEW SCROLL CONTROLLER FOR USER STORY SECTION

	*

	****************************************************/

	

	

	/****************************************************

	*

	* 	UPDATE MAIN NAV

	*

	****************************************************/

	updateMainNav: function(nav){

		

		$(nav).parent('li').siblings('li').children('a').removeClass('active');

		$(nav).addClass('active');

		

	},

	/****************************************************

	*

	* 	END UPDATE MAIN NAV

	*

	****************************************************/

	

	

	/****************************************************

	*

	* 	SOCIAL FEEDS CONTROLLER

	*

	****************************************************/

	socialFeedController: function(){

		

		// parse jSon and insert html

		var socailfeed = $.getJSON('templates/social.json', function(json, textStatus) {
			feedData = json[0].posts;
			$.each(feedData, function(i, val) {

				App.globals.userStory.feeds.push(
					'<div class="box">',

						'<div class="content">',

							'<div class="social '+feedData[i].network+'"></div>',

							'<div class="avatar">',

								'<img src="'+feedData[i].user_profile_image_url+'" alt="">',

							'</div>',

							'<div class="text">',

								'<div class="header">'+feedData[i].name+' <a href="javascript: void(0)">'+feedData[i].username+'</a> <span>'+feedData[i].post_date_time+'</span></div>',

								'<p>',

								feedData[i].texts,

								'</p>',

							'</div>',

							'<div class="footer">',

								'<a class="less">Read Less</a>',

								'<a href="javscript: void(0)" class="share">Share</a>',

							'</div>',

						'</div>',

					'</div>'

				);

			}).bottom;

		

			$(App.globals.userStory.feedContainer).append(App.globals.userStory.feeds.join(''));

			App.globals.userStory.feeds.length = 0;

		});

		// END parse jSon and insert html

		

		// attach click handlers

		$('body').on('click', '#feeds .box .content .text', function(e){

			$(this).parents('.box').siblings('.box').removeClass('active');

			$(this).parents('.box').addClass('active');

		});

		$('body').on('click', '#feeds .box .content .footer a.less', function(e){

			$(this).parents('.box').removeClass('active');

		});

		// END attach click handlers

		

		// if tablet or mobile then call slick

		socailfeed.complete(function(){

			if(App.globals.screenType.tablet == true || App.globals.screenType.mobile == true){

				

				var centerpadding;

				if(App.globals.screenType.tablet == true){

					centerpadding = '150px';

				}

				if(App.globals.screenType.mobile == true){

					centerpadding = '30px';

				}

				

				// activate carousel for social feed

				$('#feeds').slick({

					accessibility: false,

					adaptiveHeight : true,

					arrows : false,

					infinite: true,

					centerMode : true,

					centerPadding: centerpadding,

					slidesToShow: 1,

					slidesToScroll: 1

				});

				// END activate carousel for social feed

				

				// restore opened feed On swipe event

				$('#feeds').on('afterChange', function(event, slick, currentSlide){

				  //console.log(direction);

				  $('#feeds').find('.active').removeClass('active');

				});

				// END restore opened feed On swipe event

			}

		});

		// if tablet or mobile then call slick

		

	},

	/****************************************************

	*

	* 	END SOCIAL FEEDS CONTROLLER

	*

	****************************************************/

	

	

	/****************************************************

	*

	* 	PROMO MODAL

	*

	****************************************************/

	promoModal: function(){

		

		var inst = $('[data-remodal-id=promocode]').remodal({hashTracking:false});

		

		$(document).on('closed', '.remodal', function (e) {



		  // Reason: 'confirmation', 'cancellation'

		  $('html, body').css({overflow: 'visible'});

		  $('body').css({overflowX: 'hidden'});

		  //console.log('Modal is closing' + (e.reason ? ', reason: ' + e.reason : ''));

		  

		});

		

	},

	/****************************************************

	*

	* 	END PROMO MODAL

	*

	****************************************************/

	

	

	/****************************************************

	*

	* 	RTB RE-STRUCTURE FOR TAB/MOBILE

	*

	****************************************************/

	tabMobileRTB: function(){

		var headerHTML = '<div class="rtb-header">'+
						 '<h2><span><span>Incredible Benefits Header</h2>'+
						 '<p>Lorem ipsum dolor sit amet</p>'+
						 '</div>';
		var headerHTML2 = '<div class="rtb-header">'+
						 '<h2><span><strong>Benefits</strong><span>Incredible Benefits Header</h2>'+
						 '<p>Lorem ipsum dolor sit amet</p>'+
						 '</div>';

		// parse jSon and insert html
		if(App.globals.screenType.mobile == true){
			var rtb = $.getJSON('templates/mobile-rtb.json', function(json, textStatus) {
	
				rtbData = json[0].features;
	
				$.each(rtbData, function(i, val) {
	
					App.globals.features.features.push(
	
						'<div class="slides feature-'+parseInt(i+1)+'">',
	
							'<figure>',
	
								'<img src="'+rtbData[i].image+'" alt="">',
	
							'</figure>',
	
							'<div class="content">',
	
								'<h2>'+rtbData[i].h2+'</h2>',
	
								'<p>'+rtbData[i].p+'</p>',
	
							'</div>',
	
						'</div>'
	
					);
	
				}).bottom;
	
			
	
				$(App.globals.features.featureSlideContainer).html(App.globals.features.features.join(''));
	
				App.globals.features.features.length = 0;
				
				$('#features .mid-phone .inner-cont').prepend(headerHTML2);
				
			});
	
			// END parse jSon and insert html
	
			
	
			rtb.complete(function(){
	
				$(App.globals.features.featureSlideContainer).slick({
	
					accessibility: false,
	
					adaptiveHeight : true,
	
					arrows : true,
					prevArrow : '<a class="phone-sl-nav phone-sl-nav-prev"></a>',
					nextArrow : '<a class="phone-sl-nav phone-sl-nav-next"></a>',
					appendArrows : $('#features .mid-phone .inner-cont'),
					dots : true,
	
					infinite: true,
	
					centerMode : true,
	
					centerPadding: '25px',
	
					slidesToShow: 1,
	
					slidesToScroll: 1
	
				});
				
				
			
			$('#features .mid-phone').append('<a data-remodal-target="promocode" href="javascript: void(0)" class="mobidown">DOWNLOAD THE APP</a>');
	
			});
		}
		
		if(App.globals.screenType.tablet == true){
			var rtb = $.getJSON('templates/tab-rtb.json', function(json, textStatus) {
	
				rtbData = json[0].features;
	
				$.each(rtbData, function(i, val) {
	
					App.globals.features.features.push(
	
						'<div class="slides feature-'+parseInt(i+1)+'">',
	
							'<figure>',
	
								'<img src="'+rtbData[i].image+'" alt="">',
	
							'</figure>',
	
							'<div class="content">',
	
								'<h2>'+rtbData[i].h2+'</h2>',
	
								'<p>'+rtbData[i].p+'</p>',
	
								'<a href="javascript: void(0)" data-remodal-target="promocode">'+rtbData[i].link+'</a>',
	
							'</div>',
	
						'</div>'
	
					);
	
				}).bottom;
	
			
	
				$(App.globals.features.featureSlideContainer).html(App.globals.features.features.join(''));
	
				App.globals.features.features.length = 0;
				
				$('#features .mid-phone .inner-cont').prepend(headerHTML);
				
			});
	
			// END parse jSon and insert html
	
			
	
			rtb.complete(function(){
	
				$(App.globals.features.featureSlideContainer).slick({
	
					accessibility: false,
	
					adaptiveHeight : true,
	
					arrows : true,
					prevArrow : '<a class="phone-sl-nav phone-sl-nav-prev"></a>',
					nextArrow : '<a class="phone-sl-nav phone-sl-nav-next"></a>',
					appendArrows : $('#features .mid-phone .inner-cont'),
					dots : true,
	
					infinite: true,
	
					centerMode : true,
	
					centerPadding: '25px',
	
					slidesToShow: 1,
	
					slidesToScroll: 1
	
				});
	
			});
		}
		

	},

	/****************************************************

	*

	* 	END RTB RE-STRUCTURE FOR TAB/MOBILE

	*

	****************************************************/

	

	

	/****************************************************
	*

	* 	CUSTOM SCROLL

	*

	****************************************************/

	customScroll: function(){
		var $window = $(window);		//Window object
		//var scrollTime = App.globals.customScroll.scTime; //Scroll time
		//var scrollDistance = App.globals.customScroll.scDistance; //Distance. Use smaller value for shorter scroll and greater value for longer scroll
		$window.on("mousewheel DOMMouseScroll", function(event){
			event.preventDefault();	
			var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
			var scrollTop = $window.scrollTop();
			var finalScroll = scrollTop - parseInt(delta*App.globals.customScroll.scDistance);
			
			TweenMax.to($window, App.globals.customScroll.scTime, {
				scrollTo : { y: finalScroll, autoKill:true },
				ease: Power1.easeOut,	//For more easing functions see http://api.greensock.com/js/com/greensock/easing/package-detail.html
				autoKill: true,
				overwrite: 5							
			});
		});
	},

	/****************************************************

	*

	* 	END CUSTOM SCROLL

	*

	****************************************************/

	

	

	/****************************************************

	*

	* 	MANUAL SCROLL

	*

	****************************************************/

	manualScrollto: function(distance){
		var $window = $(window);		//Window object
		var scrollDistance = distance;
		TweenMax.to($window, App.globals.customScroll.scTime, {
			scrollTo : { y: scrollDistance, autoKill:true },
				ease: Power1.easeOut,	//For more easing functions see http://api.greensock.com/js/com/greensock/easing/package-detail.html
				autoKill: true,
		});
	}

	/****************************************************

	*

	* 	END MANUAL SCROLL

	*

	****************************************************/

	

}

// END Main Application





// call Application

App.init();



	

})(window, document, jQuery);

