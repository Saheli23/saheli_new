import firebase from './firebase';

export default class Common {

    static build(obj) {
      
    const itemsRef = firebase.database().ref('music');

        switch (obj.source) {
            case 'all':
             var results=[]; 
              itemsRef.on('value', (snapshot) => {
                let items=snapshot.val();
                for (let item in items) {
                  results.push(items[item]);
                }
               
              });

              return results;  

            case 'top':

             var results=[]; 
              itemsRef.orderByChild("isTop").equalTo(true).on('value', (snapshot) => {
                let items=snapshot.val();
                for (let item in items) {
                  results.push(items[item]);
                }
               
              });
               
              return results;  
            default:
                return itemsRef;
        }
    }
}