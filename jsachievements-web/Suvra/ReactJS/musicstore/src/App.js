import React, { Component } from 'react';
import Common from './factory/Common';
import './App.css';
import Navbar from './component/Navbar';
import Container from './component/Container';


class App extends Component {
  constructor(props) {
     super(props)
   }	
  render() {
    return (
      <div className="content-wrapper">
        <Navbar></Navbar>
        <Container></Container>
      </div>
    );
  }
}

export default App;
