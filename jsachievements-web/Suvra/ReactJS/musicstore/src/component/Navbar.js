import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class Navbar extends Component {
  getCurrent(pathName)
  {
    let path=window.location.pathname;
    if(pathName===path)
       return 'active';
  }
  render() {
    this.getCurrent()
    return (
      <nav className="navbar navbar-inverse">
        <div className="container-fluid">
          <div className="navbar-header">
            <Link to='/' className="navbar-brand">Youtube Music Store</Link>
          </div>
          <ul className="nav navbar-nav">
            <li className={this.getCurrent('/')}><Link to='/' >Top Track</Link></li>
            <li className={this.getCurrent('/singer')}><Link to='/singer'>Singer</Link></li>
            <li className={this.getCurrent('/all')}><Link to='/all'>Track Management</Link></li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default Navbar;
