import React, { Component } from 'react';
import firebase from '../factory/firebase';

class TopTrack extends Component {
	constructor(props) {
	   super(props);
	    this.state={
        data:[],
        layoutStyle:'col-xs-4 col-lg-4',
        listStyle:'grid',
        showModal:'',
        selectedRow:{},

      }
	 }
  componentDidMount() {

      const itemsRef = firebase.database().ref('music');
              itemsRef.orderByChild("isTop").equalTo(true).on('value', (snapshot) => {
                let items=snapshot.val();
                let result=[];
                for (let item in items) {
                  var obj={'key':item,data:items[item]}
                  result.push(obj);
                }
                this.setState({data:result});
               
              });

  }

  Layout(e){
    const type=e.target.getAttribute('id');
       this.setState({listStyle:type});

          if(type==='grid'){
            this.setState({layoutStyle:'col-xs-4 col-lg-4'});
          }
          else
          {
            this.setState({layoutStyle:'col-xs-12 col-lg-12 list'});

          }

  }
  getSelected(type)
  {
      return (this.state.listStyle===type) ?'layoutType':'';
      

  }
  getScreen( url,type )
{
  if(url === null){ return ""; }

  var vid;
  var results;

  results = url.match("[\\?&]v=([^&#]*)");

  vid = ( results === null ) ? url : results[1];
    if(type==='img')
    return "http://img.youtube.com/vi/"+vid+"/0.jpg";
   else
     return "https://www.youtube.com/embed/"+vid;
}



likeTrack(e){
    const index=e.target.getAttribute('data-info');
    var obj =this.state.data.filter(x=>{if(x.key===index){ return x;}});
    if(localStorage.getItem("myfav"))
    {
      let list=JSON.parse(localStorage.getItem("myfav"));
      let found=false;
      var i=list.map(function(e) { return e.key; }).indexOf(index);
      if(i!==-1)
      {
        list.splice(i,1);
         e.target.classList.remove("glyphicon-heart");
         e.target.classList.add("glyphicon-heart-empty");
      }
      else
      {
        list.push(obj[0])
        e.target.classList.remove("glyphicon-heart-empty");
        e.target.classList.add("glyphicon-heart");
      }

      localStorage.setItem("myfav", JSON.stringify(list));
    }
    else
    {
      let list=[];
      list.push(obj[0])
      localStorage.setItem("myfav", JSON.stringify(list));
      e.target.classList.remove("glyphicon-heart-empty");
      e.target.classList.add("glyphicon-heart");

    }

  }

  isFav(keyinfo){
        if(localStorage.getItem("myfav"))
         {
          let found=false;
          let list=JSON.parse(localStorage.getItem("myfav"));
          var i=list.map(function(e) { return e.key; }).indexOf(keyinfo);
          if(i===-1)
          {
           return 'glyphicon-heart-empty';
          }
          else
          {
           return 'glyphicon-heart';
          }
         }
         else
         {
            return 'glyphicon-heart-empty';

         }

  }
  getRowInfo(e)
  {
       let key=e.target.getAttribute("data-index");
       let dtype=e.target.getAttribute("data-ytype");

        const ref = firebase.database().ref(`/music/${key}`);
         ref.on('value', (snapshot) => {
           // e.target.closest('tr').insertBefore('<tr>')
            var obj=snapshot.val();
            var results = (obj.media).match("[\\?&]v=([^&#]*)");

            var vid = ( results === null ) ? obj.media : results[1];
            obj['mediaId']=vid;

             this.setState({selectedRow:obj});
             this.open(dtype);

          });


  }
  close(e) {
     this.setState({ showModal: ' ' });
    var divStyle = {
      display: 'none'
    };
      this.setState({ blockModal: divStyle });
     var openmodalType= e.target.getAttribute('data-ytype');
     if(openmodalType=='video')
     {
        document.getElementById('myModal').classList.remove('in');
        document.getElementById('myModal').style.display='none';     
     }
     else
     {
        document.getElementById('myModalaudio').classList.remove('in');
        document.getElementById('myModalaudio').style.display='none';         
     }
     //document.getElementsByClassName('modal').classList.remove('in');
     //document.getElementsByClassName('modal').style.display='none';

   }

   open(dtype) {
     this.setState({ showModal: ' in ' });

     var divStyle = {
      display: 'block'
    };
     this.setState({ blockModal: divStyle });

     if(dtype=='video')
     {
        document.getElementById('myModal').classList.add('in');
        document.getElementById('myModal').style.display='block';     
     }
     else
     {
        document.getElementById('myModalaudio').classList.add('in');
        document.getElementById('myModalaudio').style.display='block';  





     }


   }


  

  render() {
     let result="";

     if(this.state.data){
         result=this.state.data.map((result,i)=>{ 
              var res=result.data,keyinfo=result.key;

              return ( <div className={"item "+this.state.layoutStyle} key={i}>
                     <div className="thumbnail">
                         <img className="group list-group-image" src={this.getScreen(res.media,'img')} alt=""  onClick ={this.getRowInfo.bind(this)} data-index={keyinfo} data-ytype='video'/>
                         <div className="caption">
                             <h4 className="group inner list-group-item-heading">
                                 {res.name} <span className={"glyphicon "+this.isFav(keyinfo) } onClick={this.likeTrack.bind(this)} data-info={keyinfo}></span>
                             </h4>
                             <p><span className="glyphicon glyphicon-facetime-video" title="Show Video"  onClick ={this.getRowInfo.bind(this)} data-ytype='video'  data-index={keyinfo}></span>
                                 <span className="glyphicon glyphicon-play" title="Show Audio"  onClick ={this.getRowInfo.bind(this)} data-ytype='audio'  data-index={keyinfo}></span>
                                 <a href={'//www.youtubeinmp3.com/fetch/?video='+res.media} className="downlaodBtn">
                                   <span className="glyphicon glyphicon glyphicon-download"></span>
                                 </a>

                                 </p>
                             <p className="group inner list-group-item-text">
                              Music:{res.Music}
                            </p>
                             <div className="row">
                                 <div className="col-xs-12 col-md-6">
                                     <p className="lead">
                                         {res.released}</p>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div> );


     
        });
     }
     else
     {
      result="<p className='text-center'>Loading...</p>"
     }


    return (
      <div>
          <div className="well well-sm">
              <div className="btn-group">
                  <a href="javascript:void(0);" id="list" className={"btn btn-default btn-sm "+this.getSelected('list')} onClick={this.Layout.bind(this)} ><span className="glyphicon glyphicon-th-list" >
                  </span>List</a> <a href="javascript:void(0);" id="grid" className={"btn btn-default btn-sm "+this.getSelected('grid')} onClick={this.Layout.bind(this)}><span
                      className="glyphicon glyphicon-th"></span>Grid</a>

              </div>
          </div>

          <div id="products" className="row list-group">
            {result}
          </div>

          <div id="myModal" className={"modal fade"} role="dialog"  >
            <div className="modal-dialog">

              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" data-ytype='video' onClick={this.close.bind(this)}>&times;</button>
                  <h4 className="modal-title">{this.state.selectedRow.name}</h4>
                </div>
                <div className="modal-body">
                  <iframe width="100%" height="315" src={"https://www.youtube.com/embed/"+this.state.selectedRow.mediaId}></iframe>
                  <p>{this.state.selectedRow.singer}</p>

                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.close.bind(this)}  data-ytype='video' >Close</button>
                </div>
              </div>

            </div>
          </div>



          <div id="myModalaudio" className={"modal fade"} role="dialog"  >
            <div className="modal-dialog">

              <div className="modal-content">
                <div className="modal-header">
                  <button data-ytype='audio' type="button" className="close" data-dismiss="modal" onClick={this.close.bind(this)}>&times;</button>
                  <h4 className="modal-title">{this.state.selectedRow.name}</h4>
                </div>
                <div className="modal-body">
                  
                  
                   <div data-video={this.state.selectedRow.mediaId}  
                         data-autoplay="0"         
                         data-loop="1"             
                         id="youtube-audio">
                  </div>
                  <p>{this.state.selectedRow.singer}</p>

                </div>
                <div className="modal-footer">
                  <button data-ytype='audio' type="button" className="btn btn-default" data-dismiss="modal" onClick={this.close.bind(this)}>Close</button>
                </div>
              </div>

            </div>
          </div>


     </div>
    );
  }
}

export default TopTrack;