
import React, { Component } from 'react';
import firebase from '../factory/firebase';
import axios from 'axios';


class ListTrack extends Component {
  constructor(props) {
     super(props);

      this.state={
        showCreate:false,
        data:[],
        formData:{},
        showModal:false,
        selectedRow:{},
        setautocomplete:'hidden',
        fillData:''
      }
    var d = new Date();
    this.id= d.getTime();
    this.setState({id:this.id})
    this.itemsRef = firebase.database().ref('music');

    // var formDataInfo=this.state.formData;
    // formDataInfo['name']="";
    // formDataInfo['Music']="";
    // formDataInfo['released']="";
    // formDataInfo['media']="";

    // this.setState({
    //   formData: formDataInfo
    // });

   }

   componentDidMount() {

    this.loadData()

   }
    loadData()
    {
           this.itemsRef.on('value', (snapshot) => {
                let items=snapshot.val();
                var results=[]
                for (let item in items) {
                  var obj={'key':item,data:items[item]}
                  results.push(obj);
                }

                this.setState({data:results});
              });

    }
   deleteRow(e){
    let key=e.target.getAttribute("data-index");
     const ref = firebase.database().ref(`/music/${key}`);
     ref.remove();
   }
  getRowInfo(e)
  {
        let key=e.target.getAttribute("data-index");

        const ref = firebase.database().ref(`/music/${key}`);
         ref.on('value', (snapshot) => {
           // e.target.closest('tr').insertBefore('<tr>')
            var obj=snapshot.val();
            var results = (obj.media).match("[\\?&]v=([^&#]*)");

            var vid = ( results === null ) ? obj.media : results[1];
            obj['mediaId']=vid;

             this.setState({selectedRow:obj});
             this.open();

          });


  }

  resetData()
  {
    var formDataInfo=this.state.formData;
    formDataInfo['name']="";
    formDataInfo['Music']="";
    formDataInfo['released']="";
    formDataInfo['media']="";

    this.setState({
      formData: formDataInfo
    });
    document.getElementById("name").value="";

  }
   handleChange(e)
   {
     const target = e.target;
     let value ='';
     const name = target.name;
     if(name==="isTop")
     {
        value =target.type === 'checkbox' ? target.checked : target.value;
     }
     else
     {
       value = target.value;
     }

     var formDataInfo=this.state.formData;
     formDataInfo[name]=value;
     this.setState({
       formData: formDataInfo
     });

   }

   setVideo(e)
   {
     var index=e.target.getAttribute('data-index');

     var current=(this.state.videos.items.map(function(res) { return res; }))[index];

     var d=new Date(current.snippet.publishedAt);
     var month = new Array();
     month[0] = "Jan";
     month[1] = "Feb";
     month[2] = "March";
     month[3] = "April";
     month[4] = "May";
     month[5] = "June";
     month[6] = "July";
     month[7] = "Aug";
     month[8] = "Sep";
     month[9] = "Oct";
     month[10] = "Nov";
     month[11] = "Dec";
     var yr = d.getDate() +" "+month[d.getMonth()]+" , "+d.getFullYear();

     var formDataInfo=this.state.formData;
     formDataInfo['name']=current.snippet.title;
     formDataInfo['Music']=current.snippet.channelTitle;
     formDataInfo['released']=yr;
     formDataInfo['media']="https://www.youtube.com/watch?v="+current.id.videoId;

     this.setState({
       formData: formDataInfo
     });
     this.setState({
       videos: null
     });
     this.setState({setautocomplete:'hidden'});
     document.getElementById("name").value=current.snippet.title;


   }
   populateVideos()
   {
      //let targetSection=document.getElementById("showautocomplete").getElementsByTagName("ul")[0];
      //targetSection.innerHTML="";
      var self=this;
      let content='';
      this.state.videos.items.map((result,i)=>{
          let title=result.snippet.title;
          // var node = document.createElement("li");              
          // var textnode = document.createTextNode(title);       
          // node.appendChild(textnode); 
          // node.setAttribute("onClick",self.setVideo)

          // targetSection.appendChild(node);

         // content+="<li onClick={self.setVideo.bind(self)}>"+title+"</li>"



      });

      this.setState({fillData:content});


   }


   changeName(e)
   {
      const target = e.target;
      let searchTerm =(target.value).trim();
      var self=this;
      if(searchTerm.length >3)
      {
            var params = {
                part: 'snippet',
                key: 'AIzaSyBFMfDKQcz44uazhhNTOwTUM7yM2Kwhfto',
                q: searchTerm
            };

            axios.get('https://www.googleapis.com/youtube/v3/search', {
                params
              })
            .then(function (response) {

              self.setState({setautocomplete:''});

              self.setState({
                videos: response.data
              });
            })
            .catch(function (error) {
              console.log(error);
            });

      }
      else
      {
         //let targetSection=document.getElementById("showautocomplete").getElementsByTagName("ul")[0];
         //targetSection.innerHTML="";
         this.setState({setautocomplete:'hidden'});
        // this.setState({fillData:''});


      }

   }

   handelSubmit(e)
   {
      e.preventDefault();
      this.itemsRef.push(this.state.formData);
      e.target.reset();
      this.setState({showCreate:false});
       this.resetData();
   }

   addData(e)
   {
     this.setState({showCreate:true});
   }

   addDataCancel(e)
   {
     this.setState({showCreate:false});
      this.resetData();
   }
  close() {
     this.setState({ showModal: ' ' });
     var divStyle = {
      display: 'none'
    };
      this.setState({ blockModal: divStyle });
   }

   open() {
     this.setState({ showModal: ' in modalShow' });
     var divStyle = {
      display: 'block'
    };
      this.setState({ blockModal: divStyle });
   }
  render() {
    let ishidden=(this.state.showCreate)?'':'hidden';
    let content="";

    if(this.state.videos)
    {
      content= this.state.videos.items.map((result,i)=>{
          let title=result.snippet.title;
         return ( <li onClick={this.setVideo.bind(this)} key={i} data-index={i}>{title}</li> );
      });
    }

    return (
      <div className="row">
      <div className={"col-md-offset-3 col-md-6 "+ishidden}>
       <h2>Create Data</h2>
      <section className="add-item">
          <form onSubmit={this.handelSubmit.bind(this)} autoComplete="off">
            <div className="form-group">
              <label>Song:</label>
              <input type="text" className="form-control" id="name" name="name" onChange={this.changeName.bind(this)}  autoComplete="off" ref={(input) => this.input = input}/>
              <div className={"autocomplete "+ this.state.setautocomplete } id="showautocomplete">
                <ul>
                {content}
                </ul>
              </div>
            </div>
            <div className="form-group">
              <label>Lyrics:</label>
              <input type="text" className="form-control" id="lyrics" name="lyrics" onChange={this.handleChange.bind(this)}  autoComplete="off" />
            </div>
            <div className="form-group">
              <label>Music:</label>
              <input type="text" className="form-control" id="Music" name="Music" onChange={this.handleChange.bind(this)}  autoComplete="off"  value={this.state.formData.Music} ref={(input) => this.input = input} />
            </div>
            <div className="form-group">
              <label>Singer:</label>
              <input type="text" className="form-control" id="singer" name="singer" onChange={this.handleChange.bind(this)}  autoComplete="off"/>
            </div>
            <div className="form-group">
              <label >Released:</label>
              <input type="text" className="form-control" id="released" name="released" onChange={this.handleChange.bind(this)}  autoComplete="off"  value={this.state.formData.released} ref={(input) => this.input = input} />
            </div>
            <div className="form-group">
              <label >Media:</label>
              <input type="text" className="form-control" id="media" name="media" onChange={this.handleChange.bind(this)}  autoComplete="off"  value={this.state.formData.media} ref={(input) => this.input = input} />
            </div>
            <div className="checkbox">
              <label><input type="checkbox" id="isTop" name="isTop" onChange={this.handleChange.bind(this)}/> Show in Top List</label>
            </div>
            <button type="submit" className="btn btn-warning">Submit</button>  <button type="button" className="btn btn-danger" onClick={this.addDataCancel.bind(this)}>Cancel</button>
          </form>
      </section>
      </div>
      <div className="col-md-12">
       <p><button type="button" className="btn btn-success" onClick={this.addData.bind(this)}>Add</button></p>
      <div className="table-responsive">          
        <table className="table">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Music</th>
              <th>Singer</th>
              <th>Released</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
     {     

              this.state.data.map((result,i)=>{
                var res=result.data,keyinfo=result.key;

                    return ( <tr key={i}>
                           <td>{i+1}</td>
                           <td width="15%">{res.name}</td>
                           <td>{res.Music}</td>
                           <td>{res.singer}</td>
                           <td>{res.released}</td>
                           <td> <button type="button" className="btn btn-info" onClick ={this.getRowInfo.bind(this)} data-index={keyinfo}>View</button> <button type="button" className="btn btn-danger" onClick ={this.deleteRow.bind(this)} data-index={keyinfo}>Delete</button></td>
                         </tr>);
              
                 })
           



       }
          </tbody>
        </table>
           </div>     
        </div>
        <div id="myModal" className={"modal fade"+this.state.showModal} role="dialog" style={this.state.blockModal} >
          <div className="modal-dialog">

            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" onClick={this.close.bind(this)}>&times;</button>
                <h4 className="modal-title">{this.state.selectedRow.name}</h4>
              </div>
              <div className="modal-body">
                <iframe width="100%" height="315" src={"https://www.youtube.com/embed/"+this.state.selectedRow.mediaId}></iframe>
                <p>{this.state.selectedRow.singer}</p>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.close.bind(this)}>Close</button>
              </div>
            </div>

          </div>
        </div>
      </div>
    );
  }
}

export default ListTrack;