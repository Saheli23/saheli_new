
import React, { Component } from 'react';
import axios from 'axios';

class SingerTrack extends Component {

constructor(props) {
     super(props);
     this.state={
        videos:[],
        showLoadMore:'hidden'
      }

 }

handleChange(e)
   {
     const target = e.target;
     let value =target.value;
     const name = target.name;
     this.setState({
       name: value
     });

   }
handelSubmit(e)
{
   e.preventDefault();
   e.target.reset();
   var self=this;
   let searchTerm=this.state.name;
	  var params = {
	      part: 'snippet',
	      key: 'AIzaSyBFMfDKQcz44uazhhNTOwTUM7yM2Kwhfto',
	      q: searchTerm,
	      maxResults:12
	  };

    if(this.state.nextPageToken)
    {
      params['pageToken']=this.state.nextPageToken;

    }

	  axios.get('https://www.googleapis.com/youtube/v3/search', {
	      params
	    })
	  .then(function (response) {
	    self.setState({
	      videos: response.data.items
	    });

     self.setState({
        showLoadMore: ' '
      });

      self.setState({nextPageToken:response.data.nextPageToken});

	  })
	  .catch(function (error) {
	    console.log(error);
	  }); 

}


loadMore()
{
	   var self=this;
	   let searchTerm=this.state.name;
		  var params = {
		      part: 'snippet',
		      key: 'AIzaSyBFMfDKQcz44uazhhNTOwTUM7yM2Kwhfto',
		      q: searchTerm,
		      maxResults:12
		  };
      if(this.state.nextPageToken)
      {
        params['pageToken']=this.state.nextPageToken;

      }
		  axios.get('https://www.googleapis.com/youtube/v3/search', {
		      params
		    })
		  .then(function (response) {
		
		    self.setState({
		      videos: self.state.videos.concat(response.data.items)
		    });
        self.setState({nextPageToken:response.data.nextPageToken});

        self.setState({
          showLoadMore: ' '
        });

		  })
		  .catch(function (error) {
		    console.log(error);
		  }); 

}

  render() {
  	let content="";


  	if(this.state.videos){
  	    content=this.state.videos.map((result,i)=>{ 
  	         let res=result.snippet,keyinfo=i;

  	         let d=new Date(res.publishedAt);
  	         let month = new Array();
  	         month[0] = "Jan";
  	         month[1] = "Feb";
  	         month[2] = "March";
  	         month[3] = "April";
  	         month[4] = "May";
  	         month[5] = "June";
  	         month[6] = "July";
  	         month[7] = "Aug";
  	         month[8] = "Sep";
  	         month[9] = "Oct";
  	         month[10] = "Nov";
  	         month[11] = "Dec";
  	         let yr = d.getDate() +" "+month[d.getMonth()]+" , "+d.getFullYear();
             let media="https://www.youtube.com/watch?v="+result.id.videoId

  	         return ( <div className="item col-xs-4 col-lg-4'" key={i}>
  	                <div className="thumbnail">
  	                    <img className="group list-group-image imgVid" src={"http://img.youtube.com/vi/"+result.id.videoId+"/0.jpg"} alt=""  />
  	                    <div className="caption">
  	                        <h4 className="group inner list-group-item-heading minHeight">
  	                            {res.title}  <a href={'//www.youtubeinmp3.com/fetch/?video='+media} className="downlaodBtn">
                                   <span className="glyphicon glyphicon glyphicon-download"></span>
                                 </a>
  	                        </h4>
  	                     
  	                        <div className="row">
  	                            <div className="col-xs-12 col-md-6">
  	                                <p className="lead">
  	                                    {yr}</p>
  	                            </div>
  	                        </div>
  	                    </div>
  	                </div>
  	            </div> );


  	
  	   });
  	}
    return (
      <div>
		     
          <div className="well well-sm">
             <h5>Search Song Using Singer</h5>
              <form onSubmit={this.handelSubmit.bind(this)} autoComplete="off" className="form-inline">
                <div className="form-group">
                  <label>Singer:</label>
                  <input type="text" className="form-control" id="name" name="name" onChange={this.handleChange.bind(this)}  autoComplete="off" ref={(input) => this.input = input}/>
                  <button type="submit" className="btn btn-warning">Submit</button>
                </div>
                
              </form>
          </div>
		       <div id="products" className="row list-group">
                  {content}
		       </div>   
		       <div className={"loadmore "+this.state.showLoadMore}>
		       	<p onClick={this.loadMore.bind(this)} className="btn btn-warning">Load More</p>
		       </div>
      </div>
    );
  }
}

export default SingerTrack;