import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'
import TopTrack from './TopTrack';
import SingerTrack from './SingerTrack';
import FavTrack from './FavTrack';
import ListTrack from './ListTrack';
import Common from '../factory/Common';

class Container extends Component {

  render() {
    return (
      <div  className="container-fluid">
        <Switch>
          <Route exact path='/' component={TopTrack} />
          <Route exact path='/singer' component={SingerTrack} />
          <Route exact path='/favorite' component={FavTrack} />
          <Route exact path='/all'  component={ListTrack}/>} />
        </Switch>

      </div>
    );
  }
}

export default Container;
