// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

// CSS Included
//require('../static/css/bootstrap.min.css');

import Vue from 'vue'
import * as uiv from 'uiv'
Vue.use(uiv, {prefix: 'uiv'})
import router from './router'
import VueSession from 'vue-session'
Vue.use(VueSession)
import  _ from 'lodash'
import App from './App'



/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: {
        App
    }
})
