<?php
////////////////////////////////////////////////////
// PHPMailer - PHP email class
//
// Class for sending email using either
// sendmail, PHP mail(), or SMTP.  Methods are
// based upon the standard AspEmail(tm) classes.
//
// Copyright (C) 2001 - 2003  Brent R. Matzelle
//
// License: LGPL, see LICENSE
////////////////////////////////////////////////////

/**
 * PHPMailer - PHP email transport class
 * @package PHPMailer
 * @author Brent R. Matzelle
 * @copyright 2001 - 2003 Brent R. Matzelle
 */
class PHPMailer
{
    /////////////////////////////////////////////////
    // PUBLIC VARIABLES
    /////////////////////////////////////////////////

    /**
     * Email priority (1 = High, 3 = Normal, 5 = low).
     * @var int
     */
    var $Priority          = 3;

    /**
     * Sets the CharSet of the message.
     * @var string
     */
    var $CharSet           = "iso-8859-1";

    /**
     * Sets the Content-type of the message.
     * @var string
     */
    var $ContentType        = "text/plain";

    /**
     * Sets the Encoding of the message. Options for this are "8bit",
     * "7bit", "binary", "base64", and "quoted-printable".
     * @var string
     */
    var $Encoding          = "8bit";

    /**
     * Holds the most recent mailer error message.
     * @var string
     */
    var $ErrorInfo         = "";

    /**
     * Sets the From email address for the message.
     * @var string
     */
    var $From               = "root@localhost";

    /**
     * Sets the From name of the message.
     * @var string
     */
    var $FromName           = "Root User";

    /**
     * Sets the Sender email (Return-Path) of the message.  If not empty,
     * will be sent via -f to sendmail or as 'MAIL FROM' in smtp mode.
     * @var string
     */
    var $Sender            = "";

    /**
     * Sets the Subject of the message.
     * @var string
     */
    var $Subject           = "";

    /**
     * Sets the Body of the message.  This can be either an HTML or text body.
     * If HTML then run IsHTML(true).
     * @var string
     */
    var $Body               = "";

    /**
     * Sets the text-only body of the message.  This automatically sets the
     * email to multipart/alternative.  This body can be read by mail
     * clients that do not have HTML email capability such as mutt. Clients
     * that can read HTML will view the normal Body.
     * @var string
     */
    var $AltBody           = "";

    /**
     * Sets word wrapping on the body of the message to a given number of 
     * characters.
     * @var int
     */
    var $WordWrap          = 0;

    /**
     * Method to send mail: ("mail", "sendmail", or "smtp").
     * @var string
     */
    var $Mailer            = "mail";

    /**
     * Sets the path of the sendmail program.
     * @var string
     */
    var $Sendmail          = "/usr/sbin/sendmail";
    
    /**
     * Path to PHPMailer plugins.  This is now only useful if the SMTP class 
     * is in a different directory than the PHP include path.  
     * @var string
     */
    var $PluginDir         = "";

    /**
     *  Holds PHPMailer version.
     *  @var string
     */
    var $Version           = "1.73";

    /**
     * Sets the email address that a reading confirmation will be sent.
     * @var string
     */
    var $ConfirmReadingTo  = "";

    /**
     *  Sets the hostname to use in Message-Id and Received headers
     *  and as default HELO string. If empty, the value returned
     *  by SERVER_NAME is used or 'localhost.localdomain'.
     *  @var string
     */
    var $Hostname          = "";

    /////////////////////////////////////////////////
    // SMTP VARIABLES
    /////////////////////////////////////////////////

    /**
     *  Sets the SMTP hosts.  All hosts must be separated by a
     *  semicolon.  You can also specify a different port
     *  for each host by using this format: [hostname:port]
     *  (e.g. "smtp1.example.com:25;smtp2.example.com").
     *  Hosts will be tried in order.
     *  @var string
     */
    var $Host        = "localhost";

    /**
     *  Sets the default SMTP server port.
     *  @var int
     */
    var $Port        = 25;

    /**
     *  Sets the SMTP HELO of the message (Default is $Hostname).
     *  @var string
     */
    var $Helo        = "";

    /**
     *  Sets SMTP authentication. Utilizes the Username and Password variables.
     *  @var bool
     */
    var $SMTPAuth     = false;

    /**
     *  Sets SMTP username.
     *  @var string
     */
    var $Username     = "";

    /**
     *  Sets SMTP password.
     *  @var string
     */
    var $Password     = "";

    /**
     *  Sets the SMTP server timeout in seconds. This function will not 
     *  work with the win32 version.
     *  @var int
     */
    var $Timeout      = 10;

    /**
     *  Sets SMTP class debugging on or off.
     *  @var bool
     */
    var $SMTPDebug    = false;

    /**
     * Prevents the SMTP connection from being closed after each mail 
     * sending.  If this is set to true then to close the connection 
     * requires an explicit call to SmtpClose(). 
     * @var bool
     */
    var $SMTPKeepAlive = false;

    /**#@+
     * @access private
     */
    var $smtp            = NULL;
    var $to              = array();
    var $cc              = array();
    var $bcc             = array();
    var $ReplyTo         = array();
    var $attachment      = array();
    var $CustomHeader    = array();
    var $message_type    = "";
    var $boundary        = array();
    var $language        = array();
    var $error_count     = 0;
    var $LE              = "\n";
    /**#@-*/
    
    /////////////////////////////////////////////////
    // VARIABLE METHODS
    /////////////////////////////////////////////////

    /**
     * Sets message type to HTML.  
     * @param bool $bool
     * @return void
     */
    function IsHTML($bool) {
        if($bool == true)
            $this--->ContentType = "text/html";
        else
            $this-&gt;ContentType = "text/plain";
    }

    /**
     * Sets Mailer to send message using SMTP.
     * @return void
     */
    function IsSMTP() {
        $this-&gt;Mailer = "smtp";
    }

    /**
     * Sets Mailer to send message using PHP mail() function.
     * @return void
     */
    function IsMail() {
        $this-&gt;Mailer = "mail";
    }

    /**
     * Sets Mailer to send message using the $Sendmail program.
     * @return void
     */
    function IsSendmail() {
        $this-&gt;Mailer = "sendmail";
    }

    /**
     * Sets Mailer to send message using the qmail MTA. 
     * @return void
     */
    function IsQmail() {
        $this-&gt;Sendmail = "/var/qmail/bin/sendmail";
        $this-&gt;Mailer = "sendmail";
    }


    /////////////////////////////////////////////////
    // RECIPIENT METHODS
    /////////////////////////////////////////////////

    /**
     * Adds a "To" address.  
     * @param string $address
     * @param string $name
     * @return void
     */
    function AddAddress($address, $name = "") {
        $cur = count($this-&gt;to);
        $this-&gt;to[$cur][0] = trim($address);
        $this-&gt;to[$cur][1] = $name;
    }

    /**
     * Adds a "Cc" address. Note: this function works
     * with the SMTP mailer on win32, not with the "mail"
     * mailer.  
     * @param string $address
     * @param string $name
     * @return void
    */
    function AddCC($address, $name = "") {
        $cur = count($this-&gt;cc);
        $this-&gt;cc[$cur][0] = trim($address);
        $this-&gt;cc[$cur][1] = $name;
    }

    /**
     * Adds a "Bcc" address. Note: this function works
     * with the SMTP mailer on win32, not with the "mail"
     * mailer.  
     * @param string $address
     * @param string $name
     * @return void
     */
    function AddBCC($address, $name = "") {
        $cur = count($this-&gt;bcc);
        $this-&gt;bcc[$cur][0] = trim($address);
        $this-&gt;bcc[$cur][1] = $name;
    }

    /**
     * Adds a "Reply-to" address.  
     * @param string $address
     * @param string $name
     * @return void
     */
    function AddReplyTo($address, $name = "") {
        $cur = count($this-&gt;ReplyTo);
        $this-&gt;ReplyTo[$cur][0] = trim($address);
        $this-&gt;ReplyTo[$cur][1] = $name;
    }


    /////////////////////////////////////////////////
    // MAIL SENDING METHODS
    /////////////////////////////////////////////////

    /**
     * Creates message and assigns Mailer. If the message is
     * not sent successfully then it returns false.  Use the ErrorInfo
     * variable to view description of the error.  
     * @return bool
     */
    function Send() {
        $header = "";
        $body = "";
        $result = true;

        if((count($this-&gt;to) + count($this-&gt;cc) + count($this-&gt;bcc)) &lt; 1)
        {
            $this-&gt;SetError($this-&gt;Lang("provide_address"));
            return false;
        }

        // Set whether the message is multipart/alternative
        if(!empty($this-&gt;AltBody))
            $this-&gt;ContentType = "multipart/alternative";

        $this-&gt;error_count = 0; // reset errors
        $this-&gt;SetMessageType();
        $header .= $this-&gt;CreateHeader();
        $body = $this-&gt;CreateBody();

        if($body == "") { return false; }

        // Choose the mailer
        switch($this-&gt;Mailer)
        {
            case "sendmail":
                $result = $this-&gt;SendmailSend($header, $body);
                break;
            case "mail":
                $result = $this-&gt;MailSend($header, $body);
                break;
            case "smtp":
                $result = $this-&gt;SmtpSend($header, $body);
                break;
            default:
            $this-&gt;SetError($this-&gt;Mailer . $this-&gt;Lang("mailer_not_supported"));
                $result = false;
                break;
        }

        return $result;
    }
    
    /**
     * Sends mail using the $Sendmail program.  
     * @access private
     * @return bool
     */
    function SendmailSend($header, $body) {
        if ($this-&gt;Sender != "")
            $sendmail = sprintf("%s -oi -f %s -t", $this-&gt;Sendmail, $this-&gt;Sender);
        else
            $sendmail = sprintf("%s -oi -t", $this-&gt;Sendmail);

        if(!@$mail = popen($sendmail, "w"))
        {
            $this-&gt;SetError($this-&gt;Lang("execute") . $this-&gt;Sendmail);
            return false;
        }

        fputs($mail, $header);
        fputs($mail, $body);
        
        $result = pclose($mail) &gt;&gt; 8 &amp; 0xFF;
        if($result != 0)
        {
            $this-&gt;SetError($this-&gt;Lang("execute") . $this-&gt;Sendmail);
            return false;
        }

        return true;
    }

    /**
     * Sends mail using the PHP mail() function.  
     * @access private
     * @return bool
     */
    function MailSend($header, $body) {
        $to = "";
        for($i = 0; $i &lt; count($this-&gt;to); $i++)
        {
            if($i != 0) { $to .= ", "; }
            $to .= $this-&gt;to[$i][0];
        }

        if ($this-&gt;Sender != "" &amp;&amp; strlen(ini_get("safe_mode"))&lt; 1)
        {
            $old_from = ini_get("sendmail_from");
            ini_set("sendmail_from", $this-&gt;Sender);
            $params = sprintf("-oi -f %s", $this-&gt;Sender);
            $rt = @mail($to, $this-&gt;EncodeHeader($this-&gt;Subject), $body, 
                        $header, $params);
        }
        else
            $rt = @mail($to, $this-&gt;EncodeHeader($this-&gt;Subject), $body, $header);

        if (isset($old_from))
            ini_set("sendmail_from", $old_from);

        if(!$rt)
        {
            $this-&gt;SetError($this-&gt;Lang("instantiate"));
            return false;
        }

        return true;
    }

    /**
     * Sends mail via SMTP using PhpSMTP (Author:
     * Chris Ryan).  Returns bool.  Returns false if there is a
     * bad MAIL FROM, RCPT, or DATA input.

     * @access private
     * @return bool
     */
    function SmtpSend($header, $body) {
        include_once($this-&gt;PluginDir . "class.smtp.php");
        $error = "";
        $bad_rcpt = array();

        if(!$this-&gt;SmtpConnect())
            return false;

        $smtp_from = ($this-&gt;Sender == "") ? $this-&gt;From : $this-&gt;Sender;
        if(!$this-&gt;smtp-&gt;Mail($smtp_from))
        {
            $error = $this-&gt;Lang("from_failed") . $smtp_from;
            $this-&gt;SetError($error);
            $this-&gt;smtp-&gt;Reset();
            return false;
        }

        // Attempt to send attach all recipients
        for($i = 0; $i &lt; count($this-&gt;to); $i++)
        {
            if(!$this-&gt;smtp-&gt;Recipient($this-&gt;to[$i][0]))
                $bad_rcpt[] = $this-&gt;to[$i][0];
        }
        for($i = 0; $i &lt; count($this-&gt;cc); $i++)
        {
            if(!$this-&gt;smtp-&gt;Recipient($this-&gt;cc[$i][0]))
                $bad_rcpt[] = $this-&gt;cc[$i][0];
        }
        for($i = 0; $i &lt; count($this-&gt;bcc); $i++)
        {
            if(!$this-&gt;smtp-&gt;Recipient($this-&gt;bcc[$i][0]))
                $bad_rcpt[] = $this-&gt;bcc[$i][0];
        }

        if(count($bad_rcpt) &gt; 0) // Create error message
        {
            for($i = 0; $i &lt; count($bad_rcpt); $i++)
            {
                if($i != 0) { $error .= ", "; }
                $error .= $bad_rcpt[$i];
            }
            $error = $this-&gt;Lang("recipients_failed") . $error;
            $this-&gt;SetError($error);
            $this-&gt;smtp-&gt;Reset();
            return false;
        }

        if(!$this-&gt;smtp-&gt;Data($header . $body))
        {
            $this-&gt;SetError($this-&gt;Lang("data_not_accepted"));
            $this-&gt;smtp-&gt;Reset();
            return false;
        }
        if($this-&gt;SMTPKeepAlive == true)
            $this-&gt;smtp-&gt;Reset();
        else
            $this-&gt;SmtpClose();

        return true;
    }

    /**
     * Initiates a connection to an SMTP server.  Returns false if the 
     * operation failed.
     * @access private
     * @return bool
     */
    function SmtpConnect() {
        if($this-&gt;smtp == NULL) { $this-&gt;smtp = new SMTP(); }

        $this-&gt;smtp-&gt;do_debug = $this-&gt;SMTPDebug;
        $hosts = explode(";", $this-&gt;Host);
        $index = 0;
        $connection = ($this-&gt;smtp-&gt;Connected()); 

        // Retry while there is no connection
        while($index &lt; count($hosts) &amp;&amp; $connection == false)
        {
            if(strstr($hosts[$index], ":"))
                list($host, $port) = explode(":", $hosts[$index]);
            else
            {
                $host = $hosts[$index];
                $port = $this-&gt;Port;
            }

            if($this-&gt;smtp-&gt;Connect($host, $port, $this-&gt;Timeout))
            {
                if ($this-&gt;Helo != '')
                    $this-&gt;smtp-&gt;Hello($this-&gt;Helo);
                else
                    $this-&gt;smtp-&gt;Hello($this-&gt;ServerHostname());
        
                if($this-&gt;SMTPAuth)
                {
                    if(!$this-&gt;smtp-&gt;Authenticate($this-&gt;Username, 
                                                  $this-&gt;Password))
                    {
                        $this-&gt;SetError($this-&gt;Lang("authenticate"));
                        $this-&gt;smtp-&gt;Reset();
                        $connection = false;
                    }
                }
                $connection = true;
            }
            $index++;
        }
        if(!$connection)
            $this-&gt;SetError($this-&gt;Lang("connect_host"));

        return $connection;
    }

    /**
     * Closes the active SMTP session if one exists.
     * @return void
     */
    function SmtpClose() {
        if($this-&gt;smtp != NULL)
        {
            if($this-&gt;smtp-&gt;Connected())
            {
                $this-&gt;smtp-&gt;Quit();
                $this-&gt;smtp-&gt;Close();
            }
        }
    }

    /**
     * Sets the language for all class error messages.  Returns false 
     * if it cannot load the language file.  The default language type
     * is English.
     * @param string $lang_type Type of language (e.g. Portuguese: "br")
     * @param string $lang_path Path to the language file directory
     * @access public
     * @return bool
     */
    function SetLanguage($lang_type, $lang_path = "language/") {
        if(file_exists($lang_path.'phpmailer.lang-'.$lang_type.'.php'))
            include($lang_path.'phpmailer.lang-'.$lang_type.'.php');
        else if(file_exists($lang_path.'phpmailer.lang-en.php'))
            include($lang_path.'phpmailer.lang-en.php');
        else
        {
            $this-&gt;SetError("Could not load language file");
            return false;
        }
        $this-&gt;language = $PHPMAILER_LANG;
    
        return true;
    }

    /////////////////////////////////////////////////
    // MESSAGE CREATION METHODS
    /////////////////////////////////////////////////

    /**
     * Creates recipient headers.  
     * @access private
     * @return string
     */
    function AddrAppend($type, $addr) {
        $addr_str = $type . ": ";
        $addr_str .= $this-&gt;AddrFormat($addr[0]);
        if(count($addr) &gt; 1)
        {
            for($i = 1; $i &lt; count($addr); $i++)
                $addr_str .= ", " . $this-&gt;AddrFormat($addr[$i]);
        }
        $addr_str .= $this-&gt;LE;

        return $addr_str;
    }
    
    /**
     * Formats an address correctly. 
     * @access private
     * @return string
     */
    function AddrFormat($addr) {
        if(empty($addr[1]))
            $formatted = $addr[0];
        else
        {
            $formatted = $this-&gt;EncodeHeader($addr[1], 'phrase') . " &lt;" . 
                         $addr[0] . "&gt;";
        }

        return $formatted;
    }

    /**
     * Wraps message for use with mailers that do not
     * automatically perform wrapping and for quoted-printable.
     * Original written by philippe.  
     * @access private
     * @return string
     */
    function WrapText($message, $length, $qp_mode = false) {
        $soft_break = ($qp_mode) ? sprintf(" =%s", $this-&gt;LE) : $this-&gt;LE;

        $message = $this-&gt;FixEOL($message);
        if (substr($message, -1) == $this-&gt;LE)
            $message = substr($message, 0, -1);

        $line = explode($this-&gt;LE, $message);
        $message = "";
        for ($i=0 ;$i &lt; count($line); $i++)
        {
          $line_part = explode(" ", $line[$i]);
          $buf = "";
          for ($e = 0; $e<count($line_part); $e++)="" {="" $word="$line_part[$e];" if="" ($qp_mode="" and="" (strlen($word)=""> $length))
              {
                $space_left = $length - strlen($buf) - 1;
                if ($e != 0)
                {
                    if ($space_left &gt; 20)
                    {
                        $len = $space_left;
                        if (substr($word, $len - 1, 1) == "=")
                          $len--;
                        elseif (substr($word, $len - 2, 1) == "=")
                          $len -= 2;
                        $part = substr($word, 0, $len);
                        $word = substr($word, $len);
                        $buf .= " " . $part;
                        $message .= $buf . sprintf("=%s", $this-&gt;LE);
                    }
                    else
                    {
                        $message .= $buf . $soft_break;
                    }
                    $buf = "";
                }
                while (strlen($word) &gt; 0)
                {
                    $len = $length;
                    if (substr($word, $len - 1, 1) == "=")
                        $len--;
                    elseif (substr($word, $len - 2, 1) == "=")
                        $len -= 2;
                    $part = substr($word, 0, $len);
                    $word = substr($word, $len);

                    if (strlen($word) &gt; 0)
                        $message .= $part . sprintf("=%s", $this-&gt;LE);
                    else
                        $buf = $part;
                }
              }
              else
              {
                $buf_o = $buf;
                $buf .= ($e == 0) ? $word : (" " . $word); 

                if (strlen($buf) &gt; $length and $buf_o != "")
                {
                    $message .= $buf_o . $soft_break;
                    $buf = $word;
                }
              }
          }
          $message .= $buf . $this-&gt;LE;
        }

        return $message;
    }
    
    /**
     * Set the body wrapping.
     * @access private
     * @return void
     */
    function SetWordWrap() {
        if($this-&gt;WordWrap &lt; 1)
            return;
            
        switch($this-&gt;message_type)
        {
           case "alt":
              // fall through
           case "alt_attachments":
              $this-&gt;AltBody = $this-&gt;WrapText($this-&gt;AltBody, $this-&gt;WordWrap);
              break;
           default:
              $this-&gt;Body = $this-&gt;WrapText($this-&gt;Body, $this-&gt;WordWrap);
              break;
        }
    }

    /**
     * Assembles message header.  
     * @access private
     * @return string
     */
    function CreateHeader() {
        $result = "";
        
        // Set the boundaries
        $uniq_id = md5(uniqid(time()));
        $this-&gt;boundary[1] = "b1_" . $uniq_id;
        $this-&gt;boundary[2] = "b2_" . $uniq_id;

        $result .= $this-&gt;HeaderLine("Date", $this-&gt;RFCDate());
        if($this-&gt;Sender == "")
            $result .= $this-&gt;HeaderLine("Return-Path", trim($this-&gt;From));
        else
            $result .= $this-&gt;HeaderLine("Return-Path", trim($this-&gt;Sender));
        
        // To be created automatically by mail()
        if($this-&gt;Mailer != "mail")
        {
            if(count($this-&gt;to) &gt; 0)
                $result .= $this-&gt;AddrAppend("To", $this-&gt;to);
            else if (count($this-&gt;cc) == 0)
                $result .= $this-&gt;HeaderLine("To", "undisclosed-recipients:;");
            if(count($this-&gt;cc) &gt; 0)
                $result .= $this-&gt;AddrAppend("Cc", $this-&gt;cc);
        }

        $from = array();
        $from[0][0] = trim($this-&gt;From);
        $from[0][1] = $this-&gt;FromName;
        $result .= $this-&gt;AddrAppend("From", $from); 

        // sendmail and mail() extract Bcc from the header before sending
        if((($this-&gt;Mailer == "sendmail") || ($this-&gt;Mailer == "mail")) &amp;&amp; (count($this-&gt;bcc) &gt; 0))
            $result .= $this-&gt;AddrAppend("Bcc", $this-&gt;bcc);

        if(count($this-&gt;ReplyTo) &gt; 0)
            $result .= $this-&gt;AddrAppend("Reply-to", $this-&gt;ReplyTo);

        // mail() sets the subject itself
        if($this-&gt;Mailer != "mail")
            $result .= $this-&gt;HeaderLine("Subject", $this-&gt;EncodeHeader(trim($this-&gt;Subject)));

        $result .= sprintf("Message-ID: &lt;%s@%s&gt;%s", $uniq_id, $this-&gt;ServerHostname(), $this-&gt;LE);
        $result .= $this-&gt;HeaderLine("X-Priority", $this-&gt;Priority);
        $result .= $this-&gt;HeaderLine("X-Mailer", "PHPMailer [version " . $this-&gt;Version . "]");
        
        if($this-&gt;ConfirmReadingTo != "")
        {
            $result .= $this-&gt;HeaderLine("Disposition-Notification-To", 
                       "&lt;" . trim($this-&gt;ConfirmReadingTo) . "&gt;");
        }

        // Add custom headers
        for($index = 0; $index &lt; count($this-&gt;CustomHeader); $index++)
        {
            $result .= $this-&gt;HeaderLine(trim($this-&gt;CustomHeader[$index][0]), 
                       $this-&gt;EncodeHeader(trim($this-&gt;CustomHeader[$index][1])));
        }
        $result .= $this-&gt;HeaderLine("MIME-Version", "1.0");

        switch($this-&gt;message_type)
        {
            case "plain":
                $result .= $this-&gt;HeaderLine("Content-Transfer-Encoding", $this-&gt;Encoding);
                $result .= sprintf("Content-Type: %s; charset=\"%s\"",
                                    $this-&gt;ContentType, $this-&gt;CharSet);
                break;
            case "attachments":
                // fall through
            case "alt_attachments":
                if($this-&gt;InlineImageExists())
                {
                    $result .= sprintf("Content-Type: %s;%s\ttype=\"text/html\";%s\tboundary=\"%s\"%s", 
                                    "multipart/related", $this-&gt;LE, $this-&gt;LE, 
                                    $this-&gt;boundary[1], $this-&gt;LE);
                }
                else
                {
                    $result .= $this-&gt;HeaderLine("Content-Type", "multipart/mixed;");
                    $result .= $this-&gt;TextLine("\tboundary=\"" . $this-&gt;boundary[1] . '"');
                }
                break;
            case "alt":
                $result .= $this-&gt;HeaderLine("Content-Type", "multipart/alternative;");
                $result .= $this-&gt;TextLine("\tboundary=\"" . $this-&gt;boundary[1] . '"');
                break;
        }

        if($this-&gt;Mailer != "mail")
            $result .= $this-&gt;LE.$this-&gt;LE;

        return $result;
    }

    /**
     * Assembles the message body.  Returns an empty string on failure.
     * @access private
     * @return string
     */
    function CreateBody() {
        $result = "";

        $this-&gt;SetWordWrap();

        switch($this-&gt;message_type)
        {
            case "alt":
                $result .= $this-&gt;GetBoundary($this-&gt;boundary[1], "", 
                                              "text/plain", "");
                $result .= $this-&gt;EncodeString($this-&gt;AltBody, $this-&gt;Encoding);
                $result .= $this-&gt;LE.$this-&gt;LE;
                $result .= $this-&gt;GetBoundary($this-&gt;boundary[1], "", 
                                              "text/html", "");
                
                $result .= $this-&gt;EncodeString($this-&gt;Body, $this-&gt;Encoding);
                $result .= $this-&gt;LE.$this-&gt;LE;
    
                $result .= $this-&gt;EndBoundary($this-&gt;boundary[1]);
                break;
            case "plain":
                $result .= $this-&gt;EncodeString($this-&gt;Body, $this-&gt;Encoding);
                break;
            case "attachments":
                $result .= $this-&gt;GetBoundary($this-&gt;boundary[1], "", "", "");
                $result .= $this-&gt;EncodeString($this-&gt;Body, $this-&gt;Encoding);
                $result .= $this-&gt;LE;
     
                $result .= $this-&gt;AttachAll();
                break;
            case "alt_attachments":
                $result .= sprintf("--%s%s", $this-&gt;boundary[1], $this-&gt;LE);
                $result .= sprintf("Content-Type: %s;%s" .
                                   "\tboundary=\"%s\"%s",
                                   "multipart/alternative", $this-&gt;LE, 
                                   $this-&gt;boundary[2], $this-&gt;LE.$this-&gt;LE);
    
                // Create text body
                $result .= $this-&gt;GetBoundary($this-&gt;boundary[2], "", 
                                              "text/plain", "") . $this-&gt;LE;

                $result .= $this-&gt;EncodeString($this-&gt;AltBody, $this-&gt;Encoding);
                $result .= $this-&gt;LE.$this-&gt;LE;
    
                // Create the HTML body
                $result .= $this-&gt;GetBoundary($this-&gt;boundary[2], "", 
                                              "text/html", "") . $this-&gt;LE;
    
                $result .= $this-&gt;EncodeString($this-&gt;Body, $this-&gt;Encoding);
                $result .= $this-&gt;LE.$this-&gt;LE;

                $result .= $this-&gt;EndBoundary($this-&gt;boundary[2]);
                
                $result .= $this-&gt;AttachAll();
                break;
        }
        if($this-&gt;IsError())
            $result = "";

        return $result;
    }

    /**
     * Returns the start of a message boundary.
     * @access private
     */
    function GetBoundary($boundary, $charSet, $contentType, $encoding) {
        $result = "";
        if($charSet == "") { $charSet = $this-&gt;CharSet; }
        if($contentType == "") { $contentType = $this-&gt;ContentType; }
        if($encoding == "") { $encoding = $this-&gt;Encoding; }

        $result .= $this-&gt;TextLine("--" . $boundary);
        $result .= sprintf("Content-Type: %s; charset = \"%s\"", 
                            $contentType, $charSet);
        $result .= $this-&gt;LE;
        $result .= $this-&gt;HeaderLine("Content-Transfer-Encoding", $encoding);
        $result .= $this-&gt;LE;
       
        return $result;
    }
    
    /**
     * Returns the end of a message boundary.
     * @access private
     */
    function EndBoundary($boundary) {
        return $this-&gt;LE . "--" . $boundary . "--" . $this-&gt;LE; 
    }
    
    /**
     * Sets the message type.
     * @access private
     * @return void
     */
    function SetMessageType() {
        if(count($this-&gt;attachment) &lt; 1 &amp;&amp; strlen($this-&gt;AltBody) &lt; 1)
            $this-&gt;message_type = "plain";
        else
        {
            if(count($this-&gt;attachment) &gt; 0)
                $this-&gt;message_type = "attachments";
            if(strlen($this-&gt;AltBody) &gt; 0 &amp;&amp; count($this-&gt;attachment) &lt; 1)
                $this-&gt;message_type = "alt";
            if(strlen($this-&gt;AltBody) &gt; 0 &amp;&amp; count($this-&gt;attachment) &gt; 0)
                $this-&gt;message_type = "alt_attachments";
        }
    }

    /**
     * Returns a formatted header line.
     * @access private
     * @return string
     */
    function HeaderLine($name, $value) {
        return $name . ": " . $value . $this-&gt;LE;
    }

    /**
     * Returns a formatted mail line.
     * @access private
     * @return string
     */
    function TextLine($value) {
        return $value . $this-&gt;LE;
    }

    /////////////////////////////////////////////////
    // ATTACHMENT METHODS
    /////////////////////////////////////////////////

    /**
     * Adds an attachment from a path on the filesystem.
     * Returns false if the file could not be found
     * or accessed.
     * @param string $path Path to the attachment.
     * @param string $name Overrides the attachment name.
     * @param string $encoding File encoding (see $Encoding).
     * @param string $type File extension (MIME) type.
     * @return bool
     */
    function AddAttachment($path, $name = "", $encoding = "base64", 
                           $type = "application/octet-stream") {
        if(!@is_file($path))
        {
            $this-&gt;SetError($this-&gt;Lang("file_access") . $path);
            return false;
        }

        $filename = basename($path);
        if($name == "")
            $name = $filename;

        $cur = count($this-&gt;attachment);
        $this-&gt;attachment[$cur][0] = $path;
        $this-&gt;attachment[$cur][1] = $filename;
        $this-&gt;attachment[$cur][2] = $name;
        $this-&gt;attachment[$cur][3] = $encoding;
        $this-&gt;attachment[$cur][4] = $type;
        $this-&gt;attachment[$cur][5] = false; // isStringAttachment
        $this-&gt;attachment[$cur][6] = "attachment";
        $this-&gt;attachment[$cur][7] = 0;

        return true;
    }

    /**
     * Attaches all fs, string, and binary attachments to the message.
     * Returns an empty string on failure.
     * @access private
     * @return string
     */
    function AttachAll() {
        // Return text of body
        $mime = array();

        // Add all attachments
        for($i = 0; $i &lt; count($this-&gt;attachment); $i++)
        {
            // Check for string attachment
            $bString = $this-&gt;attachment[$i][5];
            if ($bString)
                $string = $this-&gt;attachment[$i][0];
            else
                $path = $this-&gt;attachment[$i][0];

            $filename    = $this-&gt;attachment[$i][1];
            $name        = $this-&gt;attachment[$i][2];
            $encoding    = $this-&gt;attachment[$i][3];
            $type        = $this-&gt;attachment[$i][4];
            $disposition = $this-&gt;attachment[$i][6];
            $cid         = $this-&gt;attachment[$i][7];
            
            $mime[] = sprintf("--%s%s", $this-&gt;boundary[1], $this-&gt;LE);
            $mime[] = sprintf("Content-Type: %s; name=\"%s\"%s", $type, $name, $this-&gt;LE);
            $mime[] = sprintf("Content-Transfer-Encoding: %s%s", $encoding, $this-&gt;LE);

            if($disposition == "inline")
                $mime[] = sprintf("Content-ID: &lt;%s&gt;%s", $cid, $this-&gt;LE);

            $mime[] = sprintf("Content-Disposition: %s; filename=\"%s\"%s", 
                              $disposition, $name, $this-&gt;LE.$this-&gt;LE);

            // Encode as string attachment
            if($bString)
            {
                $mime[] = $this-&gt;EncodeString($string, $encoding);
                if($this-&gt;IsError()) { return ""; }
                $mime[] = $this-&gt;LE.$this-&gt;LE;
            }
            else
            {
                $mime[] = $this-&gt;EncodeFile($path, $encoding);                
                if($this-&gt;IsError()) { return ""; }
                $mime[] = $this-&gt;LE.$this-&gt;LE;
            }
        }

        $mime[] = sprintf("--%s--%s", $this-&gt;boundary[1], $this-&gt;LE);

        return join("", $mime);
    }
    
    /**
     * Encodes attachment in requested format.  Returns an
     * empty string on failure.
     * @access private
     * @return string
     */
    function EncodeFile ($path, $encoding = "base64") {
        if(!@$fd = fopen($path, "rb"))
        {
            $this-&gt;SetError($this-&gt;Lang("file_open") . $path);
            return "";
        }
        $magic_quotes = get_magic_quotes_runtime();
        set_magic_quotes_runtime(0);
        $file_buffer = fread($fd, filesize($path));
        $file_buffer = $this-&gt;EncodeString($file_buffer, $encoding);
        fclose($fd);
        set_magic_quotes_runtime($magic_quotes);

        return $file_buffer;
    }

    /**
     * Encodes string to requested format. Returns an
     * empty string on failure.
     * @access private
     * @return string
     */
    function EncodeString ($str, $encoding = "base64") {
        $encoded = "";
        switch(strtolower($encoding)) {
          case "base64":
              // chunk_split is found in PHP &gt;= 3.0.6
              $encoded = chunk_split(base64_encode($str), 76, $this-&gt;LE);
              break;
          case "7bit":
          case "8bit":
              $encoded = $this-&gt;FixEOL($str);
              if (substr($encoded, -(strlen($this-&gt;LE))) != $this-&gt;LE)
                $encoded .= $this-&gt;LE;
              break;
          case "binary":
              $encoded = $str;
              break;
          case "quoted-printable":
              $encoded = $this-&gt;EncodeQP($str);
              break;
          default:
              $this-&gt;SetError($this-&gt;Lang("encoding") . $encoding);
              break;
        }
        return $encoded;
    }

    /**
     * Encode a header string to best of Q, B, quoted or none.  
     * @access private
     * @return string
     */
    function EncodeHeader ($str, $position = 'text') {
      $x = 0;
      
      switch (strtolower($position)) {
        case 'phrase':
          if (!preg_match('/[\200-\377]/', $str)) {
            // Can't use addslashes as we don't know what value has magic_quotes_sybase.
            $encoded = addcslashes($str, "\0..\37\177\\\"");

            if (($str == $encoded) &amp;&amp; !preg_match('/[^A-Za-z0-9!#$%&amp;\'*+\/=?^_`{|}~ -]/', $str))
              return ($encoded);
            else
              return ("\"$encoded\"");
          }
          $x = preg_match_all('/[^\040\041\043-\133\135-\176]/', $str, $matches);
          break;
        case 'comment':
          $x = preg_match_all('/[()"]/', $str, $matches);
          // Fall-through
        case 'text':
        default:
          $x += preg_match_all('/[\000-\010\013\014\016-\037\177-\377]/', $str, $matches);
          break;
      }

      if ($x == 0)
        return ($str);

      $maxlen = 75 - 7 - strlen($this-&gt;CharSet);
      // Try to select the encoding which should produce the shortest output
      if (strlen($str)/3 &lt; $x) {
        $encoding = 'B';
        $encoded = base64_encode($str);
        $maxlen -= $maxlen % 4;
        $encoded = trim(chunk_split($encoded, $maxlen, "\n"));
      } else {
        $encoding = 'Q';
        $encoded = $this-&gt;EncodeQ($str, $position);
        $encoded = $this-&gt;WrapText($encoded, $maxlen, true);
        $encoded = str_replace("=".$this-&gt;LE, "\n", trim($encoded));
      }

      $encoded = preg_replace('/^(.*)$/m', " =?".$this-&gt;CharSet."?$encoding?\\1?=", $encoded);
      $encoded = trim(str_replace("\n", $this-&gt;LE, $encoded));
      
      return $encoded;
    }
    
    /**
     * Encode string to quoted-printable.  
     * @access private
     * @return string
     */
    function EncodeQP ($str) {
        $encoded = $this-&gt;FixEOL($str);
        if (substr($encoded, -(strlen($this-&gt;LE))) != $this-&gt;LE)
            $encoded .= $this-&gt;LE;

        // Replace every high ascii, control and = characters
        $encoded = preg_replace('/([\000-\010\013\014\016-\037\075\177-\377])/e',
                  "'='.sprintf('%02X', ord('\\1'))", $encoded);
        // Replace every spaces and tabs when it's the last character on a line
        $encoded = preg_replace("/([\011\040])".$this-&gt;LE."/e",
                  "'='.sprintf('%02X', ord('\\1')).'".$this-&gt;LE."'", $encoded);

        // Maximum line length of 76 characters before CRLF (74 + space + '=')
        $encoded = $this-&gt;WrapText($encoded, 74, true);

        return $encoded;
    }

    /**
     * Encode string to q encoding.  
     * @access private
     * @return string
     */
    function EncodeQ ($str, $position = "text") {
        // There should not be any EOL in the string
        $encoded = preg_replace("[\r\n]", "", $str);

        switch (strtolower($position)) {
          case "phrase":
            $encoded = preg_replace("/([^A-Za-z0-9!*+\/ -])/e", "'='.sprintf('%02X', ord('\\1'))", $encoded);
            break;
          case "comment":
            $encoded = preg_replace("/([\(\)\"])/e", "'='.sprintf('%02X', ord('\\1'))", $encoded);
          case "text":
          default:
            // Replace every high ascii, control =, ? and _ characters
            $encoded = preg_replace('/([\000-\011\013\014\016-\037\075\077\137\177-\377])/e',
                  "'='.sprintf('%02X', ord('\\1'))", $encoded);
            break;
        }
        
        // Replace every spaces to _ (more readable than =20)
        $encoded = str_replace(" ", "_", $encoded);

        return $encoded;
    }

    /**
     * Adds a string or binary attachment (non-filesystem) to the list.
     * This method can be used to attach ascii or binary data,
     * such as a BLOB record from a database.
     * @param string $string String attachment data.
     * @param string $filename Name of the attachment.
     * @param string $encoding File encoding (see $Encoding).
     * @param string $type File extension (MIME) type.
     * @return void
     */
    function AddStringAttachment($string, $filename, $encoding = "base64", 
                                 $type = "application/octet-stream") {
        // Append to $attachment array
        $cur = count($this-&gt;attachment);
        $this-&gt;attachment[$cur][0] = $string;
        $this-&gt;attachment[$cur][1] = $filename;
        $this-&gt;attachment[$cur][2] = $filename;
        $this-&gt;attachment[$cur][3] = $encoding;
        $this-&gt;attachment[$cur][4] = $type;
        $this-&gt;attachment[$cur][5] = true; // isString
        $this-&gt;attachment[$cur][6] = "attachment";
        $this-&gt;attachment[$cur][7] = 0;
    }
    
    /**
     * Adds an embedded attachment.  This can include images, sounds, and 
     * just about any other document.  Make sure to set the $type to an 
     * image type.  For JPEG images use "image/jpeg" and for GIF images 
     * use "image/gif".
     * @param string $path Path to the attachment.
     * @param string $cid Content ID of the attachment.  Use this to identify 
     *        the Id for accessing the image in an HTML form.
     * @param string $name Overrides the attachment name.
     * @param string $encoding File encoding (see $Encoding).
     * @param string $type File extension (MIME) type.  
     * @return bool
     */
    function AddEmbeddedImage($path, $cid, $name = "", $encoding = "base64", 
                              $type = "application/octet-stream") {
    
        if(!@is_file($path))
        {
            $this-&gt;SetError($this-&gt;Lang("file_access") . $path);
            return false;
        }

        $filename = basename($path);
        if($name == "")
            $name = $filename;

        // Append to $attachment array
        $cur = count($this-&gt;attachment);
        $this-&gt;attachment[$cur][0] = $path;
        $this-&gt;attachment[$cur][1] = $filename;
        $this-&gt;attachment[$cur][2] = $name;
        $this-&gt;attachment[$cur][3] = $encoding;
        $this-&gt;attachment[$cur][4] = $type;
        $this-&gt;attachment[$cur][5] = false; // isStringAttachment
        $this-&gt;attachment[$cur][6] = "inline";
        $this-&gt;attachment[$cur][7] = $cid;
    
        return true;
    }
    
    /**
     * Returns true if an inline attachment is present.
     * @access private
     * @return bool
     */
    function InlineImageExists() {
        $result = false;
        for($i = 0; $i &lt; count($this-&gt;attachment); $i++)
        {
            if($this-&gt;attachment[$i][6] == "inline")
            {
                $result = true;
                break;
            }
        }
        
        return $result;
    }

    /////////////////////////////////////////////////
    // MESSAGE RESET METHODS
    /////////////////////////////////////////////////

    /**
     * Clears all recipients assigned in the TO array.  Returns void.
     * @return void
     */
    function ClearAddresses() {
        $this-&gt;to = array();
    }

    /**
     * Clears all recipients assigned in the CC array.  Returns void.
     * @return void
     */
    function ClearCCs() {
        $this-&gt;cc = array();
    }

    /**
     * Clears all recipients assigned in the BCC array.  Returns void.
     * @return void
     */
    function ClearBCCs() {
        $this-&gt;bcc = array();
    }

    /**
     * Clears all recipients assigned in the ReplyTo array.  Returns void.
     * @return void
     */
    function ClearReplyTos() {
        $this-&gt;ReplyTo = array();
    }

    /**
     * Clears all recipients assigned in the TO, CC and BCC
     * array.  Returns void.
     * @return void
     */
    function ClearAllRecipients() {
        $this-&gt;to = array();
        $this-&gt;cc = array();
        $this-&gt;bcc = array();
    }

    /**
     * Clears all previously set filesystem, string, and binary
     * attachments.  Returns void.
     * @return void
     */
    function ClearAttachments() {
        $this-&gt;attachment = array();
    }

    /**
     * Clears all custom headers.  Returns void.
     * @return void
     */
    function ClearCustomHeaders() {
        $this-&gt;CustomHeader = array();
    }


    /////////////////////////////////////////////////
    // MISCELLANEOUS METHODS
    /////////////////////////////////////////////////

    /**
     * Adds the error message to the error container.
     * Returns void.
     * @access private
     * @return void
     */
    function SetError($msg) {
        $this-&gt;error_count++;
        $this-&gt;ErrorInfo = $msg;
    }

    /**
     * Returns the proper RFC 822 formatted date. 
     * @access private
     * @return string
     */
    function RFCDate() {
        $tz = date("Z");
        $tzs = ($tz &lt; 0) ? "-" : "+";
        $tz = abs($tz);
        $tz = ($tz/3600)*100 + ($tz%3600)/60;
        $result = sprintf("%s %s%04d", date("D, j M Y H:i:s"), $tzs, $tz);

        return $result;
    }
    
    /**
     * Returns the appropriate server variable.  Should work with both 
     * PHP 4.1.0+ as well as older versions.  Returns an empty string 
     * if nothing is found.
     * @access private
     * @return mixed
     */
    function ServerVar($varName) {
        global $HTTP_SERVER_VARS;
        global $HTTP_ENV_VARS;

        if(!isset($_SERVER))
        {
            $_SERVER = $HTTP_SERVER_VARS;
            if(!isset($_SERVER["REMOTE_ADDR"]))
                $_SERVER = $HTTP_ENV_VARS; // must be Apache
        }
        
        if(isset($_SERVER[$varName]))
            return $_SERVER[$varName];
        else
            return "";
    }

    /**
     * Returns the server hostname or 'localhost.localdomain' if unknown.
     * @access private
     * @return string
     */
    function ServerHostname() {
        if ($this-&gt;Hostname != "")
            $result = $this-&gt;Hostname;
        elseif ($this-&gt;ServerVar('SERVER_NAME') != "")
            $result = $this-&gt;ServerVar('SERVER_NAME');
        else
            $result = "localhost.localdomain";

        return $result;
    }

    /**
     * Returns a message in the appropriate language.
     * @access private
     * @return string
     */
    function Lang($key) {
        if(count($this-&gt;language) &lt; 1)
            $this-&gt;SetLanguage("en"); // set the default language
    
        if(isset($this-&gt;language[$key]))
            return $this-&gt;language[$key];
        else
            return "Language string failed to load: " . $key;
    }
    
    /**
     * Returns true if an error occurred.
     * @return bool
     */
    function IsError() {
        return ($this-&gt;error_count &gt; 0);
    }

    /**
     * Changes every end of line from CR or LF to CRLF.  
     * @access private
     * @return string
     */
    function FixEOL($str) {
        $str = str_replace("\r\n", "\n", $str);
        $str = str_replace("\r", "\n", $str);
        $str = str_replace("\n", $this-&gt;LE, $str);
        return $str;
    }

    /**
     * Adds a custom header. 
     * @return void
     */
    function AddCustomHeader($custom_header) {
        $this-&gt;CustomHeader[] = explode(":", $custom_header, 2);
    }
}

?>