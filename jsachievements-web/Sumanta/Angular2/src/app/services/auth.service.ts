import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth, AngularFireAuthProvider, AngularFireAuthModule } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {
  
  public user: Observable<firebase.User>;

  constructor(private angularFireAuth: AngularFireAuth, private router: Router) {
  	this.user = angularFireAuth.authState; 
  }

  loginWithGoogle(){
  	return Observable.fromPromise(
  		this.angularFireAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
  	)
  }

  login(email, password): Observable<any>{
  	return Observable.fromPromise(
  		this.angularFireAuth.auth.signInWithEmailAndPassword(email, password)
  	)
  }

  isAuthenticated(): Observable<boolean>{
  	return this.user.map(user => user && user.uid !== undefined);
  }

  logout(){
  	this.angularFireAuth.auth.signOut(); 
  	this.router.navigate(['/login']);
  }

}
