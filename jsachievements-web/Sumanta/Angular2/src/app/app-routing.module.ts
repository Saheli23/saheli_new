import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {itemComponent} from './item.component';
import {DashboardComponent} from './dashboard.component';
import {LoginComponent} from './login.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard',  component: DashboardComponent },
  { path: 'items/:id',  component: itemComponent },
  { path: 'login',     component: LoginComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
