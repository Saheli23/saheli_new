import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public isLoggedin;

  constructor(private authService: AuthService){
  	authService.isAuthenticated()
  	  .subscribe(
  	  	success => this.isLoggedin = success
  	  )
  }

  logout(){
  	this.authService.logout();
  }
}
