import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';
// import { HeroService } from './hero.service';

@Component({
  selector: 'my-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css' ]
})
export class LoginComponent implements OnInit {
  // heroes: Hero[] = [];
  public form: FormGroup;

  constructor(
    private formBilder: FormBuilder, 
    private authService: AuthService,
    private router : Router) { 
  	this.form = this.formBilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  login(): void{
    const inputValue = this.form.value;
    this.authService.login(inputValue.email, inputValue.password)
      .subscribe(
        success =>  this.router.navigate(['/dashboard']),
        error => alert(error)
      );
  }

  loginWithGoogle(){
    this.authService.loginWithGoogle()
      .subscribe(
        success =>  this.router.navigate(['/dashboard']),
        error => alert(error)
      );
  }
}
