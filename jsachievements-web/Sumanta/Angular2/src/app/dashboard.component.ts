import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
// import { HeroService } from './hero.service';

@Component({
  selector: 'my-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  // heroes: Hero[] = [];
  items: FirebaseListObservable<any[]>;

  constructor(db: AngularFireDatabase) { 
  	this.items = db.list('/restaurants', {
      query: {
        limitToLast: 4
      }
    });
  }

  ngOnInit(): void {
    // this.heroService.getHeroes()
      // .then(heroes => this.heroes = heroes.slice(1, 5));
  }
}
