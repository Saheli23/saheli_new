import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { DashboardComponent }   from './dashboard.component';
import { itemComponent } from './item.component';
import { LoginComponent } from './login.component';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthService } from './services/auth.service';

export const firebaseConfig = {
  apiKey: 'AIzaSyBLAjan7CP7GXCQBu8uerNXflzYkeux7UQ',
  authDomain: 'undo-bb6de.firebaseapp.com',
  databaseURL: 'https://undo-bb6de.firebaseio.com',
  storageBucket: 'undo-bb6de.appspot.com'
  // messagingSenderId: 'undo-bb6de'
};

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    itemComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
