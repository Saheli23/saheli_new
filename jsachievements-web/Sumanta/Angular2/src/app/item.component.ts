import { Component, OnInit } from '@angular/core';
// import { HeroService } from './services/restaurants.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location }                 from '@angular/common';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';

@Component({
  selector: 'item-root',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})

export class itemComponent implements OnInit {
	items: FirebaseListObservable<any[]>;

	constructor(
	    private route: ActivatedRoute,
	    private location: Location,
	    db: AngularFireDatabase) {
		this.items = db.list('/restaurants/'+route.snapshot.paramMap.get('id')+'/items', {
		    query: {
		      limitToLast: 4
		    }
		});
	}

	ngOnInit(): void {

	}
}