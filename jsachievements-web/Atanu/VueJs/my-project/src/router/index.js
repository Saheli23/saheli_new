import Vue from 'vue'
import Router from 'vue-router'
import addblog from '@/components/addblog'
import showBlog from '@/components/showBlog'
import singleBlog from '@/components/singleBlog'

Vue.use(Router)

export default new Router({
  //mode:'history',
  routes: [
    { path: '*', 
      redirect: '/'
    },
    {
      path: '/',
      name: 'addblog',
      component: addblog
    },
    {
      path: '/show',
      name: 'showBlog',
      component: showBlog	
    },
    {
      path: '/blog/:id',
      name: 'singleBlog',
      component: singleBlog	
    },
    {
      path: '/:id',
      name: 'editblog',
      component: addblog
    }
  ]
})
