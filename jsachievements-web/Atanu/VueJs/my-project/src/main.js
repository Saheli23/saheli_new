
import Vue from 'vue'
import App from './App'
import VueResource from 'vue-resource'
import router from './router'

//export const bus = new Vue();

//Vue.config.productionTip = false

// Use vue-resource package
Vue.use(VueResource);

// Filters
Vue.filter('to-uppercase', function(value){
    return value.toUpperCase();
});

Vue.filter('snippet',function(value){
      return value.slice(0,100)+' ...';
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
