import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login'
import loginHome from '@/components/loginHome'
import showAll from '@/components/showAll'
import likes from '@/components/likes'

Vue.use(Router)

const router = new Router({
  //mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login',
      component: login
    },
    {
      path: '/home',
      name: 'AddNewBook',
      component: loginHome,
      meta: { requiresAuth: true }
    },
    {
      path: '/books',
      name: 'showAllBook',
      component: showAll,
      meta: { requiresAuth: true }
    },
    {
      path: '/home/:id',
      name: 'EditBook',
      component: loginHome,
      meta: { requiresAuth: true }
    },
    {
      path: '/mylike',
      name: 'myLike',
      component: likes,
      meta: { requiresAuth: true }
    }
  ]
});

router.beforeEach((to, from, next)=>{
  if(to.meta.requiresAuth)
  {
    const currentUser=sessionStorage.getItem("userName") || "";
    if(currentUser!=""){
      next()
    }else{
      alert('Please login before access this page...');
      next({name:'login'})
    }
  }
  next()
});

export default router;