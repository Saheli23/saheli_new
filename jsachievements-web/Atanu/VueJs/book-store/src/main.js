import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'

Vue.config.productionTip = false;
Vue.use(VueResource);
export const bus = new Vue();

Vue.filter('snippet',function(value){
      return value.slice(0,50)+' ...';
});
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})

