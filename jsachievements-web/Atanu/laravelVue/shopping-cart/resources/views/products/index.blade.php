@extends('layouts.app')

@section('content')

<div class="col-md-9 col-lg-9">
	<div class="panel panel-primary"> 
		<div class="panel-heading">Product List</div> 
		<table class="table"> 
			<thead> 
				<tr> 
					<th>#</th> 
					<th class="text-center">Product Name</th> 
					<th class="text-center">Price</th>
					<th class="text-center">Permission</th>
				</tr> 
			</thead> 
			<tbody> 
				@foreach($products as $indexKey =>$product)
					<tr> 
						<th scope="row">{{++$indexKey}}</th> 
						<td class="text-center">{{$product->name}}</td> 
						<td class="text-center">{{$product->price}}</td> 
						<td class="text-center">
							<a href="/products/{{$product->id}}"><button class="btn btn-success">View</button></a>
							<a href="/products/{{$product->id}}/edit"><button class="btn btn-primary">Edit</button></a>

							<button class="btn btn-danger"
								onclick="
									var result=confirm('Are you sure you wish to delete this Product?');
									if(result){
										event.preventDefault();
										document.getElementById('delete-form-{{$indexKey}}').submit();
									}
								">
								Delete
							</button>

							<form id="delete-form-{{$indexKey}}" action="{{ route('products.destroy', [$product->id]) }}"method="POST" style="display: none;">
								<input type="hidden" name="_method" value="delete">
								{{ csrf_field() }}
							</form>
							<!-- <span class="glyphicon glyphicon-trash"></span>-->
	        			</td> 
					</tr> 
				@endforeach
			</tbody> 
		</table> 
	</div>
</div>

@endsection