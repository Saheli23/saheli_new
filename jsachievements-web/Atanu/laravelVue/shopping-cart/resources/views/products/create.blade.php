@extends('layouts.app')

@section('content')

<div class="col-md-9 col-lg-9 col-sm-9">
  <div class="panel panel-default">
    <div class="panel-heading">Add New Product</div>
    <div class="panel-body">
      <form method="post" action="{{route('products.store')}}" enctype="multipart/form-data">
      {{ csrf_field() }}  
        <input type="hidden" name="userId" id="userId" value="{{Auth::id()}}">
        <div class="form-group">
          <label for="name">Name:</label>
          <input type="text" class="form-control" id="pname" placeholder="Enter Product name" name="pname">
        </div>

        <div class="form-group">
          <label for="category">Category:</label>
          <select class="form-control" name="pcategory" id="pcategory">
            <option value="">Please Select...</option>
            <option value="2">Mobile</option>
            <option value="3">Laptop</option>
          </select>
        </div>

        <div class="form-group">
          <label for="price">Price:</label>
          <input type="text" class="form-control" id="pprice" placeholder="Enter Product Price" name="pprice">
        </div>

        <div class="form-group">
          <label for="Image">Select Image:</label>
          <input type="file" class="form-control" id="pimage" name="pimage">
        </div>

        <div class="form-group">
          <label for="desc">Description:</label>
          <textarea class="form-control" id="pdesc" placeholder="Enter Product Description" name="pdesc"></textarea>
        </div>

        <!--<div class="checkbox">
          <label><input type="checkbox" name="remember"> Remember me</label>
        </div>-->

        <button type="submit" class="btn btn-primary">Add</button>
      </form>
    </div>
  </div>
</div> 
@endsection