@extends('layouts.app')

@section('content')

<div class="col-md-9 col-lg-9 col-sm-9">
  <div class="panel panel-default">
    <div class="panel-heading">Edit Details</div>
    <div class="panel-body">
      <form method="post" action="{{route('products.update',[$product->id])}}">
      {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        <div class="form-group">
          <label for="name">Name:</label>
          <input type="text" class="form-control" id="pname" placeholder="Enter Product name" name="pname" value="{{$product->name}}">
        </div>

        <div class="form-group">
          <label for="price">Price:</label>
          <input type="text" class="form-control" id="pprice" placeholder="Enter Product Price" name="pprice" value="{{$product->price}}">
        </div>

        <div class="form-group">
          <label for="Image">Select Image:</label>
          <input type="file" class="form-control" id="pimage" placeholder="Select Product Image" name="pimage" value="{{$product->image}}">
        </div>

        <div class="form-group">
          <label for="desc">Description:</label>
          <textarea class="form-control" id="pdesc" placeholder="Enter Product Description" name="pdesc">{{$product->desc}}</textarea>
        </div>

        <!--<div class="checkbox">
          <label><input type="checkbox" name="remember"> Remember me</label>
        </div>-->

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div> 
@endsection