@extends('layouts.app')

@section('content')

<div class="col-md-9 col-lg-9">
	<div class="panel panel-primary"> 
		<div class="panel-heading">
			Product Details 
			<a href="/products/">
				<button class="btn btn-danger pull-right btn-sm">Close</button>
			</a>
		</div> 
		<table class="table"> 
			<thead> 
				<tr> 
					<th class="text-center">Name</th> 
					<th class="text-center">Description</th>
				</tr> 
			</thead> 
			<tbody> 
				<tr> 
					<td class="text-center"> Name </td>
					<td class="text-center">{{$product->name}}</td> 
				</tr> 
				<tr> 
					<td class="text-center"> Price </td>
					<td class="text-center">{{$product->price}}</td> 
				</tr> 
				<tr> 
					<td class="text-center"> Image </td>
					<td class="text-center">
						<img src="{{ asset('images/' . $product->image) }}" style="width: 200px" >
					</td> 
				</tr> 
				<tr> 
					<td class="text-center"> Description </td>
					<td class="text-center">{{$product->desc}}</td> 
				</tr> 
			</tbody> 
		</table> 
	</div>
</div>

@endsection