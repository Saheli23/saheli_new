<div class="col-md-3 col-lg-3 col-sm-3 pull-left">
    <div class="panel panel-default">
        <div class="panel-heading">Dashboard</div>
        <div class="panel-body"> 
            <table class="table"> 
                <tbody> 
                    <tr><td>
                        <h4>
                            <a href="/home">Home</a>
                        </h4>
                    </td></tr>
                    <tr><td>
                        <h4>
                            <a href="/products/create"> Add New </a>
                        </h4>
                    </td></tr>
                    <tr><td>
                        <h4>
                            <a href="/products">Products</a>
                        </h4>
                    </td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>