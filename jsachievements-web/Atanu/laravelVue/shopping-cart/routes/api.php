<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*--Requestvia -http://localhost:8000/api/prductdemo--*/
Route::get('prductdemo', 'ProductdemoController@index');
Route::get('prductdemo/{id}', 'ProductdemoController@show');
Route::post('prductdemo', 'ProductdemoController@store');
Route::put('prductdemo/{id}', 'ProductdemoController@update');
Route::delete('prductdemo/{id}', 'ProductdemoController@delete');


