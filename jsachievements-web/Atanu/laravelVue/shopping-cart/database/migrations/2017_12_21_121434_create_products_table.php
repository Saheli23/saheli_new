<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   if(!Schema::hasTable('products'))
        {   Schema::create('products', function (Blueprint $table) {
                $table->engine = "InnoDB";
                $table->increments('id');
                $table->integer('product_category_id')->unsigned();
                $table->foreign('product_category_id')->references('id')->on('product_category');
                $table->string('name');
                $table->integer('price');
                $table->string('image');
                $table->string('desc');
                $table->integer('created_user_id')->unsigned();
                $table->foreign('created_user_id')->references('id')->on('users');
                $table->integer('updated_user_id')->unsigned();
                $table->foreign('updated_user_id')->references('id')->on('users');
                $table->timestamps();
            });
        }
        /*Schema::table('products', function($table){
            $table->foreign('product_category_id')->references('id')->on('product_category');
            $table->foreign('created_user_id')->references('id')->on('users');
            $table->foreign('updated_user_id')->references('id')->on('users');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
