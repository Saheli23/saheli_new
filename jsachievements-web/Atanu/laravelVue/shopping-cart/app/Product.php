<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'price', 
        'desc',
        'image',
        'created_user_id',
        'updated_user_id',
        'product_category_id'
    ];

   	public function users(){
        return $this->belongsTo('app/User');
    }

    public function product_category(){
        return $this->belongsTo('app/ProductCategory');
    }

    /*protected $hidden = [
        'product_category_id'
    ];*/
}
