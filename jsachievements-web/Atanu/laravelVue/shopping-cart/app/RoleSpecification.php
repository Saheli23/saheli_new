<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleSpecification extends Model
{
    protected $fillable = [
        'name',
        'feature',
    ];

    public function roles(){
        return $this->belongsTo('app/Role');
    }

    protected $hidden = [
       'roles_id', 
    ];
}
