<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name'
    ];

    public function users(){
        return $this->hasMany('app/User');
    }

    public function role_specification(){
        return $this->belongsTo('app/RoleSpecification');
    }
}
