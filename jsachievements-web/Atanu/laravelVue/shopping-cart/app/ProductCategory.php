<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $fillable = [
        'name',
    ];

   	public function products(){
        return $this->belongsTo('app/Product');
    }

    protected $hidden = [
        'parent_id',
    ];
}
