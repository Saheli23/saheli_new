<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use File;
class ProductListController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $products = Product::all();
        return view('products.index', ['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        //
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        if(Auth::check()){
            if(Input::has('pimage')){
                $file = Input::file('pimage');
                $destinationPath = public_path(). '/images/';
                $filename = time(). $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $product = Product::create([
                    'name'=>$request->input('pname'),
                    'product_category_id'=>$request->input('pcategory'),
                    'price'=>$request->input('pprice'),
                    'image'=>$filename,
                    'desc'=>$request->input('pdesc'),
                    'created_user_id'=>$request->input('userId'),
                    'updated_user_id'=>$request->input('userId')
                ]);

                if($product){
                    return redirect()->route('products.show',['product'=>$product->id])
                    ->with('success', 'Product Added Successfully.');
                   // print_r("Successfully Added");
                }
            } 
            //return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $product = Product::where('id', $product->id)->first();
        return view('products.show', ['product'=>$product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product = Product::find($product->id);
        return view('products.edit', ['product'=>$product]);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {   $id = Auth::id();
        $productUpdate = Product::where('id', $product->id)
                                ->update([
                                    'name'=>$request->input('pname'),
                                    'price'=>$request->input('pprice'),
                                    'desc'=>$request->input('pdesc'),
                                    'updated_user_id'=>$id
                                ]);
        if($productUpdate){
            return redirect()->route('products.show', ['product'=>$product->id])
            ->with('success', 'Product Update Successfully');
        }
        //redirect
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {   //dd($product);
        //print_r($product->id);
        $file_path =public_path().'/images/'.$product->image;
        unlink($file_path);
        $findProduct = Product::find($product->id);
        if($findProduct->delete()){
            return redirect()->route('products.index')
            ->with('success', 'Product deleted successfully');
        }
        return back()->withInput()->with('error', 'Product could not be deleted');
    }
}
