import Vue from 'vue'
import {baseUrl} from '../data/url';


export default {
    get(url, request) {
        let path=baseUrl+url;
        return Vue.http.get(path, request)
            .then((response) => Promise.resolve(response.body))
            .catch((error) => Promise.reject(error));
    },
    post(url, request) {
        let path=baseUrl+url;
        return Vue.http.post(path, request)
            .then((response) => Promise.resolve(response))
            .catch((error) => Promise.reject(error));
    },
    patch(url, request) {
       let path=baseUrl+url;
        return Vue.http.patch(path, request)
            .then((response) => Promise.resolve(response))
            .catch((error) => Promise.reject(error));
    },
    delete(url, request) {
        let path=baseUrl+url;
        return Vue.http.delete(path, request)
            .then((response) => Promise.resolve(response))
            .catch((error) => Promise.reject(error));
    }
}