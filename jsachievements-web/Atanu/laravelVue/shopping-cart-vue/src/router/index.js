import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Home from '@/components/Home'
import viewProduct from '@/components/viewProduct'
import viewCartItem from '@/components/viewCartItem'
import userLogin from '@/components/UserLogin'
import orderCheckout from '@/components/orderCheckout'

Vue.use(Router)

export default new Router({
  mode: 'history',  
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/details/:id',
      name: 'viewDetails',
      component: viewProduct
    },
    {
      path: '/viewCart',
      name: 'viewCartDetails',
      component: viewCartItem
    },
    {
      path: '/login',
      name: 'userLogin',
      component: userLogin
    },
    {
      path: '/checkout',
      name: 'orderCheckout',
      component: orderCheckout
    }
  ]
})
