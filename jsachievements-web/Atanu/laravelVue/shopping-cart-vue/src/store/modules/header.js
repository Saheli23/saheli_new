import api from '../../api/api';
import {baseUrl} from '../../data/url';
// initial state
const state = {
  activeClass:
  {
    homeActive:true,
    loginActive:false
  },
  no_of_cart_item:'',
  searchText:''
}

// getters
const getters = {
  activeClass: state => state.activeClass,
  no_of_cart_item: state => state.no_of_cart_item,
  searchText: state => state.searchText
}

// actions
const actions = {
	getSearchData: (context,data) =>{
    context.commit('UPDATE_SEARCHTEXT',data);
  } 
}

// mutations
const mutations = {
  UPDATE_SEARCHTEXT:(state,  data) =>{
    state.searchText=data;
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}