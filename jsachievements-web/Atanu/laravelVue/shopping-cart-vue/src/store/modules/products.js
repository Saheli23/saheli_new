import api from '../../api/api';
import {baseUrl} from '../../data/url';
// initial state
const state = {
  products: [],
  panelActive:true
}

// getters
const getters = {
  allProducts: state => state.products,
  panelActive: state => state.panelActive
}

// actions
const actions = {
  	getAllProducts (context) {
    	return api.get('/prductdemo')
    		.then((response) => context.commit('RECEIVE_PRODUCTS', response))
    		.catch((error) => context.commit('API_FAILURE', error));
  	}
}

// mutations
const mutations = {
  RECEIVE_PRODUCTS(state,  data) {
    state.products = data;
    state.panelActive=false;
    //console.log(data);
  },
  API_FAILURE(state, error){
    console.log('API_FAILURE');
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}