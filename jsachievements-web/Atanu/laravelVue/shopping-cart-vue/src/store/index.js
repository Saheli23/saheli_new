import Vue from 'vue'
import VueX from 'vuex'
import products from './modules/products'
import header from './modules/header'

Vue.use(VueX);

const debug = process.env.NODE_ENV !== 'production'

export default new VueX.Store({
	modules: {
		products,
		header
	},
	strict:debug
})