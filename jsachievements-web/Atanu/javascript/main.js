$(document).ready(function(){
	document.getElementById("itemInputForm").addEventListener('submit', saveItem);
	
	document.addEventListener("click", function(e){
		if(e.target.matches('.btnEditItem')){
			var id=e.target.id;
			//console.log(id);
			setStatusEdit(id)
		}
		if(e.target.matches('.btnDeleteItem')){
			var id=e.target.id;
			//console.log(id);
			setStatusDelete(id);
		}
		if(e.target.matches('.categoryFilter')){
			var id=e.target.id;
			myFunction(id);
		}
		//console.log(e.target);
	});
})
var filterCategoryIndex=0;
function saveItem(e){
	e.preventDefault();
	var itemName=document.getElementById('nameInput').value;
	var itemCategory=document.getElementById('categoryInput').value;
	var editItemId=document.getElementById('categoryId').value;
	
	if (itemName == "" || itemCategory=="") {
        alert("Name and Category must be filled out");
        return false;
    }
    else
    {
		addNewCategory(itemCategory);
		if(editItemId!=""){
			var editItems = JSON.parse(localStorage.getItem('items'));
			for(var i=0; i<editItems.length; i++){
				if(editItems[i].id==editItemId)
					index=i;
			}

			editItems[index]['itemName']=itemName;
			editItems[index]['itemCategory']=itemCategory;
			localStorage.setItem('items', JSON.stringify(editItems));
			document.getElementById('categoryId').value="";
		}
		else
		{
			var index=0;
			if(localStorage.getItem('index') == null){
				index=0;
				localStorage.setItem('index', index);
			}else{
				index = localStorage.getItem('index');
				index++;
				localStorage.setItem('index', index);
			}

			var item = {
				id: index,
				itemName: itemName,
				itemCategory: itemCategory
			}
	
			if(localStorage.getItem('items') == null){
				var items = [];
				items.push(item);
				localStorage.setItem('items', JSON.stringify(items));
			}else{
				var items = JSON.parse(localStorage.getItem('items'));
				items.push(item);
				localStorage.setItem('items', JSON.stringify(items));
			}
		}
		document.getElementById('itemInputForm').reset();
		fetchItem();
	}
}

function fetchItem(){
	var items = JSON.parse(localStorage.getItem('items')) || [];
	var itemList = document.getElementById('itemList');
	itemList.innerHTML = '';
	if(items.length>0)
	{
		for (var i = 0; i <items.length; i++) {
			var id = items[i].id;
			var name=items[i].itemName;
			var category=items[i].itemCategory;
			//console.log(id);
			itemList.innerHTML += 
				'<div class="col-md-4"><div class="well">'+
					'<h6> Item ID : '+id+'</h6>'+
					'<p> Name : '+name+'</p>'+
					'<p> Category : '+category+'</p>'+
					'<p><a href="#" id="'+ id +'" class="btn btn-warning btnEditItem"> Edit </a>'+" "+
					'<a href="#" id="'+ id +'" class="btn btn-danger btnDeleteItem"> Delete </a></p>'
				+'</div></div>';
		}
	}
	else{
		itemList.innerHTML = '<div class="col-md-12 cen"><div class="well">No data found.</div></div>';
	}
	displayFilterMenu();
}

function setStatusEdit(id){
	var items = JSON.parse(localStorage.getItem('items')) || [];
	//console.log(items[id]);
	var index = 0;
	for(var i=0; i<items.length; i++){
		if(items[i].id==id)
			index=i;
	}
	console.log(index);

	document.getElementById('nameInput').value = items[index].itemName;
	document.getElementById('categoryInput').value = items[index].itemCategory;
	document.getElementById('categoryId').value = items[index].id;

}
function setStatusDelete(id){
	var items = JSON.parse(localStorage.getItem('items')) || [];
	//console.log(items[id]);
	var index = 0;
	for(var i=0; i<items.length; i++){
		if(items[i].id==id)
			index=i;
	}
	console.log(index);

	items.splice(index,1);
	//delete items[id];
	localStorage.setItem('items', JSON.stringify(items));
	//fetchItem();
	//index=
	console.log(filterCategoryIndex);
	myFunction(filterCategoryIndex);
}

function addNewCategory(CategoryName){
	var categoryList = JSON.parse(localStorage.getItem('categoryList')) || [];
	var index = categoryList.indexOf(CategoryName);
	//console.log(index);
	//console.log(categoryList);
	if (index == -1) {
		categoryList.push(CategoryName);
		localStorage.setItem('categoryList', JSON.stringify(categoryList));
	}
}

function displayFilterMenu(){
	var categoryList = JSON.parse(localStorage.getItem('categoryList')) || [];
	var menuList = document.getElementById('navItem');
	menuList.innerHTML = '<li class="active" id="'+ 0 +'"><a class="categoryFilter" id="'+ 0 +'">All</a></li>';
	for(var i=0; i<categoryList.length; i++){
		menuList.innerHTML +='<li id="'+ (i+1) +'"><a class="categoryFilter" id="'+ (i+1) +'">'+ categoryList[i] +'</a></li>';
	}
}
function myFunction(id){
	console.log('Clicked Item=>'+id);
	filterCategoryIndex=id;
	if(id==0)
		fetchItem();
	else
		fetchItemByCategory(id);

	var x = document.getElementById("navItem");
	var list = x.getElementsByTagName("li");
	for(var i=0; i<list.length; i++)
	{	
		list[i].className = ""; 
	}
	document.getElementById(id).className = "active"; 
}

function fetchItemByCategory(id){
	var allItems = JSON.parse(localStorage.getItem('items')) || [];
	var itemList = document.getElementById('itemList');
	var categoryList = JSON.parse(localStorage.getItem('categoryList')) || [];
	var items=allItems.filter(function(item) {
  		return (item.itemCategory == categoryList[(id-1)]);
  	});
	/*for(var i=0; i<allItems.length; i++)
	{
		if(allItems[i].itemCategory==categoryList[(id-1)])
			items.push(allItems[i]);
	}*/
	itemList.innerHTML = '';
	if(items.length>0)
	{
		for (var i = 0; i <items.length; i++) {
			var id = items[i].id;
			var name=items[i].itemName;
			var category=items[i].itemCategory;
			//console.log(id);
			itemList.innerHTML += 
				'<div class="col-md-4"><div class="well">'+
					'<h6> Item ID : '+id+'</h6>'+
					'<p> Name : '+name+'</p>'+
					'<p> Category : '+category+'</p>'+
					'<p><a href="#" id="'+ id +'" class="btn btn-warning btnEditItem"> Edit </a>'+" "+
					'<a href="#" id="'+ id +'" class="btn btn-danger btnDeleteItem"> Delete </a></p>'
				+'</div></div>';
		}
	}else{
		itemList.innerHTML = '<div class="col-md-12 cen"><div class="well">No data found.</div></div>';
	}
	displayFilterMenu();
}