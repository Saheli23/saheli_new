$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
    items : 1,
    loop: true,
    center: true,
    dots: false,
    nav: true,
    navText: ["<img src='images/prev-image.png'>","<img src='images/next-image.png'>"],
    autoplay: false,
    autoplayTimeout: 2000
  });
});
