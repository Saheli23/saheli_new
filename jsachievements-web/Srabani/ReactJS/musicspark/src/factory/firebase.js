import firebase from 'firebase'

// Initialize Firebase
var config = {
    apiKey: "AIzaSyBo_FMENMMXwY58Aa5Xh_VS8TK3ZZZW9YQ",
    authDomain: "music-spark.firebaseapp.com",
    databaseURL: "https://music-spark.firebaseio.com",
    projectId: "music-spark",
    storageBucket: "music-spark.appspot.com",
    messagingSenderId: "548855882401"
};
firebase.initializeApp(config);
export default firebase;
