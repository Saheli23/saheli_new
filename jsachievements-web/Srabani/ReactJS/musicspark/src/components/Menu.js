import React, { Component } from 'react';
import { Link, Route , withRouter } from 'react-router-dom';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import '../styles/Menu.css';


class Menu extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoggedIn: false
        };
    }

    componentWillMount(){
        // if session not set redirect to register page
        if (typeof sessionStorage.getItem('userInfo') !== 'undefined' && sessionStorage.getItem('userInfo') !== null) {
            this.setState({ isLoggedIn: true });
        }
    }

    handleLogoutClick(event){
        if (typeof sessionStorage.getItem('userInfo') !== 'undefined') {
            sessionStorage.removeItem('userInfo');
            this.props.history.push("/login");
        }
    }

    render(){
        const isLoggedIn = this.state.isLoggedIn;
        const button = isLoggedIn ? (
            <NavItem eventKey={2} href="javascript:void(0);" onClick={(event) => this.handleLogoutClick(event)}> Logout </NavItem>
        ) : (
            <NavItem eventKey={2} href="/login"> Login </NavItem>
        );
        return (
            <Navbar collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#brand"></a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <NavItem eventKey={1} href="/">
                            Home
                        </NavItem>
                        <NavItem eventKey={2} href="/favourites">
                            My Favourites
                        </NavItem>
                        <NavItem eventKey={2} href="/downloads">
                            My Downloads
                        </NavItem>
                    </Nav>
                    <Nav pullRight>
                        <NavDropdown eventKey={3} title="Settings" id="basic-nav-dropdown">
                            <MenuItem eventKey={3.1} href="/changepassword">Change Password</MenuItem>
                            <MenuItem eventKey={3.2} href="/uploadimage">Upload Image</MenuItem>
                        </NavDropdown>
                        {button}
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}
export default Menu;