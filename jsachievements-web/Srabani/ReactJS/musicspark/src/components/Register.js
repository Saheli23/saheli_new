import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import '../styles/Register.css';
import { Form, Button, FormGroup, FormControl } from 'react-bootstrap';
import axios from 'axios';
import { withSwalInstance } from 'sweetalert2-react';
import swal from 'sweetalert2';
 
const SweetAlert = withSwalInstance(swal);

class Register extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
            fullname: '',
            email: '',
            password: '',
            cpassword: '',
            show: false,
            isEmail: true,
            emailRequired: true,
            passwordRequired: true,
            fullnameRequired: true,
            cpasswordRequired: true
	    };
  	}   

    componentWillMount(){
        // if session set redirect to home page
        console.log(typeof sessionStorage.getItem('userInfo'))
        if (typeof sessionStorage.getItem('userInfo') !== 'undefined' && sessionStorage.getItem('userInfo')!== null) {
            this.props.history.push("/");
        }
    }

    onHandleInputChange(e) {
        var state = this.state;
        state[e.target.name] = e.target.value;
        this.setState(state);
        this.validate(e.target.name, $.trim(e.target.value));  
    }

    validate(name, value){
        switch(name){
            case 'fullname':
                // check fullname field length
                const fullname = value.length !== 0 ? true : false;             
                this.setState({ fullnameRequired: fullname });
                break;

            case 'email':
                // first check email field length
                if(value.length !== 0){
                    this.setState({ emailRequired: true });

                    // now check if it is valid email
                    var regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
                        reg = new RegExp(regex),
                        isEmail = reg.test(value);
                    
                    this.setState({ isEmail: isEmail });
                                    
                }else{
                    this.setState({ emailRequired: false });
                }                
                break;

            case 'password':
                // check password field length
                const password = value.length !== 0 ? true : false;             
                this.setState({ passwordRequired: password });
                break;

            case 'cpassword':
                // check password field length
                const cpassword = value.length !== 0 ? true : false;             
                this.setState({ cpasswordRequired: cpassword });
                break;
        }
    }

    validateAll(callback){
        this.validate( 'fullname', $.trim( $('input[name=fullname]').val() ) );
        this.validate( 'email', $.trim( $('input[name=email]').val() ) );
        this.validate( 'password', $.trim( $('input[name=password]').val() ) );
        this.validate( 'cpassword', $.trim( $('input[name=cpassword]').val() ) );
        callback();
    }
    
    onHandleRegisterClick(event) {
        this.validateAll(() => {
            if(this.state.isEmail && this.state.emailRequired && this.state.passwordRequired){  
                axios.post('http://dev92.developer24x7.com/jsachievements-web/Srabani/PHP/music-api/register.php', {
                    fullName: this.state.fullname,
                    email: this.state.email,
                    password: this.state.password
                })
                .then((response) => {
                    console.log(response);
                    if(response.data.status === 200) {
                        // Save data to sessionStorage
                        sessionStorage.setItem('userInfo', this.state.email);
                        this.setState({ show: true });
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
            }
        });
    }

    sweetAlertConfirm(event){
        this.setState({ show: false });
        this.props.history.push("/");
    }

    sweetAlertCancel(event){
        this.setState({ show: false });
    }

	render(){
        const isEmail = this.state.isEmail,
              emailRequired = this.state.emailRequired,
              fullnameRequired = this.state.fullnameRequired,
              passwordRequired = this.state.passwordRequired,
              cpasswordRequired = this.state.cpasswordRequired;              


        const emailError = emailRequired ? (
            isEmail ? (
                <p className="input-perfect">Perfect</p>
            ) : (
                <p className="input-error">Invalid Email</p>
            )
        ) : (
            <p className="input-error">Email Required</p>
        );

        const fullnameError = fullnameRequired ? (
            <p className="input-perfect">Perfect</p>
        ) : (
            <p className="input-error">Name Required</p>
        );

        const passwordError = passwordRequired ? (
            <p className="input-perfect">Perfect</p>
        ) : (
            <p className="input-error">Password Required</p>
        );

        const cpasswordError = cpasswordRequired ? (
            <p className="input-perfect">Perfect</p>
        ) : (
            <p className="input-error">Confirm Password Required</p>
        );

		return (
            <div className="container-fluid">
                <div className="grid col-sm-12 col-md-12 col-xs-12">
                    <div className="main-register-wrapper">
                        <div className="register-form-wrapper">
                            <Form>
                                <div className="row">            
                                    <FormGroup controlId="fullname">
                                         <FormControl type="text" name="fullname" value={this.state.fullname} placeholder="Full Name" onChange={(event) => this.onHandleInputChange(event)}/>
                                         {fullnameError}    
                                    </FormGroup>
                                </div>
                                <div className="row">
                                    <FormGroup controlId="email">
                                        <FormControl type="email" name="email" value={this.state.email} placeholder="Email" onChange={(event) => this.onHandleInputChange(event)}/>
                                        {emailError}    
                                    </FormGroup>
                                </div>
                                <div className="row">
                                    <div className="col-sm-6 col-md-6 col-xs-12 password">
                                        <FormGroup controlId="password">
                                            <FormControl type="password" name="password" value={this.state.password} placeholder="Password" onChange={(event) => this.onHandleInputChange(event)}/>
                                            {passwordError}    
                                        </FormGroup>
                                    </div>
                                    <div className="col-sm-6 col-md-6 col-xs-12 confirm-password">
                                        <FormGroup controlId="cpassword">
                                            <FormControl type="password" name="cpassword" value={this.state.cpassword} placeholder="Confirm Password" onChange={(event) => this.onHandleInputChange(event)}/>
                                            {cpasswordError}    
                                        </FormGroup>
                                    </div>
                                </div>
                                <div className="row">
                                    <Button className="register-btn" onClick = {(event) => this.onHandleRegisterClick(event)}>Sign In</Button>
                                </div>
                                <div className="row">
                                    <div className="col-sm-6 col-md-6 col-xs-6 forgot-password">
                                        <Link to="/forgotpassword">Forgot Password?</Link>
                                    </div>
                                        
                                    <div className="col-sm-6 col-md-6 col-xs-6 login-link">
                                        <Link to="/login">Already Registered?</Link>
                                    </div>
                                </div>
                            </Form>
                        </div>
                        <SweetAlert show={this.state.show} title="Thank You" text="Successfully Registred"             
                            onConfirm={(event) => this.sweetAlertConfirm(event)}
                            onCancel={(event) => this.sweetAlertCancel(event)}
                            onEscapeKey={(event) => this.sweetAlertCancel(event)}
                        />
                    </div>
                </div>                
            </div>

	    );
	}
}
export default Register;