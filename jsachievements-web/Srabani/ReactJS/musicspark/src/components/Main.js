import React, { Component } from 'react';
import Slider from 'react-slick';
import axios from 'axios';
import $ from 'jquery';
import AudioPlayer from 'react-cl-audio-player';
import '../styles/Main.css';

// Just Take a look on these two links:
// https://www.npmjs.com/package/react-player
// https://www.npmjs.com/package/react-player-controls

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toptracks: [],
            topartists: [],
            topalbums: [],
            playlist: []
        }
    }

    componentWillMount(){  
        // React Media Player
        // http://voidcanvas.com/8-best-react-audio-video-player-plugins-with-demo/
        // 1. React Media Player
        // 2. React Audio Player
        // 3. React Responsive Audio Player
        this.setState({
            playlist: [{
                url: 'http://res.cloudinary.com/alick/video/upload/v1502689683/Luis_Fonsi_-_Despacito_ft._Daddy_Yankee_uyvqw9.mp3',
                cover: 'http://res.cloudinary.com/alick/image/upload/v1502689731/Despacito_uvolhp.jpg',
                artist: {
                    name: 'Luis Fonsi, Daddy Yankee',
                    song: 'Despacito'
                }
            }, {
                url: 'https://downpwnew.com/12997/Dil Chori - Yo Yo Honey Singh 320Kbps.mp3',
                cover: 'https://pagalworld.info/GpE34Kg9Gq/12997/thumb-dil-chori-sada-ho-gaya-yo-yo-honey-singh-mp3-song-300.jpg',
                artist: {
                    name: 'Yo Yo Honey Singh',
                    song: 'Dil Chori'
                }
            }]
        });
    }

    componentDidMount(){
        
        axios.get('http://dev92.developer24x7.com/jsachievements-web/Srabani/PHP/music-api/topSongs.php')
             .then(
                (response) => { 
                    this.setState({toptracks: response.data.data});
                    // console.log(response)
                    this.setDimention();
                }                
        );

        axios.get('http://dev92.developer24x7.com/jsachievements-web/Srabani/PHP/music-api/topAlbum.php')
             .then(
                (response) => { 
                    this.setState({topalbums: response.data.data});
                    this.setDimention();
                }                
        );

        axios.get('http://dev92.developer24x7.com/jsachievements-web/Srabani/PHP/music-api/topArtist.php')
             .then(
                (response) => { 
                    this.setState({topartists: response.data.data});
                    this.setDimention();
                }                
        );  

        window.addEventListener("resize", this.setDimention);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.setDimention);
    }

    setDimention(){        
        $('.list').css({
            'height': $('.list').width() + 'px',
            'max-height': '190px'
        });
    }    

    onHandleTrackChange(event, item){
        this.setState({
            playlist: [{
                url: item.trackUrl,
                cover: item.albumImage,
                artist: {
                    name: item.artistName,
                    song: item.trackName
                }
            }]
        });
    }

    render(){
        var settings = {
            infinite: false,
            slidesToShow: 6,
            slidesToScroll: 1
        };
        
        return (
            <div className="container-fluid">
                <div className="main-wrapper">
                    <div className="slick-wrapper">
                        <div className="row">
                            <h4 className="song-heading">Trending Songs</h4>
                        </div>
                        <div className="row trending-tracks">
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <Slider {...settings}>
                                    {
                                        this.state.toptracks.map((item, index) => {
                                            if(index < 9){
                                                return (
                                                    <div className="list" key={index}>
                                                        <div className="toptracks"  onClick={(event) => this.onHandleTrackChange(event, item)}>
                                                            <a href='javascript:void(0);'><img src={item.albumImage}/></a>
                                                        </div>     
                                                        <div className="music-playbutton">
                                                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" height="40" width="40" viewBox="0 0 41.999 41.999">
                                                                <path d="M36.068,20.176l-29-20C6.761-0.035,6.363-0.057,6.035,0.114C5.706,0.287,5.5,0.627,5.5,0.999v40  c0,0.372,0.206,0.713,0.535,0.886c0.146,0.076,0.306,0.114,0.465,0.114c0.199,0,0.397-0.06,0.568-0.177l29-20  c0.271-0.187,0.432-0.494,0.432-0.823S36.338,20.363,36.068,20.176z"></path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                );
                                            }
                                        })
                                    }
                                </Slider>
                            </div>
                        </div>
                    </div>


                    <div className="slick-wrapper">
                        <div className="row">
                            <h4 className="song-heading">Top Albums</h4>
                        </div>
                        <div className="row trending-artists">
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <Slider {...settings}>
                                    {
                                        this.state.topalbums.map((item, index) => {
                                            if(index < 9){
                                                return (
                                                    <div className="list" key={index}>
                                                        <div className="topalbums">
                                                            <a href='javascript:void(0);'><img src={item.albumImage}/></a>
                                                        </div>     
                                                        <div className="music-playbutton">
                                                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" height="40" width="40" viewBox="0 0 41.999 41.999">
                                                                <path d="M36.068,20.176l-29-20C6.761-0.035,6.363-0.057,6.035,0.114C5.706,0.287,5.5,0.627,5.5,0.999v40  c0,0.372,0.206,0.713,0.535,0.886c0.146,0.076,0.306,0.114,0.465,0.114c0.199,0,0.397-0.06,0.568-0.177l29-20  c0.271-0.187,0.432-0.494,0.432-0.823S36.338,20.363,36.068,20.176z"></path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                );
                                            }
                                        })
                                    }
                                </Slider>
                            </div>
                        </div>
                    </div>


                    <div className="slick-wrapper">
                        <div className="row">
                            <h4 className="song-heading">Top Artists</h4>
                        </div>
                        <div className="row trending-artists">
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <Slider {...settings}>
                                    {
                                        this.state.topartists.map((item, index) => {
                                            if(index < 9){
                                                return (
                                                    <div className="list" key={index}>
                                                        <div className="topartists">
                                                            <a href='javascript:void(0);'><img src={item.artistImage}/></a>
                                                        </div>     
                                                        <div className="music-playbutton">
                                                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" height="40" width="40" viewBox="0 0 41.999 41.999">
                                                                <path d="M36.068,20.176l-29-20C6.761-0.035,6.363-0.057,6.035,0.114C5.706,0.287,5.5,0.627,5.5,0.999v40  c0,0.372,0.206,0.713,0.535,0.886c0.146,0.076,0.306,0.114,0.465,0.114c0.199,0,0.397-0.06,0.568-0.177l29-20  c0.271-0.187,0.432-0.494,0.432-0.823S36.338,20.363,36.068,20.176z"></path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                );
                                            }
                                        })
                                    }
                                </Slider>
                            </div>
                        </div>
                    </div>


                    <div className="slick-wrapper">
                        <div className="row">
                            <h4 className="song-heading">Editor's Choice</h4>
                        </div>
                        <div className="row editors-chioce">
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <Slider {...settings}>
                                    {
                                        this.state.topartists.map((item, index) => {
                                            if(index < 9){
                                                return (
                                                    <div className="list" key={index}>
                                                        <div className="editorlist">
                                                            <a href='javascript:void(0);'><img src={item.albumImage}/></a>
                                                        </div>     
                                                        <div className="music-playbutton">
                                                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" height="40" width="40" viewBox="0 0 41.999 41.999">
                                                                <path d="M36.068,20.176l-29-20C6.761-0.035,6.363-0.057,6.035,0.114C5.706,0.287,5.5,0.627,5.5,0.999v40  c0,0.372,0.206,0.713,0.535,0.886c0.146,0.076,0.306,0.114,0.465,0.114c0.199,0,0.397-0.06,0.568-0.177l29-20  c0.271-0.187,0.432-0.494,0.432-0.823S36.338,20.363,36.068,20.176z"></path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                );
                                            }
                                        })
                                    }
                                </Slider>
                            </div>
                        </div>
                    </div>                    

                    <div className="music-player">
                        <AudioPlayer songs={this.state.playlist} />
                    </div>
                </div>
            </div>
        );
    }
}
export default Main;