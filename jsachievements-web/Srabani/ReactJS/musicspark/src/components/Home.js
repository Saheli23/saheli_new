import React, { Component } from 'react';
import Menu from './Menu';
import Main from './Main';

class Home extends Component {    
    constructor(props) {
        super(props);
    }

    componentWillMount(){
        // if session not set redirect to register page
        // if (typeof sessionStorage.getItem('userInfo') === 'undefined') {
        //     this.props.history.push("/register");
        // }
    }

    render(){
        return (
            <div className="container-fluid"> 
                <div className="Header">
                    <Menu history={this.props.history}/>
                </div>		    
                           
                <div className="Main">
                    <Main />
                </div>
            </div>
        );
    }
}
export default Home;