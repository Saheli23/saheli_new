import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import $ from 'jquery';
import '../styles/Register.css';
import { Form, Button, FormGroup, FormControl } from 'react-bootstrap';
import axios from 'axios';
import { withSwalInstance } from 'sweetalert2-react';
import swal from 'sweetalert2';

const SweetAlert = withSwalInstance(swal);

class Login extends Component{
	constructor(props){
		super(props);
		this.state = {
			show: false,
			email: '',
			password: '',
			isEmail: true,
			emailRequired: true,
			passwordRequired: true
		}

        // Login through Facebook
        // 1. https://stormpath.com/blog/react-facebook-login
        // 2. https://github.com/stormpath/stormpath-express-react-example
        // 3. https://medium.com/front-end-hacking/facebook-authorization-in-a-react-app-b7a9176aacb6
        // 4. http://jslancer.com/blog/2017/11/27/facebook-google-login-react/
        // 5. https://www.npmjs.com/package/react-facebook-auth
        // 6. https://stackoverflow.com/questions/27717555/implement-facebook-api-login-with-reactjs
	}
	
	onHandleInputChange(e) {
        var state = this.state;
        state[e.target.name] = e.target.value;
        this.setState(state);
        this.validate(e.target.name, $.trim(e.target.value));        
    }

    validate(name, value){
    	switch(name){
        	case 'email':
                // first check email field length
        		if(value.length !== 0){
        			this.setState({ emailRequired: true });

        			// now check if it is valid email
        			var regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
	        			reg = new RegExp(regex),
	        			isEmail = reg.test(value);
			        
		            this.setState({ isEmail: isEmail });
			               			
        		}else{
        			this.setState({ emailRequired: false });
        		}

                
        		break;

        	case 'password':
                // check password field length
        		const password = value.length !== 0 ? true : false;        		
    			this.setState({ passwordRequired: password });
        		break;
        }
    }

    validateAll(callback){
        this.validate( 'email', $.trim( $('input[name=email]').val() ) );
        this.validate( 'password', $.trim( $('input[name=password]').val() ) );
        callback();
    }

	onHandleLoginClick(event){
        this.validateAll(() => {
    		if(this.state.isEmail && this.state.emailRequired && this.state.passwordRequired){		
    			axios.post('http://dev92.developer24x7.com/jsachievements-web/Srabani/PHP/music-api/login.php', {
    	            email: this.state.email,
    	            password: this.state.password
    	        })
    	        .then((response) => {
    	            console.log(response);
    	            if(response.data.status === 200) {            	
    	                // Save data to sessionStorage
    	                sessionStorage.setItem('userInfo', this.state.email);
    	        		this.props.history.push("/");
    	            }else if(response.data.status === 404){
    	                this.setState({ show: true });
    	            }
    	        })
    	        .catch(function (error) {
    	            console.log(error);
    	        });
    	    }else{
    	    	console.log('there are some error(s)');
    	    }
            
        }); 
	}

	sweetAlertConfirm(event){
        this.setState({ show: false });
    }

    sweetAlertCancel(event){
        this.setState({ show: false });
    }

	render () {
        const isEmail = this.state.isEmail,
              emailRequired = this.state.emailRequired,
              passwordRequired = this.state.passwordRequired;

        const emailError = emailRequired ? (
            isEmail ? (
                <p className="input-perfect">Perfect</p>
            ) : (
                <p className="input-error">Invalid Email</p>
            )
        ) : (
            <p className="input-error">Email Required</p>
        );

        const passwordError = passwordRequired ? (
            <p className="input-perfect">Perfect</p>
        ) : (
            <p className="input-error">Password Required</p>
        );

		return (
			<div className="container-fluid">
                <div className="grid col-sm-12 col-md-12 col-xs-12">
                    <div className="main-register-wrapper">
                        <div className="register-form-wrapper">
                            <Form>
                                <div className="row">
                                    <FormGroup controlId="email">
                                        <FormControl type="email" name="email" value={this.state.email} placeholder="Email" onChange={(event) => this.onHandleInputChange(event)}/>                                        
                                        {emailError}                                      
                                    </FormGroup>
                                </div>
                                <div className="row">
                                    <FormGroup controlId="password">
                                        <FormControl type="password" name="password" value={this.state.password} placeholder="Password" onChange={(event) => this.onHandleInputChange(event)}/>                                        
                                        {passwordError}
                                    </FormGroup>
                                </div>
                                <div className="row">
                                    <Button className="register-btn" onClick = {(event) => this.onHandleLoginClick(event)}>Log In</Button>
                                </div>
                                <div className="row">
                                    <div className="col-sm-6 col-md-6 col-xs-6 forgot-password">
                                        <Link to="/forgotpassword">Forgot Password?</Link>
                                    </div>
                                        
                                    <div className="col-sm-6 col-md-6 col-xs-6 login-link">
                                        <Link to="/register">New Visitor?</Link>
                                    </div>
                                </div>
                            </Form>
                        </div>
                        <SweetAlert show={this.state.show} title="SORRY" text="USER NOT FOUND"             
                            onConfirm={(event) => this.sweetAlertConfirm(event)}
                            onCancel={(event) => this.sweetAlertCancel(event)}
                            onEscapeKey={(event) => this.sweetAlertCancel(event)}
                        />
                    </div>
                </div>                
            </div>
		);
	}
}
export default Login;