import Vue from 'vue';
import Router from 'vue-router';
import Hello from '@/components/Hello';
import Home from '@/components/Home';
import Product from '@/components/Product';
import Login from '@/components/Login';
import Wishlist from '@/components/Wishlist';
import Cart from '@/components/Cart';



Vue.use(Router);

export default new Router({
    routes: [{
        path: '/',
        name: 'Home',
        component: Home,
    }, {
        path: '/product/:id',
        name: 'Product',
        component: Product,
    }, {
        path: '/login',
        name: 'Login',
        component: Login,
    }, {
        path: '/wishlist',
        name: 'Wishlist',
        component: Wishlist,
    }, {
        path: '/cart',
        name: 'Cart',
        component: Cart,
    }],
    //    mode: 'history',
});
