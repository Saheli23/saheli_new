// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import App from './App';
import router from './router';
import $ from "jquery";
//import VueLocalStorage from 'vue-localstorage';
import VueSession from 'vue-session';
import '../node_modules/slick-carousel/slick/slick.css';
import 'vue-msgbox/lib/vue-msgbox.css';
 

Vue.config.productionTip = true;

Vue.use(VueAxios, axios);
//Vue.use(VueLocalStorage);
Vue.use(VueSession);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
});
