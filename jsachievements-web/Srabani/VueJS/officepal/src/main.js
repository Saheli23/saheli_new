// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VeeValidate from 'vee-validate'
import VueSession from 'vue-session'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSweetalert2 from 'vue-sweetalert2'

Vue.use(VueSweetalert2);
Vue.use(VueAxios, axios);
Vue.use(VeeValidate, {
  dictionary: {
    en: {
      custom: {
        email: {
          required: 'Email field is empty',
          email: 'Invalid Email Id'
        },
        name: {
          required: 'Name field is empty'
        },
        password: {
          required: 'Password field is empty'
        },
        rpassword: {
          confirmed: 'Oops!!! Password fields are different'
        },
        terms: {
          required: 'You must read our Privacy Policy and agree to Terms before Signing up'
        }
      }
    }
  }
});

Vue.use(VueSession);

Vue.config.productionTip = true

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  }
})