import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Signup from '@/components/Signup'
import Login from '@/components/Login'
import ForgotPassword from '@/components/ForgotPassword'
import PageNotFound from '@/components/PageNotFound'


Vue.use(Router)

export default new Router({
    routes: [{
        path: '/',
        name: 'Home',
        component: Home
    }, {
        path: '/signup',
        name: 'Signup',
        component: Signup
    }, {
        path: '/login',
        name: 'Login',
        component: Login
    }, {
        path: '/forgotpassword',
        name: 'ForgotPassword',
        component: ForgotPassword,
    }, { 
        path: "*", // and finally the default route, when none of the above matches:
        component: PageNotFound 
    }]
})
