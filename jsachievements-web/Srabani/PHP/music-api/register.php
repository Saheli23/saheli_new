<?php
	// Reference: 
	// 1. http://www.phpzag.com/how-to-create-simple-rest-api-in-php/
	// 2. https://www.codeofaninja.com/2017/02/create-simple-rest-api-in-php.html

	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Max-Age: 3600");
	header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
 	include_once("database.php");

 	$data = json_decode(file_get_contents("php://input"));
 	
	if(!empty($data->fullName) && !empty($data->email) && !empty($data->password)) {
		$user = register($conn, $data);
		if(empty($user)) {
			jsonResponse(400,"User registration unsuccessfull");
		} else {
			jsonResponse(200,"User registration successfull");
		}
	} else {
		jsonResponse(400,"Invalid Request",NULL);
	}	

	function jsonResponse($status, $message) {
		$response['status']		=	$status;
		$response['message']	=	$message;
		echo json_encode($response);
	}

	function register($conn, $data) {
		
		$sql = "INSERT INTO srabani_users (fullName, email, password, status, createdAt) 
				VALUES ('".$data->fullName."', '".$data->email."', '".md5($data->password)."', 1, '".date('Y-m-d h:i:s')."')";
		
		$resultset = mysqli_query($conn, $sql) or die("database error:". mysqli_error($conn));
		return $resultset;
	}
?>