<?php
	// Reference: 
	// 1. http://www.phpzag.com/how-to-create-simple-rest-api-in-php/
	// 2. https://www.codeofaninja.com/2017/02/create-simple-rest-api-in-php.html

	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Max-Age: 3600");
	header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
 	include_once("database.php");

 	$data = json_decode(file_get_contents("php://input"));
 	
	if(!empty($data->email) && !empty($data->password)) {
		$user = login($conn, $data);
		if(empty($user)) {
			jsonResponse(404,"User Not Found");
		} else {
			jsonResponse(200,"User Found");
		}
	} else {
		jsonResponse(400,"Invalid Request",NULL);
	}	

	function jsonResponse($status, $message) {
		$response['status']		=	$status;
		$response['message']	=	$message;
		echo json_encode($response);
	}

	function login($conn, $data) {
		
		$sql = "SELECT * FROM srabani_users WHERE email = '".$data->email."' AND password = '".md5($data->password)."'";
		
		$resultset = mysqli_query($conn, $sql) or die("database error:". mysqli_error($conn));
		
		// Return the number of rows in result set
		$rowcount = mysqli_num_rows($resultset);
		return $rowcount;
	}
?>