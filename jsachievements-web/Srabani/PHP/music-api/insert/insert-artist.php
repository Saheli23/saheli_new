<!DOCTYPE html>
<html>
<head>
	<title>Insert artist</title>
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" /> -->
	<style type="text/css">
		body{
			overflow-x: hidden;
			margin: 0; 
			box-sizing: border-box;
			padding: 0;
		}

		*{
			box-sizing: border-box;
		}
		
		.add-album-wrapper{
			height: 75px;
		    background-color: #03A9F4;
		    color: #f1f1f1;
		    text-align: center;
		}

		.add-album-wrapper h2{
			margin: 0;
			padding: 0;
			line-height: 70px;
		    font-size: 30px;
		    font-weight: 500;
		}

		.insert-artist-wrapper{
			width: 98%;
			margin: 0 auto;
		}

		.form-control{
			margin: 35px 0;
			overflow: hidden;
		}

		.input-control{
			width: 100%;
			height: 34px;
			border: 1px solid #03A9F4;
			font-size: 15px;
    		color: #6e7273;
    		padding-left: 10px;
    		padding-right: 10px;
		}

		.column{
			width: 47%;
			float: left;
			margin-right: 15px;
		}

		.column.plus-minus-button{
			width: 3%;
			margin-right: 0;
			cursor: pointer;

		}

		button#save-artist{
			padding: 10px 15px;
		    background: #03A9F4;
		    border: transparent;
		    color: #f1f1f1;
		    font-size: 15px;
		}
	</style>
</head>
<body>
	<div class="main-wrapper">
		<div class="add-album-wrapper"><h2>Add Album</h2></div>
		<div class="insert-artist-wrapper">
			<form name="insertArtistForm">			
				<div class="form-control">
					<input type="text" placeholder="Album Name" name="albumName" id="albumName" class="input-control" style="width: 95.5%;" />
				</div>
				<div class="form-control">
					<div class="column">
						<input type="text" placeholder="Artist Names" name="artistName" id="artistName" class="input-control" />				
					</div>
					<div class="column">
						<input type="text" placeholder="Track Name" name="trackName" id="trackName" class="input-control" >
					</div>
					<div class="column plus-minus-button">
						<!-- <i class="fas fa-plus-circle"></i> -->
						<img src="https://use.fontawesome.com/releases/v5.0.9/svgs/solid/plus-circle.svg">
					</div>
				</div>
				<div>
					<button type="button" id="save-artist">Save Album</button>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
	<script>
		// Initialize Firebase
		var config = {
			apiKey: "AIzaSyBo_FMENMMXwY58Aa5Xh_VS8TK3ZZZW9YQ",
			authDomain: "music-spark.firebaseapp.com",
			databaseURL: "https://music-spark.firebaseio.com",
			projectId: "music-spark",
			storageBucket: "music-spark.appspot.com",
			messagingSenderId: "548855882401"
		};
		firebase.initializeApp(config);
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			// $('#save-artist').on('click', function(){
			// 	var albumName = $('#albumName').val(),
			// 		artistName = $('#artistName').val();

			// 	var firebaseRef = firebase.database().ref('artist');
			// 	firebaseRef.push().set({
					
			// 	});
			// });
		});
	</script>
</body>
</html>