<?php
	// Reference: 
	// http://www.phpzag.com/how-to-create-simple-rest-api-in-php/
	// https://github.com/ThingEngineer/PHP-MySQLi-Database-Class#insert-query

	header('Access-Control-Allow-Origin: *');
	header("Content-Type: application/json");

	include_once("database.php");


	$topsongs = getTopSongs($conn);
	// print_r($topsongs);

	if(empty($topsongs)) {
		jsonResponse(404, "404 Song Not Found", NULL);
	} else {
		jsonResponse(200, "Songs Found", $topsongs);
	}
	function jsonResponse($status, $status_message, $data) {
		$response['status']		= $status;
		$response['message']	= $status_message;
		$response['data']		= $data;
		$json_response 			= json_encode($response);
		echo $json_response;
	}

	function getTopSongs($conn) {
		$sql = "SELECT srabani_tracks.* , srabani_albums.albumImage, GROUP_CONCAT(DISTINCT srabani_singers.artistName SEPARATOR ', ') as artistName
				FROM srabani_tracks, srabani_albums, srabani_singers, srabani_track_album_singer
				WHERE srabani_tracks.id = srabani_track_album_singer.trackId
				AND srabani_albums.id = srabani_track_album_singer.albumId
				AND srabani_singers.id = srabani_track_album_singer.artistId
				GROUP BY srabani_tracks.id
				ORDER BY srabani_tracks.trendings ASC LIMIT 8";

		$resultset = mysqli_query($conn, $sql) or die("database error:". mysqli_error($conn));
		$data = array();

		while( $rows = mysqli_fetch_assoc($resultset) ) {
			$data[] = $rows;
		}
		return $data;
	}
?>