<?php
	// Reference: http://www.phpzag.com/how-to-create-simple-rest-api-in-php/
	header('Access-Control-Allow-Origin: *');
	header("Content-Type:application/json");

	include_once("database.php");

	$topartists = getTopArtists($conn);
	if(empty($topartists)) {
		jsonResponse(404,"Artist Not Found",NULL);
	} else {
		jsonResponse(200,"Artists Found",$topartists);
	}
	function jsonResponse($status,$status_message,$data) {
		$response['status']=$status;
		$response['message']=$status_message;
		$response['data']=$data;
		$json_response = json_encode($response);
		echo $json_response;
	}

	function getTopArtists($conn) {
		$sql = "SELECT srabani_singers.* FROM srabani_singers ORDER BY srabani_singers.trendings ASC LIMIT 8";
		$resultset = mysqli_query($conn, $sql) or die("database error:". mysqli_error($conn));
		$data = array();
		while( $rows = mysqli_fetch_assoc($resultset) ) {
			$data[] = $rows;
		}
		return $data;
	}
?>