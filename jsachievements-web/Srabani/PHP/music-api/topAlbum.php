<?php
	// Reference: http://www.phpzag.com/how-to-create-simple-rest-api-in-php/
	header('Access-Control-Allow-Origin: *');
	header("Content-Type:application/json");

	include_once("database.php");

	$topalbums = getTopAlbums($conn);
	if(empty($topalbums)) {
		jsonResponse(404,"Album Not Found",NULL);
	} else {
		jsonResponse(200,"Albums Found",$topalbums);
	}
	function jsonResponse($status, $status_message, $data) {
		$response['status']		=	$status;
		$response['message']	=	$status_message;
		$response['data']		=	$data;
		$json_response 			= 	json_encode($response);
		echo $json_response;
	}

	function getTopAlbums($conn) {
		$sql = "SELECT srabani_albums.* FROM srabani_albums ORDER BY srabani_albums.trendings ASC LIMIT 8";
		$resultset = mysqli_query($conn, $sql) or die("database error:". mysqli_error($conn));
		$data = array();
		while( $rows = mysqli_fetch_assoc($resultset) ) {
			$data[] = $rows;
		}
		return $data;
	}
?>