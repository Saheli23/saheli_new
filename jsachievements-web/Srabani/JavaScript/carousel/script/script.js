$(document).ready(function () {
	let transform_from = -200;
	const sliderWidth = $('li').width(),
    sliderLength = $('li').length;	

	$('.carousel').css({
        'width': sliderWidth * sliderLength + 'px',
		'-moz-transform': 'translateX(' + transform_from + 'px)',
        '-webkit-transform': 'translateX(' + transform_from + 'px)',
        'transform': 'translateX(' + transform_from + 'px)'
    });

    $('.gallery').css({
        'width': sliderWidth * 5 + 'px'        
    });

	$('.previous-arrow').on('click', function() {
        const transform = {
            transformX: transform_from
        };

        $(transform).stop().animate({
            transformX: transform_from - sliderWidth
        }, {
            duration: 500,            
            step: function (now, fx) {
                $('.carousel').css({
                    '-moz-transform': 'translateX(' + now + 'px)',
                    '-webkit-transform': 'translateX(' + now + 'px)',
                    'transform': 'translateX(' + now + 'px)'
                });
            },
            complete: function() {                
                const item = $('.carousel').find('li').eq(0);
                $('.carousel').find('li').eq(0).remove();
                $('.carousel').append(item);

                $('.carousel').css({
                    '-moz-transform': 'translateX(' + transform_from + 'px)',
                    '-webkit-transform': 'translateX(' + transform_from + 'px)',
                    'transform': 'translateX(' + transform_from + 'px)'
                });
            }
        });  
	});

	$('.next-arrow').on('click', function() {
		const transform = {
            transformX: transform_from
        };

        $(transform).stop().animate({
            transformX: transform_from + sliderWidth
        }, {
            duration: 500,
            step: function (now, fx) {
                $('.carousel').css({
                    '-moz-transform': 'translateX(' + now + 'px)',
                    '-webkit-transform': 'translateX(' + now + 'px)',
                    'transform': 'translateX(' + now + 'px)'
                });
            },
            complete: function() {                
                const item = $('.carousel').find('li').eq( sliderLength - 1 );
                $('.carousel').find('li').eq( sliderLength - 1 ).remove();
                $('.carousel').prepend(item);

                $('.carousel').css({
                    '-moz-transform': 'translateX(' + transform_from + 'px)',
                    '-webkit-transform': 'translateX(' + transform_from + 'px)',
                    'transform': 'translateX(' + transform_from + 'px)'
                });
            }
        });
	});

	$('ul.carousel li').hover( function() {
		const self = $(this);
		self.css('z-index', 2);

		const scaling = {
            scale: '1'
        };

        $(scaling).stop().animate({
            scale: '1.2'
        }, {
            duration: 500,
            start: function () {
            	self.css('z-index', 2);
            },
            step: function (now, fx) {
                self.find('img').css({
                    '-moz-transform': 'scale(' + now + ')',
                    '-webkit-transform': 'scale(' + now + ')',
                    'transform': 'scale(' + now + ')'
                });
            }
        });
	}, function() {
		const self = $(this);	

		const scaling = {
            scale: '1.2'
        };

        $(scaling).stop().animate({
            scale: '1'
        }, {
            duration: 500,
            step: function (now, fx) {
                self.find('img').css({
                    '-moz-transform': 'scale(' + now + ')',
                    '-webkit-transform': 'scale(' + now + ')',
                    'transform': 'scale(' + now + ')'

                });
            },
            complete: function(){
				self.css('z-index', 1);
            }
        });
	});
})