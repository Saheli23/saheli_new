$(document).ready(() => {
	let pallete = ['#087b71', '#e92fef', '#6546e2', '#eaea13', '#55e0df', '#da6261', '#63ea80', '#ff9800'];
	let altPallete = [];
	let index;
	function colorblink(){
		$.each($('.jewellery-block'), (key, element) => {
			// calculate random index
			index = Math.floor(Math.random() * pallete.length);		

			// set the color to color block
			$(element).css({
				'background-color': pallete[index]
			});

			// push the color code into an alternate storage
			altPallete.push(pallete[index]);

			// removes the color code from the array not to be selected for later block
			pallete.splice(index, 1);
		});

		// get back the colors pallete
		pallete = altPallete;
		altPallete = [];
	}

	setInterval( () => {
		colorblink();
	}, 500);

	colorblink();
})