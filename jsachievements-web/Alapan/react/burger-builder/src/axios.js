import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://my-burger-builder-acf7a.firebaseio.com/'
});

export default instance;