import React from 'react';

import classes from './BuildControls.css';
import 'font-awesome/css/font-awesome.min.css';
import BuildControl from './BuildControl/BuildControl';

const controls = [
    {label: 'Bacon', type:'bacon'},
    {label: 'Cheese', type:'cheese'},
    {label: 'Meat', type:'meat'},
    {label: 'Salad', type:'salad'},
];

const buildControls = (props) => {
    return (
        <div className={classes.BuildControls}>
            <p>Current Price : <i className="fa fa-inr">INR</i><strong> {props.price.toFixed(2)}</strong></p>
            {
                //Loop throughs controls element and create our build controls
                controls.map( ctrl => {
                    return <BuildControl 
                                key={ctrl.label} 
                                label={ctrl.label} 
                                added={props.ingredientAdded.bind(this, ctrl.type)}
                                removed={props.ingredientRemoved.bind(this, ctrl.type)}
                                disabled={props.disabled[ctrl.type]} />;
                })
            }
            <button 
                className={classes.OrderButton}
                disabled={!props.purchasable}
                onClick={props.ordered}>ORDER NOW</button>
        </div>
    );
};

export default buildControls;