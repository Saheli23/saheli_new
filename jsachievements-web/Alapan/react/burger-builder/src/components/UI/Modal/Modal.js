import React, { Component } from 'react';

import classes from './Modal.css';
import Aux from '../../../hoc/Auxiliary/Aux';
import Backdrop from '../Backdrop/Backdrop';

class Modal extends Component {

    shouldComponentUpdate ( nextProps, nextState ) {
        return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    }

    componentWillUpdate () {
        console.log('[Modal] willUpdate');
    }

    render () {
        const assignedClasses = [];

        //set the Modal default class
        assignedClasses.push( classes.Modal );

        //show or hide the modal conditionally
        if(this.props.show){
            assignedClasses.push( classes.ShowModal );
        }else{
            assignedClasses.push( classes.HideModal );
        }

        return (
            <Aux>
                <Backdrop show={this.props.show} clicked={this.props.modalClosed} />
                <div className={assignedClasses.join(' ')}>
                    { this.props.children }
                </div>
            </Aux>    
        );
    }
}

export default Modal;