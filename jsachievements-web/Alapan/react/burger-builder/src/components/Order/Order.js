import React from 'react';

import classes from './Order.css';

const order = ( props ) => {

    const ingredients = Object.keys( props.ingredients ).map( igKey => {
                            return {
                                name: igKey,
                                amount: +props.ingredients[ igKey ]      
                            }
                        } );
    
    const ingredientOutput = ingredients.filter( ig => ig.amount !== 0).map( ig => {
        return <span 
                    style={{
                        textTransform: 'capitalize',
                        display: 'inline-block',
                        margin: '0 8px',
                        border: '1px solid #ccc',
                        padding: '5px'
                    }}
                    key={ig.name}>{ig.name} ({ig.amount})</span>;
    } );

    const orderedAt = new Date( props.orderedAt ).toString();

    return (
        <div className={classes.Order}>
            <p>Ingredients: { ingredientOutput }</p>
            <p>Price: <strong>INR {props.price}</strong></p>
            <p>Ordered At: <strong>{orderedAt}</strong></p>
        </div>
    );
}

export default order;