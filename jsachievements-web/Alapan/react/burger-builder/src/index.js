import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import firebase from 'firebase';

 // Initialize Firebase
 var config = {
    apiKey: "AIzaSyCIxmyFf2_vgtojywQXfFAz_1k6Ey_WP1s",
    authDomain: "my-burger-builder-acf7a.firebaseapp.com",
    databaseURL: "https://my-burger-builder-acf7a.firebaseio.com",
    projectId: "my-burger-builder-acf7a",
    storageBucket: "my-burger-builder-acf7a.appspot.com",
    messagingSenderId: "903187047234"
  };
firebase.initializeApp(config);

//Use BrowserRouter to enable routing throughout our entire application
const app = (
  <BrowserRouter 
      basename="/jsachievements-web/Alapan/react/burger-builder/build/#/">
    <App />
  </BrowserRouter>
);

ReactDOM.render( app, document.getElementById('root') );
registerServiceWorker();
