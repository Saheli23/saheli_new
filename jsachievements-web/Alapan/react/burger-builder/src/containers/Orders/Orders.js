import React, { Component } from 'react';

import Order from '../../components/Order/Order';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import Spinner from '../../components/UI/Spinner/Spinner';
import axios from '../../axios';

class Orders extends Component{

    constructor( props ){
        super( props );
        this.state = {
           orders: [],
           loading: true,
           error: false     
        };
    }

    componentDidMount() {
        //fetch orders from firebase
        axios.get('/orders.json')
             .then( res => {
                 //We get orders data as objects from firebase and convert it to array
                const fetchedOrders = [];
                const ordersObject  = res.data;
                for( let key in ordersObject ){
                    fetchedOrders.push( {
                        ...ordersObject[ key ],
                        id: key 
                    } );
                }
                //Reverse array to sort in reverse chronological order
                fetchedOrders.reverse();
                //Set our state here
                this.setState({
                    loading: false,
                    orders: fetchedOrders
                });  
             }).catch( error => {
                this.setState( { loading: false, error: true } );
             })

    }

    render() {
        let orders = null;

        //Initially show a loader
        if( this.state.loading ){
            orders = <Spinner />
        }

        //No Orders Currently
        if( !this.state.loading && !this.state.error && this.state.orders.length === 0 ){
            orders = (
                <p style={ {
                    'fontWeight':'bold',
                    'textAlign':'center',
                    'padding': '20px 0'
                } }>No Orders to Show</p>
            );
        }else if( !this.state.loading && !this.state.error && this.state.orders.length > 0 ){
            orders = this.state.orders.map( order => {
                return (
                    <Order 
                        key={order.id}
                        ingredients={order.ingredients}
                        price={order.price}
                        orderedAt={order.orderedAt} />
                );
            });
        }

        //If axios Network error occurred
        if( this.state.error ){
            orders = (
                <p style={ {
                    'fontWeight':'bold',
                    'textAlign':'center',
                    'padding': '20px 0'
                } }>Orders can't be loaded</p>
            );
        }

        return (
            <div>
                { orders }
            </div>
        );
    }
}

export default withErrorHandler( Orders, axios );