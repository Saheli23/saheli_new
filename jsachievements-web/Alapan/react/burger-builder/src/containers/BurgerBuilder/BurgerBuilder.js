import React, { Component } from 'react';

import Aux from '../../hoc/Auxiliary/Aux';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axios';

const INGREDIENT_PRICES = {
    salad: 10,
    cheese: 15,
    meat: 40,
    bacon: 30
};

class BurgerBuilder extends Component{

    //Set our state inside the constructor function
    constructor(props){
        super(props);
        this.state = {
            ingredients: null,
            defaultIngredients: null,
            totalPrice: 20,
            purchasable: false, //Initially we start with by setting purchasable to false
            purchasing: false, //Our Order summary Modal should be visible when we set this to true
            loading: false, //state to represent any loading activity like sending a http request and waiting for response
            error: false
        };
    }

    //Fetch Our Ingredients dynamically from firebase
    componentDidMount() {
        axios.get('ingredients.json')
            .then( response => this.setState( { ingredients: response.data, defaultIngredients: response.data } ))
            .catch( error => this.setState( { error : true } ) );   
    }

    //Method to check if burger is purchasable or not
    updatePurchaseState(ingredients){
        const sum   = Object.keys( ingredients )
                            .map( igKey => ingredients[igKey ] )
                            .reduce( ( sum,el ) => sum + el, 0 );
        this.setState({purchasable: sum > 0});                              
    }

    //Add our ingredient here
    addIngredientHandler = (type) => {
        //Update Ingredient count
        const oldCount           = this.state.ingredients[type];
        const updatedCount       = oldCount + 1;
        const updatedIngredients = { ...this.state.ingredients };
        updatedIngredients[type] = updatedCount;

        //Update Total Price of burger
        const priceAddition = INGREDIENT_PRICES[type];
        const oldPrice      = this.state.totalPrice;
        const newPrice      = oldPrice + priceAddition;

        this.setState({
            ingredients: updatedIngredients,
            totalPrice : newPrice
        });

        //Update purchase state
        this.updatePurchaseState(updatedIngredients);

    }

    //Remove our ingredient here
    removeIngredientHandler = (type) => {
        //Update Ingredient count
        const oldCount = this.state.ingredients[type];

        //Check for oldCount if it is less than or equal to 0
        //If we do not check this our ingredient count will become negative and hence will throw error
        if(oldCount <= 0){
            return;
        }

        const updatedCount = oldCount - 1;
        const updatedIngredients = { ...this.state.ingredients };
        updatedIngredients[type] = updatedCount;

        //Update Total Price of burger
        const priceDeduction = INGREDIENT_PRICES[type];
        const oldPrice       = this.state.totalPrice;
        const newPrice       = oldPrice - priceDeduction;

        this.setState({
            ingredients: updatedIngredients,
            totalPrice : newPrice
        });

        //Update purchase state
        this.updatePurchaseState(updatedIngredients);

    }

    //Handler to update purchasing state
    purchasedHandler = () => {
        this.setState({purchasing: true});
    }

    //Handler to update purchasing set to false and hence close Modal
    purchaseCancelHandler = () => {
        this.setState({purchasing: false});
    }

    //Handler to continue purchasing the burger
    purchaseContinueHandler = () => {
        //Pass Ingredients
        const queryParams = Object.keys( this.state.ingredients ).map( igKey => {
                                return encodeURIComponent( igKey ) + "=" + encodeURIComponent( this.state.ingredients[igKey] ); 
                             } );
        //Pass Total price
        queryParams.push( 'price=' + this.state.totalPrice );
        const queryString = queryParams.join( '&' );     
        this.props.history.push( {
            pathname: '/checkout',
            search: '?' + queryString    
        } );
    }
    
    render(){

        //Passing the disabled Information to build controls regarding which buttons will be disabled
        const disabledInfo = { ...this.state.ingredients };
        for(let key in disabledInfo){
            disabledInfo[key] = disabledInfo[key] <= 0;
        }

        //Error Condition to handle when ingredients cant be loaded
        const ingredientError = <p style={ {
            'fontWeight':'bold',
            'textAlign':'center',
            'padding': '20px 0'
        } }>Ingredients can't be loaded</p>;

        let orderSummary = null;

        //Only Render Burger Related Components once ingredients are ready and loaded from firbase
        let burger = this.state.error ? ingredientError  : <Spinner />;
        if( this.state.ingredients ){
            burger      =   <Aux>    
                                <Burger ingredients={this.state.ingredients} />
                                <BuildControls 
                                    ingredientAdded={this.addIngredientHandler}
                                    ingredientRemoved={this.removeIngredientHandler}
                                    ordered={this.purchasedHandler}
                                    disabled={disabledInfo}
                                    purchasable={this.state.purchasable} 
                                    price={this.state.totalPrice}/>
                            </Aux>;
            orderSummary = <OrderSummary
                                price={this.state.totalPrice} 
                                ingredients={this.state.ingredients}
                                purchaseContinued={this.purchaseContinueHandler}
                                purchaseCancelled={this.purchaseCancelHandler} />;
        }
        //Conditionally render between order summary and spinner
        if( this.state.loading ){
            orderSummary = <Spinner />;
        }
    
        return (
            <Aux>
                <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
                       { orderSummary }
                </Modal>
                { burger }
            </Aux>    
        );
    }
}

export default withErrorHandler( BurgerBuilder, axios );