import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import ContactData from '../../containers/Checkout/ContactData/ContactData';

class Checkout extends Component{

    constructor( props ){
        super( props );
        this.state = {
            ingredients: null,
            totalprice: 0
        };
    }

    componentWillMount() {
        //Get URL params and update our ingredients & total price
        const query = new URLSearchParams( this.props.location.search );
        const ingredients = {};
        let price = 0;
        for( let param of query.entries() ){
            if( param[0] === 'price' ){
                price = param[1];
            }else{
                ingredients[param[0]] = +param[1];
            }
        }
        this.setState( { ingredients: ingredients, totalprice: +price } );
    }

    checkoutCancelledHandler = () => {
        //Use goback method to navigate to burger builder page
        this.props.history.goBack();
    }

    checkoutContinuedHandler = () => {
        //If continued then move forwrad to user contact data page
        this.props.history.replace('/checkout/contact-data');
    }

    render() {
        return (
            <div>
                <CheckoutSummary 
                    ingredients={this.state.ingredients}
                    checkoutCancelled={this.checkoutCancelledHandler}
                    checkoutContinued={this.checkoutContinuedHandler}/>
                <Route 
                    path={this.props.match.path + '/contact-data'} 
                    render={ () => ( 
                        <ContactData 
                            ingredients={this.state.ingredients} 
                            totalPrice={this.state.totalprice} /> 
                        ) } />    
            </div>
        );
    }
}

export default Checkout;