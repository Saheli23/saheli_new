import React,  { Component } from 'react';
import { withRouter } from 'react-router-dom';

import Button from '../../../components/UI/Button/Button';
import Spinner from '../../../components/UI/Spinner/Spinner';
import firebase from 'firebase';
import classes from './ContactData.css';


class ContactData extends Component{
    
    constructor( props ){
        super( props );
        this.state = {
            name: '',
            email: '',
            address: {
                zipcode: '',
                houseNo: '',
                locality: '', 
            },
            loading: false
        };
    }

    //Method to handle orders
    orderHandler = ( event ) => {
        event.preventDefault();
        this.setState( { loading: true } );
        //prepare our order data to be stored in the database
        const order = {
            ingredients: this.props.ingredients,
            price: this.props.totalPrice.toFixed(2),
            customer: {
                name: 'Alapan Chatterjee',
                address: {
                    houseNo: '74',
                    locality: 'Ashokgarh',
                    zipCode: '700108',
                    state: 'West Bengal',
                    country: 'India'
                },
                email: 'alapan@capitalnumbers.com'
            },
            deliveryMethod: 'fastest',
            orderedAt: +new Date()
        };
        //store data into firebase
        const db        = firebase.database();
        const orderKey  = db.ref().child( 'orders' ).push().key; //create a new order id
        const dbRef     = db.ref().child( '/orders/' + orderKey );
        
        dbRef.set( order ).then( response => {
            this.setState( { loading: false } );
            this.props.history.push('/');
        })
        .catch( error => {
            this.setState( { loading: false } ) 
        } );
    }

    render() {
        let form = (
            <form>
                <input className={classes.Input} type="text" name="name" placeholder="Your Name" />
                <input className={classes.Input} type="email" name="email" placeholder="Your Email" />
                <input className={classes.Input} type="text" name="zipcode" placeholder="Zipcode" />
                <input className={classes.Input} type="text" name="houseno" placeholder="Houseno" />
                <input className={classes.Input} type="text" name="locality" placeholder="Locality" />
                <Button 
                    btnType="Success"
                    clicked={this.orderHandler}>ORDER</Button>
            </form>
        );
        //Show a spinner until response comes back from the server
        if( this.state.loading ){
            form = <Spinner />;
        }
        return (
            <div className={classes.ContactData}>
                <h4>Enter your contact Data</h4>
                { form }  
            </div>    
        );
    }
}

export default withRouter( ContactData );