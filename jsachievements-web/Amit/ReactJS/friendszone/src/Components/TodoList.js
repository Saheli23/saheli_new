import React, { Component } from 'react';
import Todo from './Todo';
import { Scrollbars } from 'react-custom-scrollbars';

class TodoList extends Component {

  remove(id){	
  	this.props.remove(id);
  }	

  complete(id){
    this.props.complete(id);
  }
  
  render() {
	// Map through the todos
	let todoNode;
	if(this.props.todos){
  	 	todoNode = this.props.todos.map((todo,i) => {
	    	return (<Todo todo={todo} key={i} complete={this.complete.bind(this)} remove={this.remove.bind(this)}/>)
	  	});
  	}	
  	return (
      <Scrollbars style={{ height: 284 }}>
      <div className="list-group">
        <ul className="list" >{todoNode}</ul>
      </div>
      </Scrollbars>
  		
	);
  }
}

export default TodoList;
