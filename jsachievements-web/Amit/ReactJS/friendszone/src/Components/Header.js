import React, { Component } from 'react';


class Header extends Component {
  render() {
    return (
		<div className="header">
      		<span>Todo List Total-{this.props.todoCount}</span>
  		</div>
    );
  }
}

export default Header;
