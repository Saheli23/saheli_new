import React, { Component } from 'react';
import uuid from 'uuid';
// import './App.css';
// import axios from 'axios';

class TodoForm extends Component {
  constructor(props){
  	super(props);
  	this.state = {
  		newTodo : {}
  	}
  }

  addTodo(e){
  	if(this.refs.text.value !== ""){
  		// Assemble data
	    
	    this.setState({newTodo : {
	    	"text"      : this.refs.text.value,
        "id"        : uuid.v4(),
        "complete"  :"false"
	    }},()=>{
	    	this.props.addTodo(this.state.newTodo);
	    });
	    
  	}else{
  		alert('Please put the text');
  	}
  	e.preventDefault();
  }

  render() {
    	// Input Tracker
	  	let input;
	  	// Return JSX
	  	return (
  			<div>
		  		
			    <form onSubmit={this.addTodo.bind(this)} >
			      <input placeholder="Add New Todo" className="form-control col-md-12 add-new" ref="text"/>
			    </form>
		    </div>
    	)
  }
}

export default TodoForm;
