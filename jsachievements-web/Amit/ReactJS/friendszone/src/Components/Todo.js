import React, { Component } from 'react';



class Todo extends Component {
  
  removeTodo(id){
  	this.props.remove(id);
  }

  completeTodo(id){
    this.props.complete(id);
  }

  render() {
    var classes = 'todoList';
    if (this.props.todo.complete === 'true') {
      classes = classes + ' list-item-success';
    }
    return (
  		<li className={classes}>
        {this.props.todo.text}
        <div className="action-section" role="group">
          <button type="button" className="success-btn" onClick={this.completeTodo.bind(this,this.props.todo.id)}>&#x2713;</button> 
          <button type="button" className="error-btn" onClick={this.removeTodo.bind(this,this.props.todo.id)}>&#xff38;</button>
        </div>
  	  </li>
	  );
  }
}

export default Todo;
