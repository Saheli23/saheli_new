import React, { Component } from 'react';
import Header from './Components/Header';
import TodoForm from './Components/TodoForm';
import TodoList from './Components/TodoList';
//Library
// import axios from 'axios';
const todoData = [
					{"id":"1","text": "My First Task","complete":"false"},
					{"id":"2","text": "My Second Task","complete":"false"},
					{"id":"3","text": "My Third Task","complete":"false"},
					{"id":"4","text": "My Fourth Task","complete":"false"},
					{"id":"5","text": "My Fifth Task","complete":"false"}
				 ];
class App extends Component {
	constructor(props){
		super(props);
		this.state = {
			data : []
		};
	}

	componentDidMount(){
	    // Make HTTP request with axios
	    this.getUsers();
	    
  	}
  	getUsers(){
  		// axios.get('https://57b1924b46b57d1100a3c3f8.mockapi.io/api/todos')
		  // .then((res) => {
	        // console.log(res.data);
	        this.setState({data:todoData});
      	// });
  	}

  	handleRemove(id){
  		// console.log(id);
  		//Filter all todos except the one to be removed
	    let remainder = this.state.data.filter((todo) => {
	      if(todo.id !== id) return todo;
	      return false;
	    });
	    this.setState({data: remainder});
	    //Update state with filter
	    // axios.delete('https://57b1924b46b57d1100a3c3f8.mockapi.io/api/todos'+'/'+id)
	    //   .then((res) => {
	    //     this.setState({data: remainder});
     //  	})
  	}

  	handleComplete(id){
  		var data = this.state.data;
		for (var i in data) {
			if (data[i].id === id) {
				data[i].complete = data[i].complete === 'true' ? 'false' : 'true';
				break;
			}
		}
		this.setState({data});
		return;
  	}

  	handleAddTodo(todo){
  		// console.log(todo);
  		let todoDatas = this.state.data;
  		todoDatas.push(todo);
  		this.setState({data: todoDatas});
	  		// Assemble data
	    // const todo = {text: val}
	    // Update data
	    // axios.post('https://57b1924b46b57d1100a3c3f8.mockapi.io/api/todos', todo)
	    //    .then((res) => {
	    //       this.state.data.push(res.data);
	    //       this.setState({data: this.state.data});
     //   	});
     //   	this.getUsers();
  	}

  	render() {
	    return (
	      <div className="wrap">
	      	<Header todoCount={this.state.data.length}/>
	      	<TodoForm addTodo={this.handleAddTodo.bind(this)}/>
	      	<TodoList todos={this.state.data} complete={this.handleComplete.bind(this)} remove={this.handleRemove.bind(this)} />
	      </div>
	    );
	}
}

export default App;
