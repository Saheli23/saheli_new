import React, { Component } from 'react';
// import logo from './logo.svg';

import './App.css';
import Gallery from 'react-photo-gallery';
// import Measure from 'react-measure';
import Lightbox from 'react-images';
const PHOTO_SET = [
  {
    src: 'https://farm6.staticflickr.com/5603/15577035142_6e2f35581d.jpg',
    srcset: [
      'https://farm6.staticflickr.com/5603/15577035142_6e2f35581d.jpg 333w',
      'https://farm6.staticflickr.com/5603/15577035142_6e2f35581d_c.jpg 533w',
      'https://farm6.staticflickr.com/5603/15577035142_6e2f35581d_b.jpg 682w',
      'https://farm6.staticflickr.com/5603/15577035142_bf7441b8a0_h.jpg 1065w'
    ],
    sizes:[
      '(min-width: 480px) 50vw',
      '(min-width: 1024px) 33.3vw',
      '100vw'
    ],
    width: 681,
    height: 1024,
    alt: 'image 1',
  },
  {
    src: 'https://farm6.staticflickr.com/5825/20149281203_0335e3ed19.jpg',
    srcset: [
      'https://farm6.staticflickr.com/5825/20149281203_0335e3ed19.jpg 500w',
      'https://farm6.staticflickr.com/5825/20149281203_0335e3ed19_c.jpg 800w',
      'https://farm6.staticflickr.com/5825/20149281203_0335e3ed19_b.jpg 1024w',
      'https://farm6.staticflickr.com/5825/20149281203_16cdbb86d5_h.jpg 1600w',
    ],
    sizes:[
      '(min-width: 480px) 50vw',
      '(min-width: 1024px) 33.3vw',
      '100vw'
    ],
    width: 600,
    height: 600,
    alt: 'image 2',
  },
  {
    src: 'https://farm4.staticflickr.com/3948/15421251759_79587a5f68.jpg',
    srcset: [
      'https://farm4.staticflickr.com/3948/15421251759_79587a5f68.jpg 500w',
      'https://farm4.staticflickr.com/3948/15421251759_79587a5f68_c.jpg 800w',
      'https://farm4.staticflickr.com/3948/15421251759_79587a5f68_b.jpg 1024w',
      'https://farm4.staticflickr.com/3948/15421251759_0091d2645c_h.jpg 1600w',
    ],
    sizes:[
      '(min-width: 480px) 50vw',
      '(min-width: 1024px) 33.3vw',
      '100vw'
    ],
    width: 219.30749999999998,
    height: 324,
    alt: 'image 3',
  },
   {
    src: 'https://farm4.staticflickr.com/3942/15421889947_2c211aebbb.jpg',
    srcset: [
      'https://farm4.staticflickr.com/3942/15421889947_2c211aebbb.jpg 500w',
      'https://farm4.staticflickr.com/3942/15421889947_2c211aebbb_c.jpg  800w',
      'https://farm4.staticflickr.com/3942/15421889947_2c211aebbb_b.jpg 1024w',
      'https://farm4.staticflickr.com/3942/15421889947_b4ab3190ec_h.jpg 1600w',
    ],
    sizes:[
      '(min-width: 480px) 50vw',
      '(min-width: 1024px) 33.3vw',
      '100vw'
    ],
    width: 215.66250000000002,
    height: 324,
    alt: 'image 4',
  },
  {
    src: 'https://farm4.staticflickr.com/3942/15421889947_2c211aebbb.jpg',
    srcset: [
      'https://farm4.staticflickr.com/3942/15421889947_2c211aebbb.jpg 500w',
      'https://farm4.staticflickr.com/3942/15421889947_2c211aebbb_c.jpg  800w',
      'https://farm4.staticflickr.com/3942/15421889947_2c211aebbb_b.jpg 1024w',
      'https://farm4.staticflickr.com/3942/15421889947_b4ab3190ec_h.jpg 1600w',
    ],
    sizes:[
      '(min-width: 480px) 50vw',
      '(min-width: 1024px) 33.3vw',
      '100vw'
    ],
    width: 215.66250000000002,
    height: 324,
    alt: 'image 4',
  },

];
const Vidstyle = {
  background: "#666",
  position: 'relative',
  
};
class App extends Component {
  constructor(){
    super();
    this.state = {photos:null, pageNum:1, totalPages:1, loadedAll: false, currentImage:0};
   
    this.closeLightbox = this.closeLightbox.bind(this);
    this.openLightbox = this.openLightbox.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
    this.gotoPrevious = this.gotoPrevious.bind(this);
  }
  openLightbox(index, event){
    event.preventDefault();
    this.setState({
      currentImage: index,
      lightboxIsOpen: true
    });
  }
  closeLightbox(){
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  }
  gotoPrevious(){
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  }
  gotoNext(){
    if(PHOTO_SET.length - 2 === this.state.currentImage){
      //this.loadMorePhotos();
    }
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  }
  render() {
    return (
      <div className="App">
        
          <nav className="navbar navbar-default" >
            <div className="container-fluid">
              <div className="navbar-header">
                <a className="navbar-brand" href="#">WebSiteName</a>
              </div>
              <ul className="nav navbar-nav pull-right" >
                <li className="active"><a href="#">Home</a></li>
                <li><a href="#">Page 1</a></li>
                <li><a href="#">Page 2</a></li>
                <li><a href="#">Page 3</a></li>
              </ul>
            </div>
          </nav>
           <div id='big-video-wrap' style={Vidstyle}>
           
           <video id="big-video-vid_html5_api" loop className="vjs-tech" preload="auto" data-setup="{}"
           autoPlay src="https://player.vimeo.com/external/134025228.hd.mp4?s=9fdf3d36312edfd18b3a5a8df0be17e3&amp;profile_id=113"></video>
              <div className="upper-video-text">
                <h1 className="hero-header">Wilderness within reach</h1>
                  <div className="sub-content">
                     <p>This is the place where forest, river and sea meet at the end of the West Coast Road.
                     </p>
           
                  </div>
             </div>
             <a className="rust-icons animateIn link-arrow smooth-scroll" href="#content-block-2"><img src={"Images/if_Arrow.png"} /></a>
           </div>
          
       
          <Gallery photos={PHOTO_SET} onClickPhoto={this.openLightbox}/>
          <Lightbox 
            theme={{container: { background: 'rgba(0, 0, 0, 0.85)' }}}
            images={PHOTO_SET}
            backdropClosesModal={true}
            onClose={this.closeLightbox}
            onClickPrev={this.gotoPrevious}
            onClickNext={this.gotoNext}
            currentImage={this.state.currentImage}
            isOpen={this.state.lightboxIsOpen}
            width={1600}
          />
       
      </div>
    );
  }
}

export default App;
